# Project Title: Docker Container Generator for CREDIL

This is a pilot project to help CREDIL developers in creating containers.

## Getting Started

To start up your container go to your dockergen folder and run:
./dockergen.sh

Provide your container, project and volume names.

Docker name: mycontainer

Project name: website 
The exception is for option 5, this variable is not be used.

Volume/Service name [code]: [code] means default value if field is blank


Then select one of container types:
´1. Rails Base

´2. Rails CREDIL

´3. Django + Postgresql

´4. Flask + Ubuntu

´5.  Rails - Jason's framework


Provide the version you would like to have installed or accept the default.

Change to the container folder and run:

docker-compose up


Sometimes you also need to install database:

docker-compose run web rake db:create

Notes:

1) Once you have your docker container created you may edit Dockerfile and docker-compose.yml for your specific need.

2) To run and access the container execute the following:

docker exec -ti <service name> /bin/bash

> You can find the <service name> running docker-compose ps and get the firs column Name.

### Prerequisites

What things you need to install the software and how to install them

```
Install docker and docker-compose, in some computers there is need to install dialog (it creates box/window to select)
```
```
To show your full name, please add your full name inside /etc/passwd files
...
amisaka:x:1085:1085:Antonio Misaka:/home/amisaka:/bin/bash
...
```
## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Antonio Misaka** - *Initial work* - [CREDIL](http://www.credil.org)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
