#!/bin/bash

### set -x
echo -e "**** Welcome to dockerizing tool ****\n\tHere are the options:\n\t====================="
echo -e "\t1. Rails Base\n\t2. NGINX with Alpine\n\t3. Django + Postgresql\n\t4. Flask + Ubuntu\n\t5. Rails - Jason's framework"
read -p "Docker folder name: " dname
read -p "Project name: [leave it blank for option 5] " pname
echo "Creating the Docker project"
mkdir -p $dname

HEIGHT=12
WIDTH=40
CHOICE_HEIGHT=5
title='Select the project type: '
prompt="Pick an option:"
options=(1 "Rails Base"
         2 "NGINX with Alpine"
         3 "Django + Postgresql"
         4 "Flask + Ubuntu"
         5 "Rails - Jason's framework")
backtitle="Docker Container Generation Tool"
tversion=""

echo "$title"
PS3="$prompt "

CHOICE=$(dialog --clear --backtitle "$backtitle" \
                --title "$title" \
                --menu "$prompt" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${options[@]}" \
                 2>&1 >/dev/tty)
clear

case $CHOICE in
  1)
    read -p "Volume + Container name [code]: " vname
    vname=${vname:-code}
    read -p "Ruby version range [2.5]: " tversion
    tversion=${tversion:-2.5}
    read -p "Rails version range [5.2.0]: " rorver
    rorver=${rorver:-5.2.0}
    bash ./lib/dfileror.sh ruby $vname $tversion > ./$dname/Dockerfile
    bash ./lib/dgemfile.sh $rorver > ./$dname/Gemfile
    touch ./$dname/Gemfile.lock
    bash ./lib/dcompror.sh "postgres" "3000" $dname $vname > ./$dname/docker-compose.yml
    cd $dname
    docker-compose run web rails new . --force --database=postgresql
    /usr/bin/sudo chown $USER:$USER -R . 2> /dev/null
    docker-compose build
    cd ..
    cp $dname/config/database.yml database.yml
    ./lib/replacer -i database.yml -s ./templates/ser_rordb.txt -r ./templates/rep_rordb.txt -o databasetmp.yml
    mv databasetmp.yml $dname/config/database.yml
    rm database.yml
    whiptail --title "Additional Steps" --msgbox "Run the following command inside the Docker folder\ndocker-compose up" 8 78
    whiptail --title "Create the Database" --msgbox "In another terminal, maybe as sudo run:\ndocker-compose run web rake db:create" 8 78
    ;;
  2)
    echo "Generating NGINX Docker container"
    bash ./lib/dfilenginx.sh "alpine" "latest" > ./$dname/Dockerfile
    bash ./lib/dcompnginx.sh "8080" > ./$dname/docker-compose.yml
    cp -r ./templates/files_nginx ./$dname/files
    cd $dname
    docker image build --tag local:docker-nginx .
    whiptail --title "Additional Steps" --msgbox "Run the following command inside the Docker folder\ndocker-compose up" 8 78
    ;;
  3)
    #set -x
    read -p "Volume + Container name [code]: " vname
    vname=${vname:-code}
    read -p "First application name [myapp]: " appname
    appname=${appname:-myapp}
    mkdir -p ./$dname/templates/$appname
    cp ./templates/bs_index.html ./$dname/templates/$appname/index.html
    read -p "Django version range [2.1.1]: " tversion
    tversion=${tversion:-2.1.1}
    bash ./lib/dfilepython.sh python $vname > ./$dname/Dockerfile
    bash ./lib/dreqpython.sh $tversion > ./$dname/requirements.txt
    bash ./lib/dcomppython.sh "postgres" "8000" $dname $vname $appname > ./$dname/docker-compose.yml
    cd $dname
    sudo -u $USER -c $(docker-compose run web django-admin.py startproject $pname .)
    /usr/bin/sudo chown $USER:$USER -R . 2> /dev/null
    cd ..
    cp $dname/$pname/settings.py settings.py
    ./lib/replacer -i settings.py -s ./templates/ser_allowed_hosts.txt -r ./templates/rep_allowed_hosts.txt -o settings1.py
    mv settings1.py settings.py
    ./lib/replacer -i settings.py -s ./templates/ser_databases.txt -r ./templates/rep_pgdatabase.txt -o settings1.py
    mv settings1.py $dname/$pname/settings.py
    rm settings.py
    whiptail --title "Additional Steps" --msgbox "Run the following command inside the Docker folder\ndocker-compose up --build" 8 78
    ;;
  4)
    read -p "Volume + Container name [code]: " vname
    vname=${vname:-code}
    read -p "Ubuntu version [16.04]: " tversion
    tversion=${tversion:-16.04}
    bash ./lib/dgenflaskapp.sh > ./$dname/app.py
    bash ./lib/dfileflask.sh ubuntu $vname $tversion > ./$dname/Dockerfile
    bash ./lib/dreqflask.sh  > ./$dname/requirements.txt
    bash ./lib/dcompflask.sh "redis" "5000" $dname $vname > ./$dname/docker-compose.yml
    cd $dname
    whiptail --title "Start the application" --msgbox "Run the follow command inside the Docker folder\ndocker-compose up" 8 78
    ;;
  5)
    # set -x
    pname="docker"
    appname="app"
    read -p "Ruby version range [2.3.1]: " tversion
    tversion=${tversion:-2.3.1}
    read -p "Rails version range [5.0.0]: " rorver
    rorver=${rorver:-5.0.0}
    # read -p "First application name [app]: " appname
    # appname=${appname:-app}
    read -p "Volume + Container app name [/var/www/rails]: " vname
    vname=${vname:-/var/www/rails}
    read -p "Volume + Container development name [/var/www/rails_dev]: " vname2
    vname2=${vname2:-/var/www/rails_dev}
    mkdir -p $dname/$pname/$appname
    mkdir -p $dname/$pname/web
    mkdir -p $dname/src/public/
    mkdir -p $dname/etc/private/
    mkdir -p $dname/var/private/
    bash ./lib/djbgemfile.sh $rorver > ./$dname/src/Gemfile
    bash ./lib/dvimrcror.sh > ./$dname/var/.vimrc
    bash ./lib/dcompcredilror.sh "postgres" "8002" $pname $appname > ./$dname/docker-compose.yml
    cp ./templates/jb_rails_README.txt ./$dname/README.txt
    cp ./templates/jb_rails_nginx.conf ./$dname/$pname/web/nginx.conf
    cp ./templates/jb_Capfile ./$dname/src/Capfile
    cp ./templates/jb_Gemfile.lock ./$dname/src/Gemfile.lock
    cp ./templates/jb_Guardfile ./$dname/src/Guardfile
    cp ./templates/jb_Procfile ./$dname/src/Procfile
    cp ./templates/jb_Rakefile ./$dname/src/Rakefile
    cp ./templates/jb_README.md ./$dname/src/README.md
    cp ./templates/jb_etc_README.txt ./$dname/etc/README.txt
    cp ./templates/jb_var_README.txt ./$dname/var/README.txt
    cp -r ./templates/jb_src/* ./$dname/src/
    bash ./lib/dfilecredilrorapp.sh ruby $tversion $vname > ./$dname/$pname/$appname/Dockerfile
    bash ./lib/dfilecredilrorweb.sh nginx $vname2 $pname > ./$dname/$pname/web/Dockerfile
    whiptail --title "Success!" --msgbox "Your templates are installed and will be built and run in a while." 8 78
    cd $dname
    docker-compose build
    docker-compose up
    cd ..
    ;;
esac
