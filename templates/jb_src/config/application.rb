require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Beaumont4
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller

    config.generators do |g|
      g.factory_bot false
    end

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    config.active_record.time_zone_aware_types = [:datetime]

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.default_locale = :de

    # Autoload locales in semantic paths
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    # Autoload lib paths
    config.autoload_paths << Rails.root.join('lib')

    # Eager load (for production) paths
    config.eager_load_paths << Rails.root.join('lib')

    # set up ActiveJob.
    config.active_job.queue_adapter = :delayed_job
  end
end

# MONKEY PATCH so that we can keep pepper in config/secrets.yml
module Devise
  class Engine
    initializer "devise.pepper" do |app|
      if app.respond_to?(:secrets)
        Devise.pepper ||= app.secrets.pepper
        #puts "APP has new pepper: #{Devise.pepper}\n"
      end
    end
  end
end
