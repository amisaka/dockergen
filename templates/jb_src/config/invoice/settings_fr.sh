# Language specific settings for French.
# These variables specify values that cannot be set through the html content.

# title displayed by the pdf reader
TITLE="IP4B Inc. facture"
# invoice footer text
FOOTER_RIGHT_TEXT="Page [page] de [topage]"
FOOTER_CENTER_TEXT="IP4B Inc. 9600, boul. du Golf, Montréal (Quebéc) H1J 3A8"
