# Language neutral settings that are imported into bash scripts, centralizing script options.
# NOTE: "USER_PATH" this is the most likely folder to configure to point to a system specific location.

BASH_SCRIPT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

# root of git repository
GIT_ROOT_PATH="${BEAUMONT_PATH-$(dirname "$BASH_SCRIPT_PATH")}"

# settings used both for data cleaning and processing.

USER_FOLDER="user"
XML_FOLDER="xml"
XSLT_FOLDER="xsl"

CUSTOMER_FILENAME="customer.xml"
SERVICES_FILENAME="services.xml"
INVOICE_FILENAME="invoice.xml"
CHARGES_FILENAME="charges.xml"
PREVIOUS_CHARGES_FILENAME="previous_charges.xml"
LINES_FILENAME="lines.xml"
CALLS_FILENAME="calls.xml"
AREA_CODES_FILENAME="area_codes.xml"

# base folder for data.
DATA_FOLDER="data"

# folder that contains xml templates that are used as defaults.
DEFAULT_FOLDER="default"

CUSTOMERS_FOLDER="customers"
SERVICES_FOLDER="services"
SUMMARY_FOLDER="summaries"
CALLS_FOLDER="calls"
PAYMENT_FOLDER="payment"

# path for temporary files that need multiple passes to clean.
TEMP_FOLDER="tmp"

# path for data that is in a bad format and needs to be parsed.
RAW_FOLDER="raw"

# path to root of user data
DATA_PATH=${DATA_PATH-"$GIT_PARENT_PATH/$DATA_FOLDER"}

# path to root of user data.
# defaults to creating a temp folder in the root of the data folder.
TEMP_PATH="$DATA_PATH/$TEMP_FOLDER"

# path to root of xslt transforms location
XSLT_PATH="$GIT_ROOT_PATH/lib/invoicing/$XSLT_FOLDER"

# base path for each user folder
# user data is grouped into a folder named for the billing phone number
USER_PATH=${USER_PATH-"$DATA_PATH/$USER_FOLDER"}

# base path for cleaned data.

SERVICES_PATH="$DATA_PATH/$SERVICES_FOLDER"

# base path for source dirty data.

CUSTOMERS_RAW_PATH="$DATA_PATH/$RAW_FOLDER/$CUSTOMERS_FOLDER"
SERVICES_RAW_PATH="$DATA_PATH/$RAW_FOLDER/$SERVICES_FOLDER"
CALLS_PATH="$DATA_PATH/$RAW_FOLDER/$CALLS_FOLDER"
PAYMENT_RAW_PATH="$DATA_PATH/$RAW_FOLDER/$PAYMENT_FOLDER"

# paths for data containing summary information used in blocks of the invoice.

CHARGE_SUMMARY_FOLDER="charge_summary"
CHARGE_SUMMARY_PATH="$DATA_PATH/$SUMMARY_FOLDER/$CHARGE_SUMMARY_FOLDER"

AREA_CODE_SUMMARY_FOLDER="area_code_summary"
AREA_CODE_SUMMARY_PATH="$DATA_PATH/$SUMMARY_FOLDER/$AREA_CODE_SUMMARY_FOLDER"

LINE_SUMMARY_FOLDER="line_summary"
LINE_SUMMARY_PATH="$DATA_PATH/$SUMMARY_FOLDER/$LINE_SUMMARY_FOLDER"

# output specific settings.

HTML_FOLDER="xhtml"
PDF_FOLDER="pdf"
ORGANIZATION="default"
RESOURCE_URL=".." # or "http://localhost:9000"
FOOTER_FONT_SIZE=8
BASE_URL="file://${OUTPUT_PATH}" # or "http://localhost:9000"


