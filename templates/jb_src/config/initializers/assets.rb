# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.2'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

# Rails.application.config.assets.prefix = 'assets'
Rails.application.config.assets.precompile += %w( active_scaffold/indicator.gif )
Rails.application.config.assets.precompile += %w( project_logo_small.png )
Rails.application.config.assets.precompile += %w( prev_arrow.png next_arrow.png )
Rails.application.config.assets.precompile += %w( close.png )

# AdminLTE template
Rails.application.config.assets.precompile += %w( admin_lte.css )
Rails.application.config.assets.precompile += %w( admin_lte.js )

# Images
Rails.application.config.assets.precompile += %w( user2-160x160.jpg )

# Invoices
Rails.application.config.assets.precompile += %w( invoice.css )
Rails.application.config.assets.precompile += %w( extra.css )
