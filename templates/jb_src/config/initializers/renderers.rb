# config/initializers/renderers.rb
ActionController::Renderers.add :pdf do |obj, options|
  filename = options[:filename] || 'document'
  pdf = obj.respond_to?(:to_pdf) ? obj.to_pdf : obj.to_s
  send_data(pdf, filename: "#{filename}.pdf", type: 'application/pdf')
end
