# RAVEN specific configuration (.dsn) has been moved to environment/production.rb
# on the target machines themselves. With .dsn, raven will not be enabled.
Raven.configure do |config|
  config.environments = [ENV['SENTRY_ENV']]
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
  config.silence_ready = true
  config.dsn = ENV['SENTRY_DSN']
end
