Rails.application.routes.draw do

  resources :inventories
  get 'inventories/:id/mod_new', to: 'inventories#mod_new', as: :mod_new_inventory

  resources :contacts, only: [:index]

  resources :payments
  root 'root#index'

  post 'reset', to: 'query#reset'
  post 'clients_reset', to: 'clients#reset'

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  post '/search', to: 'searches#create'
  get '/search', to: 'searches#show', as: :show_search

  resources :dashboards, only: [:index] do
    collection do
      get 'payment/process', to: 'dashboards#process_payment'
    end
  end

  concern :activatable do
    patch :activate
    patch :deactivate
  end


  resources :billable_services, concerns: :activatable do
    collection do
      get '/exports', to: 'billable_services#exports'
      get '/onetime', to: 'billable_services#one_time_exports'
    end
  end


  resources :settings
  resources :contacts
  resources :payments
  # resources :rates
  # resources :services
  resources :billable_services
  # resources :stats_periods
  # resources :performance_stats
  # resources :local_exchanges
  # resources :local_exchanges_north_american_exchanges
  # resources :north_american_exchanges
  # resources :rate_centers
  # resources :calls
  # resources :products, only: [:index, :show, :edit, :update, :new ]
  resources :products
  resources :invoices, only: [:index, :show]

  resources :clients do
    resource :billing, only: [:edit, :update]

    #resources :contacts, only: [:index, :phone_contact_create]
    #resources :contacts, concerns: :activatable

    resources :contacts do

      collection do
        resources :phone_contacts#, :only => [:new, :edit, :destroy]
        resources :email_contacts#, :only => [:new, :edit, :destroy]
        # resources :technical_contacts, :only => [:new, :edit, :destory]
        resources :technical_contacts
      end

    end
    resources :billable_services, concerns: :activatable, :only => [:new, :edit, :index]
    resources :invoices, only: [:index, :show]

    resources :payments

    collection do
      get 'typeahead', constraints: { format: 'json' }
    end

    resources :sites, concerns: :activatable

    # resources :calls do
    #   resources :cdrs
    # end

    # resources :incoming_cdrs, :controller => "cdrs"
    #
    # resources :outgoing_cdrs, :controller => "cdrs"

    # resources :btns do
    #
    #   resources :phones
    #
    #   get '/invoices', to: 'invoices#index'
    #   get '/invoices/:year/:month', to: 'invoices#show'
    #
    #   resources :calls
    # end

    resources :services, concerns: :activatable
    # resources :billable_services, concerns: :activatable
    resources :sites, concerns: :activatable
    resources :buildings
    # resources :callstats
    #
    # resources :stats_periods do
    #   resources :callstats
    # end
  end

  # resources :services do
  #   resources :client_services
  # end

  # resources :client_services

  resources :btns do
    collection do
      get 'typeahead', constraints: { format: 'json' }
    end

    # resources :calls
  end

  # resources :datasources do
  #   resources :calls
  #   resources :cdrs do
  #     resources :calls
  #   end
  # end

  resources :sites, concerns: :activatable

  resources :buildings

  resources :services, concerns: :activatable, path_names: { new: 'new/client/:client_id'}

  # resources :callingphones, :controller => "phones" do
  #   resources :cdrs
  # end
  #
  # resources :calledphones, :controller => "phones" do
  #   resources :cdrs
  # end

  # resources :cdrs do
  # end

  namespace :admin do
    resources :users
  end

  resources :client_line_of_businesses, only: [:update]

  namespace :api, defaults: { format: 'json' } do
      namespace :v1, constraints: { format: 'json' } do
        resources :btns, only: [], param: 'slug' do
          resources :services, only: [:index, :show], param: 'slug'
        end
      end
  end

end
