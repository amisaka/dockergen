require 'date'

$LOG_NEW_CLIENTS = true
$NOLOG_TESTING = false
if ENV['HAKKU']
        $CONNECTMSSQL = true
        $HAKKU        = true
elsif ENV['HAKKA']
        $CONNECTMSSQL = true
        $HAKKU        = false
else
        $CONNECTMSSQL = false
        $HAKKU        = false
end
$INVOICEDEBUG = false;
$CDR_LOAD_PRELOAD_PHONES = true;
$SUITECRM_ENABLED = false
$CALC_PRICE_AT_CDRLOAD = false

# 1800GOLFTIP = 1-800-465-4847
$BtnOfLastResortId  = nil
$LastResortClientId = 1
$NumberOfPhonesAutocreated = 0
$AutoCreatePhoneDebug = false
$DefaultRateCenter    = 999999
$InvoiceRunHistoryLegacy = 29932
$XSP_URL = "http://xsp1.example.com/com.broadsoft.xsi-actions/v2.0/"

$INVOICEDEBUG = true
$InvoiceOutputDir ||= nil
$InvoiceBaseUrl = "/files/invoices"

$TelcordiaDataDir = File.join(ENV['HOME'], "ip4b/telecom/Technique/Telcordia")
$LegacyCdrDir      = File.join(ENV['HOME'], "ip4b/telecom/Operations/CDR");
$LegacyInvoicingEpoch = Date.new(2017,1,1)


# these must be set before Rails loads files, as it affects relationships.
$OTRS   = !(ENV['OTRS']   == 'false')
$PORTAL = !(ENV['PORTAL'] == 'false')



# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!



require 'gpgme'
begin
  GPGME::Key.import(File.open(File.expand_path("config/gpgme/pubring.gpg")))
end

# initialize UUID, and forget about state file for now
UUID.state_file = false
UUID.generator.next_sequence
