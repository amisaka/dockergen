FactoryBot.define do
  factory :did_supplier do
    name { ['Troop supplier', 'Broadsoft supplier', 'Draper LD cards', 'OBM records', 'Telus Rebiller', 'Rogers'].sample }
    voip { [true, false].sample }
  end
end
