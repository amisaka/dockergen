FactoryBot.define do
  factory :building do
    address1 { "#{FFaker::AddressCA.building_number} #{FFaker::AddressCA.street_name}" }
    address2 { ['', FFaker::AddressCA.neighborhood, FFaker::AddressCA.secondary_address].sample }
    city { FFaker::AddressCA.city }
    province { FFaker::AddressCA.province }
    country { FFaker::AddressCA.country }
    postalcode { FFaker::AddressCA.postal_code }
  end
end
