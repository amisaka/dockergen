FactoryBot.define do
  factory :phone do
    site
    phone_number { "+1#{FFaker::PhoneNumber.short_phone_number.gsub('-','')}" }
    callerid { [phone_number, "+1#{FFaker::PhoneNumber.short_phone_number.gsub('-','')}"].sample }
    extension { (0..9999).to_a.sample }
    notes { FFaker::Lorem.sentence }

    sip_username { phone_number }
    email { FFaker::Internet.email }

    activated { [true, false].sample }
    activated_at { rand(Date.civil(2004,1,1)..Date.today) }
    deactivated_at { deactivated? ? rand(activated_at..Date.today) : nil }

    factory :phone_with_client do
      btn
    end
  end
end
