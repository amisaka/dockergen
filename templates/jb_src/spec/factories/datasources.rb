FactoryBot.define do
  factory :datasource do
    active_day { 0 }
    analysis_level { 0 }
    calls_count { 1 }
    cdrs_count { 4 }
    datasource_filename { "oneCallMultipleLegs_file1.csv" }
    load_time { 3 }
    did_supplier_id { [4,5,6,8].sample }

    # Create a new datasource that is supplied by Telus
    factory :datasource_from_telus do
      did_supplier_id 14
    end

    # Create a new datasource that is supplied by Rogers
    factory :datasource_from_rogers do
      did_supplier_id 15
    end

    # Create a new datasource that is supplied by Drapper
    factory :datasource_from_drapper do
      did_supplier_id 12
    end

    # Create a new datasource that is supplied by Bell
    factory :datasource_from_bell do
      did_supplier_id 16
    end

    # Create a new datasource that is supplied by Troop
    factory :datasource_from_troop do
      did_supplier_id 10
    end

    # Create a new datasource that is supplied by OMB
    factory :datasource_from_obm do
      did_supplier_id 13
    end
  end
end
