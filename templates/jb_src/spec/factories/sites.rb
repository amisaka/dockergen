FactoryBot.define do
  factory :site do
    name { FFaker::Venue.name }
    client
    # association :site_rep, factory: :otrs_customer_user
    building
    unitnumber { [('A'..'Z').to_a.sample, (1..500).to_a.sample ].sample }
    site_number { (1..7777).to_a.sample }

    activated { [true, false].sample }
    activated_at { rand(Date.civil(2004,1,1)..Date.today) }
    deactivated_at { deactivated? ? rand(activated_at..Date.today) : nil }
  end
end
