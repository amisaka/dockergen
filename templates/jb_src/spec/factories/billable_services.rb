FactoryBot.define do
  factory :billable_service do
    client
    service
    invoice

    qty { (1..5).to_a.sample }

    billing_period { [:never, :monthly, :yearly].sample }

    # TODO: rework
    billing_period_started_at do
      date1 = DateTime.parse('2015-01-01')
      date2 = DateTime.parse('2016-12-31')
      Time.at((date2.to_f - date1.to_f)*rand + date1.to_f).at_beginning_of_month
    end

    # TODO: rework
    billing_period_ended_at { billing_period_started_at.at_end_of_month }

    service_name "555-555-5555"
    discount 1.0

    comments { FFaker::Lorem.phrase }
    totalprice { (0..20).to_a.sample + (0..99).to_a.sample / 100.0 }

  end
end
