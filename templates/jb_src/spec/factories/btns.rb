FactoryBot.define do
  factory :btn do
    client

    billingtelephonenumber { "+1#{FFaker::PhoneNumber.short_phone_number.gsub('-','')}" }

    rate_plan_id 1
    rate_center_id 455
    local_rate_plan_id 1
    national_rate_plan_id 2
    world_rate_plan_id 3

    # factory :btn_with_billable_services do
    #   transient do
    #     invoices_count (1..3).to_a.sample
    #   end
    #
    #   after(:create) do |btn, evaluator|
    #     create_list(:invoice_with_billable_services, evaluator.invoices_count, btn: btn, client: btn.client)
    #   end
    # end

    # factory :btn_with_invoices do
    #   transient do
    #     invoices_count (1..3).to_a.sample
    #   end
    #
    #   after(:create) do |btn, evaluator|
    #     create_list(:invoice, evaluator.invoices_count, btn: btn, client: btn.client)
    #   end
    # end

  end
end
