FactoryBot.define do
  factory :crm_account do
    name { FFaker::Company.name }
  end
end
