FactoryBot.define do
  factory :client do
    transient do
      sites_count (1..3).to_a.sample
    end

    trait :active do
      started_at { Time.now - 1.year }
      terminated_at { nil }
    end

    trait :pending_start do
      started_at { Time.now + 30.days }
      terminated_at { nil }
    end

    trait :closed do
      started_at { Time.now - 1.year }
      terminated_at { Time.now - 30.days }
    end

    trait :pending_close do
      started_at { Time.now - 1.year }
      terminated_at { Time.now + 30.days }
    end

    trait :unknown do
      started_at { nil }
      terminated_at { nil }
    end

    admin_contact { FFaker::Name.name }
    technical_contact { FFaker::Name.name }
    description { FFaker::Company.bs }
    language_preference { ['French', 'English'].sample }

    crm_account

    after(:create) do |client, evaluator|
      create_list(:site, evaluator.sites_count, client: client)
      client.billing_site = client.sites.first
      client.save
    end

    # factory :client_with_btns do
    #   transient do
    #     btns_count (1..3).to_a.sample
    #   end
    #
    #   after(:create) do |client, evaluator|
    #     create_list(:btn, evaluator.btns_count, client: client)
    #   end
    # end
    #
    # factory :client_with_billable_services do
    #   transient do
    #     btns_count (1..3).to_a.sample
    #   end
    #
    #   after(:create) do |client, evaluator|
    #     create_list(:btn_with_billable_services, evaluator.btns_count, client: client)
    #   end
    # end
    #
    # factory :client_with_invoices do
    #   transient do
    #     btns_count (1..3).to_a.sample
    #   end
    #
    #   after(:create) do |client, evaluator|
    #     create_list(:btn_with_invoices, evaluator.btns_count, client: client)
    #   end
    # end

  end
end
