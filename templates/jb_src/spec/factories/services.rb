FactoryBot.define do
  factory :service do
    description { FFaker::Lorem.phrase }
    name { FFaker::Lorem.words(4).join(' ').capitalize }
    unitprice { (0..20).to_a.sample + (0..99).to_a.sample / 100.0 }
    renewable { [true, false].sample }
    available { [true, false].sample }

    trait :renewable do
      renewable true
    end

    trait :available do
      available true
    end
  end
end
