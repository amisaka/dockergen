FactoryBot.define do
  factory :user do
    email { FFaker::Internet.email }
    password { FFaker::Internet.password }
    password_confirmation { password }
    fullname { FFaker::Name.name }

    admin { [true, false].sample }
    agent { [true, false].sample }

    before(:create) { |user| user.skip_confirmation! }
  end
end
