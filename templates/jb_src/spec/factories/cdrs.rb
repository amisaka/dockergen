FactoryBot.define do
  factory :cdr do

    accesscallid { "c1134d30812c6f3b" }
    accessdeviceaddress { "192.168.4.190" }
    answerdatetime { "2011-09-19 13:38:51 Z" }
    answerindicator { 'Yes' }
    answertime { '20110919133851.238' }
    ascalltype { "Network" }

    # call

    callcategory { ['other', 'internat', 'intralat', 'national', 'private', 'local'].sample }

    callednumber { '5143527746' }

    association :calledphone, factory: :phone
    association :calledbtn, factory: :btn

    callingnumber { '18009598281' }
    callingpartycategory { "Ordinary" }
    callingpresentationindicator { "Public" }
    calltype { "Normal" }
    chargeindicator { [true, false].sample }
    codec { "PCMU/8000" }
    configurableclid { '+15146670180' }

    datasource

    destinationnumber { '5143527746' }
    destinationportedflag { [true, false].sample }
    dialeddigits { '5143527746' }
    direction { "Terminating" }
    duration { "8" }
    lineno { "8767" }
    localcallid { '8772939:1' }
    networkcallid { "BW0938510521909111414036767@192.168.21.2" }

    # Network Call Type
    networkcalltype { ['ispl', 'to', 'lo', 'in'].sample }

    networktranslatednumber { '5143527746' }
    networktype { "VoIP" }
    newanswertime { "2000-01-01 13:38:51 Z" }
    newreleasetime { "2000-01-01 13:38:59 Z" }
    originatenumber { '+13000003104' }
    originateportedflag { [true, false].sample }
    productid { '3000003104' }
    recordid { "00011716910016365B3F1A20110919133851.0441-040000" }
    releasedatetime { "2011-09-19 13:38:59 Z" }
    releasetime { "'20110919133859.211'" }
    releasingparty { "local" }
    route { "192.168.4.136:5060" }
    serviceprovider { "NovaVision Service Provider" }
    startdatetime { "2011-09-19 13:38:51 Z" }
    starttime { "'20110919133851.044'" }
    terminationcause { '016' }
    typeofnetwork { "public" }
    usagetype { "LOC" }

    userid { "3000003104@voip.novavision.ca" }
    usernumber { '+13000003104' }
    usertimezone { "1-040000" }

    factory :cdr_from_telus do
      association :datasource, factory: :datasource_from_telus
    end

    factory :cdr_from_rogers do
      association :datasource, factory: :datasource_from_rogers
    end

    factory :cdr_from_bell do
      association :datasource, factory: :datasource_from_bell
    end

    factory :cdr_from_drapper do
      association :datasource, factory: :datasource_from_drapper
    end

    factory :cdr_from_troop do
      association :datasource, factory: :datasource_from_troop
    end

    factory :cdr_from_obm do
      association :datasource, factory: :datasource_from_obm
    end
  end
end
