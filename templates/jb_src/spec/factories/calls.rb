FactoryBot.define do
  factory :call do
    btn { FFaker::PhoneNumber.phone_number }
    did { btn }
    called_number { FFaker::PhoneNumber.phone_number }
    call_date { ((Date.today - 3.month)..(Date.today)).to_a.sample }
    date { call_date.to_date.strftime "%b %d" }
    time { call_date.to_time.strftime "%H:%M:%S" }
    area { FFaker::AddressCA.city }
    province { FFaker::AddressCA.province }
    duration { rand * 3600 }
    cost { duration * 0.015 }
  end
end
