describe Building do

  it { should validate_presence_of :address1 }
  it { should validate_presence_of :city }
  it { should validate_presence_of :province }
  it { should validate_presence_of :country }

  it { should allow_value(FFaker::AddressCA.postal_code.gsub(Building::POSTAL_CODE_SANITIZER, '')).for(:postalcode) }
  it { should allow_value(FFaker::AddressUS.zip_code.gsub(Building::POSTAL_CODE_SANITIZER, '')).for(:postalcode) }
  it { should validate_length_of(:postalcode).is_at_least(5).is_at_most(9) }

  describe "#postal_code" do

    CANADIAN_POSTAL_CODE_FORMAT  =  /[A-Z]\d[A-Z]\s\d[A-Z]\d/
    AMERICAN_ZIP_CODE_FORMAT     =  /\d{5}(-\d{4}\d)?/

    subject { build(:building) }

    context "given a canadian postal code" do
      it "displays the postal code as a formatted string" do
        subject.postalcode = FFaker::AddressCA.postal_code.gsub(Building::POSTAL_CODE_SANITIZER, '')
        subject.valid?
        expect(subject.postal_code).to match CANADIAN_POSTAL_CODE_FORMAT
      end

      it "should format the postal code before saving" do
        subject.postalcode = FFaker::AddressCA.postal_code
        subject.save!
        expect(subject.postal_code).to match CANADIAN_POSTAL_CODE_FORMAT
      end

      it "does not allow invalid formats" do
        subject.postalcode = '123 123'
        subject.valid?
        expect(subject).not_to be_valid

        subject.postalcode = 'AAA AAA'
        subject.valid?
        expect(subject).not_to be_valid

        subject.postalcode = '0H0 H0H'
        subject.valid?
        expect(subject).not_to be_valid
      end
    end

    context "given an american zip code" do
      it "displays the zip code as a formatted string" do
        subject.postalcode = FFaker::AddressUS.zip_code.gsub(Building::POSTAL_CODE_SANITIZER, '')
        subject.valid?
        expect(subject.zip_code).to match AMERICAN_ZIP_CODE_FORMAT
      end

      it "should format the zip code before saving" do
        subject.postalcode = FFaker::AddressUS.zip_code
        subject.save!
        expect(subject.zip_code).to match AMERICAN_ZIP_CODE_FORMAT
      end

      it "does not allow invalid formats" do
        subject.postalcode = 'ABCDE'
        subject.valid?
        expect(subject).not_to be_valid

        subject.postalcode = 'ABCDE-ABCD'
        subject.valid?
        expect(subject).not_to be_valid

        subject.postalcode = '1234'
        subject.valid?
        expect(subject).not_to be_valid
      end
    end

    it "should save a building with a blank postal code" do
      building = create(:building, postalcode: '')
      expect(building.save).to be_truthy
    end

    it "should sanitize a postal code with spaces in it" do
      building = create(:building, postalcode: "h0H 0H0")
      building.sanitize_postalcode
      expect(building.postalcode).to eq("H0H0H0")

      expect(building.valid?).to be_truthy
    end
  end

  it { should have_many :sites }

  describe "#name" do
    subject { create(:building) }

    it "should be a String" do
      expect(subject.name).to be_a(String).or(be_nil)
    end

    it { should respond_to :name }
  end

  describe "#to_s" do
    subject { create(:building) }

    it "should be a String" do
      expect(subject.to_s).to be_a(String).or(be_nil)
    end

    it "should match either the name, address1 or address2" do
      expect(subject.to_s).to eq(subject.name).or(eq(subject.address1)).or(eq(subject.address2))
    end
  end

end
