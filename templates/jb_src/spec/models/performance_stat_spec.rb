require 'rails_helper'

RSpec.describe PerformanceStat, type: :model do
  it "should collect a time interval" do
    a = PerformanceStat.measure(name: "test one", login: "fred") do
      sleep(5)
    end
    a.work_count = 10
    expect(a.duration).to be_within(0.1).of(5)
    expect(a.rate).to     be_within(0.01).of(0.5)
  end
end
