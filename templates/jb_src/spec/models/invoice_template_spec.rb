require 'rails_helper'

RSpec.describe InvoiceTemplate, type: :model do
  describe "relationships" do
    it { should have_many(:btns) }
  end

  describe "billing" do
    it "should not show support invoices" do
      expect(InvoiceTemplate.all.regular_billable).to_not include(invoice_templates(:support))
    end
  end
end
