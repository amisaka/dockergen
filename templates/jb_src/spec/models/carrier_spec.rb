# coding: utf-8
require 'rails_helper'

RSpec.describe Carrier, :type => :model do

  describe "relations" do
    it "should have one or more rate plans" do
      expect(carriers(:Thinktel).rate_plans.count).to be >= 0
    end
  end

end
