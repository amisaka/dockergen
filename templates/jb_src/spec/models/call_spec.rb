describe Call do
  #
  # def datafake
  #   Datasource.create(:datasource_filename=>'fake1')
  # end
  #
  # describe "relations" do
  #   it "should have a relation to cdr table" do
  #     call1 = create(:call)
  #     assert_equal 3, call1.cdrs.count
  #   end
  #
  #   it "should have a relation to client table" do
  #     call1 = create(:call)
  #     expect( call1.client ).to_not be_nil
  #   end
  #
  #   it "should be able to merge with self" do
  #     call1 = Call.create
  #     ncall = call1.merge_with(call1)
  #     expect(ncall).to eq(ncall)
  #   end
  #
  #   it "should be able to merge with another call" do
  #     call1 = Call.create
  #     call1.cdrs << Cdr.create
  #     call1.cdrs << Cdr.create
  #     expect(call1.cdrs.count).to eq( 2)
  #
  #     call2 = Call.create
  #     call2.cdrs << Cdr.create
  #     expect(call2.cdrs.count).to eq( 1)
  #
  #     ncall = call1.merge_with(call2)
  #     expect(ncall).to eq( call1)
  #   end
  #
  #   it "should be able to be merged with another call" do
  #     call1 = Call.create
  #     call1.cdrs << Cdr.create
  #     call1.cdrs << Cdr.create
  #     expect(call1.cdrs.count).to eq( 2)
  #
  #     call2 = Call.create
  #     call2.cdrs << Cdr.create
  #     expect(call2.cdrs.count).to eq( 1)
  #
  #     # assumes id are allocate in increasing order
  #     ncall = call2.merge_with(call1)
  #     expect(ncall).to eq( call1)
  #   end
  #
  #   it "should be okay to merge with nil" do
  #     call1 = Call.create
  #     call1.cdrs << Cdr.create
  #     call1.cdrs << Cdr.create
  #     expect(call1.cdrs.count).to eq( 2)
  #
  #     # assumes id are allocate in increasing order
  #     ncall = call1.merge_with(nil)
  #     expect(ncall).to eq( call1)
  #   end
  #
  #   it "should be okay to merge with unsaved call" do
  #     call1 = Call.create
  #     call1.cdrs << Cdr.create
  #     call1.cdrs << Cdr.create
  #     expect(call1.cdrs.count).to eq( 2)
  #
  #     call2 = Call.new
  #     call2.cdrs << Cdr.new
  #
  #     # assumes id are allocate in increasing order
  #     ncall = call1.merge_with(call2)
  #     expect(ncall).to eq( call1)
  #   end
  # end
  #
  # describe "create" do
  #   it "should find an existing call by master_callid" do
  #     cl1 = create(:call)
  #     cl2 = Call.find_or_make(cl1.master_callid, datafake)
  #     expect(cl1).to eq(cl2)
  #   end
  #
  #   it "should create a one" do
  #     cl1 = create(:call)
  #     cl2 = Call.find_or_make("newone", datafake)
  #     expect(cl1).to_not eq(cl2)
  #   end
  #
  #   it "should find a new one" do
  #     cl1 = Call.find_or_make("newone", datafake)
  #     expect( cl1 ).to_not be_nil
  #     cl2 = Call.find_or_make("secondone", datafake)
  #     expect(cl1).to_not eq(cl2)
  #   end
  # end
  #
  # describe "imports" do
  #   it "should create a call entry whenever a CDR entry is created" do
  #     c1 = Cdr.import_row_test($CdrRow1, datafake, 1)
  #     expect( c1.call ).to_not be_nil
  #     c2 = Cdr.import_row_test($CdrRow2, datafake, 2)
  #     expect( c2.call ).to_not be_nil
  #   end
  #
  #   it "should create the same call entry whenever related CDR are imported" do
  #     fakeDataSource = datafake;
  #     c1 = Cdr.import_row_test($CdrRow1, fakeDataSource, 1)
  #     c2 = Cdr.import_row_test($CdrRow2, fakeDataSource, 2)
  #     assert_equal c1.datasource_id, c2.datasource_id
  #     assert_equal c1.call, c2.call
  #   end
  #
  #   it "should create the same call entry when related CDR are imported in opposite order" do
  #     fakeDataSource = datafake;
  #     c2 = Cdr.import_row_test($CdrRow2, fakeDataSource, 1)
  #     c1 = Cdr.import_row_test($CdrRow1, fakeDataSource, 2)
  #     assert_equal c1.call, c2.call
  #   end
  #
  #   # XXX first, these data is not well enough sanitized.
  #   #     second, mcr suspects that id=4719337 (cdr107) is a copy
  #   #             and paste duplicate of 106.
  #   #     recommend getting another Transfer Consult example.
  #   it "should create a call entry for a Transfer Type = Transfer Consult" do
  #     c1 = cdrviewer_cdr(:cdr106)
  #     c1.associate_to_call_save!
  #     #c2 = cdrviewer_cdr(:cdr107)
  #     #c2.associate_to_call_save!
  #     c4 = cdrviewer_cdr(:cdr109)
  #     c4.associate_to_call_save!
  #     assert_equal c1.call, c4.call
  #   end
  #
  # end
  #
  # describe "derived data" do
  #   it "should have a valid starttime" do
  #     c1 = cdrviewer_cdr(:cdr106)
  #     c1.associate_to_call_save!
  #     c2 = cdrviewer_cdr(:cdr107)
  #     c2.associate_to_call_save!
  #     c4 = cdrviewer_cdr(:cdr109)
  #     c4.associate_to_call_save!
  #     call = c4.call
  #     call.fixdates!
  #     expect( call ).to_not be_nil
  #     assert c1.startdatetime >= call.startdatetime
  #     assert c2.startdatetime >= call.startdatetime
  #     assert c4.startdatetime >= call.startdatetime
  #   end
  #
  #   it "should have a valid answertime" do
  #     c1 = cdrviewer_cdr(:cdr106)
  #     c1.associate_to_call_save!
  #     c2 = cdrviewer_cdr(:cdr107)
  #     c2.associate_to_call_save!
  #     c4 = cdrviewer_cdr(:cdr109)
  #     c4.associate_to_call_save!
  #     call = c4.call
  #     call.fixdates!
  #     expect( call ).to_not be_nil
  #     assert c1.answerdatetime >= call.answerdatetime
  #     assert c2.answerdatetime >= call.answerdatetime
  #     assert c4.answerdatetime >= call.answerdatetime
  #   end
  #
  #   it "should have a valid releasetime" do
  #     c1 = cdrviewer_cdr(:cdr106)
  #     c1.associate_to_call_save!
  #     c2 = cdrviewer_cdr(:cdr107)
  #     c2.associate_to_call_save!
  #     c4 = cdrviewer_cdr(:cdr109)
  #     c4.associate_to_call_save!
  #     call = c4.call
  #     call.fixdates!
  #     expect( call ).to_not be_nil
  #     assert c1.releasedatetime <= call.releasedatetime
  #     assert c2.releasedatetime <= call.releasedatetime
  #     assert c4.releasedatetime <= call.releasedatetime
  #   end
  #
  #   it "should have a valid call from" do
  #     c1 = cdrviewer_cdr(:cdr106)
  #     c1.associate_to_call_save!
  #     c2 = cdrviewer_cdr(:cdr107)
  #     c2.associate_to_call_save!
  #     c4 = cdrviewer_cdr(:cdr109)
  #     c4.associate_to_call_save!
  #     call = c4.call
  #     call.fixdates!
  #     expect( call ).to_not be_nil
  #     expect( call.our_number ).to_not be_nil
  #   end
  #
  #   it "should have a valid call to" do
  #     c1 = cdrviewer_cdr(:cdr106)
  #     c1.associate_to_call_save!
  #     c2 = cdrviewer_cdr(:cdr107)
  #     c2.associate_to_call_save!
  #     c4 = cdrviewer_cdr(:cdr109)
  #     c4.associate_to_call_save!
  #     call = c4.call
  #     call.fixdates!
  #     expect( call ).to_not be_nil
  #     expect( call.their_number ).to_not be_nil
  #   end
  #
  #   it "should have a valid duration" do
  #     c1 = cdrviewer_cdr(:cdr106)
  #     c1.associate_to_call_save!
  #     c2 = cdrviewer_cdr(:cdr107)
  #     c2.associate_to_call_save!
  #     c4 = cdrviewer_cdr(:cdr109)
  #     c4.associate_to_call_save!
  #     call = c4.call
  #     call.fixdates!
  #     expect( call ).to_not be_nil
  #     expect( call.duration  ).to_not be_nil
  #     assert_equal 154, call.duration
  #   end
  #
  #   it "should associate these seven records in some way" do
  #     #Call.call_merge_debug = true
  #     #Cdr.debugnow = true
  #     l1 = cdrviewer_cdr(:seven_leg1)
  #     expect(l1.datasource).to_not be_nil
  #     l1.reload_associate_to_call_save!
  #     expect(l1.call).to_not be_nil
  #
  #     l1b = cdrviewer_cdr(:seven_leg1b)
  #     l1b.reload_associate_to_call_save!
  #     expect(l1b.call).to eq( l1.call)
  #
  #     l2 = cdrviewer_cdr(:seven_leg2)
  #     l2.reload_associate_to_call_save!
  #     expect(l2.call).to eq( l1.call)
  #
  #     l2b = cdrviewer_cdr(:seven_leg2b)
  #     expect(l2b.datasource).to_not be_nil
  #     l2b.reload_associate_to_call_save!
  #     expect(l2b.call).to eq( l1.call)
  #
  #     l3 = cdrviewer_cdr(:seven_leg3)
  #     l3.reload_associate_to_call_save!
  #     expect(l3.call).to eq( l1.call)
  #
  #     l3b = cdrviewer_cdr(:seven_leg3b)
  #     expect(l3b.datasource).to_not be_nil
  #     l3b.reload_associate_to_call_save!
  #     expect(l3b.call).to eq( l1.call)
  #
  #     l4 = cdrviewer_cdr(:seven_leg4)
  #     expect(l4.datasource).to_not be_nil
  #     l4.reload_associate_to_call_save!
  #     expect(l4.call).to eq( l1.call)
  #
  #     l5 = cdrviewer_cdr(:seven_leg5)
  #     expect(l5.datasource).to_not be_nil
  #     l5.reload_associate_to_call_save!
  #
  #     l6 = cdrviewer_cdr(:seven_leg6)
  #     expect(l6.datasource).to_not be_nil
  #     l6.reload_associate_to_call_save!
  #     expect(l6.call).to eq( l1.call)
  #
  #     l7 = cdrviewer_cdr(:seven_leg7)
  #     expect(l7.datasource).to_not be_nil
  #     l7.reload_associate_to_call_save!
  #     expect(l7.call).to eq( l1.call)
  #
  #     # this will not be discovered until l7 is processed.
  #     l5.reload
  #     expect(l5.call).to eq( l1.call)
  #   end
  #
  #
  #   it "should handle legs with nil release datetime" do
  #     opts1 = { file: "spec/files/nilReleasedDateTime.csv",
  #               overwrite: true,
  #               verbose:   false}
  #     number,durations,firstdata,datasource = Cdr.load_bw_file(opts1)
  #     expect(number).to eq(1)
  #   end
  #
  #   it "should associate these legs into one call.  See t10428 and parent t10413" do
  #       # First pull up the calls involved
  #       call1 = create(:call)
  #       call2 = create(:call)
  #       call3 = create(:call)
  #
  #       # Make sure we pulled up something
  #       expect(call1).to_not be_nil
  #       expect(call2).to_not be_nil
  #       expect(call3).to_not be_nil
  #
  #       # Now get a set of all the CDRs
  #       allTheLegs = Set.new
  #       allTheLegs = allTheLegs.merge(call1.cdrs).merge(call2.cdrs).merge(call3.cdrs).flatten;
  #
  #       # Make sure we have 7 as expected
  #       expect(allTheLegs.count).to eq(7)
  #
  #       # Pull up all the datasources in a set
  #       allTheDs = Set.new
  #       allTheLegs.each { |cdr| allTheDs.add(cdr.datasource) }
  #
  #       # Make sure we have two as expected
  #       expect(allTheDs.count).to eq(2)
  #
  #       # Reset their analysis level to 2
  #       allTheDs.each { |ds| ds.analysis_level = 2; ds.save }
  #
  #       # We now have recreated the state we desire for figuring out ticket 10428
  #
  #       # Call the method as the rake task would
  #       # I don't know if this interferes with other tests, I'm assuming it does not
  #       #debugger
  #       Datasource.update_call_leg_association('201511', 3)
  #
  #       # Now test if the call id is the same for all those legs
  #       i=1
  #       allTheLegs.each { |cdr|
  #           cdr.reload
  #           cdr.call.reload
  #           expect(cdr.call).not_to be_nil, "call of cdr #{cdr.id} is nil (interation #{i})"
  #           expect(cdr.call.id).to eq(allTheLegs.first.call.id), "cdr #{cdr.id} does not have the same call id (#{cdr.call.id}) as cdr #{allTheLegs.first.id} (#{allTheLegs.first.call.id}) (iteration #{i})"
  #           i += 1
  #       }
  #   end
  #
  #   it "should associate these many records for user-busy into three calls" do
  #     l01 = cdrviewer_cdr(:user_leg01)   # 873337387:2A
  #     l02 = cdrviewer_cdr(:user_leg02)   # 873337387:2
  #     l03 = cdrviewer_cdr(:user_leg03)   # 873346589:1
  #     l04 = cdrviewer_cdr(:user_leg04)   # 873341217:2A
  #     l05 = cdrviewer_cdr(:user_leg05)   # 873341217:1A
  #     l06 = cdrviewer_cdr(:user_leg06)   # 873337387:1A
  #     l07 = cdrviewer_cdr(:user_leg07)   # 873337387:1
  #     l08 = cdrviewer_cdr(:user_leg08)   # 873346589:0
  #     l09 = cdrviewer_cdr(:user_leg09)   # 873335409:1A
  #     l10 = cdrviewer_cdr(:user_leg10)   # 873335409:1
  #     l11 = cdrviewer_cdr(:user_leg11)   # 873337387:0
  #     l12 = cdrviewer_cdr(:user_leg12)   # 873341217:2
  #     l13 = cdrviewer_cdr(:user_leg13)   # 873341217:1
  #
  #     #Call.call_merge_debug = true
  #     #Cdr.debugnow = true
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #     l05.reload_associate_to_call_save!
  #     l06.reload_associate_to_call_save!
  #     l07.reload_associate_to_call_save!
  #     l08.reload_associate_to_call_save!
  #     l09.reload_associate_to_call_save!
  #     l10.reload_associate_to_call_save!
  #     l11.reload_associate_to_call_save!
  #     l12.reload_associate_to_call_save!
  #     l13.reload_associate_to_call_save!
  #
  #     # this is call 1: leg10, leg09 and leg11
  #     assert_equal l10.call, l09.call
  #     assert_equal l10.call, l11.call
  #
  #     # this is call 2: leg13, leg05, leg07, leg06, leg08
  #     assert_equal l13.call, l05.call
  #     assert_equal l13.call, l07.call
  #     assert_equal l13.call, l06.call
  #     assert_equal l13.call, l08.call
  #
  #     # this is call 3: leg12, leg04, leg02, leg03
  #     assert_equal l12.call, l04.call
  #     assert_equal l12.call, l02.call
  #     assert_equal l12.call, l01.call
  #     assert_equal l12.call, l03.call
  #   end
  #
  #   it "should associate a call-forward-always" do
  #     l01 = cdrviewer_cdr(:always_forward_leg1)
  #     l02 = cdrviewer_cdr(:always_forward_leg2)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #
  #     call1 = l01.call
  #     call1.fixdates!
  #     l01.reload
  #     l02.reload
  #     assert_equal l01.call, l02.call
  #   end
  #
  #   it "should associate a call-centre" do
  #     l01 = cdrviewer_cdr(:call_centre_leg1)
  #     l02 = cdrviewer_cdr(:call_centre_leg2)
  #     expect(l01.datasource).to_not be_nil
  #     expect(l02.datasource).to_not be_nil
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #
  #     call1 = l01.call
  #     call1.fixdates!
  #     l01.reload
  #     l02.reload
  #     assert_equal l01.call, l02.call
  #   end
  #
  #
  #   it "should associate multiple legs of the same local/remote id in two different files, but not more than 2 hours apart" do
  #       l01 = cdrviewer_cdr(:oneCallMultipleLegs_leg1)
  #       l02 = cdrviewer_cdr(:oneCallMultipleLegs_leg2)
  #       l03 = cdrviewer_cdr(:oneCallMultipleLegs_leg3)
  #       l04 = cdrviewer_cdr(:oneCallMultipleLegs_leg4)
  #       l05 = cdrviewer_cdr(:oneCallMultipleLegs_leg5)
  #
  #       #Legs 6 to 9 could be ditched from this test and fixtures
  #       l06 = cdrviewer_cdr(:oneCallMultipleLegs_leg6)
  #       l07 = cdrviewer_cdr(:oneCallMultipleLegs_leg7)
  #       l08 = cdrviewer_cdr(:oneCallMultipleLegs_leg8)
  #       l09 = cdrviewer_cdr(:oneCallMultipleLegs_leg9)
  #
  #       expect(l01.datasource).to_not be_nil
  #       expect(l02.datasource).to_not be_nil
  #       expect(l03.datasource).to_not be_nil
  #       expect(l04.datasource).to_not be_nil
  #       expect(l05.datasource).to_not be_nil
  #       expect(l06.datasource).to_not be_nil
  #       expect(l07.datasource).to_not be_nil
  #       expect(l08.datasource).to_not be_nil
  #       expect(l09.datasource).to_not be_nil
  #
  #       l01.reload_associate_to_call_save!
  #       l02.reload_associate_to_call_save!
  #       l03.reload_associate_to_call_save!
  #       l04.reload_associate_to_call_save!
  #       l05.reload_associate_to_call_save!
  #       l06.reload_associate_to_call_save!
  #       l07.reload_associate_to_call_save!
  #       l08.reload_associate_to_call_save!
  #       l09.reload_associate_to_call_save!
  #
  #       #puts l01.id.to_s + " " + l01.call_id.to_s + " "  + l01.datasource.id.to_s
  #       #puts l02.id.to_s + " " + l02.call_id.to_s + " "  + l02.datasource.id.to_s
  #       #puts l03.id.to_s + " " + l03.call_id.to_s + " "  + l03.datasource.id.to_s
  #       #puts l04.id.to_s + " " + l04.call_id.to_s + " "  + l04.datasource.id.to_s
  #       #puts l05.id.to_s + " " + l05.call_id.to_s + " "  + l05.datasource.id.to_s
  #       #puts l06.id.to_s + " " + l06.call_id.to_s + " "  + l06.datasource.id.to_s
  #       #puts l07.id.to_s + " " + l07.call_id.to_s + " "  + l07.datasource.id.to_s
  #       #puts l08.id.to_s + " " + l08.call_id.to_s + " "  + l08.datasource.id.to_s
  #       #puts l09.id.to_s + " " + l09.call_id.to_s + " "  + l09.datasource.id.to_s
  #
  #       set1 = [l02, l03, l04]
  #
  #       strFormat = "%10s\t %10s\t %15s\t %15s\t %25s\t %25s\n"
  #
  #       if not $NOLOG_TESTING
  #         printf strFormat, "CDR ID", "Call ID", "Local ID", "Remote ID", "Related call reason", "Start date time"
  #       end
  #       [l01, l02, l03, l04, l05, l06, l07, l08, l09].each { |cdr|
  #           cdr.reload
  #           cdr.call.reload
  #           if not $NOLOG_TESTING
  #             printf strFormat, cdr.id, cdr.call.id, cdr.localcallid, cdr.remotecallid, cdr.relatedcallidreason, cdr.startdatetime
  #           end
  #       }
  #
  #       # The call id of this set should be the same
  #       assert_equal l03.call_id, l02.call_id
  #       assert_equal l03.call_id, l04.call_id
  #
  #       l01.reload; l05.reload
  #       set1.each { |l|
  #         l.reload;
  #         if not $NOLOG_TESTING
  #           printf "%d should not be equal to %d and %d\n", l.call_id, l01.call_id, l05.call_id
  #         end
  #         expect(l.call_id).not_to eq(l01.call_id), "Leg #{l.id} call_id #{l.call_id} is equal to l01.call_id #{l01.call_id} when it should not be"
  #         expect(l.call_id).not_to eq(l05.call_id), "Leg #{l.id} call_id #{l.call_id} is equal to l05.call_id #{l05.call_id} when it should not be"
  #       }
  #  end
  #
  #   it "should deal with associating no-answer and associate by remotecallid" do
  #     l01 = cdrviewer_cdr(:noanswer_leg1)
  #     l02 = cdrviewer_cdr(:noanswer_leg2)
  #     l03 = cdrviewer_cdr(:noanswer_leg3)
  #     l04 = cdrviewer_cdr(:noanswer_leg4)
  #     expect(l01.datasource).to_not be_nil
  #     expect(l02.datasource).to_not be_nil
  #     expect(l03.datasource).to_not be_nil
  #     expect(l04.datasource).to_not be_nil
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #
  #     #File.open("/home/mcr/galaxy/tmp/noanswer.csv", "w") do | file| l01.dump_with_friends(file, [l02,l03,l04]) end
  #     l01.reload
  #     l02.reload
  #     l03.reload
  #     l04.reload
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #     assert_equal l01.call_id, l04.call_id
  #   end
  #
  #   it "can associate legs when the second leg has transfer_type as Deflection and they are associated by (second leg) and remotecallid (first leg)" do
  #     l01  = cdrviewer_cdr(:deflection_leg1)
  #     l02  = cdrviewer_cdr(:deflection_leg2)
  #     l03  = cdrviewer_cdr(:deflection_leg3)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #
  #     #File.open("/home/mcr/galaxy/tmp/deflection.csv", "w") do | file| c1.dump_with_friends(file, [c2]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #   end
  #
  #   it "should deal with internal call forwarding" do
  #     l01  = cdrviewer_cdr(:internal_forward_leg1)
  #     l02  = cdrviewer_cdr(:internal_forward_leg2)
  #     l03  = cdrviewer_cdr(:internal_forward_leg3)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/forwarding.csv", "w") do | file| c1.dump_with_friends(file, [c2]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #   end
  #
  #   it "can associate legs when the second leg has transfer_type as Transfer Consult and they are associated by the first leg via remotecallid" do
  #     l01  = cdrviewer_cdr(:transfer_consult_leg1)
  #     l02  = cdrviewer_cdr(:transfer_consult_leg2)
  #     l03  = cdrviewer_cdr(:transfer_consult_leg3)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #
  #     l01.reload; l02.reload; l03.reload;
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/transfer_consult.csv", "w") do | file| l01.dump_with_friends(file, [l02, l03]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #   end
  #
  #   it "can associate legs when the second leg has redirectingreason as deflection and they are associated by remotecallid to localcallid (deflection1)" do
  #     l01  = cdrviewer_cdr(:deflection1_leg1)
  #     l02  = cdrviewer_cdr(:deflection1_leg2)
  #     l03  = cdrviewer_cdr(:deflection1_leg3)
  #     l04  = cdrviewer_cdr(:deflection1_leg4)
  #     expect(l01.datasource).to_not be_nil
  #     expect(l02.datasource).to_not be_nil
  #     expect(l03.datasource).to_not be_nil
  #     expect(l04.datasource).to_not be_nil
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #
  #     l01.reload; l02.reload; l03.reload; l04.reload
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/deflection1.csv", "w") do | file| l01.dump_with_friends(file, [l02,l03,l04]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #     assert_equal l01.call_id, l04.call_id
  #   end
  #
  #   it "can associate legs when the second leg has redirectingreason as deflection and they are associated by remotecallid (deflection2)" do
  #     l01  = cdrviewer_cdr(:deflection2_leg1)
  #     l02  = cdrviewer_cdr(:deflection2_leg2)
  #     l03  = cdrviewer_cdr(:deflection2_leg3)
  #     l04  = cdrviewer_cdr(:deflection2_leg4)
  #     expect(l01.datasource).to_not be_nil
  #     expect(l02.datasource).to_not be_nil
  #     expect(l03.datasource).to_not be_nil
  #     expect(l04.datasource).to_not be_nil
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #
  #     l01.reload; l02.reload; l03.reload; l04.reload
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/deflection2.csv", "w") do | file| l01.dump_with_friends(file, [l02]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #     assert_equal l01.call_id, l04.call_id
  #   end
  #
  #   it "can associate legs when the second leg has redirectingreason as deflection and they are associated by localcallid (second leg) and remotecallid (first leg) (deflection3)" do
  #     l01  = cdrviewer_cdr(:deflection3_leg1)
  #     l02  = cdrviewer_cdr(:deflection3_leg2)
  #     l03  = cdrviewer_cdr(:deflection3_leg3)
  #     l04  = cdrviewer_cdr(:deflection3_leg4)
  #     l05  = cdrviewer_cdr(:deflection3_leg5)
  #     expect(l01.datasource).to_not be_nil
  #     expect(l02.datasource).to_not be_nil
  #     expect(l03.datasource).to_not be_nil
  #     expect(l04.datasource).to_not be_nil
  #     expect(l05.datasource).to_not be_nil
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #     l05.reload_associate_to_call_save!
  #
  #     l01.reload; l02.reload; l03.reload; l04.reload; l05.reload
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/deflection3.csv", "w") do | file| l01.dump_with_friends(file, [l02, l03, l04, l05]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #     assert_equal l01.call_id, l04.call_id
  #     assert_equal l01.call_id, l05.call_id
  #   end
  #
  #   it "can associate legs when the second leg has redirectingreason as deflection and they are associated by remotecallid (second leg) and localcallid (first leg) (deflection4)" do
  #     l01  = cdrviewer_cdr(:deflection4_leg1)
  #     l02  = cdrviewer_cdr(:deflection4_leg2)
  #     l03  = cdrviewer_cdr(:deflection4_leg3)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #
  #     l01.reload; l02.reload; l03.reload;
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/deflection4.csv", "w") do | file| l01.dump_with_friends(file, [l02, l03]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #   end
  #
  #   it "should associate similar relatedcallid when there are no redirect, related explanation (related1)" do
  #     l01  = cdrviewer_cdr(:related1_leg1)
  #     l02  = cdrviewer_cdr(:related1_leg2)
  #     l03  = cdrviewer_cdr(:related1_leg3)
  #     l04  = cdrviewer_cdr(:related1_leg4)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #
  #     l01.reload; l02.reload; l03.reload; l04.reload
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/related1.csv", "w") do | file| l01.dump_with_friends(file, [l02, l03]) end
  #     assert_equal l01.call_id, l02.call_id
  #     assert_equal l01.call_id, l03.call_id
  #     assert_equal l01.call_id, l04.call_id
  #   end
  #
  #   it "should load a call leg for no-answer that does not have a relatedcallid" do
  #     l01  = cdrviewer_cdr(:unrelated_noanswer)
  #
  #     l01.reload_associate_to_call_save!
  #
  #     l01.reload;
  #     #File.open("/home/mcr/galaxy/nv/clientportaltest/data/related1.csv", "w") do | file| l01.dump_with_friends(file, [l02, l03]) end
  #     expect( l01.call ).to_not be_nil
  #   end
  #
  #   it "can determine redundant legs" do
  #     l01  = cdrviewer_cdr(:nvexample2_leg1)
  #     l02  = cdrviewer_cdr(:nvexample2_leg2)
  #     l03  = cdrviewer_cdr(:nvexample2_leg3)
  #     l04  = cdrviewer_cdr(:nvexample2_leg4)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #
  #     call_id = l01.call_id
  #     call = Call.find(id = call_id)
  #     non_redundant_legs = call.non_redundant_legs
  #
  #     assert_equal 3, non_redundant_legs.count
  #   end
  #
  #   it "should find leg1 of abandoned call 1 as handsetrings" do
  #     l01  = cdrviewer_cdr(:abandonedcall01_leg01)
  #     l02  = cdrviewer_cdr(:abandonedcall01_leg02)
  #     l03  = cdrviewer_cdr(:abandonedcall01_leg03)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #
  #     call = l01.call
  #     handsetrings = call.cdrs.handsetrings
  #     expect(handsetrings.count).to eq(1)
  #     expect(handsetrings.first).to eq(l02)
  #   end
  #
  #   it "should find leg2 of abandoned call 1 as startingleg" do
  #     l01  = cdrviewer_cdr(:abandonedcall01_leg01)
  #     l02  = cdrviewer_cdr(:abandonedcall01_leg02)
  #     l03  = cdrviewer_cdr(:abandonedcall01_leg03)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #
  #     call = l01.call
  #     startinglegs = call.cdrs.startinglegs
  #     expect(startinglegs.count).to eq(1)
  #     expect(startinglegs.first).to eq(l01)
  #   end
  #
  #   it "should associate three legs of abandoned call 1 as an abandoned call" do
  #     l01  = cdrviewer_cdr(:abandonedcall01_leg01)
  #     l02  = cdrviewer_cdr(:abandonedcall01_leg02)
  #     l03  = cdrviewer_cdr(:abandonedcall01_leg03)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #
  #     # XXX this call is 21s long
  #     call = l01.call
  #     expect(call.cdrs.count).to eq(3)
  #     expect(call).to be_abandoned2
  #   end
  #
  #   it "should associate three legs of abandoned call 2 as an abandoned call" do
  #     l01  = cdrviewer_cdr(:abandonedcall02_leg01)
  #     l02  = cdrviewer_cdr(:abandonedcall02_leg02)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #
  #     call = l01.call
  #     expect(call.cdrs.count).to eq(2)
  #     expect(call).to be_abandoned2
  #   end
  #
  #   it "should count a call as abandoned if too short and caller hung up" do
  #     l01  = cdrviewer_cdr(:nvexample3_leg1)
  #
  #     l01.reload_associate_to_call_save!
  #     call = l01.call
  #
  #     # this test succeeds because the single leg is not identified as
  #     # being a handset ring.  Not sure what this means yet.
  #     expect(call).to be_abandoned
  #   end
  #
  #   it "should associate five legs of voice mail call 3 as not an abandoned call" do
  #     l01  = cdrviewer_cdr(:abandonedcall03_leg01)
  #     l02  = cdrviewer_cdr(:abandonedcall03_leg02)
  #     l03  = cdrviewer_cdr(:abandonedcall03_leg03)
  #     l04  = cdrviewer_cdr(:abandonedcall03_leg04)
  #     l05  = cdrviewer_cdr(:abandonedcall03_leg05)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #     l05.reload_associate_to_call_save!
  #
  #     call = l05.call
  #     call.reload
  #     #call.calculate_abandoned!
  #     #call.calculate_voicemail!
  #     expect(call.cdrs.count).to eq(5)
  #     expect(call).to_not be_abandoned
  #     expect(call).to be_transfertovoicemail
  #   end
  #
  #   it "should associate five legs of voice mail call 4 as not an abandoned call" do
  #     l01  = cdrviewer_cdr(:abandonedcall04_leg01)
  #     l02  = cdrviewer_cdr(:abandonedcall04_leg02)
  #     l03  = cdrviewer_cdr(:abandonedcall04_leg03)
  #     l04  = cdrviewer_cdr(:abandonedcall04_leg04)
  #     l05  = cdrviewer_cdr(:abandonedcall04_leg05)
  #
  #     l01.reload_associate_to_call_save!
  #     l02.reload_associate_to_call_save!
  #     l03.reload_associate_to_call_save!
  #     l04.reload_associate_to_call_save!
  #     l05.reload_associate_to_call_save!
  #
  #     call = l05.call
  #     call.reload
  #     #call.calculate_abandoned!
  #     #call.calculate_voicemail!
  #     expect(call.cdrs.count).to eq(5)
  #     expect(call).to_not be_abandoned
  #     expect(call).to be_transfertovoicemail
  #   end
  #
  #
  # end
  #
  # describe "to a small office" do
  #   it "should have three CDRs with appropriate connections to calledphones" do
  #     # this is just to validate some ways in which data sometimes rots
  #     cdr1 = cdrviewer_cdr(:cdr8940)
  #     expect(cdr1.calledphone).to_not be_nil
  #   end
  #
  #   it "should mark a call with extension dialed count towards the did and extension" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be_truthy
  #     p2549 = phones(:did5555555121)
  #     x201  = phones('5555555121x201')
  #     expect(p2549).to_not be_nil      # make sure fixtures exist.
  #     expect(x201).to_not  be_nil
  #
  #     expect(c1.involved_phones.count).to eq(3)   # external calling number
  #     expect(c1.involved_phones).to include(x201)
  #     expect(c1.involved_phones).to include(p2549)
  #
  #     expect(c1.incoming_did?(p2549)).to be true
  #
  #     expect(c1.huntgroup?).to           be false
  #     expect(c1.no_extension_picked_up?).to be false
  #     expect(c1.abandoned?(p2549)).to    be false
  #     expect(c1.abandoned?(x201)).to     be false
  #     expect(c1.answered?(p2549)).to     be true
  #     expect(c1.answered?(x201)).to      be true
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :incoming
  #     expect(cat.keys.length).to         eq(2)
  #     expect(cat['AnsweredCallStat']).to eq(p2549)
  #     expect(cat['DidCallstat']).to      eq(x201)
  #
	#   #This may not be required, added due to merge conflict --jlam
  #     expect(cat.keys.length).to eq(2)
  #   end
  #
  #   it "should mark a direct call to a handset with a DID" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be_truthy
  #     p4381  = phones('5555558338')
  #     expect(p4381).to_not be_nil      # make sure fixtures exist.
  #
  #     # external calling number, plus handset
  #     expect(c1.involved_phones.count).to eq(2)
  #     expect(c1.involved_phones).to include(p4381)
  #
  #     expect(c1.incoming_did?(p4381)).to be true
  #
  #     expect(c1.huntgroup?).to           be false
  #     expect(c1.answered?(p4381)).to     be true
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :incoming
  #     expect(cat.keys.length).to         eq(2)
  #     expect(cat['AnsweredCallStat']).to eq(p4381)
  #     expect(cat['DidCallstat']).to      eq(p4381)
  #   end
  #
  #   it "should mark a call to pick up voice mail as an internal call" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be_truthy
  #     x101  = phones(:t10429_5555552649x101)
  #     x900  = phones('5555555121x900')
  #     expect(x101).to_not  be_nil      # make sure fixtures exist.
  #     expect(x900).to_not  be_nil
  #
  #     expect(c1.involved_phones.count).to eq(2)
  #     expect(c1.involved_phones).to include(x900)
  #     expect(c1.involved_phones).to include(x101)
  #
  #     expect(c1.no_extension_picked_up?).to be false
  #     expect(c1.abandoned?(x101)).to     be false
  #     expect(c1.abandoned?(x900)).to     be false
  #     expect(c1.answered?(x101)).to      be false
  #     expect(c1.answered?(x900)).to      be true
  #     expect(c1.oneLegToVoicemail?).to   be true
  #     expect(c1.transfertovoicemail?).to be false  # user *CALLED* VM.
  #     expect(c1.collected_voicemail?).to  be true
  #     expect(c1.internal_call?).to       be true
  #
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :vmcollected
  #     expect(cat['VMcollectedCallstat']).to eq(x101)
  #     expect(cat.keys.length).to eq(1)
  #   end
  #
  #   it "should mark a call to the hunt-group, which was answered by x101" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be true
  #     x101  = phones(:t10429_5555552649x101)
  #     x102  = phones('5555555121x102')
  #     x104  = phones(:t10429_5555554381x104)
  #     x105  = phones(:t10429_5555554381x105)
  #     x900  = phones('5555555121x900') # voice mail portal
  #     x950  = phones('5555553223')     # the incoming DID itself.
  #     expect(x101).to_not  be_nil      # make sure fixtures exist.
  #     expect(x102).to_not  be_nil      # make sure fixtures exist.
  #     expect(x104).to_not  be_nil      # make sure fixtures exist.
  #     expect(x105).to_not  be_nil      # make sure fixtures exist.
  #     expect(x900).to_not  be_nil
  #     expect(x950).to_not  be_nil
  #
  #     # four handsets, voice mail, plus incoming call
  #     expect(c1.involved_phones.count).to eq(6)
  #     expect(c1.involved_phones).to_not include(x900)
  #     expect(c1.involved_phones).to include(x950)
  #     expect(c1.involved_phones).to include(x101)
  #     expect(c1.involved_phones).to include(x102)
  #     expect(c1.involved_phones).to include(x104)
  #     expect(c1.involved_phones).to include(x105)
  #
  #     expect(c1.no_extension_picked_up?).to be false
  #     expect(c1.abandoned?(x101)).to     be false
  #     expect(c1.abandoned?(x102)).to     be false
  #     expect(c1.abandoned?(x104)).to     be false
  #     expect(c1.abandoned?(x105)).to     be false
  #     expect(c1.abandoned?(x950)).to     be false
  #
  #     expect(c1.incoming_did?(x950)).to  be true
  #
  #     expect(c1.huntgroup?).to           be true
  #     expect(c1.answered?(x102)).to      be false
  #     expect(c1.answered?(x104)).to      be false
  #     expect(c1.answered?(x105)).to      be false
  #     expect(c1.answered?(x101)).to      be true    # was answered by handset
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :huntgroupanswered
  #     expect(cat['HuntGroupCallstat']).to eq(x101)
  #     expect(cat['AnsweredCallStat']).to      eq(x950)
  #     expect(cat.keys.length).to eq(2)
  #   end
  #
  #   it "should mark an outgoing call as such on x101" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be true
  #
  #     x101  = phones(:t10429_5555552649x101)
  #     x102  = phones('5555555121x102')
  #     x104  = phones(:t10429_5555554381x104)
  #     x105  = phones(:t10429_5555554381x105)
  #     x900  = phones('5555555121x900')
  #     x950  = phones('5555553223')     # extension 950
  #
  #     # originating handset, plus destination number
  #     expect(c1.involved_phones.count).to eq(2)
  #
  #     expect(c1.involved_phones).to_not include(x900)
  #     expect(c1.involved_phones).to_not include(x950)
  #     expect(c1.involved_phones).to include(x101)
  #     expect(c1.involved_phones).to_not include(x102)
  #     expect(c1.involved_phones).to_not include(x104)
  #     expect(c1.involved_phones).to_not include(x105)
  #
  #     # this is true, because it was outgoing!
  #     expect(c1.no_extension_picked_up?).to be true
  #
  #     expect(c1.incoming_did?(x950)).to  be false
  #
  #     expect(c1.abandoned?(x101)).to     be false
  #     expect(c1.abandoned?(x102)).to     be false
  #     expect(c1.abandoned?(x104)).to     be false
  #     expect(c1.abandoned?(x105)).to     be false
  #     expect(c1.abandoned?(x950)).to     be false
  #
  #     expect(c1.outgoingTheirNumber?(x101)).to be true
  #     expect(c1.answered?(x102)).to      be false
  #     expect(c1.answered?(x104)).to      be false
  #     expect(c1.answered?(x105)).to      be false
  #     expect(c1.answered?(x101)).to      be false
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :outgoing
  #     expect(cat['OutgoingCallStat']).to eq(x101)
  #     expect(cat.keys.length).to eq(1)
  #   end
  #
  #   it "should mark an outgoing call, unanswered, as such on x102" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be false
  #
  #     x102  = phones('5555555121x102')
  #
  #     # originating handset, plus destination number
  #     expect(c1.involved_phones.count).to eq(2)
  #
  #     expect(c1.involved_phones).to include(x102)
  #
  #     # this is true, because it was outgoing!
  #     expect(c1.no_extension_picked_up?).to be true
  #
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :outgoing
  #     expect(cat['OutgoingCallStat']).to         eq(x102)
  #     expect(cat['OutgoingNoAnswerCallstat']).to eq(x102)
  #     expect(cat.keys.length).to eq(2)
  #   end
  #
  #   # this case is rather screwed up: it is an outgoing call claimed to be from
  #   # phone: "3241" to this number.
  #   it "should mark an outgoing call 69499197 as such on x102" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be true
  #
  #     did2649  = phones(:did5555555121)
  #
  #     # originating handset, plus destination number
  #     expect(c1.involved_phones.count).to eq(2)
  #     expect(c1.involved_phones).to include(did2649)
  #
  #     # this is true, because it was outgoing!
  #     expect(c1.no_extension_picked_up?).to be true
  #
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :outgoing
  #     expect(cat['InternalCallstat']).to include(did2649)
  #     expect(cat.keys.length).to eq(1)
  #   end
  #
  #   it "should mark an incoming call to the hunt-group, which goes to VM" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be true
  #
  #     x101  = phones(:t10429_5555552649x101)
  #     x102  = phones('5555555121x102')
  #     x104  = phones(:t10429_5555554381x104)
  #     x105  = phones(:t10429_5555554381x105)
  #     x900  = phones('5555555121x900')
  #     x950  = phones('5555553223')     # incoming DID.
  #
  #     # four handsets, voice mail, DID, plus caller
  #     expect(c1.involved_phones.count).to eq(7)
  #     expect(x101.voicemail_number?).to   be true
  #
  #     expect(c1.involved_phones).to include(x900)
  #     expect(c1.involved_phones).to include(x950)
  #     expect(c1.involved_phones).to include(x101)
  #     expect(c1.involved_phones).to include(x102)
  #     expect(c1.involved_phones).to include(x104)
  #     expect(c1.involved_phones).to include(x105)
  #
  #     expect(c1.no_extension_picked_up?).to be true
  #
  #     expect(c1.abandoned?(x101)).to     be false
  #     expect(c1.abandoned?(x102)).to     be false
  #     expect(c1.abandoned?(x104)).to     be false
  #     expect(c1.abandoned?(x105)).to     be false
  #     expect(c1.abandoned?(x950)).to     be false
  #
  #     expect(c1.incoming_did?(x950)).to  be true
  #
  #     expect(c1.huntgroup?).to           be true
  #     expect(c1.outgoingTheirNumber?(x101)).to be false
  #     expect(c1.answered?(x102)).to      be false
  #     expect(c1.answered?(x104)).to      be false
  #     expect(c1.answered?(x105)).to      be false
  #     expect(c1.answered?(x101)).to      be false
  #     expect(c1.oneLegToVoicemail?).to   be true
  #     expect(c1.transfertovoicemail?).to be true
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     # this exercises calc_category case 4
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :huntgroupvm
  #     expect(cat.keys.length).to eq(2)
  #     expect(cat['VoiceMailCallstat']).to eq(x101)
  #     expect(cat['AnsweredCallStat']).to  eq(x950)
  #   end
  #
  #   it "should mark a second incoming call to the hunt-group, which goes to VM" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be true
  #
  #     x101  = phones(:t10429_5555552649x101)
  #     x102  = phones('5555555121x102')
  #     x104  = phones(:t10429_5555554381x104)
  #     x105  = phones(:t10429_5555554381x105)
  #     x900  = phones('5555555121x900')
  #     xDID  = phones('did5555552649')     # incoming DID (phone id 39702)
  #
  #     # four handsets, voice mail, DID, plus caller
  #     expect(c1.involved_phones.count).to eq(7)
  #
  #     expect(c1.involved_phones).to include(x900)
  #     expect(c1.involved_phones).to include(xDID)
  #     expect(c1.involved_phones).to include(x101)
  #     expect(c1.involved_phones).to include(x102)
  #     expect(c1.involved_phones).to include(x104)
  #     expect(c1.involved_phones).to include(x105)
  #
  #     expect(c1.no_extension_picked_up?).to be true
  #
  #     expect(c1.abandoned?(x101)).to     be false
  #     expect(c1.abandoned?(x102)).to     be false
  #     expect(c1.abandoned?(x104)).to     be false
  #     expect(c1.abandoned?(x105)).to     be false
  #     expect(c1.abandoned?(xDID)).to     be false
  #
  #     expect(c1.incoming_did?(xDID)).to  be true
  #
  #     expect(c1.outgoingTheirNumber?(x101)).to be false
  #     expect(c1.answered?(x102)).to      be false
  #     expect(c1.answered?(x104)).to      be false
  #     expect(c1.answered?(x105)).to      be false
  #     expect(c1.answered?(x101)).to      be false
  #     expect(c1.oneLegToVoicemail?).to   be true
  #     expect(c1.transfertovoicemail?).to be true
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     # this exercises calc_category case 4
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :huntgroupvm
  #     expect(cat.keys.length).to eq(2)
  #     expect(cat['VoiceMailCallstat']).to eq(x101)
  #     expect(cat['AnsweredCallStat']).to  eq(xDID)
  #   end
  #
  #   it "should mark an incoming call that was answered" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be true
  #
  #     x101  = phones(:t10429_5555552649x101)
  #     x102  = phones('5555555121x102')
  #     x104  = phones(:t10429_5555554381x104)
  #     x105  = phones(:t10429_5555554381x105)
  #     x900  = phones('5555555121x900')
  #     #xDID  = phones('did5555555121')     # incoming DID.
  #     xDID  = phones('did5555552649')     # incoming DID (phone id 39702)
  #
  #     # four handsets, DID, plus caller
  #     expect(c1.involved_phones.count).to eq(6)
  #
  #     expect(c1.involved_phones).to include(xDID)
  #     expect(c1.involved_phones).to include(x101)
  #     expect(c1.involved_phones).to include(x102)
  #     expect(c1.involved_phones).to include(x104)
  #     expect(c1.involved_phones).to include(x105)
  #
  #     expect(c1.no_extension_picked_up?).to be false
  #
  #     expect(c1.abandoned?(x101)).to     be false
  #     expect(c1.abandoned?(x102)).to     be false
  #     expect(c1.abandoned?(x104)).to     be false
  #     expect(c1.abandoned?(x105)).to     be false
  #     expect(c1.abandoned?(xDID)).to     be false
  #
  #     expect(c1.incoming_did?(xDID)).to  be true
  #
  #     expect(c1.outgoingTheirNumber?(x101)).to be false
  #     expect(c1.answered?(x102)).to      be false
  #     expect(c1.answered?(x104)).to      be false
  #     expect(c1.answered?(x105)).to      be false
  #     expect(c1.answered?(x101)).to      be true
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     # this exercises calc_category case 2
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :huntgroupanswered
  #     expect(cat.keys.length).to eq(2)
  #     expect(cat['HuntGroupCallstat']).to eq(x101)
  #     expect(cat['AnsweredCallStat']).to  eq(xDID)
  #   end
  #
  #   it "should mark a second incoming call that was answered by x101" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be true
  #
  #     x101  = phones(:t10429_5555552649x101)
  #     x102  = phones('5555555121x102')
  #     x104  = phones(:t10429_5555554381x104)
  #     x105  = phones(:t10429_5555554381x105)
  #     x900  = phones('5555555121x900')
  #     xDID  = phones('5555553223')     # incoming DID.
  #
  #     # four handsets, DID, plus caller
  #     expect(c1.involved_phones.count).to eq(6)
  #
  #     expect(c1.involved_phones).to include(xDID)
  #     expect(c1.involved_phones).to include(x101)
  #     expect(c1.involved_phones).to include(x102)
  #     expect(c1.involved_phones).to include(x104)
  #     expect(c1.involved_phones).to include(x105)
  #
  #     expect(c1.no_extension_picked_up?).to be false
  #
  #     expect(c1.incoming_did?(xDID)).to  be true
  #     expect(c1.abandoned?(x101)).to     be false
  #     expect(c1.abandoned?(x102)).to     be false
  #     expect(c1.abandoned?(x104)).to     be false
  #     expect(c1.abandoned?(x105)).to     be false
  #     expect(c1.abandoned?(xDID)).to     be false
  #
  #     expect(c1.outgoingTheirNumber?(x101)).to be false
  #     expect(c1.answered?(x102)).to      be false
  #     expect(c1.answered?(x104)).to      be false
  #     expect(c1.answered?(x105)).to      be false
  #     expect(c1.answered?(x101)).to      be true
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     # this exercises calc_category case 2
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :huntgroupanswered
  #     expect(cat.keys.length).to eq(2)
  #     expect(cat['HuntGroupCallstat']).to eq(x101)
  #     expect(cat['AnsweredCallStat']).to  eq(xDID)
  #   end
  #
  #   it "should mark an abandoned call to a hunt group" do
  #     c1 = create(:call)
  #     c1.calc_all!
  #
  #     expect(c1.answered?).to be false
  #
  #     x101  = phones(:t10429_5555552649x101)
  #     x102  = phones('5555555121x102')
  #     x104  = phones(:t10429_5555554381x104)
  #     x105  = phones(:t10429_5555554381x105)
  #     x900  = phones('5555555121x900')
  #     #xDID  = phones('did5555555121')     # incoming DID.
  #     xDID  = phones('did5555552649')     # incoming DID (phone id 39702)
  #
  #     # four handsets, DID, plus caller
  #     expect(c1.involved_phones.count).to eq(6)
  #
  #     expect(c1.involved_phones).to include(xDID)
  #     expect(c1.involved_phones).to include(x101)
  #     expect(c1.involved_phones).to include(x102)
  #     expect(c1.involved_phones).to include(x104)
  #     expect(c1.involved_phones).to include(x105)
  #
  #     expect(c1.no_extension_picked_up?).to be true
  #
  #     expect(c1.incoming_did?(xDID)).to  be true
  #     expect(c1.abandoned?(x101)).to     be true
  #     expect(c1.abandoned?(x102)).to     be true
  #     expect(c1.abandoned?(x104)).to     be true
  #     expect(c1.abandoned?(x105)).to     be true
  #     expect(c1.abandoned?(xDID)).to     be true
  #
  #     expect(c1.outgoingTheirNumber?(x101)).to be false
  #     expect(c1.answered?(x102)).to      be false
  #     expect(c1.answered?(x104)).to      be false
  #     expect(c1.answered?(x105)).to      be false
  #     expect(c1.answered?(x101)).to      be false
  #     expect(c1.oneLegToVoicemail?).to   be false
  #     expect(c1.transfertovoicemail?).to be false
  #     expect(c1.collected_voicemail?).to be false
  #     expect(c1.internal_call?).to       be false
  #
  #     # this exercises calc_category case 2
  #     cat,catype = c1.calc_category
  #     expect(catype).to                  be :huntgroupabandoned
  #     expect(cat.keys.length).to eq(2)
  #     expect(cat['AbandonedCallstat']).to eq(x101)
  #     expect(cat['AnsweredCallStat']).to  eq(xDID)
  #   end
  #
  #   it "should do a count of calls incoming at 9am, x101 having 1 outgoing call" do
  #     notify = false
  #     day = Time.gm(2015, 12, 1)
  #     hour = 9                    # 19UTC = 15:00 EDT, May is EDT.
  #     sp = StatsPeriod.find_or_create_with_hour(day,hour)
  #
  #     c776 = create(:client)
  #
  #     c776.calc_daily_stats(day, hour, notify)
  #
  #     p101 = phones(:t10429_5555552649x101)
  #     expect(p101.outgoing_call_count(day, hour, false, true, notify)).to eq(1)
  #   end
  #
  # end
  #
  # describe "voicemail" do
  #   it "should be found it the call is CallForwardNoAnswer to VM box" do
  #     nv2 = create(:call)
  #     nv2.calculate_abandoned!
  #     nv2.calculate_voicemail!
  #     assert nv2.transfertovoicemail?
  #   end
  #
  #   it "should be counted by the scope transfertovoicemail" do
  #     nv2 = create(:call)
  #
  #     # create a scope of just this call.
  #     expect(Call.where(:id => nv2.id).went_to_voicemail_calc.count).to eq(1)
  #   end
  #
  #   it "should cache went to voicemail value" do
  #     nv = create(:client)
  #     nv_call = create(:call)
  #
  #     expect(nv_call.transfertovoicemail).to be_nil
  #     nv_call.calculate_voicemail!
  #     expect(nv_call.transfertovoicemail).to be_truthy
  #   end
  #
  #   it "should count zero extension to voicemail for 2013-01-30" do
  #     day = Time.gm(2013,1,30)
  #     hour = 19  # in EST.
  #
  #     nv = create(:client)
  #     nv_call = create(:call)
  #     nv_call.calculate_voicemail!
  #     nv_call.save!
  #     phone1 = phones(:nv5143527747x3215)
  #     expect(phone1).to_not be_nil
  #
  #     # in fact, the nv_call we had should be part of nvexample2
  #     # which goes to voice mail.
  #     expect(phone1.incoming_voicemail_count(day, hour)).to eq(0)
  #     expect(phone1.incoming_abandoned_count(day, hour)).to eq(0)
  #   end
  #
  #   it "should count per extension (internal) to voicemail for 2012-10-05" do
  #     nv = create(:client)
  #
  #     nv_call = create(:call)
  #     sp = StatsPeriod.for_hour(nv_call.startdatetime.to_datetime.in_time_zone("EST5EDT"))
  #     nv_call.calc_all!
  #     b = nv_call.update_stats(sp)
  #
  #     phone1 = phones(:nv5143527747x3215)
  #     expect(phone1).to_not be_nil
  #
  #     expect(nv_call).to_not be_abandoned_call
  #
  #     notify = false
  #
  #     expect(phone1.incoming_call_count(sp.day, sp.hour, true)).to eq(2)
  #     expect(phone1.internal_call_count(sp.day, sp.hour, false, true, notify)).to       eq(1)
  #     expect(phone1.incoming_voicemail_count(sp.day, sp.hour, false, true, notify)).to  eq(1)
  #     expect(phone1.incoming_abandoned_count(sp.day, sp.hour, false, true, notify)).to  eq(0)
  #   end
  #
  #   it "should count zero extensions abandoned for 2013-01-30" do
  #     nv = create(:client)
  #     nv_call = create(:call)
  #     sp = StatsPeriod.for_hour(nv_call.startdatetime.to_datetime.in_time_zone("EST5EDT"))
  #
  #     nv_call.calc_all!
  #     nv_call.update_stats(sp)
  #     phone1 = phones(:nv5143527747x3215)
  #     expect(phone1).to_not be_nil
  #
  #     # in fact, the nv_call we had should be part of nvexample2
  #     # which goes to voice mail.
  #     expect(phone1.incoming_voicemail_count(sp.day, sp.hour)).to eq(1)
  #     expect(phone1.incoming_abandoned_count(sp.day, sp.hour)).to eq(0)
  #   end
  #
  #   it "should count per extension abandoned for 2012-10-06, but now internal" do
  #     nv = create(:client)
  #
  #     nv_leg  = cdrviewer_cdr(:nvexample3_leg1)
  #     nv_leg.reload_associate_to_call_save!
  #     nv_call = nv_leg.call
  #     sp = StatsPeriod.for_hour(nv_call.startdatetime.to_datetime.in_time_zone("EST5EDT"))
  #     nv_call.calc_all!
  #     b = nv_call.update_stats(sp)
  #     phone1 = nv_leg.calledphone
  #     expect(phone1).to_not be_nil
  #
  #     # this example no longer shows as abandoned, because it is now counted
  #     # as internal.
  #     expect(phone1.incoming_call_count(sp.day, sp.hour)).to eq(1)
  #     expect(phone1.incoming_voicemail_count(sp.day, sp.hour)).to eq(0)
  #     expect(phone1.internal_call_count(sp.day, sp.hour)).to eq(1)
  #   end
  #
  #   it "should not be indicated for a regular call" do
  #     ordinary = create(:call)
  #     assert !ordinary.transfertovoicemail?
  #   end
  # end
end
