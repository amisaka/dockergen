require 'rails_helper'

RSpec.describe Contact, type: :model do
  it { should belong_to :client }

  # it { should validate_presence_of :name }
  # it { should validate_presence_of :email }
  # it { should validate_presence_of :role }
  # it { should validate_presence_of :phone_number }

  describe "phone_number" do
    it { should allow_value('555-555-5555').for(:phone_number) }
    it { should allow_value('613-693-0684').for(:phone_number) }
    it { should_not allow_value('My Phone Number').for(:phone_number) }
  end
end
