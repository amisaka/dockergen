module Hakka
  if $CONNECTMSSQL
    describe ChargeAssignment, type: :model do
      subject { Hakka::ChargeAssignment.offset(rand(Hakka::ChargeAssignment.count)).first }

      it { should belong_to :customer }
      it { should belong_to :department }
      it { should belong_to :customer }

      describe "#active" do
        it "is an alias of :Active" do
          expect(subject.active).to eq(subject.Active)
        end
      end

      describe "#active?" do
        it "is an alias of :Active" do
          expect(subject.active?).to eq(subject.Active)
        end
      end

      describe ".active" do
        it "returns active charge assignments" do
          expect(ChargeAssignment.active).to match_array(ChargeAssignment.where(:Active => 1))
        end
      end

      describe ".inactive" do
        it "returns inactive charge assignments" do
          expect(ChargeAssignment.inactive).to match_array(ChargeAssignment.where(:Active => 0))
        end
      end
    end

  end
end
