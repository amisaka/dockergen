module Hakka
  if $CONNECTMSSQL
    describe Department, type: :model do

      it { should belong_to :customer }
      it { should have_many :charge_assignments }
      it { should have_many :products }

      describe "#name" do
        it "has a name" do
          expect(subject.name).to be_a_kind_of(String).or(be_nil)
        end

        it "is an alias of #DepartmentName" do
          expect(subject.name).to match(subject.DepartmentName)
        end
      end

      describe "#active_charges" do
        subject { Hakka::Department.find(420) }

        it { should have_many :active_charges }

        it "ensures charges are active" do
          subject.active_charges.each do |charge|
            expect(charge).to be_active
          end
        end
      end

      describe "#inactive_charges" do
        subject { Hakka::Department.find(420) }

        it { should have_many :inactive_charges }

        it "ensures charges are inactive" do
          subject.inactive_charges.each do |charge|
            expect(charge).not_to be_active
          end
        end
      end

      describe "#active_products" do
        subject { Hakka::Department.find(420) }

        it "ensures products are active" do
          skip
        end
      end

      describe "#inactive_products" do
        subject { Hakka::Department.find(420) }

        it "ensures products are inactive" do
          skip
        end
      end

    end

    describe "#active_charges" do
      subject { Hakka::Department.find(420) }

      it { should have_many :active_charges }

      it "ensures charges are active" do
        subject.active_charges.each do |charge|
          expect(charge).to be_active
        end
      end
    end

    describe "#inactive_charges" do
      subject { Hakka::Department.find(420) }

      it { should have_many :inactive_charges }

      it "ensures charges are inactive" do
        subject.inactive_charges.each do |charge|
          expect(charge).not_to be_active
        end
      end
    end

    describe "#active_products" do
      subject { Hakka::Department.find(420) }

      it "ensures products are active" do
        skip
      end
    end

    describe "#inactive_products" do
      subject { Hakka::Department.find(420) }

      it "ensures products are inactive" do
        skip
      end
    end

  end
end
