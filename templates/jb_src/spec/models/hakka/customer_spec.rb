module Hakka
  if $CONNECTMSSQL
    describe Customer, type: :model do
      subject { Hakka::Customer.offset(rand(Hakka::Customer.count)).first }

      it { should have_one :client }
      it { should have_many :charge_assignments }
      it { should have_many :departments }
      it { should have_many :products }

      describe "#active_charges" do
        subject { Hakka::Customer.find(310) }

        it { should have_many :active_charges }

        it "ensures records are active" do
          subject.active_charges.each do |charge|
            expect(charge).to be_active
          end
        end
      end

      describe "#inactive_charges" do
        subject { Hakka::Customer.find(310) }

        it { should have_many :inactive_charges }

        it "ensures records are inactive" do
          subject.inactive_charges.each do |charge|
            expect(charge).not_to be_active
          end
        end
      end

      describe "#name" do
        it "has a name" do
          expect(subject.name).to be_a_kind_of(String).or(be_nil)
        end

        it "is an alias of #CustomerName" do
          expect(subject.name).to match(subject.CustomerName)
        end
      end

      describe "#make_client_hash" do
        it "provides a valid client hash" do
          expect(Client.new(subject.make_client_hash)).to be_valid
        end
      end

      describe "#map_by_foreign_pid" do
        it "is an array or nil" do
          expect(subject.map_by_foreign_pid).to be_a_kind_of(Array).or(be_nil)
        end
      end

      describe "#map_by_btn" do
        it "is an array or nil" do
          expect(subject.map_by_btn).to be_a_kind_of(Array).or(be_nil)
        end
      end

      describe "#map_by_did" do
        it "is an array or nil" do
          expect(subject.map_by_did).to be_a_kind_of(Array).or(be_nil)
        end
      end

      describe "#simplecode" do
        it "removes non numeric characters" do
          expect(subject.simplecode).to match(/[0-9]/)
        end
      end

      describe "#find_associated_client" do
        skip
      end

    end
  end
end
