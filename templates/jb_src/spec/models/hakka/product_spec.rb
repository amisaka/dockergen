module Hakka
  if $CONNECTMSSQL
    describe Product, type: :model do
      it { should have_many :customers }
      it { should have_many :departments }
      it { should have_many :charge_assignments }
      it { should have_one :service }
    end
  end
end
