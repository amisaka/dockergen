describe ClientLineOfBusiness do
  it { should belong_to :client }
  it { should belong_to :line_of_business }
end
