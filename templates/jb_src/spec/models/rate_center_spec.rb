require 'rails_helper'

describe RateCenter do

  describe "relations" do
    it "should have connection to north_american_exchanges" do
      rc1 = rate_centers(:rc13035)
      expect(rc1.local_exchanges.count).to          be >= 0
      expect(rc1.north_american_exchanges.count).to be >= 0
      expect(rc1.cdrs.count).to be >= 0
    end

    it "should have a connection to local_exchanges_north_american_exchanges" do
      rc1 = rate_centers(:rc13035)
      expect(rc1.local_exchanges_north_american_exchanges.count).to be >= 0
    end

    it "should have associated with it some rates" do
      rc1 = rate_centers(:rc1)
      expect(rc1.rates.count).to be >= 1
    end
  end

  describe "active" do
    it "should be active if not deleted" do
      rc5 = rate_centers(:rc5)
      expect(rc5).to be_active
    end

    it "should be inactive if marked deleted" do
      rc999 = rate_centers(:rc999)
      expect(rc999).to be_inactive
    end

    it "should be active if not deleted" do
      rc999 = rate_centers(:rc999)
      expect(RateCenter.active.where(id: rc999.id).take).to be_nil
    end
  end

  describe "footprint" do
    it "should add a new item, and connect it to an existing rate center" do
      # Get aqguarnish item
      aq = rate_centers(:rc13041)

      aq.find_matching_nae


      le = LocalExchange.create(carrier: 'TestTel',
                                carrierplan_id: 2,
                                name: 'FORT MACLEOD',
                                province: 'AB')
      le.connect_to_rate_center
      expect(le.rate_center).to_not be_nil

      #le.rate_center.try(:connect_to_nae)
      #le.connect_to_na
    end

    it "should find a rate center by localname" do
      stats = Hash.new(0)
      le = LocalExchange.create(carrier: 'TestTel',
                                carrierplan_id: 2,
                                name: 'NICENAME',
                                province: 'ON')

      rc = RateCenter.find_by_localname_permuted(le.name, le.province, stats)
      expect(rc).to_not be_nil
    end

    it "should find a rate center by localname with space" do
      stats = Hash.new(0)
      le = LocalExchange.create(carrier: 'TestTel',
                                carrierplan_id: 2,
                                name: 'NICERNAME',
                                province: 'ON')

      rc = RateCenter.find_by_localname_permuted(le.name, le.province, stats)
      expect(rc).to_not be_nil
    end

    it "should find a rate center by localname with accent" do
      stats = Hash.new(0)
      le = LocalExchange.create(carrier: 'TestTel',
                                carrierplan_id: 2,
                                name: "NICE' NAME",
                                province: 'ON')

      rc = RateCenter.find_by_localname_permuted(le.name, le.province, stats)
      expect(rc).to_not be_nil
    end

    it "should find a rate center by lerg6name" do
      stats = Hash.new(0)
      le = LocalExchange.create(carrier: 'TestTel',
                                carrierplan_id: 2,
                                name: 'GODERICH',
                                province: 'ON')

      rc = RateCenter.find_by_localname_permuted(le.name, le.province, stats)
      expect(rc).to_not be_nil
    end

    it "should add a local exchange, connecting to rate center by second name" do
      # Get aqguarnish item
      aq = rate_centers(:rc255)
      le = local_exchanges(:le857)

      le.connect_to_rate_center

      expect(le.rate_center).to_not be_nil

      #le.rate_center.try(:connect_to_nae)
      #le.connect_to_na
    end
  end

  describe "acceptable numbers" do
    it "should take a non-local rate center and indicate if it is in the right country" do
      plan = rate_plans(:north_american_silver)
      taledo = rate_centers(:rc8788)

      expect(plan.acceptable_rate_center(taledo)).to be_truthy
    end

    it "should take a carrier and indicate that the carrier serves that exchange" do
      expect(rate_centers(:rc79).includes_exchange?(carriers(:Thinktel))).to be_truthy
    end

    it "should take a carrier and indicate that the carrier does not serve that exchange" do
      # note that le581 includes a different carrier
      expect(rate_centers(:rc581).includes_exchange?(carriers(:Thinktel))).to be_falsey
    end
  end

  describe "canonify" do
    it "should canonify a phone number" do
      expect(RateCenter.canonify_number("750-596-8666")).to eq("+17505968666")
    end

    it "should add a +1 to a ten digit number" do
      expect(RateCenter.canonify_number('5142021234')).to eq("+15142021234")
    end

    it "should add a +1 to 2nd ten digit number" do
      expect(RateCenter.canonify_number('5199274044')).to eq("+15199274044")
    end

    it "should add a + to a eleven digit number" do
      expect(RateCenter.canonify_number('15142021234')).to eq("+15142021234")
    end

    it "should do nothing if preceeded by +" do
      expect(RateCenter.canonify_number('+15142021234')).to eq("+15142021234")
    end

    it "should canonify 1800 number" do
      expect(RateCenter.canonify_number('18009991234')).to eq("+18009991234")
    end
  end

  describe "lookup" do
    it "should take a 1514444 number and make a pass through NorthAmericanExchange" do
      rc = RateCenter.lookup_by_partial('+15144444044')
      expect(rc).to_not       be_nil
      expect(rc.lerg6name).to eq('MONTREAL')
    end

    it "should take a 1519927 number and make a pass through NorthAmericanExchange" do
      rc = RateCenter.lookup_by_partial('15199274044')
      expect(rc).to_not       be_nil
      expect(rc.lerg6name).to eq('CALEDON')
    end

    it "should take a UK number, with + and return a UK rate center" do
      rc = RateCenter.lookup_by_partial('+442081234567')
      expect(rc).to_not       be_nil
      expect(rc.isocountry).to eq('GB')
    end

    it "should take an UK number, with a 011 and return a UK rate center" do
      rc = RateCenter.lookup_by_partial('011442081234567')
      expect(rc).to_not       be_nil
      expect(rc.isocountry).to eq('GB')
    end
  end
end
