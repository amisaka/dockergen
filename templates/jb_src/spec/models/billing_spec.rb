describe Billing, type: :model do
  subject { create(:billing) }

  describe ".payment_methods" do
    it { expect(Billing).to respond_to(:payment_methods) }

    it "delegates to Payment" do
      expect(Billing.payment_methods).to eq(Payment.payment_methods)
    end
  end

  describe ".payment_types" do
    it { expect(Billing).to respond_to(:payment_types) }

    it "delegates to Payment" do
      expect(Billing.payment_types).to eq(Payment.payment_types)
    end
  end

  it { should delegate_method(:payment_type).to(:payment) }
  it { should respond_to(:payment_method) }

  it { should delegate_method(:current_balance).to(:client) }
  it { should respond_to(:balance) }

  it { should delegate_method(:started_at).to(:client) }
  it { should respond_to(:start_date) }

  it { should delegate_method(:charges_start).to(:invoice) }
  it { should respond_to(:billing_start_date) }

  it { should delegate_method(:terminated_at).to(:client) }

  describe "#persisted?" do
    it "returns true" do
      expect(subject.persisted?).to be true
    end
  end

  describe "#update" do
    before do
      @client = create(:client_with_invoices)
      @invoice = @client.most_recent_invoice
      @payment = create(:payment, invoice: @invoice)
      @billing = @client.billing
      @new_billing_attributes = attributes_for(:billing)
    end

    it "assigns attributes to billing" do
      @billing.update(@new_billing_attributes)
      @billing.reload

      expect(@billing.start_date).to eq(@new_billing_attributes[:start_date])
      expect(@billing.billing_start_date).to eq(@new_billing_attributes[:billing_start_date])
      expect(@billing.last_usage_date).to eq(@new_billing_attributes[:last_usage_date])
      expect(@billing.last_statement_date).to eq(@new_billing_attributes[:last_statement_date])
      expect(@billing.last_statement_amount).to eq(@new_billing_attributes[:last_statement_amount])
      expect(@billing.statement_type).to eq(@new_billing_attributes[:statement_type])
      expect(@billing.payment_method).to eq(@new_billing_attributes[:payment_method])
    end
  end

  describe "#save" do

  end


end
