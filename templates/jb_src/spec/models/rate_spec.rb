require 'rails_helper'

RSpec.describe Rate, type: :model do

  describe "relationships" do
    it "should have an associated rate_center" do
      rate = rates(:rate1)
      expect(rate.rate_center).to be_truthy
    end
    it "should have an associated rate plan" do
      rate = rates(:rate1)
      expect(rate.rate_plan).to   be_truthy
    end
  end

  describe "importing" do
    it "should import a heading line, ignoring it" do
      row = ['Destination Country Code *',
             'Destination City Code *',
             'Usage Group','Rate Type',
             'Cost','Per Usage','Rate Code','Volume Scheme','Rounding Scheme',
             'Minimum Charge Per Usage Record']
      rate_plan = rate_plans(:world_silver)
      stats = Hash.new(0)
      expect(Rate.import_row(row, rate_plan, stats)).to be_nil
      expect(stats[:header_lines]).to eq(1)
      expect(stats[:country_file]).to be_truthy
    end

    it "should import a heading line for NA rates, ignoring it" do
      row = ['Destination Area Code ( NPA ) *','Destination Exchange ( NXX ) *','Destination Block','Usage Group','Rate Type','Cost','Per Usage','Rate Code','Volume Scheme','Rounding Scheme','Minimum Charge Per Usage Record']
      rate_plan = rate_plans(:north_american_silver)
      stats = Hash.new(0)
      expect(Rate.import_row(row, rate_plan, stats)).to be_nil
      expect(stats[:header_lines]).to eq(1)
      expect(stats[:country_file]).to be_falsey
      expect(stats[:north_american_file]).to be_truthy
    end

    it "should import a single line as a rate" do
      row1 = ['7','','International','Simple','0.387360','60','','','30/6','']
      row2 = ['20','','International','Simple','0.409920','60','','','30/6','']

      stats = Hash.new(0)
      stats[:country_file] = true

      r1 = Rate.import_row(row1, rate_plans(:world_silver), stats)
      expect(r1).to_not be_nil

      r2 = Rate.import_row(row2, rate_plans(:world_silver), stats)
      expect(r2).to_not be_nil
      expect(r2.billing_increment).to   eq(6)
      expect(r2.minimum_call_length).to eq(30)
      expect(r2.cost).to be_within(0.000005).of(0.409920)
      expect(stats[:lines]).to eq(2)
    end

    it "should import a single line as a north american rate" do
      row1 = ['514','352','','USA','Simple','0.182130','60','','','30/6','']

      stats = Hash.new(0)
      stats[:country_file] = false
      stats[:north_american_file] = true

      r1 = Rate.import_row(row1, rate_plans(:north_american_silver), stats)
      expect(r1).to_not be_nil
      expect(r1.billing_increment).to   eq(6)
      expect(r1.minimum_call_length).to eq(30)
      expect(r1.cost).to be_within(0.000005).of(0.182130)
      expect(stats[:lines]).to          eq(1)

      # the rate is in a fixture, so it is not new.
      expect(stats[:rate_created]).to   eq(0)
      expect(stats[:rate_updated]).to   eq(1)
    end

    it "should import a file line as a list of international rates" do
      file = Rails.root.join("spec","files","VoIP-Country-Bronze-sample.csv")

      stats = Hash.new(0)
      before_count = rate_plans(:world_bronze).rates.count
      Rate.import_file(file, rate_plans(:world_bronze), stats)
      after_count = rate_plans(:world_bronze).rates.count
      expect(after_count - before_count).to eq(38)

      expect(stats[:lines]).to eq(39)
      expect(stats[:country_file]).to be_truthy
      expect(stats[:header_lines]).to eq(1)
    end

    it "should import a file line as a list of north american rates" do
      file = Rails.root.join("spec","files","VoIP-NPANXX-Silver-sample.csv")

      stats = Hash.new(0)
      before_count = rate_plans(:world_bronze).rates.count
      Rate.import_file(file, rate_plans(:world_bronze), stats)
      after_count = rate_plans(:world_bronze).rates.count
      expect(after_count - before_count).to eq(2)

      expect(stats[:lines]).to eq(4)
      expect(stats[:rate_center_not_found]).to eq(0)
      expect(stats[:north_american_file]).to be_truthy
      expect(stats[:header_lines]).to eq(1)
    end
  end

  describe "pricing" do
    it "should assign a price for call to Taledo" do
      cdr1 = cdrviewer_cdr(:calltotaledo1)
      type = cdr1.detailed_toll_calculation
      expect(type).to eq('to')
      expect(cdr1.called_rate_center).to_not be_nil
      expect(cdr1.price).to_not be_nil
      expect(cdr1.price).to be_within(0.01).of(1.83)
    end

    it "should calculate a price based upon a duration" do
      plan     = rate_plans(:north_american_silver)
      toledo_r = rates(:rate_silver_taledo)
      duration = 180  # 3 minutes exactly.

      price = plan.plan_discount(toledo_r, duration)
      expect(price).to_not be_nil
      expect(price).to be_within(0.01).of(0.09)
    end

    it "should round 0 down according minimums" do
      rate     = rates(:rate_silver_taledo)
      duration = 0
      expect(rate.duration_round(duration)).to eq(0)
    end

    it "should round 1 up according minimums" do
      rate     = rates(:rate_silver_taledo)
      duration = 1
      expect(rate.duration_round(duration)).to eq(30)
    end

    it "should round 27 up according minimums" do
      rate     = rates(:rate_silver_taledo)
      duration = 27
      expect(rate.duration_round(duration)).to eq(30)
    end

    it "should round 31 to 36" do
      rate     = rates(:rate_silver_taledo)
      duration = 31
      expect(rate.duration_round(duration)).to eq(36)
    end

    it "should round up on 35s call" do
      rate     = rates(:rate_silver_taledo)
      duration = 35
      expect(rate.duration_round(duration)).to eq(36)
    end

    it "should round 36 to 36 call" do
      rate     = rates(:rate_silver_taledo)
      duration = 36
      expect(rate.duration_round(duration)).to eq(36)
    end

    it "should round up to 1:48" do
      rate     = rates(:rate_silver_taledo)
      duration = 103
      expect(rate.duration_round(duration)).to eq(1*60 + 48)
    end

    it "should round up to 1:19:06" do
      rate     = rates(:rate_silver_taledo)
      duration = 4744
      expect(rate.duration_round(duration)).to eq(1*3600 + 19*60 + 6)
    end

    it "should round according minimums" do
      rate     = rates(:rate_silver_taledo)
      duration = 184
      expect(rate.duration_round(duration)).to eq(186)
    end
  end

end
