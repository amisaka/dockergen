require 'rails_helper'

describe CrmAccount, type: :model do

  describe "#client" do
    it { should have_one :client }
  end

  describe "#name" do
    it { should validate_length_of(:name).is_at_least(0).is_at_most(255) }
    it { should_not allow_value(nil).for :name }
    it { should_not allow_value('').for :name }
  end

  describe "relations" do
    it "should have a single client" do
      ac1 = crm_accounts(:novavision)
      expect(ac1.client).to_not be_nil
    end
    it "should have an attached crm_user (salesperson)" do
      ac1 = crm_accounts(:novavision)
      expect(ac1.crm_user).to_not be_nil
      expect(ac1.crm_user.user_name).to eq('sfoisy')
    end
  end

end
