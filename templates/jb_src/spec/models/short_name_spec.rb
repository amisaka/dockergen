# -*- encoding : utf-8 -*-
#<Encoding: utf-8>
require 'rails_helper'

describe ShortName do

  describe "permutations" do
    it "should return name for MOUNTAIN" do
      forms = ShortName.permute("ROCKY MOUNTAIN HOUSE")

      expect(forms.count).to eq(2)
    end

    it "should return name for ALBERTA BEACH" do
      forms = ShortName.permute("ALBERTA BEACH")

      expect(forms.count).to eq(1)
    end

    it "should permute names" do
      n01 = short_names(:n01)
      b1 = n01.substitute("ALBERTA BEACH")
      expect(b1).to eq("ALBERTA BCH")
    end

  end


end
