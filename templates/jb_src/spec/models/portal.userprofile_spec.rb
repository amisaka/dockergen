require 'rails_helper'

describe Portal::UserProfile do
  if $PORTAL

    describe "relations" do
      it "should have one user" do
        up1 = portal_userprofile(:crediluser1)

        expect(up1).to_not be_nil
        expect(up1.user).to_not be_nil
      end
    end
  end
end
