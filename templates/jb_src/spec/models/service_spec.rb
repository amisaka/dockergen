describe Service, type: :model do

  it { should have_many :billable_services }
  it { should validate_uniqueness_of :foreign_pid }
  it { should allow_value(nil).for :foreign_pid }

  if $CONNECTMSSQL
    it { should belong_to :hakka_product }
  end

end
