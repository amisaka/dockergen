describe Payment, type: :model do

  it { should belong_to :client }

  it { should have_many :payment_invoices }
  it { should have_many :invoices }

  it { should validate_presence_of :client }

  it { should validate_presence_of :amount }
  it { should validate_numericality_of :amount }

  it { should validate_presence_of :payment_date }

  describe "relationships" do
    it "should have an associated payments_invoice" do
      p1 = payments(:pay1)
      expect(p1.invoices.count).to be > 0
    end

    it "should have been made by a specific client" do
      p1 = payments(:pay1)
      expect(p1.client).to_not be_nil
    end
  end
end
