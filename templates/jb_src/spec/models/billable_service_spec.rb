describe BillableService, type: :model do

  describe "#service" do
    it { should validate_presence_of :service }
  end

  describe "relationships" do
    it { should belong_to :service }
    it { should belong_to :site }
    it { should belong_to :client }
  end

  describe "#billing_period" do
    it { should allow_value('never').for(:billing_period) }
    it { should allow_value(:never).for(:billing_period) }
    it { should allow_value(0).for(:billing_period) }

    it { should allow_value('monthly').for(:billing_period) }
    it { should allow_value(:monthly).for(:billing_period) }
    it { should allow_value(1).for(:billing_period) }

    it { should allow_value('yearly').for(:billing_period) }
    it { should allow_value(:yearly).for(:billing_period) }
    it { should allow_value(2).for(:billing_period) }
  end

end
