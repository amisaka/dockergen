#<Encoding:utf-8>
require 'rails_helper'

describe Cdr do

  def datafake
    @fake ||= Datasource.create(datasource_filename: 'fake1',
                                did_supplier: did_suppliers(:broadsoft))

  end

  # if ApplicationRecord.postgresql?
  #   after(:each) do
  #     ::ActiveRecord::Base.connection.execute("set application_name = 'spec_overhead';")
  #   end
  # end


  describe "relations" do
    it { should belong_to(:invoice) }

    it "should have an associated callingphone" do
      cdr1 = cdrviewer_cdr(:call3)
      expect( cdr1.callingphone ).to_not be_nil
    end

    it "should have an associated calledphone" do
      cdr1 = cdrviewer_cdr(:call3)
      expect( cdr1.calledphone ).to_not be_nil
    end

    it "should have an associated outgoing call" do
      cdr1 = cdrviewer_cdr(:cdr1)
      expect( cdr1.callingphone ).to_not be_nil
    end

    it "should have an associated incoming call" do
      cdr1 = cdrviewer_cdr(:cdr1)
      expect( cdr1.calledphone ).to_not be_nil
    end

    it "should have an associated datasource" do
      cdr1 = cdrviewer_cdr(:cdr1)
      expect( cdr1.datasource ).to_not be_nil
    end

    it "should have an call to group cdr records" do
      call1 = cdrviewer_cdr(:call1)
      expect( call1.call ).to_not be_nil

      call2 = cdrviewer_cdr(:call2)
      expect( call2.call ).to_not be_nil
    end

    it "should have a client associated via called/calling phone and btn" do
      call3 = cdrviewer_cdr(:call3)
      expect( call3.client ).to_not be_nil
      assert_equal "Novavision Communications TEST", call3.client.name
    end

    it "should have a called rate center" do
      cdr1 = cdrviewer_cdr(:cdr1)
      expect(cdr1.called_rate_center).to_not be_nil
    end

    it "should have few non-voip suppliers" do
      expect(Cdr.all.legacy_did_supplier.count).to eq(13)
    end
  end

  describe "operations" do
    it "should return a service name (called) for cdr1" do
      cdr1 = cdrviewer_cdr(:cdr1)
      assert_equal "{->5146531234}",cdr1.servicename
    end

    it "should return a service name (calling)" do
      cdr1 = cdrviewer_cdr(:mcrphonehome1)
      assert_equal "{<-2025559910}",cdr1.servicename
    end

    it "should return a string describing the cdr1 record" do
      cdr1 = cdrviewer_cdr(:cdr1)
      assert_equal "0/181916 {->5146531234} [  - 2011-10-13 18:19:16 UTC ]  +12015550189 +15141234567", cdr1.to_str

    end

    it "should return a string describing the mcrphonehome1 record" do
      cdr1 = cdrviewer_cdr(:mcrphonehome1)
      assert_equal "0/181916 {<-2025559910} [  - 2012-03-13 18:19:16 UTC ]  +16136930684 +15141234567", cdr1.to_str
    end

    it "should indicate true when the call is terminating" do
      cdr1 = cdrviewer_cdr(:cdr1)
      assert cdr1.terminating?
    end

    it "should indicate false when the call is originating" do
      cdr1 = cdrviewer_cdr(:mcrphonehome1)
      assert !cdr1.terminating?
    end

    it "should validate DID into canonical form" do
      #need to remove ' from DID, and optional +1
      assert_equal '2025551212', Phone.vdidify("2025551212")
      assert_equal '2025551212', Phone.vdidify("'2025551212")
      assert_equal '2025551212', Phone.vdidify("2025551212'")
      assert_equal '2025551212', Phone.vdidify("+2025551212")
      assert_equal '2025551212', Phone.vdidify("12025551212")
      assert_equal '2025551212', Phone.vdidify("+12025551212")
    end
  end

  describe "converting" do
    it "should convert a date like YYYMMDDhhmmss" do
      assert_equal Time.utc(2011,02,28,13,01,23), Cdr.cvtdate("20110228130123")

    end

    it "should convert a quoted date YYYMMDDhhmmss" do
      assert_equal Time.utc(2011,02,28,13,01,23), Cdr.cvtdate("\"20110228130123\"")
    end
  end

  describe "importing" do
    it "should import a new CDR record from a row" do
      aRow = ['00004275830016365B3F1A20120418154438.0381-040000',  # 1
        '"NovaVision HD"','Normal','15143527746','15143527747',    # 2-5
        'Originating',    '13510', 'Public',     '15143527746',    # 6-9
        '20120418154438', '1-040000','Yes','20120418154438.1',     # 10-13
        '20120418154718.8','16',                                   # 14-15
        'VoIP', '', '', '', '',                                    # 16-20
        '', '', 'remote','132.213.238.249',                        # 21-24
        '56da5e947042da7946c6d8f47358f6d3@132.213.238.249',        # 25
        'PCMU/8000', '','','','',                                  # 26-30
        '','NovaVision','','','',                                  # 31-35
        '','','','','',                                            # 36-40
        '','y','','','13440056:125',                               # 41-45
        '','','','','',                                            # 46-50
        '','','','','', '','','','','',                            # 51-60
        '','','','','', '','','','','', '','','','','',            # 61-75
        '','','','','', '','','','','', '','','','','',            # 76-90
        '','','','','', '','','','','', '','','','','',            # 91-105
        '','','','','', '','','','','', '','','','','',            # 106-120
        '5143527746@voip.novavision.ca','"Michael Richardson"',    # 121-122
        'Public','','', '','','','','', '','','','','',            # 123-125,126-135
        '','','','','', 'Yes','','','','',                         # 136-145
        '','','','20120418154444.8','Success',                     # 146-150
        '322561347:0A','Deflection','','','',                      # 151-155
        '','160.737','','','',                                     # 156-160
        '','','','','', '','','','','', '','','','','',            # 161-175
        '','','','','', '','','','','', '','','','','',            # 176-190
        '','','','','', '','','','','', '','','','','',            # 191-205
        '','','','','', '','','','','', '','','','','',            # 206-220
        '','','','','', '','','','','', 'Network','','','','',     # 221-235
        '','','','','', '','','','','', '','','','','',            # 236-250
        '','','','','', '','','','','', '','','','','',            # 251-265
        '','','','','', '','Yes']                                  # 266-272

      assert_equal 272, aRow.size
      c1,n = Cdr.import_row(aRow, datafake)
      expect( c1 ).to_not be_nil
      assert c1.originating?
      assert_equal 'NovaVision HD', c1.serviceprovider
      assert_equal '{<-5143527746}', c1.servicename
    end

    it "should import a new CDR record from a row with accents" do
      aRow = ['00002515800016365B3F1A20110321162056.3631-040000',
        'Frénk',
        'Normal','+12035550147','+12035550100','Originating',
        '+12035550147','Public','5143527746','20110321162056.363',
        '1-040000','Yes','20110321162056.529','20110321162128.205',
        '016','VoIP','','5143527746','other','ispl','5143527746',
        '','local','192.168.4.136:5060',
        'BW122056371210311333715021@192.168.21.2','PCMU/8000',
        '38.102.81.98','bc3e641fc950eb5c','','','','Frank Laval',
        '','','','','','','','','','y','public','',
        '24569897:0','','Ordinary','','','','','','',
        '','','','','','','','','','','','','','',
        '','','','','','','','','','','','','','',
        '','','','','','','','','','','','','','',
        '','','','','','','','','','','','','','',
        '','','','','','','','','','','','2035550147@voip.novavision.ca',
        '','','','','','','','','','','','','',
        '','','','','','','','','','','','','',
        '','','','','','','','','','31.675','',
        '','','','','','','','','','','','','',
        '','','','','','','','','','','','','',
        '','','','','','','','','','','','','',
        '','','','','','','','','','','','','',
        '','','','','','','','','','','','','',
        '','','','','','','','Network','','','',
        '','','','','','','','','','','','',
        '+15142345678','','','','']
      assert_equal 251,aRow.size
      c1,n = Cdr.import_row(aRow, datafake)
      expect( c1 ).to_not be_nil
    end

    it "should import a new CDR record from a sample" do
      aRow = ["00010019460016365B3F1A20120427040036.8211-040000",
        "NovaVision Service Provider", "Normal", "+12015550176",
        "+15142738578", "Terminating", "Unavailable", "Unavailable",
        "+12015550176", "20120427040036.821", "1-040000", "Yes",
        "20120427040045.421", "20120427040106.275", "016", "VoIP",
        nil, nil, nil, nil, nil, nil, "local", "Group", nil, "PCMU/8000",
        "192.168.4.190", "BW000036823270412523630598@192.168.21.2",
        nil, nil, nil, "Les Commissionnaires", nil, nil, nil,
        "+15142738578", "Public", "user-busy", "+15142738578",
        "Public", "user-busy", "n", nil, nil, "448490099:0",
        "448489377:1A", nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, "2015550176@voip.novavision.ca", nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, "Yes", nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, "20.853", nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, "Group", nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, "Yes", nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
      assert_equal 314,aRow.size
      c1,n = Cdr.import_row(aRow, datafake)
      expect( c1 ).to_not be_nil
    end

    it "should import a new CDR record from sample two" do
      aRow = ["00010019460016365B3F1A20120427040036.8211-040000",
        "NovaVision Service Provider", "Normal", "+12015550176",
        "+15149998578", "Terminating", "Unavailable", "Unavailable",
        "+12015550176", "20120427040036.821", "1-040000", "Yes",
        "20120427040045.421", "20120427040106.275", "016", "VoIP",
        nil, nil, nil, nil, nil, nil, "local", "Group", nil,
        "PCMU/8000", "192.168.4.190",
        "BW000036823270412523630598@192.168.21.2", nil, nil, nil,
        "Les Commedien de l'Ecosse", nil, nil, nil, "+15149998578",
        "Public", "user-busy", "+15149998578", "Public", "user-busy", "n",
        nil, nil, "448490099:0", "448489377:1A", nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, "2015550176@voip.novavision.ca", nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, "Yes", nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, "20.853", nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, "Group", nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, "Yes", nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        nil, nil, nil]
      assert_equal 314,aRow.size
      c1,n = Cdr.import_row(aRow, datafake)
      expect( c1 ).to_not be_nil
      expect( c1.call ).to_not be_nil
      expect(c1.call_cost).to  be_nil
      expect(c1).to            be_terminating
      expect(c1.call_price).to be_within(0.001).of(0.0)
    end

    it "should import a new CDR that has a field with a comma" do
      string = '00010095900016365B3F1A20120427130117.8331-040000,NovaVision Service Provider,Normal,+15145551234,+15145552345,Terminating,+15145551234,Public,+15145552345,20120427130117.833,1-040000,Yes,20120427130120.919,20120427130401.277,016,VoIP,,,,,,,local,205.205.211.110:5060,UEld58As1Rd0Wae@205.205.211.110,PCMU/8000,192.168.4.190,BW090117835270412-2144279368@192.168.21.2,,,,Gordon Lightfoot Chart Toper,,,,,,,,,,y,,,454004577:14,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,5145551234@voip.novavision.ca,Light\, Foot,Public,,,,,,,,,,,,Gordon Lightfood Chart Topper/Anjou_1,,,,,,Yes,,,,,,,,,,,,,,,,160.357,,,,,,,,,,,,,,,,,,,,,,,,,Normal,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Network,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Yes,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal 314,row.size
        c1,n = Cdr.import_row(row, datafake)
        expect( c1 ).to_not be_nil
        assert_equal "Light, Foot",c1.otherpartyname
      end
    end

    it "should be able to strip a callid" do
      cdr1 = cdrviewer_cdr(:call1)

      assert_equal "13440056:0",cdr1.callid_strip_text("13440056:0A")
      assert_equal "322561347:0",cdr1.callid_strip_text("322561347:0")
      assert_equal "322561347.123",cdr1.callid_strip_text("322561347.123A")
      assert_equal "322561347.123",cdr1.callid_strip_text("322561347.123")
    end

    it "should be able to associate similar legs together using our helper method for localcallid" do
      cdr1 = cdrviewer_cdr(:always_forward_leg1)
      # localcallid: 42720936:0A
      # remotecallid: "42720939:0"
      cdr2 = cdrviewer_cdr(:always_forward_leg2)
      # localcallid: "42720936:0"

      cdr1.associate_by_localcallid_using_permutation(cdr1.localcallid)

      assert_equal cdr1.call,cdr2.call
    end

    it "should be able to associate similar legs together using our helper method for localcallid" do
      cdr1 =["00017625660016365B3F1A20121106150607.2340-050000","NovaVision HD","Normal","+15146531234","+15143527747","Originating","+15146531234","Public","+16135802400","20121106150607.234","0-050000","Yes","20121106150609.760","20121106150646.412","016","VoIP",nil,"3221","toll",nil,nil,nil,"remote","Group",nil,"PCMU/8000","192.168.4.190","dc9b796d53f17593",nil,nil,nil,"NovaVision",nil,nil,nil,nil,nil,nil,nil,nil,nil,"n",nil,nil,"42723941:0","42720939:0A","Ordinary",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"2025550110@voip.novavision.ca","Frank Rubble","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"36.650",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"+15143527747",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
      c1,n = Cdr.import_row(cdr1, datafake)

      # assert some things about this outgoing leg.
      expect(c1.call_cost).to  be_nil
      expect(c1).to            be_originating
      expect(c1.call_price).to be_within(0.001).of(0.065)

      cdr2 = cdrviewer_cdr(:always_forward_leg1)
      cdr2_remotecallid = cdr2.remotecallid
      cdr2.associate_by_remotecallid_using_permutation(cdr2_remotecallid)

      expect(c1.call).to eq(cdr2.call)
    end

    it "should import a second CDR without indexing nil" do
      string='00056263920016365B3F1A20120119131546.5640-050000,Frisbee Fun,Normal,+12065550110,+15145551234,Originating,+12065550110,Public,Unavailable@voip.novavision.ca,20120119131546.564,0-050000,No,,20120119131547.278,003,VoIP,,Unavailable@voip.novavision.ca,,,,,local,unconfirmed,,PCMU/8000,192.168.4.190,3aeec92d7bc87bed,,,,Frisbee Fun,,,,,,,,,,n,,,83877225:0,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2065550110@voip.novavision.ca,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,0.549,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Network URL,,,,,,,,,,,,,,,,+15145551234,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal 251,row.size
        c1,n = Cdr.import_row(row, datafake)
        expect( c1 ).to_not be_nil
        expect( c1.call ).to_not be_nil
      end
    end

    it "should truncate transfer_type to 20 characters" do
      string='00007687570016365B3F1A20120424121329.3651-040000,NovaVision Service Provider,Normal,+15145551234,+15145551234,Terminating,+15143527747,Public,+15145551234,20120424121329.365,1-040000,Yes,20120424121332.098,20120424121334.241,016,VoIP,,,,,,,remote,192.168.21.2,BW081329364240412-901212688@192.168.21.2,PCMU/8000,,,,,,Lung Stores,,,,,,,,,,y,,,407699465:0,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,5145551234@voip.novavision.ca,NovaVision,Public,,,,,,,,,,,,Lung_Stores/Lung_Stores,,,,,,Yes,,,,,,,,20120424121329.416,Success,407699465:1A,Trunk Group Forward Unconditional,,,,,2.141,,,,,,,,,,,,,,,,,,,,,,,,,Unconditional,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Network,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Yes,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal 314,row.size
        c1,n = Cdr.import_row(row, datafake)
        expect( c1 ).to_not be_nil
        assert_equal 20,c1.transfer_type.length
      end
    end


    it "should import first call without a transfer_type" do
      string='00041495390016365B3F1A20120602130347.0781-040000,Trévi,Normal,+12115550142,+14504356550,Terminating,+12035550166,Public,+12115550142,20120602130347.078,1-040000,Yes,20120602130348.922,20120602130600.563,016,VoIP,,,,,,,local,Enterprise,,PCMU/8000,192.168.4.190,BW090347081020612-1317963163@192.168.21.2,,,,Trevi Blainville,,,,,,,,,,n,,,1006475709:0,1006474935:1,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2115550142@voip.novavision.ca,2204 Stephanie Cote,Public,,,,,,,,,,,,,,,,,,Yes,,,,,,,,,,,,,,,,121.502,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Enterprise,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Yes,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal 314,row.size
        c1,n = Cdr.import_row(row, datafake)
        expect( c1 ).to_not be_nil
      end
    end

    it "should import second call with a transfer_type of Transfer Consult" do
      string='00041495450016365B3F1A20120602130326.6761-040000,Frénk,Normal,+12035550166,+15146531234,Terminating,+15141112222,Public,+12035550166,20120602130326.676,1-040000,Yes,20120602130331.545,20120602130600.563,016,VoIP,,,,,,,remote,Group,,PCMU/8000,,,,,,Frenk Siege Sociale,,,,+15142287384,Public,deflection,+15146531234,Public,call-center,n,,,1006474935:0,1006474927:2,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2035550166@voip.novavision.ca,,,,,,,,,,,,,,,,,,,,Yes,,,,,,,,20120602130359.011,Success,1006474935:1,Transfer Consult,,,,,149.017,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Group,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Yes,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal 314,row.size
        c1,n = Cdr.import_row(row, datafake)
        expect( c1 ).to_not be_nil
      end
    end

    it "should import third call with a transfer_type of Transfer Consult" do
      string='00041495450016365B3F1A20120602130326.6761-040000,Frénk,Normal,+12035550166,+15145631234,Terminating,+15149842071,Public,+12035550166,20120602130326.676,1-040000,Yes,20120602130331.545,20120602130600.563,016,VoIP,,,,,,,remote,Group,,PCMU/8000,,,,,,Frank Siege Sociale,,,,+15145631234,Public,deflection,+15145631234,Public,call-center,n,,,1006474935:0,1006474927:2,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2035550166@voip.novavision.ca,,,,,,,,,,,,,,,,,,,,Yes,,,,,,,,20120602130359.011,Success,1006474935:1,Transfer Consult,,,,,149.017,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Group,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Yes,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal 314,row.size
        c1,n = Cdr.import_row(row, datafake)
        expect( c1 ).to_not be_nil
      end
    end


    it "should import fourth call with a transfer_type of Transfer Consult" do
      string='00041495420016365B3F1A20120602130347.0781-040000,Trévi,Normal,+12035550166,+15142287384,Originating,+12035550166,Public,+12115550142,20120602130347.078,1-040000,Yes,20120602130348.922,20120602130600.563,016,VoIP,,9603,private,,,,remote,Enterprise,,PCMU/8000,,,,,,Trevi Siege Sociale,,,,,,,,,,n,,,1006474935:1,1006475709:0,Ordinary,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2035550166@voip.novavision.ca,9603 Pierre-Emmanuel Girard,Public,,,,,,,,,,,,,,,,,,,,,,,,,,20120602130359.011,Success,1006474935:0,Transfer Consult,,,,,121.501,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Enterprise,,,,,,,,,,,,,,,,+15148320550,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal 314,row.size
        c1,n = Cdr.import_row(row, datafake)
        expect( c1 ).to_not be_nil
      end
    end


    it "should import redundant leg into existing call" do
      fakeDataSource  = datafake()

      first_leg_a     = ["00000188790016365B3F1A20121005143111.9511-040000","NovaVision HD","Normal","+12025550112","+15143527747","Originating","+12025550112","Public","+12025550115","20121005143111.951","1-040000","Yes","20121005143118.465","20121005143124.242","016","VoIP",nil,"3220","private",nil,nil,nil,"local","Group",nil,"PCMU/8000","192.168.4.190","8b4d42ef0b9f21f5",nil,nil,nil,"NovaVision",nil,nil,nil,nil,nil,nil,nil,nil,nil,"n",nil,nil,"1255681263:0","1255681267:0","Ordinary",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"2025550112@voip.novavision.ca","Libre .","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"5.774",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"+15143527747",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
      redundant_leg_a = ["00000188800016365B3F1A20121005143111.9511-040000","NovaVision HD","Normal","+12025550115","+15143527747","Terminating","+12025550112","Public","+12025550115","20121005143111.951","1-040000","Yes","20121005143118.465","20121005143124.242","016","VoIP",nil,nil,nil,nil,nil,nil,"remote","Group",nil,"PCMU/8000","192.168.4.190","BW103111955051012-223882304@192.168.21.2",nil,nil,nil,"NovaVision",nil,nil,nil,nil,nil,nil,nil,nil,nil,"n",nil,nil,"1255681267:0","1255681263:0",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"2025550115@voip.novavision.ca","Nick Foisy","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"5.776",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

      first_leg_b     = ["00017625650016365B3F1A20121106150607.2340-050000","NovaVision HD","Normal","+12015550142","+15143527747","Terminating","+12025550110","Public","+12015550142","20121106150607.234","0-050000","Yes","20121106150609.760","20121106150646.412","016","VoIP",nil,nil,nil,nil,nil,nil,"local","Group",nil,"PCMU/8000","192.168.4.190","BW100607238061112-141996111@192.168.21.2",nil,nil,nil,"NovaVision",nil,nil,nil,nil,nil,nil,nil,nil,nil,"n",nil,nil,"1317053123:0","1317053115:0",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"2015550142@voip.novavision.ca","Jean Francois Berthiaume","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"36.651",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
      redundant_leg_b =["00017625660016365B3F1A20121106150607.2340-050000","NovaVision HD","Normal","+12025550110","+15143527747","Originating","+12025550110","Public","+12015550142","20121106150607.234","0-050000","Yes","20121106150609.760","20121106150646.412","016","VoIP",nil,"3221","private",nil,nil,nil,"remote","Group",nil,"PCMU/8000","192.168.4.190","dc9b796d53f17593",nil,nil,nil,"NovaVision",nil,nil,nil,nil,nil,nil,nil,nil,nil,"n",nil,nil,"1317053115:0","1317053123:0","Ordinary",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"2025550110@voip.novavision.ca","Etienne Blain","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"36.650",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"+15143527747",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

      c1,n = Cdr.import_row(first_leg_a, fakeDataSource)
      expect( c1 ).to_not be_nil
      c2, = Cdr.import_row(redundant_leg_a, fakeDataSource)
      expect( c2 ).to_not be_nil

      assert_equal c1.call_id, c2.call_id

      c3,n = Cdr.import_row(first_leg_b, fakeDataSource)
      expect( c3 ).to_not be_nil
      c4,n = Cdr.import_row(redundant_leg_b, fakeDataSource)
      expect( c4 ).to_not be_nil

      assert_equal c3.call_id, c4.call_id

    end

    it "should deal with associating no-answer and associate by localcallid" do
      fakeDataSource  = datafake()


      first_leg   = ["00000174780016365B3F1A20121005142124.5361-040000","NovaVision Service Provider","Normal","+15146587213",nil,"Originating","+15143527747","Public","+15143127644","20121005142124.536","1-040000","Yes","20121005142124.595","20121005142141.554","016","VoIP",nil,"900","private",nil,nil,nil,"remote","Group",nil,"PCMU/8000",nil,nil,nil,nil,nil,"NovaVision Telecom",nil,nil,nil,"+15146587213","Public","no-answer","+15146587213","Public","no-answer","n",nil,nil,"1255658639:0A","1255659955:0",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"5146587213@voip.novavision.ca","Voice Portal Voice Portal","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"1255658639:0","Call Forward No Answer",nil,nil,nil,nil,nil,nil,nil,nil,"16.958",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"+15146587213",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
      second_leg  = ["00000174790016365B3F1A20121005142124.5361-040000","NovaVision Service Provider","Normal","+15143127644",nil,"Terminating","+15143527747","Public","+15143127644","20121005142124.536","1-040000","Yes","20121005142124.595","20121005142141.554","016","VoIP",nil,nil,nil,nil,nil,nil,"remote","Group",nil,"PCMU/8000",nil,nil,nil,nil,nil,"NovaVision Telecom",nil,nil,nil,"+15146587213","Public","no-answer","+15146587213","Public","no-answer","n",nil,nil,"1255659955:0","1255658639:0A",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"194228864_VMR@voip.novavision.ca","NovaVision","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"16.959",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

      c1,n = Cdr.import_row(first_leg, fakeDataSource)
      expect( c1 ).to_not be_nil

      c2,n = Cdr.import_row(second_leg, fakeDataSource)
      expect( c2 ).to_not be_nil

      assert_equal c1.call_id, c2.call_id
    end

    it "should deal with associating Call Forward No Answer and associated by remotecallid" do
      first_leg  = ["00007551450016365B3F1A20121004182746.5281-040000","NovaVision HD","Normal","+12905550110","+15143527747","Terminating","+12025550115","Public","+12905550110","20121004182746.528","1-040000","Yes","20121004182746.562","20121004182807.973","016","VoIP",nil,nil,nil,nil,nil,nil,"remote","Group",nil,"PCMU/8000",nil,nil,nil,nil,nil,"NovaVision",nil,nil,nil,"+15143527747","Public","no-answer","+15143527747","Public","no-answer","n",nil,nil,"1254002515:0","1254001443:0A",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"127638530_VMR@voip.novavision.ca","Libre .","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"21.410",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
      second_leg = ["00007551470016365B3F1A20121004182746.5281-040000","NovaVision HD","Normal","+12025550109","+15143527747","Originating","+12025550115","Public","+12905550110","20121004182746.528","1-040000","Yes","20121004182746.562","20121004182807.973","016","VoIP",nil,"900","private",nil,nil,nil,"remote","Group",nil,"PCMU/8000",nil,nil,nil,nil,nil,"NovaVision",nil,nil,nil,"+15143527747","Public","no-answer","+15143527747","Public","no-answer","n",nil,nil,"1254001443:0A","1254002515:0","Ordinary",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"2025550109@voip.novavision.ca","Voice Portal Voice Portal","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"1254001443:0","Call Forward No Answer",nil,nil,nil,nil,nil,nil,nil,nil,"21.408",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"+15143527747",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

      fakeDataSource  = datafake()

      c1,n = Cdr.import_row(first_leg, fakeDataSource)
      expect( c1 ).to_not be_nil

      c2,n = Cdr.import_row(second_leg, fakeDataSource)
      expect( c2 ).to_not be_nil

      assert_equal c1.call_id, c2.call_id
    end

    it "should deal with associating Call Forward No Answer and associated by localcallid" do
      first_leg  = ["00000174770016365B3F1A20121005142054.5311-040000","NovaVision Service Provider","Normal","+15146587213",nil,"Terminating","+15143527747","Public","+15146587213","20121005142054.531","1-040000","Yes-PostRedirection","20121005142124.595","20121005142141.554","016","VoIP",nil,nil,nil,nil,nil,nil,"remote","192.168.21.2","BW102054526051012899167488@192.168.21.2","PCMU/8000",nil,nil,nil,nil,nil,"NovaVision Telecom",nil,nil,nil,nil,nil,nil,nil,nil,nil,"y",nil,nil,"1255658639:0",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"5146587213@voip.novavision.ca","NovaVision","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"16.958",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Network",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
      second_leg = ["00000174780016365B3F1A20121005142124.5361-040000","NovaVision Service Provider","Normal","+15146587213",nil,"Originating","+15143527747","Public","+15143127644","20121005142124.536","1-040000","Yes","20121005142124.595","20121005142141.554","016","VoIP",nil,"900","private",nil,nil,nil,"remote","Group",nil,"PCMU/8000",nil,nil,nil,nil,nil,"NovaVision Telecom",nil,nil,nil,"+15146587213","Public","no-answer","+15146587213","Public","no-answer","n",nil,nil,"1255658639:0A","1255659955:0",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"5146587213@voip.novavision.ca","Voice Portal Voice Portal","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"1255658639:0","Call Forward No Answer",nil,nil,nil,nil,nil,nil,nil,nil,"16.958",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"+15146587213",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

      fakeDataSource  = datafake()

      c1,n = Cdr.import_row(first_leg, fakeDataSource)
      expect( c1 ).to_not be_nil

      c2,n = Cdr.import_row(second_leg, fakeDataSource)
      expect( c2 ).to_not be_nil

      assert_equal c1.call_id, c2.call_id
    end

    it "can associate legs when the second leg has a transfer type as deflection" do
      ds1 = Datasource.create(:datasource_filename=>'BW-CDR-20121005151500-2-0016365B3F1A-004457.csv')
      leg1 = ["00000534340016365B3F1A20121005190823.2591-040000","NovaVision HD","Normal","+12025550109","+15143527747","Terminating","+12025550115","Public","+12025550109","20121005190823.259","1-040000","Yes","20121005190825.819","20121005190857.583","016","VoIP","","","","","","","local","Group","","PCMU/8000","192.168.4.190","BW150823353051012-1601852048@192.168.21.2","","","","NovaVision","","","","+15143527747","Public","deflection","+15143527747","Public","deflection","n","","","1256277573:0","1256276065:0A","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","2025550109@voip.novavision.ca","Libre .","Public","","","","","","","","","","","","","","","","","","Yes","","","","","","","","","","","","","","","","31.762","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Group","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Yes","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]
      leg2 = ["00000534350016365B3F1A20121005190741.5821-040000","NovaVision HD","Normal","+12025550115","+15143527747","Originating","+12025550115","Public","+12025550113","20121005190741.582","1-040000","Yes","20121005190744.957","20121005190857.583","016","VoIP","","3215","private","","","","remote","Group","","PCMU/8000","192.168.4.190","4d84faca19dfc0d0","","","","NovaVision","","","","","","","","","","n","","","1256276061:0","1256276065:0","Ordinary","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","2025550115@voip.novavision.ca","Natasha Trindade","Public","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","72.623","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Group","","","","","","","","","","","","","","","","+15143527747","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]
      leg3 = ["00000534360016365B3F1A20121005190741.5821-040000","NovaVision HD","Normal","+12025550113","+15143527747","Terminating","+12025550115","Public","+12025550113","20121005190741.582","1-040000","Yes","20121005190744.957","20121005190857.583","016","VoIP","","","","","","","remote","Group","","PCMU/8000","","","","","","NovaVision","","","","","","","","","","n","","","1256276065:0","1256276061:0","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","2025550113@voip.novavision.ca","Libre .","Public","","","","","","","","","","","","","","","","","","Yes","","","","","","","","20121005190823.259","Success","1256276065:0A","Deflection","","","","","72.625","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Group","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Yes","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]
      leg4 = ["00000534370016365B3F1A20121005190823.2591-040000","NovaVision HD","Normal","+12025550113","+15143527747","Originating","+12025550115","Public","+12025550109","20121005190823.259","1-040000","Yes","20121005190825.819","20121005190857.583","016","VoIP","","3211","private","","","","remote","Group","","PCMU/8000","","","","","","NovaVision","","","","+12025550113","Public","deflection","+12025550113","Public","deflection","n","","","1256276065:0A","1256277573:0","Ordinary","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","2025550113@voip.novavision.ca","John Mateus","Public","","","","","","","","","","","","","","","","","","","","","","","","","","20121005190823.259","Success","1256276065:0","Deflection","","","","","31.758","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","Group","","","","","","","","","","","","","","","","+15143527747","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]

      c1,n = Cdr.import_row(leg1, ds1)
      expect( c1 ).to_not be_nil

      c2,n = Cdr.import_row(leg2, ds1)
      expect( c2 ).to_not be_nil

      c3,n = Cdr.import_row(leg3, ds1)
      expect( c3 ).to_not be_nil

      c4,n = Cdr.import_row(leg4, ds1)
      expect( c4 ).to_not be_nil

      c1.reload; c2.reload; c3.reload; c4.reload
      assert_equal c2.call_id, c1.call_id
      assert_equal c3.call_id, c1.call_id
      assert_equal c4.call_id, c1.call_id
    end

    it "can associate legs when the second leg has redirectingreason as deflection and they are associated by localcallid" do
      fakeDataSource = datafake

      first_leg  = ["00000537470016365B3F1A20121005190928.6911-040000","NovaVision HD","Normal","+15143527747","+15143527747","Terminating","+15142937856","Public","+15143527747","20121005190928.691","1-040000","Yes","20121005190928.708","20121005191051.295","016","VoIP",nil,nil,nil,nil,nil,nil,"remote","205.205.211.110:5060","mWld58ys4mj9sbr@205.205.211.110","PCMU/8000",nil,nil,nil,nil,nil,"NovaVision",nil,nil,nil,nil,nil,nil,nil,nil,nil,"y",nil,nil,"1256280127:0",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"5143527746@voip.novavision.ca",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,"20121005190939.585","Success","1256280127:0A","Deflection",nil,nil,nil,nil,"82.587",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Network",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Yes",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
      second_leg = ["00000537480016365B3F1A20121005190939.5851-040000","NovaVision HD","Normal","+15143527747","+15143527747","Originating","+15142937856","Public","+12125550180","20121005190939.585","1-040000","Yes","20121005190939.636","20121005191051.295","016","VoIP",nil,"2125550180","private",nil,nil,nil,"remote","Group",nil,"PCMU/8000",nil,nil,nil,nil,nil,"NovaVision",nil,nil,nil,"+15143527747","Public","deflection","+15143527747","Public","deflection","n",nil,nil,"1256280127:0A","1256280429:0",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"5143527746@voip.novavision.ca","Main Menu En.","Public",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"20121005190939.585","Success","1256280127:0","Deflection",nil,nil,nil,nil,"71.658",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,"Group",nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

      c1,n = Cdr.import_row(first_leg, fakeDataSource)
      expect( c1 ).to_not be_nil

      c2,n = Cdr.import_row(second_leg, fakeDataSource)
      expect( c2 ).to_not be_nil

      c1.reload; c2.reload;
      assert_equal c1.call_id, c2.call_id
    end

    it "should convert strings to dates" do
      cdr1 = cdrviewer_cdr(:mcrphonehome3)
      cdr1.cvt_dates
      expect( cdr1.startdatetime ).to_not be_nil
      expect( cdr1.answerdatetime ).to_not be_nil
      expect( cdr1.releasedatetime ).to_not be_nil
    end

    it "should look up callingnumber and callednumber to get phone" do
      cdr1 = cdrviewer_cdr(:mcrphonehome3)
      cdr1.calc_derived(0)
      expect( cdr1.callingphone ).to_not be_nil
      assert_nil cdr1.calledbtn
      assert cdr1.duration > 0
    end

    it "should create a new callingnumber for outgoing calls with missing phone" do
      $BtnOfLastResortId  = btns(:lastresort).id
      cdr1 = cdrviewer_cdr(:mcrphonehome3)
      wrongnum = '6135559910'
      cdr1.callingnumber = wrongnum
      expect(Phone.lookup_vdid_record_for(wrongnum)).to be nil

      cdr1.lookup_calling_phone(1)
      expect(cdr1.callingphone).to_not be_nil
    end

    it "should look up 6930684 to get phone" do
      num = "2025559910"
      cdr1 = cdrviewer_cdr(:mcrphonehome3)
      p1 = Phone.lookup_vdid_record_for(num)
      expect( p1 ).to_not be_nil
    end

    it "should look up 16136930684 to get phone" do
      cdr1 = cdrviewer_cdr(:mcrphonehome3)
      p1 = cdr1.lookup_calling_phone(1)
      expect( p1 ).to_not be_nil
    end

    it "should import a sample file" do
      start=Cdr.all.count
      opts = { file: "spec/files/cdr-sample1.csv",
               overwrite: true,
               verbose:   false}
      number,duration,firstdate,datasource = Cdr.load_bw_file(opts)
      newcount=Cdr.all.count
      assert newcount > start
      expect( datasource ).to_not be_nil
      expect(datasource.cdrs_count).to  eq(177)
      expect(datasource.calls_count).to eq(96)
      assert datasource.load_time > 0
      expect( firstdate ).to_not be_nil
    end

    it "should import a sample file that has backslash-quoted commas" do
      start=Cdr.all.count
      opts = { file: "spec/files/cdr-comma1.csv",
               overwrite: true,
               verbose:   false}
      number,duration,firstdate,datasource = Cdr.load_bw_file(opts)
      newcount=Cdr.all.count
      assert newcount > start

      expect( datasource ).to_not be_nil
      expect(datasource.cdrs_count).to  eq(1)
      expect(datasource.calls_count).to eq(1)
      expect(datasource.cdrs.first.group).to eq("Gesture, Placemat PLC site 1")
      expect( firstdate ).to_not be_nil
    end

    it "should ignore failover records" do
      start=Cdr.all.count
      opts = { file: "spec/files/failOverCDR.csv",
               overwrite: true,
               verbose:   false}
      number,duration,firstdate,datasource = Cdr.load_bw_file(opts)
      newcount=Cdr.all.count
      assert newcount > start
      expect( datasource ).to_not be_nil
    end

    it "should import a csv file with two back quoted commas" do
      string='00065960420016365B3F1A20120703180551.5461-040000,Foo Bar,Normal,+12105550129,+14505551212,Originating,+12105550129,Public,5145551212\,2006#\,11@voip.novavision.ca,20120703180551.546,1-040000,No,,20120703180556.531,003,VoIP,,5145551212\,2006#\,11@voip.novavision.ca,,,,,local,unconfirmed,,PCMU/8000,173.246.31.51,325450829646@192.168.3.24,,,,Laval,,,,,,,,,,n,,,1084435919:0,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,2105550129@voip.novavision.ca,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,4.896,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,Network URL,,,,,,,,,,,,,,,,+14055551212,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
      CredilCSV.parse(string, :escape_char => '\\') do |row|
        assert_equal '5145551212,2006#,11@voip.novavision.ca', row[8]
      end
    end

    it "should import a cdr troop sample file" do

      start=Cdr.all.count
      opts1 = { file: "spec/files/cdrtroop.csv",
                overwrite: true,
                verbose:   false,
              }
      n1 = Cdr.load_cdr_troop(opts1)
      expect(n1).to   be_truthy
    end

    it "should import a sample file that has a funny date" do
      start=Cdr.all.count
      opts1 = { file: "spec/files/out-of-range.csv",
               overwrite: true,
               verbose:   false}
      Cdr.load_bw_file(opts1)
      newcount=Cdr.all.count
      assert newcount > start
    end

    it "should import a sample file that has unclosed quoted field" do
      start=Cdr.all.count
      opts1 = { file: "spec/files/unclosedQuote1.csv",
               overwrite: true,
               verbose:   false}

      Cdr.load_bw_file(opts1)

      opts2 = { file: "spec/files/unclosedQuote2.csv",
               overwrite: true,
               verbose:   false}
      Cdr.load_bw_file(opts2)
      newcount=Cdr.all.count
      assert newcount > start
    end

    it "should import a sample file:cdr-sample1, ignoring second time" do
      start=Cdr.all.count
      opts1 = { file: "spec/files/cdr-sample1.csv",
               overwrite: false,
               verbose:   false}
      count, time = Cdr.load_bw_file(opts1)
      expect(count).to eq(177)

      newcount=Cdr.all.count
      expect(newcount).to be > start

      assert_equal :duplicate, Cdr.load_bw_file(opts1)
      new2 = Cdr.all.count
      assert_equal newcount, new2
    end

    it "should import a CDR file with the related call id reason Barge-in" do
      opts1 = { file: "spec/files/relatedCallReason_bargeIn.csv",
               overwrite: false,
               verbose:   false}
      count, time = Cdr.load_bw_file(opts1)
      assert_equal 4, count

    end
  end

  describe "calculating" do
    it "should import two CDR records which are related and discover relation" do
      assert_equal 272,$CdrRow1.size
      assert_equal 272,$CdrRow2.size
      c1,n = Cdr.import_row($CdrRow1, datafake, 1)
      expect( c1.localcallid ).to_not be_nil
      callid = c1.localcallid
      c2,n = Cdr.import_row($CdrRow2, datafake, 2)
      expect( c2.transfer_relatedcallid ).to_not be_nil
      assert_equal callid,c2.transfer_relatedcallid
    end

    it "should connect four CDR records with the Novavision HQ client and BTN" do
      leg1 = cdrviewer_cdr(:nvexample1_leg1)
      leg2 = cdrviewer_cdr(:nvexample1_leg2)
      leg3 = cdrviewer_cdr(:nvexample1_leg3)
      leg4 = cdrviewer_cdr(:nvexample1_leg4)

      assert_equal leg1.call, leg2.call
      assert_equal leg1.call, leg3.call
      assert_equal leg1.call, leg4.call
    end

    it "should pick leg01 as incoming call" do
      l01  = cdrviewer_cdr(:abandonedcall01_leg01)
      l02  = cdrviewer_cdr(:abandonedcall01_leg02)
      l03  = cdrviewer_cdr(:abandonedcall01_leg03)

      expect(l01).to be_startingleg
      expect(l02).not_to be_startingleg
      expect(l03).not_to be_startingleg

      l01  = cdrviewer_cdr(:abandonedcall02_leg01)
      l02  = cdrviewer_cdr(:abandonedcall02_leg02)

      expect(l01).to be_startingleg
      expect(l02).not_to be_startingleg
    end

    it "should pick leg02 as handset ring" do
      l01  = cdrviewer_cdr(:abandonedcall01_leg01)
      l02  = cdrviewer_cdr(:abandonedcall01_leg02)
      l03  = cdrviewer_cdr(:abandonedcall01_leg03)

      expect(l01).not_to be_handsetring
      expect(l02).to     be_handsetring
      expect(l03).not_to be_handsetring

      l01  = cdrviewer_cdr(:abandonedcall02_leg01)
      l02  = cdrviewer_cdr(:abandonedcall02_leg02)

      expect(l01).not_to be_handsetring
      expect(l02).to     be_handsetring
    end

    it "should import a CDR with a call type Long Duration without stopping on the debugger" do
      begin_time = Time.now
      opts1 = { file: "spec/files/longDurationCallTypeCDR.csv",
                overwrite: false,
                verbose:   false}
      count, time = Cdr.load_bw_file(opts1)

      end_time = Time.now
      assert_equal 2, count
    end

    it "should not stop on the debugger for an unkwown call type" do
      begin_time = Time.now
      opts1 = { file: "spec/files/unknownCallTypeCDR.csv",
                overwrite: false,
                verbose:   false}
      count, time, firstdate, datasource = Cdr.load_bw_file(opts1)
      end_time = Time.now
      assert_equal 2, count
      expect($UNKNOWN_CALL_TYPE).to be true
    end
  end

  describe "toll typing" do
    it "should map a 514 call as local" do
      cdr1 = Cdr.create(networkcalltype: 'to',
                        callednumber: '+15144444044',
                        callingphone: phones(:phone_rate41))

      expect(cdr1.detailed_toll_calculation).to eq('lo')
    end

    it "should map an 844 call as toll-free" do
      cdr1 = Cdr.create(networkcalltype: 'to',
                        callednumber: '+18444444742',
                        callingphone: phones(:phone_rate41))

      expect(cdr1.detailed_toll_calculation).to eq('lo')
    end

    it "should map a call to ARNOLDs COVE as On-Net for a quebec plan" do
      cdr1 = cdrviewer_cdr('calltoarnoldscove')

      cdr1.lookup_phones
      cdr1.detailed_toll_calculation(true)
      expect(cdr1.called_rate_center).to eq(rate_centers(:rc79))
      expect(cdr1.callingphone.local_plan).to eq(rate_plans(:quebec))
      expect(cdr1.callingphone.local_plan.carrier).to eq(carriers(:Thinktel))
      expect(cdr1.networkcalltype).to eq('to')
    end

    it "should map a call to ARNOLDs COVE as On-Net for a national plan" do
      cdr1 = cdrviewer_cdr('calltoarnoldscove_national')

      cdr1.lookup_phones
      cdr1.detailed_toll_calculation(true)
      expect(cdr1.called_rate_center).to eq(rate_centers(:rc79))
      expect(cdr1.callingphone.local_plan).to eq(rate_plans(:national))
      expect(cdr1.callingphone.local_plan.carrier).to eq(carriers(:Thinktel))
      expect(cdr1.networkcalltype).to eq('lo')
    end

    it "should price a 519 cdr as domestic" do
      cdr1 = cdrviewer_cdr('cdr0009232468005056B72B8F20161013215712.3071-040000')
      expect(cdr1.no_bill?).to be false
    end

    it "should price tpc1 call at 0.078$" do
      tpc1 = cdrviewer_cdr(:billing_tpc1)

      expect(tpc1.active_duration).to                           eq(184)
      expect(tpc1.rate.duration_round(tpc1.active_duration)).to eq(186)
      expect(tpc1.billed_duration).to be_within(1).of(3*60 + 6)
      expect(tpc1.price).to be_within(0.001).of(0.046)
    end

    it "should calculate a toll call and export an edit script" do
      cdr1 = Cdr.create(datasource:   datafake,
                        recordid:     '012345689',
                        lineno:       123,
                        networkcalltype: 'to',
                        callednumber: '+15144444044',
                        callingphone: phones(:phone_rate41))

      expect(cdr1.toll_edit_file).to      eq(Rails.root.join('tmp', 'cdredit', 'fake1.sh').to_s)
      expect(cdr1.record_toll_type_change).to eq('lo')
      expect(cdr1.billingreason).to       eq('default-rate-center')
      datafake.close_edit_handle
    end
  end

  describe "Import of Bell TROOP CDRs" do
    it "should find some ut*.csv files" do

      cnt = 0
      Cdr.find_legacy_cdr_files(Cdr.troop_pattern, Rails.root.join('spec','files', 'troop')) { |path, basename|
        cnt += 1
      }
      expect(cnt).to eq(117)
    end

    it "should import a single bt290 file" do
      file = 'spec/files/ur20161101130921.bt290'
      base = File.basename(file)
      number,durations,firstdate,datasource = Cdr.load_cdr_troop({file: file})
      expect(number).to eq(12)
      expect(datasource.datasource_filename).to eq(base)
      expect(datasource.did_supplier.try(:name)).to eq('Troop supplier')

      cdr1 = datasource.cdrs.first
      expect(cdr1.call_cost).to be_within(0.001).of(0.01)
    end

    it "should have few non-voip suppliers" do
      expect(Cdr.all.legacy_did_supplier.count).to eq(13)
    end
  end

  describe "import of Tellus Rebiller" do
    def line1
      # it is base-64 encoded, because embedded spaces are fragile in some editors.
      # this is line 2 of file spec/files/cdrtellusrebiller.csv
      # sed -n -e 2p spec/files/cdrtellusrebiller.csv | base64
      Base64.decode64("MDEwMTAxMTYxMDA5MTA5MTExMTc2ODk4ICAgMTA4NjY4ODgxMTExMDAwMDQwICAgICAgICAgMTEx"+
                      "NjMyMDAwMjI1NzA0ICAgICAgICAzNTUgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDUx"+
                      "NDM1Mjc3NDdTWURORVkgICAgTlNNT05UUkUxTCAgUFFPTVpaICAxMTAyMjcyNyAgMDAgICAgICAg"+
                      "ICAgICAgICAgICA0NTQxMDcwMDgwICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg"+
                      "ICAgICAgICAgICAgICAgICAgICAgICAgICAwCg==")
    end

    it "should parse a single telus line" do
      row1 = Cdr.row_from_cdr_telus(line1)
      expect(row1.length).to eq(9)
      expect(row1[0]).to     eq("010101")
      expect(row1[1]).to     eq("161009")
      expect(row1[2]).to     eq("9111176898")
      expect(row1[3]).to     eq("8668881111")
      expect(row1[4]).to     eq("111632")
      expect(row1[5]).to     eq("0002257")
      expect(row1[6]).to     eq("5143527747")
      expect(row1[7]).to     eq("4541070080")
    end

    it "should import a single telus line" do
      options   = { }
      linecount = 1
      cdr1 = Cdr.import_cdr_telus_line(options, linecount, line1, datafake)
      expect(cdr1.startdatetime).to eq(Time.zone.parse('2016-10-09 11:16:32'))
      expect(cdr1.duration).to eq(2257)
      expect(cdr1.call_cost).to be_nil
    end

    it "should import a cdr tellus rebiller sample file" do

      start=Cdr.all.count
      opts1 = { file: "spec/files/cdrtellusrebiller.csv",
                overwrite: true,
                verbose:   false,
              }
      linecount,durations,firstdate,datasource = Cdr.load_cdr_telus_rebiller(opts1)
      expect(datasource.did_supplier).to eq(DIDSupplier.find_by_symbol(:telus))
      expect(linecount).to   eq(5)
    end

    it "should import a cdr draper sample file" do

      start=Cdr.all.count
      opts1 = { file: "spec/files/cdrdrapper.csv",
                overwrite: true,
                origin:    :draper,
                verbose:   false,
              }

      linecount,durations,firstdate,datasource = Cdr.load_cdr_telus_rebiller(opts1)
      expect(datasource.did_supplier).to eq(DIDSupplier.find_by_symbol(:draper))
      expect(linecount).to   eq(5)
    end

    it "should find some tr290 files" do
      cnt = 0
      Cdr.find_legacy_cdr_files(Cdr.telus_pattern, Rails.root.join('spec','files', 'telus')) { |path, basename|
        cnt += 1
      }
      expect(cnt).to eq(373)
    end

  end

  describe "import of OBM records" do
    def obm_row1
      ["25/10/2016","5143162633   002","6133453333","","8","100","",
       "","F","613-345-3333","","","903124273333",
       "26/09/2016","08:52:00","4.0000","","","Brock","ON",
       "903124273333","","Turkey","","Regular overseas long distance",
       "","","","$22.04000","$22.04000","$24.91000","BELL CANADA",
       "0102203333   002","September 2016","2016","3:53.0","","","","","",
       "903","","","","","","","","","$2.87000","","$2.87000",
       "4:00.0","Z","","613","","","","","","","","$0.00000",
       "","","","","0","3.88","0","1","26th","Monday","","",
       "903-124","0","","","Usage","$2.87000","0","ZZ","Wireline","0",
       "613-345","","","Q3 2016","1027851","","",
       "6133453333-USAGE/UTILIS","Voice","N","",
       "Discount Overseas Rate Period","","26/09/2016","Weekday",
       "","","","","","","","","","","","","","","","","","","",
       "","","0.0000","","0.0000","","","","","","","","","","",
       "","","","","","","","","","","","","","","","","","Corporate",
       "","","","Unassigned","","","","","","","","","","","","","",
       "","","","","","","","","","","","","","","","","","Unassigned","",""]
    end

    it "should import a single obm line" do
      options   = { }
      linecount = 1
      cdr1 = Cdr.import_cdr_obm_row(options, linecount, obm_row1, datafake)
      expect(cdr1.startdatetime).to eq(Time.zone.parse('2016-09-26 08:52:00'))
      expect(cdr1.duration).to eq(3*60+53)
      expect(cdr1.call_cost).to be_within(0.01).of(22.04)
    end

    it "should import a cdr obm sample file" do
      start=Cdr.all.count
      opts1 = { file: "spec/files/cdrobm.csv",
                overwrite: true,
                verbose:   false,
              }

      number,durations,firstdate,datasource = Cdr.load_cdr_obm(opts1)
      expect(number).to   eq(4)
    end

    it "should find some obm files" do
      cnt = 0
      Cdr.find_legacy_cdr_files(Cdr.obm_pattern, Rails.root.join('spec','files', 'obm')) { |path, basename|
        cnt += 1
      }
      expect(cnt).to eq(89)
    end

  end

  describe "import of Rogers records" do
    def rogers_line
      Base64.decode64("NTE0OTk5MTIzNDQ3ODc3NzY3OTMzMyAgICAyNTA1NjM2NjI1" +
                      "VzhTVjA2NjgwMTY3UHJpbmNlIEdl"+
                      "b0JDICAgNDUwNjk2MTA5MiAgICAg"+
                      "MDQ0MjA1NjlQb250IFZpYXUgUFFYMDAwMDAzOTAwMDAwMDEw"+
                      "MDAwMDAwMDAwMDAwMDAwMDAwMDAw" +
                      "VzE4MDgyNUsgIDAwMDE3MDMyMDE2MTAxMDE4MDkxODAwMDEw" +
                      "MDAwMDAwMDAwMDA0MjJFQQ==")
    end

    it "should import a single rogers line" do
      options   = { }
      linecount = 1
      cdr1 = Cdr.import_cdr_rogers_line(options, linecount, rogers_line, datafake)
      expect(cdr1.startdatetime).to eq(Time.zone.parse('2016-10-10 18:09:18'))
      expect(cdr1.duration).to eq(100)
      expect(cdr1.call_cost).to be_nil
    end

    it "should import a cdr rogers sample file" do

      start=Cdr.all.count
      opts1 = { file: "spec/files/cdrrogers.csv",
                overwrite: true,
                verbose:   false,
              }
      number,durations,firstdate,datasource = Cdr.load_cdr_rogers(opts1)
      expect(number).to   eq(4)
    end

    it "should find some rogers files" do
      cnt = 0
      Cdr.find_legacy_cdr_files(Cdr.rogers_pattern, Rails.root.join('spec','files', 'rogers')) { |path, basename|
        cnt += 1
      }
      expect(cnt).to eq(491)
    end
  end

  describe ".international" do
    subject { Cdr }

    it "returns all international calls" do
      expect(subject.international.count).to eq(3)
    end
  end
end
