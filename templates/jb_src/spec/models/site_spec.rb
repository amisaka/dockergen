describe Site do

  # it { should validate_presence_of :name }
  # it { should validate_presence_of :site_number }

  describe "#city" do
    it { should delegate_method(:city).to(:building) }
  end

  describe "#city=" do
    it { should delegate_method(:city=).to(:building).with_arguments('foobar') }
  end

  describe "#province" do
    it { should delegate_method(:province).to(:building) }
  end

  describe "#province=" do
    it { should delegate_method(:province=).to(:building).with_arguments('foobar') }
  end

  describe "#country" do
    it { should delegate_method(:country).to(:building) }
  end

  describe "#country=" do
    it { should delegate_method(:country=).to(:building).with_arguments('foobar') }
  end

  describe "#postalcode" do
    it { should delegate_method(:postalcode).to(:building) }
  end

  describe "#postalcode=" do
    it { should delegate_method(:postalcode=).to(:building).with_arguments('foobar') }
  end

  describe "relationships" do
    it { should have_many :phones }

    it { should belong_to :building }

    # it { should have_one  :otrs_customercompany } if $OTRS
    # it { should have_many :otrs_customeruser    } if $OTRS
    # it { should belong_to :site_rep } if $OTRS

    it { should have_many :billable_services }

    it { should have_one(:btn) }
  end

  describe "life cycle" do
    context "when the site was terminated in the past" do
      subject { create(:site, activated: false )}
      it "reports the site as inactive" do
        expect(subject.active?).to be_falsey
      end
    end

    context "when the site was activated in the future" do
      subject { create(:site, activated: true, activated_at: Time.now + 1.month )}
      it "reports the site as inactive" do
        expect(subject.active?).to be_falsey
      end
    end

    context "when the sites activation dates are nil" do
      subject { create(:site)}

      it "reports the site as active" do
        expect(subject.active?).to be_truthy
      end
    end

    context "when the sites activation dates are bracketed" do
      subject { create(:site, activated_at: Time.now - 1.day, deactivated_at: Time.now + 1.day)}

      it "reports the site as active" do
        expect(subject.active?).to be_truthy
      end
    end


    context "given an active site" do
      subject { create(:site, activated: true) }

      it "marks the site as deactivated when calling `deactivate`" do
        expect{subject.deactivate}.to change(Site.activated, :count).by(-1)
        expect(subject.deactivated?).to be_truthy
        # expect(@site.site_rep.deactivated?).to be_truthy if $OTRS
      end
    end

    # NOTE: OTRS
    # it "should update the support service level from site" do
    #   ln = create(:site)
    #
    #   ln.support_service_level = '1'
    #   expect(ln.valid?).to be_truthy
    #   # expect(ln.site_rep.support_service_level).to eq('1') if $OTRS
    # end
  end

  # NOTE: extract
  # describe "import" do
  #   it "should read in a single row for a customer site, matching them on PID and site name" do
  #     example1=['9994','5144444044','Active','2008/08/25',1,"Gatineau","283 Alexandre-Tache Blvd",'Salle F3018','Gatineau','Quebec',"J9A1L8",'']
  #
  #     nv = clients(:novavision)
  #     nv.client_crm_account_code_fix
  #     before = nv.sites.count
  #     site = Site.import_row(example1)
  #     expect(site).to_not be_nil
  #     expect(site).to_not eq(:comment)
  #     expect(site).to be_a_kind_of(Site)
  #     expect(site.name).to eq('Gatineau')
  #     expect(site).to be_persisted
  #
  #     nv.reload
  #     after = nv.sites.count
  #     expect(after - before).to eq(1)
  #   end
  #
  #   it "should read in a single row of deleted customer site" do
  #     example1=['9994','5144444044','Inactive','2008/08/25',1,"Gatineau","283 Alexandre-Tache Blvd",'Salle F3018','Gatineau','Quebec',"J9A1L8",'2016-02-01']
  #
  #     nv = clients(:novavision)
  #     nv.client_crm_account_code_fix
  #     before = nv.sites.count
  #
  #     site = Site.import_row(example1)
  #     expect(site).to_not be_nil
  #     expect(site).to_not eq(:comment)
  #     expect(site).to be_terminated
  #   end
  #
  #   it "should read in a csv file full of sites, matching them on PID and site name" do
  #     file1 = "spec/files/site_example.csv"
  #
  #     nv = clients(:novavision)
  #     nv.client_crm_account_code_fix
  #     before = nv.sites.count
  #
  #     Site.import_file({ file: file1 })
  #
  #     nv.reload
  #     after = nv.sites.count
  #     expect(after - before).to eq(1)
  #   end
  #
  #   it "should read four entries with sites and get the site_number correct" do
  #     client=["310","Active","2013/09/30","4188777766","Les beau elephant d'Anjou","1991 Boul Elephant Way","","QUEBEC","Quebec","H0H 0H0",""]
  #     row1=["310","4188777766","Active","2014/02/01","2","Anjou","","","","","",""]
  #     row2=["310","4188777766","Active","2014/02/01","3","Molson","","","","","",""]
  #     row3=["310","4188777766","Active","2013/09/30","1","Quebec","","","","","",""]
  #     row4=["310","4188777766","Active","2014/04/22","4","Titan","","","","","",""]
  #
  #     nc = Client.import_row(client, 1)
  #     expect(nc).to_not be_nil
  #
  #     s1 = Site.import_row(row1);
  #     expect(s1).to_not be_nil
  #     expect(s1.name).to eq("Anjou")
  #     expect(s1.site_number).to eq("2")
  #
  #     s2 = Site.import_row(row2);
  #     expect(s2).to_not be_nil
  #     expect(s2.name).to eq("Molson")
  #     expect(s2.site_number).to eq("3")
  #
  #     s3 = Site.import_row(row3);
  #     expect(s3).to_not be_nil
  #     expect(s3.name).to eq("Quebec")
  #     expect(s3.site_number).to eq("1")
  #
  #     s4 = Site.import_row(row4);
  #     expect(s4).to_not be_nil
  #     expect(s4.name).to eq("Titan")
  #     expect(s4.site_number).to eq("4")
  #   end
  # end

  # NOTE: extract
  # describe "merging" do
  #   it "pick a site with fewer phones and fold it into one with more" do
  #     site1 = create(:site)
  #     site2 = create(:site)
  #     Site.mergesites(site1, site2) {}
  #     expect(site1.alt_name).to eq(site2.name)
  #     expect(site2).to be_terminated
  #   end
  #
  #   it "merge two sites, keeping the old name as an altname" do
  #     site1 = create(:site)
  #     site2 = create(:site)
  #
  #     site1.merge_from(site2)
  #     expect(site1.alt_name).to eq(site2.name)
  #     expect(site2).to be_terminated
  #    end
  # end

  # NOTE: remove this, don't create relations like this
  # describe "before_save" do
  #   context "has an associated building" do
  #     before { @site = create(:site) }
  #
  #     it "doesn't create an associated building" do
  #       expect{@site.save}.not_to change(Building, :count)
  #     end
  #   end
  #
  #   context "doesn't have an associated building" do
  #     subject { build(:site, building: nil) }
  #
  #     it "creates an associated building" do
  #       subject
  #       expect{subject.save}.to change(Building, :count).by(1)
  #       expect(subject.building).not_to be nil
  #     end
  #   end
  # end

end
