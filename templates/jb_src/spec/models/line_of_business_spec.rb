describe LineOfBusiness do
  it { should have_many :clients }
  it { should have_many :client_line_of_businesses }
end
