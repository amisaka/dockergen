# coding: utf-8
module Hakku
  if $HAKKU
    describe Service, type: :model do
      describe "loading" do
        it "load a base service" do
          row1 = [
            "1","","1191235555","5","119-123-5555 1-1","1234-5678 Québec Inc (Mul",
            "","1","","9218","530","","2","","1","2","2014-03-09 14:22:24.580",
            "2014-02-21 00:00:00.000","","","0","0","","0","","","1","","2065",
            "","","","","","","0","1","","0","","0","1","" ]
          ir = Hakku::Service.import_row(row1)
          expect(ir.ServicePID).to eq(1)
          expect(ir.ID).to         eq("1191235555")
          expect(ir.CustomerLineOfBusinessPID).to   eq(9218)
          expect(ir.CreatedOn).to  eq('2014-3-9 14:22:24.580'.to_datetime)
        end

        it "load a service with description with one comma" do
          row2 = [
            "9","","2015550149","5","201-555-0149"," Conf bridge","NovaVision",
            "","1","","49","532","","2","","1","2","2014-03-09 14:22:24.737",
            "2012-02-01 00:00:00.000","","","0","0","","0","","","1","",
            "2073","","","","","","","0","1","","0","","0","1",""          ]
          ir = Hakku::Service.import_row(row2)
          expect(ir.ServicePID).to eq(9)
          expect(ir.ID).to         eq("2015550149")
          expect(ir.CustomerLineOfBusinessPID).to   eq(49)
          expect(ir.CreatedOn).to  eq('2014-03-09 14:22:24.737'.to_datetime)
        end

        it "load a service with repeated Display/Description columns" do
          row3 = [
            "10591","","2155550134","5","2155550134"," 31 Elvis Presley","2155550134",
            " 31 Elvis Presley","","1","","9702","1719","","2","","1","2",
            "2014-11-05 16:16:16.987","2014-10-31 00:00:00.000","","","0","1",
            "","0","","","5","5","22929","","","","","","","0","5","","0","","0","1",""
          ]
          ir = Hakku::Service.import_row(row3)
          expect(ir.ServicePID).to eq(10591)
          expect(ir.ID).to         eq("2155550134")
          expect(ir.Description).to  eq(" 31 Elvis Presley")
          expect(ir.CustomerLineOfBusinessPID).to   eq(9702)
          expect(ir.CreatedOn).to  eq('2014-11-05 16:16:16.987'.to_datetime)
        end

        it "should load three services from a CSV file" do
          file = "spec/files/legacy_service1.csv"
          Hakku::Service.import_file(file)

          ir = Hakku::Service.find(1)
          expect(ir.ServicePID).to eq(1)
          expect(ir.ID).to         eq("1191235555")
          expect(ir.CustomerLineOfBusinessPID).to   eq(9218)
          expect(ir.CreatedOn).to  eq('2014-3-9 14:22:24.580'.to_datetime)

          ir = Hakku::Service.find(9)
          expect(ir.ID).to         eq("2015550149")
          expect(ir.CustomerLineOfBusinessPID).to   eq(49)
          expect(ir.CreatedOn).to  eq('2014-03-09 14:22:24.737'.to_datetime)

          ir = Hakku::Service.find(10591)
          expect(ir.ID).to         eq("2155550134")
          expect(ir.Description).to  eq(" 31 Elvis Presley")
          expect(ir.CustomerLineOfBusinessPID).to   eq(9702)
          expect(ir.CreatedOn).to  eq('2014-11-05 16:16:16.987'.to_datetime)
        end
      end
    end
  end
end
