# coding: utf-8
module Hakku
  if $HAKKU
    describe Customer, type: :model do
      describe "loading" do
        it "load a simpler customer" do
          row1 = [
            "1","4181996040","Pierre Trudeau","","","","","","","4","2014-03-09 14:19:54.673",
            "2008-08-25 00:00:00.000","","2","","1","Français","1","1","0","1","0","","",
            "","","","","","","","","","","2014-02-01 00:00:00.000","1","","1","3",
            "2014-03-14 14:59:54.063","2","0","260","" ]
          expect(row1.count).to eq(44)
          ir = Hakku::Customer.import_row(row1)
          expect(ir.CustomerPID).to eq(1)
          expect(ir.Code).to       eq("4181996040")
          expect(ir.BillingCyclePID).to   eq(4)
          expect(ir.StartDate).to         eq(Time.zone.parse("2008-Aug-25"))
          expect(ir.AccountPID).to eq(260)
        end

        it "load a customer with comma in name" do
          row1 = [
            "286","4181997785","Ernie and Bert"," sesame street","","","",
            "","","","7","2014-03-09 14:20:07.020","2013-11-06 00:00:00.000",
            "2015-03-01 00:00:00.000","5","","1","Français","286","1","0","286",
            "0","","","","","","","","","","","","","2014-02-01 00:00:00.000",
            "1","","1","6","2014-11-17 18:22:34.127","1","0","","" ]
          expect(row1.count).to eq(45)
          ir = Hakku::Customer.import_row(row1)
          expect(ir.CustomerPID).to eq(286)
          expect(ir.Code).to       eq("4181997785")
          expect(ir.BillingCyclePID).to   eq(7)
          expect(ir.CreatedOn).to         eq(Time.zone.parse("2014-Mar-09 14:20:07.020"))
          expect(ir.AccountPID).to eq(nil)
        end

        it "load a customer with multiple commas in name" do
          row1 = [
            "1752","4501992576","Crimson"," Permanent"," Assurance",
            "","","","","","","8", "2014-11-13 10:08:27.527", "2005-02-15 00:00:00.000",
            "","2","","1","Français","1752","1","0","23169","0","","","","",
            "","","","","","","","","2014-10-01 00:00:00.000","1","","1","6",
            "2014-12-03 15:41:13.897","12","0","1743",""
          ]
          expect(row1.count).to eq(46)
          ir = Hakku::Customer.import_row(row1)
          expect(ir.CustomerPID).to eq(1752)
          expect(ir.Code).to       eq("4501992576")
          expect(ir.BillingCyclePID).to   eq(8)
          expect(ir.CreatedOn).to  eq(Time.zone.parse("2014-11-13 10:08:27.527"))
          expect(ir.AccountPID).to eq(1743)
        end

        it "should load two customer from a CSV file" do
          file = "spec/files/legacy_customer1.csv"
          Hakku::Customer.all.delete_all
          Hakku::Customer.import_file(file)

          ir = Hakku::Customer.find(1)
          expect(ir.CustomerPID).to eq(1)
          expect(ir.Code).to       eq("4181996040")
          expect(ir.BillingCyclePID).to   eq(4)
          expect(ir.StartDate).to  eq(Time.zone.parse("2008-Aug-25 00:00:00"))

          ir = Hakku::Customer.find(286)
          expect(ir.CustomerPID).to eq(286)
          expect(ir.Code).to       eq("4181997785")
          expect(ir.BillingCyclePID).to eq(7)
          expect(ir.CreatedOn).to  eq(Time.zone.parse("2014-Mar-09 14:20:07.020"))
        end
      end
    end
  end
end
