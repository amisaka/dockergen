# coding: utf-8
module Hakku
  if $HAKKU
    describe Product, type: :model do
      describe "loading" do
        it "load a base product" do
          row1 = [ "1","Base Charge","","1","0",".0000","","","1",
                   "1","1","0","","0","","","2","",
                   "2014-03-09 14:01:04.453","","","","","Base Charge            " ]
          ir = Hakku::Product.import_row(row1)
          expect(ir.ProductPID).to eq(1)
          expect(ir.Code).to       eq("Base Charge")
          expect(ir.UnitCost).to   eq("")
        end

        it "load a product with description with one comma" do
          row2 = [
            "196","82135B-0003-S","Fiber optics 10M best-effort 8  IP",
            " band 0 regional 5 year contract","1","0","375.0000",
            ".0000","3","8","4","6","0","16","0","1","","1","6",
            "2014-03-09 14:07:12.970","2015-05-27 19:36:50.133","","","186","82135B-0003-S"
          ]
          ir = Hakku::Product.import_row(row2)
          expect(ir.ProductPID).to eq(196)
          expect(ir.Description).to eq("Fiber optics 10M best-effort 8  IP, band 0 regional 5 year contract")
          expect(ir.Amount).to      eq("375.0000")
        end

        it "load a product with description with two comma" do
          row3 = [
            "350","20001B-0003-S","ADSL dedicated HSA internet 840K/6M","8 IP","1 year contract","1","0","194.9500",".0000","3","8","4","6","0","16","0","1","","1","","2014-03-09 14:13:37.867","","","","245","20001B-0003-S"
          ]
          ir = Hakku::Product.import_row(row3)
          expect(ir.ProductPID).to eq(350)
          expect(ir.Description).to eq("ADSL dedicated HSA internet 840K/6M,8 IP,1 year contract")
          expect(ir.Amount).to      eq("194.9500")
        end

        it "should load three products from a CSV file" do
          file = "spec/files/legacy_product1.csv"
          Hakku::Product.import_file(file)

          ir = Hakku::Product.find(350)
          expect(ir.ProductPID).to eq(350)
          expect(ir.Description).to eq("ADSL dedicated HSA internet 840K/6M,8 IP,1 year contract")
          expect(ir.Amount).to      eq("194.9500")

          ir = Hakku::Product.find(1082)
          expect(ir.ProductPID).to eq(1082)
          expect(ir.Description).to eq("Fibre 100Mbps, modulable 8 IP, bande 0,, Métro contrat 5 ans")
          expect(ir.Amount).to      eq("450.0000")
        end
      end
    end
  end
end
