# coding: utf-8
module Hakku
  if $HAKKU
    describe Department, type: :model do
      describe "loading" do
        it "load a base department" do
          row1 = [
            "1","0","1","PlaceOne","","395","","1","5","2014-03-09 14:22:13.470",
            "2011-10-07 00:00:00.000","2017-04-01 00:00:00.000","","1","","0",
            "1651","","","","","","0","","","0","","1","","1","","","1",""
          ]
          ir = Hakku::Department.import_row(row1)
          expect(ir.DepartmentPID).to eq(1)
          expect(ir.Code).to       eq("1")
          expect(ir.DepartmentName).to eq("PlaceOne")
        end

        it "load a department with description with one comma" do
          row2 = [
            "78","0","2","9060"," CommaOne","","1058","","1","2",
            "2014-03-09 14:22:13.877","2007-01-01 00:00:00.000","","","1","","0",
            "1728","","","","","","0","","","0","","1","","1","","","0",""
          ]
          ir = Hakku::Department.import_row(row2)
          expect(ir.DepartmentPID).to eq(78)
          expect(ir.Code).to          eq("2")
          expect(ir.DepartmentName).to eq("9060, CommaOne")
        end

        it "load a department with description with two comma" do
          row3 = [
            "212","0","ST-LAMBERT","110"," boul Mt-Rose"," St-Lam","",
            "1192","","1","2","2014-03-09 14:22:14.457","2011-09-01 00:00:00.000",
            "","","1","","0","1862","","","","","","0","","","0","","1","","1","","","0",""
          ]
          ir = Hakku::Department.import_row(row3)
          expect(ir.DepartmentPID).to eq(212)
          expect(ir.Code).to          eq("ST-LAMBERT")
          expect(ir.DepartmentName).to eq("110, boul Mt-Rose, St-Lam")
        end

        it "should load three department from a CSV file" do
          file = "spec/files/legacy_department1.csv"
          Hakku::Department.import_file(file)

          ir = Hakku::Department.find(78)
          expect(ir.DepartmentPID).to eq(78)
          expect(ir.Code).to          eq("2")
          expect(ir.DepartmentName).to eq("9060, CommaOne")

          ir = Hakku::Department.find(212)
          expect(ir.DepartmentPID).to eq(212)
          expect(ir.Code).to          eq("ST-LAMBERT")
          expect(ir.DepartmentName).to eq("110, boul Mt-Rose, St-Lam")
        end
      end
    end
  end
end
