# coding: utf-8
module Hakku
  if $HAKKU
    describe ChargeAssignment, type: :model do
      describe "loading" do
        it "load a simple relationship" do
          Time.zone = "UTC"
          row1 = [
            "1","753","3","0","2014-02-21 00:00:00.000","2014-02-21 00:00:00.000","1.00",".7500","2","","","2","1","2014-03-31 00:00:00.000","","","","","","530","","1","","911 service VoIP - monthly access fee","0","","1","","1","2","2014-03-09 14:27:56.097","","","","3","2014-04-01 00:00:00.000","2014-03-31 00:00:00.000","1697","","","",".7500","","","","1"
          ]
          expect(row1.count).to eq(46)
          ir = Hakku::ChargeAssignment.import_row(row1)
          expect(ir.ChargeAssignmentPID).to eq(1)
          expect(ir.ProductPID).to          eq(753)
          expect(ir.Quantity).to            eq(1)
          expect(ir.CustomerPID).to         eq(530)
          expect(ir.CreatedOn).to           eq(Time.zone.parse("2014-3-9 14:27:56.097"))
        end

        it "load a customer with commas in descriptions" do
          Time.zone = "UTC"
          row1 = [
            "5","523","3","0","2013-03-07 00:00:00.000","2014-03-01 00:00:00.000","1.00","44.9500","2","","","2","5","2014-03-31 00:00:00.000","","","","","","531","","2","","Internet cable modem"," 10M download / 1M upload"," 100GB transfer"," 60 month contract","0","","1","","1","2","2014-03-09 14:27:56.150","","","","2","2014-04-01 00:00:00.000","2014-03-31 00:00:00.000","1698","","","","44.9500","","","","1"
          ]
          expect(row1.count).to eq(49)
          ir = Hakku::ChargeAssignment.import_row(row1)
          expect(ir.ChargeAssignmentPID).to eq(5)
          expect(ir.ProductPID).to          eq(523)
          expect(ir.Quantity).to            eq(1)
          expect(ir.CustomerPID).to         eq(531)
          expect(ir.CreatedOn).to           eq(Time.zone.parse("2014-3-9 14:27:56.150"))
        end

        it "load a billable service with commas in memo" do
          Time.zone = "UTC"
          row1 = [
            "22489","1","1","0","2014-11-16 18:40:41.310","2014-10-31 00:00:00.000",
            "1.00","5.0000","1","","","1","","","","","","","","1739","","","",
            "Frais Minium $5.00 - Frais Minium $5.00","0","","","","2","2",
            "2014-11-17 00:40:41.307","","1","15265","0","","","","","","","5.0000","","","","1"
          ]
          expect(row1.count).to eq(46)
          ir = Hakku::ChargeAssignment.import_row(row1)
          expect(ir.ChargeAssignmentPID).to eq(22489)
          expect(ir.ProductPID).to          eq(1)
          expect(ir.Description).to         eq("Frais Minium $5.00 - Frais Minium $5.00")
          expect(ir.Memo).to                eq("")
          expect(ir.Quantity).to            eq(1)
          expect(ir.CustomerPID).to         eq(1739)
          expect(ir.CreatedOn).to           eq(Time.zone.parse("2014-11-17 00:40:41.307"))
        end



        it "should load three customer from a CSV file" do
          file = "spec/files/legacy_chargeassignments1.csv"
          Time.zone = "UTC"
          Hakku::ChargeAssignment.all.delete_all
          Hakku::ChargeAssignment.import_file(file)

          ir = Hakku::ChargeAssignment.find(1)
          expect(ir.ChargeAssignmentPID).to eq(1)
          expect(ir.ProductPID).to          eq(753)
          expect(ir.Quantity).to            eq(1)
          expect(ir.CustomerPID).to         eq(530)
          expect(ir.CreatedOn).to           eq(Time.zone.parse("2014-3-9 14:27:56.097"))

          ir = Hakku::ChargeAssignment.find(5)
          expect(ir.ChargeAssignmentPID).to eq(5)
          expect(ir.ProductPID).to          eq(523)
          expect(ir.Quantity).to            eq(1)
          expect(ir.CustomerPID).to         eq(531)
          expect(ir.CreatedOn).to           eq(Time.zone.parse("2014-3-9 14:27:56.150"))
        end
      end
    end
  end
end
