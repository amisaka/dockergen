require 'rails_helper'

describe OTRS::CustomerUser do
  if $OTRS

    it "should belong to a specific site vis customer company" do
      slater = OTRS::CustomerUser.find(111)

      credil = slater.otrs_customercompany
      expect( credil ).to_not be_nil
      expect( credil.site ).to_not be_nil
    end

    it "should be a rep for a specific site" do
      slater = OTRS::CustomerUser.find(111)

      expect( slater.sites.count ).to be > 0
    end
  end
end
