require 'rails_helper'

RSpec.describe Lerg8, :type => :model do
  describe "province" do
    it "should translate PQ to QC" do
      expect(LergMethods::cleanProvince("QC")).to eq("QC")
      expect(LergMethods::cleanProvince("PQ")).to eq("QC")
    end
  end

  describe "importing" do
    it "should find newest ZIP file" do
      # this script sets up some tests into tmp, which need
      # to have correct dates.
      system(Rails.root.join("bin/telecordiatestdir").to_s)
      tmpTelecordiaDataDir = Rails.root.join("tmp", "telecordia")

      lerg8file = LergMethods::latestLerg8ZipFile(tmpTelecordiaDataDir)
      expect(File.basename(lerg8file)).to eq("LERGDATA0616.ZIP")
    end

    it "should extract LERG8.DAT file from ZIP file" do
      # this script sets up some tests into tmp, which need
      # to have correct dates.
      tmpTelecordiaDataDir = Rails.root.join("spec", "files", "telecordia")

      lerg8fileA = LergMethods::latestLerg8ZipFile(tmpTelecordiaDataDir)
      expect(File.basename(lerg8fileA)).to eq("LERGDATA0616.ZIP")

      lerg8file,tmpdir = LergMethods::findlerg8(lerg8fileA)
      expect(File.exists?(lerg8file)).to be true

      File.unlink(lerg8file)
      FileUtils.rmdir(tmpdir) if tmpdir
    end

    it "should read a line from lerg8 file" do
      line1="888  CANADA                     NBALBERT     ALBERT                                            0320601009          506                               N                      "

      l1 = Lerg8.import_row(line1)
      expect(l1.shortName).to eq("ALBERT")
      expect(l1.fullName).to eq("ALBERT")
      expect(l1.province).to eq("NB")
    end

    it "should read LERG8 file" do
      tmpTelecordiaDataDir = Rails.root.join("spec", "files", "telecordia")

      lerg8file = LergMethods::latestLerg8ZipFile(tmpTelecordiaDataDir)
      expect(File.basename(lerg8file)).to eq("LERGDATA0616.ZIP")

      lerg8file,tmpdir = LergMethods::findlerg8(lerg8file)
      count = Lerg8.import_file(lerg8file)

      expect(count).to eq(100)
    end
  end
end
