# coding: utf-8
require 'rails_helper'

RSpec.describe RatePlan, :type => :model do

  describe "relations" do
    it "should have connection to btn" do
      rp1 = rate_plans(:quebec)
      expect(rp1.btns.count).to be > 0
    end

    it "should have associated with it some rates" do
      rc1 = rate_plans(:world_silver)
      expect(rc1.rates.count).to be >= 1
    end

    it "should have an association to a carrier" do
      expect(rate_plans(:quebec).carrier).to eq(carriers(:Thinktel))
    end
  end

  describe "default plan" do
    it "should be found by name" do
      expect(RatePlan.default.province).to eq('QC')
    end
  end

  describe "evaluation" do
    it "should return true if rate center is marked as toll free" do
      montreal = local_exchanges(:le527)
      expect(montreal.name).to eq("MONTRÉAL")
      quebecplan = rate_plans(:quebec)
      expect(quebecplan.acceptable_rate_center(montreal.rate_center)).to be_truthy
    end

    it "should return false if rate center is not within rate plan because it is off-net" do
      alexandria   = local_exchanges(:le13)
      expect(alexandria.name).to eq("ALEXANDRIA")
      quebecplan = rate_plans(:quebec)
      expect(quebecplan.acceptable_rate_center(alexandria.rate_center)).to be_falsy
    end

    it "should return false if rate center is within the On-Net part of the provincial rate plan" do
      arnoldcove     = local_exchanges(:le2853)
      expect(arnoldcove.name).to eq("ARNOLD'S COVE")
      quebecplan = rate_plans(:quebec)
      expect(quebecplan.acceptable_rate_center(arnoldcove.rate_center)).to be_falsy
    end

    it "should return true if rate center is within the On-Net part of the national rate plan" do
      arnoldcove     = local_exchanges(:le2853)
      expect(arnoldcove.name).to eq("ARNOLD'S COVE")
      nationalplan = rate_plans(:national)
      expect(nationalplan.acceptable_rate_center(arnoldcove.rate_center)).to be_truthy
    end
  end

end
