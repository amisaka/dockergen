require "cancan/matchers"

describe "Ability" do
  # describe "as guest html" do
  #   before(:each) do
  #     @ability = Ability.new(nil, 'html')
  #   end
  #
  #   it "can only not see any data" do
  #     # Define what a guest can and cannot do
  #     expect(@ability).to_not be_able_to(:update, :users)
  #   end
  # end
  #
  # describe "as guest json" do
  #   before(:each) do
  #     @ability = Ability.new(nil, 'json')
  #   end
  #
  #   it "can only not see any data" do
  #     # Define what a guest can and cannot do
  #     expect(@ability).to_not be_able_to(:update, :users)
  #   end
  # end
  #
  # describe "as user html" do
  #   before(:each) do
  #     @user    = create(:user)
  #     @user2   = create(:user)
  #     @ability = Ability.new(@user, 'html')
  #   end
  #
  #   it "can only see its own data" do
  #     # Define what a guest can and cannot do
  #     expect(@ability).to be_able_to(:update, @user)
  #     expect(@ability).to_not be_able_to(:update, @user2)
  #   end
  # end
  #
  # # there must be a better way to repeat these tests with different preconditions?
  # describe "as user with nil format" do
  #   before(:each) do
  #     @user    = create(:user)
  #     @user2   = create(:user)
  #     @ability = Ability.new(@user, nil)
  #   end
  #
  #   it "can only see its own data" do
  #     # Define what a guest can and cannot do
  #     expect(@ability).to be_able_to(:update, @user)
  #     expect(@ability).to_not be_able_to(:update, @user2)
  #   end
  # end
end
