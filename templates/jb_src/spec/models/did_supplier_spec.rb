require 'rails_helper'

RSpec.describe DIDSupplier, type: :model do
  describe "relations" do
    it { should have_many :phones }
    it { should have_many :datasources }
  end

end
