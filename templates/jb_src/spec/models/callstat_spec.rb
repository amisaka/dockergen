require 'rails_helper'

describe Callstat do

  it "should allocate some sequence numbers for compute server" do
    one = Callstat.create
    two = Callstat.create
    expect(two.id).to eq(one.id + 1)
    numbers = Callstat.allocate_id(1024)
    expect(numbers).to eq(two.id + 1)

    three = Callstat.create
    expect(three.id).to eq(numbers + 1024 + 1)
  end

  describe "relationships" do
    it "should have a client" do
      st = create(:callstat)
      expect( st.client ).to_not be_nil
    end

    it "should get found by date from a client" do
      st = create(:callstat)
      sp1= create(:stats_period)
      nv = create(:client)
      n = nv.callstats.today(sp1.day)
      expect(n.first).to eq(sp1.callstats.first)
    end

    it "should find a callstat by stats period" do
      nv = create(:client)
      sp1= create(:stats_period)
      stat1= create(:callstat)

      n = nv.callstats.find_or_create_for_today(sp1.day, nil)
      expect(n.first).to eq(stat1)
    end

    it "should count number of records for a day" do
      nv = create(:client)
      sp1= create(:stats_period)

      expect(nv.callstats.today(sp1.day).count).to be > 0
    end

    it "could have a relationship to phone" do
      cs1 = Callstat.new
      nv1 = create(:phone)
      cs1.phone = nv1
    end

  end

  describe "polymorphism" do
    it "should have a type daily" do
      st = create(:callstat)
      expect(st.class).to eq(CallCallstat)
    end
  end

  describe "calculating" do
    it "should return a scope for a today" do
      # this makes sure that we are not doing the actual query!
      nv = create(:client)
      day = '2012-05-19'.to_date
      expect(nv.calls.today(day,day).class).to eq(Call::ActiveRecord_AssociationRelation)
    end

    it "should get zero calculated for 2012-05-19" do
      nv = create(:client)
      day = '2012-05-19'.to_date
      cs1 = nv.callstats.find_or_create_for_today(day)
      expect( cs1 ).to_not be_nil
      expect( cs1.first ).to_not be_nil

      cs1.first.do_counts

      assert_equal 0, cs1.first.value
    end

    it "should calculate 1 for 2012-10-05" do
      nv = create(:client)
      day = '2012-10-05'.to_date
      cs1 = nv.callstats.find_or_create_for_today(day)
      expect( cs1 ).to_not be_nil
      expect( cs1.first ).to_not be_nil

      cs1.first.do_counts

      assert_equal 2, cs1.first.value
    end

    it "should generate a query to find the maximum number of calls" do
      nv = create(:client)
      y2012 = '2012-01-01'.to_date
      sp  = StatsPeriod.new(:type => 'YearlyStatsPeriod', :day => y2012)
      sp.save!     # should have an id for below.
      cs1 = nv.callstats.find_all_or_create_for_statsperiod(sp, 'PeakCallCountStat')
      expect(cs1.size).to eq(1)
      cs = cs1.first
      expect(cs.count).to eq(104)
    end

    it "should generate a query to find the number of voicemail calls" do
      nv = create(:client)
      nv_call = create(:call)

      nv_call.calculate_voicemail!
      nv_call.save!

      nv_call_day  = nv_call.startdatetime.to_date
      nv_call_hour = nv_call.startdatetime.hour - 4  # local time.

      sp  = StatsPeriod.new(:type => 'HourlyStatsPeriod',
			    :hour => nv_call_hour,
			    :day  => nv_call_day)
      sp.save!     # should have an id for below.

      cs1 = nv.callstats.find_all_or_create_for_statsperiod(sp, 'VoiceMailCallstat')
      expect(cs1.size).to eq(1)
      cs = cs1.first
      expect(cs.count).to eq(1)
    end

    it "should calculate a call into the correct Callstat based upon the call category" do
      sp = create(:stats_period)
      sp.callstats.delete_all          # clear things out?

      c1 = create(:call)
      x201  = create(:phone)
      c1.calc_all!
      c1.update_stats(sp, false)  # false - do not notify creating new records
      expect(x201.didin_call_count(sp.day, sp.hour, false, true, false)).to eq(1)
    end

    it "should generate call stats records for this client (but only once)" do
      nv = create(:client)
      nv_call = create(:call)
      nv_call_day  = nv_call.startdatetime.to_date
      nv_call_hour = nil

      before = Callstat.count
      nv.gen_daily_stats(nv_call_day)
      after  = Callstat.count
      expect(after).to be > before

      # if we run it again, it should not generate additional records
      before = after
      nv.gen_daily_stats(nv_call_day)
      after  = Callstat.count
      expect(after).to eq(before)
    end

  end

end
