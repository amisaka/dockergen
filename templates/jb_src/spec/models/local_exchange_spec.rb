require 'rails_helper'

describe LocalExchange do

  describe "relations" do
    it "should have connection to north_american_exchanges" do
      le4 = local_exchanges(:le4)
      expect(le4.north_american_exchanges.count).to be > 1
    end

    it "should have a connection to rate centers" do
      le4 = local_exchanges(:le4)
      expect(le4.rate_center).to_not be_nil
    end
  end

  describe "active" do
    it "should be active if not deleted" do
      le4 = local_exchanges(:le4)
      expect(le4).to be_active
    end

    it "should be inactive if marked deleted" do
      le19 = local_exchanges(:le19)
      expect(le19).to be_inactive
    end

    it "should be active if not deleted" do
      le19 = local_exchanges(:le19)
      expect(LocalExchange.active.where(id: le19.id).take).to be_nil
    end
  end

  describe "import" do
    it "should ignore header" do
      rowh = ['Exchange','Province','LIR','LIR','Name','GroupID','Note']
      stats = Hash.new(0)
      le = LocalExchange.import_row('provider1', rowh, true, stats)
      expect(stats[:header]).to eq(1)
    end

    def row1
      ['Blenheim','ON','Bell-LIR-ON01','Chatham',431]
    end
    def row2
      ['Bothwell','ON','Bell-LIR-ON01','Chatham',425]
    end

    def alma_qc
      ['ALMA', 'QC', 'Bell-LIR-QC02','Alma', 433]
    end

    it "should import a row from an array" do
      stats = Hash.new(0)
      le = LocalExchange.import_row('provider1', row1, false, stats)

      expect(le).to_not be_nil
      expect(le.name).to eq('BLENHEIM')
    end

    it "should mark an entry as not-deleted" do
      stats = Hash.new(0)
      alma = local_exchanges(:le19)
      expect(alma).to be_inactive

      le = LocalExchange.import_row('Comwave', alma_qc, false, stats)

      expect(le).to be_active
    end

    it "should import a file with TAB delimiters" do
      stats = Hash.new(0)
      filename = Rails.root.join("spec","files","Thinktel_OnNetFootprint_20140721_v1.21.csv")

      LocalExchange.import_file(filename, 'Thinktel', "\t", false, stats)
      expect(stats[:header]).to    eq(1)
      expect(stats[:line]).to     eq(1017)
      expect(stats[:created]).to   eq(1013)
      expect(stats[:unchanged]).to eq(3)
    end

    it "should import a file with CSV delimiters" do
      stats = Hash.new(0)
      filename = Rails.root.join("spec","files","think1test.csv")

      LocalExchange.import_file(filename, 'Thinktel', ',', false, stats)
      expect(stats[:header]).to    eq(1)
      expect(stats[:line]).to     eq(10)
      expect(stats[:created]).to   eq(9)
      expect(stats[:unchanged]).to eq(0)
    end

    it "should import a Bell format row" do
      row = [403972,"ACADIA VLY","Acadia Valley","AB","ABAcadia",403,972,"NOT AVAILABLE","ON-NET","NOT PLANNED AT THIS TIME"]
      stats = Hash.new(0)
      lae = LocalExchange.import_bell_row('Bell', row, false, stats)
      expect(lae.rate_center).to eq(rate_centers(:rc605))
    end

    it "should import a Bell format file" do
      filename = Rails.root.join("spec","files","BellSIPfootprint.csv")

      stats = Hash.new(0)
      LocalExchange.import_file(filename, 'Bell', ',', false, stats)
      expect(stats[:header]).to    eq(1)
      expect(stats[:line]).to     eq(20)
      expect(stats[:created]).to   eq(19)
      expect(stats[:unchanged]).to eq(0)
    end

    it "should discover the correct file" do
      system(Rails.root.join("bin/fakefootprintfile").to_s)

      tmpFakeFootPrintDir = Rails.root.join("tmp", "thinktel")
      path,timdir,workfile = LocalExchange.latestFootPrint(tmpFakeFootPrintDir)
      expect(path).to match(/2016-08-05.csv.txt/)
    end

    it "should import a new-style Footprint" do
      row2a = ['Name','Porting','New Numbers','Available','Network Type']
      row2b = ["70 Mile House, BC",'TRUE','FALSE','SIP']

      stats = Hash.new(0)
      le = LocalExchange.import_row('provider1', row2a, true, stats)
      expect(stats[:header]).to eq(1)
      expect(stats[:name_province]).to eq(true)

      le = LocalExchange.import_row('provider1', row2b, true, stats)
      expect(le.name).to eq('70 MILE HOUSE')
      expect(le.province).to eq('BC')
      expect(le.rate_center).to eq(rate_centers(:seventymilehouse))
    end

  end

  describe "footprint" do
    it "should extend relationship using rate center" do
      stats = Hash.new(0)
      count = LocalExchange.extend_footprint("TestTel", stats)

      expect(stats[:new_local_exgh]).to eq(7)
      expect(stats[:plain_rc]).to eq(3)
    end

    it "should add a new item, and connect it to an existing rate center" do
      le = LocalExchange.create(carrier: 'TestTel',
                                carrierplan_id: 2,
                                name: 'FORT MACLEOD',
                                province: 'AB')
      le.connect_to_rate_center
      expect(le.rate_center).to_not be_nil

      #le.rate_center.try(:connect_to_nae)
      #le.connect_to_na
    end

    it "should add a new item, and connect it to an existing rate center" do
      le = LocalExchange.create(carrier: 'TestTel',
                                carrierplan_id: 2,
                                name: 'FORT MACLEOD',
                                province: 'AB')
      le.connect_to_rate_center
      expect(le.rate_center).to_not be_nil

      #le.rate_center.try(:connect_to_nae)
      #le.connect_to_na
    end

  end


end
