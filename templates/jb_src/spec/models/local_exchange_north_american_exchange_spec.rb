require 'rails_helper'

describe LocalExchangesNorthAmericanExchange do

  describe "relations" do
    it "should have connection to north_american_exchanges" do
      lenae25417 = local_exchanges_north_american_exchanges(:lenae25417)
      expect(lenae25417.north_american_exchange).to_not be_nil
      expect(lenae25417.local_exchange).to_not be_nil
    end

    it "should have a connection to rate center" do
      lenae14097 = local_exchanges_north_american_exchanges(:lenae14097)
      expect(lenae14097.rate_center).to_not be_nil
    end

    it "should permit a new relationship to be created" do
      newle3344 = local_exchanges(:newle3344)
      nae13035  = north_american_exchanges(:nae13035)
      n1 = LocalExchangesNorthAmericanExchange.connect_le_nae(newle3344,nae13035)
      expect(n1).to_not be_nil
    end

    it "should not duplicate an existing relationship" do
      lenae25417 = local_exchanges_north_american_exchanges(:lenae25417)
      le = lenae25417.local_exchange
      nae= lenae25417.north_american_exchange

      n2,rc,created = LocalExchangesNorthAmericanExchange.connect_le_nae_create(le,nae)
      expect(n2).to_not be_nil
      expect(created).to be false
      expect(lenae25417.id).to eq(n2.id)
    end
  end


end
