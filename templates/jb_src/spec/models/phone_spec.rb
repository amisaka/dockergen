describe Phone do

  it { should have_many :billable_services }

  describe "with extensions" do
    it "should show extension when non-blank" do
      p1 = create(:phone, phone_number: "6136930684", extension: "3501")
      expect(p1.name).to eq("6136930684")
    end

    it "should omit extension when blank" do
      p1 = create(:phone, phone_number: "6136930684", extension: nil)
      expect(p1.name).to eq("6136930684")
    end
  end

  it "should assume btn value if callerid nil" do
    phone1 = create(:phone, callerid: nil, btn: create(:btn))
    expect(phone1[:callerid]).to be_nil
    expect(phone1.callerid).to eq(phone1.btn.billingtelephonenumber)
  end

  describe "activated" do
    it "should have a phone activated if not terminated" do
      p1 = create(:phone, activated: true)
      expect(p1).not_to be_terminated
    end

    it "should include out of service if terminated" do
      p1 = create(:phone, activated: false)
      expect(Phone.deactivated).to include(p1)
    end

    # TODO: update this
    # it "should clean up duplicate out of service entries" do
    #   p1 = create(:phone)
    #   expect(Phone.deactivated.where(phone_number: p1.phone_number, extension: p1.extension).count).to eq(2)
    #
    #   Phone.remove_duplicate_deleted!
    #
    #   expect(Phone.deactivated.where(phone_number: p1.phone_number, extension: p1.extension).count).to eq(1)
    # end
  end

  describe "relations" do
    it { should have_many :callstats }

    it { should belong_to :rate_center }

    # NOTE: this should be tested on btn
    # it "should become associated to a rate center via btn" do
    #   nv1 = create(:phone)
    #   expect(nv1.find_rate_center).to_not be_nil
    # end

    it { should belong_to :site }

    it "should have an associated client item" do
      phone1 = create(:phone)
      expect(phone1.site.client).to_not be_nil
    end

    it { should belong_to :btn }

    it { should have_many :outgoing_calls }
    it { should have_many :incoming_calls }

    it { should belong_to :did_supplier }

    it { should belong_to :local_rate_plan }
    it { should belong_to :national_rate_plan }
    it { should belong_to :world_rate_plan }
  end

  it "should return existing phone number, ignoring dash" do
    phone1 = create(:phone, btn: create(:btn, client: create(:client)))
    phone1 = Phone.find_or_make(phone1.btn.client, phone1.btn, '5146531234', nil)
    client = phone1.btn.client

    ph2 = Phone.find_or_make(phone1.btn.client, phone1.btn, '514-653-1234', nil)
    assert_equal phone1, ph2
  end

  it "should canonify a phone number" do
    expect(Phone.phonecanonify("750-596-8666")).to eq("7505968666")
    #expect(Phone.vdidify("750-596-8666")).to       eq("+17505968666")
  end

  it "should create a new phone with last-resort btn" do
    wrongnum = '6135559910'
    $BtnOfLastResortId  = create(:btn, lastresort: true).id
    expect(Phone.lookup_vdid_record_for(wrongnum)).to be_nil

    newone = Phone.autocreate(wrongnum)
    expect(newone.autocreate?).to be true
    expect(newone.btn.lastresort?).to be true

    # it should be found.
    expect(Phone.lookup_vdid_record_for(wrongnum)).to_not be_nil
  end

  it "should have an associated SIP peer" do
    pending "relation not yet completed"
    raise NotImplementedError
  end

  it "should have a phone type" do
    pending "phone type not yet loaded"
    raise NotImplementedError
  end

  it "should encrypt the sip_password field" do
    nphone = Phone.create(:sip_username => '5146315358',
                          :sip_password => 'hellothere')

    assert nphone.save

    expect(nphone.attributes[:sip_password]).not_to equal('hellothere')
  end

  # describe "when importing" do
  #   it "should log an error when you import with a nil btn" do
  #     Phone.start_import
  #
  #     did1='9989108441'
  #     btn1='5145551213'
  #     callerid='5145551213'
  #     provider='NovaVision'
  #     name="V2B Technologies"
  #     sitename=""
  #     # provider, client,BTN,pbx_type,Site,callerid,Numéro,virtualdid,"Poste tél.","Boite vocale",Access,"Appel International access","Services / Group",Notes,"Date d'activation","Fin de   service",Paging,Licenses,"MAC address","Type device","SIP U/N","SIP P/W","IP address","Tested 911","VOIP Supplier","Courriel Contact","Type de connexion"
  #     row1 = [provider,   # provider
  #       name,             # name
  #       nil,              # btnname
  #       'hosted',         # pbx_type: private/hosted
  #       'no access',      # dns
  #       '1',              # depart num
  #       sitename,         # sitename
  #       callerid,         # callerid
  #       did1,             # phoneno
  #       'NO',             # virtualdid
  #       nil,              # extension
  #       nil,              # voicemail
  #       '',               # access.
  #       nil,              # appel international access
  #       "1 de 2",         # services/group
  #       nil,              # services
  #       "Frank",          # firstname
  #       "Jones",          # lastname
  #       nil,              # notes
  #       nil,              # activation_date
  #       nil,              # termination_date
  #       "spa_a325",       # auth_dev_username
  #       "pw_a325",        # auth_dev_password
  #       nil,              # paging
  #       nil,              # licenses
  #       '0004f227a325',   # macaddress
  #       'SPA2102-NA',     # device_type
  #       '998-145-1212',   # sip_username
  #       'badpw',          # sip_password
  #       "G711",           # codec
  #       nil,              # ip_address
  #       "yes",            # page_multicast
  #       nil,              # e911_notes
  #       'ISP',            # voip_suplier
  #       'info@v2b.example',# email
  #       nil,              # connection_type
  #       nil,              # time_allocation_monthly
  #       nil,              # time_allocation_cost
  #       "yes",            # palladion
  #       false ]
  #     stats  = Hash.new(0)
  #     result = Phone.import_row(row1, 0, false, stats)
  #     expect(result).to be false
  #     expect(stats[:btnnameblank]).to eq(1)
  #   end
  #
  #
  #   def import_client1(did1, btn1, did2, name, sitename, row1, row2)
  #     stats = Hash.new
  #     Phone.import_row(row1, 1, false, stats)
  #
  #     expect( Client.find_by_name(name)).to_not be_nil
  #     expect( Btn.find_by_billingtelephonenumber(btn1)).to_not be_nil
  #
  #     p1 = Phone.find_by_phone_number(did1)
  #     assert         p1,      "phone not created"
  #     expect( p1.btn).to_not be_nil
  #     expect( p1.callerid).to_not be_nil
  #     expect(p1.virtualdid).to eq(false)
  #     assert p1.email.include?("@")
  #
  #     # there will be no CustomerCompany if there is no building!
  #     if p1.site.building
  #       expect(p1.site.otrs_customeruser.count).to be > 0
  #     end
  #
  #     if row2
  #       p22 = Phone.import_row(row2, 2, false, stats)
  #       p2 = Phone.find_by_phone_number(did2)
  #       expect( p2).to_not be_nil
  #       expect( p2.btn).to_not be_nil
  #       expect( p2.callerid).to_not be_nil
  #       expect(p1.btn).to       eq(p2.btn)
  #       expect(p2.site).to      eq(p1.site)
  #       expect(p2.callerid).to_not be_nil
  #       expect(p2.virtualdid).to eq(false)
  #     end
  #   end
  #
  #   it "should create client, site when a new csv is imported" do
  #     Phone.start_import
  #
  #     did1='9989108441'
  #     btn1='5145551213'
  #     name="Fun Technologies"
  #     b1=Btn.create(billingtelephonenumber: btn1,client: Client.find_or_make(name))
  #     callerid='5145551213'
  #     did2='5145561218'
  #     provider='NovaVision'
  #     sitename="hello"
  #     row1 = [provider,name,btn1,'hosted',dnsyes,'1',sitename,callerid,did1,'NO',nil,nil,'','YES',"1 de 2",'','Frank','Jones','notes',nil,nil,nil,nil,'paging','license','mac','SPA2102-NA','998-145-1212','badpw',nil,nil,nil,'2015-01-01','ISP','info@v2b.example','Fiber',nil,nil,'YES']
  #     row2 = [nil,nil,nil,       'hosted',dnsyes,'1',nil,     nil,     did2,'NO',nil,nil,nil,'yes',"2 de 2",'','Fred','Smith','notes',nil,nil,nil,nil,'paging','license','mac','SPA2102-NA','514-556-1218','badpw2',nil,nil,nil,nil,'ISP',nil,nil,nil,nil,nil]
  #
  #     import_client1(did1, btn1, did2, name, sitename, row1, row2)
  #   end
  #
  #   it "should create a voicemailportal extension when service_group says so" do
  #     Btn.create(:billingtelephonenumber => '6135551212',
  #                :client => Client.find_or_make('Frank Jones'))
  #     Phone.start_import
  #     row1 = ['Frank',    #provider             0,
  #       'Frank Jones',    # client name         1
  #       '6135551212',     # btnname             2
  #       'Hosted',         # pbx_type            3
  #       dnsyes,           # uses DNS for registration 4
  #       '1',              # depart num          5
  #       'Jonestown',      # sitename            6
  #       '6135550101',     # phoneno             7
  #       '6135550101',     # callerid            8
  #       'y',              # virtualdid          9
  #       '900',            # extension          10
  #       nil,              # voicemail          11
  #       '',               # access.            12
  #       nil,              # appel international access 13
  #       "Voice Portal",   # services/group     14
  #       '',               # non-reachable number 15
  #       nil,              # firstname          16
  #       nil,              # familyname         17
  #       nil,              # notes              18
  #       nil,              # activation_date    19
  #       nil,              # termination_date   20
  #       nil,              # paging
  #       nil,              # licenses
  #       '0004f227a325',   # macaddress
  #       'SPA2102-NA',     # device_type
  #       '998-145-1212',   # sip_username
  #       'badpw',          # sip_password
  #       nil,              # ip_address
  #       nil,              # e911_notes
  #       'ISP',            # voip_suplier
  #       'info@v2b.example',# email
  #       nil]              # connection_type
  #     p1 = Phone.import_row(row1)
  #     expect(p1.voicemailportal).to eq(true)
  #   end
  #
  #   it "should create client when a btn of n/a is used" do
  #     Phone.start_import
  #
  #     did1='9989108441'
  #     btn1='5145551213'
  #     callerid1='5145551213'
  #     did2='5145561218'
  #     provider='NovaVision'
  #     name="Fun Technologies"
  #     departnum='1'
  #     sitename=""
  #     Btn.create(:billingtelephonenumber => btn1,:client => Client.find_or_make(name))
  #     row1 = [provider,name,btn1, 'private', dnsyes, departnum, sitename,callerid1,did1,'NO',nil,nil,'', 'YES',"1 de 2",'','Frank','Jones','notes',nil,nil,nil,nil,'paging','license','mac','SPA2102-NA','998-145-1212','badpw', nil,nil,nil,'2015-01-01','ISP','info@v2b.example','Fiber',nil,nil,'YES']
  #     row2 = [nil,     nil,'n/a', nil,       dnsyes, departnum, nil,     callerid1,did2,'NO',nil,nil,nil,'yes',"2 de 2",'', 'Fred', 'Smith','notes',nil,nil,nil,nil,'paging','license','mac','SPA2102-NA','514-556-1218','badpw2',nil,nil,nil,nil,         'ISP',nil,nil,nil,nil,nil]
  #
  #     import_client1(did1, btn1, did2, name, sitename, row1, row2)
  #   end
  #
  #   it "should create a phone when client name is blank" do
  #     # the important part of the sample data is that the
  #     # row3 skips the client name, but has the same site as the second row.
  #     # it should create row3 data in the same client, but at a different site.
  #
  #     # since BTN became canonical, and no longer auto-created, must mock up
  #     # the BTN for this to work.
  #     #ENV['IMPORT_DEBUG']="true"
  #     #$NOLOG_TESTING=false;
  #
  #     b1 = Btn.create(:billingtelephonenumber => '+15149998834')
  #     c1 = Client.find_or_make('FrankTown')
  #     b1.client = c1;       b1.save;
  #     c1.client_crm_account_code_fix
  #     Btn.uncache_btns!
  #
  #     Phone.start_import
  #
  #     row1=['','FrankTown','5149998834','hosted',dnsyes,'1','Laval','5149998834','2905550165','','YES','0165','','','','Voice Portal','','','','','','','','','','n/a','2905550165','79557644','','','','Phillipe','NovaVision','sam@frank.org','Fibre Bell']
  #     row2=['','FrankTown','5149998834','hosted',dnsyes,'2','Mirabel','5149998835','2915550123','','YES','0123','','','','Voice Portal','','','','','','','','','','','','n/a','n/a','79557644','','','','Andr','NovaVision','sam@frank.org','Fibre Bell2']
  #     row3=['','','5149998834','hosted',         dnsyes,'2','Mirabel','5145558836','2035550175','','YES','2227','OUI','','','','','Jean','Chretien','','','','','','','Standard Bus.','0004f2276bc8','Polycom 335','2035550175','nova11224','','','','Andr','NovaVision','sam@frank.org','Fibre Bell3']
  #
  #     stats = Hash.new
  #     ph1=Phone.import_row(row1, 1, false, stats)
  #     puts stats       unless ph1
  #     expect(ph1).to be_truthy
  #     expect( ph1.btn).to_not be_nil
  #     expect( ph1.btn.client).to_not be_nil
  #
  #     ph2=Phone.import_row(row2, 2, false, stats)
  #     puts stats       unless ph1
  #     expect(ph2).to be_truthy
  #     ph3=Phone.import_row(row3, 3, false, stats)
  #     puts stats       unless ph1
  #     expect(ph3).to be_truthy
  #     assert ph1.btn.client.id == ph2.btn.client.id
  #     assert ph3.btn.client.id == ph2.btn.client.id
  #   end
  #
  #   def make_franktown
  #     b1 = Btn.create(:billingtelephonenumber => '+15149998834')
  #     c1 = Client.find_or_make('FrankTown')
  #     b1.client = c1;       b1.save;
  #     c1.client_crm_account_code_fix
  #     Btn.uncache_btns!
  #   end
  #
  #   it "should terminate a phone once, even when multiple termination lines" do
  #     make_franktown
  #     stats = Hash.new
  #
  #     Phone.start_import
  #     #     0    1          2            3        4      5   6       7            8            9   10    11     12  13
  #     row1=['p','FrankTown','5149998834','hosted',dnsyes,'1','Laval','5149998834','2905550165','n','YES','0165','A','I',
  #           #14            15   16     17     18           19
  #           'Voice Portal','', 'First','Last','notes', '2012-01-01','',          '','','','','','','','n/a',
  #           '2905550165','79557644','','','','Phillipe','NovaVision','sam@frank.org','Fibre Bell']
  #     ph1=Phone.import_row(row1, 1, false, stats)
  #     expect(ph1).to_not be_terminated
  #     Phone.end_import
  #
  #     Phone.start_import
  #     #     0   1          2            3        4      5   6       7            8            9  10    11     12 13
  #     row2=['p','FrankTown','5149998834','hosted',dnsyes,'1','Laval','5149998834','2905550165','n','YES','0165','a','i',
  #           #14            15      16     17       18           19
  #           'Voice Portal','',     'First','Last','notes', '2012-01-01','2015-01-01','','','','','','','','n/a',
  #           '2905550165','79557644','','','','Phillipe','NovaVision','sam@frank.org','Fibre Bell']
  #     ph2=Phone.import_row(row2, 1, false, stats)
  #     expect(ph2).to be_terminated
  #
  #     expect(ph1.id).to eq(ph2.id)
  #     Phone.end_import
  #
  #     Phone.start_import
  #     # row2 repeated above
  #     ph3=Phone.import_row(row2, 1, false, stats)
  #     expect(ph3).to be_terminated
  #
  #     expect(ph1.id).to eq(ph3.id)
  #     Phone.end_import
  #   end
  #
  #   it "should terminate a phone, even if termination date is unparseable" do
  #     make_franktown
  #     stats = Hash.new
  #
  #     Phone.start_import
  #     #     0   1          2            3        4      5   6       7            8            9  10    11     12 13
  #     row2=['p','FrankTown','5149998834','hosted',dnsyes,'1','Laval','5149998834','2905550165','n','YES','0165','a','i',
  #           #14            15      16     17       18           19
  #           'Voice Portal','',     'First','Last','notes', '2012-01-01','avril 2011','','','','','','','','n/a',
  #           '2905550165','79557644','','','','Phillipe','NovaVision','sam@frank.org','Fibre Bell']
  #     ph2=Phone.import_row(row2, 1, false, stats)
  #     expect(ph2).to be_terminated
  #     Phone.end_import
  #
  #   end
  #
  #   # when updating things, update the file from a real entry from the CSV databas.
  #   # Steve Foisy is the CEO, but change the phone number nicely.
  #   it "should create a btn for a from CSV" do
  #     Phone.start_import
  #
  #     csv1file = 'spec/files/phone1.csv'
  #
  #     name = "Steve Foisy"
  #     phone= '4505551234'
  #     Btn.create(:billingtelephonenumber => "+1" + phone,:client => Client.find_or_make(name))
  #     Phone.import_csv(csv1file)
  #     nph = Client.find_by_name(name)
  #     expect(nph).to be_truthy
  #
  #     p1 = Phone.find_by_phone_number(phone)
  #     expect(p1).to be_truthy
  #     expect(p1.btn).to be_truthy
  #     expect(p1.phone_mac_addr.downcase).to eq('0090f8095583')
  #     expect(p1.email).to eq('asfoisy@ip4b.ca')
  #   end
  # end

  describe "yes/no values" do
    it "should answer true for YES" do
      expect(Phone.yes_no_string("YES")).to eq(true)
    end
    it "should answer true for yes" do
      expect(Phone.yes_no_string("yes")).to eq(true)
    end
    it "should answer true for Yes" do
      expect(Phone.yes_no_string("Yes")).to eq(true)
    end
    it "should answer false for no" do
      expect(Phone.yes_no_string("no")).to eq(false)
    end
    it "should answer false for non" do
      expect(Phone.yes_no_string("non")).to eq(false)
    end
    it "should answer false for n" do
      expect(Phone.yes_no_string("n")).to eq(false)
    end
    it "should answer true for oui" do
      expect(Phone.yes_no_string("Oui")).to eq(true)
    end
    it "should answer false for Non" do
      expect(Phone.yes_no_string("Non")).to eq(false)
    end
    it "should answer false for nil" do
      expect(Phone.yes_no_string(nil)).to eq(false)
    end
  end

end
