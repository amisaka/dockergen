require 'rails_helper'

describe Portal::User do
  if $PORTAL

    describe "relations" do
      it "should have one userprofile" do
        u1 = auth_user(:crediluser1)

        expect(u1).to_not be_nil
        expect(u1.userprofile).to_not be_nil
      end
    end
  end
end
