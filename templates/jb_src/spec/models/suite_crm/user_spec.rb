require 'rails_helper'

describe SuiteCrm do
  describe "relations" do
    it "should have an associated crm_account" do
      sf  = SuiteCrm::User.where(user_name: 'sfoisy').take
      expect(sf.crm_accounts.count).to be > 0
    end
  end

end
