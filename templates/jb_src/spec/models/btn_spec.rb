# coding: utf-8
require 'rails_helper'

describe Btn do

  describe "relations" do
    it { should have_many :phones }

    it { should belong_to :client }

    it { should have_many :incoming_cdrs }
    it { should have_many :outgoing_cdrs }

    it { should belong_to :rate_center }

    it { should belong_to :local_rate_plan }
    it { should belong_to :national_rate_plan }
    it { should belong_to :world_rate_plan }

    it { should belong_to(:invoice_template) }

    it { should belong_to(:billing_site) }
  end

  it "should return existing BTN if found" do
    b1 = create(:btn)
    btn1 = Btn.find_or_make(b1.billingtelephonenumber)

    assert_equal b1.id, btn1.id
  end

  # it "should return existing BTN, even with dash if found" do
  #   b1 = create(:btn)
  #   btn1 = Btn.find_or_make('+1514-555-1214')
  #
  #   assert_equal b1.id, btn1.id
  # end

  it "should record new BTN without dash" do
    btn3 = Btn.find_or_make('(999)555-1277')
    expect(btn3.billingtelephonenumber).to eq('+19995551277')
  end

 it "should return new BTN if new" do
    b1 = create(:btn)
    b2 = create(:btn)

    btn3 = Btn.find_or_make('9995551215')
    expect(b1.id).to_not eq(btn3.id)
    expect(b2.id).to_not eq(btn3.id)
  end

  it "should create a new btn if previous one was end_date deleted" do
    btn3 = Btn.find_or_make('(999)555-1216')
    expect( btn3 ).to_not be_nil
    id3 = btn3.id
    assert btn3.active? == true

    btn3.end_date = '2015-09-23'.to_date
    btn3.save!
    assert btn3.active?('2015-09-22'.to_date) == true
    assert btn3.active?('2015-09-24'.to_date) == false

    btn4 = Btn.find_or_make('(999)555-1216')
    expect( btn4 ).to_not be_nil
    assert id3 != btn4.id
  end

  it { should have_many(:billable_services) }

  describe "#add_billable_service" do
    before do
      @invoice = create(:invoice)
      @btn = create(:btn)
      @billable_service = create(:billable_service)
    end

    it "adds a billable service to the btn's invoice" do
      @btn.add_billable_service(@invoice, @billable_service)
      @btn.reload && @btn.billable_services.reload
      expect(@btn.billable_services).to include(@billable_service)
    end
  end

  describe "importing" do
    it "should import a billing address to a btn, setting the linked billing_site address" do
      row1 = [ "Active","8195550909","Adrian Pennino","19A Boul Gérard Franklin O\nPhiladelphia  QC  H01 1N0 ","218 9e Rue\nGerald Franklin  QC  H01 1N0 ","218 9e Rue\n","","Philadelpha","QC","H011N0" ]
      site1 = Btn.import_billing_address(1, row1)
      expect(create(:btn).find_billing_site.address1).to eq("218 9e Rue")
      expect(create(:btn).find_billing_site.postalcode).to eq("H011N0")
    end
  end

  describe "billing" do
    it "should not show support invoices" do
      expect(Btn.all.regular_billable).to_not include(create(:btn))
    end
  end

end
