describe Client do

  it { should have_many :line_of_businesses }
  it { should have_many :client_line_of_businesses }

  it { should have_many :contacts }

  describe "#name" do
    it { should delegate_method(:name).to(:crm_account) }
  end

  describe "#billingtelephonenumber" do
    it { should delegate_method(:billingtelephonenumber).to(:btn) }
  end

  describe "#billingtelephonenumber=" do
    it { should delegate_method(:billingtelephonenumber=).to(:btn).with_arguments('foobar') }
  end

  describe "#billing_address_1" do
    it { should delegate_method(:billing_address_1).to(:crm_account) }
  end

  describe "#billing_address_2" do
    it { should delegate_method(:billing_address_2).to(:crm_account) }
  end

  describe "#billing_address_3" do
    it { should delegate_method(:billing_address_3).to(:crm_account) }
  end

  describe "#billing_address_4" do
    it { should delegate_method(:billing_address_4).to(:crm_account) }
  end

  describe "#billing_province" do
    it { should delegate_method(:billing_province).to(:crm_account) }
  end

  describe "#billing_country" do
    it { should delegate_method(:billing_country).to(:crm_account) }
  end

  # describe "#city" do
  #   it { should delegate_method(:city).to(:crm_account) }
  # end

  describe "#city=" do
    it { should delegate_method(:city=).to(:crm_account).with_arguments('foobar') }
  end

  describe "#province" do
    it { should delegate_method(:province).to(:crm_account) }
  end

  describe "#province=" do
    it { should delegate_method(:province=).to(:crm_account).with_arguments('foobar') }
  end

  describe "#country" do
    it { should delegate_method(:country).to(:crm_account) }
  end

  describe "#country=" do
    it { should delegate_method(:country=).to(:crm_account).with_arguments('foobar') }
  end

  # describe "#postalcode" do
  #   it { should delegate_method(:postalcode).to(:billing_site) }
  # end

  describe "#postalcode=" do
    it { should delegate_method(:postalcode=).to(:crm_account).with_arguments('foobar') }
  end

  describe "#language_preference" do
    it { should validate_presence_of :language_preference }
    it { should allow_value('French').for :language_preference }
    it { should allow_value('English').for :language_preference }
    it { should_not allow_value(nil).for :language_preference }
    it { should_not allow_value('').for :language_preference }
    it { should_not allow_value('Pirate').for :language_preference}
  end

  describe "#status" do
    context "when the client has an activation date in the past" do
      subject { create(:client, started_at: Time.now - 1.day )}
      it "reports the client as active" do
        expect(subject.status).to eq(:active)
      end
    end

    context "when the client has an activation date in the future" do
      subject { create(:client, started_at: Time.now + 1.day )}
      it "reports the client as pending start" do
        expect(subject.status).to eq(:pending_start)
      end
    end

    context "when the client has a termination date in the past" do
      subject { create(:client, terminated_at: Time.now - 1.day )}
      it "reports the client as closed" do
        expect(subject.status).to eq(:closed)
      end
    end

    context "when the client has a termination date in the future" do
      subject { create(:client, terminated_at: Time.now + 1.day )}
      it "reports the client as pending close" do
        expect(subject.status).to eq(:pending_close)
      end
    end

    context "when the client has neither an activation date or a termination date" do
      subject { create(:client)}
      it "reports the client as unknown" do
        expect(subject.status).to eq(:unknown)
      end
    end
  end

  describe "#status=" do
    subject { create(:client) }

    context "when passing 'active' or :active" do
      it "sets the clients status to :active" do
        subject.status = 'active'
        expect(subject.status).to eq(:active)
        subject.status = :active
        expect(subject.status).to eq(:active)
      end
    end

    context "when passing 'closed' or :closed" do
      it "sets the clients status to :closed" do
        subject.status = 'closed'
        expect(subject.status).to eq(:closed)
        subject.status = :closed
        expect(subject.status).to eq(:closed)
      end
    end

    context "when passing 'pending_start' or :pending_start" do
      it "sets the clients status to :pending_start" do
        subject.status = 'pending_start'
        expect(subject.status).to eq(:pending_start)
        subject.status = :pending_start
        expect(subject.status).to eq(:pending_start)
      end
    end

    context "when passing 'pending_close' or :pending_close" do
      it "sets the clients status to :pending_close" do
        subject.status = 'pending_close'
        expect(subject.status).to eq(:pending_close)
        subject.status = :pending_close
        expect(subject.status).to eq(:pending_close)
      end
    end
  end

  describe "statuses scopes" do
    before do
      @active_clients = create_list(:client, 2, :active)
      @closed_clients = create_list(:client, 2, :closed)
      @pending_start_clients = create_list(:client, 2, :pending_start)
      @pending_close_clients = create_list(:client, 2, :pending_close)
      @unknown_clients = create_list(:client, 2, :unknown)
    end

    describe ".active" do
      it "returns the list of active clients" do
        expect(Client.active).to include(*@active_clients)
        expect(Client.active).not_to include(*@closed_clients)
        expect(Client.active).not_to include(*@pending_start_clients)
        expect(Client.active).not_to include(*@pending_close_clients)
        expect(Client.active).not_to include(*@unknown_clients)
      end
    end

    describe ".closed" do
      it "returns the list of closed clients" do
        expect(Client.closed).not_to include(*@active_clients)
        expect(Client.closed).to include(*@closed_clients)
        expect(Client.closed).not_to include(*@pending_start_clients)
        expect(Client.closed).not_to include(*@pending_close_clients)
        expect(Client.closed).not_to include(*@unknown_clients)
      end
    end

    describe ".pending_start" do
      it "returns the list of clients pending activation" do
        expect(Client.pending_start).not_to include(*@active_clients)
        expect(Client.pending_start).not_to include(*@closed_clients)
        expect(Client.pending_start).to include(*@pending_start_clients)
        expect(Client.pending_start).not_to include(*@pending_close_clients)
        expect(Client.pending_start).not_to include(*@unknown_clients)
      end
    end

    describe ".pending_close" do
      it "returns the list of clients pending termination" do
        expect(Client.pending_close).not_to include(*@active_clients)
        expect(Client.pending_close).not_to include(*@closed_clients)
        expect(Client.pending_close).not_to include(*@pending_start_clients)
        expect(Client.pending_close).to include(*@pending_close_clients)
        expect(Client.pending_close).not_to include(*@unknown_clients)
      end
    end

    describe ".unknown" do
      it "returns the list of clients with unknown status" do
        expect(Client.unknown).not_to include(*@active_clients)
        expect(Client.unknown).not_to include(*@closed_clients)
        expect(Client.unknown).not_to include(*@pending_start_clients)
        expect(Client.unknown).not_to include(*@pending_close_clients)
        expect(Client.unknown).to include(*@unknown_clients)
      end
    end
  end

  describe "relations" do

    it { should belong_to :crm_account }

    it "should have one or more callstats associated with the client" do
      n1 = Client.create(:name => 'new one')
      period = StatsPeriod.create(:type => 'DailyStatsPeriod',
				  :day => '2012-01-01', :hour => 12)
      n1.callstats << Callstat.create(:stats_period => period,
				       :value => 20,    :client => n1)
      expect(n1.callstats.count).to eq(1)
    end

    it { should have_many :payments }

  end

  # describe "should manage customer contacts" do
  #   it "when a customer has a contact" do
  #     credil = create(:client)
  #
  #     # count OTRS objects attached to active sites only.
  #     cnt = 0
  #     credil.sites.activated.each { |site|
  #       cnt += 1 if site.otrs_customeruser.count > 0
  #     }
  #     expect(cnt).to eq(1)
  #   end
  # end

  # it "should return existing Client if found" do
  #   credil = create(:client)
  #   client1 = Client.find_or_make('CREDIL')
  #
  #   assert_equal client1.id, credil.id
  # end

  # it "should return existing Client if found, ignoring trailing spaces" do
  #   credil = create(:client)
  #   client1 = Client.find_or_make('CREDIL ')
  #
  #   assert_equal client1.id, credil.id
  # end
  #
  # it "should return existing Client if found, ignoring leading spaces" do
  #   credil = create(:client)
  #   client1 = Client.find_or_make(' CREDIL')
  #
  #   assert_equal client1.id, credil.id
  # end

  it "should have to_s that gives client name/id" do
    client = create(:client)
    expect(client.to_s).to eq "{#{client.id}}#{client.name}"
  end

  # describe "with a known billing address" do
  #   # it "should create a new site for the given client" do
  #   #   credil = create(:client)
  #   #
  #   #   billing = credil.billing_site
  #   #   sitecount = credil.sites.activated.count
  #   #   expect(sitecount).to eq(2)
  #   #
  #   #   credil.sites.find_or_make("Aylmer")
  #   #   sitecount = credil.sites.activated.count
  #   #   expect(sitecount).to eq(3)
  #   #
  #   #   expect(credil.billing_site).to eq(billing)
  #   # end
  #
  #   it "should not duplicate existing site name" do
  #     credil = create(:client)
  #
  #     billing = credil.billing_site
  #     sitecount = credil.sites.activated.count
  #     expect(sitecount).to eq(2)
  #
  #     credil.sites.find_or_make(billing.name)
  #     sitecount = credil.sites.activated.count
  #     expect(sitecount).to eq(2)
  #
  #     expect(credil.billing_site).to eq(billing)
  #   end
  # end

  describe "without a known billing address" do
    it "should create a billing address" do
      fdku = create(:client)

      assert_nil fdku.billing_site

      fdku.sites.find_or_make("Laval")
      sitecount = fdku.sites.count
      assert_equal 2, sitecount
      expect( fdku.billing_site ).to_not be_nil
    end
  end

  it { should have_many :billable_services }

  describe "#add_billable_service" do
    before do
      @btn = create(:btn)
      @client = create(:client, btn: @btn)
      @invoice = create(:invoice, client: @client)
      @billable_service = create(:billable_service, client: @client)
    end

    it "should add a billable service to this client's btn-invoice" do
      @client.add_billable_service(@btn, @invoice, @billable_service)
      @client.reload && @client.billable_services.reload
      expect(@client.billable_services).to include(@billable_service)
    end
  end

  it { should have_many :sites }

  describe "#add_site" do
    subject { create(:client) }
    before { @site = build(:site, client: nil) }

    it "adds a site to client" do
      expect{subject.add_site(@site)}.to change(subject.sites, :count).by(1)
    end
  end

  describe "#remove_site" do
    subject { create(:client) }
    before { @site = create(:site, client: subject) }

    it "removes a site from client" do
      expect{subject.remove_site(@site)}.to change(subject.sites, :count).by(-1)
    end
  end

  describe "#add_phone" do

    before { @phone = build(:phone) }
    subject { create(:client_with_btns) }

    it "adds a phone to client" do
      expect{subject.add_phone(subject.btns.first, @phone)}.to change(subject.phones, :count).by(1)
    end
  end

  describe "#remove_phone" do

    before do
      @phone = create(:phone_with_client)
      @btn = @phone.client.btns.first
    end
    subject { @phone.btn.client }

    it "removes a phone from client" do
      expect{subject.remove_phone(@btn, @phone)}.to change(subject.phones, :count).by(-1)
    end
  end

  describe "billing" do
    it "should not show clients with support-only invoices" do
      expect(Client.all.regular_billable).to_not include(create(:client))
    end
  end

end
