require 'rails_helper'

describe NorthAmericanExchange do

  describe "relations" do
    it "should have connection to local_exchanges" do
      nae13035 = north_american_exchanges(:nae13035)
      expect(nae13035.local_exchanges.count).to be > 0
    end

    it "should belong to a rate center" do
      nae13035 = north_american_exchanges(:nae13035)
      expect(nae13035.rate_center).to_not be_nil
    end
  end

  describe "active" do
    it "should be active if not deleted" do
      nae0 = north_american_exchanges(:nae13040)
      expect(nae0).to be_active
    end

    it "should be inactive if marked deleted" do
      nae = north_american_exchanges(:nae13041)
      expect(nae).to be_inactive
    end

    it "should be active if not deleted" do
      nae = north_american_exchanges(:nae13041)
      expect(NorthAmericanExchange.active.where(id: nae.id).take).to be_nil
    end
  end

  def line1
    "888  CANADA              E041016782823A    EOCN   Y1010YG054 154EMONTAGUE    PEMONTAGUE   00009999CHTWPETN0MD00    1212A      N888   022216 041016 041016                 "
  end
  def line2
    "888  CANADA              E041016782823A    EOCN   Y1010YG054 154EMONTAGUE    PEMIRAMICHI  00009999CHTWPETN0MD00    1212A      N888   022216 041016 041016                 "
  end
  def line3
    "888  CANADA              E041016782823A    EOCN   Y1010YG054 154EMONTAGUE    NBMONTAGUE   00009999CHTWPETN0MD00    1212A      N888   022216 041016 041016                 "
  end

  describe "import" do
    it "should import a reference line" do

      stats = Hash.new(0)
      item = NorthAmericanExchange.import_row(line1, stats)
      expect(item.npa).to  eq(782)
      expect(item.nxx).to  eq(823)
      expect(item.name).to eq("MONTAGUE")
      expect(item.province).to eq("PE")
      expect(item.ocn).to      eq("154E")
      expect(item.lata).to     eq("888")
      expect(item.coc_type).to eq("EOC")
      expect(item.switch).to   eq("CHTWPETN0MD")
      expect(item.portable_indicator).to be_truthy
      expect(item.rate_center).to_not be_nil
    end

    it "should update a name" do
      stats = Hash.new(0)
      item1 = NorthAmericanExchange.import_row(line1, stats)
      item = NorthAmericanExchange.import_row(line2, stats)
      expect(item.npa).to  eq(782)
      expect(item.nxx).to  eq(823)
      expect(item.name).to eq("MIRAMICHI")
      expect(item.province).to eq("PE")
      expect(item.rate_center).to_not be_nil
      expect(stats[:updated_name]).to eq(1)
    end

    it "should update a province" do
      stats = Hash.new(0)
      item = NorthAmericanExchange.import_row(line1, stats)
      item = NorthAmericanExchange.import_row(line3, stats)
      expect(item.npa).to  eq(782)
      expect(item.nxx).to  eq(823)
      expect(item.name).to eq("MONTAGUE")
      expect(item.province).to eq("NB")
      expect(stats[:updated_province]).to eq(1)
    end

    it "should find newest ZIP file" do
      # this script sets up some tests into tmp, which need
      # to have correct dates.
      system(Rails.root.join("bin/telecordiatestdir").to_s)
      tmpTelecordiaDataDir = Rails.root.join("tmp", "telecordia")

      lerg6file = LergMethods::latestLerg6ZipFile(tmpTelecordiaDataDir)
      expect(File.basename(lerg6file)).to eq("LERGDATA0616.ZIP")
    end

    it "should extract LERG6.DAT file from ZIP file" do
      # this script sets up some tests into tmp, which need
      # to have correct dates.
      tmpTelecordiaDataDir = Rails.root.join("spec", "files", "telecordia")

      lerg6fileA = LergMethods::latestLerg6ZipFile(tmpTelecordiaDataDir)
      expect(File.basename(lerg6fileA)).to eq("LERGDATA0616.ZIP")

      lerg6file,tmpdir = LergMethods::findlerg6(lerg6fileA)
      expect(File.exists?(lerg6file)).to be true

      File.unlink(lerg6file)
      FileUtils.rmdir(tmpdir) if tmpdir
    end

    it "should read LERG6 file" do
      tmpTelecordiaDataDir = Rails.root.join("spec", "files", "telecordia")

      lerg6file = LergMethods::latestLerg6ZipFile(tmpTelecordiaDataDir)
      expect(File.basename(lerg6file)).to eq("LERGDATA0616.ZIP")

      lerg6file,tmpdir = LergMethods::findlerg6(lerg6file)
      count = Lerg8.import_file(lerg6file)

      expect(count).to eq(1000)
    end

    it "should import a sample file" do
      stats0 = Hash.new(0)

      file = Rails.root.join("spec","files","LERG6.201603.forTesting.DAT")

      NorthAmericanExchange.import_file(file, stats0)
      expect(stats0[:line]).to    eq(304)
      expect(stats0[:created]).to eq(304)
      expect(stats0[:badline]).to eq(0)

      stats1 = Hash.new(0)
      NorthAmericanExchange.import_file(file, stats1)
      expect(stats1[:line]).to    eq(304)
      expect(stats1[:created]).to eq(0)
    end

  end

  describe "NNACL export" do
    def nnacl_acton_1
      "450366       EOCN   ACVAPQ73AMD   2243888  ACTON VALE"+
        "                    0000000000             PQY "
    end
    it "should create a single line from a record" do
      nae13035 = north_american_exchanges(:nae13035)
      expect(nae13035.to_nnacl).to eq(nnacl_acton_1)
    end
  end
end
