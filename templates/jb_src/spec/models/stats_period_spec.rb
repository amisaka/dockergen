require 'rails_helper'

describe StatsPeriod do
  describe "relations" do
    it "should have a number of callstats associated with each period" do
      n1 = Client.create(:name => 'new one')
      period = StatsPeriod.create(:type => 'DailyStatsPeriod',
				  :day => '2012-01-01', :hour => 12)
      cs = Callstat.create(:stats_period => period,
			    :value => 20,    :client => n1)
      cs.client = n1
    end
  end

  it "should allocate some sequence numbers for compute server" do
    one = StatsPeriod.create
    two = StatsPeriod.create
    expect(two.id).to eq(one.id+1)
    numbers = StatsPeriod.allocate_id(1024)
    expect(numbers).to eq(two.id+1)

    three = StatsPeriod.create
    expect(three.id).to eq(numbers+1024+1)
  end

  describe "time periods" do
    it "should cover one hour" do
      period = StatsPeriod.new(:type => 'DailyStatsPeriod',
			       :day => '2012-01-01', :hour => 9)

      expect(period.start_hour).to eq(9)
      expect(period.end_hour).to eq(10)

      Time.zone = period.timezone
      expect(period.beginning_of_period.to_datetime).to eq(Time.zone.local(2012,01,01,9,00,00).to_datetime)
      expect(period.end_of_period.to_datetime).to eq(Time.zone.local(2012,01,01,10,00,00).to_datetime)
    end

    it "should cover one year" do
      period = StatsPeriod.new(:type => 'YearlyStatsPeriod',
			       :day  => '2012-01-01')
      expect(period.start_hour).to eq(0)
      expect(period.end_hour).to   eq(24)
      expect(period.beginning_of_period.to_datetime).to eq(Time.gm(2012,1,1,0,0,0).to_datetime)

      # this works around a bug in == for datetime:
      # Diff:2012-12-31T23:59:59+00:00.==(2012-12-31T23:59:59+00:00) returned false
      #   even though the diff between 2012-12-31T23:59:59+00:00 and 2012-12-31T23:59:59+00:00
      #   is empty. Check the implementation of 2012-12-31T23:59:59+00:00.==.
      # the subtraction results in (86399/86400), a fraction, which is less than 1s.
      expect(Time.gm(2012,12,31,23,59,59).to_datetime - period.end_of_period.to_datetime).to be < 1
    end
  end

  describe "scopes" do
    it "should return a scope for the entire year" do
      period = StatsPeriod.new(:type => 'YearlyStatsPeriod',
			       :day  => '2012-01-01')
      expect(period.within_period.to_sql).to eq(
	%q{SELECT "stats_periods".* FROM "stats_periods" WHERE ("stats_periods"."day" BETWEEN '2012-01-01' AND '2012-12-31')}
      )
    end
    it "should return a scope for all the DailyStatsPeriods" do
      period = StatsPeriod.new(:type => 'YearlyStatsPeriod',
			       :day  => '2012-01-01')

      # %q{} is like ''
      expect((period.within_period.merge(period.contained_period_types)).to_sql).to eq(
	%q{SELECT "stats_periods".* FROM "stats_periods" WHERE ("stats_periods"."day" BETWEEN '2012-01-01' AND '2012-12-31') AND "stats_periods"."type" = 'DailyStatsPeriod' AND (stats_periods.hour IS NULL OR stats_periods.hour = 0)}
      )
    end
  end

  describe "finding" do
    it "should return last period that was created" do

      # find newest the manual way
      newest = StatsPeriod.first
      StatsPeriod.find_each { |sp|
	if sp.day > newest.day
	  newest = sp
	end
      }

      expect(StatsPeriod.last_period.day).to eq(newest.day)
    end
  end


end
