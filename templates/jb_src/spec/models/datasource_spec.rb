require 'rails_helper'

describe Datasource do

  describe "relations" do
    it "should have a associated cdr records" do
      source1 = cdrviewer_datasource(:source1)
      assert source1.cdrs.count > 0
    end
    it { should belong_to :did_supplier }
  end

  describe "finds" do
    it "should find a datasource that already exists" do
      source1 = cdrviewer_datasource(:source1)
      assert_equal source1, Datasource.find_or_make(source1.datasource_filename)
    end

    it "should create a new datasource" do
      ds1 = Datasource.find_or_make("foobar")
      expect( ds1 ).to_not be_nil
      ds2 = Datasource.find_or_make("foobar")
      assert_equal ds1, ds2
    end
  end

  describe "reprogressing" do
    it "should provide a file to which edit commands may be appended" do
      source1 = cdrviewer_datasource(:source1)

      expect(source1.toll_edit_file).to eq(Rails.root.join('tmp','cdredit','source1.sh').to_s)
    end
  end
end
