require 'rails_helper'

RSpec.describe Invoice, type: :model do

  describe "#client" do
    it { should validate_presence_of :client }
  end

  describe "#btn" do
    it { should validate_presence_of :btn }
  end

  describe "relationships" do

    it { should have_many :payments }
    it { should have_many :payment_invoices }

    it "should have an associated client" do
      i1 = invoices(:motherhubbard01)
      expect(i1.client).to_not be_nil
      expect(i1.client.name).to eq("Mother Hubbard")
    end

    it "should have an associated billing-telephone-number" do
      i1 = invoices(:motherhubbard01)
      expect(i1.btn).to_not be_nil
      expect(i1.btn.name).to eq("+18195550909")
    end

    it "should have one or more associated billable services" do
      i1 = invoices(:motherhubbard01)
      expect(i1.billable_services.count).to be > 0
    end

    it { should have_many(:cdrs) }

    it "should have a parent relationship" do
      ir0 = invoice_runs(:july2017)
      i3 = ir0.invoice_for_btn(btns(:med5555556683))
      i3.setup_department_invoices
      expect(i3.billable_services.count).to eq(14)
      expect(i3.departmental_invoices.count).to eq(28)

      i3.migrate_billable_services_to_departments
      i3.reload
      expect(i3.billable_services.count).to eq(0)
    end

    it "should find billable services for both summary and departmental invoices" do
      i3 = invoices(:c211_i3_20177)
      b1 = i3.btn
      s1 = nil
      expect(i3.departmental_invoices.count).to eq(1)
      bsa = BillableServiceAccessor.new(b1.client)
      bs = bsa.for(b1, i3, s1)
      expect(bs.size).to eq(14)
    end
  end

  describe "types" do
    it "should have a default type of SimpleInvoice" do
      it = Invoice.create
      expect(it.invoice_type).to eq(:base_invoice)
    end
  end

  describe "due dates" do
    it "should calculate one business days before end of month" do
      i1 = invoices(:motherhubbard01)
      expect(i1.due_date).to eq('2016-04-28'.to_date)
    end
  end

  describe "invoice calculations" do
    it "should calculate an invoice in the future of same year" do
      inv0 = invoices(:motherhubbard01)
      expect(inv0.btn).to_not be_nil
      inv1 = inv0.invoice_in_month(3)
      expect(inv1.service_start).to eq('2016-07-01'.to_date)
    end

    it "should calculate an invoice in the future of next year" do
      inv0 = invoices(:motherhubbard01)
      expect(inv0.btn).to_not be_nil
      inv1 = inv0.invoice_in_month(10)
      expect(inv1.service_start).to eq('2017-02-01'.to_date)
    end

    it "should calculate an invoice next time month=X comes" do
      inv0 = invoices(:motherhubbard01)
      expect(inv0.btn).to_not be_nil
      inv1 = inv0.invoice_next_month_num(7)
      expect(inv1.service_start).to eq('2016-07-01'.to_date)
    end

    it "should calculate an invoice next time month=X comes next year" do
      inv0 = invoices(:motherhubbard01)
      expect(inv0.btn).to_not be_nil
      inv1 = inv0.invoice_next_month_num(1)
      expect(inv1.service_start).to eq('2017-01-01'.to_date)
    end

    it "should calculate the taxes on the services for Quebec" do
      inv0 = invoices(:fatherhubbard02)
      inv0.calc_taxes
      expect(inv0.call_tvq).to     be_within(0.01).of(1.71)
      expect(inv0.call_gst_tps).to be_within(0.01).of(0.86)
    end

    it "should calculate the taxes on the services for Ontario" do
      inv1 = invoices(:motherhubbard01)
      inv1.calc_taxes
      expect(inv1.call_hst_tvh).to be_within(0.01).of(2.23)
      expect(inv1.hst_tvh).to      be_within(0.01).of(1.32)
      expect(inv1.call_gst_tps).to be_within(0.01).of(0)
    end

    it "should calculate the total amount invoiced" do
      inv1 = invoices(:motherhubbard01)
      expect(inv1.amount_invoiced).to be_within(0.001).of(27.32)  # fixture value
      inv1.recalc_derived
      inv1.calc_tollsummary
      inv1.calc_subtotals
      expect(inv1.amount_invoiced).to be_within(0.01).of(74.23)
    end
  end

  describe "call typing" do
    it "should have some ignored, and some intl and 1+canada" do
      # note $LegacyInvoicingEpoch = Date.new(2016,1,1) is set in test.rb
      i1 = invoices(:motherhubbard01)
      i1.recalc_derived
      #i1.invoice_debug = true
      expect(i1.invoice_calls.count).to eq(34)
      expect(i1.total_call_count).to    eq(32)
      expect(i1.subtotal_call_count(i1.client.btns.first.phones.by_extension[0].name)).to eq(15)
      expect(i1.subtotal_call_count(i1.client.btns.first.phones.by_extension[1].name)).to eq(17)
      expect(i1.callsummary(:bytype, :intl).count).to eq(1)
      expect(i1.callsummary(:bytype, :caribbean_calls).count).to eq(1)
      expect(i1.callsummary(:bytype, :usa_calls).count).to eq(1)
      expect(i1.callsummary(:bytype, :tollfree_recv).count).to eq(0)
      expect(i1.callsummary(:bytype, :canada_calls).count).to eq(29)
      expect(i1.callsummary(:byareacode, '905').count).to eq(23)
      expect(i1.callsummary(:byareacode, '419').count).to eq(1)
      expect(i1.callsummary(:byareacode, '519').count).to eq(6)
    end
  end

  describe "exporting" do
    it "should have three services listed" do
      i1 = invoices(:motherhubbard01)

      expect(i1.billable_services.count).to eq(5)
      expect(i1.billable_services).to include(billable_services(:bill1))
      expect(i1.billable_services.by_id.first.service).to eq(services(:service31))
    end

    it "should write some html files to designated directory" do
      i1 = invoices(:motherhubbard01)

      i1.recalc_derived
      outdir = Rails.root.join('tmp','invoices')
      i1.generate_invoice_to(outdir)

      invoicedir  = File.join(outdir, i1.invoice_subdir)
      invoicefile = File.join(invoicedir, 'invoice.html')
      reffile     = Rails.root.join('spec', 'files', 'mother_invoice.html')

      expect(File.exist?(invoicefile)).to be true
      puts "diff #{invoicefile} #{reffile}"
      expect(system "diff #{invoicefile} #{reffile}").to be true
    end

    it "should write an interim invoice to designated directory" do
      i1 = invoices(:motherhubbard01)

      i1.recalc_derived
      outdir = Rails.root.join('tmp','invoices')
      i1.generate_invoice_to(outdir, {interim_invoice: '2017-09-01'})

      invoicedir  = File.join(outdir, i1.invoice_subdir)
      invoicefile = File.join(invoicedir, 'invoice.html')
      reffile     = Rails.root.join('spec', 'files', 'mother_interim_invoice.html')

      expect(File.exist?(invoicefile)).to be true
      puts "diff #{invoicefile} #{reffile}"

      # NOTE: this keeps failing for me
      expect(system "diff #{invoicefile} #{reffile}").to be true
    end

    it "should validate may 2017 data analyzes the same" do
      ir1 = invoice_runs(:june2017)
      btn = btns(:mother0909)
      i1  = ir1.invoice_for_btn(btn)

      cnt = btn.outgoing_cdrs.count
      i1.recalc_derived
      expect(btn.outgoing_cdrs.count).to eq(cnt)
    end

    it "should count calls for an invoice for july 2017" do
      ir1 = invoice_runs(:july2017)
      btn = btns(:mother0909)
      i1  = ir1.invoice_for_btn(btn)
      i1.recalc_derived

      expect(i1.tollcalls.count).to eq(4)
    end

    it "should count calls for an invoice for june 2017" do
      ir1 = invoice_runs(:june2017)
      btn = btns(:mother0909)
      i1  = ir1.invoice_for_btn(btn)

      expect(i1.tollcalls.count).to eq(48)
    end

    it "should write an invoice for may 2017" do
      ir1 = invoice_runs(:june2017)
      btn = btns(:mother0909)
      i1  = ir1.invoice_for_btn(btn)

      i1.recalc_derived
      expect(i1.charges_start).to  eq('2017-05-01'.to_datetime)
      expect(i1.charges_finish).to eq('2017-05-28'.to_datetime)

      expect(i1.service_start).to  eq('2017-06-01'.to_datetime)
      expect(i1.service_finish).to eq('2017-06-30'.to_datetime)

      outdir = Rails.root.join('tmp','invoices')
      i1.export_to({outdir: outdir})

      invoicedir = File.join(outdir, i1.invoice_subdir)
      invoicefile = File.join(invoicedir, 'invoice.html')
      reffile     = Rails.root.join('spec', 'files', 'father_invoice.html')

      expect(File.exist?(invoicefile)).to be true
      puts "diff #{invoicefile} #{reffile}"
      expect(system "diff #{invoicefile} #{reffile}").to be true
    end

    it "should find legacy calls from May for a June 2017 invoice" do
      ir1 = invoice_runs(:june2017)
      btn = btns(:legacy1)
      expect(btn.incoming_cdrs.count).to eq(0)
      expect(btn.outgoing_cdrs.count).to eq(13)

      i1  = ir1.invoice_for_btn(btn)
      i1.collect_legacy_calls
      expect(i1.cdrs.count).to eq(13)

      outdir = Rails.root.join('tmp','invoices')
      i1.export_to({outdir: outdir})

      invoicedir  = File.join(outdir, i1.invoice_subdir)
      invoicefile = File.join(invoicedir, 'invoice.html')
      reffile     = Rails.root.join('spec', 'files', 'legacy_invoice.html')

      expect(File.exist?(invoicefile)).to be true
      puts "diff #{invoicefile} #{reffile}"
      expect(system "diff #{invoicefile} #{reffile}").to be true
    end

    it "should write a multi-department html invoice" do
      c1 = clients(:medguy)
      btn1=btns(:med5555556683)
      i1 = btn1.invoice_service_for(2017,7)

      i1.setup_department_invoices
      i1.migrate_billable_services_to_departments
      i1.recalc_derived
      i1.calc_subtotals
      outdir = Rails.root.join('tmp','invoices')
      i1.export_to({outdir: outdir})

      invoicedir  = File.join(outdir, i1.invoice_subdir)
      invoicefile = File.join(invoicedir, 'invoice.html')
      reffile     = Rails.root.join('spec', 'files', 'medguy_invoice.html')

      expect(File.exist?(invoicefile)).to be true
      puts "diff #{invoicefile} #{reffile}"
      expect(system "diff #{invoicefile} #{reffile}").to be true
    end

    it "specifies a path from which to load an invoice" do
      i1 = invoices(:motherhubbard01)

      # 2016/11/cycle300/18195550909/1010101-5/invoice.pdf
      expect(i1.path_to_pdf).to eq(File.join($InvoiceBaseUrl,
                                             i1.invoice_subdir, "invoice.pdf"))
    end

    it "specifies a basename for downloads" do
      i1 = invoices(:motherhubbard01)
      expect(i1.basename).to eq("201604-18195550909")
    end
  end

  describe "import balance" do
    it "should read line of balance" do
      row1 = ['Active','8195550909', 'Mother Hubbard', '$4,444.97']
      b1 = btns(:mother0909)

      stats = Hash.new(0)
      CreditMemo.import_historical_balance(row1, '2017-06-15'.to_date, stats)
      expect(stats[:payments]).to eq(1)
    end

    it "should read a file of payments" do
      i0 = invoices(:motherhubbard01)

      stats = Hash.new(0)
      CreditMemo.import_balance_file({file: 'spec/files/payments1.csv'}, stats)
      expect(stats[:comments]).to eq(1)
      expect(stats[:payments]).to eq(5)
      expect(stats[:zeros]).to    eq(2)
    end

    it "should have a negative balance" do
      bn1 = btns(:mother0909)
      expect(bn1.invoice_totals('2017-07-01'.to_date)).to be_within(0.01).of(4472.29)
    end

    it "should have no payment" do
      bn1 = btns(:mother0909)
      expect(bn1.payment_totals('2017-07-01'.to_date)).to be_within(0.01).of(241.93)
    end

    it "should have an initial negative balance" do
      bn1 = btns(:mother0909)
      expect(bn1.balance_as_of('2017-07-01'.to_date)).to be_within(0.01).of(-4230.36)
    end

    it "should set the historical payment values" do
      ir1 = invoice_runs(:june2017)
      btn = btns(:mother0909)
      i1  = ir1.invoice_for_btn(btn)
      i1.calc_payments

      expect(i1.last_amount_invoiced).to eq(214.61)
      expect(i1.last_amount_paid).to     eq(250)
    end
  end

  describe "#add_billable_service" do
    before do
      @invoice = invoices(:motherhubbard01)
      @billable_service = billable_services(:bill1)
    end

    it "adds a billable service to the invoice" do
      @invoice.add_billable_service(@billable_service)
      @invoice.reload && @invoice.billable_services.reload
      expect(@invoice.billable_services).to include(@billable_service)
    end
  end
end
