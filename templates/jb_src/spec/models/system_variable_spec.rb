require 'rails_helper'

describe SystemVariable do

  it "find a variable called bar" do
    l = system_variables(:one)
    expect(l).to_not be_nil
    expect(l.variable).to eq("bar")
    expect(l.value).to    eq("34")
  end

  it "should find variable bar via lookup" do
    l = SystemVariable.lookup(:bar)
    expect(l).to_not be_nil
    expect(l.variable).to eq("bar")
    expect(l.value).to    eq("34")
  end

  it "should make a new niceone variable" do
    l = SystemVariable.findormake(:niceone)
    expect(l.value).to be_nil
  end

  it "should support incrementing counter" do
    l = SystemVariable.nextval(:counter)
    expect(l).to eq(1)

    l = SystemVariable.nextval(:counter)
    expect(l).to eq(2)

    g = SystemVariable.findormake(:counter)
    expect(g.number).to eq(3)
  end

  describe "filechanged" do
    it "should consider a new file as changed" do
      expect(SystemVariable.filechanged(:filesym, "filesum1.txt",
                                        :filesum, "2e9b5a5254e84358188cad6313d38b4be35b182f358a3db8ffe4a5b6")).to be true
    end

    it "should consider a file with no sum as changed" do
      SystemVariable.set_string(:filesym, "filesum1.txt")
      expect(SystemVariable.filechanged(:filesym, "filesum1.txt",
                                        :filesum, "2e9b5a5254e84358188cad6313d38b4be35b182f358a3db8ffe4a5b6")).to be true
    end

    it "should consider a file with sum changed as changed" do
      SystemVariable.set_string(:filesym, "filesum1.txt")
      SystemVariable.set_string(:filesum, "2e9b5a5254e84358188cad63133459348534582f358a3db8ffe4a5b6")
      expect(SystemVariable.filechanged(:filesym, "filesum1.txt",
                                        :filesum, "2e9b5a5254e84358188cad6313d38b4be35b182f358a3db8ffe4a5b6")).to be true
    end

    it "should consider a file with sum same as not changed" do
      SystemVariable.set_string(:filesym, "filesum1.txt")
      sum = "2e9b5a5254e84358188cad6313d38b4be35b182f358a3db8ffe4a5b6"
      SystemVariable.set_string(:filesum, sum)
      expect(SystemVariable.filechanged(:filesym, "filesum1.txt",
                                        :filesum, sum)).to be false
    end
  end

end
