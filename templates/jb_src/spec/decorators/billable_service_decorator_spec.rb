describe BillableServiceDecorator do
  describe "#phone_number" do
    context "with an associated phone" do
      before { @billable_service = create(:billable_service_with_phone) }
      subject { BillableServiceDecorator.new(@billable_service) }

      it "renders the phone number wrapped in a span tag" do
        expect(subject.phone_number).to eq("<span class=\"bs-phone-number\">#{@billable_service.phone_number}</span>")
      end
    end

    context "without an associated phone" do
      before { @billable_service = create(:billable_service, asset: nil) }
      subject { BillableServiceDecorator.new(@billable_service) }

      it "renders a dash" do
        expect(subject.phone_number).to eq("<span class=\"bs-phone-number\">-</span>")
      end

      # it "warns of missing phone in the logs" do
      #   expect(Rails.logger).to receive(:warn).with "BillableService : #{@billable_service.name} (#{@billable_service.id}) is missing an associated phone."
      #   subject.phone_number
      # end
    end
  end
end
