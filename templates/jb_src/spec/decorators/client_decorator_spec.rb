require 'spec_helper'

describe ClientDecorator do
  before  { @client = clients(:novavision) }
  subject { ClientDecorator.new(@client) }

  it "should render a salesperson name" do
    expect(subject.salesperson).to match("Steve Foisy")
  end
end
