describe ApplicationHelper, type: :helper do
  describe "#bold_match" do
    context "given regexp chars" do
      it "does not raise an error" do
        query = '\A.*| \s*\d/ \\\/\!@# $%^&*) asdf\z'
        expect{ helper.bold_match('foobar', query) }.not_to raise_error
      end
    end

    context "given a client with unclosed parenthesis" do
      it "does not raise an error" do
        query = 'Centre Visuel Optika (9003-836'
        expect{ helper.bold_match('Centre Visuel Optika (9003-836', query) }.not_to raise_error
      end

      it "returns the matches in bold" do
        query = 'Centre Visuel Optika (9003-836'
        expect(helper.bold_match('Centre Visuel Optika (9003-836', query)).to eq('<b>Centre</b> <b>Visuel</b> <b>Optika</b> <b>(9003-836</b>')
      end
    end
  end

  describe "#has_invoice_cycle?" do
    let(:client) { instance_double('client') }

    it "returns false if client is nil" do
      expect(helper.has_invoice_cycle?(nil)).to be false
    end

    it "returns true if client has a Stock commercial invoice cycle" do
      allow(client).to receive(:invoice_cycle).and_return('100-200')
      expect(helper.has_invoice_cycle?(client)).to be true
    end

    it "returns true if client has a Co-operative invoice cycle" do
      allow(client).to receive(:invoice_cycle).and_return('300-400')
      expect(helper.has_invoice_cycle?(client)).to be true
    end

    it "returns true if client has a Co-operative invoice cycle" do
      allow(client).to receive(:invoice_cycle).and_return('101-301')
      expect(helper.has_invoice_cycle?(client)).to be true
    end

    it "returns false if client has Support Only invoices cycle" do
      allow(client).to receive(:invoice_cycle).and_return('support')
      expect(helper.has_invoice_cycle?(client)).to be false
    end

    it "returns false if client has no invoice cycle" do
      allow(client).to receive(:invoice_cycle).and_return(nil)
      expect(helper.has_invoice_cycle?(client)).to be false
    end

    it "returns false if client has blank invoice cycle" do
      allow(client).to receive(:invoice_cycle).and_return('')
      expect(helper.has_invoice_cycle?(client)).to be false
    end
  end
end
