include ApplicationHelper

describe "layouts/application" do

  context "given a signed in admin user" do
    before do
      @admin = create(:user, admin: true)
      sign_in(@admin)
    end

    it "renders admin link" do
      render

      assert_select "li > a[href=/admin/users]", text: 'Users'
    end
  end

  context "given a mortal user" do
    before do
      @user = create(:user, admin: false)
      sign_in(@user)
    end

    it "does not renders admin link" do
      render

      assert_select "li > a[href=/admin/users]", false, text: 'Users'
    end
  end

end
