describe "services/edit", type: :view do
  fixtures :services

  before(:each) do
    @service = assign(:service, services(:service31))
  end

  it "renders the edit service form" do
    render

    assert_select "form[action=?][method=?]", service_path(@service), "post" do

      assert_select "textarea#service_description[name=?]", "service[description]"

      assert_select "textarea#service_name[name=?]", "service[name]"

      assert_select "input#service_unitprice[name=?]", "service[unitprice]"

      assert_select "input#service_renewable[name=?]", "service[renewable]"

      assert_select "input#service_available[name=?]", "service[available]"
    end
  end
end
