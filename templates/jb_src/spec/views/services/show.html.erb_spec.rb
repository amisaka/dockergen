describe "services/show", type: :view do
  fixtures :services

  before(:each) do
    @service = assign(:service, services(:service31))
  end

  it "renders attributes in <p>" do
    render

    assert_select "p", text: /#{@service.name}/
    assert_select "p", text: /#{@service.description}/
    assert_select "p", text: /#{@service.unitprice}/
    assert_select "p", text: /#{@service.available}/
    assert_select "p", text: /#{@service.renewable}/
  end
end
