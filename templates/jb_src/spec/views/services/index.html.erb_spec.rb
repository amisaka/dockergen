describe "services/index", type: :view do
  fixtures :services

  before(:each) do
    @services = assign(:services, Kaminari.paginate_array([services(:service31), services(:service32)]).page(0))
  end

  it "renders a list of services" do
    render

    @services.each do |service|
      assert_select "tr>td", text: /#{service.name}/
      assert_select "tr>td", text: /#{service.description}/
      assert_select "tr>td", text: /#{service.unitprice}/
      assert_select "tr>td", text: /#{service.available}/
      assert_select "tr>td", text: /#{service.renewable}/
    end
  end
end
