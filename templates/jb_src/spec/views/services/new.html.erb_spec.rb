describe "services/new", type: :view do
  fixtures :services

  before(:each) do
    assign(:service, Service.new)
  end

  it "renders new service form" do
    render

    assert_select "form[action=?][method=?]", services_path, "post" do

      assert_select "textarea#service_description[name=?]", "service[description]"

      assert_select "textarea#service_name[name=?]", "service[name]"

      assert_select "input#service_unitprice[name=?]", "service[unitprice]"

      assert_select "input#service_renewable[name=?]", "service[renewable]"

      assert_select "input#service_available[name=?]", "service[available]"
    end
  end
end
