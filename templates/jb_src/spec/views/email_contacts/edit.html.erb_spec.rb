require 'rails_helper'

RSpec.describe "email_contacts/edit", type: :view do
  before(:each) do
    @email_contact = assign(:email_contact, EmailContact.create!())
  end

  it "renders the edit email_contact form" do
    render

    assert_select "form[action=?][method=?]", email_contact_path(@email_contact), "post" do
    end
  end
end
