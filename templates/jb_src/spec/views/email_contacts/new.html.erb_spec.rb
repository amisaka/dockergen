require 'rails_helper'

RSpec.describe "email_contacts/new", type: :view do
  before(:each) do
    assign(:email_contact, EmailContact.new())
  end

  it "renders new email_contact form" do
    render

    assert_select "form[action=?][method=?]", email_contacts_path, "post" do
    end
  end
end
