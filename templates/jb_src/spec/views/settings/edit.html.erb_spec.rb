require 'rails_helper'

RSpec.describe "settings/edit", type: :view do
  before(:each) do
    @setting = assign(:setting, Setting.create!(
      :name => "MyText",
      :value => "MyText"
    ))
  end

  it "renders the edit setting form" do
    render

    assert_select "form[action=?][method=?]", setting_path(@setting), "post" do

      assert_select "textarea#setting_name[name=?]", "setting[name]"

      assert_select "textarea#setting_value[name=?]", "setting[value]"
    end
  end
end
