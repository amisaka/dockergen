describe "clients/edit", type: :view do

  before(:each) do
    @client = assign(:client, clients(:credil))
  end

  describe "form" do
    before { render }

    it "renders a form to edit client" do
      expect(rendered).to have_css 'form.edit_client'
    end

    it "has a submit button" do
      expect(rendered).to have_css 'form.edit_client input[type=submit]'
    end

    it "renders within a box layout" do
      expect(rendered).to have_css 'form.edit_client .box'
    end

    describe '.form-group' do
      let(:page) {Capybara::Node::Simple.new( rendered ) }

      it "should render 14 form groups" do
        expect(page.all('.form-group').count).to eq(11)
      end

      it "renders input with form-control class" do
        expect(rendered).to have_css('.form-group .form-control')
      end
    end

  end
end
