describe 'clients/sites/_sites.html.erb', type: :view do
  describe 'table' do
    subject do
      @client = assign(:client, create(:client))
      @sites = assign(:sites, create_list(:site, 10).collect { |s| s.decorate })
    end

    before { render partial: 'clients/sites/sites', locals: { sites: subject } }

    let(:page) {Capybara::Node::Simple.new( rendered ) }

    it "renders sites in a table" do
      expect(rendered).to have_css('table.table.table-bordered')
    end

    it "renders all sites in table row" do
      expect(page.all('tr.site-row.site').count).to eq(10)

      @sites.each do |site|
        expect(rendered).to have_css('tr > td.site-name', text: site.name)
        expect(rendered).to have_css('tr > td.site-building', text: site.building)
      end
    end
  end
end
