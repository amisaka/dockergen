describe "clients/index", type: :view do
  fixtures :clients

  before(:each) do
    @clients = assign(:clients, Kaminari.paginate_array( \
      ClientDecorator.decorate_collection( \
        create_list(:client, 10))).page(0))
    @client_search = ClientSearch.new
  end

  it "renders a list of clients" do
    render

    expect(rendered).to have_css('tr > th', text: 'Name')
    expect(rendered).to have_css('tr > th', text: 'Address')
    expect(rendered).to have_css('tr > th', text: 'Contact')
    expect(rendered).to have_css('tr > th', text: 'Status')
    expect(rendered).to have_css('tr > th', text: 'Invoice Cycle')
    expect(rendered).to have_css('tr > th', text: 'Site')

    @clients.each do |client|
      expect(rendered).to have_css('tr > td', text: client.name)
      expect(rendered).to have_css('tr > td', text: client.billing_site.billing_address_1)
      expect(rendered).to have_css('tr > td', text: client.admin_contact)
      expect(rendered).to have_css('tr > td', text: client.billing_site)
    end
  end
end
