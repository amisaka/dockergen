describe "clients/show", type: :view do
  fixtures :clients

  before(:each) do
    @client = assign(:client, ClientDecorator.new(create(:client_with_btns)) )
  end

  it "renders attributes in <p>" do
    # render # NOTE: Something is wrong with render
    render partial: 'clients/client', locals: { client: @client }

    expect(rendered).to render_template partial: 'clients/_client'
    # expect(rendered).to render_template partial: 'shared/_box'
    # expect(rendered).to render_template partial: 'clients/btns/_btns'
    # expect(rendered).to render_template partial: 'buildings/_buildings'
    # expect(rendered).to render_template partial: 'clients/billable_services/_billable_services'
    # expect(rendered).to render_template partial: 'clients/phones/_phones'

    expect(rendered).to match 'Name'
    expect(rendered).to match 'Address'
    expect(rendered).to match 'Language'
    expect(rendered).to match 'Sales Person'

    expect(rendered).to include(@client.name)
    expect(rendered).to include(@client.billing_address)
    expect(rendered).to match /#{@client.language_preference}/

    # assert_select "tr > th", text: 'Name :', count: 1
    # assert_select "tr > th", text: 'Address :', count: 1
    # assert_select "tr > th", text: 'Language :', count: 1
    #
    # assert_select "tr > td", text: /#{@client.name}/
    # assert_select "tr > td", text: /#{@client.billing_address}/
    # assert_select "tr > td", text: /#{@client.language_preference}/
  end

  it "renders a link to the selected invoice" do
    @client = assign(:client, clients(:motherhubbard).decorate)
    @selectable_btns = assign(:selectable_btns, @client.btns.decorate)
    @selected_btn    = assign(:selected_btn, @selectable_btns.first)
    @selectable_invoices = assign(:selectable_invoices, @client.invoices.decorate)
    @selected_invoice = assign(:selected_invoice, invoices(:motherhubbard01).decorate)

    @bs_accessor = BillableServiceAccessor.new(@client)
    @billable_services = @bs_accessor.for(@selected_btn, @selected_invoice).page(params[:services_page]).per(10)
    @billable_services = assign(:billable_services, @billable_services.decorate) unless @billable_services.empty?
    # for a new form
    @billable_service = assign(:billable_service, BillableService.new)
    @selectable_services ||= assign(:selectable_services, Service.limit(25))
    @selectable_sites    = assign(:selectable_sites, @client.sites)

    # this line might be needed if we ever remove the above fixtures
    # allow(@client).to receive(:invoice_cycle).and_return('100-200')

    render partial: 'billable_services/billable_services', locals: {
             billable_services: @billable_services,
             total_billable_services_price: 10.75,
           }

    expect(rendered).to render_template partial: 'billable_services/_billable_services'

    expect(rendered).to match /PDF/
  end
end
