describe "clients/new", type: :view do
  before(:each) do
    @client = Client.new
    @crm_account = @client.crm_account = CrmAccount.new
    @billing_site = @client.billing_site = Site.new
    @client.sites << @billing_site
    @client.billing_site.building = Building.new
    @client.btns.build

    assign(:client, @client)
  end

  describe "form" do
    before { render }

    it "renders a form to create a new client" do
      expect(rendered).to have_css 'form.new_client'
    end

    it "has a submit button" do
      expect(rendered).to have_css 'form.new_client input[type=submit]'
    end

    it "renders within a box layout" do
      expect(rendered).to have_css 'form.new_client .box'
    end

    describe '.form-group' do
      let(:page) {Capybara::Node::Simple.new( rendered ) }

      it "should render 12 form groups" do
        expect(page.all('form.new_client .form-group').count).to eq(11)
      end

      it "renders input with form-control class" do
        expect(rendered).to have_css('.form-group .form-control')
      end
    end

  end
end
