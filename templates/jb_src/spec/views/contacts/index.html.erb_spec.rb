require 'rails_helper'

RSpec.describe "contacts/index", type: :view do
  before(:each) do
    assign(:contacts, [
      Contact.create!(
        :email => "Email",
        :name => "Name",
        :role => "Role",
        :phone_number => "Phone Number",
        :active => false
      ),
      Contact.create!(
        :email => "Email",
        :name => "Name",
        :role => "Role",
        :phone_number => "Phone Number",
        :active => false
      )
    ])
  end

  it "renders a list of contacts" do
    render
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Role".to_s, :count => 2
    assert_select "tr>td", :text => "Phone Number".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
