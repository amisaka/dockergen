require 'rails_helper'

RSpec.describe "contacts/edit", type: :view do
  before(:each) do
    @contact = assign(:contact, Contact.create!(
      :email => "MyString",
      :name => "MyString",
      :role => "MyString",
      :phone_number => "MyString",
      :active => false
    ))
  end

  it "renders the edit contact form" do
    render

    assert_select "form[action=?][method=?]", contact_path(@contact), "post" do

      assert_select "input#contact_email[name=?]", "contact[email]"

      assert_select "input#contact_name[name=?]", "contact[name]"

      assert_select "input#contact_role[name=?]", "contact[role]"

      assert_select "input#contact_phone_number[name=?]", "contact[phone_number]"

      assert_select "input#contact_active[name=?]", "contact[active]"
    end
  end
end
