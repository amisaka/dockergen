describe 'sites/_sites.html.erb', type: :view do
  describe 'table' do
    subject { @sites = assign(:sites, create_list(:site, 10)) }
    before { render partial: 'sites/sites', locals: { sites: subject } }

    let(:page) {Capybara::Node::Simple.new( rendered )}

    it "renders sites in a table" do
      expect(rendered).to have_css('table.table.table-bordered')
    end

    it "renders all sites in table row" do
      expect(page.all('tr.site-row.site').count).to eq(10)

      @sites.each do |site|
        expect(rendered).to have_css('tr > td.site-id', text: site.id)
        expect(rendered).to have_css('tr > td.site-name', text: site.name)
        expect(rendered).to have_css('tr > td.site-building', text: site.building)
        expect(rendered).to have_css('tr > td.site-city', text: site.city)
        expect(rendered).to have_css('tr > td.site-unitnumber', text: site.unitnumber)
      end
    end
  end
end
