describe "btns/show", type: :view do

  before(:each) do
    @btn = assign(:btn, create(:btn).decorate)
  end

  it "renders btns attributes in a box" do
    render

    assert_select ".box" do
      assert_select "table.table" do
        assert_select 'tr > th', text: 'Billing Telephone Number :'
        assert_select 'tr > th', text: 'Start Date :'
        assert_select 'tr > th', text: 'End Date :'

        assert_select 'tr > td', text: @btn.billingtelephonenumber
        assert_select 'tr > td', text: @btn.start_date.to_s
        assert_select 'tr > td', text: @btn.end_date.to_s
      end
    end
  end
end
