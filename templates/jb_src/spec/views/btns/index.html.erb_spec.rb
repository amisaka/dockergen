describe "btns/index", type: :view do
  fixtures :btns

  before(:each) do
    @btns = assign(:btns, Kaminari.paginate_array([btns(:credil1), btns(:credil2)]).page(0))
  end

  it "renders a list of btns" do
    render

    assert_select "tr>th", text: "Billing Telephone Number"
    assert_select "tr>th", text: "Phones"
    assert_select "tr>th", text: "Start Date"
    assert_select "tr>th", text: "End Date"

    @btns.each do |btn|
      assert_select "tr\#btn-row-#{btn.id}>td.btn-row-btn", text: btn.billingtelephonenumber
      assert_select "tr\#btn-row-#{btn.id}>td.btn-row-phn" do
        if btn.phones.any?
          assert_select "ul" do
            btn.phones.each do |phone|
              assert_select "li", text: phone.phone_number
            end
          end
        end
      end
      assert_select "tr\#btn-row-#{btn.id}>td.btn-row-sdt", text: btn.start_date.to_s
      assert_select "tr\#btn-row-#{btn.id}>td.btn-row-edt", text: btn.end_date.to_s
    end
  end
end
