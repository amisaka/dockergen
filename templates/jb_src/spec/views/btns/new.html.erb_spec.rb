describe "btns/new", type: :view do
  fixtures :btns

  before(:each) do
    assign(:btn, Btn.new(btns(:credil1).attributes))
  end

  it "renders new btn form" do
    render

    assert_select "form[action=?][method=?]", btns_path, "post" do

      assert_select "input#btn_billingtelephonenumber[name=?]", "btn[billingtelephonenumber]"
    end
  end
end
