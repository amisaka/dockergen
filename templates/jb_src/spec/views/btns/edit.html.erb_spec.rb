describe "btns/edit", type: :view do
  fixtures :btns

  before(:each) do
    @btn = assign(:btn, btns(:credil1))
  end

  it "renders the edit btn form" do
    render

    assert_select "form[action=?][method=?]", btn_path(@btn), "post" do

      assert_select "input#btn_billingtelephonenumber[name=?]", "btn[billingtelephonenumber]"
    end
  end
end
