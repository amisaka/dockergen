require 'rails_helper'

RSpec.describe "inventories/index", type: :view do
  before(:each) do
    assign(:inventories, [
      Inventory.create!(
        :address1 => "Address1",
        :adresse_ip_lan_data => "Adresse Ip Lan Data",
        :adresse_ip_lan_voip => "Adresse Ip Lan Voip",
        :adresse_ip_link_data => "Adresse Ip Link Data",
        :adresse_ip_link_voip => "Adresse Ip Link Voip",
        :adresse_ipv6_lan_data => "Adresse Ipv6 Lan Data",
        :adresse_ipv6_lan_voip => "Adresse Ipv6 Lan Voip",
        :adresse_ipv6_link_data => "Adresse Ipv6 Link Data",
        :adresse_ipv6_link_voip => "Adresse Ipv6 Link Voip",
        :agreement_num => "Agreement Num",
        :auto_renewal_term => "Auto Renewal Term",
        :billingtelephonenumber => "Billingtelephonenumber",
        :btn => "Btn",
        :circuit_num_logique => "Circuit Num Logique",
        :circuit_num_physique => "Circuit Num Physique",
        :circuit_num => "Circuit Num",
        :city => "City",
        :compte_fttb => "Compte Fttb",
        :customer_name => "Customer Name",
        :customer_sn => "Customer Sn",
        :deleted => false,
        :description => "Description",
        :devicenamemonitored => "Devicenamemonitored",
        :device => "Device",
        :did_supplier_id => 2,
        :download_speed => "Download Speed",
        :email_of_courrier => "Email Of Courrier",
        :installed_on_line => "Installed On Line",
        :inventory_type => "Inventory Type",
        :inventory_type_id => 3,
        :mac_address => "Mac Address",
        :numerodereference => "Numerodereference",
        :parent_agreement_num => "Parent Agreement Num",
        :password => "Password",
        :product_id => 4,
        :product_name => "Product Name",
        :quantity => "Quantity",
        :realsyncspeeddownload => "Realsyncspeeddownload",
        :realsyncspeedupload => "Realsyncspeedupload",
        :router_principal => "Router Principal",
        :router_vrrp => "Router Vrrp",
        :service_id => "Service",
        :site_id => 5,
        :site_name => "Site Name",
        :site_number => "Site Number",
        :sous_interface_data => "Sous Interface Data",
        :sous_interface_voip => "Sous Interface Voip",
        :sous_interface_voip_vrrp => "Sous Interface Voip Vrrp",
        :supplier_num => "Supplier Num",
        :supplier => "Supplier",
        :terminate_on => "Terminate On",
        :term => "Term",
        :upload_speed => "Upload Speed",
        :username => "Username",
        :vlan_data_q_in_q => "Vlan Data Q In Q",
        :vlan_supplier => "Vlan Supplier",
        :vlan_voip_q_in_q => "Vlan Voip Q In Q",
        :vl_videotron => "Vl Videotron",
        :vrrp_id => "Vrrp",
        :z_end => "Z End"
      ),
      Inventory.create!(
        :address1 => "Address1",
        :adresse_ip_lan_data => "Adresse Ip Lan Data",
        :adresse_ip_lan_voip => "Adresse Ip Lan Voip",
        :adresse_ip_link_data => "Adresse Ip Link Data",
        :adresse_ip_link_voip => "Adresse Ip Link Voip",
        :adresse_ipv6_lan_data => "Adresse Ipv6 Lan Data",
        :adresse_ipv6_lan_voip => "Adresse Ipv6 Lan Voip",
        :adresse_ipv6_link_data => "Adresse Ipv6 Link Data",
        :adresse_ipv6_link_voip => "Adresse Ipv6 Link Voip",
        :agreement_num => "Agreement Num",
        :auto_renewal_term => "Auto Renewal Term",
        :billingtelephonenumber => "Billingtelephonenumber",
        :btn => "Btn",
        :circuit_num_logique => "Circuit Num Logique",
        :circuit_num_physique => "Circuit Num Physique",
        :circuit_num => "Circuit Num",
        :city => "City",
        :compte_fttb => "Compte Fttb",
        :customer_name => "Customer Name",
        :customer_sn => "Customer Sn",
        :deleted => false,
        :description => "Description",
        :devicenamemonitored => "Devicenamemonitored",
        :device => "Device",
        :did_supplier_id => 2,
        :download_speed => "Download Speed",
        :email_of_courrier => "Email Of Courrier",
        :installed_on_line => "Installed On Line",
        :inventory_type => "Inventory Type",
        :inventory_type_id => 3,
        :mac_address => "Mac Address",
        :numerodereference => "Numerodereference",
        :parent_agreement_num => "Parent Agreement Num",
        :password => "Password",
        :product_id => 4,
        :product_name => "Product Name",
        :quantity => "Quantity",
        :realsyncspeeddownload => "Realsyncspeeddownload",
        :realsyncspeedupload => "Realsyncspeedupload",
        :router_principal => "Router Principal",
        :router_vrrp => "Router Vrrp",
        :service_id => "Service",
        :site_id => 5,
        :site_name => "Site Name",
        :site_number => "Site Number",
        :sous_interface_data => "Sous Interface Data",
        :sous_interface_voip => "Sous Interface Voip",
        :sous_interface_voip_vrrp => "Sous Interface Voip Vrrp",
        :supplier_num => "Supplier Num",
        :supplier => "Supplier",
        :terminate_on => "Terminate On",
        :term => "Term",
        :upload_speed => "Upload Speed",
        :username => "Username",
        :vlan_data_q_in_q => "Vlan Data Q In Q",
        :vlan_supplier => "Vlan Supplier",
        :vlan_voip_q_in_q => "Vlan Voip Q In Q",
        :vl_videotron => "Vl Videotron",
        :vrrp_id => "Vrrp",
        :z_end => "Z End"
      )
    ])
  end

  it "renders a list of inventories" do
    render
    assert_select "tr>td", :text => "Address1".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ip Lan Data".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ip Lan Voip".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ip Link Data".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ip Link Voip".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ipv6 Lan Data".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ipv6 Lan Voip".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ipv6 Link Data".to_s, :count => 2
    assert_select "tr>td", :text => "Adresse Ipv6 Link Voip".to_s, :count => 2
    assert_select "tr>td", :text => "Agreement Num".to_s, :count => 2
    assert_select "tr>td", :text => "Auto Renewal Term".to_s, :count => 2
    assert_select "tr>td", :text => "Billingtelephonenumber".to_s, :count => 2
    assert_select "tr>td", :text => "Btn".to_s, :count => 2
    assert_select "tr>td", :text => "Circuit Num Logique".to_s, :count => 2
    assert_select "tr>td", :text => "Circuit Num Physique".to_s, :count => 2
    assert_select "tr>td", :text => "Circuit Num".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Compte Fttb".to_s, :count => 2
    assert_select "tr>td", :text => "Customer Name".to_s, :count => 2
    assert_select "tr>td", :text => "Customer Sn".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Devicenamemonitored".to_s, :count => 2
    assert_select "tr>td", :text => "Device".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Download Speed".to_s, :count => 2
    assert_select "tr>td", :text => "Email Of Courrier".to_s, :count => 2
    assert_select "tr>td", :text => "Installed On Line".to_s, :count => 2
    assert_select "tr>td", :text => "Inventory Type".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Mac Address".to_s, :count => 2
    assert_select "tr>td", :text => "Numerodereference".to_s, :count => 2
    assert_select "tr>td", :text => "Parent Agreement Num".to_s, :count => 2
    assert_select "tr>td", :text => "Password".to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "Product Name".to_s, :count => 2
    assert_select "tr>td", :text => "Quantity".to_s, :count => 2
    assert_select "tr>td", :text => "Realsyncspeeddownload".to_s, :count => 2
    assert_select "tr>td", :text => "Realsyncspeedupload".to_s, :count => 2
    assert_select "tr>td", :text => "Router Principal".to_s, :count => 2
    assert_select "tr>td", :text => "Router Vrrp".to_s, :count => 2
    assert_select "tr>td", :text => "Service".to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => "Site Name".to_s, :count => 2
    assert_select "tr>td", :text => "Site Number".to_s, :count => 2
    assert_select "tr>td", :text => "Sous Interface Data".to_s, :count => 2
    assert_select "tr>td", :text => "Sous Interface Voip".to_s, :count => 2
    assert_select "tr>td", :text => "Sous Interface Voip Vrrp".to_s, :count => 2
    assert_select "tr>td", :text => "Supplier Num".to_s, :count => 2
    assert_select "tr>td", :text => "Supplier".to_s, :count => 2
    assert_select "tr>td", :text => "Terminate On".to_s, :count => 2
    assert_select "tr>td", :text => "Term".to_s, :count => 2
    assert_select "tr>td", :text => "Upload Speed".to_s, :count => 2
    assert_select "tr>td", :text => "Username".to_s, :count => 2
    assert_select "tr>td", :text => "Vlan Data Q In Q".to_s, :count => 2
    assert_select "tr>td", :text => "Vlan Supplier".to_s, :count => 2
    assert_select "tr>td", :text => "Vlan Voip Q In Q".to_s, :count => 2
    assert_select "tr>td", :text => "Vl Videotron".to_s, :count => 2
    assert_select "tr>td", :text => "Vrrp".to_s, :count => 2
    assert_select "tr>td", :text => "Z End".to_s, :count => 2
  end
end
