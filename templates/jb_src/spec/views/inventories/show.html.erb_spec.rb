require 'rails_helper'

RSpec.describe "inventories/show", type: :view do
  before(:each) do
    @inventory = assign(:inventory, Inventory.create!(
      :address1 => "Address1",
      :adresse_ip_lan_data => "Adresse Ip Lan Data",
      :adresse_ip_lan_voip => "Adresse Ip Lan Voip",
      :adresse_ip_link_data => "Adresse Ip Link Data",
      :adresse_ip_link_voip => "Adresse Ip Link Voip",
      :adresse_ipv6_lan_data => "Adresse Ipv6 Lan Data",
      :adresse_ipv6_lan_voip => "Adresse Ipv6 Lan Voip",
      :adresse_ipv6_link_data => "Adresse Ipv6 Link Data",
      :adresse_ipv6_link_voip => "Adresse Ipv6 Link Voip",
      :agreement_num => "Agreement Num",
      :auto_renewal_term => "Auto Renewal Term",
      :billingtelephonenumber => "Billingtelephonenumber",
      :btn => "Btn",
      :circuit_num_logique => "Circuit Num Logique",
      :circuit_num_physique => "Circuit Num Physique",
      :circuit_num => "Circuit Num",
      :city => "City",
      :compte_fttb => "Compte Fttb",
      :customer_name => "Customer Name",
      :customer_sn => "Customer Sn",
      :deleted => false,
      :description => "Description",
      :devicenamemonitored => "Devicenamemonitored",
      :device => "Device",
      :did_supplier_id => 2,
      :download_speed => "Download Speed",
      :email_of_courrier => "Email Of Courrier",
      :installed_on_line => "Installed On Line",
      :inventory_type => "Inventory Type",
      :inventory_type_id => 3,
      :mac_address => "Mac Address",
      :numerodereference => "Numerodereference",
      :parent_agreement_num => "Parent Agreement Num",
      :password => "Password",
      :product_id => 4,
      :product_name => "Product Name",
      :quantity => "Quantity",
      :realsyncspeeddownload => "Realsyncspeeddownload",
      :realsyncspeedupload => "Realsyncspeedupload",
      :router_principal => "Router Principal",
      :router_vrrp => "Router Vrrp",
      :service_id => "Service",
      :site_id => 5,
      :site_name => "Site Name",
      :site_number => "Site Number",
      :sous_interface_data => "Sous Interface Data",
      :sous_interface_voip => "Sous Interface Voip",
      :sous_interface_voip_vrrp => "Sous Interface Voip Vrrp",
      :supplier_num => "Supplier Num",
      :supplier => "Supplier",
      :terminate_on => "Terminate On",
      :term => "Term",
      :upload_speed => "Upload Speed",
      :username => "Username",
      :vlan_data_q_in_q => "Vlan Data Q In Q",
      :vlan_supplier => "Vlan Supplier",
      :vlan_voip_q_in_q => "Vlan Voip Q In Q",
      :vl_videotron => "Vl Videotron",
      :vrrp_id => "Vrrp",
      :z_end => "Z End"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Address1/)
    expect(rendered).to match(/Adresse Ip Lan Data/)
    expect(rendered).to match(/Adresse Ip Lan Voip/)
    expect(rendered).to match(/Adresse Ip Link Data/)
    expect(rendered).to match(/Adresse Ip Link Voip/)
    expect(rendered).to match(/Adresse Ipv6 Lan Data/)
    expect(rendered).to match(/Adresse Ipv6 Lan Voip/)
    expect(rendered).to match(/Adresse Ipv6 Link Data/)
    expect(rendered).to match(/Adresse Ipv6 Link Voip/)
    expect(rendered).to match(/Agreement Num/)
    expect(rendered).to match(/Auto Renewal Term/)
    expect(rendered).to match(/Billingtelephonenumber/)
    expect(rendered).to match(/Btn/)
    expect(rendered).to match(/Circuit Num Logique/)
    expect(rendered).to match(/Circuit Num Physique/)
    expect(rendered).to match(/Circuit Num/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Compte Fttb/)
    expect(rendered).to match(/Customer Name/)
    expect(rendered).to match(/Customer Sn/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/Devicenamemonitored/)
    expect(rendered).to match(/Device/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Download Speed/)
    expect(rendered).to match(/Email Of Courrier/)
    expect(rendered).to match(/Installed On Line/)
    expect(rendered).to match(/Inventory Type/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Mac Address/)
    expect(rendered).to match(/Numerodereference/)
    expect(rendered).to match(/Parent Agreement Num/)
    expect(rendered).to match(/Password/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/Product Name/)
    expect(rendered).to match(/Quantity/)
    expect(rendered).to match(/Realsyncspeeddownload/)
    expect(rendered).to match(/Realsyncspeedupload/)
    expect(rendered).to match(/Router Principal/)
    expect(rendered).to match(/Router Vrrp/)
    expect(rendered).to match(/Service/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/Site Name/)
    expect(rendered).to match(/Site Number/)
    expect(rendered).to match(/Sous Interface Data/)
    expect(rendered).to match(/Sous Interface Voip/)
    expect(rendered).to match(/Sous Interface Voip Vrrp/)
    expect(rendered).to match(/Supplier Num/)
    expect(rendered).to match(/Supplier/)
    expect(rendered).to match(/Terminate On/)
    expect(rendered).to match(/Term/)
    expect(rendered).to match(/Upload Speed/)
    expect(rendered).to match(/Username/)
    expect(rendered).to match(/Vlan Data Q In Q/)
    expect(rendered).to match(/Vlan Supplier/)
    expect(rendered).to match(/Vlan Voip Q In Q/)
    expect(rendered).to match(/Vl Videotron/)
    expect(rendered).to match(/Vrrp/)
    expect(rendered).to match(/Z End/)
  end
end
