require 'rails_helper'

RSpec.describe "inventories/new", type: :view do
  before(:each) do
    assign(:inventory, Inventory.new(
      :address1 => "MyString",
      :adresse_ip_lan_data => "MyString",
      :adresse_ip_lan_voip => "MyString",
      :adresse_ip_link_data => "MyString",
      :adresse_ip_link_voip => "MyString",
      :adresse_ipv6_lan_data => "MyString",
      :adresse_ipv6_lan_voip => "MyString",
      :adresse_ipv6_link_data => "MyString",
      :adresse_ipv6_link_voip => "MyString",
      :agreement_num => "MyString",
      :auto_renewal_term => "MyString",
      :billingtelephonenumber => "MyString",
      :btn => "MyString",
      :circuit_num_logique => "MyString",
      :circuit_num_physique => "MyString",
      :circuit_num => "MyString",
      :city => "MyString",
      :compte_fttb => "MyString",
      :customer_name => "MyString",
      :customer_sn => "MyString",
      :deleted => false,
      :description => "MyString",
      :devicenamemonitored => "MyString",
      :device => "MyString",
      :did_supplier_id => 1,
      :download_speed => "MyString",
      :email_of_courrier => "MyString",
      :installed_on_line => "MyString",
      :inventory_type => "MyString",
      :inventory_type_id => 1,
      :mac_address => "MyString",
      :numerodereference => "MyString",
      :parent_agreement_num => "MyString",
      :password => "MyString",
      :product_id => 1,
      :product_name => "MyString",
      :quantity => "MyString",
      :realsyncspeeddownload => "MyString",
      :realsyncspeedupload => "MyString",
      :router_principal => "MyString",
      :router_vrrp => "MyString",
      :service_id => "MyString",
      :site_id => 1,
      :site_name => "MyString",
      :site_number => "MyString",
      :sous_interface_data => "MyString",
      :sous_interface_voip => "MyString",
      :sous_interface_voip_vrrp => "MyString",
      :supplier_num => "MyString",
      :supplier => "MyString",
      :terminate_on => "MyString",
      :term => "MyString",
      :upload_speed => "MyString",
      :username => "MyString",
      :vlan_data_q_in_q => "MyString",
      :vlan_supplier => "MyString",
      :vlan_voip_q_in_q => "MyString",
      :vl_videotron => "MyString",
      :vrrp_id => "MyString",
      :z_end => "MyString"
    ))
  end

  it "renders new inventory form" do
    render

    assert_select "form[action=?][method=?]", inventories_path, "post" do

      assert_select "input#inventory_address1[name=?]", "inventory[address1]"

      assert_select "input#inventory_adresse_ip_lan_data[name=?]", "inventory[adresse_ip_lan_data]"

      assert_select "input#inventory_adresse_ip_lan_voip[name=?]", "inventory[adresse_ip_lan_voip]"

      assert_select "input#inventory_adresse_ip_link_data[name=?]", "inventory[adresse_ip_link_data]"

      assert_select "input#inventory_adresse_ip_link_voip[name=?]", "inventory[adresse_ip_link_voip]"

      assert_select "input#inventory_adresse_ipv6_lan_data[name=?]", "inventory[adresse_ipv6_lan_data]"

      assert_select "input#inventory_adresse_ipv6_lan_voip[name=?]", "inventory[adresse_ipv6_lan_voip]"

      assert_select "input#inventory_adresse_ipv6_link_data[name=?]", "inventory[adresse_ipv6_link_data]"

      assert_select "input#inventory_adresse_ipv6_link_voip[name=?]", "inventory[adresse_ipv6_link_voip]"

      assert_select "input#inventory_agreement_num[name=?]", "inventory[agreement_num]"

      assert_select "input#inventory_auto_renewal_term[name=?]", "inventory[auto_renewal_term]"

      assert_select "input#inventory_billingtelephonenumber[name=?]", "inventory[billingtelephonenumber]"

      assert_select "input#inventory_btn[name=?]", "inventory[btn]"

      assert_select "input#inventory_circuit_num_logique[name=?]", "inventory[circuit_num_logique]"

      assert_select "input#inventory_circuit_num_physique[name=?]", "inventory[circuit_num_physique]"

      assert_select "input#inventory_circuit_num[name=?]", "inventory[circuit_num]"

      assert_select "input#inventory_city[name=?]", "inventory[city]"

      assert_select "input#inventory_compte_fttb[name=?]", "inventory[compte_fttb]"

      assert_select "input#inventory_customer_name[name=?]", "inventory[customer_name]"

      assert_select "input#inventory_customer_sn[name=?]", "inventory[customer_sn]"

      assert_select "input#inventory_deleted[name=?]", "inventory[deleted]"

      assert_select "input#inventory_description[name=?]", "inventory[description]"

      assert_select "input#inventory_devicenamemonitored[name=?]", "inventory[devicenamemonitored]"

      assert_select "input#inventory_device[name=?]", "inventory[device]"

      assert_select "input#inventory_did_supplier_id[name=?]", "inventory[did_supplier_id]"

      assert_select "input#inventory_download_speed[name=?]", "inventory[download_speed]"

      assert_select "input#inventory_email_of_courrier[name=?]", "inventory[email_of_courrier]"

      assert_select "input#inventory_installed_on_line[name=?]", "inventory[installed_on_line]"

      assert_select "input#inventory_inventory_type[name=?]", "inventory[inventory_type]"

      assert_select "input#inventory_inventory_type_id[name=?]", "inventory[inventory_type_id]"

      assert_select "input#inventory_mac_address[name=?]", "inventory[mac_address]"

      assert_select "input#inventory_numerodereference[name=?]", "inventory[numerodereference]"

      assert_select "input#inventory_parent_agreement_num[name=?]", "inventory[parent_agreement_num]"

      assert_select "input#inventory_password[name=?]", "inventory[password]"

      assert_select "input#inventory_product_id[name=?]", "inventory[product_id]"

      assert_select "input#inventory_product_name[name=?]", "inventory[product_name]"

      assert_select "input#inventory_quantity[name=?]", "inventory[quantity]"

      assert_select "input#inventory_realsyncspeeddownload[name=?]", "inventory[realsyncspeeddownload]"

      assert_select "input#inventory_realsyncspeedupload[name=?]", "inventory[realsyncspeedupload]"

      assert_select "input#inventory_router_principal[name=?]", "inventory[router_principal]"

      assert_select "input#inventory_router_vrrp[name=?]", "inventory[router_vrrp]"

      assert_select "input#inventory_service_id[name=?]", "inventory[service_id]"

      assert_select "input#inventory_site_id[name=?]", "inventory[site_id]"

      assert_select "input#inventory_site_name[name=?]", "inventory[site_name]"

      assert_select "input#inventory_site_number[name=?]", "inventory[site_number]"

      assert_select "input#inventory_sous_interface_data[name=?]", "inventory[sous_interface_data]"

      assert_select "input#inventory_sous_interface_voip[name=?]", "inventory[sous_interface_voip]"

      assert_select "input#inventory_sous_interface_voip_vrrp[name=?]", "inventory[sous_interface_voip_vrrp]"

      assert_select "input#inventory_supplier_num[name=?]", "inventory[supplier_num]"

      assert_select "input#inventory_supplier[name=?]", "inventory[supplier]"

      assert_select "input#inventory_terminate_on[name=?]", "inventory[terminate_on]"

      assert_select "input#inventory_term[name=?]", "inventory[term]"

      assert_select "input#inventory_upload_speed[name=?]", "inventory[upload_speed]"

      assert_select "input#inventory_username[name=?]", "inventory[username]"

      assert_select "input#inventory_vlan_data_q_in_q[name=?]", "inventory[vlan_data_q_in_q]"

      assert_select "input#inventory_vlan_supplier[name=?]", "inventory[vlan_supplier]"

      assert_select "input#inventory_vlan_voip_q_in_q[name=?]", "inventory[vlan_voip_q_in_q]"

      assert_select "input#inventory_vl_videotron[name=?]", "inventory[vl_videotron]"

      assert_select "input#inventory_vrrp_id[name=?]", "inventory[vrrp_id]"

      assert_select "input#inventory_z_end[name=?]", "inventory[z_end]"
    end
  end
end
