describe "invoices/_callcharge.html.erb", type: :view do

  fixtures :invoices

  before do
    @callcharge = assign(:callcharge, invoices(:motherhubbard01).tollcalls.first)
  end

  it "renders a call charge" do
    render template: 'invoices/_callcharge', locals: { callcharge: @callcharge }

    assert_select 'tr' do
      assert_select 'td.charge-date', text: I18n.localize(@callcharge.invoice_fmt_date, format: :short), count: 1
      assert_select 'td.charge-time', text: "#{@callcharge.invoice_fmt_time}", count: 1
      assert_select 'td.charge-number', text: /#{@callcharge.invoice_fmt_callednumber}/, count: 1
      assert_select 'td.charge-city-place', text: "#{@callcharge.invoice_fmt_city_place}", count: 1
      assert_select 'td.charge-prov-place', text: "#{@callcharge.invoice_fmt_prov_place}", count: 1
      assert_select 'td.charge-duration', text: "#{@callcharge.invoice_fmt_duration}", count: 1
      assert_select 'td.charge-price', text: money(@callcharge.invoice_fmt_price, precision: 3), count: 1
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders a call charge in english" do
      render template: 'invoices/_callcharge', locals: { callcharge: @callcharge }

      assert_select 'tr' do
        assert_select 'td.charge-date', text: 'Mar 03', count: 1
        assert_select 'td.charge-time', text: '15:43:42', count: 1
        assert_select 'td.charge-number', text: /#{@callcharge.invoice_fmt_callednumber}/, count: 1
        assert_select 'td.charge-city-place', text: "#{@callcharge.invoice_fmt_city_place}", count: 1
        assert_select 'td.charge-prov-place', text: "#{@callcharge.invoice_fmt_prov_place}", count: 1
        assert_select 'td.charge-duration', text: "#{@callcharge.invoice_fmt_duration}", count: 1
        assert_select 'td.charge-price', text: money(@callcharge.invoice_fmt_price, precision: 3), count: 1
      end
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders a call charge in french" do
      render template: 'invoices/_callcharge', locals: { callcharge: @callcharge }

      assert_select 'tr' do
        assert_select 'td.charge-date', text: '16-03-03', format: :short, count: 1
        assert_select 'td.charge-time', text: '15:43:42', count: 1
        assert_select 'td.charge-number', text: /#{@callcharge.invoice_fmt_callednumber}/, count: 1
        assert_select 'td.charge-city-place', text: "#{@callcharge.invoice_fmt_city_place}", count: 1
        assert_select 'td.charge-prov-place', text: "#{@callcharge.invoice_fmt_prov_place}", count: 1
        assert_select 'td.charge-duration', text: "#{@callcharge.invoice_fmt_duration}", count: 1
        assert_select 'td.charge-price', text: money(@callcharge.invoice_fmt_price, precision: 3), count: 1
      end
    end

    after { I18n.locale = I18n.default_locale }
  end
end
