describe 'invoices/_pagetwo.html.erb', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
  end

  it "renders the second page of the monthly invoice" do
    render

    assert_select 'h4.address-change', text: I18n.translate('invoices._pagetwo.change_of_address'), count: 2

    assert_select '#company-info', text: I18n.translate('invoices._pagetwo.company_information')
    assert_select '#company-address', html: I18n.translate('invoices._pagetwo.contact_html').strip

    assert_select '#business-hours', text: I18n.translate('invoices._pagetwo.business_hours')

    assert_select '#payment-info', text: I18n.translate('invoices._pagetwo.payment_information')

    assert_select '#dc-policy', text: I18n.translate('invoices._pagetwo.disconnect_policy')

    assert_select '#service-termination', text: I18n.translate('invoices._pagetwo.service_termination')

    assert_select '#disputes', text: I18n.translate('invoices._pagetwo.billing_disputes')

    assert_select '#services-offered', text: I18n.translate('invoices._pagetwo.services_offered')
    assert_select '#internet', text: I18n.translate('invoices._pagetwo.internet')

    assert_select '#account-name' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.account_name')
    end

    assert_select '#new-address' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.new_address')
    end

    assert_select '#city' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.city')
    end

    assert_select '#state' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.state')
    end

    assert_select '#zip' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.zip')
    end

    assert_select '#work-phone' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.work_phone')
    end

    assert_select '#contact-name' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.contact_name')
    end

    assert_select '#email-address' do
      assert_select 'div', text: I18n.translate('invoices._pagetwo.email_address')
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders the second page in english" do
      render

      assert_select 'h4#company-info', text: 'Company Information'
      assert_select 'p', text: 'Please send any correspondence and written inquiries to the address below:'

      assert_select '#company-address', text: /Montreal \(Quebec\) H1J 2Y7/

      assert_select '#business-hours', text: 'Business Hours'
      assert_select 'p', text: /Customer Service: M-F 8h - 17h/

      assert_select '#payment-info', text: 'Payment Information'

      assert_select '#late-fees', text: 'Payments are due by the “Due Date” on the front of your invoice. Payment received after the “Due Date” may not be reflected on your invoice. If we do not receive the payment by the “Due Date”, your account will become past due and will be subject to a late fee of 1.5% per month or $5.00 whichever is the lower fee.'

      assert_select '#dc-policy', text: 'Disconnect Policy'

      assert_select 'p', text: 'Services will be disconnected if there are any outstanding balances 60 days past due. If an account is disconnected, the following amount for reconnection fee will be charged.'

      assert_select 'ol li', text: '$250.00 administration fee'

      assert_select 'ol li', text: 'Plus any Telco fees that may apply'

      assert_select '#responsibility', text: 'You are responsible for the payment of all charges on your bill. Failure to pay any portion of your bill may result in collection action.'

      assert_select 'h4.address-change', text: 'Change of Address', count: 2

      assert_select 'p', text: 'Please inform us of a change in address or phone numbers by checking the box on the front of this invoice then complete the Address Change form below.'
      assert_select 'p', text: '* Take note that for a change of address, you have to inform us 30 days in advance.'

      assert_select 'h4#service-termination', text: 'Service Termination'
      assert_select 'p', text: 'You must notify IP4B Telecom with a thirty (30) day advance written request for any service disconnection. Failure to do so will result in your receiving the monthly bill for all charges until IP4B Telecom is notified otherwise.'
      assert_select 'p', text: '* Clients must consult their IP4Bs services agreement for termination penalty fees during the service term.'

      assert_select 'h4#disputes', text: 'Billing Disputes'
      assert_select 'p', text: 'Billing disputes need to be submitted in writing to IP4B Telecom within 60 days of the bill date stated on the first page of your invoice.'

      assert_select 'h4#services-offered', text: 'Services Offered'
      assert_select 'p', text: 'Telephony'
      assert_select 'ul' do
        assert_select 'li', text: 'Telephone line: VoIP Hosted telephone solution, VoIP phone lines, traditional telephone lines and PRI'
        assert_select 'li', text: 'Calling Cards'
        assert_select 'li', text: 'Conference Bridge'
      end
      assert_select 'h6#internet', text: 'Internet'
      assert_select 'ul' do
        assert_select 'li', text: 'Internet access: fiber optic, cable modem, ADSL'
        assert_select 'li', text: 'Web, FTP and email hosting'
        assert_select 'li', text: 'Private Multi-Site Network'
      end

      assert_select 'p', text: 'Please check the box on the reverse side'

      assert_select '#account-name' do
        assert_select 'div', text: 'Account Name:'
      end

      assert_select '#new-address' do
        assert_select 'div', text: 'New Address:'
      end

      assert_select '#city' do
        assert_select 'div', text: 'City:'
      end

      assert_select '#state' do
        assert_select 'div', text: 'State:'
      end

      assert_select '#zip' do
        assert_select 'div', text: 'Zip:'
      end

      assert_select '#work-phone' do
        assert_select 'div', text: 'Work Phone:'
      end

      assert_select '#contact-name' do
        assert_select 'div', text: 'Contact Name:'
      end

      assert_select '#email-address' do
        assert_select 'div', text: 'Email Address:'
      end
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders the second page in french" do
      render

      assert_select 'h4#company-info', text: 'Informations de la compagnie'
      assert_select 'p', text: "Toute correspondance et requêtes écrites doivent être envoyées à l'adresse ci ­dessous :"

      assert_select '#company-address', text: /Montréal \(Québec\) H1J 2Y7/

      assert_select '#business-hours', text: "Heures d'ouverture"
      assert_select 'p', text: /Service clientèle - lundi au vendredi 8 h - 17 h/

      assert_select '#payment-info', text: 'Informations de paiement'

      assert_select '#late-fees', text: 'Les paiements sont dus à la «Date d’échéance» sur la première page de votre facture. Les paiements reçus après la «Date d’échéance» peuvent ne pas apparaître sur votre facture. Si nous ne recevons pas le paiement avant la «Date d’échéance», le solde passé dû sera soumis à une pénalité de retard de 1.5 % par mois ou 5,00 $, selon le plus petit montant de deux.'

      assert_select '#dc-policy', text: 'Politique de débranchement'

      assert_select 'p', text: "Les services seront débranchés s'il y a un solde dû de plus de 60 jours. Si un compte est débranché, des honoraires seront facturés pour la reconnexion."

      assert_select 'ol li', text: "Honoraires d'administration de 250,00 $"
      assert_select 'ol li', text: "Tous les honoraires du Telco qui peuvent s'appliquer"

      assert_select '#responsibility', text: 'Vous êtes responsables du paiement de toutes les charges sur votre facture. Suite à un défaut de paiement de votre facture, en partie ou en totalité, nous pourrions faire appel à une agence de recouvrement.'

      assert_select 'h4.address-change', text: "Changement d'adresse", count: 2

      assert_select 'p', text: "Veuillez nous informer de tous changements d'adresse ou de numéros de téléphone en cochant la boîte au verso puis, complétez le formulaire Changement d’adresse ci ­dessous."
      assert_select 'p', text: '* Prenez note que nous devons être avisé d’un changement d’adresse au moins 30 jours à l’avance.'

      assert_select 'h4#service-termination', text: 'Fin de services'
      assert_select 'p', text: 'Pour mettre fin à un ou plusieurs services, vous devez aviser IP4B Telecom par écrit au moins trente (30) jours avant la date de débranchement souhaitée. Tant que IP4B Telecom ne recevra pas un tel avis, vous continuerez d’être facturé pour ces services à chaque mois.'
      assert_select 'p', text: '* Le client doit consulter son entente de services avec IP4B Telecoms pour les honoraires de pénalité de terminaison pendant le terme de service.'

      assert_select 'h4#disputes', text: 'Litige de facturation'
      assert_select 'p', text: 'Tout litige ou désaccord sur la facturation doit être soumis par écrit à IP4B Telecom dans les 60 jours suivant de la date de facturation apparaissant sur la première page de votre facture. Aucune demande reçue après ce délai ne sera traitée.'

      assert_select 'h4#services-offered', text: 'Services offerts'
      assert_select 'p', text: 'Téléphonie'
      assert_select 'ul' do
        assert_select 'li', text: 'Lignes téléphoniques : solution VoIP hébergée, lignes téléphoniques et PRI VoIP, lignes téléphoniques et PRI traditionnels'
        assert_select 'li', text: "Cartes d'appels"
        assert_select 'li', text: "Service d'appel conférence"
      end
      assert_select 'h6#internet', text: 'Internet'
      assert_select 'ul' do
        assert_select 'li', text: 'Accès Internet : fibre optique, Fibex, modem câble, ADSL'
        assert_select 'li', text: 'Web, FTP et hébergement de courrier électronique'
        assert_select 'li', text: 'Réseau multi-­sites privés'
      end

      assert_select 'p', text: 'Avez-vous coché la boîte au verso?'

      assert_select '#account-name' do
        assert_select 'div', text: 'Nom du compte :'
      end

      assert_select '#new-address' do
        assert_select 'div', text: 'Nouvelle adresse :'
      end

      assert_select '#city' do
        assert_select 'div', text: 'Ville :'
      end

      assert_select '#state' do
        assert_select 'div', text: 'Province :'
      end

      assert_select '#zip' do
        assert_select 'div', text: 'Code Postal :'
      end

      assert_select '#work-phone' do
        assert_select 'div', text: 'Téléphone au travail :'
      end

      assert_select '#contact-name' do
        assert_select 'div', text: 'Nom du contact :'
      end

      assert_select '#email-address' do
        assert_select 'div', text: 'Adresse électronique'
      end
    end

    after { I18n.locale = I18n.default_locale }
  end
end
