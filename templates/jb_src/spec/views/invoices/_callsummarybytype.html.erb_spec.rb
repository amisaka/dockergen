describe 'invoices/_callsummarybytype', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
    @callsummarybytype = @invoice.callsummary_by_type.first
  end

  it "renders a call summary by type" do
    render partial: 'invoices/callsummarybytype', locals: { callsummarybytype: @callsummarybytype }

    assert_select '.callsummary-type' do
      assert_select '.cst-desc', text: ''
      assert_select '.cst-count', text: '0'
      assert_select '.cst-duration', text: '00:00:00'
      assert_select '.cst-cost', text: '$0.00'
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders a call summary by type in english" do
      render partial: 'invoices/callsummarybytype', locals: { callsummarybytype: @callsummarybytype }

      skip "can't meaningfully test english localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders a call summary by type in french" do
      render partial: 'invoices/callsummarybytype', locals: { callsummarybytype: @callsummarybytype }

      skip "can't meaningfully test french localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end

end
