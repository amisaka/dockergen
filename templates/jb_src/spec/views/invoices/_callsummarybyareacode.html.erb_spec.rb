describe 'invoices/_callsummarybyareacode', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
    @callsummarybyareacode = @invoice.callsummary_by_areacode.first
  end

  it "renders a call summary by areacode" do
    render partial: 'invoices/callsummarybyareacode', locals: { callsummarybyareacode: @callsummarybyareacode, invoice: @invoice }

    assert_select '.callsummary-areacode' do
      assert_select '.csc-desc', text: ''
      assert_select '.csc-count', text: '0'
      assert_select '.csc-duration', text: '00:00:00'
      assert_select '.csc-cost', text: '$0.00'
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders a call summary by areacode in english" do
      render partial: 'invoices/callsummarybyareacode', locals: { callsummarybyareacode: @callsummarybyareacode, invoice: @invoice }

      skip "can't meaningfully test english localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders a call summary by areacode in french" do
      render partial: 'invoices/callsummarybyareacode', locals: { callsummarybyareacode: @callsummarybyareacode, invoice: @invoice }

      skip "can't meaningfully test french localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end

end
