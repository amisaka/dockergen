describe 'invoices/_monthlycharge', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
    @monthlycharge = @invoice.billable_services.by_servicename.first
  end

  it "renders a monthly charge" do
    render partial: 'invoices/monthlycharge', locals: { monthlycharge: @monthlycharge }

    assert_select '.monthlycharge' do
      assert_select '.mc-service',  text: @monthlycharge.service_name
      assert_select '.mc-date',     text: @monthlycharge.date_range
      assert_select '.mc-desc',     text: /#{@monthlycharge.description}/
      assert_select '.mc-qty',      text: "#{@monthlycharge.qty}"
      # assert_select '.mc-price',    text: /#{@monthlycharge.unit_price}/
      assert_select '.mc-total',    text: /#{@monthlycharge.total}/
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders a call charge in english" do
      render partial: 'invoices/monthlycharge', locals: { monthlycharge: @monthlycharge }

      skip "can't meaningfully test english localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders a call charge in french" do
      render partial: 'invoices/monthlycharge', locals: { monthlycharge: @monthlycharge }

      skip "can't meaningfully test french localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end
end
