describe 'invoices/_callsummarybyservice', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
    @callsummarybyservice = @invoice.callsummary_by_service.first
  end

  it "renders a call summary by service" do
    render partial: 'invoices/callsummarybyservice', locals: { callsummarybyservice: @callsummarybyservice }

    assert_select '.callsummary-service' do
      assert_select '.css-desc', text: ''
      assert_select '.css-count', text: '0'
      assert_select '.css-duration', text: '00:00:00'
      assert_select '.css-cost', text: '$0.00'
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders a call summary by service in english" do
      render partial: 'invoices/callsummarybyservice', locals: { callsummarybyservice: @callsummarybyservice }

      skip "can't meaningfully test english localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders a call summary by service in french" do
      render partial: 'invoices/callsummarybyservice', locals: { callsummarybyservice: @callsummarybyservice }

      skip "can't meaningfully test french localization of this partial at this time!"
    end

    after { I18n.locale = I18n.default_locale }
  end

end
