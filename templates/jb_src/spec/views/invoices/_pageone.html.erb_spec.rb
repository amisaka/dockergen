# coding: utf-8
describe 'invoices/_pageone.html.erb', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
  end

  it "renders the first page of the monthly invoice" do
    render "invoices/pageone", pageone: @invoice

    assert_select '.row' do
      assert_select '.col-xs-7' do
        expect(rendered).to render_template(partial: 'invoices/_address')
      end

      assert_select '#summary' do
        assert_select '#summary-account' do
          expect(rendered).to render_template(partial: 'invoices/_accountnumbers')
        end
      end
    end

    assert_select '#detachable' do
      assert_select '.col-xs-7' do
        expect(rendered).to render_template(partial: 'invoices/_address')
      end
    end
  end

  it "displays the IP4B large logo in the heading" do
    render "invoices/pageone", pageone: @invoice

    assert_select 'img.logoLarge', count: 1
  end

  it "displays IP4B address" do
    render "invoices/pageone", pageone: @invoice

    assert_select 'address', html: I18n.t('invoices._pageone.address').strip
  end

  it "displays the IP4B small logo in the detachable part" do
    render "invoices/pageone", pageone: @invoice

    assert_select 'img.logoLarge', count: 1
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders the first page in english" do
      @invoice.client.language_preference = 'English'

      render "invoices/pageone", pageone: @invoice

      # Checks the english address
      assert_select 'address', text: /Montreal/

      # Checks the english customer service information
      assert_select '#customer-service-information > .form-spacer' do
        assert_select '.row > strong', text: 'CUSTOMER SERVICE INFORMATION'
        assert_select '.row > strong', text: 'Customer Service'
        assert_select '.row > strong', text: 'Technical Support'
        assert_select '.row > strong', text: 'Billing Email'
      end

      # Checks the english tax registrations
      assert_select '#tax-registrations > span', text: 'Registration Numbers G.S.T. 885630608RT P.S.T. 1204900538'

      # Checks the english payments received message
      assert_select '.paymentreceived-msg > div > div', text: @invoice.invoice_run.processed_up_to_en

      # Checks the english payments due message
      assert_select '.paymentsdue-msg > div', text: @invoice.invoice_run.payments_due_at_en

      # Checks the english seasonal message
      assert_select '.seasonal-msg > div', text: @invoice.invoice_run.seasonal_message_en

      # Checks the english account history
      assert_select '#account-history', count: 1 do
        assert_select '#summary-account-history > strong', text: 'Summary of Account History'
        assert_select '.row > span', text: 'Previous Balance'
        assert_select '.row > span', text: 'Payment Received'
        assert_select '.row > span', text: 'Late Fee Charges'
        assert_select '.row > strong', text: 'Past Due Balance'
      end

      # Checks the english current charges history
      assert_select '#current-charges', count: 1 do
        assert_select '#summary-current-charges > strong', text: 'Summary of Current Charges'
        assert_select '.row > span', text: 'Monthly Charges'
        assert_select '.row > span', text: 'Usage Charges'
        assert_select '.row > strong', text: 'Total Current Charges'
      end

      # Checks the english dues summary
      assert_select '#summary-dues', count: 1 do
        assert_select '#total-due-immediately > strong', text: 'Total Due Immediately'
        assert_select '#total-due-by > strong', text: "Total Due by #{l @invoice.due_date, format: :long}"
        assert_select '#total-due > span', text: 'Total Due'
      end

      # Checks the english detach message
      assert_select '#detach-msg > p', text: 'Please detach and return portion below with your payment.'

      # Checks the english amount enclosed
      assert_select '#amount-enclosed', text: 'Amount Enclosed'

      # Checks the english footer message
      assert_select '#footer-msg' do
        assert_select 'p', text: 'Please return this payment slip with your cheque made payable to: IP4B Telecom'
        assert_select 'p', text: /Check here and complete reverse side for address or contact change./
      end
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders the first page in french" do
      @invoice.client.language_preference = 'French'

      render "invoices/pageone", pageone: @invoice

      # Checks the french address
      assert_select 'address', text: /Montréal/

      # Checks the french customer service information
      assert_select '#customer-service-information > .form-spacer' do
        assert_select '.row > strong', text: 'POUR NOUS REJOINDRE'
        assert_select '.row > strong', text: 'Service à la clientèle'
        assert_select '.row > strong', text: 'Soutien technique'
        assert_select '.row > strong', text: 'Courriel pour la facturation'
      end

      # Checks the french tax registrations numbers
      assert_select '#tax-registrations > span', text: "Numéros d'enregistrements T.P.S. 885630608RT T.V.Q. 1204900538"

      # Checks the french payments received message
      assert_select '.paymentreceived-msg > div > div', text: @invoice.invoice_run.processed_up_to_fr

      # Checks the french payments due message
      assert_select '.paymentsdue-msg > div', text: @invoice.invoice_run.payments_due_at_fr

      # Checks the french seasonal message
      assert_select '.seasonal-msg > div', text: @invoice.invoice_run.seasonal_message_fr

      # Checks the french account history
      assert_select '#account-history', count: 1 do
        assert_select '#summary-account-history > strong', text: 'Mois précédent'
        assert_select '.row > span', text: 'Solde antérieur'
        assert_select '.row > span', text: 'Paiement reçu'
        assert_select '.row > span', text: 'Supplément de retard'
        assert_select '.row > strong', text: 'Solde impayé'
      end

      # Checks the french current charges history
      assert_select '#current-charges', count: 1 do
        assert_select '#summary-current-charges > strong', text: 'Mois courant'
        assert_select '.row > span', text: 'Services'
        assert_select '.row > span', text: 'Communications facturées'
        assert_select '.row > strong', text: 'Total - mois courant'
      end

      # Checks the french dues summary
      assert_select '#summary-dues', count: 1 do
        assert_select '#total-due-immediately > strong', text: 'Montant dû immédiatement'
        assert_select '#total-due-by > strong', text: "Montant dû le #{l @invoice.due_date, format: :long}"
        assert_select '#total-due > span', text: 'Montant total dû'
      end

      # Checks the french detach message
      assert_select '#detach-msg > p', text: 'Veuillez détacher et retourner le coupon de paiement avec votre chèque.'

      # Checks the french amount enclosed
      assert_select '#amount-enclosed', text: 'Paiement'

      # Checks the french footer message
      assert_select '#footer-msg' do
        assert_select 'p', text: 'Veuillez retourner ce coupon avec votre chèque au nom de IP4B Telecom.'
        assert_select 'p', text: /Cochez et remplir le formulaire au verson pour un changement d'adresse ou de contact./
      end

      # Checks the french tax registrations numbers
      assert_select '#tax-registrations > span', text: "Numéros d'enregistrements T.P.S. 885630608RT T.V.Q. 1204900538"

      # Checks the french account history
      assert_select '#account-history', count: 1 do
        assert_select '#summary-account-history > strong', text: 'Mois précédent'
        assert_select '.row > span', text: 'Solde antérieur'
        assert_select '.row > span', text: 'Paiement reçu'
        assert_select '.row > span', text: 'Supplément de retard'
        assert_select '.row > strong', text: 'Solde impayé'
      end

      # Checks the french tax registrations numbers
      assert_select '#tax-registrations > span', text: "Numéros d'enregistrements T.P.S. 885630608RT T.V.Q. 1204900538"
    end

    after { I18n.locale = I18n.default_locale }
  end
end
