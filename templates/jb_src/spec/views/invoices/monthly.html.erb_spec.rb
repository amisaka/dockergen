describe 'invoices/monthly.html.erb', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
    assign(:taxlist, @invoice.taxlist)
  end

  it "renders a monthly invoice" do
    render

    assert_select '#invoice-page-1', count: 1 do
      assert_select '.container-fluid' do
        expect(rendered).to render_template partial: 'invoices/_pageone'
      end
    end

    assert_select '#invoice-page-2', count: 1 do
      assert_select '.container-fluid' do
        expect(rendered).to render_template partial: 'invoices/_pagetwo'
      end
    end

    assert_select '#invoice-page-3', count: 1 do
      assert_select '.container-fluid' do
        expect(rendered).to render_template partial: 'invoices/_monthlycharges'
      end
    end

    assert_select '#invoice-page-4', count: 1 do
      assert_select '.container-fluid' do
        expect(rendered).to render_template partial: 'invoices/_callsummary'
      end
    end

    assert_select '#invoice-page-5', count: 1 do
      assert_select '.container-fluid' do
        expect(rendered).to render_template partial: 'invoices/_calldetail'
      end
    end

    assert_select '.break', count: 4
  end
end
