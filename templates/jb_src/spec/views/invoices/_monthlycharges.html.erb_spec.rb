describe 'invoices/_monthlycharges', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
    @taxlist = assign(:taxlist, @invoice.taxlist)
  end

  it "renders a monthly charge" do
    render 'invoices/monthlycharges', invoice: @invoice

    expect(rendered).to render_template partial: 'invoices/_customerinfobox'

    assert_select 'table' do
      assert_select 'caption', text: I18n.translate('invoices._monthlycharges.monthly_charges')

      assert_select 'thead' do
        assert_select 'tr' do
          assert_select 'th', text: I18n.translate('invoices._monthlycharges.service')
          assert_select 'th', text: I18n.translate('invoices._monthlycharges.date_range')
          assert_select 'th', text: I18n.translate('invoices._monthlycharges.description')
          assert_select 'th', text: I18n.translate('invoices._monthlycharges.quantity')
          # assert_select 'th', text: I18n.translate('invoices._monthlycharges.unit_price')
          assert_select 'th', text: I18n.translate('invoices._monthlycharges.total')
        end
      end

      assert_select 'tbody' do
        expect(rendered).to render_template partial: 'invoices/_monthlycharge'

        assert_select 'tr' do
          assert_select 'td', text: I18n.translate('invoices._monthlycharges.total')
          assert_select 'td', text: /#{@invoice.billable_services_total}/
        end
      end
    end

    assert_select 'table' do
      assert_select 'caption', text: I18n.translate('invoices._monthlycharges.tax_summary_for_charges')

      assert_select 'thead' do
        assert_select 'tr' do
          assert_select 'th', text: I18n.translate('invoices._monthlycharges.account_level')
          @taxlist.each do |taxthing|
            assert_select 'th', text: taxthing.second
          end
          assert_select 'th', text: I18n.translate('invoices._monthlycharges.total')
        end
      end

      assert_select 'tbody' do
        assert_select 'tr' do
          assert_select 'td', text: @invoice.client.name
          @taxlist.each do |taxthing|
            assert_select 'td', text: /#{@invoice.taxes[taxthing.first]}/
          end
          assert_select 'td', text: /#{@invoice.taxtotal}/
        end

        assert_select 'tr' do
          assert_select 'td', text: I18n.translate('invoices._monthlycharges.total')
          @taxlist.each do |taxthing|
            assert_select 'td', text: /#{@invoice.taxes[taxthing.first]}/
          end
          assert_select 'td', text: /#{@invoice.taxtotal}/
        end
      end
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders a call charge in english" do
      render 'invoices/monthlycharges', invoice: @invoice

      assert_select 'table' do
        assert_select 'caption', text: 'Monthly Charges'

        assert_select 'thead' do
          assert_select 'tr' do
            assert_select 'th', text: 'Service'
            assert_select 'th', text: 'Date Range'
            assert_select 'th', text: 'Description'
            assert_select 'th', text: 'Quantity'
            # assert_select 'th', text: 'Unit Price'
            assert_select 'th', text: 'Total'
          end
        end

        assert_select 'tbody' do
          assert_select 'tr' do
            assert_select 'td', text: 'Total'
            assert_select 'td#billable-services-total', text: number_to_currency(@invoice.billable_services_total)
          end
        end
      end

      assert_select 'table' do
        assert_select 'caption', text: 'Tax Summary for Charges'

        assert_select 'thead' do
          assert_select 'tr' do
            assert_select 'th', text: 'Account Level'
            assert_select 'th', text: 'Total'
          end
        end

        assert_select 'tbody' do
          assert_select 'tr' do
            assert_select 'td', text: 'Total'
          end
        end
      end
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders a call charge in french" do
      render 'invoices/monthlycharges', invoice: @invoice

      assert_select 'table' do
        assert_select 'caption', text: 'Frais mensuels'

        assert_select 'thead' do
          assert_select 'tr' do
            assert_select 'th', text: 'Service'
            assert_select 'th', text: 'Date'
            assert_select 'th', text: 'Description'
            assert_select 'th', text: 'Quantité'
            # assert_select 'th', text: 'Prix'
            assert_select 'th', text: 'Total'
          end
        end

        assert_select 'tbody' do
          assert_select 'tr' do
            assert_select 'td', text: 'Total'
            assert_select 'td#billable-services-total', text: number_to_currency(@invoice.billable_services_total)
          end
        end
      end

      assert_select 'table' do
        assert_select 'caption', text: 'Sommaire des taxes des frais mensuels'

        assert_select 'thead' do
          assert_select 'tr' do
            assert_select 'th', text: 'Compte'
            assert_select 'th', text: 'Total'
          end
        end

        assert_select 'tbody' do
          assert_select 'tr' do
            assert_select 'td', text: 'Total'
          end
        end
      end
    end

    after { I18n.locale = I18n.default_locale }
  end
end
