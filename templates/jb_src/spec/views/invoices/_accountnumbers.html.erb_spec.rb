describe 'invoices/_accountnumbers.html.erb', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
  end

  it 'renders the account numbers of the invoice' do
    render "invoices/accountnumbers", accountnumbers: @invoice

    # renders the account number
    assert_select '#account-number' do
      assert_select 'strong.pull-left', count: 1, text: I18n.t('invoices._accountnumbers.account_number')
      assert_select 'span.pull-right', count: 1, text: @invoice.account_code
      assert_select '.clearfix', count: 1
    end

    # renders the invoice number
    assert_select '#invoice-number' do
      assert_select 'strong.pull-left', count: 1, text: I18n.t('invoices._accountnumbers.invoice_number')
      assert_select 'span.pull-right', count: 1, text: @invoice.invoicenumber
      assert_select '.clearfix', count: 1
    end

    # renders the invoice date
    assert_select '#invoice-date' do
      assert_select 'strong.pull-left', count: 1, text: I18n.t('invoices._accountnumbers.invoice_date')
      assert_select 'span.pull-right', count: 1, text: @invoice.invoice_day
      assert_select '.clearfix', count: 1
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders the account numbers in english" do
      render "invoices/accountnumbers", accountnumbers: @invoice

      assert_select '#account-number > strong', text: 'Account Number'
      assert_select '#invoice-number > strong', text: 'Invoice Number'
      assert_select '#invoice-date > strong', text: 'Invoice Date'
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders the account numbers in french" do
      render "invoices/accountnumbers", accountnumbers: @invoice

      assert_select '#account-number > strong', text: 'Numéro de compte'
      assert_select '#invoice-number > strong', text: 'Numéro de facture'
      assert_select '#invoice-date > strong', text: 'Date de facturation'
    end

    after { I18n.locale = I18n.default_locale }
  end
end
