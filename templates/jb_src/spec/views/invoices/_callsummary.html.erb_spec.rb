describe 'invoices/_callsummary', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
    @taxlist = assign(:taxlist, @invoice.taxlist)
  end

  it "renders a call summary" do
    render 'invoices/callsummary', callsummary: @invoice

    expect(rendered).to render_template partial: 'invoices/_customerinfobox'

    assert_select 'table' do
      assert_select 'caption', text: 'Call Summary'

      assert_select 'thead' do
        assert_select 'tr' do
          assert_select 'th', text: 'Description'
          assert_select 'th', text: '#Calls'
          assert_select 'th', text: 'Duration'
          assert_select 'th', text: 'Total'
        end
      end

      assert_select 'tbody' do

        # NOTE: This is failing because the partial is not rendered.
        # Whats going on here?
        # expect(rendered).to render_template partial: 'invoices/_callsummarybytype'

        assert_select '.cst-totals' do
          assert_select '.cst-total', text: 'Total'
          assert_select '.cst-total-count', text: @invoice.total_call_count.to_s
          assert_select '.cst-total-duration', text: @invoice.total_call_duration_s
          assert_select '.cst-total-cost', text: money(@invoice.total_call_cost)
        end
      end
    end

    assert_select 'table' do
      assert_select 'caption', text: 'Call Summary by Service'

      assert_select 'thead' do
        assert_select 'tr' do
          assert_select 'th', text: 'Telephone Number'
          assert_select 'th', text: '#Calls'
          assert_select 'th', text: 'Duration'
          assert_select 'th', text: 'Total'
        end
      end

      assert_select 'tbody' do
        expect(rendered).to render_template partial: 'invoices/_callsummarybyservice'

        assert_select '.css-totals' do
          assert_select '.css-total', text: 'Total'
          assert_select '.css-total-count', text: @invoice.total_call_count.to_s
          assert_select '.css-total-duration', text: @invoice.total_call_duration_s
          assert_select '.css-total-cost', text: money(@invoice.total_call_cost)
        end
      end
    end

    assert_select 'table' do
      assert_select 'caption', text: 'Call Summary by Area Code'

      assert_select 'thead' do
        assert_select 'tr' do
          assert_select 'th', text: 'Area Code'
          assert_select 'th', text: '#Calls'
          assert_select 'th', text: 'Duration'
          assert_select 'th', text: 'Total'
        end
      end

      assert_select 'tbody' do
        expect(rendered).to render_template partial: 'invoices/_callsummarybyareacode'

        assert_select '.csc-totals' do
          assert_select '.csc-total', text: 'Total'
          assert_select '.csc-total-count', text: @invoice.total_call_count.to_s
          assert_select '.csc-total-duration', text: @invoice.total_call_duration_s
          assert_select '.csc-total-cost', text: money(@invoice.total_call_cost)
        end
      end
    end

    assert_select 'table' do
      assert_select 'caption', text: 'Tax Summary for Calls'

      assert_select 'thead' do
        assert_select 'tr' do
          assert_select 'th', text: 'Account Level'
          @taxlist.each do |taxthing|
            assert_select 'th', text: taxthing.second
          end
          assert_select 'th', text: 'Total'
        end
      end

      assert_select 'tbody' do
        assert_select 'tr' do
          assert_select 'td', text: @invoice.client.name
          @taxlist.each do |taxthing|
            assert_select 'td', text: /#{@invoice.calls_taxes[taxthing.first]}/
          end
          assert_select 'td', text: /#{@invoice.calls_taxtotal}/
        end

        assert_select 'tr' do
          assert_select 'td', text: 'Total'
          @taxlist.each do |taxthing|
            assert_select 'td', text: /#{@invoice.calls_taxes[taxthing.first]}/
          end
          assert_select 'td', text: /#{@invoice.calls_taxtotal}/
        end
      end
    end
  end

  context "given an english locale" do
    before { I18n.locale = :en }

    it "renders a call summary in english" do
      render 'invoices/callsummary', callsummary: @invoice

      skip
    end

    after { I18n.locale = I18n.default_locale }
  end

  context "given a french locale" do
    before { I18n.locale = 'fr-CA' }

    it "renders a call summary in french" do
      render 'invoices/callsummary', callsummary: @invoice

      skip
    end

    after { I18n.locale = I18n.default_locale }
  end

end
