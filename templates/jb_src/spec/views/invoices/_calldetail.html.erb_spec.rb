describe 'invoices/_calldetail', type: :view do

  fixtures :invoices

  before do
    @invoice = assign(:invoice, invoices(:motherhubbard01))
  end

  it "renders the call detail" do
    render "invoices/calldetail", calldetail: CallDetailPage.new(1), invoice: @invoice

    expect(rendered).to render_template(partial: 'invoices/_customerinfobox')

    assert_select "table.table" do

    end
  end
end
