describe "users/new.html.erb" do
  it "can see password box on create form" do

    pending  # this is active scaffolded right now.
    render

    assert_select "form", :action => admin_users_path, :method => "post" do
      assert_select "input#users_password", :name => "users[password]"
    end
  end

end
