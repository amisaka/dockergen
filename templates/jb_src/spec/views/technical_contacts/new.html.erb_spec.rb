require 'rails_helper'

RSpec.describe "technical_contacts/new", type: :view do
  before(:each) do
    assign(:technical_contact, TechnicalContact.new())
  end

  it "renders new technical_contact form" do
    render

    assert_select "form[action=?][method=?]", technical_contacts_path, "post" do
    end
  end
end
