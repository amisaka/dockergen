require 'rails_helper'

RSpec.describe "technical_contacts/edit", type: :view do
  before(:each) do
    @technical_contact = assign(:technical_contact, TechnicalContact.create!())
  end

  it "renders the edit technical_contact form" do
    render

    assert_select "form[action=?][method=?]", technical_contact_path(@technical_contact), "post" do
    end
  end
end
