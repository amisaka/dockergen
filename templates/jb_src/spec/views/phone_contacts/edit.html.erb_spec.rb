require 'rails_helper'

RSpec.describe "phone_contacts/edit", type: :view do
  before(:each) do
    @phone_contact = assign(:phone_contact, PhoneContact.create!())
  end

  it "renders the edit phone_contact form" do
    render

    assert_select "form[action=?][method=?]", phone_contact_path(@phone_contact), "post" do
    end
  end
end
