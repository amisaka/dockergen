require 'rails_helper'

RSpec.describe "phone_contacts/new", type: :view do
  before(:each) do
    assign(:phone_contact, PhoneContact.new())
  end

  it "renders new phone_contact form" do
    render

    assert_select "form[action=?][method=?]", phone_contacts_path, "post" do
    end
  end
end
