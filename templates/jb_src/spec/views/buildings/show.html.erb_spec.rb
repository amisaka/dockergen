describe "buildings/show", type: :view do
  fixtures :buildings
  
  before(:each) do
    @building = assign(:building, buildings(:uqo))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Address1/)
    expect(rendered).to match(/Address2/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Province/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/Postalcode/)
  end
end
