describe "buildings/new", type: :view do
  fixtures :buildings

  before(:each) do
    assign(:building, Building.new(buildings(:uqo).attributes))
  end

  it "renders new building form" do
    render

    assert_select "form[action=?][method=?]", buildings_path, "post" do

      assert_select "input#building_address1[name=?]", "building[address1]"

      assert_select "input#building_address2[name=?]", "building[address2]"

      assert_select "input#building_city[name=?]", "building[city]"

      assert_select "input#building_province[name=?]", "building[province]"

      assert_select "input#building_country[name=?]", "building[country]"

      assert_select "input#building_postalcode[name=?]", "building[postalcode]"
    end
  end
end
