describe "buildings/index", type: :view do

  before(:each) do
    @buildings = assign(:buildings,
      Kaminari.paginate_array(create_list(:building, 10)).page)
  end

  it "renders a list of buildings" do

    render

    assert_select "tr > th", text: "Address"
    assert_select "tr > th", text: "City"
    assert_select "tr > th", text: "Province"
    assert_select "tr > th", text: "Country"
    assert_select "tr > th", text: "Postal Code"

    @buildings.each do |building|
      [:id, :address1, :address2, :city, :province, :country, :postalcode].each do |k|
        expect(rendered).to include(CGI.escapeHTML(building.send(k).to_s))
      end
    end
  end

  it "buildings should have an edit button" do

    render

    @buildings.each do |building|
      assert_select "tr#bldg-#{building.id} > td > div.text-right > a" do |selected|
        expect(selected).to have_css('.btn.btn-warning.btn-box.btn-xs')
        expect(selected).to have_css('.fa.fa-edit')
      end
    end
  end

  it "buildings should have a delete button" do
    render

    @buildings.each do |building|
      assert_select "tr#bldg-#{building.id} > td > div.text-right > a" do |selected|
        expect(selected).to have_css('.btn.btn-danger.btn-box.btn-xs')
        expect(selected).to have_css('.fa.fa-remove')
      end
    end
  end

end
