require 'rails_helper'
require 'rake'
#Beaumont4::Application.load_tasks

describe 'novavision lca rake task' do

    #let(:rake)      { Rake::Application.new }
    #let(:task_name) { self.class.top_level_description }
    #let(:task_path) { "lib/tasks/lca.rake" }
    #subject         { rake[task_name] }



    def loaded_files_excluding_current_rake_file
        $".reject {|file| file == Rails.root.join("#{task_path}.rake").to_s }
    end


    before do
        path = File.expand_path("../../../lib/tasks/lca.rake", __FILE__)
        if ! File.exists? (path)
            $stderr.puts("There does not seem to be a file at #{path}")
        end
        load path
        #Rake.application.rake_require(task_path, [Rails.root.to_s], loaded_files_excluding_current_rake_file)

        Rake::Task.define_task(:environment)
        #Rake::Task["novavision:updateLerg8"].application.load_rakefile
    end

    describe 'novavision:listCarriers' do
        it 'should run' do
            #debugger

            assert_operator LocalExchange.all.count, :>, 0

            p = Rake::Task["novavision:listCarriers"].invoke
            Rake::Task["novavision:updateLerg6"].clear_actions

            assert(true, "This just tests we got the method")
        end
    end

    describe 'novavison:updateLerg8' do
        it 'should correctly read 2 LERG8 files and delete, find and create LERG8 entries as appropriate' do
            ENV['deleteMissing'] = 'yes'
            ENV['quiet'] = 'very'

            # Determining the folder of the test files
            path = Rails.root.to_s + '/spec/files/LERG8.2015.forTesting.DAT'

            numLerg8start = Lerg8.count(:all)

            # Load lerg file 1
            ARGV[1] = path
            Rake::Task["novavision:updateLerg8"].invoke

            # Count the lerg8 entries
            numLerg8before = Lerg8.count(:all)

            # Read second Lerg 8 file
            path = Rails.root.to_s + '/spec/files/LERG8.201603.forTesting.DAT'

            # Load lerg file 1
            ARGV[1] = path
            Rake::Task["novavision:updateLerg8"].reenable
            Rake::Task["novavision:updateLerg8"].invoke

            # Count the lerg 8 entires
            numLerg8after = Lerg8.count(:all)

            # Do the expect
            assert_operator numLerg8after, :>, numLerg8before
        end
    end

    describe 'novavison:updateLerg6' do
        it 'should correctly read 2 LERG6 files and delete, find and create LERG6 entries as appropriate' do
            ENV['deleteMissing'] = 'yes'
            #ENV['debug'] = 'yes'
            ENV['quiet'] = 'very'
            numLerg6start = NorthAmericanExchange.count(:all)

            # Determining the folder of the test files
            path = Rails.root.to_s + '/spec/files/LERG6.2015.forTesting.DAT'

            # Load lerg file 1
            ARGV[1] = path
            Rake::Task["novavision:updateLerg6"].invoke

            # Count the lerg6 entries
            numLerg6before = NorthAmericanExchange.count(:all)

            # Read second Lerg 6 file
            path = Rails.root.to_s + '/spec/files/LERG6.201603.forTesting.DAT'

            # Load lerg file 1
            ARGV[1] = path
            #debugger
            Rake::Task["novavision:updateLerg6"].reenable
            Rake::Task["novavision:updateLerg6"].invoke

            # Count the lerg 6 entires
            numLerg6after = NorthAmericanExchange.count(:all)

            # Do the expect
            assert_operator numLerg6after, :>, numLerg6before

            Rake::Task["novavision:updateLerg6"].clear_actions
        end
    end

    describe 'novavison:updateCarrier' do
        it 'should correctly read 2 peer footprint files and delete, find and create peer footprint entries as appropriate' do
            ENV['deleteMissing'] = 'yes'
            #ENV['debug'] = 'yes'
            ENV['quiet'] = 'very'
            ENV['carrier']='Thinktel'
            numLerg6start = LocalExchange.count(:all)

            # Determining the folder of the test files
            path = Rails.root.to_s + '/spec/files/Thinktel_OnNetFootprint_20140721_v1.21.csv'

            # Load lerg file 1
            ENV['path'] = path
            Rake::Task["novavision:updateCarrier"].invoke

            # Count the lerg6 entries
            numLerg6before = LocalExchange.count(:all)

            # Read second Lerg 6 file
            path = Rails.root.to_s + '/spec/files/thinkTel.2016-04-21.csv'

            # Load lerg file 1
            ENV['sep'] = ','
            ENV['path'] = path
            Rake::Task["novavision:updateCarrier"].reenable
            Rake::Task["novavision:updateCarrier"].invoke

            # Count the lerg 6 entires
            numLerg6after = LocalExchange.count(:all)

            # Do the expect
            assert_operator numLerg6after, :>, numLerg6before
        end
    end


    describe 'novavision:listNPANXX' do
        it 'should include the NPA and NXX of phantom RC' do
            ENV['deleteMissing'] = 'no'
            ENV['quiet'] = 'very'

            searchFor = '204555'

            before = NorthAmericanExchange.where(:npa => searchFor[0..2], :nxx => searchFor[3..5]).count

            # Load a LERG6 file at least wtih all canadian phantom RCs (or manitoba NPA NXX?)
            path = Rails.root.to_s + '/spec/files/LERG6.201603.canadaOnly.phantomOnly.DAT'
            ARGV[1] = path
            Rake::Task["novavision:updateLerg6"].reenable
            Rake::Task["novavision:updateLerg6"].invoke

            after = NorthAmericanExchange.where(:npa => searchFor[0..2], :nxx => searchFor[3..5]).count

            assert_operator after, :>, before

            #$stderr.puts "Before / After: #{before} / #{after}"


            path = '/tmp/listNPANXX.test.txt'
            # http://stackoverflow.com/questions/32314830/feature-testing-rake-tasks-adds-up-the-output-to-stdout-how-to-avoid-this#question
            ENV['FILE'] = path
            ENV['CARRIER'] = 'Thinktel'


            # Print the list
            Rake::Task["novavision:listNPANXX"].invoke

            command = "wc -l #{path}"
            wcOutput = `#{command}`
            line_count = wcOutput.strip.split(' ')[0].to_i
            expect(line_count).to be > 0, "Line count of #{path} was #{line_count}, wcOutput was #{wcOutput} for command #{command} for head "  + `head #{path}`

            # check if the right NPA NXX are there
            p = File.open path do |f|
                f.find { |l| l =~ /#{searchFor}/ }
            end

            #$stderr.puts "p is #{p} (a #{p.class.name})"

            assert(! p.nil?, "p is nil")

            assert_equal p.strip, searchFor

        end
    end
end
