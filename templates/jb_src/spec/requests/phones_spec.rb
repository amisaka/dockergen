describe "Phones", type: :request do

  describe "GET /phones" do
    it "responds with HTTP 302" do
      get phones_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get phones_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /phones/:id" do
    subject { create(:phone) }

    it "responds with HTTP 302" do
      get phone_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get phone_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /phones/new" do
    it "responds with HTTP 302" do
      get new_phone_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get new_phone_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /phones/:id/edit" do
    subject { create(:phone) }

    it "responds with HTTP 302" do
      get edit_phone_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get edit_phone_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST /phones" do
    subject { attributes_for :phone }

    it "responds with HTTP 302" do
      post phones_path, params: { phone: subject }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post phones_path, params: { phone: subject }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PATCH /phones/:id" do
    subject { create :phone }
    let(:new_phone) { attributes_for :phone}

    it "responds with HTTP 302" do
      patch phone_path(subject), params: { phone: new_phone }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        patch phone_path(subject), params: { phone: new_phone }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT /phones/:id" do
    subject { create :phone }
    let(:new_phone) { attributes_for :phone}

    it "responds with HTTP 302" do
      put phone_path(subject), params: { phone: new_phone }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        put phone_path(subject), params: { phone: new_phone }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /phones/:id" do
    subject { create :phone }

    it "responds with HTTP 302" do
      delete phone_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete phone_path(subject)
        expect(response).to have_http_status(302)
      end
    end
  end

end
