describe "Payments", type: :request do

  describe "GET /payments" do
    before { @client = create(:client) }

    it "responds with HTTP 302" do
      get payments_path params: { client_id: @client.id }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get payments_path params: { client_id: @client.id }
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /payments/:id" do
    subject { create(:payment) }

    it "responds with HTTP 302" do
      get payment_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get payment_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /payments/new" do
    before { @client = create(:client) }

    it "responds with HTTP 302" do
      get new_payment_path params: { client_id: @client.id }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get new_payment_path params: { client_id: @client.id }
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /payments/:id/edit" do
    subject { create(:payment) }

    it "responds with HTTP 302" do
      get edit_payment_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get edit_payment_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST /payments" do
    before do
      @client = create(:client)
      @btn = create(:btn, client: @client)
      @invoice = create(:invoice, client: @client)
      @payment = build(:payment, client: @client)
      # @payment_invoice = build(:payment_invoice, btn: @btn, invoice: @invoice)
      # @payment_attributes = @payment.attributes.merge(payment_invoices_attributes: [@payment_invoice.attributes])
    end

    it "responds with HTTP 302" do
      post payments_path, params: { payment: attributes_for(:payment) }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post payments_path, params: { payment: attributes_for(:payment) }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PATCH /payments/:id" do
    subject { create :payment }
    let(:new_payment) { attributes_for :payment}

    it "responds with HTTP 302" do
      patch payment_path(subject), params: { payment: new_payment }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        patch payment_path(subject), params: { payment: new_payment }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT /payments/:id" do
    subject { create :payment }
    let(:new_payment) { attributes_for :payment}

    it "responds with HTTP 302" do
      put payment_path(subject), params: { payment: new_payment }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        put payment_path(subject), params: { payment: new_payment }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /payments/:id" do
    subject { create :payment }

    it "responds with HTTP 302" do
      delete payment_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete payment_path(subject)
        expect(response).to have_http_status(302)
      end
    end
  end

end
