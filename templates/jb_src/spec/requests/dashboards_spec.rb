describe "Dashboards", type: :request do

  describe "GET /dashboards" do
    it "responds with HTTP 302" do
      get dashboards_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get dashboards_path
        expect(response).to have_http_status(200)
      end
    end
  end
end
