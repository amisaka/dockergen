describe "Clients", type: :request do

  describe "GET /clients" do
    it "responds with HTTP 302" do
      get clients_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get clients_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:id" do
    subject { create(:client, btns: create_list(:btn, 1)) }

    it "responds with HTTP 302" do
      get client_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get client_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/new" do
    it "responds with HTTP 302" do
      get new_client_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get new_client_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:id/edit" do
    subject { create(:client) }

    it "responds with HTTP 302" do
      get edit_client_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get edit_client_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST /clients" do
    subject { attributes_for :client }

    it "responds with HTTP 302" do
      post clients_path, params: { client: subject }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post clients_path, params: { client: subject }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PATCH /clients/:id" do
    subject { create(:client, btns: create_list(:btn, 1)) }
    let(:new_client) { attributes_for :client}

    it "responds with HTTP 302" do
      patch client_path(subject), params: { client: new_client }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        patch client_path(subject), params: { client: new_client }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT /clients/:id" do
    subject { create(:client, btns: create_list(:btn, 1)) }
    let(:new_client) { attributes_for :client}

    it "responds with HTTP 302" do
      put client_path(subject), params: { client: new_client }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        put client_path(subject), params: { client: new_client }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /clients/:id" do
    subject { create :client }

    it "responds with HTTP 302" do
      delete client_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete client_path(subject)
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "GET /clients/typeahead" do
    it "responds with HTTP 302" do
      get typeahead_clients_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get typeahead_clients_path format: :json
        expect(response).to have_http_status(200)
      end
    end
  end

end
