describe "Buildings", type: :request do

  describe "GET /buildings" do
    it "responds with HTTP 302" do
      get buildings_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get buildings_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /buildings/:id" do
    subject { create(:building) }

    it "responds with HTTP 302" do
      get building_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get building_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /buildings/new" do
    it "responds with HTTP 302" do
      get new_building_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get new_building_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /buildings/:id/edit" do
    subject { create(:building) }

    it "responds with HTTP 302" do
      get edit_building_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get edit_building_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST /buildings" do
    subject { attributes_for :building }

    it "responds with HTTP 302" do
      post buildings_path, params: { building: subject }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post buildings_path, params: { building: subject }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PATCH /buildings/:id" do
    subject { create :building }
    let(:new_building) { attributes_for :building}

    it "responds with HTTP 302" do
      patch building_path(subject), params: { building: new_building }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        patch building_path(subject), params: { building: new_building }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT /buildings/:id" do
    subject { create :building }
    let(:new_building) { attributes_for :building}

    it "responds with HTTP 302" do
      put building_path(subject), params: { building: new_building }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        put building_path(subject), params: { building: new_building }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /buildings/:id" do
    subject { create :building }

    it "responds with HTTP 302" do
      delete building_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete building_path(subject)
        expect(response).to have_http_status(302)
      end
    end
  end

end
