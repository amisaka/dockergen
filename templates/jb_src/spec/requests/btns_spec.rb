describe "Btns", type: :request do

  describe "GET /btns" do
    it "responds with HTTP 302" do
      get btns_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get btns_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /btns/:id" do
    subject { create(:btn) }

    it "responds with HTTP 302" do
      get btn_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get btn_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /btns/new" do
    it "responds with HTTP 302" do
      get new_btn_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get new_btn_path
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /btns/:id/edit" do
    subject { create(:btn) }

    it "responds with HTTP 302" do
      get edit_btn_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get edit_btn_path(subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST /btns" do
    subject { attributes_for :btn }

    it "responds with HTTP 302" do
      post btns_path, params: { btn: subject }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post btns_path, params: { btn: subject }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PATCH /btns/:id" do
    subject { create :btn }
    let(:new_btn) { attributes_for :btn}

    it "responds with HTTP 302" do
      patch btn_path(subject), params: { btn: new_btn }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        patch btn_path(subject), params: { btn: new_btn }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT /btns/:id" do
    subject { create :btn }
    let(:new_btn) { attributes_for :btn}

    it "responds with HTTP 302" do
      put btn_path(subject), params: { btn: new_btn }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        put btn_path(subject), params: { btn: new_btn }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /btns/:id" do
    subject { create :btn }

    it "responds with HTTP 302" do
      delete btn_path(subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete btn_path(subject)
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "GET /btns/typeahead" do
    it "responds with HTTP 302" do
      get typeahead_btns_path
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get typeahead_btns_path format: :json
        expect(response).to have_http_status(200)
      end
    end
  end

end
