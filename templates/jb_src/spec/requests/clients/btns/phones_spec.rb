describe "Clients/Btns/Phones", type: :request do

  before(:all) do
    @client = create(:client)
    @btn = create(:btn, client: @client)
  end

  describe "GET /clients/:client_id/btns/:btn_id/phones" do
    it "responds with HTTP 302" do
      get client_btn_phones_path(@client, @btn)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get client_btn_phones_path(@client, @btn)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:client_id/btns/:btn_id/phones/:id" do
    subject { create(:phone) }

    it "responds with HTTP 302" do
      get client_btn_phone_path(@client, @btn, subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get client_btn_phone_path(@client, @btn, subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:client_id/btns/:btn_id/phones/new" do
    it "responds with HTTP 302" do
      get new_client_btn_phone_path(@client, @btn)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get new_client_btn_phone_path(@client, @btn)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:client_id/btns/:btn_id/phones/:id/edit" do
    subject { create(:phone) }

    it "responds with HTTP 302" do
      get edit_client_btn_phone_path(@client, @btn, subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get edit_client_btn_phone_path(@client, @btn, subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST /clients/:client_id/btns/:btn_id/phones" do
    subject { attributes_for :phone }

    it "responds with HTTP 302" do
      post client_btn_phones_path(@client, @btn), params: { phone: subject }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post client_btn_phones_path(@client, @btn), params: { phone: subject }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PATCH /clients/:client_id/btns/:btn_id/phones/:id" do
    subject { create :phone }
    let(:new_phone) { attributes_for :phone}

    it "responds with HTTP 302" do
      patch client_btn_phone_path(@client, @btn, subject), params: { phone: new_phone }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        patch client_btn_phone_path(@client, @btn, subject), params: { phone: new_phone }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT /clients/:client_id/btns/:btn_id/phones/:id" do
    subject { create :phone }
    let(:new_phone) { attributes_for :phone}

    it "responds with HTTP 302" do
      put client_btn_phone_path(@client, @btn, subject), params: { phone: new_phone }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        put client_btn_phone_path(@client, @btn, subject), params: { phone: new_phone }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /clients/:client_id/btns/:btn_id/phones/:id" do
    subject { create :phone }

    it "responds with HTTP 302" do
      delete client_btn_phone_path(@client, @btn, subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete client_btn_phone_path(@client, @btn, subject)
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "POST /clients/:client_id/btns/:btn_id/phones/add" do
    subject { attributes_for :phone }

    it "responds with HTTP 302" do
      post add_client_btn_phones_path(@client, @btn), params: { phone: subject }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post add_client_btn_phones_path(@client, @btn), params: { phone: subject }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /clients/:client_id/btns/:btn_id/phones/:id/remove" do
    subject { create :phone }

    it "responds with HTTP 302" do
      delete remove_client_btn_phone_path(@client, @btn, subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete remove_client_btn_phone_path(@client, @btn, subject)
        expect(response).to have_http_status(302)
      end
    end
  end

end
