describe "Clients/Phones", type: :request do

  before(:all) { @client = create(:client) }

  describe "GET /clients/:client_id/phones" do
    it "responds with HTTP 302" do
      get client_phones_path(@client)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get client_phones_path(@client)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:client_id/phones/:id" do
    subject { create(:phone) }

    it "responds with HTTP 302" do
      get client_phone_path(@client, subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get client_phone_path(@client, subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:client_id/phones/new" do
    it "responds with HTTP 302" do
      get new_client_phone_path(@client)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get new_client_phone_path(@client)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "GET /clients/:client_id/phones/:id/edit" do
    subject { create(:phone) }

    it "responds with HTTP 302" do
      get edit_client_phone_path(@client, subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 200" do
        get edit_client_phone_path(@client, subject)
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "POST /clients/:client_id/phones" do
    subject { attributes_for :phone }

    it "responds with HTTP 302" do
      post client_phones_path(@client), params: { phone: subject }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        post client_phones_path(@client), params: { phone: subject }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PATCH /clients/:client_id/phones/:id" do
    subject { create :phone }
    let(:new_phone) { attributes_for :phone}

    it "responds with HTTP 302" do
      patch client_phone_path(@client, subject), params: { phone: new_phone }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        patch client_phone_path(@client, subject), params: { phone: new_phone }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT /clients/:client_id/phones/:id" do
    subject { create :phone }
    let(:new_phone) { attributes_for :phone}

    it "responds with HTTP 302" do
      put client_phone_path(@client, subject), params: { phone: new_phone }
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        put client_phone_path(@client, subject), params: { phone: new_phone }
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE /clients/:client_id/phones/:id" do
    subject { create :phone }

    it "responds with HTTP 302" do
      delete client_phone_path(@client, subject)
      expect(response).to have_http_status(302)
    end

    context "when a user is signed in" do
      before do
        sign_in create(:user, admin: true)
      end

      it "responds with HTTP 302" do
        delete client_phone_path(@client, subject)
        expect(response).to have_http_status(302)
      end
    end
  end

end
