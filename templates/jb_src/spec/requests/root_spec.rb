describe "Root", type: :request do

  describe "GET /" do
    it "should respond with http redirect" do
      get root_path
      expect(response).to have_http_status(:redirect)
    end

    context "when a user is signed in" do
      before do
        allow_any_instance_of(RootController).to \
          receive(:current_user).and_return(create(:user, admin: true))
      end

      it "redirects to users dashboard" do
        get root_path
        expect(response).to redirect_to(dashboards_path)
      end
    end

    context "when a user is not signed in" do
      it "redirects to login page" do
        get root_path
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
