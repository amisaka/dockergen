module ViewsHelpers
  module ControllerViewHelpers
    def client_search_query
      ''
    end

    def client_search_field
      ''
    end

    def client_search_status
      ''
    end

    def client_search_invoice
      ''
    end

    def query
      ''
    end

    def search_status
      ''
    end

    def redirect_back_path
      ''
    end
  end

  def initialize_view_helpers(view)
    view.extend ControllerViewHelpers
  end
end

RSpec.configure do |config|
  # Include views test helpers
  config.include(ViewsHelpers, type: :view)
  config.before(:each, type: :view) { initialize_view_helpers(view) }
end
