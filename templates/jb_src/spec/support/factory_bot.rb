# require 'factory_bot_rails'

RSpec.configure do |config|
  config.before(:suite) do
    FactoryBot.lint
  end

  config.include FactoryBot::Syntax::Methods
end
