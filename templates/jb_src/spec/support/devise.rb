# This is needed for requests specs until Devise officially adds requests helpers
module RequestSpecHelper

  include Warden::Test::Helpers

  def self.included(base)
    base.before(:each) { Warden.test_mode! }
    base.after(:each) { Warden.test_reset! }
  end

  def sign_in(resource)
    login_as(resource, scope: warden_scope(resource))
  end

  def sign_out(resource)
    logout(warden_scope(resource))
  end

  private

  def warden_scope(resource)
    resource.class.name.underscore.to_sym
  end

end

RSpec.configure do |config|

  # For Devise >= 4.1.1
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include RequestSpecHelper, type: :request

  # Use the following instead if you are on Devise <= 4.1.0
  # config.include Devise::TestHelpers, type: :controller
  # config.include RequestSpecHelper, type: :request

end
