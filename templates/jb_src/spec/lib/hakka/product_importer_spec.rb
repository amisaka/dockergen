module Hakka
  describe ProductImporter do
    if $CONNECTMSSQL
      describe ".import" do
        it "imports a single record from hakka products" do
          expect(ProductImporter.import(Product.first.to_param)).to be_a(Service)
        end
      end

      describe ".import!" do
        it "imports a single record from hakka products" do
          expect(ProductImporter.import!(Product.first.to_param)).to be_a(Service)
        end
      end

      describe ".import_all" do
        before { Service.delete_all }

        it "imports all records from hakka products" do
          expect {ProductImporter.import_all}.to change(Service, :count).from(0).to(Product.count)
        end
      end

      describe ".import_all" do
        before { Service.delete_all }

        it "imports all records from hakka products" do
          expect {ProductImporter.import_all}.to change(Service, :count).from(0).to(Product.count)
        end
      end
      
    else

      describe ".import" do
        it "doesn't importal records from hakka" do
          skip "MSSQL is turned off."
        end
      end

      describe ".import!" do
        it "doesn't importal records from hakka" do
          skip "MSSQL is turned off."
        end
      end

      describe ".import_all" do
        it "doesn't importal records from hakka" do
          skip "MSSQL is turned off."
        end
      end

      describe ".import_all!" do
        it "doesn't importal records from hakka" do
          skip "MSSQL is turned off."
        end
      end
    end
  end
end
