describe ClientSet do

  COUNT = 10
  RANGE = 0...COUNT

  before(:all) { @clients = create_list(:client, COUNT) }
  subject(:all) { ClientSet.new(@clients) }

  RANGE.each do |i|
    context "when index is #{i}" do
      before do
        @previous_client = @clients[i-1]
        @client = @clients[i]
        @next_client = @clients[i+1]
      end

      describe "#next" do
        it "returns the next client in set" do
          if i == RANGE.last
            expect(subject.next(@client)).to be_nil
          else
            expect(subject.next(@client)).to be(@next_client)
          end
        end
      end

      describe "#previous" do
        it "returns the previous client in set" do
          if i == RANGE.first
            expect(subject.previous(@client)).to be_nil
          else
            expect(subject.previous(@client)).to be(@previous_client)
          end
        end
      end
    end
  end

  context "given an empty array" do
    subject { ClientSet.new([]) }

    describe "#previous" do
      it "returns nil" do
        expect(subject.previous).to be_nil
      end
    end

    describe "#next" do
      it "returns nil" do
        expect(subject.previous).to be_nil
      end
    end
  end

  context "given a nil" do
    subject { ClientSet.new(nil) }

    describe "#previous" do
      it "returns nil" do
        expect(subject.previous).to be_nil
      end
    end

    describe "#next" do
      it "returns nil" do
        expect(subject.previous).to be_nil
      end
    end
  end

end
