describe BillableServiceAccessor do
  describe "#new" do
    context "given a Client" do
      subject { build(:client) }

      it "creates a new billable service accessor" do
        expect(BillableServiceAccessor.new(subject)).to be_a BillableServiceAccessor
      end
    end

    context "given a ClientDecorator" do
      subject { ClientDecorator.new(build(:client)) }

      it "creates a new billable service accessor" do
        expect(BillableServiceAccessor.new(subject)).to be_a BillableServiceAccessor
      end
    end

    context "given an Object" do
      subject { Object.new }

      it "raises an error" do
        expect{BillableServiceAccessor.new(subject)}.to raise_error(RuntimeError, /Accessor needs one of Client or ClientDecorator/)
      end
    end
  end

  describe "#for" do
    context  "given a client with billable services" do
      context "with a proper btn and invoice" do
        let(:client) { create(:client_with_billable_services) }
        let(:btn) { client.btns.first }
        let(:invoice) { btn.invoices.first }

        subject { BillableServiceAccessor.new(client) }

        it "returns the billable services" do
          expect(subject.for(btn, invoice)).to be_an ActiveRecord::AssociationRelation
          expect(subject.for(btn, invoice).count).to be >= 1
          expect(subject.for(btn, invoice).first).to be_an_instance_of BillableService
        end
      end

      context "with invalid btn and invoice" do
        let(:client) { create(:client_with_billable_services) }
        let(:btn) { create(:btn) }
        let(:invoice) { create(:invoice) }

        subject { BillableServiceAccessor.new(client) }

        it "raises an error" do
          expect{subject.for(btn, invoice)}.to raise_error(RuntimeError, /(btn|invoice) does not belong to client/)
        end
      end
    end

    context  "given a client without billable_services" do
      let(:client) { create(:client_with_invoices) }
      let(:btn) { client.btns.first }
      let(:invoice) { btn.invoices.first }

      subject { BillableServiceAccessor.new(client) }

      it "returns an empty array" do
        expect(subject.for(btn, invoice)).to be_an ActiveRecord::AssociationRelation
        expect(subject.for(btn, invoice)).to be_empty
        expect(subject.for(btn, invoice).count).to be == 0
        expect(subject.for(btn, invoice).first).to be_nil
      end
    end
  end
end
