module Buildings
  describe DuplicateCheck do

    def create_building_with_different_province(prov)
      b = build(:building)
      until b.province != prov
        b = build(:building)
      end
      b.save
    end

    def create_building_with_different_address2(adrs)
      b = build(:building)
      until b.address2 != adrs
        b = build(:building)
      end
      b.save
    end

    describe "#new" do
      before { @building = create(:building) }

      it "creates a new duplicate check for given building id" do
        expect(DuplicateCheck.new(@building.id)).to be_a(DuplicateCheck)
      end
    end

    describe "#find_duplicates_by_address1" do
      before { @building = create(:building) }
      subject { DuplicateCheck.new(@building.id) }

      context "when buildings have the same address1" do
        it "should find all buildings with duplicated address1" do
          expect(subject.find_duplicates_by_address1.count).to eq(0)

          create(:building, address1: @building.address1)
          expect(subject.find_duplicates_by_address1.count).to eq(1)

          create(:building, address1: @building.address1)
          expect(subject.find_duplicates_by_address1.count).to eq(2)
        end
      end

      context "when buildings have different address1" do
        it "should not find any buildings with duplicated addresses1" do
          expect(subject.find_duplicates_by_address1.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_address1.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_address1.count).to eq(0)
        end
      end
    end

    describe "#find_duplicates_by_address2" do
      before { @building = create(:building) }
      subject { DuplicateCheck.new(@building.id) }

      context "when buildings have the same address2" do
        it "should find all buildings with duplicated address2" do
          expect(subject.find_duplicates_by_address2.count).to eq(0)

          create(:building, address2: @building.address2)
          expect(subject.find_duplicates_by_address2.count).to eq(1)

          create(:building, address2: @building.address2)
          expect(subject.find_duplicates_by_address2.count).to eq(2)
        end
      end

      context "when buildings have different address2" do
        it "should not find any buildings with duplicated address2" do
          expect(subject.find_duplicates_by_address2.count).to eq(0)

          create_building_with_different_address2(@building.address2)
          expect(subject.find_duplicates_by_address2.count).to eq(0)

          create_building_with_different_address2(@building.address2)
          expect(subject.find_duplicates_by_address2.count).to eq(0)
        end
      end
    end

    describe "#find_duplicates_by_city" do
      before { @building = create(:building) }
      subject { DuplicateCheck.new(@building.id) }

      context "when buildings have the same city" do
        it "should find all buildings with duplicated city" do
          expect(subject.find_duplicates_by_city.count).to eq(0)

          create(:building, city: @building.city)
          expect(subject.find_duplicates_by_city.count).to eq(1)

          create(:building, city: @building.city)
          expect(subject.find_duplicates_by_city.count).to eq(2)
        end
      end

      context "when buildings have different city" do
        it "should not find any buildings with duplicated city" do
          expect(subject.find_duplicates_by_city.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_city.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_city.count).to eq(0)
        end
      end
    end

    describe "#find_duplicates_by_province" do
      before { @building = create(:building) }
      subject { DuplicateCheck.new(@building.id) }

      context "when buildings have the same province" do
        it "should find all buildings with duplicated province" do
          expect(subject.find_duplicates_by_province.count).to eq(0)

          create(:building, province: @building.province)
          expect(subject.find_duplicates_by_province.count).to eq(1)

          create(:building, province: @building.province)
          expect(subject.find_duplicates_by_province.count).to eq(2)
        end
      end

      context "when buildings have different province" do
        it "should not find any buildings with duplicated province" do
          expect(subject.find_duplicates_by_province.count).to eq(0)

          create_building_with_different_province(@building.province)
          expect(subject.find_duplicates_by_province.count).to eq(0)

          create_building_with_different_province(@building.province)
          expect(subject.find_duplicates_by_province.count).to eq(0)
        end
      end
    end

    describe "#find_duplicates_by_country" do
      before { @building = create(:building) }
      subject { DuplicateCheck.new(@building.id) }

      context "when buildings have the same country" do
        it "should find all buildings with duplicated country" do
          expect(subject.find_duplicates_by_country.count).to eq(0)

          create(:building, country: @building.country)
          expect(subject.find_duplicates_by_country.count).to eq(1)

          create(:building, country: @building.country)
          expect(subject.find_duplicates_by_country.count).to eq(2)
        end
      end

      context "when buildings have different country" do
        it "should not find any buildings with duplicated country" do
          expect(subject.find_duplicates_by_country.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_country.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_country.count).to eq(0)
        end
      end
    end

    describe "#find_duplicates_by_postalcode" do
      before { @building = create(:building) }
      subject { DuplicateCheck.new(@building.id) }

      context "when buildings have the same postalcode" do
        it "should find all buildings with duplicated postalcode" do
          expect(subject.find_duplicates_by_postalcode.count).to eq(0)

          create(:building, postalcode: @building.postalcode)
          expect(subject.find_duplicates_by_postalcode.count).to eq(1)

          create(:building, postalcode: @building.postalcode)
          expect(subject.find_duplicates_by_postalcode.count).to eq(2)
        end
      end

      context "when buildings have different postalcode" do
        it "should not find any buildings with duplicated postalcode" do
          expect(subject.find_duplicates_by_postalcode.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_postalcode.count).to eq(0)

          create(:building)
          expect(subject.find_duplicates_by_postalcode.count).to eq(0)
        end
      end
    end

    describe ".find_all_duplicates" do
      before do
        @building = create(:building)
        create_list(:building, 10)
        @duplicated_building = Building.create(@building.attributes.except('id'))
      end

      subject { DuplicateCheck }

      it "should find all buildings that are duplicates" do
        expect(subject.find_all_duplicates[@building]).to include @duplicated_building
      end
    end

  end
end
