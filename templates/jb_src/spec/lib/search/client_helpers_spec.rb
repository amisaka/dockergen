describe Search::ClientHelpers do

  # include Rails.application.routes.url_helpers

  class ControllerMock < ActionController::Base
    include Search::ClientHelpers
  end

  let(:controller) { ControllerMock.new }

  describe ".included" do
    it "should declare search query, status, field, params and invoice helper methods" do
      expect(ActionController::Base).to receive(:helper_method)
        .with(:client_search_field, :client_search_query,
          :client_search_params, :client_search_status, :client_search_invoice)

      ActionController::Base.include Search::ClientHelpers
    end

    it "declares helper_method" do
      expect(controller).to respond_to(:client_search_field)
      expect(controller).to respond_to(:client_search_query)
      expect(controller).to respond_to(:client_search_params)
      expect(controller).to respond_to(:client_search_status)
      expect(controller).to respond_to(:client_search_invoice)
    end
  end

  describe "#client_search_field" do
    let(:params) { ActionController::Parameters.new({ client_search: { 'field' => 'foobar' }}) }
    let(:session) { { 'client_search' => { 'field' => 'barfu' }} }

    context "when session is empty" do
      let(:session) { { 'client_search' => {} } }

      it "returns the params value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).once.and_return(session)
        expect(controller.client_search_field).to eq('foobar')
      end
    end

    context "when params is empty" do
      let(:params) { ActionController::Parameters.new({ client_search: {} }) }

      it "returns the session value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).twice.and_return(session)
        expect(controller.client_search_field).to eq('barfu')
      end
    end

    context "when both params and session are empty" do
      let(:session) { { 'client_search' => {} } }
      let(:params) { ActionController::Parameters.new({ client_search: {} }) }

      it "returns the default value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).twice.and_return(session)
        expect(controller.client_search_field).to eq(Search::ClientHelpers::DEFAULT_FIELD_VALUE)
      end
    end
  end

  describe "#client_search_query" do
    let(:params) { ActionController::Parameters.new({ client_search: { 'query' => 'foobar' }}) }
    let(:session) { { 'client_search' => { 'query' => 'barfu' }} }

    context "when session is empty" do
      let(:session) { { 'client_search' => {} } }

      it "returns the params value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).once.and_return(session)
        expect(controller.client_search_query).to eq('foobar')
      end
    end

    context "when params is empty" do
      let(:params) { ActionController::Parameters.new({ client_search: {} }) }

      it "returns the session value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).twice.and_return(session)
        expect(controller.client_search_query).to eq('barfu')
      end
    end

    context "when both params and session are empty" do
      let(:session) { { 'client_search' => {} } }
      let(:params) { ActionController::Parameters.new({ client_search: {} }) }

      it "returns the default value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).twice.and_return(session)
        expect(controller.client_search_query).to eq(Search::ClientHelpers::DEFAULT_QUERY_VALUE)
      end
    end
  end

  describe "#client_search_status" do
    let(:params) { ActionController::Parameters.new({ client_search: { 'status' => 'active' }}) }
    let(:session) { { 'client_search' => { 'status' => 'closed' }} }

    context "when session is empty" do
      let(:session) { { 'client_search' => {} } }

      it "returns the params value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).once.and_return(session)
        expect(controller.client_search_status).to eq('active')
      end
    end

    context "when params is empty" do
      let(:params) { ActionController::Parameters.new({ client_search: {} }) }

      it "returns the session value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).twice.and_return(session)
        expect(controller.client_search_status).to eq('closed')
      end
    end

    context "when both params and session are empty" do
      let(:session) { { 'client_search' => {} } }
      let(:params) { ActionController::Parameters.new({ client_search: {} }) }

      it "returns the default value" do
        expect(controller).to receive(:params).twice.and_return params
        expect(controller).to receive(:session).twice.and_return(session)
        expect(controller.client_search_status).to eq(Search::ClientHelpers::DEFAULT_STATUS_VALUE)
      end
    end
  end
end
