describe Search::ControllerHelpers do

  describe ApplicationController do
    let(:application_controller) { ApplicationController.new }

    it "should declare query and search_status helper methods" do
      expect(ApplicationController).to receive(:helper_method).with(:query, :search_status)
      ApplicationController.include Search::ControllerHelpers
    end

    describe "#query" do
      context "when params[:query] is not blank" do
        let(:params) {{query: 'foobar'}}

        it "returns the value in params[:query] hash" do
          expect(application_controller).to receive(:params).twice.and_return params
          expect(application_controller.query).to eq('foobar')
        end
      end

      context "when params[:query] is empty" do
        let(:params) {{query: ''}}

        it "returns nil" do
          expect(application_controller).to receive(:params).once.and_return params
          expect(application_controller.query).to be nil
        end
      end

      context "when params[:query] is nil" do
        let(:params) {{query: nil}}

        it "returns nil" do
          expect(application_controller).to receive(:params).once.and_return params
          expect(application_controller.query).to be nil
        end
      end
    end

    describe "#search_status" do
      context "when super is not blank" do
        let(:params) {{status: 'active'}}

        it "returns the value in params[:status] hash" do
          expect(application_controller).to receive(:params).twice.and_return params
          expect(application_controller.search_status).to eq(:active)
        end
      end

      context "when params[:status] is empty" do
        let(:params) {{status: ''}}

        it "returns nil" do
          expect(application_controller).to receive(:params).once.and_return params
          expect(application_controller.search_status).to be nil
        end
      end

      context "when params[:status] is nil" do
        let(:params) {{status: nil}}

        it "returns nil" do
          expect(application_controller).to receive(:params).once.and_return params
          expect(application_controller.search_status).to be nil
        end
      end
    end
  end

  module ControllerHelpers;end
  class ControllerHelpers::ControllerMock < ApplicationController
    include Search::ControllerHelpers
  end

  describe ControllerHelpers::ControllerMock do
    let(:controller) { ControllerHelpers::ControllerMock.new }

    describe "#query" do
      context "when params[:query] is not blank" do
        let(:params) {{query: 'foobar'}}
        let(:session) {{}}

        it "saves params[:query] into session and returns its value" do
          allow(controller).to receive(:params).and_return params
          allow(controller).to receive(:session).and_return session
          allow(session).to receive(:[]=).with(:query, 'foobar')
          expect(controller.query).to eq('foobar')
        end
      end

      context "when params[:query] is nil" do
        let(:params) {{query: nil}}

        context "when session[:query] is not blank" do
          let(:session) {{query: 'barfu'}}

          it "returns the value saved in session" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.query).to eq('barfu')
          end
        end

        context "when session[:query] is nil" do
          let(:session) {{query: nil}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.query).to be nil
          end
        end

        context "when session[:query] is empty" do
          let(:session) {{query: ''}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.query).to be nil
          end
        end
      end

      context "when params[:query] is empty" do
        let(:params) {{query: ''}}

        context "when session[:query] is not blank" do
          let(:session) {{query: 'barfu'}}

          it "returns the value saved in session" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.query).to eq('barfu')
          end
        end

        context "when session[:query] is nil" do
          let(:session) {{query: nil}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.query).to be nil
          end
        end

        context "when session[:query] is empty" do
          let(:session) {{query: ''}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.query).to be nil
          end
        end
      end
    end

    describe "#search_status" do
      context "when params[:status] is not blank" do
        let(:params) {{status: 'foobar'}}
        let(:session) {{}}

        it "saves params[:status] into session and returns its value" do
          allow(controller).to receive(:params).and_return params
          allow(controller).to receive(:session).and_return session
          allow(session).to receive(:[]=).with(:status, :foobar)
          expect(controller.search_status).to eq(:foobar)
        end
      end

      context "when params[:status] is nil" do
        let(:params) {{status: nil}}

        context "when session[:status] is not blank" do
          let(:session) {{status: 'barfu'}}

          it "returns the value saved in session" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.search_status).to eq(:barfu)
          end
        end

        context "when session[:status] is nil" do
          let(:session) {{status: nil}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.search_status).to be nil
          end
        end

        context "when session[:status] is empty" do
          let(:session) {{status: ''}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.search_status).to be nil
          end
        end
      end

      context "when params[:status] is empty" do
        let(:params) {{status: ''}}

        context "when session[:status] is not blank" do
          let(:session) {{status: 'barfu'}}

          it "returns the value saved in session" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.search_status).to eq(:barfu)
          end
        end

        context "when session[:status] is nil" do
          let(:session) {{status: nil}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.search_status).to be nil
          end
        end

        context "when session[:status] is empty" do
          let(:session) {{status: ''}}

          it "returns nil" do
            allow(controller).to receive(:params).and_return params
            allow(controller).to receive(:session).and_return session
            expect(controller.search_status).to be nil
          end
        end
      end
    end
  end

end
