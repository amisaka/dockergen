describe Report do

  fixtures :did_suppliers, :cdrviewer_cdr, :rate_centers

  describe ".telus" do
    it "should return exactly 1 CDR" do
      skip
      # create(:cdr_from_telus)
      # expect(Report.telus).to eq(1)
    end
  end

  describe ".rogers" do
    it "should return exactly 1 CDR" do
      skip
      # create(:cdr_from_rogers)
      # expect(Report.rogers).to eq(1)
    end
  end

  describe ".bell" do
    it "should return exactly 1 CDR" do
      skip
      # create(:cdr_from_bell)
      # expect(Report.bell).to eq(1)
    end
  end

  describe ".drapper" do
    it "should return exactly 1 CDR" do
      skip
      # create(:cdr_from_drapper)
      # expect(Report.drapper).to eq(1)
    end
  end

  describe ".troop" do
    it "should return exactly 1 CDR" do
      skip
      # create(:cdr_from_troop)
      # expect(Report.troop).to eq(1)
    end
  end

  describe ".obm" do
    it "should return exactly 1 CDR" do
      skip
      # create(:cdr_from_obm)
      # expect(Report.obm).to eq(1)
    end
  end

  describe ".find_cdrs_with_default_rate_center_or_none" do
    it "should return exactly 1 CDR" do
      skip
      # expect(Report.find_cdrs_with_default_rate_center_or_none.count).to eq(1)
    end
  end

  describe ".find_cdrs_without_country_code" do
    it "should return exactly 1 CDR" do
      skip
      # expect(Report.find_cdrs_without_country_code.count).to eq(1)
    end
  end

  describe ".no_dur_and_yes_call_answer" do
    it "should return 0 cdrs" do
      skip
      # expect(Report.no_dur_and_yes_call_answer).to eq(0)
    end
  end

  describe ".confirm_all_calls_got_rated" do
    it "should return no null values" do
      skip
      # expect(Report.confirm_all_calls_got_rated).to be > 30
    end
  end

  describe ".all_cdrs_have_a_btn_assigned" do
    it "should return no null values" do
      skip
      # expect(Report.all_cdrs_have_a_btn_assigned).to be > 0
    end
  end
end
