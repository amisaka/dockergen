describe UsersController do
  # include Devise::TestHelpers
  # fixtures :users
  #
  # context "when a user is signed in" do
  #   before do
  #     @request.env["devise.mapping"] = Devise.mappings[:user]
  #     sign_in users(:admin)
  #   end
  #
  #   describe "GET #show" do
  #     it "responds with HTTP success" do
  #       get :show
  #       expect(response).to have_http_status(:success)
  #     end
  #   end
  # end
  #
  # context "when no user is signed in" do
  #   describe "GET #show" do
  #     it "responds with HTTP redirect" do
  #       get :show
  #       expect(response).to have_http_status(:redirect)
  #     end
  #   end
  # end

  # def basic_auth(user, password)
  #   credentials = ActionController::HttpAuthentication::Basic.encode_credentials user, password
  #   @request.env['HTTP_AUTHORIZATION'] = credentials
  # end
  #
  # def setup
  #   @request.env["devise.mapping"] = Devise.mappings[:admin]
  # end
  #
  # describe "users controller login state" do
  #   it "users#welcome redirects to users#show when logged in" do
  #     @ravi = users(:testuser)
  #     sign_in(@ravi)
  #
  #     get :welcome
  #     assert_response 200
  #   end
  #
  #   it "users#welcome redirects to users#login when not logged in" do
  #     get :welcome
  #     assert_redirected_to new_user_session_path
  #   end
  #
  #   it "users#welcome does not set a flash when redirecting to users#login" do
  #     get :welcome
  #     assert flash[:error].blank?
  #   end
  # end
  #
  # describe "Users can" do
  #   it "read their own data" do
  #     @testuser = users(:testuser)
  #     sign_in(@testuser)
  #
  #     get :show, { :id => @testuser.id }
  #     assert_response 200
  #   end
  #
  #   it "not read others data" do
  #     @testuser = users(:testuser)
  #     sign_in(@testuser)
  #     @ravi     = users(:testuser)
  #
  #     get :show, { :id => @ravi.id }
  #     assert_response 403
  #   end
  #
  #   it "not read others data in json" do
  #     @testuser = users(:testuser)
  #     sign_in(@testuser)
  #     @ravi     = users(:testuser)
  #
  #     get :show, { :id => @ravi.id, :format => :json}
  #     assert_response 403
  #   end
  #
  #   it "users#update on self succeeds" do
  #     @testuser = users(:testuser)
  #     sign_in(@testuser)
  #
  #     post :update, { :id => @testuser.id, :user => { :fullname => 'No User' }}
  #     assert_not_nil assigns(:user)
  #     @testuser.reload
  #     assert_equal "No User", @testuser.fullname
  #     assert_redirected_to account_url
  #   end
  #
  #   it "not update their agent bit" do
  #     @testuser = users(:testuser)
  #
  #     # make sure we aren't an agent already (just checking...)
  #     assert !@testuser.agent?
  #     sign_in(@testuser)
  #
  #     post :update, { :id => @testuser.id, :user => { :agent => 1 }}
  #     assert_not_nil assigns(:user)
  #
  #     @testuser.reload
  #     # confirm that nothing changed
  #     assert !@testuser.agent?
  #     assert_redirected_to account_url
  #   end
  #
  #   it "read their own data with HTTP auth in HTML format" do
  #     @testuser = users(:testuser)
  #
  #     basic_auth("test@example.com","test1")
  #     get :show, { :id => @testuser.id,  :format => :html }
  #     assert_response 200
  #   end
  #
  #   it "not read their own data with HTTP auth in JSON format" do
  #     @testuser = users(:testuser)
  #
  #     basic_auth("test@example.com","test1")
  #     get :show, { :id => @testuser.id,  :format => :json }
  #     assert_response 406
  #   end
  #
  # end
  #
  # describe "Agent user access to" do
  #   it "users#show demands an agent before data" do
  #     @test_nonagent = users(:testuser)
  #     sign_in(@test_nonagent)
  #     @ravi     = users(:testuser)
  #
  #     get :show, { :id => @ravi.id, :format => :json}
  #     assert_response 403
  #   end
  # end
  #
  # describe "Admin user" do
  #   it "users#update (on not self) fails" do
  #     @testuser = users(:testuser)
  #     sign_in(@testuser)
  #     @ravi     = users(:testuser)
  #
  #     post :update, { :id => @ravi.id, :user => { :fullname => 'No User' }}
  #     assert_response 403
  #   end
  #
  #   it "can update others agent bit" do
  #     @adminuser= users(:admin)
  #     @testuser = users(:testuser)
  #
  #     # make sure it was not an agent already (just checking...)
  #     assert !@testuser.agent?
  #
  #     sign_in(@adminuser)
  #
  #     post :update, { :id => @testuser.id, :user => { :agent => 1 }}
  #     assert_not_nil assigns(:user)
  #     @testuser.reload
  #
  #     # confirm that something changed
  #     assert @testuser.agent?
  #     assert_redirected_to account_url
  #   end
  # end

end
