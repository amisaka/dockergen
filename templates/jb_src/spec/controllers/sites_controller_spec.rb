describe SitesController, type: :controller do

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with HTTP success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      subject { create :site }

      it "responds with HTTP success" do
        get :show, params: { id: subject.id }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with HTTP success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      subject { create :site }

      it "responds with HTTP success" do
        get :edit, params: { id: subject.id }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        subject { attributes_for(:site) }

        it "creates a new Site" do
          expect {
            post :create, params: { site: subject }
          }.to change(Site, :count).by(1)
        end

        it "responds with HTTP redirect" do
          post :create, params: { site: subject }
          expect(response).to have_http_status(:redirect)
        end

        it "redirects to the created site" do
          post :create, params: { site: subject }
          expect(response).to redirect_to(Site.last)
        end
      end

      # Sites dont have validations yet
      #
      # context "with invalid params" do
      #   it "assigns a newly created but unsaved site as @site" do
      #     post :create, site: invalid_attributes
      #     expect(assigns(:site)).to be_a_new(Site)
      #   end
      #
      #   it "re-renders the 'new' template" do
      #     post :create, site: invalid_attributes
      #     expect(response).to render_template("new")
      #   end
      # end
    end

    describe "PUT #update" do
      context "with valid params" do
        before { @site = create(:site) }
        let(:new_site) { attributes_for(:site) }

        it "updates the requested site" do
          put :update, params: { id: @site.id, site: new_site }
          @site.reload
          expect(@site.name).to eq(new_site[:name])
        end

        it "responds with HTTP redirect" do
          put :update, params: { id: @site.id, site: new_site }
          expect(response).to have_http_status(:redirect)
        end

        it "redirects to the site" do
          put :update, params: { id: @site.id, site: new_site }
          expect(response).to redirect_to(@site)
        end
      end

      # context "with invalid params" do
      #   it "assigns the site as @site" do
      #     site = Site.create! valid_attributes
      #     put :update, id: site.id, site: invalid_attributes
      #     expect(assigns(:site)).to eq(site)
      #   end
      #
      #   it "re-renders the 'edit' template" do
      #     site = Site.create! valid_attributes
      #     put :update, id: site.id, site: invalid_attributes
      #     expect(response).to render_template("edit")
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @site = create(:site) }

      it "destroys the requested site" do
        expect {
          delete :destroy, params: { id: @site.id }
        }.to change(Site, :count).by(-1)
      end

      it "redirects to the sites list" do
        delete :destroy, params: { id: @site.id }
        expect(response).to redirect_to(sites_url)
      end
    end

    describe "POST #add" do
      it "adds a site to client" do
      end
    end

    describe "DELETE #remove" do
      it "removes a site from client" do
      end
    end

  end

end
