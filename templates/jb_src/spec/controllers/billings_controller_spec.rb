describe BillingsController, type: :controller do

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #edit" do
      before(:each) { @client = create(:client) }

      subject { @billing = @client.billing }

      it "responds with HTTP success" do
        get :edit, params: { client_id: @client.id }
        expect(response).to have_http_status(:success)
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        before do
          @client = create(:client)
          @invoice = create(:invoice, client: @client)
          @payment = create(:payment, invoice: @invoice)
          @billing = @client.billing
          @new_billing_attributes = attributes_for(:billing)
        end

        it "updates the requested billing" do
          put :update, params: { client_id: @client.id, billing: @new_billing_attributes }
          @billing.reload

          # expect(@billing.balance).to eq(subject[:balance]) # -- not implemented
          expect(@billing.start_date).to eq(@new_billing_attributes[:start_date])
          expect(@billing.billing_start_date).to eq(@new_billing_attributes[:billing_start_date])
          expect(@billing.last_usage_date).to eq(@new_billing_attributes[:last_usage_date])
          expect(@billing.last_statement_date).to eq(@new_billing_attributes[:last_statement_date])
          expect(@billing.last_statement_amount).to eq(@new_billing_attributes[:last_statement_amount])
          expect(@billing.statement_type).to eq(@new_billing_attributes[:statement_type])
          expect(@billing.payment_method).to eq(@new_billing_attributes[:payment_method])
        end

        it "responds with HTTP redirect" do
          put :update, params: { client_id: @client.id, billing: @new_billing_attributes }
          expect(response).to have_http_status(:redirect)
        end

        it "redirects to the client" do
          put :update, params: { client_id: @client.id, billing: @new_billing_attributes }
          expect(response).to redirect_to(@client)
        end
      end
    end
  end
end
