describe BtnsController, type: :controller do

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with HTTP success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      subject { create(:btn) }

      it "responds with HTTP success" do
        get :show, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with HTTP success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      subject { create(:btn) }

      it "responds with HTTP success" do
        get :edit, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        subject { attributes_for(:btn) }

        it "creates a new Btn" do
          expect {
            post :create, params: { btn: subject }
          }.to change(Btn, :count).by(1)
        end

        it "responds with HTTP redirect" do
          post :create, params: { btn: subject }
          expect(response).to have_http_status(:redirect)
        end

        it "redirects to the created btn" do
          post :create, params: { btn: subject }
          expect(response).to redirect_to(Btn.last)
        end
      end

      # context "with invalid params" do
      #   it "assigns a newly created but unsaved btn as @btn" do
      #     post :create, params: { btn: invalid_attributes }
      #     expect(assigns(:btn)).to be_a_new(Btn)
      #   end
      #
      #   it "re-renders the 'new' template" do
      #     post :create, params: { btn: invalid_attributes }
      #     expect(response).to render_template("new")
      #   end
      # end
    end

    describe "PUT #update" do
      context "with valid params" do
        before { @btn = create(:btn) }
        subject { attributes_for(:btn) }

        it "updates the requested btn" do
          put :update, params: { id: @btn.to_param, btn: subject }
          @btn.reload
          expect(@btn.billingtelephonenumber).to eq(subject[:billingtelephonenumber])
        end

        it "responds with HTTP redirect" do
          put :update, params: { id: @btn.to_param, btn: subject }
          expect(response).to have_http_status(:redirect)
        end

        it "redirects to the btn" do
          put :update, params: { id: @btn.to_param, btn: subject }
          expect(response).to redirect_to(@btn)
        end
      end

      # context "with invalid params" do
      #   it "assigns the btn as @btn" do
      #     btn = Btn.create! valid_attributes
      #     put :update, params: { id: btn.to_param, btn: invalid_attributes }
      #     expect(assigns(:btn)).to eq(btn)
      #   end
      #
      #   it "re-renders the 'edit' template" do
      #     btn = Btn.create! valid_attributes
      #     put :update, params: { id: btn.to_param, btn: invalid_attributes }
      #     expect(response).to render_template("edit")
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @btn = create(:btn) }

      it "destroys the requested btn" do
        expect {
          delete :destroy, params: { id: @btn.to_param }
        }.to change(Btn, :count).by(-1)
      end

      it "redirects to the btns list" do
        delete :destroy, params: { id: @btn.to_param }
        expect(response).to redirect_to(btns_url)
      end
    end
  end

end
