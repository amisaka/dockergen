describe PaymentsController, type: :controller do

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      before { @payments = create_list(:payment, 10) }

      it "returns a success response" do
        get :index
        expect(response).to be_success
      end
    end

    describe "GET #show" do
      before { @payment = create(:payment) }

      it "returns a success response" do
        get :show, params: { id: @payment.to_param }
        expect(response).to be_success
      end
    end

    describe "GET #new" do
      before { @client = create(:client) }

      it "returns a success response" do
        get :new, params: { client_id: @client.id }
        expect(response).to be_success
      end
    end

    describe "GET #edit" do
      before { @payment = create(:payment) }

      it "returns a success response" do
        get :edit, params: { id: @payment.to_param }
        expect(response).to be_success
      end
    end

    describe "POST #create" do
      context "with valid params" do
        before do
          @client = create(:client)
          @btn = create(:btn, client: @client)
          @invoice = create(:invoice, client: @client)
          @payment = build(:payment, client: @client)
          # @payment_invoice = build(:payment_invoice, btn: @btn, invoice: @invoice)
          # @payment_attributes = @payment.attributes.merge(payment_invoices_attributes: [@payment_invoice.attributes])
        end

        it "creates a new Payment" do
          expect {
            post :create, params: { payment: @payment_attributes }
          }.to change(Payment, :count).by(1)
        end

        it "creates a new PaymentInvoice" do
          expect {
            post :create, params: { payment: @payment_attributes }
          }.to change(PaymentInvoice, :count).by(1)
        end

        it "associates PaymentInvoice with Payment" do
          post :create, params: { payment: @payment_attributes }
          expect(PaymentInvoice.last.payment).to eq Payment.last
        end

        it "associates Invoice with Payment" do
          post :create, params: { payment: @payment_attributes }
          expect(PaymentInvoice.last.invoice).to eq @invoice
        end

        it "associates BTN with Payment" do
          post :create, params: { payment: @payment_attributes }
          expect(PaymentInvoice.last.btn).to eq @btn
        end

        it "redirects to the created payment's client payments" do
          post :create, params: { payment: @payment_attributes }
          expect(response).to redirect_to(client_payments_path(Payment.last.client))
        end
      end

      # context "with invalid params" do
      #   before { @bad_payment_attributes = attributes_for(:payment, amount: nil) }
      #
      #   it "returns a success response (i.e. to display the 'new' template)" do
      #     post :create, params: { payment: @bad_payment_attributes }
      #     expect(response).to be_success
      #   end
      # end
    end

    describe "PUT #update" do
      before do
        @client = create(:client_with_invoices)
        @btn = @client.btns.first
        @invoice = @btn.invoices.first
        @payment = create(:payment, client: @client)
        @payment_invoice = create(:payment_invoice, btn: @btn, invoice: @invoice, payment: @payment)
      end

      context "with valid params" do
        before do
          @new_amount = 9000
          @new_payment_attributes = attributes_for(:payment, amount: @new_amount).merge(payment_invoices_attributes: [@payment_invoice.attributes.merge(amount: @new_amount)])
        end

        it "updates the requested payment" do
          put :update, params: { id: @payment.to_param, payment: @new_payment_attributes }
          @payment.reload
          expect(@payment.amount).to eq(@new_amount)
        end

        it "does not create a new PaymentInvoice" do
          expect{
            put :update, params: { id: @payment.to_param, payment: @new_payment_attributes }
          }.not_to change(PaymentInvoice, :count)
        end

        it "redirects to the payment" do
          put :update, params: { id: @payment.to_param, payment: @new_payment_attributes }
          expect(response).to redirect_to(@payment)
        end
      end

      # context "with invalid params" do
      #   before { @bad_payment_attributes = attributes_for(:payment, amount: nil) }
      #
      #   it "returns a success response (i.e. to display the 'edit' template)" do
      #     put :update, params: { id: @payment.to_param, payment: @bad_payment_attributes }
      #     expect(response).to be_success
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @payment = create(:payment) }

      it "destroys the requested payment" do
        expect {
          delete :destroy, params: { id: @payment.to_param }
        }.to change(Payment, :count).by(-1)
      end

      it "redirects to the payments list" do
        delete :destroy, params: { id: @payment.to_param }
        expect(response).to redirect_to(payments_url)
      end
    end

  end
end
