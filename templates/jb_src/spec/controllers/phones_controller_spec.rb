describe PhonesController, type: :controller do

  let(:valid_attributes) {
    attributes_for(:phone)
  }

  let(:invalid_attributes) {
    {}
  }

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with HTTP success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      it "responds with HTTP success" do
        get :show, params: { id: create(:phone).to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with HTTP success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      it "responds with HTTP success" do
        get :edit, params: { id: create(:phone).to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        context "when called from #new" do
          before { allow(controller.request).to receive(:referer).and_return(new_phone_path) }

          it "creates a new Phone" do
            expect {
              post :create, params: { phone: valid_attributes }
            }.to change(Phone, :count).by(1)
          end

          it "responds with HTTP found" do
            post :create, params: { phone: valid_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirects to the created phone" do
            post :create, params: { phone: valid_attributes }
            expect(response).to redirect_to(phone_path(Phone.last))
          end
        end

        context "when called from anywhere else" do
          before { allow(controller.request).to receive(:referer).and_return('elsewhere') }

          it "creates a new Phone" do
            expect {
              post :create, params: { phone: valid_attributes }
            }.to change(Phone, :count).by(1)
          end

          it "responds with HTTP found" do
            post :create, params: { phone: valid_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirect to elsewhere" do
            post :create, params: { phone: valid_attributes }
            expect(response).to redirect_to('elsewhere')
          end
        end
      end

      # context "with invalid params" do
      #   it "does not create a new Service" do
      #     expect {
      #       post :create, params: { phone: invalid_attributes }
      #     }.not_to change(Phone, :count)
      #   end
      #
      #   it "responds with HTTP success" do
      #     post :create, params: { phone: invalid_attributes }
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "PUT #update" do
      context "with valid params" do
        context "when called from #new" do
          let(:new_attributes) {
            attributes_for(:phone)
          }

          before do
            @phone = create(:phone)
            allow(controller.request).to receive(:referer).and_return(edit_phone_path(@phone))
          end

          it "updates the requested Phone" do
            expect{
              put :update, params: { id: @phone.to_param, phone: new_attributes }
              @phone.reload
            }.to change(@phone, :phone_number)
          end

          it "responds with HTTP found" do
            put :update, params: { id: @phone.to_param, phone: new_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirects to the phone" do
            put :update, params: { id: @phone.to_param, phone: new_attributes }
            expect(response).to redirect_to(phone_path(@phone))
          end
        end
      end

      context "with valid params" do
        context "when called from elsewhere" do
          let(:new_attributes) {
            attributes_for(:phone)
          }

          before do
            @phone = create(:phone)
            allow(controller.request).to receive(:referer).and_return('elsewhere')
          end

          it "updates the requested phone" do
            expect{
              put :update, params: { id: @phone.to_param, phone: new_attributes }
              @phone.reload
            }.to change(@phone, :phone_number)
          end

          it "responds with HTTP found" do
            put :update, params: { id: @phone.to_param, phone: new_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirects to elsewhere" do
            put :update, params: { id: @phone.to_param, phone: new_attributes }
            expect(response).to redirect_to('elsewhere')
          end
        end
      end

      # context "with invalid params" do
      #   it "does not update the requested phone" do
      #     expect{
      #       put :update, params: { id: @phone.to_param, phone: invalid_attributes }
      #       @phone.reload
      #     }.not_to change(@phone, :phone_number)
      #   end
      #
      #   it "responds with HTTP success" do
      #     put :update, params: { id: @phone.to_param, phone: invalid_attributes }
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @phone = create(:phone) }

      it "destroys the requested phone" do
        expect {
          delete :destroy, params: { id: @phone.to_param }
        }.to change(Phone, :count).by(-1)
      end

      it "redirects to the phones list" do
        delete :destroy, params: { id: @phone.to_param }
        expect(response).to redirect_to(phones_path)
      end
    end

  end

end
