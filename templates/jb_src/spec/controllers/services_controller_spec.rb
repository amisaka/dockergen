describe ServicesController, type: :controller do

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with HTTP success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      subject { create(:service) }

      it "responds with HTTP success" do
        get :show, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with HTTP success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      subject { create(:service) }

      it "responds with HTTP success" do
        get :edit, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      subject { attributes_for(:service) }

      context "with valid params" do
        it "creates a new Service" do
          expect {
            post :create, params: { service: subject }
          }.to change(Service, :count).by(1)
        end

        it "responds with HTTP found" do
          post :create, params: { service: subject }
          expect(response).to have_http_status(:found)
        end

        it "redirects to the created service" do
          post :create, params: { service: subject }
          expect(response).to redirect_to(Service.last)
        end
      end

      # context "with invalid params" do
      #   it "does not create a new Service" do
      #     expect {
      #       post :create, service: invalid_attributes
      #     }.not_to change(Service, :count)
      #   end
      #
      #   it "responds with HTTP success" do
      #     post :create, service: invalid_attributes
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "PUT #update" do
      context "with valid params" do
        subject { create(:service) }
        let(:new_service) { attributes_for(:service) }

        it "updates the requested service" do
          expect{
            put :update, params: { id: subject.to_param, service: new_service }
            subject.reload
          }.to change(subject, :description)
        end

        it "responds with HTTP found" do
          put :update, params: { id: subject.to_param, service: new_service }
          expect(response).to have_http_status(:found)
        end

        it "redirects to the service" do
          put :update, params: { id: subject.to_param, service: new_service }
          expect(response).to redirect_to(service_path(subject))
        end
      end

      # context "with invalid params" do
      #   it "does not update the requested service" do
      #     expect{
      #       put :update, params: { id: @service.to_param, service: invalid_attributes }
      #       @service.reload
      #     }.not_to change(@service, :description)
      #   end
      #
      #   it "responds with HTTP success" do
      #     put :update, params: { id: @service.to_param, service: invalid_attributes }
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @service = create(:service) }

      it "destroys the requested service" do
        expect {
          delete :destroy, params: { id: @service.to_param }
        }.to change(Service, :count).by(-1)
      end

      it "redirects to the services list" do
        delete :destroy, params: { id: @service.to_param }
        expect(response).to redirect_to(services_path)
      end
    end

  end

end
