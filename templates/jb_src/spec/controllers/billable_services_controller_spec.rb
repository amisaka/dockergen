describe BillableServicesController, type: :controller do

  let(:valid_attributes) {
    attributes_for(:billable_service)
  }

  let(:invalid_attributes) {
    {}
  }


  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with HTTP success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      it "responds with HTTP success" do
        get :show, params: { id: create(:billable_service) }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with HTTP success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      it "responds with HTTP success" do
        get :edit, params: { id: create(:billable_service) }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        context "when called from #new" do
          before { allow(controller.request).to receive(:referer).and_return(new_billable_service_path) }

          it "creates a new Service" do
            expect {
              post :create, params: { billable_service: valid_attributes }
            }.to change(BillableService, :count).by(1)
          end

          it "responds with HTTP found" do
            post :create, params: { billable_service: valid_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirects to the created service" do
            post :create, params: { billable_service: valid_attributes }
            expect(response).to redirect_to(billable_service_path(BillableService.last))
          end
        end

        context "when called from anywhere else" do
          before { allow(controller.request).to receive(:referer).and_return('elsewhere') }

          it "creates a new Service" do
            expect {
              post :create, params: { billable_service: valid_attributes }
            }.to change(BillableService, :count).by(1)
          end

          it "responds with HTTP found" do
            post :create, params: { billable_service: valid_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirect to elsewhere" do
            post :create, params: { billable_service: valid_attributes }
            expect(response).to redirect_to('elsewhere')
          end
        end
      end

      # context "with invalid params" do
      #   it "does not create a new Service" do
      #     expect {
      #       post :create, billable_service: invalid_attributes
      #     }.not_to change(BillableService, :count)
      #   end
      #
      #   it "responds with HTTP success" do
      #     post :create, billable_service: invalid_attributes
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "PUT #update" do
      context "with valid params" do
        context "when called from #new" do
          let(:new_attributes) {
            attributes_for(:billable_service)
          }

          before do
            @billable_service = create(:billable_service)
            allow(controller.request).to receive(:referer).and_return(edit_billable_service_path(@billable_service))
          end

          it "updates the requested service" do
            expect{
              put :update, params: { id: @billable_service.to_param, billable_service: new_attributes }
              @billable_service.reload
            }.to change(@billable_service, :description)
          end

          it "responds with HTTP found" do
            put :update, params: { id: @billable_service.to_param, billable_service: new_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirects to the service" do
            put :update, params: { id: @billable_service.to_param, billable_service: new_attributes }
            expect(response).to redirect_to(billable_service_path(@billable_service))
          end
        end
      end

      context "with valid params" do
        context "when called from elsewhere" do
          let(:new_attributes) {
            attributes_for(:billable_service)
          }

          before do
            @billable_service = create(:billable_service)
            allow(controller.request).to receive(:referer).and_return('elsewhere')
          end

          it "updates the requested service" do
            expect{
              put :update, params: { id: @billable_service.to_param, billable_service: new_attributes }
              @billable_service.reload
            }.to change(@billable_service, :description)
          end

          it "responds with HTTP found" do
            put :update, params: { id: @billable_service.to_param, billable_service: new_attributes }
            expect(response).to have_http_status(:found)
          end

          it "redirects to elsewhere" do
            put :update, params: { id: @billable_service.to_param, billable_service: new_attributes }
            expect(response).to redirect_to('elsewhere')
          end
        end
      end

      # context "with invalid params" do
      #   it "does not update the requested service" do
      #     expect{
      #       put :update, id: @billable_service.to_param, billable_service: invalid_attributes
      #       @billable_service.reload
      #     }.not_to change(@billable_service, :description)
      #   end
      #
      #   it "responds with HTTP success" do
      #     put :update, id: @billable_service.to_param, billable_service: invalid_attributes
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @billable_service = create(:billable_service) }

      it "destroys the requested service" do
        expect {
          delete :destroy, params: { id: @billable_service.to_param }
        }.to change(BillableService, :count).by(-1)
      end

      it "redirects to the services list" do
        delete :destroy, params: { id: @billable_service.to_param }
        expect(response).to redirect_to(billable_services_path)
      end
    end

    describe "POST #add" do
      before do
        @client = create(:client)
        @invoice = create(:invoice, client: @client)
        @btn = create(:btn, client: @client)
        @billable_service = create(:billable_service, invoice: @invoice)
      end

      it "Adds a billable service to the client's btn-invoice" do
        expect {
          post :add, params: { client_id: @client.id, btn_id: @btn.id, invoice_id: @invoice.id, billable_service: @billable_service.attributes }
        }.to change(@client.billable_services, :count).by(1)
      end

      it "Creates a new billable service" do
        expect {
          post :add, params: { client_id: @client.id, btn_id: @btn.id, invoice_id: @invoice.id, billable_service: @billable_service.attributes }
        }.to change(BillableService, :count).by(1)
      end
    end

  end

end
