describe BuildingsController, type: :controller do

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with HTTP success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      subject { create(:building) }

      it "responds with HTTP success" do
        get :show, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with HTTP success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      subject { create(:building) }

      it "responds with HTTP success" do
        get :edit, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates a new Building" do
          expect {
            post :create, params: { building: attributes_for(:building) }
          }.to change(Building, :count).by(1)
        end

        it "responds with HTTP redirect" do
          post :create, params: { building: attributes_for(:building) }
          expect(response).to have_http_status(:redirect)
        end

        it "redirects to the created building" do
          post :create, params: { building: attributes_for(:building) }
          expect(response).to redirect_to(Building.last)
        end
      end

      # Buildings don't have validations yet
      #
      # context "with invalid params" do
      #   it "assigns a newly created but unsaved building as @building" do
      #     post :create, params: { building: invalid_attributes }
      #     expect(assigns(:building)).to be_a_new(Building)
      #   end
      #
      #   it "re-renders the 'new' template" do
      #     post :create, params: { building: invalid_attributes }
      #     expect(response).to render_template("new")
      #   end
      # end
    end

    describe "PUT #update" do
      context "with valid params" do
        subject { create(:building) }

        it "updates the requested building" do
          put :update, params: { id: subject.to_param, building: attrs = attributes_for(:building) }
          subject.reload
          expect(subject.address1).to eq(attrs[:address1])
          expect(subject.address2).to eq(attrs[:address2])
        end

        it "responds with HTTP redirect" do
          put :update, params: { id: subject.to_param, building: attributes_for(:building) }
          expect(response).to have_http_status(:redirect)
        end

        it "redirects to the building" do
          put :update, params: { id: subject.to_param, building: attributes_for(:building) }
          expect(response).to redirect_to(subject)
        end
      end

      # context "with invalid params" do
      #   it "assigns the building as @building" do
      #
      #     put :update, params: { id: building.to_param, building: invalid_attributes }
      #     expect(assigns(:building)).to eq(building)
      #   end
      #
      #   it "re-renders the 'edit' template" do
      #
      #     put :update, params: { id: building.to_param, building: invalid_attributes }
      #     expect(response).to render_template("edit")
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @building = create(:building) }

      it "destroys the requested building" do
        expect {
          delete :destroy, params: { id: @building.to_param }
        }.to change(Building, :count).by(-1)
      end

      it "redirects to the buildings list" do
        delete :destroy, params: { id: @building.to_param }
        expect(response).to redirect_to(buildings_url)
      end
    end

  end

end
