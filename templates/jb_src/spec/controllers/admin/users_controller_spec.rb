describe Admin::UsersController do
  # include Devise::TestHelpers
  # fixtures :users

  describe "admin users" do
    it "can create new user without password" do
      @adminuser= users(:admin)
      sign_in(@adminuser)
      pending

      post :create, :user => {
        :email => 'melanie+admin@linguistech.ca',
        :fullname => 'Melanie Rivet'
      }
      assert_response 201
    end
  end

  describe "admin users" do
    it "can create new user without password" do
      @adminuser= users(:admin)
      sign_in(@adminuser)
      pending

      post :create, :user => {
        :email => 'frank.jones@example.com',
        :fullname => 'Frank Jones'
      }
      assert_response 201
    end
  end

end
