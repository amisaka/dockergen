describe ClientsController, type: :controller do

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with http success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      subject { create(:client, btns: create_list(:btn, 3)) }

      it "responds with http success" do
        get :show, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with http success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      subject { create(:client, btns: create_list(:btn, 3)) }

      it "responds with http success" do
        get :edit, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        subject { attributes_for(:client) }

        it "creates a new Client" do
          expect {
            post :create, params: { client: subject }
          }.to change(Client, :count).by(1)
        end

        it "responds with http found" do
          post :create, params: { client: subject }
          expect(response).to have_http_status(:found)
        end

        it "redirects to the created client" do
          post :create, params: { client: subject }
          expect(response).to redirect_to client_path(Client.last)
        end
      end

      # context "with invalid params" do
      #   it "responds with http success" do
      #     post :create, params: { client: invalid_attributes }
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "PUT #update" do
      context "with valid params" do

        before { @client = create(:client, btns: create_list(:btn, 3)) }
        subject { attributes_for(:client) }

        it "updates the requested client" do
          expect{
            put :update, params: { id: @client.to_param, client: subject }
            @client.reload
          }.to change(@client, :updated_at)
        end

        it "responds with http found" do
          put :update, params: { id: @client.to_param, client: subject }
          expect(response).to have_http_status(:found)
        end

        it "redirects to the client" do
          put :update, params: { id: @client.to_param, client: subject }
          expect(response).to redirect_to client_path(@client)
        end
      end

      # context "with invalid params" do
      #   before do
      #     @client = clients(:credil)
      #   end
      #
      #   it "responds with http success" do
      #     put :update, params: { id: @client.to_param, client: invalid_attributes }
      #     expect(response).to have_http_status(:success)
      #   end
      # end
    end

    describe "DELETE #destroy" do
      before { @client = create(:client) }

      it "destroys the requested client" do
        expect {
          delete :destroy, params: { id: @client.to_param }
        }.to change(Client, :count).by(-1)
      end

      it "redirects to the clients list" do
        delete :destroy, params: { id: @client.to_param }
        expect(response).to redirect_to(clients_path)
      end
    end
  end

end
