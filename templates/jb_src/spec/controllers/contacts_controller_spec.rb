describe ContactsController, type: :controller do

  let(:invalid_attributes) {
    { email: 'invalid.email',
      name: '',
      phone_number: '' }
  }

  context "when a user is signed in" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in create(:user, admin: true)
    end

    describe "GET #index" do
      it "responds with http success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      subject { create(:contact) }

      it "responds with http success" do
        get :show, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #new" do
      it "responds with http success" do
        get :new
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      subject { create(:contact) }

      it "responds with http success" do
        get :edit, params: { id: subject.to_param }
        expect(response).to have_http_status(:success)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        subject { attributes_for(:contact) }

        it "creates a new Contact" do
          expect {
            post :create, params: { contact: subject }
          }.to change(Contact, :count).by(1)
        end

        it "responds with http found" do
          post :create, params: { contact: subject }
          expect(response).to have_http_status(:found)
        end

        it "redirects to the created contact" do
          post :create, params: { contact: subject }
          expect(response).to redirect_to contact_path(Contact.last)
        end
      end

      context "with invalid params" do
        it "responds with http success" do
          post :create, params: { contact: invalid_attributes }
          expect(response).to have_http_status(:success)
        end

        it "doesn't create a new Contact" do
          expect {
            post :create, params: { contact: invalid_attributes }
          }.not_to change(Contact, :count)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do

        before { @contact = create(:contact) }
        subject { attributes_for(:contact) }

        it "updates the requested contact" do
          expect {
            put :update, params: { id: @contact.to_param, contact: subject }
            @contact.reload
          }.to change(@contact, :updated_at)
        end

        it "responds with http found" do
          put :update, params: { id: @contact.to_param, contact: subject }
          expect(response).to have_http_status(:found)
        end

        it "redirects to the contact" do
          put :update, params: { id: @contact.to_param, contact: subject }
          expect(response).to redirect_to contact_path(@contact)
        end
      end

      context "with invalid params" do
        before do
          @contact = create(:contact)
        end

        it "responds with http success" do
          put :update, params: { id: @contact.to_param, contact: invalid_attributes }
          expect(response).to have_http_status(:success)
        end

        it "doesn't update the requested contact" do
          expect {
            put :update, params: { id: @contact.to_param, contact: invalid_attributes }
          }.not_to change(@contact, :updated_at)
          @contact.reload
          expect(@contact.email).not_to eq('swag')
        end
      end
    end

    describe "DELETE #destroy" do
      before { @contact = create(:contact) }

      it "destroys the requested contact" do
        expect {
          delete :destroy, params: { id: @contact.to_param }
        }.to change(Contact, :count).by(-1)
      end

      it "redirects to the contacts list" do
        delete :destroy, params: { id: @contact.to_param }
        expect(response).to redirect_to(contacts_path)
      end
    end
  end

end
