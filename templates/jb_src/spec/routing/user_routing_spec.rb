require "spec_helper"

describe Admin::UsersController do
  describe "routing" do
    it "should give administration interface for users" do
      expect({ :get => "/admin/users" }).to route_to(:controller => "admin/users", :action => "index")
    end

  end
end
