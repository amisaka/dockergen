describe SitesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/clients/1/sites").to route_to("sites#index", :client_id => "1")
    end

    it "routes to #new" do
      expect(:get => "/clients/1/sites/new").to route_to("sites#new", :client_id => "1")
    end

    it "routes to #show" do
      expect(:get => "/clients/1/sites/1").to route_to("sites#show", :id => "1", :client_id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/clients/1/sites/1/edit").to route_to("sites#edit", :id => "1", :client_id => "1")
    end

    it "routes to #create" do
      expect(:post => "/clients/1/sites").to route_to("sites#create", :client_id => "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/clients/1/sites/1").to route_to("sites#update", :id => "1", :client_id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/clients/1/sites/1").to route_to("sites#update", :id => "1", :client_id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/clients/1/sites/1").to route_to("sites#destroy", :id => "1", :client_id => "1")
    end

    it "routes to #add" do
      expect(:post => "/clients/1/sites/add").to route_to("sites#add", :client_id => "1")
    end

    it "routes to #remove" do
      expect(:delete => "/clients/1/sites/1/remove").to route_to("sites#remove", :id => "1", :client_id => "1")
    end

  end
end
