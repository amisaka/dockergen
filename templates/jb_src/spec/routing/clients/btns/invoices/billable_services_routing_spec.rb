module Clients
  module Btns
    module Invoices
      describe BillableServicesController, type: :routing do
        describe "routing" do

          it "routes to #index" do
            expect(:get => "/clients/1/btns/1/invoices/1/billable_services").to \
              route_to("billable_services#index", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #new" do
            expect(:get => "/clients/1/btns/1/invoices/1/billable_services/new").to \
              route_to("billable_services#new", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #show" do
            expect(:get => "/clients/1/btns/1/invoices/1/billable_services/1").to \
              route_to("billable_services#show", :id => "1", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #edit" do
            expect(:get => "/clients/1/btns/1/invoices/1/billable_services/1/edit").to \
              route_to("billable_services#edit", :id => "1", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #create" do
            expect(:post => "/clients/1/btns/1/invoices/1/billable_services").to \
              route_to("billable_services#create", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #update via PUT" do
            expect(:put => "/clients/1/btns/1/invoices/1/billable_services/1").to \
              route_to("billable_services#update", :id => "1", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #update via PATCH" do
            expect(:patch => "/clients/1/btns/1/invoices/1/billable_services/1").to \
              route_to("billable_services#update", :id => "1", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #destroy" do
            expect(:delete => "/clients/1/btns/1/invoices/1/billable_services/1").to \
              route_to("billable_services#destroy", :id => "1", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #add" do
            expect(:post => "/clients/1/btns/1/invoices/1/billable_services/add").to \
              route_to("billable_services#add", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

          it "routes to #remove" do
            expect(:delete => "/clients/1/btns/1/invoices/1/billable_services/1/remove").to \
              route_to("billable_services#remove", :id => "1", :client_id => "1", :btn_id => "1", :invoice_id => "1")
          end

        end
      end
    end
  end
end
