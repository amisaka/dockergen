describe PhonesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/clients/1/btns/1/phones").to route_to("phones#index", :client_id => "1", :btn_id => "1")
    end

    it "routes to #new" do
      expect(:get => "/clients/1/btns/1/phones/new").to route_to("phones#new", :client_id => "1", :btn_id => "1")
    end

    it "routes to #show" do
      expect(:get => "/clients/1/btns/1/phones/1").to route_to("phones#show", :id => "1", :client_id => "1", :btn_id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/clients/1/btns/1/phones/1/edit").to route_to("phones#edit", :id => "1", :client_id => "1", :btn_id => "1")
    end

    it "routes to #create" do
      expect(:post => "/clients/1/btns/1/phones").to route_to("phones#create", :client_id => "1", :btn_id => "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/clients/1/btns/1/phones/1").to route_to("phones#update", :id => "1", :client_id => "1", :btn_id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/clients/1/btns/1/phones/1").to route_to("phones#update", :id => "1", :client_id => "1", :btn_id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/clients/1/btns/1/phones/1").to route_to("phones#destroy", :id => "1", :client_id => "1", :btn_id => "1")
    end

    it "routes to #add" do
      expect(:post => "/clients/1/btns/1/phones/add").to route_to("phones#add", :client_id => "1", :btn_id => "1")
    end

    it "routes to #remove" do
      expect(:delete => "/clients/1/btns/1/phones/1/remove").to route_to("phones#remove", :id => "1", :client_id => "1", :btn_id => "1")
    end

  end
end
