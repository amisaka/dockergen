describe BillableServicesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/billable_services").to route_to("billable_services#index")
    end

    it "routes to #new" do
      expect(:get => "/billable_services/new").to route_to("billable_services#new")
    end

    it "routes to #show" do
      expect(:get => "/billable_services/1").to route_to("billable_services#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/billable_services/1/edit").to route_to("billable_services#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/billable_services").to route_to("billable_services#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/billable_services/1").to route_to("billable_services#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/billable_services/1").to route_to("billable_services#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/billable_services/1").to route_to("billable_services#destroy", :id => "1")
    end

    it "does not routes to #add" do
      expect(:post => "/billable_services/add").not_to route_to("billable_services#add")
    end

    it "does not routes to #remove" do
      expect(:delete => "/billable_services/1/remove").not_to route_to("billable_services#remove", :id => "1")
    end

  end
end
