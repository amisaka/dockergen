require "rails_helper"

RSpec.describe InvoicesController, type: :routing do
  describe "invoices" do

    it "routes to #updatepdf" do
      expect(:post => "/invoices/1/generate").to route_to("invoices#generate", :id => "1")
    end

    it "routes to #viewpdf" do
      expect(:get => "/invoices/1/viewpdf").to route_to("invoices#viewpdf", :id => "1")
    end
    it "routes to #viewhtml" do
      expect(:get => "/invoices/1/viewhtml").to route_to("invoices#viewhtml", :id => "1")
    end

  end
end
