require "rails_helper"

RSpec.describe EmailContactsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/email_contacts").to route_to("email_contacts#index")
    end

    it "routes to #new" do
      expect(:get => "/email_contacts/new").to route_to("email_contacts#new")
    end

    it "routes to #show" do
      expect(:get => "/email_contacts/1").to route_to("email_contacts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/email_contacts/1/edit").to route_to("email_contacts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/email_contacts").to route_to("email_contacts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/email_contacts/1").to route_to("email_contacts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/email_contacts/1").to route_to("email_contacts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/email_contacts/1").to route_to("email_contacts#destroy", :id => "1")
    end

  end
end
