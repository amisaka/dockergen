require "rails_helper"

RSpec.describe PhoneContactsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/phone_contacts").to route_to("phone_contacts#index")
    end

    it "routes to #new" do
      expect(:get => "/phone_contacts/new").to route_to("phone_contacts#new")
    end

    it "routes to #show" do
      expect(:get => "/phone_contacts/1").to route_to("phone_contacts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/phone_contacts/1/edit").to route_to("phone_contacts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/phone_contacts").to route_to("phone_contacts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/phone_contacts/1").to route_to("phone_contacts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/phone_contacts/1").to route_to("phone_contacts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/phone_contacts/1").to route_to("phone_contacts#destroy", :id => "1")
    end

  end
end
