require "rails_helper"

RSpec.describe TechnicalContactsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/technical_contacts").to route_to("technical_contacts#index")
    end

    it "routes to #new" do
      expect(:get => "/technical_contacts/new").to route_to("technical_contacts#new")
    end

    it "routes to #show" do
      expect(:get => "/technical_contacts/1").to route_to("technical_contacts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/technical_contacts/1/edit").to route_to("technical_contacts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/technical_contacts").to route_to("technical_contacts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/technical_contacts/1").to route_to("technical_contacts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/technical_contacts/1").to route_to("technical_contacts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/technical_contacts/1").to route_to("technical_contacts#destroy", :id => "1")
    end

  end
end
