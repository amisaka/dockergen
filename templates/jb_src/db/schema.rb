# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180720212915) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "fuzzystrmatch"

  create_table "accounts", force: :cascade do |t|
    t.string   "name",                default: "",    null: false
    t.string   "description",         default: "",    null: false
    t.string   "number",              default: "",    null: false
    t.datetime "opened_at"
    t.decimal  "opening_balance",     default: "0.0", null: false
    t.decimal  "current_balance",     default: "0.0", null: false
    t.datetime "closed_at"
    t.decimal  "closing_balance",     default: "0.0", null: false
    t.boolean  "terminated",          default: false, null: false
    t.integer  "terminating_user_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "deleted",             default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "auth_group", force: :cascade do |t|
    t.string   "name",       limit: 80,                 null: false
    t.boolean  "deleted",               default: false, null: false
    t.datetime "deleted_at"
    t.index ["name"], name: "auth_group_name_key", unique: true, using: :btree
  end

  create_table "auth_group_permissions", force: :cascade do |t|
    t.integer  "group_id",                      null: false
    t.integer  "permission_id",                 null: false
    t.boolean  "deleted",       default: false, null: false
    t.datetime "deleted_at"
    t.index ["group_id", "permission_id"], name: "auth_group_permissions_group_id_permission_id_key", unique: true, using: :btree
  end

  create_table "auth_message", force: :cascade do |t|
    t.integer  "user_id",                    null: false
    t.text     "message",                    null: false
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
    t.index ["user_id"], name: "auth_message_user_id", using: :btree
  end

  create_table "auth_permission", force: :cascade do |t|
    t.string   "name",            limit: 50,                  null: false
    t.integer  "content_type_id",                             null: false
    t.string   "codename",        limit: 100,                 null: false
    t.boolean  "deleted",                     default: false, null: false
    t.datetime "deleted_at"
    t.index ["content_type_id", "codename"], name: "auth_permission_content_type_id_codename_key", unique: true, using: :btree
    t.index ["content_type_id"], name: "auth_permission_content_type_id", using: :btree
  end

  create_table "auth_user", force: :cascade do |t|
    t.string   "username",     limit: 30,                  null: false
    t.string   "first_name",   limit: 30,                  null: false
    t.string   "last_name",    limit: 30,                  null: false
    t.string   "email",        limit: 75,                  null: false
    t.string   "password",     limit: 128,                 null: false
    t.boolean  "is_staff",                                 null: false
    t.boolean  "is_active",                                null: false
    t.boolean  "is_superuser",                             null: false
    t.datetime "last_login",                               null: false
    t.datetime "date_joined",                              null: false
    t.boolean  "deleted",                  default: false, null: false
    t.datetime "deleted_at"
    t.index ["username"], name: "auth_user_username_key", unique: true, using: :btree
  end

  create_table "auth_user_groups", force: :cascade do |t|
    t.integer  "user_id",                    null: false
    t.integer  "group_id",                   null: false
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
    t.index ["user_id", "group_id"], name: "auth_user_groups_user_id_group_id_key", unique: true, using: :btree
  end

  create_table "auth_user_user_permissions", force: :cascade do |t|
    t.integer  "user_id",                       null: false
    t.integer  "permission_id",                 null: false
    t.boolean  "deleted",       default: false, null: false
    t.datetime "deleted_at"
    t.index ["user_id", "permission_id"], name: "auth_user_user_permissions_user_id_permission_id_key", unique: true, using: :btree
  end

  create_table "billable_services", force: :cascade do |t|
    t.integer  "invoice_id"
    t.integer  "product_id"
    t.integer  "qty"
    t.datetime "billing_period_started_at"
    t.datetime "billing_period_ended_at"
    t.decimal  "discount",                  precision: 5, scale: 2
    t.text     "comments"
    t.decimal  "totalprice",                precision: 8, scale: 2
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.text     "product_name"
    t.integer  "asset_id"
    t.integer  "foreign_pid"
    t.decimal  "unitprice",                 precision: 8, scale: 2
    t.datetime "human_validation_date"
    t.integer  "site_id"
    t.boolean  "deleted",                                           default: false, null: false
    t.datetime "deleted_at"
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.boolean  "activated",                                         default: true,  null: false
    t.integer  "billing_period",                                    default: 1,     null: false
    t.integer  "client_id"
    t.integer  "charge_mode_id"
    t.integer  "prorate_id_start"
    t.integer  "recurrence_type"
    t.integer  "product_category_id"
    t.integer  "prorate_id_end"
    t.integer  "assignment_level"
    t.integer  "charge_pid"
    t.string   "reference_number"
    t.integer  "department_pid"
    t.integer  "supplier_id",                                                                    comment: "Was phohne.did_supplier_id"
    t.text     "password1"
    t.text     "supplier_account_no"
    t.text     "note"
    t.index ["billing_period_ended_at"], name: "index_billable_services_on_billing_period_ended_at", using: :btree
    t.index ["billing_period_started_at"], name: "index_billable_services_on_billing_period_started_at", using: :btree
    t.index ["client_id", "activated", "recurrence_type", "deleted"], name: "index_on_billable_services_for_invoices", using: :btree
    t.index ["client_id"], name: "index_billable_services_on_client_id", using: :btree
    t.index ["foreign_pid"], name: "index_billable_services_on_foreign_pid", using: :btree
    t.index ["invoice_id"], name: "index_billable_services_on_invoice_id", using: :btree
    t.index ["product_category_id"], name: "index_billable_services_on_product_category_id", using: :btree
    t.index ["product_id"], name: "index_billable_services_on_service_id", using: :btree
    t.index ["site_id"], name: "index_billable_services_on_site_id", using: :btree
    t.index ["supplier_id"], name: "index_billable_services_on_supplier_id", using: :btree
  end

  create_table "billings", force: :cascade do |t|
    t.integer  "client_id",                             null: false
    t.decimal  "balance",               default: "0.0"
    t.datetime "activated_at"
    t.datetime "billing_start_date"
    t.datetime "last_usage_date"
    t.datetime "last_statement_date"
    t.decimal  "last_statement_amount"
    t.decimal  "last_payment_amount"
    t.datetime "last_payment_date"
    t.datetime "deactivated_at"
    t.integer  "leg_pid"
    t.boolean  "deleted",               default: false, null: false
    t.datetime "deleted_at"
    t.boolean  "activated",             default: true,  null: false
    t.integer  "load_id"
    t.index ["client_id"], name: "index_billings_on_client_id", unique: true, using: :btree
    t.index ["load_id"], name: "index_billings_on_load_id", using: :btree
  end

  create_table "btns", force: :cascade do |t|
    t.string   "billingtelephonenumber",                                 null: false
    t.integer  "client_id",                                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "activated_at",           default: '1970-01-01 00:00:00'
    t.datetime "deactivated_at"
    t.integer  "rate_plan_id"
    t.boolean  "lastresort",             default: false
    t.integer  "local_rate_plan_id"
    t.integer  "national_rate_plan_id"
    t.integer  "world_rate_plan_id"
    t.integer  "rate_center_id"
    t.integer  "invoice_template_id"
    t.integer  "billing_site_id"
    t.boolean  "department_invoice"
    t.boolean  "invoicing_enabled",      default: true
    t.boolean  "deleted",                default: false,                 null: false
    t.datetime "deleted_at"
    t.boolean  "activated",              default: true,                  null: false
    t.index ["billingtelephonenumber", "client_id", "deactivated_at"], name: "index_btns_on_btn_and_c_id_and_deact_at", unique: true, using: :btree
    t.index ["billingtelephonenumber"], name: "index_btns_on_billingtelephonenumber", unique: true, using: :btree
    t.index ["client_id"], name: "index_btns_on_client_id", unique: true, using: :btree
  end

  create_table "buildings", force: :cascade do |t|
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.string   "postalcode"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "calls", force: :cascade do |t|
    t.string   "btn"
    t.string   "did"
    t.string   "called_number"
    t.datetime "call_date"
    t.string   "date"
    t.string   "time"
    t.string   "area"
    t.string   "province"
    t.boolean  "deleted",       default: false, null: false
    t.datetime "deleted_at"
    t.decimal  "cost",          default: "0.0", null: false
    t.integer  "duration",      default: 0,     null: false
    t.integer  "service_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.datetime "imported_at"
    t.index ["service_id"], name: "index_calls_on_service_id", using: :btree
  end

  create_table "callstats", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "type"
    t.integer  "value"
    t.integer  "stats_period_id"
    t.integer  "phone_id"
    t.boolean  "deleted",         default: false, null: false
    t.datetime "deleted_at"
    t.index ["phone_id"], name: "index_callstats_on_phone_id", using: :btree
    t.index ["stats_period_id", "type"], name: "index_callstats_on_stats_period_id_and_type", using: :btree
    t.index ["stats_period_id"], name: "index_callstats_on_stats_period_id", using: :btree
    t.index ["type"], name: "index_callstats_on_type", using: :btree
  end

  create_table "carriers", force: :cascade do |t|
    t.string   "name"
    t.string   "coverage"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "cdrviewer_areacode", force: :cascade do |t|
    t.string   "npa",         limit: 3,                  null: false
    t.string   "country",     limit: 16,                 null: false
    t.string   "description", limit: 64,                 null: false
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "cdrviewer_cdr", force: :cascade do |t|
    t.string   "calltype",                             limit: 13
    t.string   "direction",                            limit: 11
    t.string   "callednumber",                         limit: 161
    t.decimal  "starttime",                                         precision: 19, scale: 3
    t.datetime "startdatetime"
    t.time     "newstarttime"
    t.string   "recordid",                             limit: 48
    t.string   "serviceprovider",                      limit: 30
    t.string   "usernumber",                           limit: 16
    t.string   "groupnumber",                          limit: 16
    t.string   "callingnumber",                        limit: 161
    t.string   "callingpresentationindicator",         limit: 20
    t.string   "usertimezone",                         limit: 11
    t.string   "answerindicator",                      limit: 19
    t.string   "answertime",                           limit: 18
    t.string   "terminationcause",                     limit: 3
    t.string   "carrieridentificationcode",            limit: 4
    t.string   "dialeddigits",                         limit: 161
    t.text     "callcategory"
    t.string   "networkcalltype",                      limit: 4
    t.string   "networktranslatednumber",              limit: 161
    t.string   "networktranslatedgroup",               limit: 32
    t.string   "releasingparty",                       limit: 6
    t.string   "route",                                limit: 86
    t.string   "networkcallid",                        limit: 161
    t.string   "codec",                                limit: 30
    t.string   "accessdeviceaddress",                  limit: 80
    t.string   "accesscallid",                         limit: 161
    t.string   "failovercorrelationid",                limit: 161
    t.string   "group",                                limit: 30
    t.string   "department"
    t.string   "accountcode",                          limit: 14
    t.string   "authorizationcode",                    limit: 14
    t.string   "originalcallednumber",                 limit: 161
    t.string   "originalcalledpresentationindicator",  limit: 20
    t.string   "originalcalledreason",                 limit: 40
    t.string   "redirectingnumber",                    limit: 161
    t.string   "redirectingreason",                    limit: 40
    t.string   "chargeindicator",                      limit: 1
    t.string   "typeofnetwork",                        limit: 7
    t.string   "voiceportalcalling_invtime",           limit: 18
    t.string   "localcallid",                          limit: 40
    t.string   "remotecallid",                         limit: 40
    t.string   "callingpartycategory",                 limit: 20
    t.decimal  "releasetime",                                       precision: 19, scale: 3
    t.datetime "releasedatetime"
    t.time     "newreleasetime"
    t.string   "networktype",                          limit: 4
    t.string   "redirectingpresentationindicator",     limit: 20
    t.string   "instantconference_to",                 limit: 161
    t.string   "instantconference_from",               limit: 161
    t.string   "instantconference_conferenceid",       limit: 128
    t.string   "instantconference_role",               limit: 12
    t.string   "instantconference_bridge",             limit: 128
    t.string   "instantconference_owner",              limit: 161
    t.string   "instantconference_ownerdn",            limit: 32
    t.string   "instantconference_title",              limit: 80
    t.string   "instantconference_projectcode",        limit: 40
    t.string   "key",                                  limit: 161
    t.string   "creator",                              limit: 80
    t.string   "originatornetwork",                    limit: 80
    t.string   "terminatornetwork",                    limit: 80
    t.string   "userid",                               limit: 161
    t.string   "otherpartyname",                       limit: 80
    t.string   "otherpartynamepresentationindicator",  limit: 20
    t.string   "hoteling_group",                       limit: 30
    t.string   "hoteling_userid",                      limit: 161
    t.string   "hoteling_usernumber",                  limit: 15
    t.string   "hoteling_groupnumber",                 limit: 15
    t.string   "trunkgroupname"
    t.string   "clidpermitted",                        limit: 3
    t.string   "accessnetworkinfo",                    limit: 1024
    t.string   "relatedcallid",                        limit: 40
    t.string   "relatedcallidreason",                  limit: 40
    t.string   "transfer_relatedcallid",               limit: 161
    t.string   "transfer_type",                        limit: 20
    t.string   "conference_starttime",                 limit: 18
    t.string   "conference_stoptime",                  limit: 18
    t.string   "conference_confid",                    limit: 40
    t.string   "conference_type",                      limit: 10
    t.string   "faxmessaging",                         limit: 9
    t.string   "trunkgroupinfo"
    t.string   "recalltype",                           limit: 20
    t.string   "q850cause",                            limit: 3
    t.string   "dialeddigitscontext",                  limit: 161
    t.string   "callednumbercontext",                  limit: 161
    t.string   "networktranslatednumbercontext",       limit: 161
    t.string   "callingnumbercontext",                 limit: 161
    t.string   "originalcallednumbercontext",          limit: 161
    t.string   "redirectingnumbercontext",             limit: 161
    t.string   "routingnumber"
    t.string   "originationmethod",                    limit: 30
    t.string   "broadworksanywhere_relatedcallid",     limit: 40
    t.string   "calledassertedidentity",               limit: 161
    t.string   "calledassertedpresentationindicator",  limit: 20
    t.string   "sdp",                                  limit: 1024
    t.string   "ascalltype",                           limit: 11
    t.string   "cbfauthorizationcode",                 limit: 14
    t.string   "callbridge_callbridgeresult",          limit: 7
    t.string   "prepaidstatus",                        limit: 22
    t.string   "configurableclid",                     limit: 20
    t.string   "instantconference_invtime",            limit: 18
    t.string   "instantconference_callid",             limit: 161
    t.string   "instantconference_recording_duration", limit: 10
    t.datetime "answerdatetime"
    t.time     "newanswertime"
    t.integer  "datasource_id"
    t.integer  "lineno"
    t.string   "usagetype",                            limit: 3
    t.string   "productid",                            limit: 161
    t.boolean  "originateportedflag"
    t.string   "originatenumber",                      limit: 161
    t.boolean  "destinationportedflag"
    t.string   "destinationnumber",                    limit: 161
    t.string   "forwardedcallingnumber",               limit: 161
    t.string   "originatelrn",                         limit: 161
    t.string   "destinationlrn",                       limit: 161
    t.integer  "duration"
    t.integer  "callingphone_id"
    t.integer  "calledphone_id"
    t.integer  "callingbtn_id"
    t.integer  "calledbtn_id"
    t.integer  "call_id"
    t.integer  "called_rate_center_id"
    t.integer  "invoice_id"
    t.text     "billingreason"
    t.text     "orig_networkcalltype"
    t.decimal  "call_cost",                                         precision: 7,  scale: 3
    t.decimal  "call_price",                                        precision: 7,  scale: 3
    t.boolean  "deleted",                                                                    default: false, null: false
    t.datetime "deleted_at"
    t.index ["call_id"], name: "index_cdrviewer_cdr_on_call_id", using: :btree
    t.index ["calledbtn_id"], name: "index_cdrviewer_cdr_on_calledbtn_id", using: :btree
    t.index ["callednumber"], name: "index_cdrviewer_cdr_on_callednumber", using: :btree
    t.index ["calledphone_id"], name: "cdrviewer_cdr_calledphone_id", using: :btree
    t.index ["callingbtn_id"], name: "index_cdrviewer_cdr_on_callingbtn_id", using: :btree
    t.index ["callingnumber"], name: "index_cdrviewer_cdr_on_callingnumber", using: :btree
    t.index ["callingphone_id"], name: "cdrviewer_cdr_callingphone_id", using: :btree
    t.index ["datasource_id"], name: "index_cdrviewer_cdr_on_datasource_id", using: :btree
    t.index ["id"], name: "index_cdrviewer_cdr_on_id", unique: true, using: :btree
    t.index ["invoice_id"], name: "index_cdrviewer_cdr_on_invoice_id", using: :btree
    t.index ["localcallid"], name: "index_cdrviewer_cdr_on_localcallid", using: :btree
    t.index ["newanswertime"], name: "cdrviewer_cdr_newanswertime", using: :btree
    t.index ["newreleasetime"], name: "cdrviewer_cdr_newreleasetime", using: :btree
    t.index ["newstarttime"], name: "cdrviewer_cdr_newstarttime", using: :btree
    t.index ["relatedcallid"], name: "index_cdrviewer_cdr_on_relatedcallid", using: :btree
    t.index ["remotecallid"], name: "index_cdrviewer_cdr_remotecallid", using: :btree
    t.index ["startdatetime", "lineno", "id"], name: "index_cdrviewer_cdr_on_startdatetime_and_lineno_and_id", using: :btree
    t.index ["startdatetime"], name: "cdrviewer_cdr_startdatetime", using: :btree
    t.index ["transfer_relatedcallid"], name: "index_cdrviewer_cdr_on_transfer_relatedcallid", using: :btree
  end

  create_table "cdrviewer_dailypeak", force: :cascade do |t|
    t.date     "date",           default: '1970-01-01'
    t.integer  "midnight_calls", default: 0,            null: false
    t.integer  "peak_calls",     default: 0,            null: false
    t.boolean  "deleted",        default: false,        null: false
    t.datetime "deleted_at"
  end

  create_table "cdrviewer_datasource", force: :cascade do |t|
    t.text     "datasource_filename",                    null: false
    t.integer  "analysis_level",         default: 0
    t.integer  "active_day",             default: 0
    t.integer  "cdrs_count",             default: 0
    t.integer  "calls_count",            default: 0
    t.integer  "load_time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "data_at"
    t.integer  "did_supplier_id"
    t.integer  "toll_recalculate_count"
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
    t.index ["datasource_filename"], name: "index_cdrviewer_datasource_on_datasource_filename", using: :btree
  end

  create_table "charge_modes", force: :cascade do |t|
    t.text     "mode",                            null: false
    t.integer  "charge_mode_pid"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "deleted",         default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "client_line_of_businesses", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "line_of_business_id"
    t.boolean  "deleted",             default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "activated",           default: true,  null: false
    t.datetime "activated_at"
    t.datetime "deactivated_at"
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.integer  "billing_site_id"
    t.string   "admin_contact"
    t.string   "technical_contact"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
    t.string   "logo"
    t.string   "url"
    t.integer  "foreign_pid"
    t.text     "billing_btn"
    t.integer  "billing_cycle_id"
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.text     "language_preference"
    t.string   "crm_account_id"
    t.boolean  "stats_enabled",         default: false
    t.datetime "human_validation_date"
    t.boolean  "needs_review",          default: false, null: false
    t.integer  "status",                default: 0,     null: false
    t.integer  "sales_person_id"
    t.integer  "service_level",         default: 1,     null: false
    t.boolean  "deleted",               default: false, null: false
    t.datetime "deleted_at"
    t.boolean  "activated",             default: true,  null: false
    t.text     "note"
    t.integer  "coverage_id",           default: 1,     null: false
    t.index ["billing_btn"], name: "index_clients_on_billing_btn", using: :btree
    t.index ["coverage_id"], name: "index_clients_on_coverage_id", using: :btree
    t.index ["id"], name: "index_clients_on_id", unique: true, using: :btree
    t.index ["sales_person_id"], name: "index_clients_on_sales_person_id", using: :btree
  end

  create_table "coverages", force: :cascade do |t|
    t.string   "name_french",                 null: false
    t.boolean  "deleted",     default: false, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "pid"
    t.datetime "deleted_at"
    t.index ["name_french"], name: "index_coverages_on_name_french", unique: true, using: :btree
  end

  create_table "crm_accounts", id: false, force: :cascade do |t|
    t.text     "id"
    t.text     "name"
    t.datetime "date_entered"
    t.datetime "date_modified"
    t.text     "modified_user_id"
    t.text     "created_by"
    t.text     "description"
    t.integer  "deleted",                     default: 0
    t.text     "assigned_user_id"
    t.text     "account_type"
    t.text     "industry"
    t.text     "annual_revenue"
    t.text     "phone_fax"
    t.text     "billing_address_street"
    t.text     "billing_address_city"
    t.text     "billing_address_state"
    t.text     "billing_address_postalcode"
    t.text     "billing_address_country"
    t.text     "rating"
    t.text     "phone_office"
    t.text     "phone_alternate"
    t.text     "website"
    t.text     "ownership"
    t.text     "employees"
    t.text     "ticker_symbol"
    t.text     "shipping_address_street"
    t.text     "shipping_address_city"
    t.text     "shipping_address_state"
    t.text     "shipping_address_postalcode"
    t.text     "shipping_address_country"
    t.text     "parent_id"
    t.text     "sic_code"
    t.text     "campaign_id"
    t.datetime "deleted_at"
    t.string   "billing_address_street_2"
    t.index ["id"], name: "index_crm_accounts_on_id", unique: true, using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0,     null: false
    t.integer  "attempts",   default: 0,     null: false
    t.text     "handler",                    null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "did_suppliers", force: :cascade do |t|
    t.text     "name"
    t.boolean  "voip"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
    t.integer  "pid"
  end

  create_table "digitalsignage_custommessage", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "custom_message", limit: 256
    t.date     "date_created"
    t.datetime "datetime_start"
    t.datetime "datetime_end"
    t.string   "server"
    t.string   "macaddr"
    t.boolean  "deleted",                    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "django_admin_log", force: :cascade do |t|
    t.datetime "action_time",                                 null: false
    t.integer  "user_id",                                     null: false
    t.integer  "content_type_id"
    t.text     "object_id"
    t.string   "object_repr",     limit: 200,                 null: false
    t.integer  "action_flag",     limit: 2,                   null: false
    t.text     "change_message",                              null: false
    t.boolean  "deleted",                     default: false, null: false
    t.datetime "deleted_at"
    t.index ["content_type_id"], name: "django_admin_log_content_type_id", using: :btree
    t.index ["user_id"], name: "django_admin_log_user_id", using: :btree
  end

  create_table "django_content_type", force: :cascade do |t|
    t.string   "name",       limit: 100,                 null: false
    t.string   "app_label",  limit: 100,                 null: false
    t.string   "model",      limit: 100,                 null: false
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
    t.index ["app_label", "model"], name: "django_content_type_app_label_model_key", unique: true, using: :btree
  end

  create_table "django_session", force: :cascade do |t|
    t.string   "session_key",  limit: 40,                 null: false
    t.text     "session_data",                            null: false
    t.datetime "expire_date",                             null: false
    t.boolean  "deleted",                 default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "django_site", force: :cascade do |t|
    t.string   "domain",     limit: 100,                 null: false
    t.string   "name",       limit: 50,                  null: false
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "email_contacts", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "email"
    t.string   "name"
    t.boolean  "activated",         default: true,  null: false
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.boolean  "deleted",           default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "email_type"
    t.boolean  "subscribed"
    t.integer  "email_address_pid"
  end

  create_table "gl_accounts", force: :cascade do |t|
    t.text     "code",                                     null: false
    t.text     "description",                              null: false
    t.boolean  "active",                    default: true, null: false
    t.integer  "gl_account_entry_type_pid"
    t.integer  "gl_account_pid"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "deleted"
    t.index ["code"], name: "index_gl_accounts_on_code", unique: true, using: :btree
    t.index ["gl_account_pid"], name: "index_gl_accounts_on_gl_account_pid", unique: true, using: :btree
  end

  create_table "inventories", force: :cascade do |t|
    t.string   "address1"
    t.string   "adresse_ip_lan_data"
    t.string   "adresse_ip_lan_voip"
    t.string   "adresse_ip_link_data"
    t.string   "adresse_ip_link_voip"
    t.string   "adresse_ipv6_lan_data"
    t.string   "adresse_ipv6_lan_voip"
    t.string   "adresse_ipv6_link_data"
    t.string   "adresse_ipv6_link_voip"
    t.string   "agreement_num"
    t.string   "auto_renewal_term"
    t.string   "billingtelephonenumber"
    t.string   "btn"
    t.string   "circuit_num_logique"
    t.string   "circuit_num_physique"
    t.string   "circuit_num"
    t.string   "city"
    t.string   "compte_fttb"
    t.string   "customer_name"
    t.string   "customer_sn"
    t.datetime "deleted_at"
    t.boolean  "deleted"
    t.string   "description"
    t.string   "devicenamemonitored"
    t.string   "device"
    t.integer  "did_supplier_id"
    t.string   "download_speed"
    t.string   "email_of_courrier"
    t.date     "expiry_date"
    t.string   "installed_on_line"
    t.string   "inventory_type"
    t.integer  "inventory_type_id"
    t.string   "mac_address"
    t.string   "numerodereference"
    t.string   "parent_agreement_num"
    t.string   "password"
    t.integer  "product_id"
    t.string   "product_name"
    t.string   "quantity"
    t.string   "realsyncspeeddownload"
    t.string   "realsyncspeedupload"
    t.string   "router_principal"
    t.string   "router_vrrp"
    t.string   "service_id"
    t.integer  "site_id"
    t.string   "site_name"
    t.string   "site_number"
    t.string   "sous_interface_data"
    t.string   "sous_interface_voip"
    t.string   "sous_interface_voip_vrrp"
    t.date     "start_date"
    t.string   "supplier_num"
    t.string   "supplier"
    t.string   "terminate_on"
    t.string   "term"
    t.string   "upload_speed"
    t.string   "username"
    t.string   "vlan_data_q_in_q"
    t.string   "vlan_supplier"
    t.string   "vlan_voip_q_in_q"
    t.string   "vl_videotron"
    t.string   "vrrp_id"
    t.string   "z_end"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "inventory_status_id"
    t.date     "activated_at"
    t.date     "deactivated_at"
  end

  create_table "inventory_auto_renewal_terms", force: :cascade do |t|
    t.string   "machine_name"
    t.string   "user_name"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "inventory_devices", force: :cascade do |t|
    t.string   "machine_name"
    t.string   "user_name"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "inventory_fournisseurs", force: :cascade do |t|
    t.string   "machine_name"
    t.string   "user_name"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "inventory_router_principals", force: :cascade do |t|
    t.string   "machine_name"
    t.string   "user_name"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "inventory_router_vrrps", force: :cascade do |t|
    t.string   "machine_name"
    t.string   "user_name"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "inventory_terms", force: :cascade do |t|
    t.string   "machine_name"
    t.string   "user_name"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "inventory_tests", force: :cascade do |t|
    t.string   "btn"
    t.string   "customer_name"
    t.string   "start_date"
    t.string   "agreement_num"
    t.string   "auto_renewal_term"
    t.string   "circuit_num_logique"
    t.string   "circuit_num_physique"
    t.string   "download_speed"
    t.string   "expiry_date"
    t.string   "parent_agreement_num"
    t.string   "term"
    t.string   "upload_speed"
    t.string   "z_end"
    t.string   "supplier"
    t.string   "email_of_courrier"
    t.string   "adresse_ip_lan_data"
    t.string   "adresse_ip_lan_voip"
    t.string   "adresse_ip_link_data"
    t.string   "adresse_ip_link_voip"
    t.string   "adresse_ipv6_lan_data"
    t.string   "adresse_ipv6_lan_voip"
    t.string   "adresse_ipv6_link_data"
    t.string   "adresse_ipv6_link_voip"
    t.string   "device"
    t.string   "router_principal"
    t.string   "router_vrrp"
    t.string   "sous_interface_data"
    t.string   "sous_interface_voip"
    t.string   "sous_interface_voip_vrrp"
    t.string   "vlan_data_q_in_q"
    t.string   "vlan_supplier"
    t.string   "vlan_voip_q_in_q"
    t.string   "vrrp_id"
    t.string   "address1"
    t.string   "city"
    t.string   "billingtelephonenumber"
    t.string   "site_number"
    t.string   "site_name"
    t.string   "description"
    t.string   "product_code"
    t.string   "product_name"
    t.string   "quantity"
    t.string   "service_id"
    t.string   "terminate_on"
    t.string   "vl_videotron"
    t.string   "supplier_num"
    t.string   "customer_sn"
    t.string   "mac_address"
    t.string   "compte_fttb"
    t.string   "installed_on_line"
    t.string   "numerodereference"
    t.string   "password"
    t.string   "realsyncspeeddownload"
    t.string   "realsyncspeedupload"
    t.string   "username"
    t.string   "circuit_num"
    t.string   "devicenamemonitored"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "inventory_types", force: :cascade do |t|
    t.string   "machine_name"
    t.string   "user_name"
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "invoice_templates", force: :cascade do |t|
    t.text     "name"
    t.text     "invoice_resources_path"
    t.text     "invoice_cycle"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "irregular_invoice",      default: false
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.decimal  "amount"
    t.decimal  "total"
    t.string   "tax_province"
    t.string   "tax_country"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "deleted",                  default: false, null: false
    t.datetime "deleted_at"
    t.decimal  "previous_balance"
    t.decimal  "payment_received"
    t.decimal  "late_fee_charges"
    t.decimal  "past_due_balance"
    t.decimal  "monthly_charges"
    t.datetime "usage_charges_from"
    t.datetime "usage_charges_to"
    t.decimal  "usage_charges"
    t.decimal  "total_current_charges"
    t.decimal  "due_immediately"
    t.date     "due_date"
    t.decimal  "total_due_by_amount"
    t.decimal  "total_due"
    t.decimal  "before_balance"
    t.decimal  "after_balance"
    t.decimal  "bc_pst_charges",           default: "0.0"
    t.decimal  "sask_pst_charges",         default: "0.0"
    t.decimal  "qst_charges",              default: "0.0"
    t.decimal  "manitoba_rst_charges",     default: "0.0"
    t.decimal  "gst_charges"
    t.decimal  "hst_charges",              default: "0.0"
    t.decimal  "tax_total_charges"
    t.decimal  "bc_pst_calls",             default: "0.0"
    t.decimal  "sask_pst_calls",           default: "0.0"
    t.decimal  "qst_calls",                default: "0.0"
    t.decimal  "manitoba_rst_calls",       default: "0.0"
    t.decimal  "hst_calls",                default: "0.0"
    t.decimal  "gst_calls"
    t.decimal  "tax_total_calls"
    t.decimal  "alberta_pst_charges",      default: "0.0"
    t.decimal  "alberta_pst_calls",        default: "0.0"
    t.datetime "posted_at"
    t.date     "payments_shown_until",                     null: false
    t.date     "late_payment_date",                        null: false
    t.date     "payments_shown_since",                     null: false
    t.integer  "site_id"
    t.integer  "parent_invoice_id",                                     comment: "Parent invoice if it's a site only invoice"
    t.date     "usage_charges_begin",                                   comment: "The latest date we include in this invoice one-tiem charges for, usually  1st of a month as of July 2018"
    t.date     "usage_charges_end",                                     comment: "The earliest date we include in this invoice one-tiem charges for, usually  1st of a month as of July 2018"
    t.date     "look_ahead_charges_begin",                              comment: "The earliest date we include monthly recurring charges for, usually  1st of a month as of July 2018"
    t.date     "look_ahead_charges_end",                                comment: "The latest date we  include monthly recurring charges for, usualy last day of a month as of July 2018"
    t.text     "notes"
    t.index ["client_id", "site_id"], name: "index_invoices_on_client_id_and_site_id", using: :btree
    t.index ["parent_invoice_id"], name: "index_invoices_on_parent_invoice_id", using: :btree
    t.index ["posted_at"], name: "index_invoices_on_posted_at", using: :btree
    t.index ["site_id"], name: "index_invoices_on_site_id", using: :btree
  end

  create_table "invoices_payments", id: false, force: :cascade do |t|
    t.integer "payment_id", null: false
    t.integer "invoice_id", null: false
    t.index ["invoice_id"], name: "index_invoices_payments_on_invoice_id", using: :btree
    t.index ["payment_id"], name: "index_invoices_payments_on_payment_id", using: :btree
  end

  create_table "lavenshtein_exchanges", id: false, force: :cascade do |t|
    t.string   "province",   limit: 255
    t.string   "localname",  limit: 255
    t.string   "lergname",   limit: 255
    t.integer  "lev"
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "ledgers", force: :cascade do |t|
    t.integer  "btn_id"
    t.integer  "balance_account_id"
    t.integer  "invoice_account_id"
    t.integer  "payment_account_id"
    t.boolean  "deleted",            default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "lerg8s", force: :cascade do |t|
    t.string   "shortName"
    t.string   "fullName"
    t.string   "province"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "status"
    t.text     "entryNum"
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "line_of_businesses", force: :cascade do |t|
    t.string   "code"
    t.string   "description"
    t.boolean  "deleted",     default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "Pid"
  end

  create_table "local_exchanges", force: :cascade do |t|
    t.string   "province"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "carrier"
    t.integer  "carrierplan_id"
    t.datetime "mapped_at"
    t.integer  "rate_center_id"
    t.datetime "deleted_at"
    t.boolean  "sip_availability", default: false
    t.boolean  "on_net",           default: false
    t.boolean  "deleted",          default: false, null: false
    t.index ["name", "province"], name: "index_local_exchanges_on_name_and_province", using: :btree
  end

  create_table "local_exchanges_north_american_exchanges", force: :cascade do |t|
    t.integer  "local_exchanges_id"
    t.integer  "north_american_exchanges_id"
    t.datetime "updated_at"
    t.integer  "rate_center_id"
    t.boolean  "deleted",                     default: false, null: false
    t.datetime "deleted_at"
    t.index ["local_exchanges_id", "north_american_exchanges_id"], name: "index_l_na_exchanges", unique: true, using: :btree
  end

  create_table "north_american_exchanges", force: :cascade do |t|
    t.string   "province"
    t.string   "name"
    t.integer  "npa"
    t.integer  "nxx"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "mapped_at"
    t.integer  "rate_center_id"
    t.datetime "deleted_at"
    t.text     "coc_type"
    t.text     "special_service_code"
    t.boolean  "portable_indicator"
    t.text     "switch"
    t.text     "ocn"
    t.text     "lata"
    t.boolean  "deleted",              default: false, null: false
    t.index ["name", "province"], name: "index_north_american_exchanges_on_name_and_province", using: :btree
    t.index ["npa", "nxx"], name: "index_north_american_exchanges_on_npa_and_nxx", unique: true, using: :btree
  end

  create_table "payments", force: :cascade do |t|
    t.datetime "date",                                                           null: false
    t.decimal  "amount",                precision: 20, scale: 3,                 null: false
    t.text     "type"
    t.text     "reason"
    t.integer  "client_id"
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.datetime "posted_date",                                                                 comment: "Imported database attribute which I hope, for better data integrity, to not use.  A posted payment is a payment that is part of a posted invoice"
    t.decimal  "balance"
    t.string   "description"
    t.boolean  "imported"
    t.boolean  "deleted",                                        default: false, null: false
    t.datetime "deleted_at"
    t.string   "reference_number"
    t.string   "check_number"
    t.string   "payment_method"
    t.integer  "GLAccount"
    t.decimal  "before_balance"
    t.decimal  "after_balance"
    t.integer  "gl_account_id"
    t.integer  "transaction_type_id"
    t.integer  "transaction_method_id"
    t.integer  "invoice_run_id"
  end

  create_table "payments2", id: false, force: :cascade do |t|
    t.integer  "id",                                             default: -> { "nextval('payments_id_seq'::regclass)" }, null: false
    t.datetime "date"
    t.decimal  "amount",                precision: 11, scale: 3
    t.integer  "client_id"
    t.datetime "created_at",                                                                                             null: false
    t.datetime "updated_at",                                                                                             null: false
    t.datetime "posted_date"
    t.string   "description"
    t.boolean  "deleted",                                        default: false,                                         null: false
    t.datetime "deleted_at"
    t.string   "reference_number"
    t.string   "check_number"
    t.integer  "gl_account_id"
    t.integer  "transaction_type_id"
    t.integer  "transaction_method_id"
    t.integer  "invoice_run_id"
  end

  create_table "payments_old", id: :integer, default: -> { "nextval('payments_id_seq'::regclass)" }, force: :cascade do |t|
    t.datetime "date"
    t.decimal  "amount",                precision: 11, scale: 3
    t.text     "type"
    t.text     "reason"
    t.integer  "client_id"
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.datetime "posted_date"
    t.decimal  "balance"
    t.string   "description"
    t.boolean  "posted"
    t.boolean  "imported"
    t.boolean  "deleted",                                        default: false, null: false
    t.datetime "deleted_at"
    t.string   "reference_number"
    t.string   "check_number"
    t.string   "payment_method"
    t.integer  "GLAccount"
    t.decimal  "before_balance"
    t.decimal  "after_balance"
    t.integer  "gl_account_id"
    t.integer  "transaction_type_id"
    t.integer  "transaction_method_id"
    t.integer  "invoice_run_id"
  end

  create_table "performance_stats", force: :cascade do |t|
    t.text     "login"
    t.text     "version"
    t.text     "name"
    t.text     "type"
    t.integer  "client_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "work_count"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "phone_contacts", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "phone_number"
    t.string   "name"
    t.boolean  "activated",      default: true,  null: false
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.boolean  "deleted",        default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "phone_type"
  end

  create_table "phone_service_types", force: :cascade do |t|
    t.string   "code"
    t.string   "description"
    t.boolean  "active",      default: true,  null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "deleted",     default: false, null: false
    t.datetime "deleted_at"
    t.integer  "Pid"
    t.boolean  "toll_free",   default: false, null: false
  end

  create_table "phones", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "site_id"
    t.integer  "btn_id"
    t.string   "phone_number"
    t.string   "extension"
    t.string   "voicemail_number"
    t.string   "access_info"
    t.text     "notes"
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.string   "phone_mac_addr"
    t.integer  "phone_type_id"
    t.string   "sip_username"
    t.string   "e911_notes"
    t.boolean  "e911_tested"
    t.integer  "e911_tested_by_id"
    t.date     "e911_tested_date"
    t.integer  "did_supplier_id"
    t.string   "email"
    t.string   "ipaddress"
    t.string   "connectiontype_id"
    t.text     "sip_password"
    t.boolean  "virtualdid",            default: false
    t.boolean  "voicemailportal",       default: false
    t.string   "callerid"
    t.boolean  "huntgroup",             default: false
    t.boolean  "autocreate",            default: false
    t.integer  "local_rate_plan_id"
    t.integer  "national_rate_plan_id"
    t.integer  "world_rate_plan_id"
    t.text     "firstname"
    t.text     "lastname"
    t.text     "calling_firstname"
    t.text     "calling_lastname"
    t.text     "foreign_userId"
    t.integer  "foreign_pid"
    t.integer  "rate_center_id"
    t.datetime "human_validation_date"
    t.boolean  "incoming_toll",         default: false
    t.boolean  "deleted",               default: false, null: false
    t.datetime "deleted_at"
    t.boolean  "activated",             default: true,  null: false
    t.integer  "line_of_business_id"
    t.integer  "department_pid"
    t.integer  "phone_service_type_id"
    t.string   "description"
    t.string   "display_text"
    t.string   "couverture"
    t.string   "defaultorigination"
    t.string   "defaultdestination"
    t.index ["foreign_pid"], name: "index_phones_on_foreign_pid", using: :btree
    t.index ["id"], name: "index_phones_on_id", unique: true, using: :btree
  end

  create_table "portal_customerservice", force: :cascade do |t|
    t.integer  "service_id",                  null: false
    t.integer  "customer_id",                 null: false
    t.datetime "startdate",                   null: false
    t.datetime "enddate"
    t.boolean  "deleted",     default: false, null: false
    t.datetime "deleted_at"
    t.index ["customer_id"], name: "portal_customerservice_customer_id", using: :btree
    t.index ["service_id"], name: "portal_customerservice_service_id", using: :btree
  end

  create_table "portal_service", force: :cascade do |t|
    t.string   "name",        limit: 32,                   null: false
    t.text     "description",                              null: false
    t.string   "url",         limit: 32, default: "/foo/", null: false
    t.string   "icon",        limit: 64, default: "",      null: false
    t.boolean  "deleted",                default: false,   null: false
    t.datetime "deleted_at"
  end

  create_table "portal_userprofile", force: :cascade do |t|
    t.integer  "user_id",                                null: false
    t.string   "logo",       limit: 256
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
    t.index ["user_id"], name: "portal_userprofile_user_id_key", unique: true, using: :btree
  end

  create_table "portal_userprofilecustomer", force: :cascade do |t|
    t.integer  "user_profile_id",                 null: false
    t.integer  "customer_id",                     null: false
    t.datetime "startdate",                       null: false
    t.datetime "enddate"
    t.boolean  "deleted",         default: false, null: false
    t.datetime "deleted_at"
    t.index ["customer_id"], name: "portal_userprofilecustomer_customer_id", using: :btree
    t.index ["user_profile_id"], name: "portal_userprofilecustomer_user_profile_id", using: :btree
  end

  create_table "product_categories", force: :cascade do |t|
    t.text     "code",                                           null: false
    t.text     "description",                                    null: false
    t.boolean  "active",                          default: true, null: false
    t.integer  "bc_product_billing_category_pid"
    t.integer  "product_category_pid"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.boolean  "deleted"
    t.datetime "deleted_at"
    t.datetime "imported_at"
    t.index ["code"], name: "index_product_categories_on_code", unique: true, using: :btree
    t.index ["product_category_pid"], name: "index_product_categories_on_product_category_pid", unique: true, using: :btree
  end

  create_table "product_classes", force: :cascade do |t|
    t.integer  "code",                             null: false
    t.text     "description",                      null: false
    t.boolean  "active",            default: true, null: false
    t.integer  "product_class_pid"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "deleted"
    t.datetime "imported_at"
    t.index ["code"], name: "index_product_classes_on_code", unique: true, using: :btree
    t.index ["product_class_pid"], name: "index_product_classes_on_product_class_pid", unique: true, using: :btree
  end

  create_table "product_types", force: :cascade do |t|
    t.text     "name",             null: false
    t.integer  "product_type_pid"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.boolean  "deleted"
    t.datetime "imported_at"
    t.index ["name"], name: "index_product_types_on_name", unique: true, using: :btree
    t.index ["product_type_pid"], name: "index_product_types_on_product_type_pid", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.text     "description"
    t.text     "name"
    t.decimal  "unit_cost",                   precision: 7, scale: 2
    t.boolean  "renewable"
    t.boolean  "active"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.integer  "product_pid"
    t.boolean  "service_has_asset",                                   default: false
    t.boolean  "deleted",                                             default: false, null: false
    t.datetime "deleted_at"
    t.text     "code"
    t.boolean  "multi_jurisdictional_taxing"
    t.decimal  "amount"
    t.decimal  "purchase_amount"
    t.integer  "product_class_id"
    t.integer  "product_category_id"
    t.integer  "product_type_id"
    t.integer  "line_of_business_id"
    t.boolean  "tax_passthrough"
    t.integer  "gl_account_id"
    t.boolean  "allow_fractional_qty"
    t.integer  "pp_product_pid"
    t.integer  "bc_product_pid"
    t.datetime "imported_at"
    t.index ["gl_account_id"], name: "index_products_on_gl_account_id", using: :btree
    t.index ["line_of_business_id"], name: "index_products_on_line_of_business_id", using: :btree
    t.index ["product_category_id"], name: "index_products_on_product_category_id", using: :btree
    t.index ["product_class_id"], name: "index_products_on_product_class_id", using: :btree
    t.index ["product_pid"], name: "index_products_on_product_pid", unique: true, using: :btree
    t.index ["product_type_id"], name: "index_products_on_product_type_id", using: :btree
  end

  create_table "project_codes", force: :cascade do |t|
    t.integer  "client_id",                            null: false
    t.integer  "project_pid"
    t.integer  "code"
    t.boolean  "active",               default: true,  null: false
    t.integer  "customer_pid"
    t.integer  "department_pid"
    t.integer  "site_id"
    t.integer  "service_id"
    t.integer  "assignment_level_pid"
    t.integer  "owner_app_user_pid"
    t.integer  "insert_app_user_pid"
    t.integer  "update_app_user_pid"
    t.datetime "imported_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.boolean  "deleted",              default: false, null: false
    t.datetime "deleted_at"
    t.text     "description"
    t.index ["client_id"], name: "index_project_codes_on_client_id", using: :btree
    t.index ["service_id"], name: "index_project_codes_on_service_id", using: :btree
    t.index ["site_id"], name: "index_project_codes_on_site_id", using: :btree
  end

  create_table "prorates", force: :cascade do |t|
    t.text     "meaning",                     null: false
    t.integer  "prorate_pid"
    t.boolean  "deleted",     default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "rate_centers", force: :cascade do |t|
    t.string   "province",              limit: 5
    t.string   "lerg6name",             limit: 10
    t.datetime "deleted_at"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.text     "isocountry",                       default: "US"
    t.text     "country",                          default: "United States"
    t.integer  "country_code",                     default: 1
    t.boolean  "mobile",                           default: false
    t.text     "localnames",                       default: [],                           array: true
    t.text     "city_code"
    t.boolean  "virtual_did",                      default: false
    t.boolean  "tollfree",                         default: false
    t.boolean  "north_american_lookup",            default: false
    t.boolean  "seven_digit"
    t.boolean  "deleted",                          default: false,           null: false
    t.index ["province", "lerg6name"], name: "index_rate_centers_on_province_and_lerg6name", using: :btree
    t.index ["province"], name: "index_rate_centers_on_province", using: :btree
  end

  create_table "rate_plans", force: :cascade do |t|
    t.string   "province"
    t.string   "country"
    t.string   "code"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "type"
    t.integer  "country_code"
    t.integer  "carrier_id"
    t.boolean  "deleted",             default: false, null: false
    t.datetime "deleted_at"
    t.date     "effective_date"
    t.integer  "rate_plan_pid"
    t.integer  "usage_type_id"
    t.boolean  "active"
    t.integer  "owner_app_user_pid"
    t.integer  "owner_team_pid"
    t.integer  "insert_app_user_pid"
    t.integer  "update_app_user_pid"
    t.integer  "bc_rate_plan_pid"
    t.datetime "imported_at"
    t.index ["usage_type_id"], name: "index_rate_plans_on_usage_type_id", using: :btree
  end

  create_table "rates", force: :cascade do |t|
    t.integer  "rate_plan_id"
    t.integer  "rate_center_id"
    t.float    "cost"
    t.integer  "billing_increment"
    t.integer  "minimum_call_length"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "deleted",             default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "reports", force: :cascade do |t|
    t.string   "name"
    t.datetime "reported_at"
    t.json     "value"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "deleted",     default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "sales_people", force: :cascade do |t|
    t.string   "name",        default: "",    null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "deleted",     default: false, null: false
    t.datetime "deleted_at"
    t.integer  "foreign_pid"
  end

  create_table "sensors", force: :cascade do |t|
    t.string   "name"
    t.string   "inner_ipv4"
    t.string   "inner_ipv6"
    t.string   "outer_ipv4"
    t.string   "outer_ipv6"
    t.string   "macaddress"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "services", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "site_id"
    t.integer  "btn_id",                                          null: false
    t.string   "service_id",                                      null: false
    t.string   "extension"
    t.string   "voicemail_number"
    t.string   "access_info"
    t.text     "notes"
    t.datetime "start_date"
    t.datetime "termination_date"
    t.string   "phone_mac_addr"
    t.integer  "phone_type_id"
    t.string   "sip_username"
    t.string   "e911_notes"
    t.boolean  "e911_tested"
    t.integer  "e911_tested_by_id"
    t.date     "e911_tested_date"
    t.string   "email"
    t.string   "ipaddress"
    t.string   "connectiontype_id"
    t.text     "sip_password"
    t.boolean  "virtualdid",                      default: false
    t.boolean  "voicemailportal",                 default: false
    t.string   "callerid"
    t.boolean  "huntgroup",                       default: false
    t.boolean  "autocreate",                      default: false
    t.integer  "local_rate_plan_id"
    t.integer  "national_rate_plan_id"
    t.integer  "world_rate_plan_id"
    t.text     "firstname"
    t.text     "lastname"
    t.text     "calling_firstname"
    t.text     "calling_lastname"
    t.text     "foreign_userId"
    t.integer  "service_pid"
    t.integer  "rate_center_id"
    t.datetime "human_validation_date"
    t.boolean  "incoming_toll",                   default: false
    t.boolean  "deleted",                         default: false, null: false
    t.datetime "deleted_at"
    t.boolean  "activated",                       default: true,  null: false
    t.integer  "department_pid"
    t.integer  "phone_service_type_id"
    t.string   "description"
    t.string   "display_text"
    t.integer  "line_of_businesses_id"
    t.string   "defaultorigination"
    t.string   "defaultdestination"
    t.integer  "service_assignment_type_pid",                                  comment: "We have a service type, but no service assignment type.  Assuming integer for now"
    t.integer  "department_line_of_business_pid"
    t.integer  "customer_category_pid"
    t.integer  "current_account_status_type_pid"
    t.boolean  "provision_later"
    t.text     "comments"
    t.boolean  "privacy",                         default: false
    t.integer  "insert_app_user_pid"
    t.bigint   "update_app_user_pid"
    t.integer  "topic_pid"
    t.boolean  "private_line"
    t.integer  "owner_app_user_pid"
    t.integer  "default_service",                 default: 0,                  comment: "All zero in source data? Boolean? We don't know"
    t.boolean  "close_finalised",                 default: false
    t.boolean  "billed",                          default: false
    t.text     "default_origination"
    t.text     "default_destination"
    t.datetime "supplier_activation_date"
    t.integer  "service_usage_type_pid",                                       comment: "We have a usage type and a service type, but no service_usage type.  Assuming integer for now"
    t.integer  "rate_plan_id"
    t.integer  "resultant_rate_plan_id"
    t.datetime "last_usage_date"
    t.integer  "supplier_id",                                                  comment: "Was phohne.did_supplier_id"
    t.integer  "coverage_id",                                                  comment: "Was phones.couverture (integer)"
    t.boolean  "validate_project_code"
    t.integer  "customer_line_of_business_pid"
    t.integer  "usage_type_id"
    t.datetime "imported_at"
    t.index ["btn_id"], name: "index_services_on_btn_id", using: :btree
    t.index ["coverage_id"], name: "index_services_on_coverage_id", using: :btree
    t.index ["department_line_of_business_pid"], name: "index_services_on_department_line_of_business_pid", using: :btree
    t.index ["id"], name: "index_services_on_id", unique: true, using: :btree
    t.index ["line_of_businesses_id"], name: "index_services_on_line_of_businesses_id", using: :btree
    t.index ["rate_plan_id"], name: "index_services_on_rate_plan_id", using: :btree
    t.index ["resultant_rate_plan_id"], name: "index_services_on_resultant_rate_plan_id", using: :btree
    t.index ["service_pid"], name: "index_services_on_service_pid", using: :btree
    t.index ["supplier_id"], name: "index_services_on_supplier_id", using: :btree
    t.index ["usage_type_id"], name: "index_services_on_usage_type_id", using: :btree
  end

  create_table "settings", force: :cascade do |t|
    t.text     "name",                         null: false, comment: "Name of the setting"
    t.text     "value",                                     comment: "Value of the setting"
    t.text     "setting_type",                 null: false, comment: "Not sure what is right enumeration for this.  Valid choices are in Setting::TYPE_LIST"
    t.integer  "max_length",   default: 0
    t.boolean  "deleted",      default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "short_names", force: :cascade do |t|
    t.string   "from"
    t.string   "to"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "sites", force: :cascade do |t|
    t.string   "name"
    t.string   "unitnumber"
    t.integer  "building_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "client_id"
    t.text     "site_number"
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.integer  "site_rep_id"
    t.text     "bw_groupId"
    t.text     "alt_name"
    t.integer  "foreign_pid"
    t.text     "support_service_level", default: "1"
    t.integer  "phones_count",          default: 0,     null: false
    t.integer  "active_phones_count",   default: 0,     null: false
    t.boolean  "deleted",               default: false, null: false
    t.datetime "deleted_at"
    t.boolean  "activated",             default: true,  null: false
    t.index ["id"], name: "index_sites_on_id", unique: true, using: :btree
  end

  create_table "statement_type_clients", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "statement_type_id"
    t.boolean  "deleted",           default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["client_id"], name: "index_statement_type_clients_on_client_id", using: :btree
    t.index ["statement_type_id"], name: "index_statement_type_clients_on_statement_type_id", using: :btree
  end

  create_table "statement_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.boolean  "deleted",     default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "locale"
  end

  create_table "stats_periods", force: :cascade do |t|
    t.date     "day"
    t.integer  "hour"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "system_variables", force: :cascade do |t|
    t.string   "variable"
    t.string   "value"
    t.integer  "number"
    t.boolean  "deleted",    default: false, null: false
    t.datetime "deleted_at"
  end

  create_table "technical_contacts", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "email"
    t.string   "name"
    t.boolean  "activated",      default: true,  null: false
    t.datetime "activated_at"
    t.datetime "deactivated_at"
    t.boolean  "deleted",        default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "department"
  end

  create_table "transaction_methods", force: :cascade do |t|
    t.text     "methodname"
    t.integer  "transaction_type_id"
    t.text     "description"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.datetime "deleted_at"
    t.boolean  "deleted",             default: false, null: false
  end

  create_table "transaction_types", force: :cascade do |t|
    t.text     "typename"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.datetime "deleted_at"
    t.boolean  "deleted",    default: false, null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "debit_account_id",                   null: false
    t.integer  "credit_account_id",                  null: false
    t.string   "name",               default: "",    null: false
    t.string   "description",        default: "",    null: false
    t.datetime "date",                               null: false
    t.string   "number",             default: "",    null: false
    t.decimal  "amount",             default: "0.0", null: false
    t.boolean  "deleted",            default: false, null: false
    t.integer  "deleted_by_user_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.datetime "deleted_at"
  end

  create_table "usage_types", force: :cascade do |t|
    t.text     "name"
    t.integer  "usage_type_pid"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "deleted",        default: false, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "class_id"
    t.string   "phone"
    t.string   "fullname"
    t.string   "username"
    t.boolean  "agent"
    t.boolean  "admin"
    t.datetime "reset_password_sent_at"
    t.boolean  "deleted",                default: false, null: false
    t.datetime "deleted_at"
    t.datetime "last_seen_at"
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["username"], name: "index_users_on_username", using: :btree
  end

  add_foreign_key "billable_services", "did_suppliers", column: "supplier_id"
  add_foreign_key "calls", "services", on_update: :cascade, on_delete: :nullify
  add_foreign_key "clients", "coverages"
  add_foreign_key "invoices", "invoices", column: "parent_invoice_id"
  add_foreign_key "products", "gl_accounts"
  add_foreign_key "products", "line_of_businesses"
  add_foreign_key "products", "product_categories"
  add_foreign_key "products", "product_classes"
  add_foreign_key "products", "product_types"
  add_foreign_key "project_codes", "clients"
  add_foreign_key "project_codes", "services"
  add_foreign_key "project_codes", "sites"
  add_foreign_key "services", "coverages"
  add_foreign_key "services", "did_suppliers", column: "supplier_id"
  add_foreign_key "services", "line_of_businesses", column: "line_of_businesses_id"
  add_foreign_key "services", "line_of_businesses", column: "line_of_businesses_id"
  add_foreign_key "services", "rate_plans", column: "resultant_rate_plan_id"
end
