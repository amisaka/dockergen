class AddDefaultToTaxColumnsInvoices < ActiveRecord::Migration[5.0]
  def change
    Invoice.new.attributes.keys.select {|a| ["pst", "qst", "hst", "rst"].any? {|t| a.include?(t)}}.map {|a| a.to_sym}.each { |tax_attribute|
        change_column_default :invoices, tax_attribute, 0
    }
  end
end
