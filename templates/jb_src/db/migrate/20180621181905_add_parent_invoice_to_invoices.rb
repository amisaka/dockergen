class AddParentInvoiceToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_reference :invoices, :parent_invoice, index: true,             null: true, comment: "Parent invoice if it's a site only invoice"
    add_foreign_key :invoices, :invoices, column: 'parent_invoice_id', null: true, comment: "Parent invoice if it's a site only invoice"
  end
end
