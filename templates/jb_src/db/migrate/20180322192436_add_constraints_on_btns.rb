class AddConstraintsOnBtns < ActiveRecord::Migration[5.0]
  def change
    change_column_null :btns, :billingtelephonenumber, false
    change_column_null :btns, :client_id, false
    remove_index :btns, :billingtelephonenumber
    remove_index :btns, :client_id
    add_index :btns, :billingtelephonenumber, unique: true
    add_index :btns, :client_id, unique: true
  end
end
