class ChangeCostToDecimal < ActiveRecord::Migration[5.0]
  def up
    remove_column :calls, :cost
    add_column :calls, :cost, :decimal, null: false, default: 0.0
  end

  def down
    remove_column :calls, :cost
    add_column :calls, :cost, :string, null: false, default: ''
  end
end
