class RenameDeptLineOfBusinessToIntegerServices < ActiveRecord::Migration[5.0]
  def change
    rename_column :services, :department_line_of_business_id, :department_line_of_business_pid
    remove_foreign_key :services, name: "fk_rails_6997e820b1"
  end
end
