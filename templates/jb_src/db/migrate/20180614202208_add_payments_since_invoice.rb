class AddPaymentsSinceInvoice < ActiveRecord::Migration[5.0]
  def up
    add_column :invoices, :payments_shown_since, :date

    sql = %Q[UPDATE invoices SET payments_shown_since = payments_shown_until - INTERVAL '1 MONTH' where payments_shown_since is null]
    Invoice.connection.execute(sql)

    change_column_null :invoices, :payments_shown_since, false
  end

  def down
    remove_column :invoices, :payments_shown_since
  end

end
