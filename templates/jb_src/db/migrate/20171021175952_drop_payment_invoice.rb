class DropPaymentInvoice < ActiveRecord::Migration[5.0]
  def up
    drop_table :payment_invoices
  end

  def down
    create_table "payment_invoices", force: :cascade do |t|
      t.integer  "payment_id"
      t.integer  "invoice_id"
      t.decimal  "amount"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer  "btn_id"
    end
  end
end
