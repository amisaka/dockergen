class AddActivatedAtToInventory < ActiveRecord::Migration[5.0]
  def change
    add_column :inventories, :activated_at, :date
  end
end
