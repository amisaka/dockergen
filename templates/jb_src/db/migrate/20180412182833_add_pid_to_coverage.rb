class AddPidToCoverage < ActiveRecord::Migration[5.0]
  def change
    add_column :coverages, :pid, :integer
  end
end
