class AddInvoiceRunId < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :invoice_run_id, :integer
  end
end
