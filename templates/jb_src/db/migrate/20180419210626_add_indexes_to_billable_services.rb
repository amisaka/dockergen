class AddIndexesToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_index :billable_services, :invoice_id
#    add_index :billable_services, :service_id
  end
end
