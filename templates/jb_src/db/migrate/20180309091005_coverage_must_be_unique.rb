class CoverageMustBeUnique < ActiveRecord::Migration[5.0]
  def change
    Coverage.where("id > ?", 2).delete_all

    add_index :coverages, [:name_french], unique: true
  end
end
