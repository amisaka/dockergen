class AddTimestampsToCalls < ActiveRecord::Migration[5.0]
  def change
	# From https://makandracards.com/makandra/19325-your-database-tables-should-always-have-timestamps
    add_timestamps :calls, null: true

    # Backfill missing data with a really old date
    time = Time.zone.parse('2000-01-01 00:00:00')
    update "UPDATE calls SET created_at = '#{time}'"
    update "UPDATE calls SET updated_at = '#{time}'"

    # Restore NOT NULL constraints to be in line with the Rails default
    change_column_null :calls, :created_at, false
    change_column_null :calls, :updated_at, false

	# Also add imported_at column
	add_column :calls, :imported_at, :datetime
  end
end
