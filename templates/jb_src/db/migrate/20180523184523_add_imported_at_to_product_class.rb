class AddImportedAtToProductClass < ActiveRecord::Migration[5.0]
  def change
    add_column :product_classes, :imported_at, :datetime
  end
end
