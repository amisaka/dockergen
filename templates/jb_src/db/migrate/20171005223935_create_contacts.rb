class CreateContacts < ActiveRecord::Migration[5.0]
  def up
    create_table :contacts do |t|

      t.belongs_to :client

      t.string  :email,        default: '',    null: false
      t.string  :name,         default: '',    null: false
      t.string  :role,         default: '',    null: false
      t.string  :phone_number, default: '',    null: false
      t.boolean :active,       default: true,  null: false
      t.boolean :deleted,      default: false, null: false

      t.timestamps null: false
    end
  end

  def down
    drop_table :contacts
  end
end
