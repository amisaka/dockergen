class AddImportedAtToProductCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :product_categories, :imported_at, :datetime
  end
end
