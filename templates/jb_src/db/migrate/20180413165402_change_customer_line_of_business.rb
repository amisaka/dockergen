class ChangeCustomerLineOfBusiness < ActiveRecord::Migration[5.0]
  def change
    remove_reference :services, :customer_line_of_business
    add_column :services, :customer_line_of_business_pid, :integer
  end
end
