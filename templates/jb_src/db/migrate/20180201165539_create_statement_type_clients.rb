class CreateStatementTypeClients < ActiveRecord::Migration[5.0]
  def change
    create_table :statement_type_clients do |t|
      t.belongs_to :client, index: true
      t.belongs_to :statement_type, index: true
      t.boolean :deleted,      default: false, null: false
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
