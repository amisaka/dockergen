class CreateChargeModes < ActiveRecord::Migration[5.0]
  def change
    create_table :charge_modes do |t|
      t.text :mode, :null =>false
      t.integer :charge_mode_pid

      t.timestamps
    end

  end
end
