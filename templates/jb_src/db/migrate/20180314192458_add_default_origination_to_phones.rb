class AddDefaultOriginationToPhones < ActiveRecord::Migration[5.0]
  def change
    add_column :phones, :defaultorigination, :string
  end
end
