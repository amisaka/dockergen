class CreateProrates < ActiveRecord::Migration[5.0]
  def change
    create_table :prorates do |t|
      t.text :meaning, null: false
      t.integer :prorate_pid

      t.boolean  :deleted, default: false, null: false
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
