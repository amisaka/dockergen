class ChangeForeignKeyConstraintOnCallsToServices < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :calls, :services

    add_foreign_key :calls, :services, on_update: :cascade, on_delete: :nullify
  end
end
