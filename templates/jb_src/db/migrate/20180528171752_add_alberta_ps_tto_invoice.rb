class AddAlbertaPsTtoInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :alberta_pst_charges, :decimal, default:0
    add_column :invoices, :alberta_pst_calls,   :decimal, default:0
  end
end
