class AddDeletedToChargeMode < ActiveRecord::Migration[5.0]
  def change
    add_column :charge_modes, :deleted, :boolean, default: false, null: false
    add_column :charge_modes, :deleted_at, :datetime

  end
end
