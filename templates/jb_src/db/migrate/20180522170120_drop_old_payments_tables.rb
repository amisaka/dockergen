class DropOldPaymentsTables < ActiveRecord::Migration[5.0]
  def change
    drop_table :payments_old2, if_exists: true
    drop_table :payments_old, if_exists: true
    drop_table :payments2, if_exists: true
    drop_table :payments3, if_exists: true
  end
end
