class AddColumnsToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :code, :text
    add_column :products, :multi_jurisdictional_texting, :boolean
    add_column :products, :amount, :decimal
    add_column :products, :purchase_amount, :decimal
    add_reference :products, :product_class, foreign_key: true
    add_reference :products, :product_category, foreign_key: true
    add_reference :products, :product_type, foreign_key: true
    add_reference :products, :line_of_business, foreign_key: true
                             #line_of_businesses
    add_column :products, :tax_passthrough, :boolean
    add_reference :products, :gl_account, foreign_key: true
    add_column :products, :allow_fractional_qty, :boolean
    add_column :products, :pp_product_pid, :integer
    add_column :products, :bc_product_pid, :integer

    rename_column :products, :available, :active
    rename_column :products, :unitprice, :unit_cost
    rename_column :products, :foreign_pid, :product_pid
  end
end
