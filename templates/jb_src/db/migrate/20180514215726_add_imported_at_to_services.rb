class AddImportedAtToServices < ActiveRecord::Migration[5.0]
  def change
    add_column :services, :imported_at, :datetime
  end
end
