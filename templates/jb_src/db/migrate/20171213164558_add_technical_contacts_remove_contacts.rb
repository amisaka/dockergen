class AddTechnicalContactsRemoveContacts < ActiveRecord::Migration[5.0]
  def up
    drop_table :contacts

    create_table :technical_contacts do |t|
      t.integer :client_id

      t.string :email
      t.string :name

      t.boolean :activated, null: false, default: true
      t.datetime :activated_at
      t.datetime :deactivated_at

      t.boolean :deleted, default: false
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end

  def down
    drop_table :technical_contacts

    create_table :contacts do |t|
      t.integer  "client_id"
      t.string   "email",          default: "",    null: false
      t.string   "name",           default: "",    null: false
      t.string   "role",           default: "",    null: false
      t.string   "phone_number",   default: "",    null: false
      t.boolean  "activated",      default: true,  null: false
      t.datetime "created_at",                     null: false
      t.datetime "updated_at",                     null: false
      t.boolean  "deleted",        default: false, null: false
      t.datetime "deleted_at"
      t.datetime "activated_at"
      t.datetime "deactivated_at"
      t.index ["client_id"], name: "index_contacts_on_client_id", using: :btree
    end
  end
end
