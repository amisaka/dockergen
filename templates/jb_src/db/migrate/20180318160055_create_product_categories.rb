class CreateProductCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :product_categories do |t|
      t.text :code, :null => false
      t.text :description, :null => false
      t.boolean :active, :default => true, :null => false
      t.integer :bc_product_billing_category_pid
      t.integer :product_category_pid

      t.timestamps
    end

    add_index :product_categories, :code, unique: true
    add_index :product_categories, :product_category_pid, unique: true

  end
end
