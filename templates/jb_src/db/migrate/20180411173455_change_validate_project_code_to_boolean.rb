class ChangeValidateProjectCodeToBoolean < ActiveRecord::Migration[5.0]
  def change
    remove_reference :services, :validate_project_code
    add_column :services, :validate_project_code, :boolean
  end
end
