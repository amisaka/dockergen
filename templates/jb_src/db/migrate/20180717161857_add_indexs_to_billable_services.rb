class AddIndexsToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_index :billable_services, :site_id
    add_index :billable_services, :billing_period_started_at
    add_index :billable_services, :billing_period_ended_at
    add_index :billable_services, :product_category_id

  end
end
