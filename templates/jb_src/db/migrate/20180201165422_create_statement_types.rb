class CreateStatementTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :statement_types do |t|
      t.string :name
      t.string :description
      t.boolean :deleted,      default: false, null: false
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
