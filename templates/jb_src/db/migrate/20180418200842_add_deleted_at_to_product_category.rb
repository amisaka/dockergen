class AddDeletedAtToProductCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :product_categories, :deleted_at, :datetime

  end
end
