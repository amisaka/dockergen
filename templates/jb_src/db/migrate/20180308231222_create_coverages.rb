class CreateCoverages < ActiveRecord::Migration[5.0]
  def change
    create_table :coverages do |t|
      t.string :name_french, null: false
      t.boolean :deleted,      default: false, null: false

      t.timestamps
    end

    Coverage.create!(:name_french => "Couverture Provinciale")
    Coverage.create!(:name_french => "Couverture Nationale")


  end
end
