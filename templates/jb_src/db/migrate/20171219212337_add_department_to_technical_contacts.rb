class AddDepartmentToTechnicalContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :technical_contacts, :department, :string
  end
end
