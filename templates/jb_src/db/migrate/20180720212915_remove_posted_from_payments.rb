class RemovePostedFromPayments < ActiveRecord::Migration[5.0]
  def change
    remove_column :payments, :posted
    execute %q[comment on column payments.posted_date is 'Imported database attribute which I hope, for better data integrity, to not use.  A posted payment is a payment that is part of a posted invoice']
  end
end
