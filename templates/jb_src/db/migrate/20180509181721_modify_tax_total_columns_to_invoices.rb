class ModifyTaxTotalColumnsToInvoices < ActiveRecord::Migration[5.0]
  def change
    rename_column   :invoices, :tax_total, :tax_total_charges
    add_column      :invoices, :tax_total_calls, :decimal
  end
end
