class AddPaymentMethodToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :payment_method, :string
  end
end
