class AddPostedAtToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :posted_at, :datetime
    add_index  :invoices, :posted_at
  end
end
