class DropInvoiceProcessingErrors < ActiveRecord::Migration[5.0]
  def up
    drop_table :invoice_processing_errors
  end

  def down
    create_table "invoice_processing_errors", force: :cascade do |t|
      t.integer  "invoice_run_id"
      t.integer  "cdr_id"
      t.text     "reason"
      t.text     "missing_thing"
      t.datetime "fixed_at"
      t.datetime "created_at",     null: false
      t.datetime "updated_at",     null: false
    end
  end
end
