class AddNotesToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :note, :text
  end
end
