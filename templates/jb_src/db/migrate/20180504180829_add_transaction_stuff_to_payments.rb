class AddTransactionStuffToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :transaction_types, :integer
    add_column :payments, :transaction_method, :integer

  end
end
