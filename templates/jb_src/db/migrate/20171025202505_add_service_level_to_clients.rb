class AddServiceLevelToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :service_level, :integer, null: false, default: 1
    Client.update_all(service_level: 1)
  end
end
