class AddColumnsToServices < ActiveRecord::Migration[5.0]
  def change
    #add_column :services, :btn, :reference,                     null: false    # Exists already
    #add_column :services, :display_text, :text                                 # Exists already
    #add_column :services, :description, :text                                  # Exists already
    add_column :services, :service_assignment_type_pid, :integer, comment: %q[We have a service type, but no service assignment type.  Assuming integer for now]
    add_reference :services, :department_line_of_business, foreign_key: { to_table: :line_of_businesses }
    add_reference :services, :customer_line_of_business, foreign_key: { to_table: :line_of_businesses }
    #add_column :services, :department_pid, :integer,           comment: %q[As of 2018/04/10, we have no departments table, so assuming this is integer]
    #add_column :services, :line_of_business, :reference                        # Exists already
    add_column :services, :customer_category_pid, :integer
    add_column :services, :current_account_status_type_pid, :integer
    #add_column :services, :created_on, :datetie                        # Exists already
    add_reference :services, :validate_project_code                     # has been changed back to boolean
    add_column :services, :provision_later, :boolean
    add_column :services, :comments, :text
    add_column :services, :privacy, :boolean,                   default: false
    add_column :services, :insert_app_user_pid, :integer
    add_column :services, :update_app_user_pid, :integer
    add_column :services, :topic_pid, :integer
    add_column :services, :private_line, :boolean
    add_column :services, :owner_app_user_pid, :integer
    add_column :services, :default_service, :integer,           default: 0, comment: %Q[All zero in source data? Boolean? We don't know]
    #add_column :services, :updated_on, :datetime                        # Exists already
    #add_column :services, :service_type, :reference,           # Exists already as phone_service_type
    add_column :services, :close_finalised, :boolean,           default: false
    add_column :services, :billed, :boolean,                    default: false
    add_column :services, :default_origination, :integer
    add_column :services, :default_destination, :integer
    add_column :services, :supplier_activation_date, :datetime
    add_column :services, :service_usage_type_pid, :integer,    comment: %q[We have a usage type and a service type, but no service_usage type.  Assuming integer for now]
    add_reference :services, :usage_types
    add_reference :services, :rate_plan
    add_reference :services, :resultant_rate_plan, foreign_key: { to_table: :rate_plans }
    add_column :services, :last_usage_date, :datetime


    #Column renames and replacements
    rename_column :services, :foreign_pid, :service_pid#,        comment: 'Primary key of source data, assuming.  Was phone.foreign_pid before'

    #add_column :services, :service_id, :integer,
    rename_column :services, :phone_number, :service_id#,        comment: 'This seems to be a vDID.  Renamed from phone.phone_number'

    #add_column :services, :start_date, :datetime
    rename_column :services, :activated_at, :start_date#,        comment: %q[Was phone.activated_at]

    #add_column :services, :termination_date, :datetime
    rename_column :services, :deactivated_at, :termination_date#,comment: %q[Was phone.deactivated_at]

    #Column replacements
    remove_column :services, :did_supplier_id
    add_reference :services, :supplier, foreign_key: { to_table: :did_suppliers}, comment: %q[Was phohne.did_supplier_id]

    remove_column :services, :couverture
    add_reference :services, :coverage, foreign_key: true,      comment: %q[Was phones.couverture (integer)]

  end
end
