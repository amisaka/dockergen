class RemoveTaxTable < ActiveRecord::Migration[5.0]
  def change
    drop_table :tax_amounts
  end
end
