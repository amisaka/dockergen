class ChangeCallsDurationInteger < ActiveRecord::Migration[5.0]
  def up
    remove_column :calls, :duration
    add_column :calls, :duration, :integer, null: false, default: 0
  end

  def down
    remove_column :calls, :duration
    add_column :calls, :duration, :string, null: false, default: ''
  end
end
