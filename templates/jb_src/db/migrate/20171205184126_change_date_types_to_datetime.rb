class ChangeDateTypesToDatetime < ActiveRecord::Migration[5.0]
  def up
    change_column :billable_services, :billing_period_started_at, :datetime
    change_column :billable_services, :billing_period_ended_at, :datetime
    change_column :btns, :activated_at, :datetime
    change_column :btns, :deactivated_at, :datetime
    change_column :phones, :activated_at, :datetime
    change_column :phones, :deactivated_at, :datetime
  end

  def down
    change_column :billable_services, :billing_period_started_at, :date
    change_column :billable_services, :billing_period_ended_at, :date
    change_column :btns, :activated_at, :date
    change_column :btns, :deactivated_at, :date
    change_column :phones, :activated_at, :date
    change_column :phones, :deactivated_at, :date
  end
end
