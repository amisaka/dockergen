class AddTollFreeBoolean < ActiveRecord::Migration[5.0]
  def change
    add_column :phone_service_types, :toll_free, :boolean, default: false, null: false

    pst = PhoneServiceType.where(code: "Toll Free Switched and Dedicated").take
    pst.toll_free = true
    pst.save!
  end
end
