class AddBeforeAfterBalanceColumns < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :before_balance, :decimal, null: true
    add_column :payments, :after_balance, :decimal, null: true
    add_column :invoices, :before_balance, :decimal, null: true
    add_column :invoices, :after_balance, :decimal, null: true
  end
end
