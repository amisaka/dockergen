class AddCheckNumberToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :check_number, :string
  end
end
