class AddProductOptionalToBillableServices < ActiveRecord::Migration[5.0]
  def change
    change_column :billable_services, :product_id, :integer, null: true
  end
end
