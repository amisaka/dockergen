class ChangeServiceToProductForBillableServices < ActiveRecord::Migration[5.0]
  def up
    execute "alter table billable_services rename column service_name to product_name;"
    execute "alter table billable_services rename column service_id to product_id;"
  end

  def down
    execute "alter table billable_services rename column product_name to service_name;"
    execute "alter table billable_services rename column product_id to service_id;"
  end
end
