class AddEffectiveDateUsageTypePidToRatePlan < ActiveRecord::Migration[5.0]
  def change
    add_column :rate_plans, :effective_date, :date
    add_column :rate_plans, :pid, :integer

    add_reference :rate_plans, :usage_type, :defaut => 1
  end
end
