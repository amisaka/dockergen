class CreateProductClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :product_classes do |t|
      t.integer :code, :null => false
      t.text :description, :null => false
      t.boolean :active, :null => false, :default => true
      t.integer :product_class_pid

      t.timestamps
    end

    add_index :product_classes, :code, :unique=>true
    add_index :product_classes, :product_class_pid, :unique=>true
  end
end
