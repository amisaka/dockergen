class RenameServicesToProducts < ActiveRecord::Migration[5.0]
  def change
    rename_table :services, :products
  end
end
