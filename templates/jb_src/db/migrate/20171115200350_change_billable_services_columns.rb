class ChangeBillableServicesColumns < ActiveRecord::Migration[5.0]
  def up
    change_table :billable_services do |t|
      t.rename :service_start, :billing_period_started_at
      t.rename :service_finish, :billing_period_ended_at
      t.integer :billing_period, null: false, default: 1
      t.remove :renewal_id
      t.remove :renewable
      t.remove :frequency_months
      t.remove :active_month
    end
  end

  def down
    change_table :billable_services do |t|
      t.rename :billing_period_started_at, :service_start
      t.rename :billing_period_ended_at, :service_finish
      t.remove :billing_period
      t.integer :renewal_id
      t.boolean :renewable
      t.integer :frequency_months
      t.boolean :active_month
    end
  end
end
