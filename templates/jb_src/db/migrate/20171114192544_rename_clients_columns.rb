class RenameClientsColumns < ActiveRecord::Migration[5.0]
  def change
    change_table :clients do |t|
      t.rename :started_at, :activated_at
      t.rename :terminated_at, :deactivated_at
      t.boolean :activated, default: true, null: false
    end
  end
end
