class AddBillingAddressStreet2ToCrmAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :crm_accounts, :billing_address_street_2, :string
  end
end
