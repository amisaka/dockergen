class DropInvoiceRuns < ActiveRecord::Migration[5.0]
  def up
    drop_table :invoice_runs
  end

  def down
    create_table "invoice_runs", force: :cascade do |t|
      t.integer  "invoice_year"
      t.integer  "invoice_month"
      t.text     "processed_up_to_en"
      t.text     "payments_due_at_en"
      t.text     "seasonal_message_en"
      t.text     "processed_up_to_fr"
      t.text     "payments_due_at_fr"
      t.text     "seasonal_message_fr"
      t.datetime "invoice_run_date"
      t.datetime "created_at",          null: false
      t.datetime "updated_at",          null: false
      t.integer  "previous_run_id"
      t.date     "charges_start"
      t.date     "charges_finish"
    end
  end
end
