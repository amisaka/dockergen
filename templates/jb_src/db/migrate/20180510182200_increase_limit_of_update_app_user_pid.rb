class IncreaseLimitOfUpdateAppUserPid < ActiveRecord::Migration[5.0]
  def change
    change_column :services, :update_app_user_pid, :integer, limit: 5
  end
end
