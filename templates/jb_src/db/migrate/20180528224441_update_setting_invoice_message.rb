class UpdateSettingInvoiceMessage < ActiveRecord::Migration[5.0]
  def up
    s = Setting.find_by_name("invoice_message")
    s.value = "<p><strong><span style=\"font-size: 14pt; color: #ff0000;\">REDONDANCY SERVICE</span></strong><br /><span style=\"font-size: 14pt;\">No more outage. Enjoy a redundancy LTE Internet connection</span><br /><span style=\"font-size: 14pt;\">from $ 9.95 / month.</span><br /><span style=\"font-size: 14pt;\">Book yours today: 514‐444‐4742 ext. 3500</span></p>"
    s.save!
  end

  def down
    true
  end
end
