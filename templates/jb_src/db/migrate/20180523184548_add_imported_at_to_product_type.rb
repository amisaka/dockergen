class AddImportedAtToProductType < ActiveRecord::Migration[5.0]
  def change
    add_column :product_types, :imported_at, :datetime
  end
end
