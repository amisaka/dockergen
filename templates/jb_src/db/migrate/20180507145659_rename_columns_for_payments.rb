class RenameColumnsForPayments < ActiveRecord::Migration[5.0]
  def change
    rename_column :payments, :transaction_types, :transaction_type_id
    rename_column :payments, :transaction_method, :transaction_method_id
  end
end
