class AddPhoneTypeToPhoneContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :phone_contacts, :phone_type, :string
  end
end
