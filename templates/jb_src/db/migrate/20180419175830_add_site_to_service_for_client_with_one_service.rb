class AddSiteToServiceForClientWithOneService < ActiveRecord::Migration[5.0]
  include ::Montbeau::Helpers

  def up
    total = Client.count
    summary = {clientsUpdated: 0, servicesUpdated: 0}
    Client.all.each_with_index { |c,i|
        printProgress("Updating sites of services for clients with 1 site...", i, total, true)
        if c.sites.count == 1;
            summary[:clientsUpdated] += 1
            c.services.each {|s|
                summary[:servicesUpdated] += 1
                s.site = c.sites[0]
                s.save!
            }
        end
    }

    puts "Summary results: #{summary}"
  end

  def down
    Service.update_all(site_id: nil)
  end
end
