class AddEmailTypeToEmailContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :email_contacts, :email_type, :string
  end
end
