class AddColumnsToRatePlan < ActiveRecord::Migration[5.0]
  def change
    add_column :rate_plans, :active, :boolean
    add_column :rate_plans, :owner_app_user_pid, :integer
    add_column :rate_plans, :owner_team_pid, :integer
    add_column :rate_plans, :insert_app_user_pid, :integer
    add_column :rate_plans, :update_app_user_pid, :integer
    add_column :rate_plans, :bc_rate_plan_pid, :integer

    rename_column :rate_plans, :name, :code
    rename_column :rate_plans, :pid, :rate_plan_pid
  end
end
