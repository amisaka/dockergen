class AddProrateToBillableService < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :prorate_id, :integer
  end
end
