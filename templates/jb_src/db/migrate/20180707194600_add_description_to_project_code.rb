class AddDescriptionToProjectCode < ActiveRecord::Migration[5.0]
  def change
    add_column :project_codes, :description, :text
  end
end
