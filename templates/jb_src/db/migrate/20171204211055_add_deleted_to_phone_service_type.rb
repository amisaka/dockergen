class AddDeletedToPhoneServiceType < ActiveRecord::Migration[5.0]
  def change
    add_column :phone_service_types, :deleted, :boolean, default: false, null: false
    add_column :phone_service_types, :deleted_at, :datetime
  end
end
