class DummyRatePlanPid244and245 < ActiveRecord::Migration[5.0]
  def change
    [244, 245].each {|pid|
        name="Missing rate plan PID #{pid}"
        rp = RatePlan.new(pid: pid, name: name, description: name, usage_type_id: 1, effective_date: Date.new(2018, 5, 10))
        rp.save!
    }
  end
end
