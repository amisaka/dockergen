class AddRecurrenceTypeToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :recurrence_type, :integer
  end
end
