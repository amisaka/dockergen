class CreateProductTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :product_types do |t|
      t.text :name, :null => false
      t.integer :product_type_id

      t.timestamps
    end

    add_index :product_types, :name, :unique => true
    add_index :product_types, :product_type_id

  end
end
