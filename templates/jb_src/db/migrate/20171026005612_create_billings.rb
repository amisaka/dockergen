class CreateBillings < ActiveRecord::Migration[5.0]
  def change
    create_table :billings do |t|
      t.integer :client_id
      t.decimal :balance
      t.datetime :start_date
      t.datetime :billing_start_date
      t.datetime :last_usage_date
      t.datetime :last_statement_date
      t.decimal :last_statement_amount
      t.decimal :last_payment_amount
      t.datetime :last_payment_date
      t.datetime :terminated_on_date
      t.integer :leg_pid
    end
  end
end
