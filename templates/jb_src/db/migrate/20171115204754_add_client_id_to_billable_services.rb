class AddClientIdToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :client_id, :integer
  end
end
