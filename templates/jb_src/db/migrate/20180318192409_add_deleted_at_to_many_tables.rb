class AddDeletedAtToManyTables < ActiveRecord::Migration[5.0]
  def change
    [:product_categories, :product_classes, :product_types, :gl_accounts].each do |table|
        add_column table, :deleted, :boolean
    end
  end
end
