class RenameProrateInBillableServices < ActiveRecord::Migration[5.0]
  def change
    rename_column :billable_services, :prorate_id, :prorate_id_start
    add_column :billable_services, :prorate_id_end, :integer
  end
end
