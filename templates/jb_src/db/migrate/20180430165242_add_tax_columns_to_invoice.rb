class AddTaxColumnsToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :bc_pst, :decimal
    add_column :invoices, :sask_pst, :decimal
    add_column :invoices, :qst, :decimal
    add_column :invoices, :manitoba_rst, :decimal
    add_column :invoices, :gst, :decimal
    add_column :invoices, :hst, :decimal
    add_column :invoices, :tax_total, :decimal
  end
end
