class CreateProjectCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :project_codes do |t|
      t.references :client, foreign_key: true, null: false
      t.integer :project_pid
      t.integer :code
      t.boolean :active, null: false, default: true
      t.integer :customer_pid
      t.integer :department_pid
      t.references :site, foreign_key: true
      t.references :service, foreign_key: true
      t.integer :assignment_level_pid
      t.integer :owner_app_user_pid
      t.integer :insert_app_user_pid
      t.integer :update_app_user_pid
      t.datetime :imported_at

      t.timestamps
    end
  end
end
