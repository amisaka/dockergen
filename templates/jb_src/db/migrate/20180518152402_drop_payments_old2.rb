class DropPaymentsOld2 < ActiveRecord::Migration[5.0]
  def change
    drop_table :payments_old2, if_exists: true
  end
end
