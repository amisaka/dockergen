class ReplaceLineOfBuisinessIntegerWithReference < ActiveRecord::Migration[5.0]
  def change
    remove_column :phones, :line_of_business_id
    add_reference :phones, :line_of_businesses, index: true
  end
end
