class AddDeletedToUsageType < ActiveRecord::Migration[5.0]
  def change
    add_column :usage_types, :deleted, :boolean, default: false, null: false
  end
end
