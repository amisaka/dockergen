class ChangeCasingDIDSupplierPid < ActiveRecord::Migration[5.0]
  def change
    rename_column :did_suppliers, :Pid, :pid
  end
end
