class ChangeInvoices < ActiveRecord::Migration[5.0]
  def up
    drop_table :invoices

    create_table :invoices do |t|
      t.integer :client_id

      t.attachment :document

      t.decimal :amount
      t.decimal :total

      t.string :tax_province
      t.string :tax_country

      t.timestamps null: false
    end
  end

  def down
    drop_table :invoices

    create_table "invoices", force: :cascade do |t|
      t.integer  "client_id"
      t.date     "invoice_date"
      t.date     "service_start"
      t.date     "service_finish"
      t.decimal  "last_amount_invoiced", precision: 11, scale: 3
      t.decimal  "last_amount_paid",     precision: 11, scale: 3
      t.decimal  "amount_invoiced",      precision: 11, scale: 3
      t.decimal  "pstbc",                precision: 11, scale: 3
      t.decimal  "pstalb",               precision: 11, scale: 3
      t.decimal  "pstsask",              precision: 11, scale: 3
      t.decimal  "hst_tvh",              precision: 11, scale: 3
      t.decimal  "gst_tps",              precision: 11, scale: 3
      t.decimal  "tvq",                  precision: 11, scale: 3
      t.text     "invoicenumber"
      t.text     "invoice_path"
      t.datetime "created_at",                                                              null: false
      t.datetime "updated_at",                                                              null: false
      t.integer  "btn_id"
      t.integer  "invoice_run_id"
      t.decimal  "pstman",               precision: 11, scale: 3
      t.decimal  "call_pstbc",           precision: 11, scale: 3
      t.decimal  "call_pstalb",          precision: 11, scale: 3
      t.decimal  "call_pstsask",         precision: 11, scale: 3
      t.decimal  "call_pstman",          precision: 11, scale: 3
      t.decimal  "call_tvq",             precision: 11, scale: 3
      t.decimal  "call_gst_tps",         precision: 11, scale: 3
      t.decimal  "call_hst_tvh",         precision: 11, scale: 3
      t.date     "charges_start"
      t.date     "charges_finish"
      t.decimal  "services_subtotal",    precision: 11, scale: 3
      t.decimal  "charges_subtotal",     precision: 11, scale: 3
      t.decimal  "late_charges",         precision: 11, scale: 3
      t.text     "type",                                          default: "SimpleInvoice"
      t.integer  "site_id"
      t.integer  "summary_invoice_id"
    end
  end
end
