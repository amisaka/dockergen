class RenameSitesColumns < ActiveRecord::Migration[5.0]
  def change
    change_table :sites do |t|
      t.rename :activation_date, :activated_at
      t.rename :termination_date, :deactivated_at
      t.boolean :activated, default: true, null: false
    end

    change_column_default :sites, :activated_at, from: Time.now, to: nil
  end
end
