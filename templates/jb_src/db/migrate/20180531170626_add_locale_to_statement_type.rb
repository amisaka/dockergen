class AddLocaleToStatementType < ActiveRecord::Migration[5.0]
  def change
    add_column :statement_types, :locale, :string

	langSet = {"french"           => "fr-CA",
        "english"       => "en",
        "coop"          => "fr-CA",
        "commercant"    => "fr-CA",
    }

    langSet.each {|substring, locale|
        StatementType.where('name ilike ?', "%#{substring}%").each {|st|
            st.locale = locale
            st.save!
        }
    }

  end
end
