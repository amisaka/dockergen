class AddTaxOnCallsColumnsToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :bc_pst_calls, :decimal
    add_column :invoices, :sask_pst_calls, :decimal
    add_column :invoices, :qst_calls, :decimal
    add_column :invoices, :manitoba_rst_calls, :decimal
    add_column :invoices, :hst_calls, :decimal
    add_column :invoices, :gst_calls, :decimal
  end
end
