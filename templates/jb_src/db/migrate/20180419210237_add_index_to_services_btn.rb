class AddIndexToServicesBtn < ActiveRecord::Migration[5.0]
  def change
    add_index :services, :btn_id
  end
end
