class AddProductCategoryToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :product_category_id, :integer
  end
end
