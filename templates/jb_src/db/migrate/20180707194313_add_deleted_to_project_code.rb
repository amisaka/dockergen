class AddDeletedToProjectCode < ActiveRecord::Migration[5.0]
  def change
    add_column :project_codes, :deleted, :boolean, null:false, default: false
    add_column :project_codes, :deleted_at, :datetime
  end
end
