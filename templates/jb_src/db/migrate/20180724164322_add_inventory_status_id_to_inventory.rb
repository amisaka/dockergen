class AddInventoryStatusIdToInventory < ActiveRecord::Migration[5.0]
  def change
    add_column :inventories, :inventory_status_id, :integer
  end
end
