class CorrectColumnNameTypoInProducts < ActiveRecord::Migration[5.0]
  def change
    rename_column :products, :multi_jurisdictional_texting, :multi_jurisdictional_taxing
  end
end
