class LocalizeSettingForInvoiceMessage < ActiveRecord::Migration[5.0]
  def up
    s = Setting.find_by_name('invoice_message');

    if s.present?
        s.name = 'invoice_message_en'
        s.save!
    else
        s = Setting.first
    end


    fr = s.dup
    fr.name = 'invoice_message_fr-CA'
    fr.value = "<p><strong><span style=\"font-size: 14pt; color: #ff0000;\">SERVICE DE REDONDANCE</span></strong><br /><span style=\"font-size: 14pt;\">Fini les pannes! Profitez d'une connexion de redondance Internet sans fil LTE à compter de 9.95 $/ mois.</span><br /><span style=\"font-size: 14pt;\">Réservez la vôtre dès maintenant: 514‐444‐4742 poste 3500</span></p>"
    fr.save!

  end

  def down
    s = Setting.find_by_name('invoice_message_en');
    s.name = 'invoice_message'
    s.save!
    Setting.where('id > 1').destroy_all
  end
end
