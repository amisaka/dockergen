class NewSettingForInvoices < ActiveRecord::Migration[5.0]
  def up
    pu = Setting.new(name: 'payments_until', value: Date.new(2018, 05, 29), setting_type: "date" )
    pu.save!
    dueDate = Setting.new(name: 'late_payment_deadline', value: Date.new(2018, 06, 27), setting_type: "date" )
    dueDate.save!
  end

  def down
    Setting.find_by_name('payments_until').try(:destroy)
    Setting.find_by_name('due_date').try(:destroy)
    Setting.find_by_name('late_payment_deadline').try(:destroy)

  end
end
