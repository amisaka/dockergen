class AddActivatedToClientLineOfBusinesses < ActiveRecord::Migration[5.0]
  def up
    add_column :client_line_of_businesses, :activated, :boolean, default: true, null: false
    add_column :client_line_of_businesses, :activated_at, :datetime
    add_column :client_line_of_businesses, :deactivated_at, :datetime
  end
end
