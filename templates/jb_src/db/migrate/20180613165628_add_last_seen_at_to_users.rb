class AddLastSeenAtToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :last_seen_at, :datetime
    add_index  :users, :username
    add_index  :users, :email
  end
end
