class AddNewFieldsToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_reference :billable_services, :supplier, foreign_key: { to_table: :did_suppliers}, comment: %q[Was phohne.did_supplier_id]
    add_column :billable_services, :password1, :text
    add_column :billable_services, :supplier_account_no, :text

  end
end
