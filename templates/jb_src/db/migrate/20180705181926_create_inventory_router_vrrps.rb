class CreateInventoryRouterVrrps < ActiveRecord::Migration[5.0]
  def change
    create_table :inventory_router_vrrps do |t|
      t.string :machine_name
      t.string :user_name
      t.boolean :deleted
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
