class CreateUsageTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :usage_types do |t|
      t.text :name
      t.integer :usageTypePid

      t.timestamps
    end
  end
end
