class ChangeContacts < ActiveRecord::Migration[5.0]
  def up
    rename_column :contacts, :active, :activated
    add_column :contacts, :activated_at, :datetime
    add_column :contacts, :deactivated_at, :datetime

    create_table :phone_contacts do |t|
      t.integer :client_id

      t.string :phone_number
      t.string :name

      t.boolean :activated, null: false, default: true
      t.datetime :activated_at
      t.datetime :deactivated_at

      t.boolean :deleted, default: false
      t.datetime :deleted_at

      t.timestamps null: false
    end

    create_table :email_contacts do |t|
      t.integer :client_id

      t.string :email
      t.string :name

      t.boolean :activated, null: false, default: true
      t.datetime :activated_at
      t.datetime :deactivated_at

      t.boolean :deleted, default: false
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end

  def down
    rename_column :contacts, :activated, :active
    remove_column :contacts, :activated_at
    remove_column :contacts, :deactivated_at

    drop_table :email_contacts
    drop_table :phone_contacts
  end
end
