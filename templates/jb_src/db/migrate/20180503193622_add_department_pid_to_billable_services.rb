class AddDepartmentPidToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :department_pid, :integer
  end
end
