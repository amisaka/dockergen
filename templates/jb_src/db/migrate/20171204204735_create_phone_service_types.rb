class CreatePhoneServiceTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :phone_service_types do |t|
      t.string :code
      t.string :description
      t.boolean :active, default: true, null: false

      t.timestamps
    end
  end
end
