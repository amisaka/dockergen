class CreateTransactionTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_types do |t|
      t.text :typename

      t.timestamps
    end
  end
end
