class MoveSiteColumnFromTaxTableToInvoice < ActiveRecord::Migration[5.0]
  def change
    remove_column :tax_amounts, :site_id
    add_reference :invoices, :site, index: true, null: true
    add_index     :invoices, [:client_id, :site_id]
  end
end
