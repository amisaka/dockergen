class CreateTransactionMethods < ActiveRecord::Migration[5.0]
  def change
    create_table :transaction_methods do |t|
      t.text :typename
      t.integer :typeid
      t.text :description

      t.timestamps
    end
  end
end
