class ChangeBtnInvoicingEnabledToTrue < ActiveRecord::Migration[5.0]
  def change
    change_column_default :btns, :invoicing_enabled, true
  end
end
