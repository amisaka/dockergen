class AddForeignKeyBetweenPhoneAndLineOfBuisiness < ActiveRecord::Migration[5.0]
  def change

    add_foreign_key :phones, :line_of_businesses, column: 'line_of_businesses_id'
  end
end
