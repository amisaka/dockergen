class AddDefaultToBillingBalance < ActiveRecord::Migration[5.0]
  def change
    change_column_default :billings, :balance, from: nil, to: 0

    puts "Total billings #{Billing.count}"
    puts "Total null billings after change: #{Billing.where('balance is null').size}"
    Billing.where('balance is null').each {|b| b.balance = 0; b.save! ;}
    puts "Total null billings before change: #{Billing.where('balance is null').size}"
  end


end
