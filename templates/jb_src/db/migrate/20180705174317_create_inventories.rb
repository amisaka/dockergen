class CreateInventories < ActiveRecord::Migration[5.0]
  def change
    create_table :inventories do |t|
      t.string :address1
      t.string :adresse_ip_lan_data
      t.string :adresse_ip_lan_voip
      t.string :adresse_ip_link_data
      t.string :adresse_ip_link_voip
      t.string :adresse_ipv6_lan_data
      t.string :adresse_ipv6_lan_voip
      t.string :adresse_ipv6_link_data
      t.string :adresse_ipv6_link_voip
      t.string :agreement_num
      t.string :auto_renewal_term
      t.string :billingtelephonenumber
      t.string :btn
      t.string :circuit_num_logique
      t.string :circuit_num_physique
      t.string :circuit_num
      t.string :city
      t.string :compte_fttb
      t.string :customer_name
      t.string :customer_sn
      t.datetime :deleted_at
      t.boolean :deleted
      t.string :description
      t.string :devicenamemonitored
      t.string :device
      t.integer :did_supplier_id
      t.string :download_speed
      t.string :email_of_courrier
      t.date :expiry_date
      t.string :installed_on_line
      t.string :inventory_type
      t.integer :inventory_type_id
      t.string :mac_address
      t.string :numerodereference
      t.string :parent_agreement_num
      t.string :password
      t.integer :product_id
      t.string :product_name
      t.string :quantity
      t.string :realsyncspeeddownload
      t.string :realsyncspeedupload
      t.string :router_principal
      t.string :router_vrrp
      t.string :service_id
      t.integer :site_id
      t.string :site_name
      t.string :site_number
      t.string :sous_interface_data
      t.string :sous_interface_voip
      t.string :sous_interface_voip_vrrp
      t.date :start_date
      t.string :supplier_num
      t.string :supplier
      t.string :terminate_on
      t.string :term
      t.string :upload_speed
      t.string :username
      t.string :vlan_data_q_in_q
      t.string :vlan_supplier
      t.string :vlan_voip_q_in_q
      t.string :vl_videotron
      t.string :vrrp_id
      t.string :z_end

      t.timestamps
    end
  end
end
