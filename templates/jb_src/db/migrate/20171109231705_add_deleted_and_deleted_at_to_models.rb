class AddDeletedAndDeletedAtToModels < ActiveRecord::Migration[5.0]
  def up
    ActiveRecord::Base.connection.tables.each do |t|
      add_column t, :deleted, :boolean, default: false, null: false unless column_exists?(t, :deleted)
      add_column t, :deleted_at, :datetime unless column_exists?(t, :deleted_at)
    end
  end

  def down
    ActiveRecord::Base.connection.tables.each do |t|
      remove_column t, :deleted if column_exists?(t, :deleted)
      remove_column t, :deleted_at if column_exists?(t, :deleted_at)
    end
  end
end
