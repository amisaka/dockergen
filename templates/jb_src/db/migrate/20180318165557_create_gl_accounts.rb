class CreateGlAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :gl_accounts do |t|
      t.text :code, :null=>false  #unique
      t.text :description, :null=>false
      t.boolean :active, :null => false, :default => true
      t.integer :gl_account_entry_type_pid
      t.integer :gl_account_pid # unique

      t.timestamps
    end

    add_index :gl_accounts, :code, :unique => true
    add_index :gl_accounts, :gl_account_pid, :unique => true


  end
end
