class AddEmailAddressPidToEmailContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :email_contacts, :email_address_pid, :integer
  end
end
