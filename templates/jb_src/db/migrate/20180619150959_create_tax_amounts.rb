class CreateTaxAmounts < ActiveRecord::Migration[5.0]
  def change
    create_table :tax_amounts do |t|
      t.references :invoice, foreign_key: true, null: false
      t.references :site, foreign_key: true, null: true
      t.integer :item_type, null: false
      t.integer :tax_type, null: false
      t.decimal :amount, null: false

      t.timestamps
    end


    add_index :tax_amounts, [:invoice_id, :site_id, :item_type, :tax_type], name: 'everything'
  end
end
