class AddReferenceNumberToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :reference_number, :string
  end
end
