class FixColumnTypeInInvoice < ActiveRecord::Migration[5.0]
  def change
    rename_column :invoices, :look_ahead_chages_begin, :look_ahead_charges_begin
  end
end
