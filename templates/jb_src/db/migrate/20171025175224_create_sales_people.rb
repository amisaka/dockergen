class CreateSalesPeople < ActiveRecord::Migration[5.0]
  def change
    create_table :sales_people do |t|
      t.string :name, null: false, default: ''

      t.timestamps
    end

    change_table :clients do |t|
      t.belongs_to :sales_person
    end
  end
end
