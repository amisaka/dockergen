class RenameBtnsColumns < ActiveRecord::Migration[5.0]
  def up
    remove_index :btns, column: [:billingtelephonenumber, :client_id, :end_date]

    change_table :btns do |t|
      t.rename :start_date, :activated_at
      t.rename :end_date, :deactivated_at
      t.boolean :activated, default: true, null: false
      t.index [:billingtelephonenumber, :client_id, :deactivated_at], name: "index_btns_on_btn_and_c_id_and_deact_at", unique: true, using: :btree
    end
  end

  def down
    remove_index :btns, column: [:billingtelephonenumber, :client_id, :deactivated_at]

    change_table :btns do |t|
      t.rename :activated_at, :start_date
      t.rename :deactivated_at, :end_date
      t.index [:billingtelephonenumber, :client_id, :end_date], name: "index_btns_on_billingtelephonenumber_and_client_id_and_end_date", unique: true, using: :btree
    end

    remove_column :btns, :activated
  end
end
