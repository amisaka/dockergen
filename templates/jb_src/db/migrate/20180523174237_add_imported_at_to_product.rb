class AddImportedAtToProduct < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :imported_at, :datetime
  end
end
