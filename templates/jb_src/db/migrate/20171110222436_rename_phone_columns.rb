class RenamePhoneColumns < ActiveRecord::Migration[5.0]
  def change
    change_table :phones do |t|
      t.rename :activation_date, :activated_at
      t.rename :termination_date, :deactivated_at
      t.boolean :activated, default: true, null: false
    end
  end
end
