class ChangePayments < ActiveRecord::Migration[5.0]
  def change
    rename_column :payments, :payment_date, :date
    rename_column :payments, :payment_type, :type
    rename_column :payments, :payment_info, :reason

    change_table :payments do |t|
      t.datetime :posted_date
      t.decimal  :balance
      t.string   :description
      t.boolean  :posted
      t.boolean  :imported
    end
  end
end
