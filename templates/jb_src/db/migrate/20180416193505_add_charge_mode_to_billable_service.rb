class AddChargeModeToBillableService < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :charge_mode_id, :integer
  end
end
