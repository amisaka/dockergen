class IntegerLimit < ActiveRecord::Migration[5.0]
  def change
    change_column :services, :default_origination, :text
    change_column :services, :default_destination, :text
  end
end
