class AddDeactivatedAtToInventory < ActiveRecord::Migration[5.0]
  def change
    add_column :inventories, :deactivated_at, :date
  end
end
