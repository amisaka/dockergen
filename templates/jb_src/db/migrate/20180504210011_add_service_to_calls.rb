class AddServiceToCalls < ActiveRecord::Migration[5.0]
  def change
    add_reference :calls, :service, index: true
    add_foreign_key :calls, :services
  end
end
