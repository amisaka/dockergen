class CreateLineOfBusiness < ActiveRecord::Migration[5.0]
  def change
    create_table :line_of_businesses do |t|
      t.string   :code
      t.string   :description

      t.boolean  :deleted, default: false, null: false
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
