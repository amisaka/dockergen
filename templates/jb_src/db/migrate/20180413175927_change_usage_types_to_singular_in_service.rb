class ChangeUsageTypesToSingularInService < ActiveRecord::Migration[5.0]
  def change
    remove_reference :services, :usage_types
    add_reference :services, :usage_type
  end
end
