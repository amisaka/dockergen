class RenameBillingsColumns < ActiveRecord::Migration[5.0]
  def change
    change_table :billings do |t|
      t.rename :start_date, :activated_at
      t.rename :terminated_on_date, :deactivated_at
      t.boolean :activated, default: true, null: false
    end
  end
end
