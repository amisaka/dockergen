class AddLookAheadMonthEnumToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :look_ahead_month, :integer
  end
end
