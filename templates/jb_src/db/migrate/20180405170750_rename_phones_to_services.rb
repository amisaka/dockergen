class RenamePhonesToServices < ActiveRecord::Migration[5.0]
  def change
    rename_table :phones, :services
  end
end
