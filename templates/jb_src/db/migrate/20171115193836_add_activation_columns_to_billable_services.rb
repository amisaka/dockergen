class AddActivationColumnsToBillableServices < ActiveRecord::Migration[5.0]
  def change
    change_table :billable_services do |t|
      t.datetime :activated_at
      t.datetime :deactivated_at
      t.boolean :activated, default: true, null: false
    end
  end
end
