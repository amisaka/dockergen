class AddDateColumnsToInvoice < ActiveRecord::Migration[5.0]
  def up
    add_column :invoices, :payments_shown_until, :date
    add_column :invoices, :late_payment_date, :date

    #Here we must set the late_payment dates and paymetns_shown until of all exising invoices
    #Set payments_shown_until where null to default value, that is, the 25th of the previous month from creation date
    sql = %Q[UPDATE invoices
        SET payments_shown_until  = make_date(date_part('YEAR', created_at)::integer, date_part('MONTH', created_at)::integer, 25) - interval '1 month'
        WHERE payments_shown_until is NULL
        ]
    Invoice.connection.execute(sql)

    #Set late_payment_date where null to default value, that is, the 29th of the  month of creation date, except for February
    sql = %Q[UPDATE invoices
        SET late_payment_date = make_date(date_part('YEAR', created_at)::integer, date_part('MONTH', created_at)::integer, 29)
        WHERE late_payment_date is NULL and date_part('MONTH', created_at)::integer != 2
        ]
    Invoice.connection.execute(sql)

    #Set late_payment_date where null to default value, that is, the 28th of the  month of creation date if creation date is in a February
    sql = %Q[UPDATE invoices
        SET late_payment_date = make_date(date_part('YEAR', created_at)::integer, date_part('MONTH', created_at)::integer, 28)
        WHERE late_payment_date is NULL and date_part('MONTH', created_at)::integer = 2
        ]
    Invoice.connection.execute(sql)

    change_column_null :invoices, :payments_shown_until, false
    change_column_null :invoices, :late_payment_date, false

  end

  def down
    remove_column :invoices, :payments_shown_until
    remove_column :invoices, :late_payment_date
  end
end
