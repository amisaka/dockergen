class AddChargesMonthDatesToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :usage_charges_begin, :date, comment: %q[The latest date we include in this invoice one-tiem charges for, usually  1st of a month as of July 2018]
    add_column :invoices, :usage_charges_end, :date, comment: %q[The earliest date we include in this invoice one-tiem charges for, usually  1st of a month as of July 2018]
    add_column :invoices, :look_ahead_chages_begin, :date, comment: %q[The earliest date we include monthly recurring charges for, usually  1st of a month as of July 2018]
    add_column :invoices, :look_ahead_charges_end, :date, comment: %q[The latest date we  include monthly recurring charges for, usualy last day of a month as of July 2018]

    remove_column :invoices, :look_ahead_month
  end
end
