class AddDeletedAtToCoverage < ActiveRecord::Migration[5.0]
  def change
    add_column :coverages, :deleted_at, :datetime
  end
end
