class ChangeDateDefaultOnPaymentDate < ActiveRecord::Migration[5.0]
  def up
    Payment.where(date: nil).each {|p| p.date = p.created_at; p.amount = 0; p.save! ; }
    change_column_null :payments, :date, false
    change_column_null :payments, :amount, false
  end

  def down
    nil
    change_column_null :payments, :date, true
    change_column_null :payments, :amount, true
  end
end
