class AddIndexToBillableService < ActiveRecord::Migration[5.0]
  def change
    add_index :billable_services, :client_id
    add_index(:billable_services, [:client_id, :activated, :recurrence_type, :deleted], name: 'index_on_billable_services_for_invoices')
  end
end
