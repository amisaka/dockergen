class AddDeletedStuffToTransactionTypeTransactionMethod < ActiveRecord::Migration[5.0]
  def change
    add_column :transaction_types, :deleted_at, :datetime
    add_column :transaction_types, :deleted, :boolean, default: false, null: false
    add_column :transaction_methods, :deleted_at, :datetime
    add_column :transaction_methods, :deleted, :boolean, default: false, null: false

  end
end
