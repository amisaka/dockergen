class AddForeignPidToSalesPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :sales_people, :foreign_pid, :integer
  end
end
