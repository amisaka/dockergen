class ChangeCalls < ActiveRecord::Migration[5.0]
  def down
    drop_table :calls

    create_table :calls do |t|
      t.string   "master_callid",          limit: 40
      t.string   "call_type",              limit: 16
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "startdatetime",                     default: '1970-01-01 00:00:00'
      t.datetime "answerdatetime",                    default: '1970-01-01 00:00:00'
      t.datetime "releasedatetime"
      t.integer  "datasource_id"
      t.string   "our_number"
      t.string   "their_number"
      t.boolean  "terminated"
      t.integer  "client_id"
      t.boolean  "transfertovoicemail"
      t.integer  "duration"
      t.integer  "viewable_legs"
      t.boolean  "no_extension_picked_up"
      t.boolean  "abandoned_call"
      t.string   "type",                              default: "Call"
    end
  end

  def up
    drop_table :calls

    create_table :calls do |t|
      # BTN and calling DID
      t.string :btn
      t.string :did
      t.string :called_number

      # Date
      t.datetime :call_date
      t.string :date
      t.string :time

      # Details
      t.string :area
      t.string :province

      t.string :duration
      t.string :cost
    end
  end
end
