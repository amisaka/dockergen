class RemovePidFromProductCategories < ActiveRecord::Migration[5.0]
  def change
    remove_column :product_categories, :Pid
  end
end
