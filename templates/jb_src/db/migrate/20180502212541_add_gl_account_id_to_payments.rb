class AddGlAccountIdToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :gl_account_id, :integer
  end
end
