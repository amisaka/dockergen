class AddUniqueNotNullConstraintToBilling < ActiveRecord::Migration[5.0]
  def change
    # Eliminate rows with duplicate client_id via Postgres SQL
    # those with highest last_statement_date being kept
    # See https://wiki.postgresql.org/wiki/Deleting_duplicates
    puts "Number of billings before delete: #{Billing.count}"
    returnValue = execute %q[
		DELETE
		FROM billings
		WHERE id IN
		    (SELECT id
		     FROM
		       (SELECT id,
		               ROW_NUMBER() OVER (PARTITION BY client_id
		                                  ORDER BY last_statement_date DESC) AS rnum
		        FROM billings) t
		     WHERE t.rnum > 1)
          ]
    puts "Number of billings after delete: #{Billing.count}"

    # Add a unique index to billings.client_id
    add_index :billings, :client_id, :unique => true

    # Add not null constraint to billings.client_id
    change_column_null :billings, :client_id, false
  end
end
