class RenameChargestaxColumnsForInvoices < ActiveRecord::Migration[5.0]
  def change
    rename_column :invoices, :bc_pst, :bc_pst_charges
    rename_column :invoices, :sask_pst, :sask_pst_charges
    rename_column :invoices, :qst, :qst_charges
    rename_column :invoices, :manitoba_rst, :manitoba_rst_charges
    rename_column :invoices, :hst, :hst_charges
    rename_column :invoices, :gst, :gst_charges
  end
end
