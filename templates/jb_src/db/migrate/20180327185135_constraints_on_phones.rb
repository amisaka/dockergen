class ConstraintsOnPhones < ActiveRecord::Migration[5.0]
  def change
    change_column_null :phones, :btn_id, false
    change_column_null :phones, :phone_number, false
  end
end
