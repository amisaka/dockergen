class AddMutipleColumnsToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :previous_balance, :decimal
    add_column :invoices, :payment_received, :decimal
    add_column :invoices, :late_fee_charges, :decimal
    add_column :invoices, :past_due_balance, :decimal
    add_column :invoices, :monthly_charges, :decimal
    add_column :invoices, :usage_charges_from, :datetime
    add_column :invoices, :usage_charges_to, :datetime
    add_column :invoices, :usage_charges, :decimal
    add_column :invoices, :total_current_charges, :decimal
    add_column :invoices, :due_immediately, :decimal
    add_column :invoices, :due_date, :date
    add_column :invoices, :total_due_by_amount, :decimal
    add_column :invoices, :total_due, :decimal
  end
end
