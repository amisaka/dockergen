class AddNoteToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :note, :text
  end
end
