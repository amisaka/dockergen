class CreatePaymentsInvoicesJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_join_table :payments, :invoices do |t|
        t.index :payment_id
        t.index :invoice_id
    end
  end
end
