class AddAssignmentlevelToBillableService < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :assignment_level, :integer
  end
end
