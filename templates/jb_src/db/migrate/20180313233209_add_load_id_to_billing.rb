class AddLoadIdToBilling < ActiveRecord::Migration[5.0]
  def change
    add_column :billings, :load_id, :integer, null: true
    add_index :billings, :load_id
  end
end
