class AddCoverageToClients < ActiveRecord::Migration[5.0]
  def change
    c = Coverage.find_by("name_french = ?", "Couverture Provinciale")
    add_reference :clients, :coverage, foreign_key: true, null: false, default: 1
  end
end
