class AddDefaultDestinationToPhones < ActiveRecord::Migration[5.0]
  def change
    add_column :phones, :defaultdestination, :string
  end
end
