class RenameProductTypeId < ActiveRecord::Migration[5.0]
  def change
    rename_column :product_types, :product_type_id, :product_type_pid
  end
end
