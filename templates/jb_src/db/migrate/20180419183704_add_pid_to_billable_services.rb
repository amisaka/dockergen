class AddPidToBillableServices < ActiveRecord::Migration[5.0]
  def change
    add_column :billable_services, :charge_pid, :integer
  end
end
