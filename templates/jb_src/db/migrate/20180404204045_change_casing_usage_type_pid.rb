class ChangeCasingUsageTypePid < ActiveRecord::Migration[5.0]
  def change
    rename_column :usage_types, :usageTypePid, :usage_type_pid
  end
end
