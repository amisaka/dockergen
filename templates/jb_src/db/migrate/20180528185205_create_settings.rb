class CreateSettings < ActiveRecord::Migration[5.0]
  def up
    create_table :settings do |t|
      t.text :name, null: false, comment: %Q[Name of the setting], unique: true
      t.text :value, comment: %Q[Value of the setting]

      t.text :setting_type, null: false, comment: %Q[Not sure what is right enumeration for this.  Valid choices are in Setting::TYPE_LIST]

      t.integer :max_length, default: 0

      t.boolean :deleted, null: false, default: false
      t.datetime :deleted_at, default: nil

      t.timestamps
    end

    s = Setting.new(:name => "invoice_message", :value => "", :setting_type=>"html")
    s.save!
  end

  def down
    drop_table :settings, if_exists: true
  end
end
