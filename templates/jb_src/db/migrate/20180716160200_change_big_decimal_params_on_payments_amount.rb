class ChangeBigDecimalParamsOnPaymentsAmount < ActiveRecord::Migration[5.0]
  def up
    change_column :payments, :amount, :decimal, precision: 15, scale: 3
  end

  def down
    nil
  end
end
