class AddSubscribedToEmailContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :email_contacts, :subscribed, :boolean
  end
end
