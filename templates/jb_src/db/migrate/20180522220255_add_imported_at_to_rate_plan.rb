class AddImportedAtToRatePlan < ActiveRecord::Migration[5.0]
  def change
    add_column :rate_plans, :imported_at, :datetime
  end
end
