class ChangeTransactionTypeTransactionMethodNames < ActiveRecord::Migration[5.0]
  def change
    rename_column :transaction_methods, :typename, :methodname
    rename_column :transaction_methods, :typeid, :transaction_type_id
  end
end
