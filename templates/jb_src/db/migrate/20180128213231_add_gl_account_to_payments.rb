class AddGlAccountToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :GLAccount, :integer
  end
end
