class ChangePhones < ActiveRecord::Migration[5.0]
  def change
    remove_column :phones, :line_of_business, :string
    remove_column :phones, :service_type, :string

    add_column :phones, :line_of_business_id, :integer
    add_column :phones, :department_pid, :integer
    add_column :phones, :phone_service_type_id, :integer
    add_column :phones, :description, :string
    add_column :phones, :display_text, :string
    add_column :phones, :couverture, :string
  end
end
