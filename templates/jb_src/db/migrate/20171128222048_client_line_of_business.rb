class ClientLineOfBusiness < ActiveRecord::Migration[5.0]
  def change
    create_table :client_line_of_businesses do |t|
      t.integer :client_id
      t.integer :line_of_business_id

      t.boolean  :deleted, default: false, null: false
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
