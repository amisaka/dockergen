class RemoveStatementTypeFromClients < ActiveRecord::Migration[5.0]
  def change
    remove_column :clients, :statement_type

    # Not used at all.  On 2018/06/20:
    #montbeau_dev=# select distinct statement_type from clients;
    # statement_type
    # ----------------
    ##
    # (1 ligne)

  end
end
