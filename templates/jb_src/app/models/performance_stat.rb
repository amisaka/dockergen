class PerformanceStat < ApplicationRecord
  belongs_to :client

  def self.measure(options)
    options.merge!({ type: 'MeasurePerformanceStat',
                     login: ENV['LOGNAME'],
                     version: $ReleaseNum })

    n = create(options)
    n.start_time = Time.now
    yield n
    n.end_time   = Time.now
    n.save!
    n
  end

  def duration
    end_time - start_time
  end

  def rate
    if work_count
      duration / work_count
    else
      nil
    end
  end

  def say_rate(units)
    sprintf("   %.3f %s/s", rate, units)
  end

  def set_work_count!(num)
    self.work_count = num
    save!
  end

end


class MeasurePerformanceStat < PerformanceStat
end
