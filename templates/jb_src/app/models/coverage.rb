class Coverage < ApplicationRecord

    has_many :client


#    def self.loadFromCSV(filePath)
#        require 'csv'
#
#        clientNotFound = clientSetCount = attemptCount = 0
#        notFoundBTNs = []
#
#        CSV.open(filePath, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
#            csv.each do |row|
#                attemptCount += 1
#                btnStr  = (row['BTN'] or '')
#                raise "Emtpy btnStr" if btnStr.empty?
#
#                btnObj  = Btn.find_by("billingtelephonenumber = ?", btnStr)
#                if btnObj.nil?
#                    notFoundBTNs.push(btnStr)
#                    next
#                end
#
#                client  = btnObj.client
#                if client.nil?
#                    clientNotFound += 1
#                    next
#                end
#
#                coverageStr = row['Coverage'] or ''
#                raise "Empty coverage" if coverageStr.empty?
#
#                coverageObj = Coverage.find(coverageStr.to_i)
#                raise "No coverage object found for btnStr #{btnStr}" if coverageObj.nil?
#
#                client.coverage = coverageObj
#                client.save!
#                clientSetCount += 1
#            end
#        end
#
#        results = {
#            "Client not found"  => clientNotFound,
#            "btn not found"     => notFoundBTNs.count,
#            "Coverage set"      => clientSetCount,
#            "Attempt count"     => attemptCount,
#        }
#
#        results.each {|label, count|
#            puts "#{label}\t#{count}"
#        }
#        puts ("Not found BTNs:" + notFoundBTNs.join(' ')) if notFoundBTNs.count > 0
#        nil
#    end


    IMPORT_PRIMARY_KEY = :pid
    IMPORT_MAPPING = {
        :name_french    => "NameFrench",
        :pid    => "PID",
    }
end
