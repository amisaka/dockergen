class Carrier < ApplicationRecord

  has_many :local_exchanges, :foreign_key => 'carrierplan_id'
  has_many :rate_plans

end
