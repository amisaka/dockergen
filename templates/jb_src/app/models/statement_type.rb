class StatementType < ApplicationRecord
  has_many :statement_type_clients
  has_many :clients, through: :statement_type_clients


  def self.loadFromCSV(filePath)
    require 'csv'

    notFoundBTNs = []
    rowsWithEmptyBtnStr = []
    csvHeader_statementType_lookup = StatementType.genStatementTypeLookup
    CSV.open(filePath, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|

           foundCount = notFoundCount = emptyBtnsCount = 0
        csv.each do |row|
            btnStr = row['BTN']

            if btnStr.nil?
                $stderr.puts "Empty btnStr"
                emptyBtnsCount += 1
                byebug
            end

            btn         = Btn.find_by("billingtelephonenumber = ?", btnStr)
            client  = btn.client if not btn.nil?

           if client.nil?
                notFoundCount += 1
                puts "NOT Found client with btn #{btnStr}"
                notFoundBTNs.push(btnStr)
                next
           else
                 foundCount += 1
                puts "Found #{client.name} with btn #{btnStr}"
                #raise "No client found for (#{clientName} - #{btn})"
           end

           csvHeader_statementType_lookup.each {|csvHeader, statementTypeObj|
                if row[csvHeader] == '1' and not client.nil?
                    if  not client.statement_types.include? (statementTypeObj)
                        client.statement_types << statementTypeObj
                    else
                        $stderr.puts %Q[Client #{client.name} (#{client.btn.billingtelephonenumber}) already has statement "#{statementTypeObj.name}"]
                    end

                end
            }



        end
        puts "Found: #{foundCount}; Not found: #{notFoundCount}; Total: #{foundCount + notFoundCount}; Empty BTNs: #{emptyBtnsCount}"
        puts ("Not found BTNs:" + notFoundBTNs.join(' ')) if notFoundBTNs.count > 0
    end

  end


  def self.genStatementTypeLookup

    expectedStatementTypeCount = 12

    # Make hash to match csv headers with statement type names
    csvHeader_StatementType_Lookup = {
            "DetailEnglish"     => "Customer Detail - English",
            "DetailFrench"      => "Customer Detail - French",
            "CustomerEnglish"   => "Customer - Project Code - English",
            "CustomerFrench"    => "Customer - Project Code - French",
            "DepartmentEnglish" => "Department - Project Code - English",
            "DepartmentFrench"  => "Department - Project Code - French",
            "Coop"              => "Customer detail Invoice - Coop",
            "Commercant"        => "Customer detail Invoice - Commercant",
            "Print"             => "Print",
            "Package"           => "Package",
       }

       # Change the lookup hash to store statement type objects, not names
       csvHeader_StatementType_Lookup.each {|k, v|
                st = StatementType.find_by("name = ?", v)

                if st.nil?
                    $stderr.puts "\nWARNING: statement type #{v} does not exist. Continue byebug and it will create. \n"
                    byebug
                    st = StatementType.create!({:name => v, :description => v})
                    byebug if st.nil?
                    nil
                end
            csvHeader_StatementType_Lookup[k] = st

       }

        numStatementTypes = StatementType.count
        puts "#{numStatementTypes} statementTypes"
        byebug if numStatementTypes != expectedStatementTypeCount
    return csvHeader_StatementType_Lookup
  end

end
