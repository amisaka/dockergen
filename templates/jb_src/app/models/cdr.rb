require 'cdr_columns'
require 'iconv'
require 'find'

class MissingRatePlan < Exception
  attr_accessor :rateplan_name

  def initialize(msg="Missing Rate plan", name = "name unknown")
    @rateplan_name = name
    super(msg)
  end
end

class MissingRate < Exception
  attr_accessor :rate_center

  def initialize(msg="Missing Rate ", rate_plan, rate_center)
    @rate_center = rate_center
    super(sprintf("%s[%u] %s %s", rate_plan, rate_plan.id, msg, rate_center.to_s))
  end
end

class Cdr < ApplicationRecord

  INTERNATIONAL_CALL_CATEGORY     = 'internat'.freeze
  INTERNATIONAL_NETWORK_CALL_TYPE = 'in'.freeze
  attr_accessor :price_calculated, :no_bill

  self.table_name = "cdrviewer_cdr"
  self.primary_key = 'id'

  belongs_to :callingphone, :class_name => 'Service' #Phone has been renamed to Service as per client lingo t13331
  belongs_to :calledphone,  :class_name => 'Service' #Phone has been renamed to Service as per client lingo t13331
  belongs_to :callingbtn,   :class_name => 'Btn'
  belongs_to :calledbtn,    :class_name => 'Btn'
  belongs_to :call
  belongs_to :invoice
  belongs_to :datasource
  belongs_to :called_rate_center, :class_name => 'RateCenter'

  @@debugnow = nil
  @@foobar = nil;   cattr_accessor :foobar
  @@counter = 0
  cattr_accessor :debugnow

  scope :byrecordid, -> { order(:recordid) }

  scope :today, lambda { |*args|
    start  = args.first   || Time.now.beginning_of_day
    finish = args.second  || start.end_of_day
    where("cdrviewer_cdr.startdatetime >= ? and cdrviewer_cdr.startdatetime <= ?", start, finish)
  }

  scope :oldest_first, -> { order("startdatetime ASC,  lineno ASC,  id ASC") }
  scope :newest_first, -> { order("startdatetime DESC, lineno DESC, id DESC")}

  scope :blank_remotecallids, lambda {
    where("remotecallid IS NULL OR remotecallid = ''")
  }

  # unanswered calls are never charged
  scope :tollcalls, lambda {
    where(networkcalltype: ['to', 'in']).where.not(answerdatetime: nil).where.not(callcategory: 'private')
  }

  scope :private_calls, lambda {
    where(callcategory: 'private')
  }

  scope :network_ascalltypes, lambda {
    where(:ascalltype => 'Network')
  }
  scope :terminated, lambda {
    where(:direction => 'Terminating')
  }
  scope :terminating, lambda {
    where(:direction => 'Terminating')
  }
  scope :startinglegs, lambda {
    blank_remotecallids.network_ascalltypes.terminated
  }

  scope :nonblank_remotecallids, lambda {
    where("remotecallid IS NOT NULL AND remotecallid != ''")
  }
  scope :private_ascalltypes, lambda {
    where(:ascalltype => 'Private')
  }
  scope :enterprise_ascalltypes, lambda {
    where(:ascalltype => 'Enterprise')
  }
  scope :group_ascalltypes, lambda {
    where(:ascalltype => 'Group')
  }
  scope :originating, lambda {
    where(:direction => 'Originating')
  }
  scope :handsetrings, lambda {
    nonblank_remotecallids.group_ascalltypes.originating
  }

  scope :international, -> {
    where("callcategory = ? OR networkcalltype = ?", INTERNATIONAL_CALL_CATEGORY, INTERNATIONAL_NETWORK_CALL_TYPE)
  }

  scope :legacy_did_supplier, -> {
    where(datasource: Datasource.where(did_supplier: DIDSupplier.where(voip: false)))
  }

  scope :did_supplier, ->(supplier) {
    where(datasource: Datasource.where(did_supplier: DIDSupplier.where(name: supplier)))
  }

  scope :voip_did_supplier, -> {
    where(datasource: Datasource.where(did_supplier: DIDSupplier.where(voip: true)))
  }

  # a handset ring leg is the leg where we ring a handset
  def handsetring?
    remotecallid and privatecalltype? and !terminating?
  end


  def ==(other_cdr)
    # NOTE: If you change the equality code, be sure to change the CDR equality
    # code in clientportal (Django frontend)
    return ( (self.startdatetime == other_cdr.startdatetime) and
             (self.releasedatetime == other_cdr.releasedatetime) and
             (self.callingnumber == other_cdr.callingnumber) and
             (self.callednumber == other_cdr.callednumber) )
  end

  # full recordid looks like:
  #   00011745020016365B3F1A20110919141031.9871-040000
  # most of which is zeros or date, so take:
  #               ^^^^^^^^^^         ^^^^^
  #   012345678901234567890123456789012345678901234567
  def shortname
    return "#{id}" if recordid.blank?
    recordid[12..21]+"."+recordid[31..35]
  end
  def to_label
    "cdr#{shortname}"
  end
  def name
    "cdr#{recordid}"
  end

  def areacode
    if self.called_rate_center.try(:country_code) == 1
      # +1514xxxyyyy
      return RateCenter.canonify_number(networktranslatednumber)[2..4]
    else
      return nil
    end
  end

  def servicename
    case
    when calledphone
      return sprintf("{->%s}", calledphone.name)

    when callingphone
      return sprintf("{<-%s}", callingphone.name)

    when calledbtn
      return sprintf("{=>+1%s}", calledbtn.billingtelephonenumber)

    when callingbtn
      return sprintf("{<=+1%s}", callingbtn.billingtelephonenumber)

    when !serviceprovider.blank?
      return serviceprovider

    else
      return "unknown-service-provider"
    end
  end

  def to_str
    serviceprovider_name = ""
    answerdatetime_name  = ""
    releasedatetime_name = ""
    usernumber_name      = ""
    groupnumber_name     = ""
    if answerdatetime
      answerdatetime_name = answerdatetime.to_s
    end
    if releasedatetime
      releasedatetime_name = releasedatetime.to_s
    end
    if usernumber
      usernumber_name = usernumber.to_s
    end

    if groupnumber
      groupnumber_name = groupnumber.to_s
    end

    sprintf("%u/%s %s [ %s - %s ] %s %s %s",
            call.try(:id) || 0,
            recordid[30..35],
            servicename,
            answerdatetime_name,
            releasedatetime_name,
            duration,
            usernumber_name,
            groupnumber_name)
  end

  def billingtelephonenumber
    case
    when originateportedflag
      return originatelrn
    when destinationportedflag
      return destinationlrn
    else
      return usernumber
    end
  end

  def terminating!
    self.direction = 'Terminating'
  end

  def terminating?
    'Terminating' == direction
  end

  def originating?
    'Originating' == direction
  end

  def huntgroup?
    'hunt-group' == redirectingreason
  end

  def otherparty
    case
    when terminating?
      return networktranslatednumber || callingnumber
    else
      return callednumber
    end
  end

  def client
    phone = calledphone || callingphone
    phone.btn.client if phone
  end

  def calledlabel
    if calledphone
      calledphone.to_label
    else
      callednumber
    end
  end

  def callinglabel
    if callingphone
      callingphone.to_label
    else
      networktranslatednumber || callingnumber
    end
  end

  def calledclient
    if calledphone
      calledphone.btn.client
    end
  end

  def callingclient
    if callingphone
      callingphone.btn.client
    end
  end

  def outgoing?
    !terminating?
  end

  def postredirection?
    answerindicator.try(:downcase) == 'yes-postredirection'
  end

  def callanswered?
    answerindicator.try(:downcase) == 'yes'
  end

  def callnotanswered?
    answerindicator.try(:downcase) == 'no'
  end

  # a call is abandoned if it is not answered, or if it is less than $TooShortDuration
  def abandoned?
    return true if callnotanswered?
    return true if duration < $TooShortDuration
    return false
  end

  def networkcalltype
    @networkcalltype ||= self['networkcalltype'].try(:downcase)
  end
  def networkcalltype=(x)
    self['networkcalltype'] = @networkcalltype = x
  end

  def international?
    INTERNATIONAL_CALL_CATEGORY == callcategory or INTERNATIONAL_NETWORK_CALL_TYPE == networkcalltype
  end

  def private?
    "private" == callcategory.try(:downcase)
  end

  def local_callcategory?
    "local" == callcategory
  end
  def tolled_callcategory?
    'tollcall' == callcategory
  end
  def tolled_category!
    self.callcategory = 'tollcall'
  end

  def lo_networkcalltype?
    "lo"   == networkcalltype
  end

  def ispl_networkcalltype?
    "ispl" == networkcalltype
  end

  def cwlo_networkcalltype?
    "cwlo" == networkcalltype
  end

  def tf_networkcalltype?
    "tf" == networkcalltype
  end

  def local?
    private? or
      local_callcategory? or
      lo_networkcalltype? or
      ispl_networkcalltype? or
      cwlo_networkcalltype? or
      tf_networkcalltype?
  end

  def self.purgedays(count)
    now = count.days.ago
    where('cdrviewer_cdr.startdatetime < ?', now).delete_all
  end

  def self.splitdate(datestring)
    # strip quotes if any
    ddt = datestring.strip.gsub("\"","")

    return nil if ddt.size < 14
    year  = ddt[0..3]
    month = ddt[4..5]
    day   = ddt[6..7]
    hour  = ddt[8..9]
    minute= ddt[10..11]
    second= ddt[12..13]
    return year, month, day, hour, minute, second
  end

  def self.cvtdate(datestring)
    return nil if datestring.blank?
    (year, month, day, hour, minute, second) = splitdate(datestring)
    #require 'byebug'; byebug
    return Time.utc( year, month, day, hour, minute, second )
  end

  def cvt_dates
    # convert the date/times we care about into datetime strings
    self.startdatetime  ||= Cdr.cvtdate(self.starttime.to_s)
    self.answerdatetime ||= Cdr.cvtdate(self.answertime.to_s)
    self.releasedatetime||= Cdr.cvtdate(self.releasetime.to_s)
  end

  def duration
    self[:duration] ||= calc_duration
  end

  def network_ascalltype?
    ascalltype.try(:downcase) == "network"
  end

  def private_ascalltype?
    ascalltype.try(:downcase) == "private"
  end

  def group_ascalltype?
    ascalltype.try(:downcase) == "group"
  end

  # a starting leg is the first leg that arrives from outside.
  # also route will have a SIP address vs "Group"
  def startingleg?
    remotecallid.blank? and network_ascalltype? and terminating?
  end

  # a handset ring leg is the leg where we ring a handset
  def handsetring?
    remotecallid and (group_ascalltype? || private_ascalltype?) and !terminating?
  end

  def calc_duration
    cvt_dates
    return nil unless releasedatetime and answerdatetime

    return (releasedatetime - answerdatetime).to_i
  end

  def calc_scope(linecount)
    #
    #   return indicator of Local, Canadian, American, Caribbean, or International call (or unknown)
    #   return one of LOC, NOR, USA, CAR, INT, FWD, INB, AUC, NAW, BUS, NOD, ABN, UNK
    #        LOC local
    #         NOR Cdn not-local  (NOR is Kilmists word for Canada I guess,
    #                                might as well use it for consistency)
    #         USA US
    #         CAR Caribbean
    #         INT International
    #         FWD Forwarded incoming call
    #         INB Inbound call (not billed)
    #         AUC audio conference (VIC = video conference)
    #         NAW no answer
    #         BUS busy
    #         NOD no destination (termination = 001 or 003)
    #         ABN abnormal termination (termination = 021, 031, 086, 111)
    #         UNK not classified (unknown)

    case
    when "001" == terminationcause || "003" == terminationcause
      'NOD'

    when "017" == terminationcause
      'BUS'

    when ( "021" == terminationcause || "031" == terminationcause ||
          "086"  == terminationcause || "111" == terminationcause )
      'ABN'

    when (answerindicator and "no" == answerindicator.try(:downcase))
      'NAW'

    when (terminating? and postredirection?)
      'FWD'

    when (terminating? and !postredirection?)
      'INB'

    when international?
      'INT'

    when local?
      'LOC'

    else
      # look up area code and get the scope from the DB
      return 'LOC' if callednumber.blank?
      npa = case
            when callednumber[0..1] == "+1"
              callednumber[2..4]
            when callednumber[0..0] == "1"
              callednumber[1..3]
            else
              callednumber[0..2]
            end

      # use filter rather than get to guard agains there being
      # more than one entry for areacode. (Caribbea 340 was duplicate)
      code = AreaCode.get_by_npa(npa)
      case code.try(:country)
      when 'Canada'
        'NOR'
      when 'USA'
        'USA'
      when 'Caribbean'
        'CAR'
      else
        'LOC'
      end
    end
  end

  def lookup_phones(times = nil)
    times[7] = Time.now if times

    lookup_calling_phone(lineno, times)
    times[12] = Time.now if times
    #
    # set derived fields based on called number
    #
    self.destinationnumber = RateCenter.canonify_number(networktranslatednumber || callednumber)
    lookup_called_phone(lineno, times)
    times[17] = Time.now if times
  end

  # this function looks up a billing telephone number from the SIP username
  # present in "number".
  # This involves looking in the phones table for the username, and then
  # mapping this to the right BTN.  The result is a BTN table id.
  #
  def calc_derived(linecount, times = nil)
    #        Figure out the values for:
    #       usagetype
    #       productid
    #        originateportedflag
    #        originatelrn
    #        destinationportedflag
    #        destinationnumber
    #        destinationlrn
    #        forwardedcallingnumber
    #        duration

    # set up defaults
    #
    self.usagetype = nil
    self.productid = nil
    self.originateportedflag = false
    self.originatelrn = nil
    self.destinationportedflag = false
    self.destinationnumber = nil
    self.destinationlrn = nil
    self.forwardedcallingnumber = nil

    self.duration = calc_duration
    #
    # calculate usage type - so complicated it has its own function
    #
    self.usagetype = calc_scope(linecount)

    #
    # set derived fields based on user number
    #
    self.productid = usernumber
    unless transfer_type.blank?
      self.transfer_type = transfer_type[0..19]
    end

    # XXX what is this about?
    case
    when 'VIC' == usagetype || 'AUC' == usagetype
      self.productid = self.instantconference_owner
    else
      self.productid = Service.vdidify(productid)
    end

    lookup_phones(times)

    # new records need to be billed, override to nil, in case db has default value.
    self.invoice = nil

    #
    # map dest number to a rate center and set the called_rate_center
    #
    # only process outgoing calls
    return incoming! unless outgoing?

    # only process calls that were typed as toll or local.
    return already_local! unless (toll? or in_toll? or
                                  self.networkcalltype.blank? )

    ntype = record_toll_type_change
    return ntype
  end

  # split out for testing and migrations
  def lookup_calling_phone(linecount, times = nil)

    times[8] = Time.now if times
    #
    # set derived fields based on calling number
    #
    if callingnumber && 'Unavailable' != callingnumber[0..10]

      phone = Service.lookup_vdid_record_for(callingnumber, startdatetime, "calling-number")
      if outgoing? and !phone
        # maybe, if it's an internal call we can find it by usernumber
        phone = Service.lookup_vdid_record_for(usernumber, startdatetime, "calling-usernumber")
      end
      unless phone and !productid.blank?
        phone = Service.lookup_vdid_record_for(productid, startdatetime, "calling-productid")
      end
      unless phone and !groupnumber.blank?
        phone = Service.lookup_vdid_record_for(groupnumber, startdatetime, "calling-groupnumber")
      end

      times[9] = Time.now if times
      unless phone
        # did not find the phone, see if we should autocreate.
        # we autocreate callingphones on outgoing calls, and
        # autocreate calledphones on incoming calls.
        if outgoing? && !$BtnOfLastResortId.blank?
          puts "Autocreating calling #{callingnumber} for #{recordid}:#{linecount}" if $AutoCreatePhoneDebug
          phone = Service.autocreate(callingnumber)
          phone.notes ||= ""
          phone.notes += "autocreated @#{DateTime.now} for #{recordid}:#{linecount}"
          phone.save!
        end
      end

      self.callingphone = phone

      # XXX mcr wonders why we have originatelrn/destinationportedflag!
      if callingphone && callingphone.btn
        self.callingbtn   = callingphone.btn
        #self.originatelrn = callingphone.btn.billingtelephonenumber
        #self.destinationportedflag = true
      end
      times[10] = Time.now if times
      # XXX also unclear why we do this.
      self.callingbtn ||= Btn.lookup_vdid(callingnumber)
    end
  end

  def lookup_called_phone(linecount, times = nil)
    if destinationnumber && ! destinationnumber.blank? && 'Unavailable' != destinationnumber[0..10]
      if ('INT' == usagetype) && ('11' == destinationnumber[0..1])
        self.destinationnumber[0..1] = ""
      end

      phone = Service.lookup_vdid_record_for(destinationnumber, startdatetime, "called-destinationnumber")
      self.calledphone = phone

      if calledphone && calledphone.btn
        self.calledbtn      = calledphone.btn
        self.destinationlrn = calledphone.btn.billingtelephonenumber
        self.destinationportedflag = true
      end
      self.calledbtn  ||= Btn.lookup_vdid(destinationnumber, startdatetime)
    end
  end

  def utf8encoder
    @@utfencoder ||= Iconv.new('UTF-8','LATIN1')
  end

  def create_call_by_localcallid
    unless call
      self.call = Call.find_or_make(localcallid || recordid, datasource)
    end
  end

  def associate_by_localcallid(searchitem)
    return if searchitem.blank?

    if related_cdr = Cdr.find_by_localcallid(searchitem)
      self.call = related_cdr.call
    elsif related_cdr = Cdr.find_by_transfer_relatedcallid(searchitem)
      self.call = related_cdr.call
    end
  end

  def get_leg_with_call(associated_legs)
    associated_leg = nil

    associated_legs.each do |leg|
      unless leg.call.nil?
        associated_leg = leg
        break
      end
    end

    associated_leg
  end

  def callid_strip_text(callid)
    index = callid.index(/[A-Z]/)
    index = callid.length            if index.nil?

    callid[0, index]
  end

  def associate_by_localcallid_using_permutation(searchitem)

    return false if searchitem.blank?

    # If we have have two cdr whose localcallid are: 1254001443:0A and 1254001443:0,
    # even if they are not associated with each other (via remotecallid, etc.), we
    # can presume they are related.
    associated_leg_searchitem = callid_strip_text(searchitem)

    # Rather than doing very expensive LIKE query, checking for pattern that the
    # legs  are usually associated with, where we have xyzabcdef:0A and
    # xyzabcdef:0. As we get more data, this query might get more complex
    associated_legs = nearby_cdrs.where("localcallid =? OR localcallid=?", associated_leg_searchitem, associated_leg_searchitem+'A')

    associated_leg = get_leg_with_call(associated_legs)

    self.call = associated_leg.call if associated_leg
    return self.call
  end

  def associate_by_remotecallid_using_permutation(searchitem)
    return false if searchitem.blank?

    # This is a very odd and specialized case:  We have two legs
    # which are related but its not easy to associate them. The only
    # way we can associate is that the remotecallid are similar (e.g.
    # One has remotecallid that is: 1254001443:0 and 1254001443:0A).
    # We want to associate these two legs with each other even though
    # there is no relationship between them.
    # Scenario that produce this issue: We make an internal call,
    # get no response and connect to voice mail
    associated_leg_searchitem = callid_strip_text(searchitem)

    # Rather than doing very expensive LIKE query, checking for pattern that the
    # legs  are usually associated with, where we have xyzabcdef:0A and
    # xyzabcdef:0. As we get more data, this query might get more complex
    associated_legs = Cdr.where("remotecallid=? OR remotecallid=?", associated_leg_searchitem, associated_leg_searchitem+'A')

    associated_leg = get_leg_with_call(associated_legs)

    self.call = associated_leg.call if associated_leg
    return self.call
  end

  def merge_calls
    return false if remotecallid.blank?

    # Get similar remotecallid
    remote_cdr = Cdr.find_by_localcallid(remotecallid)
    return false if remote_cdr.blank?
    remote_callid = remote_cdr.call_id

    if remote_callid != self.call_id
      # Get the legs that are associated with a different call and
      # associate them with the current call
      diverged_cdrs = Cdr.find_all_by_call_id(remote_callid)
      diverged_cdrs.each do |leg|
        leg.call = self.call
        leg.save!
      end

      Call.delete(remote_callid)
    end

    return self.call
  end

  def find_client
    @@client_by_phone ||= Hash.new         # make a blank hash if we haven't already.
    client = @@client_by_phone[self.calledphone_id]
    return client if client

    # nope, try calling phone
    client = @@client_by_phone[self.callingphone_id]
    return client if client

    # nope, try the hard way.
    client = self.calledphone.try(:btn).try(:client)
    if client
      @@client_by_phone[self.calledphone_id] = client
      return client
    end

    client = self.callingphone.try(:btn).try(:client)
    if client
      @@client_by_phone[self.callingphone_id] = client
      return client
    end

    return nil
  end

  def merge_with(cdr, reason="")
    puts " calling #{reason} merge #{id} and #{cdr.id}" if @@debugnow
    self.call = self.call.merge_with(cdr.call)

    # generally, this only occurs when cdr.call was nil, because otherwise,
    # the merge routine updates call.cdrs.  Generally, this can only happen
    # when the cdr was loaded as a fixture, because otherwise, it would already
    # have a valid call record.
    unless cdr.call == self.call
      cdr.call.try(:delete)
      cdr.call = self.call
      cdr.save!
    end
  end

  def nearby_cdrs
    self.class.unscoped.where(["startdatetime BETWEEN ? and ?", self.startdatetime - 2.hours, self.startdatetime + 2.hours])
  end

  def merge_with_transfer_related
    unless transfer_relatedcallid.blank?
      nearby_cdrs.where(:localcallid => transfer_relatedcallid).find_each() { |cdr|
	merge_with(cdr, "transfer_related")
      }
    end
  end

  def merge_with_relatedcallid
    unless relatedcallid.blank?
      nearby_cdrs.where(:localcallid => relatedcallid).find_each() { |cdr|
	    merge_with(cdr, "relatedcallid")
      }
    end
  end

  # these are groups of tests, used below, and also in other places.
  # how to make this more DRY????
  def callforwardnoanswer?
    relatedcallidreason == 'Call Forward No Answer'
  end

  def transfertovoicemail?
    if calledphone.try(:voicemailportal?) or
      callednumber == '+12905550110'          # deal with old data.
      true
    else
      false
    end
  end
  scope :callforwardnoanswer,
    -> { where(:relatedcallidreason => 'Call Forward No Answer') }

  # this could be callforwardnoanswer.merge(:calledphone), but when this scope gets merged, that fails
  # due to a bug in rails <=3.2.
  #  see https://github.com/rails/rails/issues/3002
  #
  # have to resort to pure SQL, can not even do merge(Service.voicemailportal), because the context gets
  # confused when we ask for a series of calls relative to a phone itself.
  scope :transfertovoicemailportal, -> {
    callforwardnoanswer.joins("join phones AS portalphones on cdrviewer_cdr.calledphone_id = portalphones.id AND portalphones.voicemailportal = 't'")
  }

  # 1. look for a CDR record with the remotecallid identical to
  #    our localcallid.
  # 2. look for a CDR record with the localcallid identical to
  #    our remotecallid.
  # 3. if transfer_type == Deflection,
  #    look for a CDR record with a localcallid identical to our
  #    transefer_relatedcallid
  #
  # If found, merge call record of found record with our call record.
  #
  # We initialize our self.call to a new Call record to avoid the
  # corner case where we do not have one, but we don't save it.
  def associate_to_call

    # the call reference may be dangling, nuke it here.
    unless self.call.present?
      self.call = nil
    end

    # make sure we have a call object to play with, it will get saved
    # below, in associate_to_call_save! if it wasn't saved already.
    self.call ||= Call.new(:master_callid => localcallid)

    puts "\nProcessing #{localcallid} / #{id}" if @@debugnow

    # now look for remotecallid same as our localcallid.
    # XXX some consideration could be given to ONLY searching among the
    # same client's data.
    unless self.call.client
      self.call.client = find_client
      self.call.client.try(:save!)
    end

    # 1 our local => their remote.
    unless localcallid.blank?
      nearby_cdrs.where(:remotecallid => localcallid).find_each() { |cdr|
	merge_with(cdr, "remotecallid")
      }
    end

    # 2 our remote => their local.
    unless remotecallid.blank?
      nearby_cdrs.where(:localcallid => remotecallid).find_each() { |cdr|
	merge_with(cdr, "localcallid")
      }
    end

    # 3 their local => our transfer
    case
    when redirectingreason.blank? && originalcalledreason.blank? &&
        transfer_type.blank? && relatedcallidreason.blank?
      # no additional work for this type
      true

    when (transfer_type == 'Deflection' or redirectingreason == 'deflection')
      merge_with_transfer_related

    when transfer_type == 'Transfer Consult'
      merge_with_transfer_related
    when (transfer_type.blank? and redirectingreason == 'user-busy')
      merge_with_relatedcallid

    when (transfer_type.blank? and redirectingreason == 'hunt-group')
      merge_with_relatedcallid

    when (transfer_type.blank? and redirectingreason == 'no-answer' and
	  callforwardnoanswer? and !relatedcallid.blank?)
      # might do something here with voicemail recognition.
      merge_with_relatedcallid

    when (transfer_type.blank? and redirectingreason == 'no-answer' and
	  !relatedcallid.blank?)
      merge_with_relatedcallid

    when (transfer_type.blank? and redirectingreason == 'no-answer' and
	  relatedcallid.blank?)
      # do nothing for this leg

    when (transfer_type.blank? and redirectingreason == 'unconditional')
      merge_with_relatedcallid

    when (transfer_type.blank? and redirectingreason == 'call-center')
      # no additional work for this type
      true

    when relatedcallidreason == 'Call Forward Always'
      # test case "should associate a call-forward-always"
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Park Retrieve'
      # XXX Call Park Retrieve
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Park'
      # XXX Call Park
      merge_with_relatedcallid

    when relatedcallidreason == 'Directed Call Pickup'
      # XXX Directed Call Pickup
      merge_with_relatedcallid

    when relatedcallidreason == 'Sequential Ring'
      # XXX Sequential Ring
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Forward Not Reachable'
      # XXX Call Forward Not Reachable
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Center'
      # XXX Call Center
      merge_with_relatedcallid

    when relatedcallidreason == 'Hunt Group'
      # XXX Hunt Group
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Pickup'
      # XXX Call Pickup
      merge_with_relatedcallid

    when relatedcallidreason == 'BroadWorks Anywhere Location'
      # This condition will be entered when BroadWorks makes call.
      # For example:  call forward occurs
      merge_with_relatedcallid

    when relatedcallidreason == 'Simultaneous Ring Personal'
      # XXX Simultaneous Ring Personal
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Forward Selective'
      # XXX Call Forward Selective
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Forward No Answer'
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Forward Busy'
      # XXX  Call Forward Busy
      merge_with_relatedcallid

    when relatedcallidreason == 'Trunk Group Forward Unreachable'[0..19]
      # XXX Trunk Group Forward Unreachable
      merge_with_relatedcallid

    when relatedcallidreason == 'Fax Deposit'
      # XXX Fax Deposit
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Forward Selective'
      # XXX Call Forward Selective
      # options include redirectingreason == 'time-of-day'
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Pickup'
      # XXX Call Pickup
      merge_with_relatedcallid

    when relatedcallidreason == 'Call Retrieve'
      # XXX Call Retrieve
      merge_with_relatedcallid

    when relatedcallidreason == 'Barge-in'
      # https://code.credil.org/issues/10150
      merge_with_relatedcallid

    when redirectingreason =~ /BW-ExplicitID/
      # XXX
      # BW-ExplicitID.7402
      # BW-ExplicitID.7701
      # BW-ExplicitID.7201
      # BW-ExplicitID.1404
      # BW-ExplicitID.7302
      # BW-ExplicitID.2224
      # BW-ExplicitID.9001
      #create_call_by_localcallid
      true

    when redirectingreason =~ /BW-ImplicitID/
      # XXX
      create_call_by_localcallid

    when redirectingreason == 'time-of-day'
      # XXX time-of-day
      create_call_by_localcallid

    when redirectingreason == 'user-busy'
      # XXX user-busy
      create_call_by_localcallid

    when redirectingreason == 'unavailable'
      # XXX unavailable
      create_call_by_localcallid

    when redirectingreason == 'unconditional'
      # XXX unconditional
      create_call_by_localcallid

    when redirectingreason == 'unknown'
      # XXX unknown
      create_call_by_localcallid

    when redirectingreason == 'follow-me'
      # XXX follow-me
      create_call_by_localcallid

    when redirectingreason == 'hunt-group'
      # XXX follow-me
      create_call_by_localcallid

    when transfer_type == 'Trunk Group Forward Unconditional'[0..19]
      # test case: "should truncate transfer_type to 20 characters" do
      self.call = Call.find_or_make(transfer_relatedcallid, datasource)




    else
      raise "No action determined for record id #{recordid} based on redirectingreason, relatedcallidreason and / or transfer_type. It has relationships: transfer_type: '#{transfer_type}' relatedcallidreason: '#{relatedcallidreason}' originalcalledreason: '#{originalcalledreason}' redirectingreason: '#{redirectingreason}' relatedcallid: '#{relatedcallid}'"
    end

    # maybe save our self, and our call because below will reach back into database.
    # if it calls associate_to_call instead.
    #save!

    # 4 now look for situations where other record would have pointed to us
    #   (as a #3), but we didn't exist yet.
    nearby_cdrs.where(:transfer_relatedcallid => localcallid).find_each() { |cdr|
      # consider calling cdr.associate_to_call rather than duplicate logic in both directions
      merge_with(cdr, "reverse-transfer_relatedcallid")
    }

    # should probably make this a union so that we hit database once.
    nearby_cdrs.where(:relatedcallid => localcallid).find_each() { |cdr|
      # consider calling cdr.associate_to_call rather than duplicate logic in both directions
      merge_with(cdr, "reverse-relatedcallid")
    }

  end

  def associate_to_call_save!
    associate_to_call
    self.call.datasource = datasource
    self.call.save!
    calc_price!       if $CALC_PRICE_AT_CDRLOAD
    save!
  end

  # used by test cases only.
  def reload_associate_to_call_save!
    reload
    associate_to_call_save!
    self.call.try(:fixdates!)
  end

  def from_csv_columns(columns, linecount = 0)
    CdrColumns.each { |cdr_fieldname, cdr_colnum|
      thing = columns[cdr_colnum - 1]
      if thing
        value = thing.strip.gsub("\"","")
        write_attribute(cdr_fieldname, utf8encoder.iconv(value))
      end
    }

    newcategory = calc_relations!(linecount)
    return self,newcategory
  end

  def calc_relations!(linecount = 0)
    case calltype
    when 'Normal'
      cvt_dates

      # calculate the derived values
      newcategory = calc_derived(linecount)

      associate_to_call_save!
      self.call.try(:fixdates!)
      self

    when 'Long Duration'
      cvt_dates

      # calculate the derived values
      newcategory = calc_derived(linecount)

      # for now, we not attempt to associate Long Duration calls.
      self
    when 'Failover'
      nil
    else
      # this global is checked by unit test cases.
      $UNKNOWN_CALL_TYPE = true
      puts sprintf("(%02s)%05u: unknown calltype: %s\n", datasource.try(:datasource_filename), linecount, calltype)
      if Rails.env == 'development'
        require 'byebug'; byebug
      end
      puts  # empty line since status uses \r.
    end
    return newcategory
  end

  def toll!(reason = nil)
    self.billingreason = reason if reason
    self.networkcalltype = 'to'
  end
  def toll?
    self.networkcalltype == 'to'
  end

  def in_toll!(reason = nil)
    self.billingreason = reason if reason
    self.networkcalltype = 'in'
  end
  def in_toll?
    self.networkcalltype == 'in'
  end

  def local!(reason = nil)
    self.billingreason = reason if reason
    self.networkcalltype = 'lo'
  end

  def incoming!
    self.billingreason = 'incoming-call'
    nil
  end
  def already_local!
    self.billingreason = 'broadsoft-lo'
    nil
  end

  def internal!(reason = nil)
    # for now, just mark as lo, change this when legacy
    # billing system is no longer involved.
    self.billingreason = reason if reason
    self.networkcalltype = 'lo'
  end

  # returns nil if can not be calculated, and so, do not change
  # CDR record in anyway.
  # otherwise, also updates record itself.
  def detailed_toll_calculation(recode_local = false)

    unless recode_local
      # many ways to be local?, but canonicalize them...
      return local!('private')               if private?
      return local!('callcategory-local')    if local_callcategory?
      return local!('networkcalltype-lo')    if lo_networkcalltype?
      return local!('networkcalltype-ispl')  if ispl_networkcalltype?
      return local!('networkcalltype-cwlo')  if cwlo_networkcalltype?
      return local!('networkcalltype-tf')    if tf_networkcalltype?
    end

    # unanswered calls are not billable.
    return local!('callnotanswered')       if callnotanswered?

    if calledphone.try(:virtualdid)
      return internal!('internal-call')
    end

    calc_rate_center
  end

  def same_rate_center_call
    if callingphone.try(:find_rate_center)
      # return true if the rate centers match.
      callingphone.find_rate_center.match(called_rate_center)
    end
  end

  def calc_rate_center
    # start by looking up a matching rate center by callednumber.
    # this will process through NorthAmericanExchange if it
    # needs to in order to locate a detailed rate center, or
    # it may just return the matching rate center by country and city code.

    # if the call is outgoing, then use the destinationnumber.
    # if the call is incoming, then use the callingnumber
    if outgoing?
      rc = RateCenter.lookup_by_partial(self.destinationnumber)
    else
      rc = RateCenter.lookup_by_partial(self.callingnumber)
    end

    unless rc
      # if we can not find the rate center, then use the default
      rc = RateCenter.default_rate_center
    end
    unless rc
      # failsafe
      return local!('no-rate-center-found')
    end

    self.called_rate_center = rc
    if self.called_rate_center.default?
      return local!('default-rate-center')
    end

    #if same_rate_center_call
    #  return local!('same-rate-center-call')
    #end

    return local!('rate-center-toll-free')  if rc.tollfree?
    return in_toll!('international-call')   if (!rc.north_american_lookup? and rc.country_code != 1)

    if outgoing?
      # now find the client for this call, and determine the rate plan.
      btn = self.callingphone.try(:btn) || self.callingbtn
    else
      btn = self.calledphone.try(:btn) || self.calledbtn
    end

    unless btn
      return local!('invalid-btn')
    end

    return internal!('virtual-did') if rc.virtual_did?

    local = true

    if btn.local_plan
      local = btn.local_plan.acceptable_rate_center(rc)
    end

    return toll!('rate-plan-says-toll') unless local
    return local!('rate-plan-says-local')
  end

  def toll_edit_file(dir = nil)
    datasource.toll_edit_file(dir)
  end

  def write_edit_command(newcategory)
    datasource.write_edit_command(self, newcategory)
  end

  def record_toll_type_change(recode_local = false)
    orig_category = networkcalltype
    self.orig_networkcalltype ||= orig_category  # do not overwrite original if reprocessing
    newcategory = detailed_toll_calculation(recode_local)

    #byebug if newcategory.nil?
    #byebug if orig_category != newcategory

    # but do update the file if there is a change, because we need to see it again.
    # note: if orig_category was nil, then do not update.
    if newcategory and
      orig_category != newcategory and
      datasource.try(:did_supplier).try(:voip)
      datasource.increment_recalculate_count
      write_edit_command(newcategory)
    end

    newcategory
  end


  ### Broadworks import
  def self.import_row(columns, datasource, linecount = 0)
    # put the column values
    # into a dictionary 'row' with keys having usable "column" names

    c1 = Cdr.new
    c1.datasource = datasource
    c1.lineno = linecount
    return c1.from_csv_columns(columns, linecount)
  end

  def self.import_row_test(columns, datasource, linecount = 0)
    nc,category = import_row(columns, datasource, linecount)
    return nc
  end

  def self.load_bw_file(options)

    defaults = { overwrite: false,
                 verbose:   false,
                 archive:   false}
    options = defaults.merge!(options)

    filename  = options[:file]
    overwrite = options[:overwrite]
    verbose   = options[:verbose]
    archive   = options[:archive]
    outdir    = options[:outdir]

    basename = File.basename(filename, ".gz")
    datasource = Datasource.find_by_datasource_filename(basename)
    $0 = "import #{basename} / #{datasource.try(:id)}"

    if verbose && datasource
      puts "Duplicated source: #{filename}\n"
    end
    return :duplicate if(datasource && !overwrite)

    if(datasource)
      datasource.cdrs_delete_all
      datasource.calls_delete_all
    end

    datasource = Datasource.find_or_make(basename)
    datasource.did_supplier = DIDSupplier.find_by_symbol(:broadworks)
    handle = File.open(filename, "r:ISO-8859-1")
    if ( filename =~ /.*\.gz$/ )
      handle = Zlib::GzipReader.new(handle)
    end

    if outdir
      datasource.toll_edit_file(outdir)
    end

    $0 = "import #{basename} / #{datasource.try(:id)} started"
    number,durations,firstdate = load_bw_from_handle(basename, handle,
                                                     datasource,
                                                     verbose)
    if outdir
      datasource.close_edit_handle
    end
    datasource.save!

    if archive
      if number == 0
        # there was no data collected, so no clue when it was collected
        # probably an empty file, rename it to such
        dir = File.join(archive, "emptyfiles")
      else
        year  = sprintf("%04d", firstdate.year)
        month = sprintf("%02d", firstdate.month)
        dir=File.join(archive, year, month)
      end
      FileUtils.mkdir_p(dir)
      afile=File.join(dir, basename)
      where=FileUtils.pwd
      #print " ..#{where} rename #{filename} to #{afile}\n";
      FileUtils.rm(afile, :force => true)
      FileUtils.mv(filename, afile, :force => true)
    end
    return number,durations,firstdate,datasource
  end

  def unique_counter
    @@counter += 1
  end

  ### Troop import
  def cdr_from_troop_row(row, linecount)
    # This is not getting the relationship established between the two tables.
    # This needs to return the id for the btn table, it is not.
    btn = Btn.get_by_billingtelephonenumber(row[0])
    if btn
      self.callingbtn = btn
    end
    self.startdatetime = row[1] + " " + row[2]
    self.callingnumber = row[3]
    self.callednumber  = row[4]

    # Need to get this one working again.
    # This is 'StartDate + Duration = EndDate'
    datetime = Time.parse(row[1] + " " + row[2])
    #n1.? = datetime + row[5].to_i # What is the end date time field?
    self.duration = row[5]

    # This saves the data input above to the database.
    # Does Rails check for duplicates? Everytime I run this script, the data
    # that I generated previously gets overridden by the new input and the ids
    # bumped up but no other data is affected?

    self.calltype = 'Normal'
    self.call_cost = row[15].to_f
    self.localcallid = sprintf("troop-%u-%u", self.datasource.id, unique_counter)
    # calculate the derived values
    newcategory = self.calc_derived(linecount)

    self.call.try(:fixdates!)
    calc_price!

    save!

    return self
  end

  def self.import_cdr_troop_row(options, linecount, row, datasource)
    # This conditional catches the first row of the CDR TROOP .csv files. This is
    # because they all contain a first row of headers.
    return nil if linecount == 0

    n1 = Cdr.new
    n1.datasource = datasource
    n1.lineno = linecount
    n1.cdr_from_troop_row(row, linecount)
    return n1
  end

  def self.load_cdr_troop(options)

    defaults = { overwrite: false,
                 verbose:   false,
                 archive:   false}
    options = defaults.merge!(options)

    filename  = options[:file]
    overwrite = options[:overwrite]
    verbose   = options[:verbose]
    archive   = options[:archive]
    outdir    = options[:outdir]
    lineno = 0

    basename = File.basename(filename, ".gz")
    datasource = Datasource.find_by_datasource_filename(basename)
    $0 = "import #{basename} / #{datasource.try(:id)}"

    if verbose && datasource
      puts "Duplicated source: #{filename}\n"
    end
    return :duplicate if(datasource && !overwrite)

    # delete the old data before importing new data
    if(datasource)
      datasource.cdrs_delete_all
      datasource.calls_delete_all
    end

    datasource = Datasource.find_or_make(basename)
    datasource.did_supplier = DIDSupplier.find_by_symbol(:troop)
    handle = File.open(filename, "r:ISO-8859-1")
    if ( filename =~ /.*\.gz$/ )
      handle = Zlib::GzipReader.new(handle)
    end

    $0 = "import-troop #{basename} / #{datasource.try(:id)} started"

    number 		= 0
    durations 		= 0
    firstdate		= nil
    linecount		= 0

    preload_tables

    CSV.parse(handle) do |row|

      before = Time.now
      $0 = sprintf("import troop %20s line %u", datasource.datasource_filename, linecount)
      n1 = import_cdr_troop_row(options, linecount, row, datasource)
      after  = Time.now
      duration = (after-before)*1000   # result is in fractions, but we want it in miliseconds
      durations += duration
      number += 1  if n1
      if n1 and !firstdate
        firstdate = n1.startdatetime
      end
      linecount += 1
    end
    datasource.cdrs_count  = number
    datasource.load_time   = durations / 1000
    datasource.calls_count = datasource.calls.count
    datasource.data_at     = firstdate
    datasource.save!

    return number,durations,firstdate,datasource
  end

  ### Telus import
  def cdr_from_telus_row(item, linecount)
    self.recordid = item[0]
    # n1.callingbtn_id = Btn.get_by_billingtelephonenumber(item[6]).id

    Time.zone = 'Eastern Time (US & Canada)'
    year =  2000 + item[1][0..1].to_i
    month = item[1][2..3].to_i
    day   = item[1][4..5].to_i
    hour  = item[4][0..1].to_i
    minute= item[4][2..3].to_i
    second= item[4][4..5].to_i

    begin
      datetime = Time.zone.local(year, month, day, hour, minute, second)
    rescue ArgumentError
      print "failed to convert time (#{item[1]},#{item[4]}) in Telus row: #{linecount}"
    end

    # XXX probably have a time zone issue here.
    self.startdatetime = datetime
    self.answerdatetime = datetime
    self.callingnumber = item[2]
    self.callednumber = item[3]

    # Need to get this one working, I am not sure what the value from the datasource is.
    self.duration = item[5]

    # This is 'StartDate + Duration (seconds) = EndDate'
    self.releasedatetime = datetime + item[5].to_i.seconds

    self.calltype = 'Normal'
    self.localcallid = sprintf("telus-%u-%u", self.datasource.id, unique_counter)
    # calculate the derived values
    newcategory = self.calc_derived(linecount)

    self.call.try(:fixdates!)
    calc_price!
    save!
  end

  def self.row_from_cdr_telus(string)
    # recordid",	  1->  6 = len 6
    # startdate",	  7-> 12 = len 6
    # gap 2
    # callingnumber",	 15-> 24 = len 10
    # gap 6
    # callednumber",	 30-> 39 = len 10
    # gap 15
    # starttime",	 55-> 60 = len 6
    # duration",	 61-> 67 = len 7
    # gap 46
    # callingbtn_id 1",	113->122 = len 10
    # gap 61
    # callingbtn_id 2",	183->192 = len 10
    #
    (recordid, startdate, ignore1,
     callingnumber, ignore2,
     callednumber,  ignore3,
     starttime, duration, ignore4, btn1, ignore5, btn2, rest) =
      string.unpack("a6a6a2a10a5a10a15a6a7a45a10a60a10a*")

    return [recordid, startdate, callingnumber, callednumber, starttime, duration, btn1, btn2, rest]
  end

  def self.import_cdr_telus_row(options, linecount, row, datasource)
    # This conditional catches the first row of the CDR TROOP .csv files. This is
    # because they all contain a first row of headers.
    return nil if linecount == 0

    n1 = Cdr.new
    n1.datasource = datasource
    n1.lineno = linecount
    n1.cdr_from_telus_row(row, linecount)
    return n1
  end

  def self.import_cdr_telus_line(options, linecount, line, datasource)
    row  = row_from_cdr_telus(line)
    cdr1 = Cdr.import_cdr_telus_row(options, linecount, row, datasource)
    return cdr1
  end

  def self.load_cdr_telus_rebiller(options)

    defaults = { overwrite: false,
                 verbose:   false,
                 origin:    :telus,
                 archive:   false}
    options = defaults.merge!(options)

    origin    = options[:origin]
    filename  = options[:file]
    overwrite = options[:overwrite]
    verbose   = options[:verbose]
    archive   = options[:archive]
    outdir    = options[:outdir]
    lineno = 0

    basename = File.basename(filename, ".gz")
    datasource = Datasource.find_by_datasource_filename(basename)
    $0 = "import #{basename} / #{datasource.try(:id)}"

    if verbose && datasource
      puts "Duplicated source: #{filename}\n"
    end
    return :duplicate if(datasource && !overwrite)

    # Why delete these?
    if(datasource)
      datasource.cdrs_delete_all
      datasource.calls_delete_all
    end

    datasource = Datasource.find_or_make(basename)
    datasource.did_supplier = DIDSupplier.find_by_symbol(origin)
    handle = File.open(filename, "r:ISO-8859-1")
    if ( filename =~ /.*\.gz$/ )
      handle = Zlib::GzipReader.new(handle)
    end

    $0 = "import-#{origin} #{basename} / #{datasource.try(:id)} started"

    durations 		= 0
    linecount		= 0

    firstdate = nil

    handle.each_line { |line|
      linecount += 1
      # skip header
      next if(line =~ /^HDR1/);
      next if(line =~ /^TRL1/);
      before = Time.now
      $0 = sprintf("import %s %20s line %u", origin, datasource.datasource_filename, linecount)
      n = import_cdr_telus_line(options, linecount, line, datasource)
      after  = Time.now
      duration = (after-before)*1000   # result is in fractions, but we want it in miliseconds
      durations += duration
      unless firstdate
        firstdate = n.startdatetime
      end
    }

    datasource.cdrs_count  = linecount
    datasource.load_time   = durations / 1000
    datasource.calls_count = datasource.calls.count
    datasource.data_at     = firstdate
    datasource.save!

    # return count of how many records loaded
    return linecount, durations, firstdate, datasource
  end

  ### OBM import

  def cn(num)
    RateCenter.canonify_number(num)
  end

  def cdr_from_obm_row(row)
    times = nil
    self.callingnumber      = cn(row[9])        # origin number  (column J)
    self.networktranslatednumber  = cn(row[20]) # called number  (column U)
    self.callednumber       = cn(row[12])       # destination number (column M)

    # message code description
    if row[24] == 'Toll-Free long distance completed' or
      row[24] == 'Toll-Free local completed'
      terminating!
      self.called_rate_center = RateCenter.tollfree
    end
    tolled_category!
    toll!

    (day, month, year) = row[13].split('/')
    (hour,minute,second) = row[14].split(/:/)

    self.recordid = "obm-"+UUID.new.generate(:compact)

    self.startdatetime = Time.zone.local(year.to_i, month.to_i, day.to_i,
                                         hour.to_i, minute.to_i, second.to_i)

    # Need to get this one working again.
    # This is 'StartDate + Duration = EndDate'
    # This takes duration splits it into minutes and seconds and adds them
    # separately first to startdatetime and then enddatetime which contains
    # the result from the former.
    conversationduration_parts= row[35].split(":")
    callduration_parts        = row[53].split(":")

    callduration = callduration_parts[0].to_i.minutes +
                   callduration_parts[1].to_i.seconds

    offhookduration = conversationduration_parts[0].to_i.minutes +
                      conversationduration_parts[1].to_i.seconds

    self.releasedatetime = self.startdatetime + callduration
    self.answerdatetime  = releasedatetime - offhookduration

    self.duration = offhookduration

    self.calltype    = 'Normal'
    self.localcallid = sprintf("obm-%u-%u", self.datasource.id, unique_counter)
    self.call ||= Call.new(:master_callid => localcallid, datasource: datasource)

    # calculate the derived values
    newcategory = self.calc_derived(lineno, times)

    cost = row[29].gsub(/\$/,'').to_f
    self.call_cost = cost

    self.call.try(:fixdates!)
    calc_price!
    save!

    #calc_write_times(times)
    #write_times(times)
  end

  def write_times(times)
    # calculate things.
    deltas  = Array.new(32)
    (1..(times.size)).each {|n|
      if times[n]
        deltas[n] = times[n] - times[0]
      end
    }
    @@obm_stats ||= File.open("tmp/obm_stats.csv", "w")
    @@obm_stats.print recordid, deltas.join(','), "\n"
  end

  def calc_write_times(times)
    # calculate things.
    diffs  = Array.new(32)
    (1..(times.size)).each {|n|
      diffs[n] = times[n] - times[n-1]
    }
    @@obm_stats ||= File.open("tmp/obm_stats.csv", "w")
    @@obm_stats.print recordid, diffs.join(','), "\n"
  end

  def self.import_cdr_obm_row(options, linecount, row, datasource)
    n1 = Cdr.new
    n1.datasource = datasource
    n1.lineno = linecount
    n1.cdr_from_obm_row(row)
    n1
  end

  def self.load_cdr_obm(options)

    defaults = { overwrite: false,
                 verbose:   false,
                 archive:   false}
    options = defaults.merge!(options)

    filename  = options[:file]
    overwrite = options[:overwrite]
    verbose   = options[:verbose]
    archive   = options[:archive]
    outdir    = options[:outdir]
    lineno = 0

    basename   = File.basename(filename, ".gz")

    basepath   = options[:basepath]

    if basepath
      len = basepath.length
      if filename[0..(len-1)] == basepath
        uniquename1 = filename[len..-1]
      end
    end
    uniquename = options[:uniquename] || uniquename1 || basename

    datasource = Datasource.find_by_datasource_filename(uniquename)
    $0 = "import #{basename} / #{datasource.try(:id)} [#{uniquename}]"

    if verbose && datasource
      puts "Duplicated source: #{filename}\n"
    end
    return :duplicate if(datasource && !overwrite)
    # Why delete these?
    if(datasource)
      datasource.cdrs_delete_all
      datasource.calls_delete_all
    end

    datasource = Datasource.find_or_make(uniquename)
    datasource.did_supplier = DIDSupplier.find_by_symbol(:obm)
    handle = File.open(filename, "r:ISO-8859-1")
    if ( filename =~ /.*\.gz$/ )
      handle = Zlib::GzipReader.new(handle)
    end

    $0 = "import-obm #{uniquename} / #{datasource.try(:id)} started"

    durations 		= 0
    firstdate		= nil
    linecount		= 0
    durations           = 0

    CSV.parse(handle) do |row|
      linecount += 1
      next if linecount == 1

      before = Time.now
      $0 = sprintf("import obm %20s line %u", datasource.datasource_filename, linecount)
      n = import_cdr_obm_row(options, linecount, row, datasource)
      after  = Time.now
      duration = (after-before)*1000   # result is in fractions, but we want it in miliseconds
      durations += duration
      unless firstdate
        firstdate = n.startdatetime
      end
      if ((linecount % 100) == 0)
        Call.dump_fixdate_debug
      end
    end
    datasource.cdrs_count  = linecount
    datasource.load_time   = durations / 1000
    datasource.calls_count = datasource.calls.count
    datasource.data_at     = firstdate
    datasource.save!

    return linecount,durations,firstdate,datasource
  end

  ### Rogers import
  def row_from_rogers_line(line)
    # Extract the data form the data files.
    #  {"field"=>"callingbtn_id",        "start"=>1,      "end"=>10   len: 10
    #                                                                 gap: 16
    #  {"field"=>"callingnumber",        "start"=>27,     "end"=>36   len: 10
    #                                                                 gap: 27
    #  {"field"=>"callednumber",         "start"=>64,     "end"=>73   len: 10
    #                                                                 gap: 79
    #  {"field"=>"startdate",            "start"=>153,    "end"=>160  len: 8
    #  {"field"=>"starttime",            "start"=>161,    "end"=>166  len: 6
    #  {"field"=>"duration",             "start"=>167,    "end"=>172  len: 6

    (accountcode,  gap1, callingnumber, gap2, callednumber, gap3, startdate, starttime, duration) =
      line.unpack("a10a16a10a27a10a79a8a6a6")

    return [accountcode, callingnumber, callednumber, startdate, starttime, duration]

  end

  def cdr_from_rogers_row(row)
    (accountcode, callingnumber, callednumber, startdate, starttime, duration) = row
    self.callingnumber = callingnumber
    self.callednumber  = callednumber

    self.recordid = "rogers-"+UUID.new.generate(:compact)

    (year, month, day)   = startdate.unpack("a4a2a2")
    (hour,minute,second) = starttime.unpack("a2a2a2")

    begin
      self.startdatetime = Time.zone.local(year.to_i, month.to_i, day.to_i,
                                           hour.to_i, minute.to_i, second.to_i)
    rescue ArgumentError
      print "failed to convert time value (#{startdate},#{starttime}) in Rogers row: #{linecount}"
    end

    self.duration         = duration.to_i.seconds
    self.localcallid      = sprintf("rogers-%u-%u", self.datasource.id, unique_counter)
    self.answerdatetime   = self.startdatetime
    self.releasedatetime  = self.startdatetime + self.duration
    calc_price!
    save!
  end

  def self.import_cdr_rogers_line(options, linecount, line, datasource)
    n1 = Cdr.new
    n1.datasource = datasource
    n1.lineno = linecount
    row = n1.row_from_rogers_line(line)
    n1.cdr_from_rogers_row(row)
    n1
  end

  def self.load_cdr_rogers(options)

    defaults = { overwrite: false,
                 verbose:   false,
                 archive:   false}
    options = defaults.merge!(options)

    filename  = options[:file]
    overwrite = options[:overwrite]
    verbose   = options[:verbose]
    archive   = options[:archive]
    outdir    = options[:outdir]
    lineno = 0

    basename = File.basename(filename, ".gz")
    datasource = Datasource.find_by_datasource_filename(basename)
    $0 = "import #{basename} / #{datasource.try(:id)}"

    if verbose && datasource
      puts "Duplicated source: #{filename}\n"
    end
    return :duplicate if(datasource && !overwrite)

    # Why delete these?
    if(datasource)
      datasource.cdrs_delete_all
      datasource.calls_delete_all
    end

    datasource = Datasource.find_or_make(basename)
    datasource.did_supplier = DIDSupplier.find_by_symbol(:rogers)
    handle = File.open(filename, "r:ISO-8859-1")
    if ( filename =~ /.*\.gz$/ )
      handle = Zlib::GzipReader.new(handle)
    end

    $0="import-rogers #{basename} / #{datasource.try(:id)} started"
    durations 		= 0
    firstdate		= nil
    linecount		= 0
    durations           = 0

    handle.each_line { |line|
      linecount += 1
      # skip header
      next if(line =~ /^HDR1/);
      next if(line =~ /^TRL1/);
      next if(line =~ /^\x1A/);  # skip closing ^Z in DOS files
      before = Time.now
      $0 = sprintf("import rogers %20s line %u", datasource.datasource_filename,
                   linecount)
      n = import_cdr_rogers_line(options, linecount, line, datasource)
      after  = Time.now
      duration = (after-before)*1000   # result is in fractions, but we want it in miliseconds
      durations += duration
      unless firstdate
        firstdate = n.startdatetime
      end
    }

    datasource.cdrs_count  = linecount
    datasource.load_time   = durations / 1000
    datasource.calls_count = datasource.calls.count
    datasource.data_at     = firstdate
    datasource.save!

    return linecount,durations,firstdate,datasource
  end

  ### end of legacy import routines

  def self.preloaded?
    @@preloaded ||= false
  end

  def self.preload_tables
    if $CDR_LOAD_PRELOAD_PHONES
      if !preloaded?
        $0 = "import preloading phones"
        Service.preload_phones  #Phone class renamed to Service t133331
        $0 = "import preloading btns"
        Btn.preload_btns!
        $0 = "import preloading complete"
        @@preloaded = true
      end
    end
  end

  def self.load_bw_from_handle(basename, handle, datasource, verbose)
    fileno=0
    linecount=1
    loadedcount=0
    durations = 0
    firstdate = nil

    # do not preload for testing...
    $0 = "import #{basename} / #{datasource.try(:id)} started"

    preload_tables
    Signal.trap("USR2") do
      puts "Status on USR2..."
      puts "Rails cache info: #{Rails.cache.inspect}"
    end

    puts "Starting import of #{basename}" if verbose
    CredilCSV.parse(handle,
                    :row_sep => "\n".encode("iso-8859-1"),
                    :escape_char => '\\') do |row|
      unless(row[0] =~ /version=/ ||
             row[2] == 'Start'    ||
             row[2] == 'Failover' ||
	     row[0].blank? ||
             row[2] == 'End')
        before = Time.now
        $0 = sprintf("import %20s line %u", datasource.datasource_filename, linecount)
        n1,newcategory = Cdr.import_row(row, datasource, linecount)
        after  = Time.now
        duration = (after-before)*1000   # result is in fractions, but we want it in miliseconds
        durations += duration
        if n1
          puts sprintf("(%02u)%05u: (%4u) %s\n", fileno, linecount, duration, n1.to_str) if verbose
          loadedcount += 1
          unless firstdate
            firstdate = n1.startdatetime
          end
        end
      end
      linecount += 1
    end
    puts "Ending import of #{basename}" if verbose
    datasource.cdrs_count  = loadedcount
    datasource.load_time  = durations / 1000
    datasource.calls_count = datasource.calls.count
    datasource.data_at    = firstdate
    datasource.save!
    return loadedcount, durations, firstdate
  end

  def btnidforphone(phone_id)
    @@phone_btn ||= Hash.new
    unless @@phone_btn[phone_id]
      @@phone_btn[phone_id] = Service.find(phone_id).try(:btn_id) #Phone class renamed to Service
    end
    @@phone_btn[phone_id]
  end

  def btnfix
    saveit=false
    if calledphone && !calledbtn
      self.calledbtn_id  = btnidforphone(self[:calledphone_id])
      #puts "Fixing #{id} #{calledphone_id} -> #{calledbtn_id}"
      saveit=true
    end
    if callingphone && !callingbtn
      self.callingbtn_id = btnidforphone(self[:callingphone_id])
      #puts "Fixing #{id} #{calledphone_id} -> #{calledbtn_id}"
      saveit=true
    end
    if saveit
      save!
      return true
    end
    return false
  end

  def self.btnfix_all
    count=0
    increment=1

    while increment > 0
      increment = 0
      Cdr.where('calledbtn_id is NULL and calledphone_id is not NULL').order('recordid ASC').limit(100).find_each { |cdr|
        count += 1
        if cdr.btnfix
          increment += 1
        end
        if (count % 1000) == 1
          puts " ..fixed up #{count} records #{increment}"
        end
      }
    end

    increment=1
    while increment > 0
      increment = 0
      Cdr.where('callingbtn_id is NULL and callingphone_id is not NULL').order('recordid ASC').limit(100).find_each { |cdr|
        count += 1
        if cdr.btnfix
          increment += 1
        end
        if (count % 1000) == 1
          puts " ..fixed up #{count} records #{increment} (2)"
        end
      }
    end
    puts "Fixed up #{count} records"
  end

  def self.callidfix_all
    count=0
    increment=1
    offsetcount= ENV['OFFSET'] || '0'
    offset = offsetcount.to_i * 200

    while increment > 0
      increment = 0
      Cdr.where('call_id is NULL').order('recordid ASC').limit(200).find_each { |cdr|

        increment += 1

        # we reload here, because the database might have changed under us.
        cdr.reload
        next if cdr.call

        count += 1
        cdr.associate_to_call
        cdr.save!
        if (count % 1000) == 1
          puts " ..fixed up #{count} records #{increment}"
        end
      }
    end
  end

  # this is a debug function.
  # use it like:
  #   File.open("/home/mcr/galaxy/tmp/k7.csv", "w") do | file| c1.dump_with_friends(file, [c2]) end
  #
  # to get something that you can open in OO or something to compare two records easily.
  def dump_with_friend(cdr2, file = $stdout)
    dump_with_friends(file, [cdr2])
  end

  def dump_with_friends(file, friends)
    attributes.each { |key, value|
      unless value.blank? &&
	  # see if other elements are nil too.
	  friends.inject(true) { |result, friend| result && friend[key].blank? }
	values = friends.collect { |friend| sprintf("\"%s\"", friend[key]) }
	file.puts "#{key},\"#{value}\"," + values.join(",")
      end
    }
  end

  def savefixturefw(fw)
    return if save_self_tofixture(fw)

    # now for related items.
    self.datasource.savefixturefw(fw)   if self.datasource
    self.calledbtn.savefixturefw(fw)    if self.calledbtn
    self.callingbtn.savefixturefw(fw)   if self.callingbtn
    self.calledphone.savefixturefw(fw)  if self.calledphone
    self.callingphone.savefixturefw(fw) if self.callingphone
    self.call.savefixturefw(fw) if self.call
  end

  # price calculation for legs

  def active_duration
    #@active_duration ||= (self.releasedatetime - self.startdatetime)
    if releasedatetime and answerdatetime
      @active_duration ||= (self.releasedatetime - self.answerdatetime)
    else
      0
    end
  end

  def calc_service_number
    if terminating?
      basename = self.calledphone.try(:name) || self.calledbtn.try(:name)
      if callednumber != basename and !callednumber.blank?
        basename = basename + ">" + callednumber
      end
      basename
    else
      self.callingphone.try(:name) || self.callingbtn.try(:name)
    end
  end
  def service_number
    @service_number ||= calc_service_number
  end

  def billing_calltype
    return 'local'           if networkcalltype == 'lo'
    if (networkcalltype == 'to' || networkcalltype == 'in') and called_rate_center
      # may return caribbean, canada, usa.
      return called_rate_center.calltype
    end
    return 'local'
  end

  def invoice_fmt_date
    # Localize date format
    if local_starttime.nil?
      calc_time(client.timezone_obj)
    end
    local_starttime.to_date
  end

  def invoice_fmt_time
    local_starttime.strftime('%H:%M:%S')
  end

  # used in debugging only.
  def invoice_fmt_rate
    sprintf("%.6f", rate.cost)
  end

  def invoice_fmt_callednumber
    self.callednumber
  end

  def invoice_fmt_price
    price
  end

  def invoice_fmt_place
    return "UNKNOWN" unless called_rate_center
    city = called_rate_center.lerg6name
    province = called_rate_center.province
    isocountry = called_rate_center.isocountry
    # Join all non-nil non-blank words with a comma
    [city, province, isocountry].compact.reject(&:blank?).join(', ')
  end

  def invoice_fmt_place_split
    false unless called_rate_center
  end

  def invoice_fmt_city_place
    return "UNKNOWN" unless called_rate_center
    called_rate_center.city_place
  end

  def invoice_fmt_prov_place
    province = called_rate_center.province
    isocountry = called_rate_center.isocountry

    # Join all non-nil non-blank words with a comma
    [province, isocountry].compact.reject(&:blank?).join(', ')
  end

  def invoice_fmt_duration
    CallSummary.fmt_duration(billed_duration)
  end

  # in order to calculate price, one needs to know the rate plan
  # associated with the btn.
  def calc_outgoing_rate_plan
    # figure out which plan can accomodate this rate center.

    # sometimes we match the BTN rather than the phone, and the
    # calling phone winds up nil.

    thing = self.callingphone
    unless thing
      thing = self.callingbtn
    end

    return nil unless thing
    plan = thing.local_plan
    if plan
      return plan if plan.acceptable_rate_center(self.called_rate_center)
    end

    plan = thing.national_plan
    if plan
      return plan if plan.acceptable_rate_center(self.called_rate_center)
    end

    return thing.world_plan
  end
  def calc_rate_plan
    if terminating?
      RatePlan.incoming_toll_rate_plan
    else
      calc_outgoing_rate_plan
    end
  end

  def rate_plan
    @rate_plan ||= calc_rate_plan
  end

  def rate_plan_name
    rate_plan.try(:name)
  end

  def rate
    @rate ||= rate_plan.find_rate_for(called_rate_center)
  end

  def calc_time(timezone_obj)
    @localtime  = timezone_obj.utc_to_local(self.startdatetime)
  end

  def local_starttime
    @localtime ||= localtime
  end

  def call_price
    unless self[:call_price]
      calc_price!
    end
    self[:call_price]
  end

  def calc_price!
    begin
      calc_price
      self.call_price = @price
    rescue MissingRatePlan
      nil
    rescue MissingRate
      nil
    end
  end

  def calc_price
    return if @price_calculated

    @nobill = false
    @billed_duration  = 0
    @price = 0

    if terminating? and !datasource.did_supplier.try(:voip?)
      # must be a legacy supplier, probably 1800 supplier.
      @nobill = false
      @price_calculated = true
      # use our cost as the price.
      @price            = call_cost
      @billed_duration  = active_duration
      return
    end

    if private? or called_rate_center.nil? or called_rate_center.try(:tollfree?)
      @nobill = true
      @price_calculated = true
      return
    end

    unless rate_plan
      @price = "rate plan error"
      @price_calculated = true
      raise MissingRatePlan
    end

    if rate_plan.always_zero?
      @nobill = true
      @billed_duration  = active_duration
      @price_calculated = true
      return
    end

    unless rate
      @price = "rate missing"
      @price_calculated = true
      @billed_duration  = active_duration
      raise MissingRate.new("calc_price can not find rate for", rate_plan, called_rate_center)
    end

    @billed_duration = rate.duration_round(active_duration)

    @price = rate_plan.plan_discount(rate, @billed_duration)
    if @price <= 0.01
      @price = 0.01
    end
    @price_calculated = true
  end

  def localtime
    calc_price
    @localtime
  end

  def billed_duration
    calc_price
    @billed_duration
  end

  def price
    calc_price
    @price
  end

  def no_bill?
    calc_price

    return @nobill
  end

  # legacy CDR support
  def self.find_legacy_cdr_files(pattern, dir, &block)

    return unless Dir.exists?(dir)
    Find.find(dir) do |path|
      stat = File.stat(path)
      next if stat.directory?

      mtime = stat.mtime
      latest_mtime ||= mtime

      # look for all the files which end in
      #    ur*.zip, or ur*.csv
      #
      # which are not already listed as datasources, and then
      # pass them on to the caller's block.  If the file was
      # zip'ed, then the file is unzipped and file by the same
      # name is examined and passed in.
      #

      # path is the file to open, basename is the name of the source.
      # path may not point to a file named basename due to unzip'ing, etc.

      basename = File.basename(path)
      if (pattern.match(basename))

        unless Datasource.find_by_datasource_filename(basename)
          block.call(path, basename, dir)
        end
      end
    end
  end

  def self.troop_pattern
    /ur.*\.(bt290|csv)$/
  end
  def self.telus_pattern
    /.tr290$/
  end
  def self.obm_pattern
    /.obm290$/
  end
  def self.rogers_pattern
    /C.*\.sprint290$/
  end
  def self.drapper_pattern
    /TELUS.*\.(tr290|csv)$/
  end

  def self.process_legacy_file(path, basename, dir, &block)
    starttime   = Time.now

    $0 = "preloading tables"
    preload_tables
    $0 = "processing files"

    #puts "Opening file #{path}"
    #print "Processing file #{basename}"

    number,durations,firstdate,datasource = block.call({file: path,
                                                        basename: basename,
                                                        basepath: dir})

    if number == :duplicate
      puts "..skipped, is duplicate" if @@legacy_debug
    else
      endtime   = Time.now
      duration = endtime-starttime
      rate = number / duration
      avgduration = duration / number
      puts " ..loaded #{number} records in #{duration}s, at #{rate} cdrs/s dbtime:#{avgduration}\n"
      Call.dump_fixdate_debug
      if(datasource.load_time > 0)
        rate1 = datasource.cdrs_count / datasource.load_time
        rate2 = datasource.calls_count / datasource.load_time
        did = datasource.id.try(:to_s) || "bad-id"
        puts sprintf(" ..datasource: %05d %05d CDRs  %05ds, at %2.2f cdrs/s\n", did, datasource.cdrs_count, datasource.load_time, rate1);
        puts sprintf(" ..datasource: %05d %05d calls %05ds, at %2.2f calls/s\n", did, datasource.calls_count, datasource.load_time, rate2);
      end
    end
  end

  def self.find_and_load_files(pattern, directory, &block)
    finish = false

    Signal.trap("USR1") do
      puts "Terminating on USR1..."
      finish=true
    end
    Signal.trap("TERM") do
      puts "Terminating on TERM at end of file..."
      finish=true
    end

    $0 = "preloading tables"
    preload_tables

    $0 = "processing files"

    Cdr::find_legacy_cdr_files(pattern, directory) { |path, basename, dir|
      exit if finish

      process_legacy_file(path, basename, dir, &block)
    }
  end


end
