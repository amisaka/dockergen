class AreaCode < ApplicationRecord

  self.table_name = "cdrviewer_areacode"
  self.primary_key = 'id'

  after_commit   :clear_id_cache
  after_destroy  :clear_id_cache

  def self.cache_name_key(npa)
    "npa:#{npa}"
  end

  def cache_name_key
    self.class.cache_name_key(npa)
  end

  def self.get_by_npa(npa)
    # caches nil replies
    Rails.cache.fetch cache_name_key(npa) do
      find_by_npa(npa)
    end
  end

  def name
    "npa#{npa}_#{id}"
  end

  protected

    def clear_id_cache
       Rails.cache.delete cache_name_key
       Rails.cache.delete cache_id_key
    end

end
