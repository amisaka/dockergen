class Hakka::HakkaRecord < ActiveRecord::Base
  if $CONNECTMSSQL
    establish_connection :hakka
    self.abstract_class = true
  end
end
