module Hakka
  class ChargeAssignment < HakkaRecord

    self.primary_key = "ChargeAssignmentPID"   # accessed as "id"
    self.table_name  = "ChargeAssignment"

    scope :active, -> { where(:Active => 1) }
    scope :inactive, -> { where(:Active => 0) }

    alias_attribute :active, :Active

    belongs_to :customer,
      foreign_key: 'CustomerPID'

    belongs_to :product, # Could be a conflict here as we are renaming service -> product (t13330), but this was already named product before t13330 effort.  But w3e haven't used Hakka ever since I've been on this project.
      foreign_key: 'ProductPID'

    belongs_to :recurrence_info,
      foreign_key: 'RecurrenceInfoPID'

    belongs_to :recurrence_type,
      foreign_key: 'RecurrenceTypePID'

    belongs_to :product, # Renamed service -> product as part of t13330
      foreign_key: 'ServicePID'

    belongs_to :department,
               foreign_key: "DepartmentPID"

    has_one :billable_services, foreign_key: 'foreign_pid', class_name: ::BillableService

    def make_billable_service_hash
      client = customer.associated_client

      hash = {
        foreign_pid: self[:ChargeAssignmentPID],
        totalprice: self[:TotalPrice],
        unitprice: self[:UnitPrice],
        qty: self[:Quantity],
        product: (product.associated_service if product),# Renamed service -> product as part of t13330
        service_name: (service[:DisplayText] if service),
        site: (department.associated_site if department),
        # self[:UnitPrice],1
        # special ProductPID: 753,
        #ActiveServiceCount
        #CloseFinalised
        #ChargeAssignmentPID: 1,
        #AssignmentLevelPID: 3,
        #Active: false,
        #StartDate: Fri, 21 Feb 2014 00:00:00 UTC +00:00,
        #ChargeModePID: 2,
        #ReferenceCode: nil,
        #Memo: nil,
        #RecurrenceTypePID: 2,
        #RecurrenceInfoPID: 1,
        #LastAppliedDate: Mon, 31 Mar 2014 00:00:00 UTC +00:00,
        #EquipmentAssignmentTypePID: nil,
        #MaxCummulativeAmount: nil,
        #CummulativeAmount: nil,
        #PurchaseOrderPID: nil,
        #ContractPID: nil,
        #DepartmentPID: nil,
        #BundleAssignmentPID: nil,
        #Description: "911 service VoIP - monthly access fee",
        #OverrideGLAccount: false,
        #GLAccountPID: nil,
        #OwnerAppUserPID: 1,
        #OwnerTeamPID: nil,
        #InsertAppUserPID: 1,
        #UpdateAppUserPID: 2,
        #CreatedOn: Sun, 09 Mar 2014 14:27:56 UTC +00:00,
        #MinimumBillingRequirementPID: nil,
        #InvoicePID: nil,
        #TotalOccurrences: 3,
        #LastChargeStartDate: Tue, 01 Apr 2014 00:00:00 UTC +00:00,
        #LastChargeEndDate: Mon, 31 Mar 2014 00:00:00 UTC +00:00,
        #LastInvoicePID: 1697,
        #ARDBName: nil,
        #TaxLocalePID: nil,
        #UnitCost: nil,
        #PPProductAssignmentPID: nil>
      }

      if self[:UpdatedOn]
        hash[:updated_at] = self[:UpdatedOn]
      end
      if self[:AssignedOn]
        hash[:created_at] = self[:AssignedOn]
      end
      if service.present?
        hash[:asset] = self.service.associated_asset
      end

      if recurrence_type.try(:Code) == "Recurring"
        hash[:renewable] = true
        if recurrence_info.try(:Frequency_Months)
          hash[:frequency_months] = recurrence_info.try(:Frequency_Months)
        end
      end

      hash
    end

    def map_by_foreign_pid
      n = BillableService.find_by_foreign_pid(self[:ChargeAssignmentPID])
      if n
        n.update(make_billable_service_hash)
        n.service_dates_force!
      end
      n
    end

    def create_billable_service
      n = BillableService.create(make_billable_service_hash)
    end

    def associated_billable_service(invoice = nil)
      n = (map_by_foreign_pid || create_billable_service)
      n.invoice = invoice
      n
    end

  end
end
