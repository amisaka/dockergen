module Hakka
  class Service < HakkaRecord

    self.primary_key = "ServicePID"
    self.table_name  = "Service"

    belongs_to :customer,
      foreign_key: 'CustomerPID'

    def make_asset_hash
      { foreign_pid: self[:ServicePID],
        service_id: self[:ID],
        btn: customer.associated_client.btn,
      }
    end

    def map_by_foreign_pid
      n = Service.where(foreign_pid: self[:ServicePID]).take
      n.update(make_asset_hash) if n
      n
    end

    def create_asset
      Service.create(make_asset_hash)
    end

    def associated_asset
      map_by_foreign_pid || create_asset
    end

  end
end
