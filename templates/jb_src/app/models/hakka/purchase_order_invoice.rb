module Hakka
  class PurchaseOrderInvoice < HakkaRecord
    self.primary_key = "InvoicePID"
    self.table_name  = "PurchaseOrderInvoice"
  end
end

