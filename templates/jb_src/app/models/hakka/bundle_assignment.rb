module Hakka
  class BundleAssignment < HakkaRecord
    self.primary_key = "BundleAssignmentPID"
    self.table_name  = "BundleAssignment"
  end
end

