# coding: utf-8
module Hakka
  class Customer < HakkaRecord
    # In March 2018, the Service model has been renamed Product as part of t13330.  This code was not tocuhed in the rename as I don't believe it was in use
    # In April 2018, the Phone   model has been renamed Service as part of t13331.  This code was not tocuhed in the rename as I don't believe it was in use

    has_one :client,
      foreign_key: 'foreign_pid'

    self.primary_key = "CustomerPID"
    self.table_name  = "Customer"

    has_many :charge_assignments,
      foreign_key: 'CustomerPID'

    has_many :active_charges,
      -> { active },
      foreign_key: 'CustomerPID',
      class_name: 'Hakka::ChargeAssignment'

    has_many :inactive_charges,
      -> { inactive },
      foreign_key: 'CustomerPID',
      class_name: 'Hakka::ChargeAssignment'

    has_many :products,
      through: :charge_assignments

    has_many :departments,
      foreign_key: 'CustomerPID'

    alias_attribute :name, :CustomerName

    def language
      # match Francais with and without accents
      return "English" unless self[:LanguagePreference]
      return "French" if self[:LanguagePreference].downcase =~ /^fr/
      return "English"
    end

    def make_client_hash
      n = { foreign_pid: self[:CustomerPID],
        billing_btn: self[:Code],
        #NameTitle
        #FirstName
        #MiddleName
        #LastName
        #NameSuffix
        #County
        billing_cycle_id: self[:BillingCyclePID],
        created_at: self[:CreatedOn],
        started_at: self[:StartDate],
        terminated_at: self[:TerminationDate],
        #CurrentAccountStatusTypePID   !!!
        #GLAccountGroupPID
        #CustomerCategoryPID
        language_preference: language,
        #BillingInfoPID                !!!
        #DiscountChargesinInvoiceOnly
        #Privacy
        #TopicPID
        #ThirdPartyBilling
        #TriggerDirectBilling
        #UsageRecordRateThresholdAmount
        #AutoCapUsageRecordRate
        #Round2Decimal
        #ResultantRound2Decimal
        #RoundingMethod
        #ResultantRoundingMethod
        #MonthlyThresholdCasualService
        #UsageRecordQuantityThreshold
        #AutoCapUsageRecordQuantity
        #CurrencyConversionFactorPID
        #ParentCustomerPID
        #MigrationDate
        #OwnerAppUserPID
        #OwnerTeamPID
        #InsertAppUserPID
        #UpdateAppUserPID
        updated_at: self[:UpdatedOn]
        #ActiveServiceCount
        #CloseFinalised
          }
      n
    end

    def map_by_foreign_pid
      client = Client.find_by_foreign_pid(self[:CustomerPID])
      client
    end

    def simplecode
      # strips non numeric chars
      @simplecode ||= self[:Code].gsub(/[^0-9]/, '')
    end

    def map_by_btn
      btn = Btn.get_by_billingtelephonenumber(self[:Code]) ||
            Btn.get_by_billingtelephonenumber("+1" + self[:Code]) ||
            Btn.get_by_billingtelephonenumber(simplecode) ||
            Btn.get_by_billingtelephonenumber("+1" + simplecode)

      client = btn.try(:client)
      client
    end

    def map_by_did
      pn = Phone.find_by_phone_number(self[:Code]) ||
           Phone.find_by_phone_number("+1" + self[:Code])
           Phone.find_by_phone_number(simplecode)
           Phone.find_by_phone_number("+1" + simplecode)

      btn    = pn.try(:btn)
      client = btn.try(:client)
      client
    end

    def fix_up_client(newone)
      newone.client_crm_account_code_fix(nil, self[:CustomerName])
      newone.fix_primary_btn
      newone.save!
    end

    def find_associated_client
      client = map_by_foreign_pid # || map_by_btn || map_by_did
      fix_up_client(client) if client
      client
    end

    def make_associated_client
      newone = Client.create(make_client_hash)
      fix_up_client(newone) if newone
      newone
    end

    def associated_client
      find_associated_client || make_associated_client
    end

    def create_billable_services(invoice)
      self.charge_assignments.active.each { |ca|
        bs = ca.associated_billable_service(invoice)
        bs.save!
      }
    end

    def billing_address
      Address.first_billing_address_by_customer_pid self[:CustomerPID]
    end

    def shipping_address
      Address.first_shipping_address_by_customer_pid self[:CustomerPID]
    end

    def primary_address
      Address.first_primary_address_by_customer_pid self[:CustomerPID]
    end

    def other_address
      Address.first_other_address_by_customer_pid self[:CustomerPID]
    end

    def addresses
      Address.find_by_customer_pid(self[:CustomerPID])
    end

  end
end
