module Hakka
  class Product < HakkaRecord
    # In March 2018, Services has been renamed to product as part of t13330.  The code here has not be touched as part of this effort as I don't believe it is in use.  -- jlam@credil.org

    self.primary_key = "ProductPID"
    self.table_name  = "Product"

    has_one :service,
      class_name: '::Service',
      foreign_key: 'foreign_pid'

    has_many :customers,
      through: :charge_assignments

    has_many :departments,
      through: :charge_assignments

    has_many :charge_assignments,
             foreign_key: 'ProductPID'

    belongs_to :product_category, foreign_key: 'ProductCategoryPID'

    def make_service_hash
      { name: self[:Code],
        foreign_pid: self[:ProductPID],
        description: self[:Description],
        unitprice: self[:UnitCost] || self[:Amount],
        renewable: true,
        available: self[:Active]
      }
    end

    def make_product
      ::Service.create(make_service_hash)
    end

    def map_by_foreign_pid
      n = ::Service.where(foreign_pid: self[:ProductPID]).take
      n.update(make_service_hash) if n
      n
    end

    def associated_service
      map_by_foreign_pid || make_product
    end


    def make_service_hash
      { name: self[:Code],
        foreign_pid: self[:ProductPID],
        description: self[:Description],
        unitprice: self[:UnitCost],
        renewable: true,
        available: self[:Active]
      }
    end

  end
end
