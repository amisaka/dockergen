module Hakka
  class ProductCategory < HakkaRecord
    self.primary_key = "ProductCategoryPID"
    self.table_name  = "ProductCategory"
  end
end

