module Hakka
  class RecurrenceType < HakkaRecord
    self.primary_key = "RecurrenceTypePID"
    self.table_name  = "RecurrenceType"
  end
end

