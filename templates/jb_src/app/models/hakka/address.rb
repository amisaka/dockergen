module Hakka
  class Address < HakkaRecord

    self.table_name  = 'Kilmist.Address'
    self.primary_key = 'AddressPID'

    def self.first_billing_address_by_customer_pid(customer_pid)
      address_for(customer_pid, 'Billing')
    end

    def self.first_shipping_address_by_customer_pid(customer_pid)
      address_for(customer_pid, 'Shipping')
    end

    def self.first_primary_address_by_customer_pid(customer_pid)
      address_for(customer_pid, 'Primary')
    end

    def self.first_other_address_by_customer_pid(customer_pid)
      address_for(customer_pid, 'Other')
    end

    def self.first_by_customer_pid(customer_pid)
      find_by_customer_pid(customer_pid).first
    end

    def self.find_by_customer_pid(customer_pid)
      find_by_sql [<<-END_SQL, customer_pid]
        SELECT Address.*
        FROM Kilmist.Address
        JOIN dbo.CustomerAddress
        ON CustomerAddress.AddressPID = Address.AddressPID
        JOIN dbo.Customer
        ON Customer.CustomerPID = CustomerAddress.CustomerPID
        WHERE Customer.CustomerPID = ?
      END_SQL
    end

    private

      def self.address_for(customer_pid, address_type)
        find_by_sql([<<-END_SQL, customer_pid, address_type]).first
          SELECT Address.*
          FROM Kilmist.Address
          JOIN dbo.CustomerAddress
          ON CustomerAddress.AddressPID = Address.AddressPID
          JOIN dbo.Customer
          ON Customer.CustomerPID = CustomerAddress.CustomerPID
          JOIN Kilmist.AddressType
          ON CustomerAddress.AddressTypePID = AddressType.AddressTypePID
          WHERE Customer.CustomerPID = ? AND AddressType.Code = ?
        END_SQL
      end

  end
end
