module Hakka
  class Department < HakkaRecord

    self.primary_key = "DepartmentPID"
    self.table_name  = "Department"

    alias_attribute :name, :DepartmentName

    belongs_to :customer,
      foreign_key: 'CustomerPID'

    has_many :charge_assignments,
      foreign_key: 'DepartmentPID'

    has_many :active_charges,
      -> { active },
      foreign_key: 'DepartmentPID',
      class_name: 'Hakka::ChargeAssignment'

    has_many :inactive_charges,
      -> { inactive },
      foreign_key: 'DepartmentPID',
      class_name: 'Hakka::ChargeAssignment'

    has_many :products,
      through: :charge_assignments

    def make_site_hash
      hash = {
        foreign_pid: self[:DepartmentPID],
        name:        self[:DepartmentName],
        # ParentDepartmentPID,
        # CustomerPID,
        # GLAccountGroupPID,
        # CustomerCategoryPID,
        # CurrentAccountStatusTypePID,
        created_at:  self[:CreatedOn],
        activation_date:  self[:StartDate],
        termination_date: self[:TerminationDate],
        site_number:      self[:Code],
        # LanguagePreference,
        # DiscountChargesinInvoiceOnly,
        # Comments,
        # Privacy,
        # TopicPID,
        # Round2Decimal,
        # ResultantRound2Decimal,
        # RoundingMethod,
        # ResultantRoundingMethod,
        # UsageRecordRateThresholdAmount,
        # AutoCapUsageRecordRate,
        # MonthlyThresholdCasualService,
        # UsageRecordQuantityThreshold,
        # AutoCapUsageRecordQuantity,
        # CurrencyConversionFactorPID,
        # OwnerAppUserPID,
        # OwnerTeamPID,
        # InsertAppUserPID,
        # UpdateAppUserPID,
        # UpdatedOn,
        # CloseFinalised,
        # PPDepartmentPID
      }

      if self[:UpdatedOn]
        hash[:updated_at] = self[:UpdatedOn]
      end

      hash
    end

    def map_by_foreign_pid
      n = Site.find_by_foreign_pid(self[:DepartmentPID])
      if n
        $HAKKASTATS[:department_reused] += 1
        begin
          n.update(make_site_hash)
        rescue ArgumentError
          byebug
          puts "Looking for Department: #{self[:DepartmentPID]}"
        end
      end
      n
    end

    def map_by_site_name
      client = customer.associated_client
      n = client.sites.find_by_name(self[:DepartmentName])
      if n
        $HAKKASTATS[:department_map_by_name] += 1
      end
      n
    end

    def create_site
      n = Site.create(make_site_hash)
      $HAKKASTATS[:department_created] += 1
      n
    end

    def associated_site
      n = (map_by_foreign_pid || map_by_site_name || create_site)
      n
    end

    def update_associated_site
      associated_site.update(make_site_hash)
    end
  end
end
