module Hakka
  class CustomerGroup < HakkaRecord
    self.primary_key = "CustomerGroupPID"
    self.table_name  = "CustomerGroup"

    has_and_belongs_to_many :customers, join_table: 'CustomerCustomerGroup', class_name: 'Hakka::Customer', foreign_key: 'CustomerGroupPID', association_foreign_key: 'CustomerPID'
  end
end

