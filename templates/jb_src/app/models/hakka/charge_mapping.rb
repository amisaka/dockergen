module Hakka
  class ChargeMapping < HakkaRecord
    self.primary_key = "ChargeMappingPID"
    self.table_name  = "BillSoftTax.ChargeMapping"
  end
end

