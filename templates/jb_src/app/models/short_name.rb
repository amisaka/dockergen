class ShortName < ApplicationRecord

  def self.permute(name)
    forms = Hash.new
    newstring = name
    find_each {|xfrm|
      newstring0 = xfrm.substitute(newstring)
      if newstring0
        forms[newstring0] = true
        newstring = newstring0
      end

      newstring1 = xfrm.substitute(name)
      if newstring1
        forms[newstring1] = true
      end
    }
    forms.keys
  end

  def substitute(str)
    str0 = str.gsub(from, to)
    if str0 == str
      return nil
    else
      return str0
    end
  end


end
