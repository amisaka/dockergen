#
# this class stores the LERG6 data
#

class NorthAmericanExchange < ApplicationRecord
  # self.primary_key = "id"

  CDN_PROVINCES = %w(AB BC MB NB NL NT NS NU ON PE QC SK YT LB PQ)
  US_STATES = %w(AL AK AZ AR AA AE AP CA CO CT DE DC FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY)



  has_many :local_exchanges_north_american_exchange,
           :foreign_key => 'north_american_exchanges_id'
  has_many :local_exchanges,
           :through => :local_exchanges_north_american_exchange

  belongs_to :rate_center

  scope :active, -> { where(deleted_at: nil) }

  def simplename
    "nae#{id}"
  end

  def active?
    deleted_at.blank?
  end
  def inactive?(now = Time.now)
    !active? && deleted_at < now
  end

  def self.find_by_npa_nxx(npa, nxx)
    NorthAmericanExchange.where(npa: npa.to_i, nxx: nxx.to_i)
  end

  def self.findbyphone(phone)

    case
    when phone.nil?
      return nil

    when phone.length == 10
      npa = phone[0..2]
      nxx = phone[3..5]

    when phone[0] == '1'
      npa = phone[1..3]
      nxx = phone[4..6]

    when (phone[0] == '+' and phone[1] == '1')
      npa = phone[2..4]
      nxx = phone[5..7]

    else
      return nil
    end

    return self.where(npa: npa, nxx: nxx).first

  end

  def self.import_row(line, stats = Hash.new(0))
    #   Populate the variables using the string.unpack method
    # LERG6 fields position:
    # 00  1- Lata(5)                2- Lata Name(20)              3- Status(1)
    # 26  4- Effective Date(6)      5- NPA(3)                     6- NXX(3)
    # 38  7- Block ID (A, 0-9) [only use A](1)                    8- filler(4)
    # 43  9- COC Type(3)           10- Special Service Code(4)   11- Diable Indicator (DIND)(Y/N)(1)
    # 51 12- End Office(EO)(2)     13- Access Tandem (AC)(2)     14- Portable Indicator (Y/N)(1)
    # 56 15- Administrative OCN(4) 16- filler(1)                 17- Operating Company Numnber OCN(4)
    # 65 18- Locality Name(10)     19- Locality Country(2)       20- Locality State/Prob/Terr/Counry(2)
    # 79 21- RC Name Abbrv(10)     22- RC Type(1)                23- Lines from #(0000)(4)
    # 94 24- Lines to #(0000)(4)   25- Switch(11)                26- Switch homing arrangement (SHA)(2)
    #  27 & up :: Misc stuff

    (lata, npa, nxx, coc_type, special_service_code, portable_indicator, ocn, prov, name, switch) =
      line.unpack("A5@32A3A3@43A3A4@55A@61A4@77a2a10@98A11")
    npa.strip!
    nxx.strip!
    prov.strip!
    prov = LergMethods::cleanProvince(prov)
    name.strip!

    if prov.length > 2 or name.blank?
      stats[:badline] += 1
      return nil
    end

    stats[:line] += 1
    item = self.where(npa: npa, nxx: nxx).take
    unless item
      stats[:created] += 1
      item = self.create(npa: npa, nxx: nxx, name: name, province: prov)
    else
      stats[:updated] += 1
      if item.province != prov
        item.province = prov
        stats[:updated_province] += 1
      end
      if item.name != name
        item.name     = name
        stats[:updated_name] += 1
      end
    end

    item.lata     = lata
    item.coc_type = coc_type
    item.ocn      = ocn
    item.switch   = switch
    item.portable_indicator = (portable_indicator == 'Y')
    item.special_service_code = special_service_code
    item.rate_center =  RateCenter.find_or_create_by(lerg6name: name, province: prov)
    item.save!

    return item
  end

  def self.import_file(file, stats = Hash.new(0))
    count = 0

    total = stats[:lerg6_total]

    File.readlines(file).each do |line|
      item = import_row(line, stats)
      count += 1
      if stats[:debug]
        if (count % 100) == 0
          print "\rImport at row: #{count}/#{total} created:#{stats[:created]} updated: #{stats[:updated]}"

        end
      end
    end
    puts "" if stats[:debug]

    return count
  end

  def attach_to(localexchange)
    LocalExchangesNorthAmericanExchange.connect_le_nae(localexchange, self)
  end

  def portable_indicator_YN
    return "Y" if portable_indicator
    return "N"
  end

  def province_screwy
    case province
    when 'QC'
      'PQ'
    else
      province
    end
  end

  def to_nnacl
    # NNACL fields position:
    # 01  1- NPA(3)                 2- NXX(3)                     3- Filler(7)
    # 13  4- COC Type(3)            5- Special Service Code(4)    6- Switch(11)
    # 31  7- Filler(3)              8- OperatingCompanyNumnber(4) 9- Lata(5)
    # 43 10- RC Name(30)           11- Filler(23)                12- State/Prov/Country(2)
    # 98 13- Portable Indicator(1) 12- Filler(1)
    [sprintf("%03u",npa), sprintf("%03u",nxx), "", coc_type, special_service_code,
     switch, "", ocn, lata, name, "0000000000", province_screwy,
     portable_indicator_YN, ""].pack("A3A3A7A3A4A11A3A4A5A30A23A2A1A1")
  end

end
