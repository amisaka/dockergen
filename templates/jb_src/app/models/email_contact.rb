class EmailContact < ApplicationRecord
  belongs_to :client

    def email_types
      return [ "Billing",
               "Technical",
               "Other",
               "COOP no email",
               "Legal no contact",
               "Print only"
             ]
    end
end
