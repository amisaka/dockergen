class Payment < Transaction
  self.inheritance_column = :_type_disabled


  validates :GLAccount, :numericality => {:only_integer => true, allow_nil: true}
  validates :amount, numericality: true
  belongs_to :transaction_method
  belongs_to :transaction_type
  belongs_to :gl_account

  has_and_belongs_to_many :invoices

  # The minimum date payments should be posted
  # All payments before this date are unposted because feature was unimplemented
  POSTED_PAYMENT_MIN_DATE = DateTime.new(2018, 05, 29, 00, 00, 00, "-04:00")

  validates :client, presence: true


  before_save {
      l = Rails.logger
      l.debug("Hook Payment.before_save fired")
      self.set_transaction_type
      self.checkAmount(); self.beforeSaveBalanceUpdates
  }
  after_save {
      l = Rails.logger
      l.debug("Hook Payment.after_save fired")
      self.afterSaveBalanceUpdates
  }
  after_destroy { self.updateBillingOnlyForDestroy }


  # Designed to limit search for unposted payments
  def self.searchPaymentsSince()
    [self::POSTED_PAYMENT_MIN_DATE, DateTime.now - 2.year].max
  end


# All deactivated in favor of refactored methods in transaction class.
#  def updateBilling
#    b = self.client.billing
#
#    # Handle missing billing object
#    if b.nil?
#        if self.client.isSupport?
#            #If no billing it is a support client
#            return nil
#        else
#            b = Billing.create!()
#            self.client.billing = b
#            self.client.save!
#        end
#    end
#
#    # I'm assuming transaction method is not necessary
#    # for changing in memory attributes, but
#    # for proper ACID in the future, should this
#    # evolve, I thought I'd put it here.
#    self.transaction do
#        b.last_payment_date = self.date
#        b.last_payment_amount = self.amount
#
#        b.balance = 0 if b.balance.nil?
#        b.balance -= (self.amount.abs)
#
#        b.save!
#    end
#  end
#
#
#    ########################################################################
#    # Updates the balance and balance audit columns.
#    # I put these here because these 5 operations need to be together
#    # Also very important to call self.save with false as arugment to
#    # avoid triggering another hook and get caught in loop
#    ########################################################################
#    def updateBalanceAndAudit(billingObject)
#        b = billingObject
#        self.before_balance = b.balance
#        self.after_balance = b.balance
#        self.save(false)
#    end

  def set_transaction_type
    if self.transaction_type == nil and self.transaction_method
      self.transaction_type = TransactionType.find(self.transaction_method.transaction_type_id)
    end
  end

  def checkAmount
    if(not self.transaction_type)
      return
    end

    case self.transaction_type.typename.downcase
    when ("Payment".downcase)
      self.amount = -(self.amount.abs)
    when ("CreditMemo".downcase)
      self.amount = -(self.amount.abs)
    when ("DebitMemo".downcase)
      self.amount.abs
    when ("adjustment")
      nil
    when ("summarycharge")
      nil
    when ("openingbalance")
      nil
    else
      $stderr.print "Unknown transaction type #{self.transaction_type.typename} in Payment.checkAmount (#{__FILE__}:#{__LINE__})"
    end

  #   case self.type.downcase
  #   when ("DebitMemo".downcase)
  #       self.amount = -(self.amount.abs)
  #   else
  #       self.amount = self.amount.abs
  #   end
  end

end
