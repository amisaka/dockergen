class InvoiceTemplate < ApplicationRecord

  has_many :btns

  scope :regular_billable, -> { where(irregular_invoice: false) }
  
end
