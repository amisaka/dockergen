class LineOfBusiness < ApplicationRecord

  validates :code,
    presence: true

  validates :description,
    presence: true

  has_many :client_line_of_businesses

  has_many :clients,
    through: :client_line_of_businesses

  has_many :services

  IMPORT_PRIMARY_KEY = :Pid

end
