    #                                              /  col-width/field #/spreadsheet Col name/ CDR field name
    #                                              /           /1-251  /    (for Steve)     /
CdrColumns = {
        'recordid' => 1,                            #*  48    1  A recordId (event #, host mac addr, timestamp)
        'serviceprovider' => 2,                     #*  30    2  B serviceProvider
        'calltype' => 3,                            #*  13    3  C callType
        'usernumber' => 4,                          #*  16    4  D userNumber
        'groupnumber' => 5,                         #*  16    5  E groupNumber
        'direction' => 6,                           #*  11    6  F direction
        'callingnumber' => 7,                       #* 161    7  G callingNumber
        'callingpresentationindicator' => 8,        #*  20    8  H callingPresentationIndicator
        'callednumber' => 9,                        #* 161    9  I calledNumber
        'starttime' => 10,                          #+  18   10  J startTime
        'usertimezone' => 11,                       #    8   11  K userTimeZone
        'answerindicator' => 12,                    #   19   12  L answerIndicator
        'answertime' => 13,#  'connectedtime',      #*  18   13  M answerTime
        'releasetime' => 14,# 'hanguptime',         #+  18   14  N releastTime
        'terminationcause' => 15,# 'sixteen',       #    3   15  O terminationCause
        'networktype' => 16, # 'type1',             #    4   16  P networkType
        'carrieridentificationcode' => 17, # 'unknown',  #4  17  Q carrierIdentificationCode
        'dialeddigits' => 18, # 'othernumber',      #  161   18  R dialedDigits
        'callcategory' => 19,# 'other',             #    8   19  S callCategory
        'networkcalltype' => 20, # 'ispl',          #    4   20  T networkCallType
        'networktranslatednumber' => 21, # 'number3',#  161  21  U networkTranslatedNumber
        'networktranslatedgroup' => 22, # 'field9',  #   32  22  V networkTranslatedGroup
        'releasingparty' => 23, # 'internal',        #    6  23  W releasingParty
        'route' => 24, # 'ipport_orig',              #   86  24  X route
        'networkcallid' => 25, # 'orig_sipid',       #  161  25  Y networkCallId
        'codec' => 26, # 'encoding',                 #   30  26  Z codec
        'accessdeviceaddress' => 27, # 'ip_dest',    #   80   27 AA accessDeviceAddress
        'accesscallid' => 28, # 'target_sipid',      #  161   28 AB accessCallId
        #'spare1' => 29,       # 'field2',           #        29 AC spare
        'failovercorrelationid' => 30, # 'field3',   #  161   30 AD failoverCorrelationId
        #'spare2' => 31,       # 'field4',           #        31 AE spare
        'group' => 32, # 'targetrealm',              #   30   32 AF group
        'department' => 33, # 'field5',              #  255   33 AG department
        'accountcode' => 34, # 'field6',             #   14   34 AH accountCode
        'authorizationcode' => 35, # 'field7',       #   14   35 AI authorizationCode
        'originalcallednumber' => 36, # 'number4',   #  161   36 AJ originalCalledNumber
        'originalcalledpresentationindicator' => 37, #   20   37 AK originalCalledPresentationIndicator
        'originalcalledreason' => 38,                #   40   38 AL originalCalledReason
        'redirectingnumber' => 39,                   #  161   39 AM redirectingNumber
        'redirectingpresentationindicator' => 40,    #   20   40 AN redirectingPresentationIndicator
        'redirectingreason' => 41, # 'conditions2',  #   40   41 AO redirectingReason
        'chargeindicator' => 42, # 'yesno',          #    1   42 AP chargeIndicator
        'typeofnetwork' => 43, # 'type4',            #    7   43 AQ typeOfNetwork
        'voiceportalcalling_invtime' => 44, # 'field8',#      44 AR voicePortalCalling.invocationTime
        'localcallid' => 45, # 'duration',           #*  40   45 AS localCallId
        'remotecallid' => 46, # 'duration2',         #   40   46 AT remoteCallId
        'callingpartycategory' => 47, # 'ordinary',  #   20   47 AU callingPartyCategory

#                CDR field name                     / col-width/field #/spreadsheet Col name
#                                                              /1-251  /    (for Steve)
        'instantconference_invtime' => 48,        #   18    48 AV
        'instantconference_callid' => 49,         #  161    49 AW
        'instantconference_to' => 50,             #  161    50 AX
        'instantconference_from' => 51,           #  161    51 AY
        'instantconference_conferenceid' => 52,   #  128    52 AZ
        'instantconference_role' => 53,           #   12    53 BA
        'instantconference_bridge' => 54,         #  128    54 BB
        'instantconference_owner' => 55,          #  161    55 BC
        'instantconference_ownerdn' => 56,        #   32    56 BD
        'instantconference_title' => 57,          #   80    57 BE
        'instantconference_projectcode' => 58,    #   40    58 BF
        'key' => 59,                              #  161    59 BG
        'creator' => 60,                          #   80    60 BH
        'originatornetwork' => 61,                #   80    61 BI
        'terminatornetwork' => 62,                #   80    62 BJ
        # 'accountCodePerCall.invocationTime' =>   #         63 BK

        'userid' => 121,                          #  161   121 DQ
        'otherpartyname' => 122,                  #   80   122 DR
        'otherpartynamepresentationindicator' => 123, #   20   123 DS
        'hoteling_group' => 129,                  #   30   129 DY
        'hoteling_userid' => 130,                 #  161   130 DZ
        'hoteling_usernumber' => 131,             #   15   131 EA
        'hoteling_groupnumber' => 132,            #   15   132 EB

        'trunkgroupname' => 135,                  #  255   135 EE
        'instantconference_recording_duration' => 136,  #        136 EF
        # 'instantGroupCall.invocationTime',     #        137 EG
        # 'instantGroupCall.pushToTalk',         #        138 EH
        # 'instantGroupCall.relatedCallId',      #        139 EI
        # 'CustomRingback.invocationTime',       #        140 EJ
        'clidpermitted' => 141,                   #    3   141 EK
        # 'autoHoldRetrieve.invocationTime',     #        142 EL
        # 'autoHoldRetrieve.action',             #        143 EM
        'accessnetworkinfo' => 144,               # 1024   144 EN
        # 'chargingFunctionAddress',             #  250   145 EO
        # 'chargeNumber',                        #   15   146 EP
        'relatedcallid' => 147,                   #   40   147 EQ
        'relatedcallidreason' => 148,             #   40   148 ER
        # 'transfer.invocationTime',             #        149 ES
        # 'transfer.result',                     #        150 ET
        'transfer_relatedcallid' => 151,          #        151 EU
        'transfer_type' => 152,                   #   20   152 EV
        'conference_starttime' => 153,             #   18  153 EW
        'conference_stoptime' => 154,             #   18   154 EX
        'conference_confid' => 155,               #   40   155 EY
        'conference_type' => 156,                 #   10   156 EZ
        # 'codecUsage',                          #   20   157 FA

        'faxmessaging' => 180,                    #    9   180 FX
        # 'twoStageDialingDigits',               #   30   181 FY
        'trunkgroupinfo' => 182,                  #  255   182 FZ
         'recalltype' => 183,                      #   20   183 GA

        'q850cause' => 188,                       #    3   188 GF
        'dialeddigitscontext' => 189,             #  161   189 GG
        'callednumbercontext' => 190,             #  161   190 GH
        'networktranslatednumbercontext' => 191,  #  161   191 GI
        'callingnumbercontext' => 192,            #  161   192 GJ
        'originalcallednumbercontext' => 193,     #  161   193 GK
        'redirectingnumbercontext' => 194,        #  161   194 GL

        'routingnumber' => 198,                   #  255   198 GP
        'originationmethod' => 199,               #   30   199 GQ
        # 'callParked.invocationTime',           #        200 GR
        'broadworksanywhere_relatedcallid' => 201,#   40   201 GS

        # 'outsideAccessCode',                   #    5   206 GX
        # 'primaryDeviceLinePort',               #  161   207 GY
        'calledassertedidentity' => 208,          #  161   208 GZ
        'calledassertedpresentationindicator' => 209,#   20   209 HA
        'sdp' => 210,                             # 1024   210 HB
        # 'mediaInitiatorFlag',                  #    1   211 HC
        # 'sdpOfferTimestamp',                   #   18   212 HD
        # 'sdbAnswerTimestamp',                  #   18   213 HE
        # 'earlyMediaSdp',                       # 1024   214 HF
        # 'earlyMediaInitiatorFlag',             # 1024   215 HG
        # 'bodyContentType',                     #  255   216 HH
        # 'bodyContentLength',                   #   10   217 HI
        # 'bodyContentDisposition',              #  255   218 HJ
        # 'bodyOriginator',                      #    1   219 HK
        # 'sipErrorCode',                        #    3   220 HL
        # 'otherInfoInPCV',                      #  128   221 HM  (or is it extrainfoinpvc?)
        # 'receivedCallingNumber',               #  161   222 HN
        # 'mediaSelection',                      #   20   223 HO
        # 'adviceOfCharge.aocType',              #    5   224 HP
        # 'adviceOfCharge.charge',               #   20   225 HQ
        # 'adviceOfCharge.currency',             #    4   226 HR
        # 'adviceOfCharge.time',                 #   18   227 HS
        # 'adviceOfCharge.sum',                  #   20   228 HT
        # 'adviceOfCharge.invocationTime',       #   18   229 HU
        # 'adviceOfCharge.result',               #    7   230 HV
        'ascalltype' => 231,                      #   11   231 HW

        'cbfauthorizationcode' => 242,            #   14   242 IH
        'callbridge_callbridgeresult' => 243,     #    7   243 II

        'prepaidstatus' => 246,                   #   22   246 IL
        'configurableclid' => 247                 #   20   247 IM
}
