# A Btn or BTN stands for Billing Telephone Number
# it is the entity that associates invoices to clients and is identified by a
# unique canonical telephone number. Typical clients, with a single montly
# invoice, will have a single BTN to which all monthly invoices are attached.
# A client could have multiple BTN with multiple series of invoices
# (departments, sites, satellite campus).
class Btn < ApplicationRecord

  include Statable
  include Searchable

  include Montbeau::Activatable

  IMPORT_PRIMARY_KEY = :billingtelephonenumber

  has_many :services
  has_many :invoices

  belongs_to :client
  belongs_to :invoice_template

  belongs_to :local_rate_plan,    :class_name => 'RatePlan'
  belongs_to :national_rate_plan, :class_name => 'RatePlan'
  belongs_to :world_rate_plan,    :class_name => 'RatePlan'
  belongs_to :rate_center

  belongs_to :billing_site, :class_name => 'Site', :foreign_key => "billing_site_id", autosave: true

  has_many :outgoing_cdrs, :class_name => 'Cdr', :foreign_key => 'callingbtn_id'
  has_many :incoming_cdrs, :class_name => 'Cdr', :foreign_key => 'calledbtn_id'

  has_many :outgoing_calls, :class_name => 'Call', :through => :outgoing_cdrs, :source => :call
  has_many :incoming_calls, :class_name => 'Call', :through => :incoming_cdrs, :source => :call



  has_many :billable_services, through: :client

  alias_attribute :activation_date, :activated_at
  alias_attribute :termination_date, :deactivated_at

  scope :active, lambda { |*args|
    ended  = args.first   || Time.now.beginning_of_day
    where("btns.deactivated_at IS NULL OR btns.deactivated_at >= ? ", ended)
  }

  scope :active_first, -> { order("(CASE WHEN deactivated_at IS NULL THEN 0 ELSE 1 END) ASC").order(:deactivated_at) }
  scope :invoice_enabled, -> { where(invoicing_enabled: true) }

  scope :oldest, lambda { order(:activated_at) }

  scope :regular_billable, -> { joins(:invoice_template).merge(InvoiceTemplate.regular_billable) }

  # Ensure billable service is added to this BTN-billed invoice
  def add_billable_service(invoice, billable_service)
    raise "Invoice does not belong to this BTN" unless self.invoices.include?(invoice)
    invoice.add_billable_service(billable_service)
  end

  # Ensure billable service is removed from this BTN-billed invoice
  def remove_billable_service(invoice, billable_service)
    raise "Invoice does not belong to this BTN" unless self.invoices.include?(invoice)
    invoice.remove_billable_service(billable_service)
  end

  # needs to get refactored.
  def self.errorlog(msg)
    $stderr.puts msg
  end
  def self.maybelog(msg)
    errorlog(msg) if ENV['IMPORT_DEBUG']
  end

  def self.btn_cache_key(number)
    "btn_by_number:#{number}"
  end

  btn_cached = false

  def self.preload_btns!
    includes(:client).find_each do |btn|
      Rails.cache.write btn_cache_key(btn.billingtelephonenumber), btn
    end
    @btn_cached = true
  end
  def self.uncache_btns!
    @btn_cached = false
  end

  def self.get_by_billingtelephonenumber(number, nukenil=true)
    number = RateCenter.canonify_number(number)
    thing = Rails.cache.fetch btn_cache_key(number) do
      #errorlog("finding btn: #{number}")
      unless @btn_cached
        active.find_by_billingtelephonenumber(number)
      end
    end
    unless thing
      if nukenil
        #errorlog("btn was nil, nuking: #{number}")
        Rails.cache.delete(btn_cache_key(number))
      end
    end
    return thing
  end

  def self.canonical_btn(btnname)
    btnname.gsub(/[\(\)\- ]/, '')
  end

  def self.find_or_make(btn, client = nil)
    # remove () and - and spaces from phone numbers.
    cbtn = canonical_btn(btn)
    cbtn = RateCenter.canonify_number(btn)
    b1 = get_by_billingtelephonenumber(cbtn)
    unless b1
      # must fetch directly to bypass canoncalization, caching, etc.
      b1 = Btn.where(billingtelephonenumber: btn).take
      if b1
        b1.billingtelephonenumber = cbtn
        b1.save!
      end
    end

    unless b1
      b1 = create(:billingtelephonenumber => cbtn)
      #errorlog("Created new BTN for #{btn} and #{client} as #{b1}\##{b1.id}")
      b1.save!
      if client
        b1.client = client
      end
    end
    b1
  end

  def self.lookup_vdid(number, onday = DateTime.now)
    number = Service.vdidify(number)
    return get_by_billingtelephonenumber(number, false)
  end

  def self.lastresort
    @btnlastresort ||= Btn.find_by_id($BtnOfLastResortId)
  end

  def name
    billingtelephonenumber
  end

  def to_s
    "btn{#{billingtelephonenumber}}"
  end

  def active?(now = Time.now)
    return true   if deactivated_at.blank?
    return true   if activated_at.blank?
    return false  if activated_at > now
    return false  if deactivated_at < now
    return true
  end
  def terminated?(now = Time.now)
    !active?(now)
  end

  def active!
    self.deactivated_at = nil
    save!
  end
  def cancel!(now = Time.now)
    terminated!(now)
  end
  def terminated!(now = Time.now)
    begin
      self.deactivated_at = now
      save!
    rescue ActiveRecord::RecordNotUnique
      attempts = 10
      saved = false
      while !saved and attempts > 0
        begin
          save
          saved = true
        rescue ActiveRecord::RecordNotUnique
          saved = false
          attempts -= 1
          self.deactivated_at = now + attempts + (rand(10).days - 5.days)
          puts "btn#{id} deactivated_at: #{deactivated_at} attempts#{attempts}"
        end
      end
    end
  end

  def calc_rate_center
    self.rate_center = RateCenter.lookup_by_partial(billingtelephonenumber)
    save!
    self.rate_center
  end

  def find_rate_center
    self.rate_center || calc_rate_center
  end

  def invoice_for_date(date)
    invoice_service_for(date.year, date.month)
  end

  def find_billing_site
    billing_site || client.try(:billing_site)
  end

  def validate_invoice_relations(invoice)
    unless invoice.btn
      invoice.btn = self
    end
    unless invoice.client
      invoice.client = self.client
    end
    invoice.invoice_run ||= InvoiceRun.invoice_run_for(invoice.service_start.year, invoice.service_start.month)
    return unless invoice.client
    invoice.save!
  end

  def invoice_for(year, month)
    invoice = self.invoices.by_year_month(year, month).first
    unless invoice
      invoice = Invoice.new_charges_invoice(self, year, month)
    end

    validate_invoice_relations(invoice)
    return invoice
  end

  def invoice_service_for(year, month)
    invoice = self.invoices.by_service_year_month(year, month).first
    unless invoice
      invoice = Invoice.new_services_invoice(self, year, month)
    end
    validate_invoice_relations(invoice)
    return invoice
  end

  def incoming_call_count(today = Time.now, hour = nil, recount = false)
    # this is synthesized from the count of calls per phone (aka services).

    # it would be nice to do this with a database query on callstats.
    # and maybe that's the right thing to do if recount = false
    # but for now, we rely on lazy creation of the callstats records
    # so we have to do it via ruby.

    sum = 0
    services.find_each { |ph|
      sum += ph.incoming_call_count(today, hour, recount)
    }
    return sum
  end

  def outgoing_call_count(today = Time.now, hour = nil, recount = false)
    # this is synthesized from the count of calls per phone (aka services).

    # it would be nice to do this with a database query on callstats.
    # and maybe that's the right thing to do if recount = false
    # but for now, we rely on lazy creation of the callstats records
    # so we have to do it via ruby.

    sum = 0
    services.find_each { |ph|
      sum += ph.outgoing_call_count(today, hour, recount)
    }
    return sum
  end

  def local_plan
    local_rate_plan
  end
  def national_plan
    national_rate_plan
  end
  def world_plan
    world_rate_plan
  end

  # note returns a site object that was updated, not the btn found.
  def self.import_billing_address(lineno, row, verbose = true)
    (account_status, account_num, name, primary_address, billing_address,
     billing_add1, billing_add2, billing_city, billing_prov, billing_pcode) = row
    btn1 = get_by_billingtelephonenumber(account_num, true)
    return nil unless btn1
    return nil unless btn1.client

    site = btn1.find_billing_site

    unless site
      btn1.client.billing_site = site = Site.new
      site.save!
      byebug
    end
    obuilding = site.try(:building).try(:dup)
    osite     = site.dup

    site.address1 = billing_add1.try(:chomp).try(:strip)
    site.address2 = billing_add2.try(:chomp).try(:strip)
    site.city     = billing_city.try(:chomp).try(:strip)
    site.province = billing_prov.try(:chomp).try(:strip)
    site.postalcode= billing_pcode.try(:chomp).try(:strip)
    site.building.try(:sanitize_postalcode)

    same = false
    if obuilding
      same = (site.address1 == obuilding.address1) ||
             (site.address2 == obuilding.address2) ||
             (site.city     == obuilding.city) ||
             (site.province == obuilding.province) ||
             (site.postalcode==obuilding.postalcode)
    end

    site.save(validate: false)

    unless same
      if verbose
        puts "Updating #{btn1.client} #{btn1} (lineno=#{lineno})"
        puts "         from: #{obuilding.address1}/#{obuilding.address2}/#{obuilding.city}/#{obuilding.postalcode}"
        puts "           to: #{site.address1}/#{site.address2}/#{site.city}/#{site.postalcode}"
      end
    end
    site
  end

  def invoice_totals(uptodate, exclude_inv = nil)
    total = 0
    invoices.where(["invoice_date <= ?", uptodate]).each { |inv|
      if inv != exclude_inv and !inv.amount_invoiced.blank?
        total += inv.amount_invoiced
      end
    }
    return total
  end

end
