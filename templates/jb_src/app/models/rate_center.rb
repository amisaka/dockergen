require "i18n"

# https://en.wikipedia.org/wiki/
class RateCenter < ApplicationRecord

  has_many :north_american_exchanges
  has_many :local_exchanges
  has_many :local_exchanges_north_american_exchanges
  has_many :cdrs, :class_name => 'Cdr', :foreign_key => 'called_rate_center_id'
  has_many :rates
  has_many :btns
  has_many :services # Phone class has been renamed to Service - t13331

  scope :active, -> { where(deleted_at: nil) }

  def name
    "rc#{id}"
  end

  def to_s
    sprintf("%s, %s, %s [%u]", lerg6name, province, country, id)
  end

  def active?
    deleted_at.blank?
  end
  def inactive?(now = Time.now)
    !active? && deleted_at < now
  end

  # a default rate center has a bogus country code.
  def default?
    country_code == 999
  end

  def self.default_rate_center
    @@default_rc ||= find($DefaultRateCenter)
  end

  def self.tollfree
    @rc800 ||= RateCenter.find(999991)
  end

  # entries which are not services by NPA/NXX codes.
  scope :country_codes, -> { where(lerg6name: nil).where(province: nil) }

  def self.complete_short_number(phonenumber, callingnumber)
  end

  def self.canonify_number(phonenumber)
    phonenumber = phonenumber.clone
    phonenumber = Service.phonecanonify(phonenumber)

    # get rid of internationally dialing prefix.
    case
    when phonenumber[0..2] == '011'
      phonenumber[0..2]='+'

    when (phonenumber.length == 11 and phonenumber[0]=='1')
      phonenumber = '+' + phonenumber

    when phonenumber.length == 10
      phonenumber = '+1' + phonenumber

    end

    phonenumber
  end

  def self.lookup_by_partial(phonenumber, callingnumber = '')
    # make copy so that we do not modify caller's string.
    return nil unless phonenumber

    phonenumber = canonify_number(phonenumber)
    c1 = phonenumber[1..1].to_i
    c2 = phonenumber[1..2].to_i
    c3 = phonenumber[1..3].to_i

    rc = active.country_codes.where(country_code: [c1,c2,c3]).where(city_code: ['',nil]).order("country_code DESC").take

    return nil unless rc

    # now that we found the right country code, we can remove those digits, and
    # look for a city_code.
    case
    when rc.country_code == c1
      cc = c1
      phonenumber[0..1] = ''

    when rc.country_code == c2
      cc = c2
      phonenumber[0..2] = ''

    when rc.country_code == c3
      cc = c3
      phonenumber[0..3] = ''
    end

    city0 = ''
    city1 = phonenumber[0..0]
    city2 = phonenumber[0..1]
    city3 = phonenumber[0..2]
    city4 = phonenumber[0..3]
    city5 = phonenumber[0..4]

    rc2 = active.country_codes.where(country_code: cc).where(city_code: [nil, city0, city1,city2,city3,city4,city5]).order("length(city_code) DESC").take

    if rc2
      rc = rc2
    end
    unless rc.city_code.blank?
      len = rc.city_code.length
      phonenumber[0..(len-1)] = ''
    end

    if rc.north_american_lookup?
      na = NorthAmericanExchange.findbyphone(phonenumber)
      if na
        rc = na.rate_center
      else
        # if we do not find a new exchange, the wipe out rate center.
        rc = nil
      end
    end
    rc
  end

  def self.find_or_create(lerg6name, prov)
    return active.find_or_create_by(country_code: 1, lerg6name: lerg6name, province: prov)
  end

  def self.find_or_create_name_name(province, lerg6name, localname)
    rc = active.where(country_code: 1, lerg6name: lerg6name, province: province).take
    unless rc
      rc = active.where(country_code: 1, lerg6name: localname, province: province).take
      unless rc
        rc = create(country_code: 1, province: province)
        puts "create #{rc.id} by #{province}"
      else
        puts "found #{rc.id} by #{localname}/#{province}"
      end
    else
      puts "found #{rc.id} by #{lerg6name}/#{province}"
    end
    rc.lerg6name = lerg6name
    rc.localnames << localname
    rc.save!
    puts "Returned RC: #{province} #{lerg6name} / #{localname} / #{rc.id}"
    return rc
  end

  def self.single_localname(localname)
    where(["? = ANY(localnames)",localname])
  end

  def self.find_by_localname(localname, prov)
    active.where(country_code: 1, province: prov).single_localname(localname)
  end

  def self.find_by_localname_permuted(localname, prov, stats)
    p = active.where(country_code: 1, province: prov)
    rc1 = p.single_localname(localname).first
    if rc1
      stats[:plain_rc] += 1
      return rc1
    end

    nocasename = localname.upcase
    rc1 = p.single_localname(nocasename).first
    if rc1
      stats[:downcase_rc] += 1
      return rc1
    end

    noaccentname = I18n.transliterate(nocasename)
    rc1 = p.single_localname(noaccentname).first
    if rc1
      stats[:accent_rc] += 1
      return rc1
    end

    nospacename = nocasename.gsub(/\W+/, '')
    rc1 = p.single_localname(nospacename).first
    if rc1
      stats[:nospace_rc] += 1
      return rc1
    end

    rc1 = p.where("? = regexp_replace(localnames[0], '\\W+', '', 'g') OR ? = regexp_replace(localnames[1], '\\W+', '', 'g')",
                  nospacename, nospacename).first
    if rc1
      stats[:nospace_nospace_rc] += 1
      return rc1
    end

    rc1 = p.where("regexp_replace(lerg6name, '\\W+', '', 'g') = ?", nospacename).first
    if rc1
      stats[:space_permute_rc] += 1
      return rc1
    end

    return nil

    # at this point, we have not found anything, so we could make a new
    # Rate Center with the localname provided, and a nil lerg6name,
    # but we'll do lavenshein between rate centers and local_exchanges.
    #stats[:new_rc_created] += 1
    #rc1 = create(localname: localname, province: prov)
    #return rc1
  end

  def self.extend_footprint_inexact1(stats = Hash.new(0))
    where(:lerg6name => nil).find_each { |rc|
      lerg6name = rc.localname.gsub(/\W+/, '')


    rc1 = p.where("regexp_replace(lerg6name, '\\W+', '', 'g') = ?", lname).first
    }
  end

  def match(rc)
    self.id == rc.id
  end

  def find_exact_le
    count = 0
    LocalExchange.where(rate_center_id: nil, province: province, name: localname).find_each {|le|
      le.rate_center = self
      le.save!
      count += 1
    }
    count
  end

  def find_exact_nae
    count = 0
    NorthAmericanExchange.where(rate_center_id: nil, province: province, name: lerg6name).find_each {|nae|
      nae.rate_center = self
      nae.save!
      count += 1
    }
    count
  end

  def find_matching_nae
    count = 0
    unless lerg6name.blank?
      count = find_exact_nae
      return if count > 0
    end

    lname = localname.gsub(/\W+/, '')
    NorthAmericanExchange.where(rate_center_id: nil).where("regexp_replace(name, '\\W+', '', 'g') = ?", lname).find_each {|nae|
      nae.rate_center = self
      nae.save!
      count =+ 1
    }
    return if count > 0

    # if still not found, then then match using shortened forms
    forms = shortened_forms
    NorthAmericanExchange.where(rate_center_id: nil).where("regexp_replace(name, '\\W+', '', 'g') = ?", lname).find_each {|nae|
      nae.rate_center = self
      nae.save!
      count =+ 1
    }
    return if count > 0
  end

  def shortened_forms
    @shortened_forms ||= ShortNames.permute(localname)
  end

  def connect_to_nae(le, stats = Hash.new(0))
    north_american_exchanges.find_each {|nae|
      nae.attach_to(le)
      stats[:new_nae] += 1
      #puts "#{le.id} #{le.province} #{le.name} #{nae.id} #{nae.npa} #{nae.nxx}"
    }
  end

  # maintenance and importing.
  # update from calling_codes/calling_codes.sql
  def self.update_from_calling_codes
    connection.execute "DROP TABLE IF EXISTS calling_codes;"
    connection.execute File.read(File.join(Rails.root, "db/calling_codes/calling_codes.sql"))

    n = Time.now

    connection.select_all("select * from calling_codes").each { |code|
      # => {"prefix"=>"1403", "territory"=>"CA", "comment"=>"Alberta, Canada", "mobile"=>"0"}

      # if country code==1, then skip.  NorthAmericanExchange table will handle it.
      next unless code["prefix"]         # protect against nil values.
      next if code["prefix"][0] == '1'

      rc = find_or_create_by(deleted_at: nil, country_code: code["prefix"], lerg6name: nil,
                             isocountry: code["territory"], country: code["comment"])
      rc.updated_at = n
      rc.mobile = (code["mobile"] == "1")
      rc.save!
    }

    # look for things to drop, which would be indicated by having a lerg6name:nil, and
    # an updated_at < n.
    active.where(lerg6name: nil).where.not(country_code: 1).where(["updated_at < ?", n]).

    connection.execute "DROP TABLE IF EXISTS calling_codes;"

  end

  def includes_exchange?(carrier)
    self.local_exchanges.where(carrierplan: carrier).count > 0
  end

  def calltype
    return :tollfree_recv  if tollfree?

    case isocountry
    when 'US'
      :usa_calls
    when 'CA'
      :canada_calls
    when 'AS','AI','AG','BS','BB','BM','VG','GP','DM','DO'
      :caribbean_calls
    when 'GD','GU','JM','MS','MP','PR','KN','LC','VC','SX'
      :caribbean_calls
    when 'TT','TC','VI'
      :caribbean_calls
    else
      :intl
    end
  end

  def city_place
    if localnames.first
      localnames.first
    elsif country_code != 1
      country
    else
      lerg6name
    end
  end

  def lca_item
    sprintf("888,%s;", lerg6name)
  end
  def lca_block(key)
    "<LCA>\n" +
      lca_item + key +"_LCA_GROUP;\n" +
    "</LCA>\n"
  end

end
