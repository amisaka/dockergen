class ProductCategory < ApplicationRecord
    IMPORT_MAPPING = {
        :code => "Code",
        :description => "Description",
        :active => "Active", # Careful, "0" converts to true
        :bc_product_billing_category_pid => "BCProductBillingCategoryPID",
        :product_category_pid => "ProductCategoryPID",
    }

    IMPORT_PRIMARY_KEY = :product_category_pid

    def product_category_label
      if self.description != " "
        return "#{self.code} : #{self.description}"
      else
        return "#{self.code}"
      end
    end

end
