class UsageType < ApplicationRecord


    IMPORT_MAPPING = {
        :name   => "UsageType",
        :usage_type_pid => "UsageTypePID"
    }

    IMPORT_PRIMARY_KEY = :usage_type_pid
end
