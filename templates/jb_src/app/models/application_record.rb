# require 'active_record/undeletable'

# @author Maxime Gauthier {mailto:maxime@credil.org <maxime@credil.org>}
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Generic behavior to import data from CSV formatted for ActiveRecord
  # some models have specific behaviors overriding these methods
  extend ::Montbeau::Import::Importable
  extend ::Montbeau::Export::Exportable

  # This module prevents any records from being deleted, and instead places
  # them outside of the default scope. They can be accessed with the `deleted` scope
  include ActiveRecord::Undeletable

  # Debugging utility methods
  include ::Montbeau::Debug

  def self.reset_all_sequences!
    ActiveRecord::Base.connection.tables.each do |t|
      ActiveRecord::Base.connection.reset_pk_sequence!(t)
    end
  end
end
