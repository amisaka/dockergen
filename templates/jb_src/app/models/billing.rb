# A billing represents the current status of a client's account.
# It should be renamed to "AccountStatus" or "Account"
class Billing < ApplicationRecord

  belongs_to :client

  validates :client,
    presence: true

  # Updates
    #last_payment_date
    #last_payment_amount
    #last_statement_amount
    #last_statement_date
    #last_usage_date
  # It is not clear if this method follows the buisness rules of how the data is being used.  Despite having an invoices table, it is not presented in the GUI.  Miranda does not think the invoices table is used.
  # Data imported into biling on 2018/01/15 was built from payments table, so any discrepencies between billing and payments is human error
  # This method was written when biling data was reported in error for Hart Stores but I had no knowledge of another biling import to be done.  Thus it may not come into use in the future.
    # -- jlam@credil.org 2018/01/15
  def updateBilling
    # Find the last payment
    c = self.client
    p = c.payments.order(date: :desc).limit(1).first
    if not p.nil?
        self.last_payment_date = p.date
        self.last_payment_amount = p.amount
    end

    # Find the last invoice
    i = c.invoices.order(created_at: :desc).limit(1).first
    if not i.nil?
        self.last_statement_date = i.created_at
        self.last_statement_amount = i.amount
    end

    # Find the last date of the previous month
    self.last_usage_date = Date.today - Date.today.day

    self.save!
  end

end
