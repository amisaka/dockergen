# Not used yet as of 2018/07/06
class CallSummaryTable


    attr_accessor :categorizer
    attr_reader :categorized_calls
    attr_reader :total_call_count
    attr_reader :total_duration
    attr_reader :total_cost
    attr_reader :duration_by_category

    def initialize()
        @categorized_calls = {}

        @duration_by_category = {}
        @cost_by_category = {}
        @count_by_category = {}

        @total_call_count = @total_duration = @total_cost = 0
    end

    def categorize_call(call)
        raise "Categorizer not set" if @categorizer.nil?

        category = @categorizer.call(call)

        return if category.nil?

        addCallToCategory(category, call)

        return @categorized_calls

    end

    def addCallToCategory(category, call)
        @categorized_calls[category] = [] if @categorized_calls[category].nil?
        @categorized_calls[category] << call

        updateTotals(category, call)
    end

    def updateTotals(category, call)
        if @count_by_category[category].nil?
            @count_by_category[category]    = 0
            @cost_by_category[category]     = 0
            @duration_by_category[category] = 0
        end

        @count_by_category[category]    += 1
        @cost_by_category[category]     += c.cost
        @duration_by_category[category] += c.duration

        @total_call_count   += 1
        @total_cost         += c.cost
        @total_duration     += c.duration
    end






end
