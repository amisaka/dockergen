class DIDSupplier < ApplicationRecord
  has_many :services
  has_many :datasources

  def self.make_supplier(sym, attrs)
    thing = where(attrs).take
    unless thing
      thing = create(attrs)
    end
    SystemVariable.setnumber(sym, thing.id)
  end

  def self.find_by_symbol(sym)
    num = SystemVariable.number(sym)
    return nil unless num
    find(num)
  end

  IMPORT_MAPPING = {
      :name => "Supplier",
      :pid  => "SupplierPID"
  }

  IMPORT_PRIMARY_KEY = :pid

end
