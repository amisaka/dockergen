# this is mostly a through table
class LocalExchangesNorthAmericanExchange < ApplicationRecord

  belongs_to :north_american_exchange, :foreign_key => 'north_american_exchanges_id'
  belongs_to :local_exchange, :foreign_key => 'local_exchanges_id'
  belongs_to :rate_center

  def self.unmatched_northamerican_exchanges
  end

  # this initializes rate centers from the LENAE table. Typically this
  # is only done during transition of an existing system.
  def self.create_rate_centers
    count = 0
    LocalExchangesNorthAmericanExchange.select('local_exchanges.province AS province, local_exchanges.name AS localname, north_american_exchanges.name AS lerg6name').joins(:local_exchange,:north_american_exchange).distinct("north_american_exchanges.name,local_exchanges.localname").each { |rc1|
      rc = RateCenter.find_or_create_name_name(rc1.province, rc1.lerg6name, rc1.localname)
      count += 1
      rc.find_exact_nae
      rc.find_exact_le
    }
    puts "Count #{count} processed"
  end

  def self.connect_le_nae_create(le, nae, rc = nil, n = Time.now)
    created = false
    lenae = self.where(local_exchange: le,
                       north_american_exchange: nae).take
    unless lenae
      lenae = new(local_exchange: le,
                  north_american_exchange: nae)
      created = true
    end

    unless rc
      if nae.rate_center
        rc = nae.rate_center
      end
    end
    lenae.rate_center = rc
    lenae.updated_at = n
    lenae.save!

    le.mapped_at  = n
    nae.mapped_at = n
    le.save!
    nae.save!

    return lenae, rc, created
  end

  def self.connect_le_nae(le, nae, rc = nil, n = Time.now)
    lenae, rc, created = connect_le_nae_create(le, nae, rc, n)
    if created
      print "\rCreated new lenae #{le.id} #{nae.id} with rc=#{rc.try(:id)}        "
      return lenae
    else
      return nil
    end
  end

  def self.report_match_rate
    lTotal      = LocalExchange.count(:all)
    naMatches   = LocalExchangesNorthAmericanExchange.count('local_exchanges_id', distinct: true)

    puts "#{ naMatches } of #{ lTotal } records found (#{ naMatches * 100/ lTotal }% - #{ lTotal - naMatches} left)"
    puts ""
  end

  # this should be run
  #  run to update the LENAE table from from the LE->RC<-NAE relationship
  #
  def self.update_from_rate_center(debug = true)
    nae_count = 0
    le_count  = 0
    rc_count  = 0
    tot_count = 0
    rc_total = RateCenter.active.count
    n = Time.now
    ototal = self.count
    RateCenter.active.find_each { |rc|
      nlneeded = false
      rc_count += 1

      nae_count = 0; nae_total = rc.north_american_exchanges.active.count
      le_total  = rc.local_exchanges.active.count

      rc.north_american_exchanges.active.find_each { |nae|
        nae_count += 1

        le_count  = 0;
        rc.local_exchanges.active.find_each { |le|
          le_count += 1

          if debug
            if (tot_count % 100) == 0
              print sprintf("\r%05d/%05d rc.id=%05u [%05d/%05d] le=%03d/%03d nae=%03d/%03d %16s",
                            tot_count, ototal,
                            rc.id, rc_count, rc_total, le_count, le_total,
                            nae_count, nae_total, rc.lerg6name)
              nlneeded = true
            end
          end
          connect_le_nae(le, nae, rc)
          tot_count += 1
        }
      }
      puts "\n" if (debug and nlneeded)
    }

    # now delete anything older than this.
    total = self.count
    old   = self.where(["updated_at < ?", n]).count
    puts "Started with #{ototal}, now have #{total}, processed #{tot_count}. Now deleting #{old} items"
    #self.where(["updated_at < ?", n]).delete_all
  end

  def simplename
    "lenae#{id}"
  end
end
