require 'devise'

class Portal::User < ApplicationRecord
  include Devise
  # self.primary_key = "id"
  # self.table_name  = "public.auth_user" if postgresql?

  has_one :userprofile, class_name: Portal::UserProfile, foreign_key: 'user_id'

end
