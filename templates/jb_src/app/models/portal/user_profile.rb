require 'devise'

class Portal::UserProfile < ApplicationRecord
  include Devise
  # self.primary_key = "id"
  # if postgresql?
  #   self.table_name  = "public.portal_userprofile"
  # else
  #   self.table_name  = "portal_userprofile"
  # end

  belongs_to :user, class_name: Portal::User, foreign_key: 'user_id'

end
