require 'active_support/concern'

module Searchable
  extend ActiveSupport::Concern

  included do

  end

  class_methods do
    def search(query)
      return paginate_array([]) if query.blank?
      results = class_name.constantize.new(query).results
      paginate_if_array(results)
    end

    private

      def paginate_if_array(results)
        if results.kind_of?(Array)
          paginate_array(results)
        else
          results
        end
      end

      def class_name
        name + 'Search'
      end

      def paginate_array(ary)
        Kaminari.paginate_array(ary)
      end

  end
end
