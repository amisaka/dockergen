class DailyStatsPeriod < StatsPeriod

  # returns a scope for type which is below this kind
  def contained_period_types
    periods = StatsPeriod.where(:type => 'HourlyStatsPeriod').where('stats_periods.hour IS NOT NULL')
  end

end

