#
#   t.integer  "rate_plan_id"
#   t.integer  "rate_center_id"
#   t.float    "cost"
#   t.integer  "billing_increment"
#   t.integer  "minimum_call_length"
#

# duck types below.
class FixedRate
  def minimum_call_length
    30
  end
  def billing_increment
    6
  end
  def cost
    # guestimate
    0.075
  end

  def duration_round(duration)
    return 0  if duration < 1

    # round up to 30s.
    return 30 if duration < minimum_call_length

    # round *up* to nearest 6s.
    base = ((duration + billing_increment - 1) / billing_increment).floor
    return base * billing_increment
  end

  def base_price(duration)
    # cost is in dollars/minute.
    # multiply first to avoid underflow in float arithmetic
    # then round to pennies.
    ((duration * cost) / 60).round(3)
  end
end

class Rate < ApplicationRecord
  belongs_to :rate_plan
  belongs_to :rate_center

  def self.import_row_country(rate_plan, stats, row)
    (dest_country_code,
     dest_city_prefix,
     usage_group, rate_type,
     cost, per_usage, rate_code, volumn_scheme,
     rounding_scheme, max_charge) = row

    dest_city_prefix ||= ''
    rc = RateCenter.where(country_code: dest_country_code,
                          city_code: dest_city_prefix).take

    # work around because some centers loaded with NULL city_code.
    if !rc and dest_city_prefix.blank?
      rc = RateCenter.where(country_code: dest_country_code,
                            city_code: nil).take

      rc.city_code = '' if rc
      stats[:rc_found_with_nil] += 1
    end

    unless rc
      rc = RateCenter.create(country_code: dest_country_code,
                             city_code: dest_city_prefix)
      stats[:rc_created] += 1
      rc.save
    end
    rate = rate_plan.rates.where(rate_center_id: rc.id).take
    unless rate
      rate = self.create(rate_center: rc,
                         rate_plan: rate_plan)
      stats[:rate_created] += 1
    end
    rate.cost = cost

    (minimum_call_length,billing_increment) = rounding_scheme.split(/\//)
    rate.billing_increment   = billing_increment
    rate.minimum_call_length = minimum_call_length

    rate.save!
    return rate
  end

  def self.import_row_areacode(rate_plan, stats, row)
    (dest_area_code, dest_exchange,
     dest_block, usage_group, rate_type,
     cost, per_usage, rate_code, volumn_scheme,
     rounding_scheme, max_charge) = row


    nae = NorthAmericanExchange.where(npa: dest_area_code.to_i,
                                      nxx: dest_exchange.to_i).take

    unless nae
      stats[:area_code_not_found] += 1
      stats[:lines_ignored] += 1
      return nil
    end

    rc = nae.rate_center

    unless rc
      stats[:rate_center_not_found] += 1
      stats[:lines_ignored] += 1
      return nil
    end

    rate = rate_plan.rates.where(rate_center_id: rc.id).take
    unless rate
      rate = self.create(rate_center: rc,
                         rate_plan: rate_plan)
      stats[:rate_created] += 1
    else
      stats[:rate_updated] += 1
    end
    rate.cost = cost

    (minimum_call_length,billing_increment) = rounding_scheme.split(/\//)
    rate.billing_increment   = billing_increment
    rate.minimum_call_length = minimum_call_length

    rate.save!
    return rate
  end

  def self.import_row(row, rate_plan, stats = Hash.new(0))
    (dest_country_code,
     dest_city_prefix,
     usage_group, rate_type,
     cost, per_usage, rate_code, volumn_scheme,
     rounding_scheme, max_charge) = row

    if dest_country_code =~ /Destination Country/
      stats[:header_lines] += 1
      stats[:country_file] = true
      stats[:north_american_file] = false
     return nil
    end
    if dest_country_code =~ /Destination Area Code/
      stats[:header_lines] += 1
      stats[:country_file] = false
      stats[:north_american_file] = true
     return nil
    end
    stats[:lines] += 1

    if stats[:country_file]
      return import_row_country(rate_plan, stats, row)
    end
    if stats[:north_american_file]
      return import_row_areacode(rate_plan, stats, row)
    end

  end

  def self.import_file(file, rate_plan, stats = Hash.new(0))
    handle = File.open(file, "rb:bom|utf-8")
    count = 0

    unless handle
      $stderr.puts "Can not open #{file}: $!"
      return false
    end

    CSV.parse(handle) do |row|
      if((count % 100) == 0)
        $stderr.print "Processed: #{count}    \r"
      end
      count += 1
      import_row(row, rate_plan, stats)
    end
    $stderr.puts "\n"
    return true

  end

  def name
    "rate#{id}"
  end

  def duration_round(duration)
    return 0  if duration < 1

    # round up to 30s.
    return 30 if duration < minimum_call_length

    # round *up* to nearest 6s.
    base = ((duration + billing_increment - 1) / billing_increment).floor
    return base * billing_increment
  end

  def base_price(duration)
    # cost is in dollars/minute.
    # multiply first to avoid underflow in float arithmetic
    # then round to pennies.
    ((duration * self.cost) / 60).round(3)
  end

  def self.tollrate_plan
    @fixed ||= FixedRate.new
  end

end
