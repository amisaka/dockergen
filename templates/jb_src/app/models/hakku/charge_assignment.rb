module Hakku
  class ChargeAssignment < HakkuRecord
    # In March 2018, the Service model was renamed Product.  This code here was not touched as part of that rename effort as I don't thihk this code has been in use

    self.primary_key = "ChargeAssignmentPID"   # accessed as "id"
    self.table_name  = "ChargeAssignment"

    scope :active, -> { where(:Active => true) }
    scope :inactive, -> { where(:Active => false) }

    alias_attribute :active, :Active

    belongs_to :customer, class_name: 'Hakku::Customer',
      foreign_key: 'CustomerPID'

    belongs_to :product,  class_name: 'Hakku::Product',
      foreign_key: 'ProductPID'

    # note pointed to Hakka as this data is not changing.
    belongs_to :recurrence_info, class_name: 'Hakka::RecurrenceInfo',
      foreign_key: 'RecurrenceInfoPID'

    belongs_to :recurrence_type, class_name: 'Hakka::RecurrenceType',
      foreign_key: 'RecurrenceTypePID'

    belongs_to :service, class_name: 'Hakku::Service',
      foreign_key: 'ServicePID'

    belongs_to :department, class_name: 'Hakku::Department',
      foreign_key: "DepartmentPID"

    has_one :billable_service, class_name: '::BillableService', foreign_key: 'foreign_pid'

    def make_billable_service_hash
      client = customer.associated_client

      if product
        $HAKKUSTATS[:service_updated] += 1
      end

      if department
        $HAKKUSTATS[:department_updated] += 1
      end

      hash = {
        foreign_pid: self[:ChargeAssignmentPID],
        totalprice: self[:TotalPrice],
        unitprice: self[:UnitPrice],
        qty: self[:Quantity],
        service: (product.associated_service if product),
        service_name: (service[:DisplayText] if service),
        site: (department.associated_site if department),
        # self[:UnitPrice],1
        # special ProductPID: 753,
        #ActiveServiceCount
        #CloseFinalised
        #ChargeAssignmentPID: 1,
        #AssignmentLevelPID: 3,
        #Active: false,
        #StartDate: Fri, 21 Feb 2014 00:00:00 UTC +00:00,
        #ChargeModePID: 2,
        #ReferenceCode: nil,
        #Memo: nil,
        #RecurrenceTypePID: 2,
        #RecurrenceInfoPID: 1,
        #LastAppliedDate: Mon, 31 Mar 2014 00:00:00 UTC +00:00,
        #EquipmentAssignmentTypePID: nil,
        #MaxCummulativeAmount: nil,
        #CummulativeAmount: nil,
        #PurchaseOrderPID: nil,
        #ContractPID: nil,
        #DepartmentPID: nil,
        #BundleAssignmentPID: nil,
        #Description: "911 service VoIP - monthly access fee",
        #OverrideGLAccount: false,
        #GLAccountPID: nil,
        #OwnerAppUserPID: 1,
        #OwnerTeamPID: nil,
        #InsertAppUserPID: 1,
        #UpdateAppUserPID: 2,
        #CreatedOn: Sun, 09 Mar 2014 14:27:56 UTC +00:00,
        #MinimumBillingRequirementPID: nil,
        #InvoicePID: nil,
        #TotalOccurrences: 3,
        #LastChargeStartDate: Tue, 01 Apr 2014 00:00:00 UTC +00:00,
        #LastChargeEndDate: Mon, 31 Mar 2014 00:00:00 UTC +00:00,
        #LastInvoicePID: 1697,
        #ARDBName: nil,
        #TaxLocalePID: nil,
        #UnitCost: nil,
        #PPProductAssignmentPID: nil>
      }

      if self[:UpdatedOn]
        hash[:updated_at] = self[:UpdatedOn]
      end
      if self[:AssignedOn]
        hash[:created_at] = self[:AssignedOn]
      end
      if service.present?
        hash[:asset] = self.service.associated_asset
      end

      if recurrence_type.try(:Code) == "Recurring"
        hash[:renewable] = true
        if recurrence_info.try(:Frequency_Months)
          hash[:frequency_months] = recurrence_info.try(:Frequency_Months)
        end
      end

      hash
    end

    def map_by_foreign_pid
      n = BillableService.find_by_foreign_pid(self[:ChargeAssignmentPID])
      if n
        $HAKKUSTATS[:ca_reused] += 1
        begin
          n.update(make_billable_service_hash)
        rescue ArgumentError
          byebug
          puts "Looking for CA: #{self[:ChargeAssignmentPID]}"
        end
      end
      n
    end

    def create_billable_service
      n = BillableService.create(make_billable_service_hash)
      $HAKKUSTATS[:ca_created] += 1
      n
    end

    def associated_billable_service(invoice = nil)
      n = (map_by_foreign_pid || create_billable_service)
      n.invoice = invoice
      n
    end

    def update_associated_billable_service
      associated_billable_service.update(make_billable_service_hash)
    end

    def self.import_row(row)

      created_on = nil
      last_applied_date = nil

      # look for non-numeric ChargeAssignmentPIDs: it's garbage
      return nil unless row[0] =~ /^\d+$/

      # can not fix a record with a blank last_applied_date, sorry
      if !row[13].blank?
        while last_applied_date == nil and row.count > 45
          # to check for a date, make sure it starts with four digits, because
          # "10" is a valid date.
          if !row[13].blank? and row[13] =~ /^\d\d\d\d/
            begin
              last_applied_date = row[13].try(:to_datetime).try(:utc)
            rescue ArgumentError
              last_applied_date = nil
            end
          end
          if last_applied_date == nil
            # row23 is the description.
            row10 = row[10] || ""
            row11 = row[11] || ""
            row[10] = row10 + ',' + row11
            row.delete_at(11)
          end
        end
      end

      while created_on == nil and row.count > 45
        # to check for a date, make sure it starts with four digits, because
        # "10" is a valid date.
        if !row[30].blank? and row[30] =~ /^\d\d\d\d/
          begin
            created_on = row[30].to_datetime.try(:utc)
          rescue ArgumentError
            created_on = nil
          end
        end
        if created_on == nil
          # row23 is the description.
          row23 = row[23] || ""
          row24 = row[24] || ""
          row[23] = row23 + ',' + row24
          row.delete_at(24)
        end
      end

      start_date = nil
      begin
        start_date = row[5].try(:to_datetime).try(:utc)
      rescue ArgumentError
        byebug
      end

      last_charge_start_date = nil
      begin
        last_charge_start_date = row[35].try(:to_datetime).try(:utc)
      rescue ArgumentError
        byebug
      end

      last_charge_end_date = nil
      begin
        last_charge_end_date = row[36].try(:to_datetime).try(:utc)
      rescue ArgumentError
        byebug
      end

      updated_on = nil
      begin
        updated_on = row[31].try(:to_datetime).try(:utc)
      rescue ArgumentError
        byebug
      end

      item = where(ChargeAssignmentPID: row[0]).take || create(ChargeAssignmentPID: row[0])

      item.update(ProductPID: row[1],
          AssignmentLevelPID: row[2],
          Active: row[3] == "1",
          AssignedOn: row[4],
          StartDate: start_date,
          Quantity: row[6],
          UnitPrice: row[7],
          ChargeModePID: row[8],
          ReferenceCode: row[9],
          Memo: row[10],
          RecurrenceTypePID: row[11],
          RecurrenceInfoPID: row[12],
          LastAppliedDate: last_applied_date,
          EquipmentAssignmentTypePID: row[14],
          MaxCummulativeAmount: row[15],
          CummulativeAmount: row[16],
          PurchaseOrderPID: row[17],
          ContractPID: row[18],
          CustomerPID: row[19],
          DepartmentPID: row[20],
          ServicePID: row[21],
          BundleAssignmentPID: row[22],
          Description: row[23],
          OverrideGLAccount: row[24],
          GLAccountPID: row[25],
          OwnerAppUserPID: row[26],
          OwnerTeamPID: row[27],
          InsertAppUserPID: row[28],
          UpdateAppUserPID: row[29],
          CreatedOn: created_on,
          UpdatedOn: updated_on,
          MinimumBillingRequirementPID: row[32],
          InvoicePID: row[33],
          TotalOccurrences: row[34],
          LastChargeStartDate: last_charge_start_date,
          LastChargeEndDate: last_charge_end_date,
          LastInvoicePID: row[37],
          ARDBName:       row[38],
          TaxLocalePID:   row[39],
          UnitCost:       row[40],
          TotalPrice:     row[41],
          PPProductAssignmentPID: row[42],
          ParentChargeAssignmentPID: row[43],
          RevChargeAssignmentPID:    row[44],
          TotalPriceMode: row[45])
      return item
    end

    def self.import_file(filename, encoding = 'CP863:UTF-8')
      cnt = 0
      CSV.foreach filename, headers: :first_row, encoding: encoding do |row|
        if (cnt % 100)==0
          print sprintf("line %04d: %30s - %30s\r", cnt, row[10], row[23])
        end
        begin
          row2 = []
          row.each_entry {|n| row2 << n[1] }
          item = self.import_row(row2)
          cnt += 1
        rescue CSV::MalformedCSVError => e # Encoding::UndefinedConversionError, RangeError => e
          Rails.logger.error e.message
        end
      end
      return cnt
    end

  end
end
