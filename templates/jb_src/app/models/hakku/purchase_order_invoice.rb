module Hakku
  class PurchaseOrderInvoice < HakkuRecord
    self.primary_key = "InvoicePID"
    self.table_name  = "PurchaseOrderInvoice"
  end
end

