module Hakku
  class Department < HakkuRecord

    self.primary_key = "DepartmentPID"
    self.table_name  = "Department"

    alias_attribute :name, :DepartmentName

    belongs_to :customer,
      foreign_key: 'CustomerPID'

    has_many :charge_assignments,
      foreign_key: 'DepartmentPID'

    has_many :active_charges,
      -> { active },
      foreign_key: 'DepartmentPID',
      class_name: 'Hakku::ChargeAssignment'

    has_many :inactive_charges,
      -> { inactive },
      foreign_key: 'DepartmentPID',
      class_name: 'Hakku::ChargeAssignment'

    has_many :products,
      through: :charge_assignments

    def make_site_hash
      hash = {
        foreign_pid: self[:DepartmentPID],
        name:        self[:DepartmentName],
        # ParentDepartmentPID,
        # CustomerPID,
        # GLAccountGroupPID,
        # CustomerCategoryPID,
        # CurrentAccountStatusTypePID,
        created_at:  self[:CreatedOn],
        activation_date:  self[:StartDate],
        termination_date: self[:TerminationDate],
        site_number:      self[:Code],
        # LanguagePreference,
        # DiscountChargesinInvoiceOnly,
        # Comments,
        # Privacy,
        # TopicPID,
        # Round2Decimal,
        # ResultantRound2Decimal,
        # RoundingMethod,
        # ResultantRoundingMethod,
        # UsageRecordRateThresholdAmount,
        # AutoCapUsageRecordRate,
        # MonthlyThresholdCasualService,
        # UsageRecordQuantityThreshold,
        # AutoCapUsageRecordQuantity,
        # CurrencyConversionFactorPID,
        # OwnerAppUserPID,
        # OwnerTeamPID,
        # InsertAppUserPID,
        # UpdateAppUserPID,
        # UpdatedOn,
        # CloseFinalised,
        # PPDepartmentPID
      }

      if self[:UpdatedOn]
        hash[:updated_at] = self[:UpdatedOn]
      end

      hash
    end

    def map_by_foreign_pid
      n = Site.find_by_foreign_pid(self[:DepartmentPID])
      if n
        $HAKKUSTATS[:department_reused] += 1
        begin
          n.update(make_site_hash)
        rescue ArgumentError
          byebug
          puts "Looking for Department: #{self[:DepartmentPID]}"
        end
      end
      n
    end

    def map_by_site_name
      client = customer.associated_client
      n = client.sites.find_by_name(self[:DepartmentName])
      if n
        $HAKKUSTATS[:department_map_by_name] += 1
      end
      n
    end

    def create_site
      n = Site.create(make_site_hash)
      $HAKKUSTATS[:department_created] += 1
      n
    end

    def associated_site
      n = (map_by_foreign_pid || map_by_site_name || create_site)
      n
    end

    def update_associated_site
      associated_site.update(make_site_hash)
    end

    def self.import_row(row)

      created_on = nil

      # look for non-numeric first argument: it's garbage
      return nil unless row[0] =~ /^\d+$/

      while created_on == nil and row.count > 33
        # to check for a date, make sure it starts with four digits, because
        # "10" is a valid date.
        if !row[9].blank? and row[9] =~ /^\d\d\d\d/
          begin
            created_on = row[9].to_datetime.try(:utc)
          rescue ArgumentError
            created_on = nil
          end
        end
        if created_on == nil
          # row3 is the description.
          row3 = row[3] || ""
          row4 = row[4] || ""
          row[3] = row3 + ',' + row4
          row.delete_at(4)
        end
      end

      item = where(DepartmentPID: row[0]).take || create(DepartmentPID: row[0])

      fields = %w[DepartmentPID SeparateInvoice Code DepartmentName ParentDepartmentPID CustomerPID GLAccountGroupPID CustomerCategoryPID CurrentAccountStatusTypePID CreatedOn StartDate TerminationDate LanguagePreference DiscountChargesinInvoiceOnly Comments Privacy TopicPID Round2Decimal ResultantRound2Decimal RoundingMethod ResultantRoundingMethod UsageRecordRateThresholdAmount AutoCapUsageRecordRate MonthlyThresholdCasualService UsageRecordQuantityThreshold AutoCapUsageRecordQuantity CurrencyConversionFactorPID OwnerAppUserPID OwnerTeamPID InsertAppUserPID UpdateAppUserPID UpdatedOn CloseFinalised PPDepartmentPID]
      rowcnt = 0
      hash = Hash.new
      fields.each { |sym| hash[sym] = row[rowcnt]; rowcnt += 1; }
      item.update(hash)

      return item
    end

    def self.import_file(filename, encoding = 'CP863:UTF-8')
      cnt = 0
      CSV.foreach filename, headers: :first_row, encoding: encoding do |row|
        if (cnt % 100)==0
          print sprintf("line %04d: %30s - %30s\r", cnt, row[3], row[9])
        end
        begin
          row2 = []
          row.each_entry {|n| row2 << n[1] }
          item = self.import_row(row2)
          cnt += 1
        rescue CSV::MalformedCSVError => e # Encoding::UndefinedConversionError, RangeError => e
          Rails.logger.error e.message
        end
      end
      return cnt
    end


  end
end
