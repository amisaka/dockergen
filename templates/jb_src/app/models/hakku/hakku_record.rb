require 'csv'

class Hakku::HakkuRecord < ActiveRecord::Base
  if $HAKKU
    establish_connection :hakku
    self.abstract_class = true

    LAST_DATA_DUMP_DATE = DateTime.parse('2016-11-15')

    def self.delta
      self.where('CreatedOn > ? OR UpdatedOn > ?', date = LAST_DATA_DUMP_DATE, date)
    end

  end
end
