module Hakku
  class LineOfBusiness < HakkuRecord
     # In March 2018, the Service model was renamed Product.  This code here was not touched as part of that rename effort as I don't thihk this code has been in use
    self.primary_key = "LineOfBusinessPID"
    self.table_name  = "LineOfBusiness"

    has_many :services,
      foreign_key: 'LineOfBusinessPID'
  end
end
