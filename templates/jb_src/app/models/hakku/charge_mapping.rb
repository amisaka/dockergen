module Hakku
  class ChargeMapping < HakkuRecord
    self.primary_key = "ChargeMappingPID"
    self.table_name  = "BillSoftTax.ChargeMapping"
  end
end

