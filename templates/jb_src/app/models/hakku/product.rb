#
# this maps to the "Service" model
# # In March 2018, the Service model was renamed Product.  This code here was not touched as part of that rename effort as I don't thihk this code has been in use
#

module Hakku
  class Product < HakkuRecord

    self.primary_key = "ProductPID"
    self.table_name  = "Product"

    has_one :service,
      class_name: '::Service',
      foreign_key: 'foreign_pid'

    has_many :customers,
      through: :charge_assignments

    has_many :departments,
      through: :charge_assignments

    has_many :charge_assignments,
             foreign_key: 'ProductPID'

    belongs_to :product_category, foreign_key: 'ProductCategoryPID'

    def make_service_hash
      { name: self[:Code],
        foreign_pid: self[:ProductPID],
        description: self[:Description],
        unitprice: self[:UnitCost] || self[:Amount],
        renewable: true,
        available: self[:Active]
      }
    end

    def make_product
      ::Service.create(make_service_hash)
      $HAKKUSTATS[:product_created] += 1
    end

    def map_by_foreign_pid
      n = ::Service.where(foreign_pid: self[:ProductPID]).take
      if n
        n.update(make_service_hash)
        $HAKKUSTATS[:product_reused] += 1
      end
      n
    end

    def associated_service
      map_by_foreign_pid || make_product
    end


    def make_service_hash
      { name: self[:Code],
        foreign_pid: self[:ProductPID],
        description: self[:Description],
        unitprice: self[:UnitCost],
        renewable: true,
        available: self[:Active]
      }
    end

    def self.import_row(row)
      while (row[3] != '0' and row[3] != '1') or row[3] == nil
        #byebug if !row[2] or !row[3]
        row3 = row[3] || ""
        row[2] = row[2] + ',' + row3
        row.delete_at(3)
      end
      item = create!( ProductPID: row[0],
                      Code: row[1],
                      Description: row[2],
                      Amount: row[5],
                      UnitCost: row[20])
      item
    end

    def self.import_file(filename, encoding = 'CP863:UTF-8')
      cnt = 0
      CSV.foreach filename, headers: :first_row, encoding: encoding do |row|
        begin
          row2 = []
          row.each_entry {|n| row2 << n[1] }
          item = self.import_row(row2)
          cnt += 1
        rescue CSV::MalformedCSVError => e # Encoding::UndefinedConversionError, RangeError => e
          Rails.logger.error e.message
        end
      end
      return cnt
    end

  end
end
