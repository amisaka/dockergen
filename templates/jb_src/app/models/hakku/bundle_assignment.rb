module Hakku
  class BundleAssignment < HakkuRecord
    self.primary_key = "BundleAssignmentPID"
    self.table_name  = "BundleAssignment"
  end
end

