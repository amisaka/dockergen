module Hakku
  class ProductCategory < HakkuRecord
    self.primary_key = "ProductCategoryPID"
    self.table_name  = "ProductCategory"
  end
end

