module Hakku
  class Contract < HakkuRecord
    self.primary_key = "ContractPID"
    self.table_name  = "Contract"
  end
end

