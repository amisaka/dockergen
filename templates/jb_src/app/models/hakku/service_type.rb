module Hakku
  class ServiceType < HakkuRecord
    # In March 2018, the Service model was renamed Product.  This code here was not touched as part of that rename effort as I don't thihk this code has been in use

    self.table_name = 'ServiceType'
    self.primary_key = 'ServiceTypePID'

    has_many :services,
      foreign_key: 'ServiceTypePID'

  end
end
