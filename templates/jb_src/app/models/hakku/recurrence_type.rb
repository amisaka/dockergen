module Hakku
  class RecurrenceType < HakkuRecord
    self.primary_key = "RecurrenceTypePID"
    self.table_name  = "RecurrenceType"
  end
end

