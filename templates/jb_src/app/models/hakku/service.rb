module Hakku
  class Service < HakkuRecord
     # In March 2018, the Service model was renamed Product.  This code here was not touched as part of that rename effort as I don't thihk this code has been in use
     # In April 2018, the Phone   model was renamed Service.  This code here was not touched as part of that rename effort as I don't thihk this code has been in use

    self.primary_key = "ServicePID"
    self.table_name  = "Service"

    belongs_to :customer,
      foreign_key: 'CustomerPID'

    belongs_to :line_of_business,
      foreign_key: 'LineOfBusinessPID'

    belongs_to :service_type,
      foreign_key: 'ServiceTypePID'

    def make_asset_hash
      { foreign_pid: self[:ServicePID],
        service_id: self[:ID],
        line_of_business: line_of_business_code,
        service_type: service_type_code,
        btn: customer_associated_client_btn,
      }
    end

    def map_by_foreign_pid
      n = Phone.where(foreign_pid: self[:ServicePID]).take
      n.update(make_asset_hash) if n
      n
    end

    def create_asset
      Phone.create(make_asset_hash)
    end

    def associated_asset
      map_by_foreign_pid || create_asset
    end

    # REVIEW: mcr please review this code for updating assets
    def update_associated_asset
      associated_asset.update(make_asset_hash)
    end

    def line_of_business_code
      line_of_business[:Code]
    rescue
      ''
    end

    def service_type_code
      service_type[:Code]
    rescue
      ''
    end

    def customer_associated_client_btn
      customer.associated_client.btn
    rescue
      nil
    end

    def update_service_type
      associated_asset.update(service_type: service_type_code)
    end

    def update_line_of_business
      associated_asset.update(line_of_business: line_of_business_code)
    end

    def self.import_row(row)

      # look for non-numeric servicePIDs: it's garbage
      return nil unless row[0] =~ /^\d+$/

      createdOn = nil

      # some rows have duplicated DisplayText/Description columns
      if row[4] == row[6] and row[5] == row[7]
        row.delete_at(6)
        row.delete_at(7)
      end

      while createdOn == nil and row.count > 42
        # to check for a date, make sure it starts with four digits, because
        # "10" is a valid date.
        if !row[16].blank? and /^\d\d\d\d/.match(row[16])
          begin
            createdOn = row[16].to_datetime.try(:utc)
          rescue ArgumentError
            createdOn = nil
          end
        end
        if createdOn == nil
          # row6 is the description.
          row5 = row[5] || ""
          row6 = row[6] || ""
          row[5] = row5 + ',' + row6
          row.delete_at(6)
        end
      end

      (servicePID,billToServicePID,id,serviceTypePID,displayText,description,parentServicePID,
       serviceAssignmentTypePID,departmentLineOfBusinessPID,customerLineOfBusinessPID,
       customerPID,departmentPID,lineOfBusinessPID,gLAccountGroupPID,customerCategoryPID,
       currentAccountStatusTypePID,createdOn,startDate,terminationDate,
       languagePreference,validateProjectCode,provisionLater,comments,privacy,
       resultantRound2Decimal,resultantRoundingMethod,insertAppUserPID,updateAppUserPID,topicPID,
       usageRecordRateThresholdAmount,autoCapUsageRecordRate,monthlyThresholdCasualService,
       usageRecordQuantityThreshold,autoCapUsageRecordQuantity,currencyConversionFactorPID,
       privateLine,ownerAppUserPID,ownerTeamPID,defaultService,updatedOn,closeFinalised,billed,
       pPServiceLinePID)=row

      unless createdOn.blank?
        createdOn = createdOn.try(:to_datetime)
      end

      item = create!( ServicePID:       servicePID,
                      BillToServicePID: billToServicePID,
                      ID:                       id,
                      ServiceTypePID:               serviceTypePID,
                      DisplayText:                  displayText,
                      Description:                  description,
                      ParentServicePID:             parentServicePID,
                      ServiceAssignmentTypePID:     serviceAssignmentTypePID,
                      DepartmentLineOfBusinessPID:  departmentLineOfBusinessPID,
                      CustomerLineOfBusinessPID:    customerLineOfBusinessPID,
                      CustomerPID:                  customerPID,
                      DepartmentPID:                departmentPID,
                      LineOfBusinessPID:            lineOfBusinessPID,
                      GLAccountGroupPID:            gLAccountGroupPID,
                      CustomerCategoryPID:          customerCategoryPID,
                      CurrentAccountStatusTypePID:  currentAccountStatusTypePID,
                      CreatedOn:                    createdOn.try(:to_datetime),
                      StartDate:                    startDate,
                      TerminationDate:              terminationDate,
                      LanguagePreference:       languagePreference,
                      ValidateProjectCode:      validateProjectCode,
                      ProvisionLater:           provisionLater,
                      Comments:                 comments,
                      Privacy:                  privacy,
                      ResultantRound2Decimal:   resultantRound2Decimal,
                      ResultantRoundingMethod:  resultantRoundingMethod,
                      InsertAppUserPID:         insertAppUserPID,
                      UpdateAppUserPID:         updateAppUserPID,
                      TopicPID:                 topicPID,
                      UsageRecordRateThresholdAmount:       usageRecordRateThresholdAmount,
                      AutoCapUsageRecordRate:               autoCapUsageRecordRate,
                      MonthlyThresholdCasualService:        monthlyThresholdCasualService,
                      UsageRecordQuantityThreshold:         usageRecordQuantityThreshold,
                      AutoCapUsageRecordQuantity:           autoCapUsageRecordQuantity,
                      CurrencyConversionFactorPID:          currencyConversionFactorPID,
                      PrivateLine:                          privateLine,
                      OwnerAppUserPID:                      ownerAppUserPID,
                      OwnerTeamPID:                         ownerTeamPID,
                      DefaultService:                       defaultService,
                      UpdatedOn:                            updatedOn,
                      CloseFinalised:                       closeFinalised,
                      Billed:                               billed,
                      PPServiceLinePID:                     pPServiceLinePID)
      item
    end

    def self.import_file(filename, encoding = 'CP863:UTF-8')
      cnt = 0
      CSV.foreach filename, headers: :first_row, encoding: encoding do |row|
        begin
          row2 = []
          row.each_entry {|n| row2 << n[1] }
          item = self.import_row(row2)
          cnt += 1
        rescue CSV::MalformedCSVError => e # Encoding::UndefinedConversionError, RangeError => e
          Rails.logger.error e.message
        end
      end
      return cnt
    end

  end
end
