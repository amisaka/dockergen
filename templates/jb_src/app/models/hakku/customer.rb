# coding: utf-8    ^^???  ^a^^^^^
module Hakku
  class Customer < HakkuRecord

    self.primary_key = "CustomerPID"
    self.table_name  = "Customer"

    has_one :client,
      foreign_key: 'foreign_pid'

    has_many :charge_assignments,
      foreign_key: 'CustomerPID',
      class_name: 'Hakku::ChargeAssignment'

    has_many :active_charges,
      -> { active },
      foreign_key: 'CustomerPID',
      class_name: 'Hakku::ChargeAssignment'

    has_many :inactive_charges,
      -> { inactive },
      foreign_key: 'CustomerPID',
      class_name: 'Hakku::ChargeAssignment'

    has_many :products,
      through: :charge_assignments

    has_many :departments,
      foreign_key: 'CustomerPID'

    alias_attribute :name, :CustomerName

    def language
      # match Francais with and without accents
      return "English" unless self[:LanguagePreference]
      return "French" if self[:LanguagePreference].downcase =~ /^fr/
      return "English"
    end

    def make_client_hash
      n = { foreign_pid: self[:CustomerPID],
        billing_btn: self[:Code],
        #NameTitle
        #FirstName
        #MiddleName
        #LastName
        #NameSuffix
        #County
        billing_cycle_id: self[:BillingCyclePID],
        created_at: self[:CreatedOn],
        started_at: self[:StartDate],
        terminated_at: self[:TerminationDate],
        #CurrentAccountStatusTypePID   !!!
        #GLAccountGroupPID
        #CustomerCategoryPID
        language_preference: language,
        #BillingInfoPID                !!!
        #DiscountChargesinInvoiceOnly
        #Privacy
        #TopicPID
        #ThirdPartyBilling
        #TriggerDirectBilling
        #UsageRecordRateThresholdAmount
        #AutoCapUsageRecordRate
        #Round2Decimal
        #ResultantRound2Decimal
        #RoundingMethod
        #ResultantRoundingMethod
        #MonthlyThresholdCasualService
        #UsageRecordQuantityThreshold
        #AutoCapUsageRecordQuantity
        #CurrencyConversionFactorPID
        #ParentCustomerPID
        #MigrationDate
        #OwnerAppUserPID
        #OwnerTeamPID
        #InsertAppUserPID
        #UpdateAppUserPID
        updated_at: self[:UpdatedOn]
        #ActiveServiceCount
        #CloseFinalised
          }
      n
    end

    def map_by_foreign_pid
      client = Client.find_by_foreign_pid(self[:CustomerPID])
      client
    end

    def simplecode
      # strips non numeric chars
      @simplecode ||= self[:Code].gsub(/[^0-9]/, '')
    end

    def map_by_btn
      btn = Btn.get_by_billingtelephonenumber(self[:Code]) ||
            Btn.get_by_billingtelephonenumber("+1" + self[:Code]) ||
            Btn.get_by_billingtelephonenumber(simplecode) ||
            Btn.get_by_billingtelephonenumber("+1" + simplecode)

      client = btn.try(:client)
      client
    end

    def map_by_did
      pn = Phone.find_by_phone_number(self[:Code]) ||
           Phone.find_by_phone_number("+1" + self[:Code])
           Phone.find_by_phone_number(simplecode)
           Phone.find_by_phone_number("+1" + simplecode)

      btn    = pn.try(:btn)
      client = btn.try(:client)
      client
    end

    def fix_up_client(newone)
      newone.client_crm_account_code_fix(nil, self[:CustomerName])
      newone.fix_primary_btn
      newone.save!
    end

    def find_associated_client
      client = map_by_foreign_pid # || map_by_btn || map_by_did
      if client
        fix_up_client(client)
        $HAKKUSTATS[:customer_reused] += 1
      end
      client
    end

    def make_associated_client
      newone = Client.create(make_client_hash)
      if newone
        fix_up_client(newone)
        $HAKKUSTATS[:customer_created] += 1
      end
      newone
    end

    def associated_client
      find_associated_client || make_associated_client
    end

    def create_billable_services(invoice)
      self.charge_assignments.active.each { |ca|
        bs = ca.associated_billable_service(invoice)
        bs.save!
      }
    end

    def billing_address
      Address.first_billing_address_by_customer_pid self[:CustomerPID]
    end

    def shipping_address
      Address.first_shipping_address_by_customer_pid self[:CustomerPID]
    end

    def primary_address
      Address.first_primary_address_by_customer_pid self[:CustomerPID]
    end

    def other_address
      Address.first_other_address_by_customer_pid self[:CustomerPID]
    end

    def addresses
      Address.find_by_customer_pid(self[:CustomerPID])
    end

    def self.import_row(row)
      billing_cycle_pid = nil

      while billing_cycle_pid == nil
        begin
          billing_cycle_pid = Integer(row[9].to_s, 10)
        rescue ArgumentError
          row3 = row[3] || ""
          row[2] = row[2] + ',' + row3
          row.delete_at(3)
        end
      end

      item = create!(CustomerPID: row[0],
                     Code: row[1],
                     CustomerName: row[2],
                     NameTitle: row[3],
                     FirstName: row[4],
                     MiddleName: row[5],
                     LastName: row[6],
                     NameSuffix: row[7],
                     County:     row[8],
                     BillingCyclePID: billing_cycle_pid,
                     CreatedOn: row[10],
                     StartDate: row[11],
                     TerminationDate: row[12],
                     CurrentAccountStatusTypePID: row[13],
                     GLAccountGroupPID: row[14],
                     CustomerCategoryPID: row[15],
                     LanguagePreference:  row[16],
                     BillingInfoPID: row[17],
                     DiscountChargesinInvoiceOnly: row[18],
                     Privacy:  row[19],
                     TopicPID: row[20],
                     ThirdPartyBilling: row[21],
                     TriggerDirectBilling: row[22],
                     UsageRecordRateThresholdAmount: row[23],
                     AutoCapUsageRecordRate: row[24],
                     Round2Decimal: row[25],
                     ResultantRound2Decimal: row[26],
                     RoundingMethod: row[27],
                     ResultantRoundingMethod: row[28],
                     MonthlyThresholdCasualService: row[29],
                     UsageRecordQuantityThreshold:  row[30],
                     AutoCapUsageRecordQuantity: row[31],
                     CurrencyConversionFactorPID: row[32],
                     ParentCustomerPID: row[33],
                     MigrationDate: row[34],
                     OwnerAppUserPID: row[35],
                     OwnerTeamPID: row[36],
                     InsertAppUserPID: row[37],
                     UpdateAppUserPID: row[38],
                     UpdatedOn: row[39],
                     ActiveServiceCount: row[40],
                     CloseFinalised: row[41],
                     AccountPID: row[42],
                     PPCustomerPID: row[43] )
      return item
    end

    def self.import_file(filename, encoding = 'CP863:UTF-8')
      cnt = 0
      CSV.foreach filename, headers: :first_row, encoding: encoding do |row|
        begin
          row2 = []
          row.each_entry {|n| row2 << n[1] }
          item = self.import_row(row2)
          cnt += 1
        rescue CSV::MalformedCSVError => e # Encoding::UndefinedConversionError, RangeError => e
          Rails.logger.error e.message
        end
      end
      return cnt
    end

    def update_associated_client
      associated_client.update(make_client_hash)
    end

  end
end
