require 'uuid'

# a CustomerUser is a representative from the end user.
# tickets can not be created without a Customer User.

class OTRS::CustomerUser < OTRS::Record
  # self.primary_key = "id"
  # self.table_name  = "otrs.customer_user"
  #
  # belongs_to :crm_account, :foreign_key => 'customer_id'
  # belongs_to :otrs_customercompany, primary_key: 'customer_id',
  #            class_name: 'OTRS::CustomerCompany',
  #            foreign_key: 'customer_id'
  # has_many  :sites, foreign_key: 'site_rep_id'
  #
  # def self.create_user(site, phoneemail)
  #   nu = self.new(valid_id: 1, create_by: 1, change_by: 1)
  #   nu.email = phoneemail
  #   nu.calculate_from_site(site, "Creating")
  #   nu
  # end
  #
  # def self.make_from_site(site)
  #   if site.otrs_customeruser.count == 0
  #     client = site.client
  #     site.site_rep = nil
  #     site.phones.activated.where.not(:email => nil).find_each {|phone|
  #       if phone.email =~ /@/
  #         phoneemail = phone.email
  #         site.site_rep = create_user(site, phoneemail)
  #         break
  #       end
  #     }
  #     # handle case where email is bad/unavailable
  #     unless site.site_rep
  #       phoneemail = "nonsense@example.ip4b.net"
  #       site.site_rep = create_user(site, phoneemail)
  #     end
  #     site.save!(validate: false)
  #     site.site_rep
  #   else
  #     site.otrs_customeruser.first
  #   end
  # end
  #
  # def name
  #   "otrsuser#{id}"
  # end
  #
  # def siteclientname(site)
  #   siteclientname = site.try(:client).try(:name)
  #   if siteclientname.blank?
  #     File.open(Rails.root.join("tmp","site_client_blank.txt"), "a") do |f|
  #       f.puts "#{site.id}"
  #     end
  #     siteclientname = "Unknown"
  #   end
  #   return siteclientname
  # end
  #
  # def clientname(site)
  #   @clientname ||= sprintf("%s / %s", siteclientname(site), site.name)
  # end
  #
  # def self.unique_domain
  #   @number ||= 1
  #   @number += 1
  #   sprintf("%04u.bad.example.com", @number)
  # end
  #
  # def calculate_from_site(site, action = 'Updating', logged = $LOG_NEW_CLIENTS)
  #   self.otrs_customercompany = site.otrs_customercompany
  #   login = "#{site.client.billing_btn}-#{site.site_number}"
  #
  #   self.login = login
  #   self.first_name = clientname(site)
  #   self.last_name = login
  #   self.support_service_level = site.support_service_level
  #   if site.building
  #     self.street = site.building.streetaddr||""
  #     self.city   = (site.building.city||"") + " " + (site.building.province||"")
  #     self.country= site.building.country||""
  #     self.zip    = site.building.postalcode||""
  #   end
  #
  #   failure = nil
  #
  #   begin
  #     save
  #
  #   rescue
  #     # PG::StringDataRightTruncation
  #     # ActiveRecord::StatementInvalid
  #     failure = $!
  #     byebug if Rails.env == 'development'
  #     puts "failing with #{$!}"
  #
  #   rescue ActiveRecord::RecordNotUnique
  #     puts "#{action} site:#{site.id} #{login} found not unique, fixing"
  #     self.where(:login => login).each {|thing|
  #       next thing.id == self.id
  #       puts " .. deleted #{thing.id}"
  #       thing.delete
  #     }
  #     save!
  #   end
  #
  #   puts "#{action} site:#{site.id} OTRS::CustomerUser:#{id} for #{self.first_name} with #{self.login}\n" if logged
  #   self
  # end
  #
  # def update_from_site(site, logged = false)
  #   calculate_from_site(site, 'Updating', logged)
  # end
  #
  # def terminated!
  #   self.valid_id = 2
  #   save!
  # end
  #
  # def terminated?
  #   # user is terminated if valid_id == 2.
  #   self.valid_id == 2
  # end
  #
  # protected

end
