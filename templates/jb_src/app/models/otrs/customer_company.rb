require 'uuid'

# a CustomerUser is a representative from the end user.
# tickets can not be created without a Customer User.

class OTRS::CustomerCompany < OTRS::Record
  # if $OTRS
  #   self.primary_key = "customer_id"
  #   self.table_name  = "otrs.customer_company"
  #
  #   has_many :otrs_customerusers, class_name: 'OTRS::CustomerUser', foreign_key: 'customer_id'
  #   belongs_to :site
  #
  # end
end
