class Building < ApplicationRecord

  POSTAL_CODE_REGEX = /(?<postal_code>\A([A-Z]\d){3})\z|(?<zip_code>\A\d{5}\d{4}|\A\d{5}\z)/i
  POSTAL_CODE_SANITIZER = /[^0-9a-z]/i

  paginates_per 18

  has_many :sites

  # validates :postalcode,
  #   format: { with: POSTAL_CODE_REGEX },
  #   length: { within: 5..9 },
  #   allow_blank: true
  #
  # validates :address1,
  #   presence: true
  #
  # validates :city,
  #   presence: true
  #
  # validates :province,
  #   presence: true,
  #   if: -> { country == 'Canada' || country == 'USA' }
  #
  # validates :country,
  #   presence: true
  #
  # before_validation :sanitize_postalcode,
  #   if: :has_postalcode?

  def address
    <<-ADDRESS
    #{address1}
    #{address2}
    #{city} #{province} #{country}
    #{postalcode}
    ADDRESS
  end

  def postal_code
    return '' if postalcode.blank?
    m = postalcode.match(POSTAL_CODE_REGEX)
    if m
      if m[:postal_code]
        postalcode.insert(3, ' ')
      elsif m[:zip_code]
        if postalcode.size > 5
          postalcode.insert(5, '-')
        else
          postalcode
        end
      end
    end
  end

  alias_method :zip_code, :postal_code

  def streetaddr
    (address1||"") + (if address2; then " " + address2; else "" end)
  end

  def after_save
    # flush cache
    @select_list = nil
  end

  def self.build_select_list
    list = all.where.not(address1: nil).map { |i| [i.name, i.id] }
    list.sort! { |a,b| a[0] <=> b[0] }
  end

  def self.select_list
    @select_list ||= build_select_list
  end

  def from_crm_account(crm)
    update(address1: crm.billing_address_street,
           city:     crm.billing_address_city,
           province: crm.billing_address_state,
           postalcode: crm.billing_address_postalcode,
           country:  crm.billing_address_country)
  end

  # Upcase and strip any non-alphanumeric characters
  def sanitize_postalcode
    if postalcode
      self.postalcode = postalcode.upcase.gsub(POSTAL_CODE_SANITIZER, '')
    end
  end

  def to_s
    name || address1 || address2
  end

  private

    def has_postalcode?
      !postalcode.blank?
    end

end
