class Invoice < Transaction

  paginates_per 50 # Saw this in service.rb, figured I try it here -jlam 2018/03/16

  validate :client_xor_site
  validate :has_parent_must_has_site

  has_attached_file :document

  validates_attachment_content_type :document, content_type: "application/pdf"

  has_many :billable_services

  belongs_to :parent_invoice, class_name: 'Invoice'
  has_many   :site_invoices, class_name: 'Invoice', foreign_key: 'parent_invoice_id'
  belongs_to :client
  belongs_to :site

  has_and_belongs_to_many :payments

  #before_save {self.beforeSaveBalanceUpdates }
  #after_save { self.afterSaveBalanceUpdates }
  #after_destroy { self.updateBillingOnlyForDestroy }


  CUTOFF_PAYMENTS_DAY_OF_MONTH = 27

  # Set this to :created_at or :next
  # created_at:          the lookahead month will be set to the month the invoice is created
  # next:                the lookahead month will be set to the next month from when the month is created
  # The lookahead month: the month used to select monthly or yearly recuring charges
  # The usage month:     always set to the month proceeding the lookahead month, it is the month
  #                         used to select one-time charges
  LOOK_AHEAD_MONTH_DEFAULTS_TO = :created_at

  # Deadline before late payemnt was imposed
  LATE_PAYMENT_DATE_DEFAULT = self::CUTOFF_PAYMENTS_DAY_OF_MONTH

  # The date the post invoices feature was implemented
  # This should remain set to 2018/June/04 16:00
  POST_FEATURE_IMPLEMENTED = DateTime.new(2018, 06, 04, 16, 00, 00, "-04:00")


  # If the paymentsShownUntil gets ajusted because it is in the future
  # what is the expected mininum number of days  between paymentsSince and paymentsShownUntil
  # for paymentsSince to be subtracted 1 month as well.  See Invocing::determineDates
  # - Only comes into affect if there are no date range specified by user
  # - This is not the required range, but just the theshold that will trigger a change to paymentsSince
  MIN_PAYMENTS_RANGE_DEFAULT = 21

  # As per conversation 2018/05/29 for what type of transaction types wanted printing in invoices:
  # "No OpeningBalance. No SummaryCharge. Yes for rest."
  TransactionTypeWanted = TransactionType.where('typename not in (?)', ['OpeningBalance', 'SummaryCharge']).map{|tt| tt.id}


  ########################################################################
  # Post all unposted invoices
  # Buisiness process outline in t13553
  ########################################################################


  def self.postInvoices
    invoicesToPost = self.where(posted_at: nil).where('created_at > ?', self::POST_FEATURE_IMPLEMENTED)
    count = invoicesToPost.count
    invoicesToPost.each_with_index {|inv, i|
        Montbeau::Helpers::printProgress("Updating balances and marking invoices posted for #{inv.client.to_s}...",  i, count, true)
        inv.post
    }
  end


    ########################################################################
    # I hope one day there will be a direct relationship between invoices and
    # calls
    ########################################################################
    def calls()
        if client.present?
            Call.for(client)
        elsif site.present?
            Call.for(site)
        else
            raise "Don't know what to list calls for"
        end
    end


    def ressource()
        client or site
    end


  def charges
    billable_services
  end

  ########################################################################
  # Pretty much the same thing as charges, I think
  ########################################################################
  def billable_services
    #This is disgusting, eventually I would like to re-architecture to have an invoiced items table
    Montbeau::Invoicing.new(self.ressource, chargesDates: self.chargesDates).billableServicesForClientAsPerMonth(createdAtDate: self.created_at)
  end


  ########################################################################
  # Converts the charges dates of the invoice object into
  # the hash expecting by Montbeau::invoicing
  # Ugly architecture I know.  Wasn't time to properly re-architecture it
  ########################################################################
  def chargesDates()
    return {
		look_ahead_begin:	self.look_ahead_charges_begin,
        look_ahead_end:		self.look_ahead_charges_end,
        usage_begin:		self.usage_charges_begin,
        usage_end:			self.usage_charges_end,
    }
  end


  def postable?
	(not self.posted? and self.created_at > Invoice::POST_FEATURE_IMPLEMENTED)
  end

  def posted?
	self.posted_at.present?
  end


  def self.checkAgainstPrintedPds(pathToCheck, outputOnlyError: true)
    il = Invoice::lastInvoiceOfEveryClient

    pathToCheck = File.expand_path(pathToCheck)

    if not Dir.exists?(pathToCheck)
        raise "Diretory #{pathToCheck} does not exist"
    end

    invoicesNotFound = {}

    il.each_with_index {|inv, i|
        Montbeau::Helpers::printProgress("Checking invoice #{inv.id}... ", i, il.count, true)

        btn, inv_cycle = inv.btn_and_invoice_cycle
        id = inv.id

        if `find #{pathToCheck} -name '*#{btn}*'`.strip.empty?
            puts "\nInvoice #{id} not found\n"
			invoicesNotFound[id] = inv.client.name
        end
    }

	puts invoicesNotFound;



  end


  ########################################################################
  # Find one and only the last invoice of every client
  # Used for /invoices index listing and for to pdf feature to get
  #   the list of invoices to transform to pdf
  ########################################################################
  def self.lastInvoiceOfEveryClient(limit: 10000, onlyUnposted: false, includeChildInvoices: false)

      ressources = ['client']
      ressources << 'site' if includeChildInvoices

      # Looks convoluted to build sql with UNION but makes pulling the list much faster, presumably.
      sql = ressources.map {|ressource|
	      sql = %Q[SELECT i.*
	        from invoices i,  #{ressource}s,
	        (select #{ressource}_id, max(id) as id from invoices group by #{ressource}_id) maxIds
	        where maxIds.id = i.id
	        and i.deleted = false
	        and i.#{ressource}_id = #{ressource}s.id
	        and #{ressource}s.deleted = false
	      ]

	      if onlyUnposted == true
	        sql += " and posted_at is null and i.created_at > '#{Invoice::POST_FEATURE_IMPLEMENTED}' "
	      else
	        sql += " and i.created_at > now() - INTERVAL '2 MONTH' "
	      end

          sql
      }.join(' UNION ')

      sql += " order by created_at desc limit #{limit}"

      puts sql

      ## As this is used in view logic, we decorate it
      return Invoice.find_by_sql(sql).map{|i| i=i.decorate}
  end



  ########################################################################
  # Post this invoice
  # Buisiness process outline in t13553
  ########################################################################
  def post
    if not postable?
    	raise "Invoice object is already posted or created before #{Invoice::POST_FEATURE_IMPLEMENTED}"
    end

    self.beforeSaveBalanceUpdates

    success = self.afterSaveBalanceUpdates

    if success == true
        self.posted_at = DateTime.now()
        self.save!
    end
  end


  ########################################################################
  # For quick consultation, gives the same output as you would see
  # in an invoice
  ########################################################################
  def to_s
    str = ''
    self.attributes.each{|attribute, value|
        str += %Q[#{attribute.to_s}        #{value.class == BigDecimal ? value.to_f : value.to_s}\n]
    }
    return str
  end




  ########################################################################
  # Destroy all unposted invoices created after this feature was implemented
  # Buisiness process outline in t13553
  ########################################################################
  def self.resetInvoices
    invoicesToDelete = Invoice.where('created_at > ?', self::POST_FEATURE_IMPLEMENTED).where(posted_at: nil)

    invoicesToDelete.destroy_all
  end

  ########################################################################
  # Also called "amount invoiced" in the invoice_decorator
  # Though "amount" is a generic term, I've kept it given the lack of a different term
  # It's the most important attribute of an invoice,
  # It is the total of recurring and call charges and
  # the amount by which the balance will be affected
  #
  # TODO: BUT DOES IT INCLUDE TAXES?
  ########################################################################
  def amountToApplyToBalance()
    self.amount
  end

  ########################################################################
  # Get the payments of a client for a specified period.
  #
  # Because the legacy code had the logic of invoicing calculation
  # before invoice object creation in invoicing.rb, we had to create this
  # logic here
  ########################################################################
  def self.paymentsForPeriod(client_in, fromDateIn, untilDateIn)
    raise "client_in cannot be null" if client_in.nil?
    client_in = Client.find(client_in) if [Integer, Fixnum].include?(client_in.class)

    fromDateIn = fromDateIn.beginning_of_day
    untilDateIn = untilDateIn.end_of_day

    ## TODO : add not in posted invoice
    q = Payment.where(client: client_in).where(:date => fromDateIn..untilDateIn).where(:transaction_type_id => Invoice::TransactionTypeWanted)

    # Payments that are not in a posted invoice
    #subQuery = Payment.joins(:invoices).where(:date => fromDateIn..untilDateIn).where(client: client_in).where.not(posted: nil)
    subQuery = Payment.joins(:invoices).where(:date => fromDateIn..untilDateIn).where(client: client_in).where('invoices.posted_at is not null')
    q = q.where.not(id: subQuery)

    return q
  end

  ########################################################################
  # Deprecated method.
  ########################################################################
  def paymentsForPeriod()
    raise "This method should not be in use"
    #Invoice::paymentsForPeriod(self.ressource.client, self.payments_shown_since, self.payments_shown_until)
  end

  ########################################################################
  # Get the start date normally shown for payments
  #
  # Because the legacy code had the logic of invoicing calculation
  # before invoice object creation in invoicing.rb, we had to create this
  # logic here
  ########################################################################
  def self.paymentsShownFrom()
    Payment::searchPaymentsSince()
  end

  ########################################################################
  # Get the end date normally shown for payments
  #
  # Because the legacy code had the logic of invoicing calculation
  # before invoice object creation in invoicing.rb, we had to create this
  # logic here
  ########################################################################
  def self.paymentsShownUntil(dateCreatedIn = nil)
	dateCreatedIn = Date.today if dateCreatedIn.nil?

    Date.new(dateCreatedIn.year, dateCreatedIn.month, Invoice::CUTOFF_PAYMENTS_DAY_OF_MONTH)
  end

  def btn_and_invoice_cycle
    InvoiceTemplate.joins(:btns).where('btns.client_id = ?', self.ressource.client.id).pluck(:billingtelephonenumber, :invoice_cycle).first
  end


  def lastMonth()
    created_at.last_month
  end

  ########################################################################
  # Check that two numbers are almost equal (within 1 cent)
  # and log an error if there is more than a difference
  # Unusedc as of 2017/07/04
  ########################################################################
  def checkTotalsAndWarn(sumsHash)
    if (sumsHash.values.first - sumsHash.values.second).abs > 0.01 and ((self.created_at - DateTime.now())/(60*60*24) < 60)
        Rails.logger.error("%s (%.3f) different then %s (%.3f) for invoice %d. Difference of %.3f" % [sumsHash.keys.first, sumsHash.values.first, sumsHash.keys.second, sumsHash.values.second, self.id, sumsHash.values.first - sumsHash.values.second])
    end
  end


  ########################################################################
  # Get the late payment date
  #
  # Because the legacy code had the logic of invoicing calculation
  # before invoice object creation in invoicing.rb, we had to create this
  # logic here
  ########################################################################
  def self.latePaymentDate(dateCreatedIn = nil)
    dateCreatedIn = Date.today if dateCreatedIn.nil?

    latePaymentDate = Date.new(dateCreatedIn.year, dateCreatedIn.month, self::LATE_PAYMENT_DATE_DEFAULT)

    return latePaymentDate
  end

  ########################################################################
  # Depreacted because it should be stored as late_payment_date
  ########################################################################
  #def latePaymentDate
  #  self.late_payment_date
  #end

  def invoice_date
    created_at.beginning_of_month.to_date
  end

  def due_date
    created_at.end_of_month.to_date
  end

  def siteOnly?()
    ressource().class == Site
  end

  def clientOnly?()
    ressource().class == Client
  end


  def allTaxes()
    tax_table = Montbeau::Invoicing::TaxTable::TAXES
    tax_acronyms = Montbeau::Invoicing::TaxTable::allKeysOfHash(tax_table)
    tax_acronyms.add("HST").add("GST")
    tax_attributes = Invoice.new.attributes.keys.select{ |ia| tax_acronyms.any?{|ta| ia.include?(ta.to_s.downcase)}}.map{|a| a.to_sym}

    self.slice(*tax_attributes)
  end

    def perTaxTypeTotals
        perTypeHash = {}  # Hash without the call/charges distinction

        taxHash = self.allTaxes

        taxHash.each {|taxAttribute, amount|
            next if amount == 0
            taxName = taxAttribute.split('_')
            taxName.pop

            taxName = taxName.join('_')

            taxTotal = (perTypeHash[taxName] or 0) + amount
            perTypeHash[taxName] = taxTotal
        }

        return perTypeHash

    end



   ########################################################################
   #
   ########################################################################
   def self.createFirstDummyPostedInvoice(allTransactionsUntil: Date.new(2018, 05, 29), chargesUntil: Date.new(2018, 06, 30).end_of_month, client: nil )

        if chargesUntil < allTransactionsUntil
            raise "chargesUntil < allTransactionsUntil. chargesUntil should be after allTransactionsUnil"
        end

        if Invoice.where.not(posted_at: nil).count != 0
            $stderr.puts "NOTICE: there are posted invoices.  Clients with posted invoices will be skipped"
            finish = 5; (1..finish).each {|i| Montbeau::Helpers::printProgress("Sleeping", i, finish, true)}
        end

        if client.nil?
            invoiceableClients = ::Client.activated.invoiceable
        else
            invoiceableClients = [client]
            invoiceableClients = [Client.find(client)] if client.class == Fixnum
        end


        total = invoiceableClients.count

        invoiceableClients.each_with_index { |c, i|
            skip = (not c.last_invoice_only.nil?)
            msg = (skip ? 'Skipping ' : 'Creating dummy posted invoice for ')
            Montbeau::Helpers::printProgress(msg + c.to_s, i, total, true)
            next if skip

            allTransactionsUntil = allTransactionsUntil.end_of_day
            previousMonth = allTransactionsUntil.last_month
            nextMonth = allTransactionsUntil.next_month

            # Create invoice and it's mandatory values
            i= Invoice.new
            i.client = c
            i.created_at = i.updated_at = allTransactionsUntil
            i.payments_shown_since = Date.new(previousMonth.year, previousMonth.month, previousMonth.day)
            i.payments_shown_until = Date.new(allTransactionsUntil.year, allTransactionsUntil.month, allTransactionsUntil.day)
            i.late_payment_date = Date.new(nextMonth.year, nextMonth.month, Invoice::LATE_PAYMENT_DATE_DEFAULT)


            # Get all payments (aka transactions) until the allTransactionsUntil date
            i.total_due = Payment.where(client: c).where('date <= ?', allTransactionsUntil).sum(:amount);

            #Now include the summary charges presumably one month later
            tt_sc = TransactionType.find_by_typename("SummaryCharge")
            i.total_due += Payment.where(client: c).where('date between ? and ?', allTransactionsUntil, chargesUntil).where(transaction_type: tt_sc).sum(:amount)

            i.posted_at = allTransactionsUntil.to_datetime

            i.notes = "Dummy posted invoice to set previous balance of the next real SGE invoice"

            puts i.slice(:client, :created_at, :updated_at, :payments_shown_since, :payments_shown_until, :late_payment_date, :posted_at, :total_due)

            # TODO: Check if this will create another invoice
            i.save!
        }
   end


    private

    def has_parent_must_has_site
        if ((not parent_invoice.nil?) and (site.nil?))
            errors.add(:base, "Site can't be null when invoice has a parent")
        end
    end

    def client_xor_site
        unless (client.present? ^ site.present?)
            errors.add(:base, "Site or client must be specified, but not both")
        end
    end


end
