class HourlyStatsPeriod < StatsPeriod

    # returns a scope for type which is below this kind
  def parent_period
    @parent ||= StatsPeriod.find_or_create_with_hour(day, nil)
  end

end

