class Service < ApplicationRecord
include Montbeau::Activatable

include ActsAsEncryptedWithGpgme




# IMPORT_MAPPING AT BOTTOM OF THIS CLASS

belongs_to :line_of_business, foreign_key: 'line_of_businesses_id'

cattr_accessor :lookupdebug

belongs_to :btn

belongs_to :site

belongs_to :phone_service_type

belongs_to :rate_plan


#belongs_to :department_line_of_business, :class_name => 'LineOfBusiness' #We don't have ref data for this yet
belongs_to :customer_line_of_business, :class_name => 'LineOfBusiness'

belongs_to :coverage

has_many :project_codes

has_many :outgoing_cdrs,
         :class_name => 'Cdr',
         :foreign_key => 'callingphone_id'

has_many :incoming_cdrs,
         :class_name => 'Cdr',
         :foreign_key => 'calledphone_id'

has_many :outgoing_calls,
         :class_name => 'Call',
         :through => :outgoing_cdrs,
         :source => :call

has_many :incoming_calls,
         :class_name => 'Call',
         :through => :incoming_cdrs,
         :source => :call

belongs_to :usage_type

belongs_to :resultant_rate_plan, :class_name => 'RatePlan'
belongs_to :local_rate_plan,    :class_name => 'RatePlan'
belongs_to :national_rate_plan, :class_name => 'RatePlan'
belongs_to :world_rate_plan,    :class_name => 'RatePlan'
belongs_to :rate_center

         belongs_to :supplier,         :class_name => 'DIDSupplier'

         has_many :billable_services, foreign_key: 'asset_id'

         after_commit   :clear_id_cache
         after_destroy  :clear_id_cache

         scope :sort_by_order, -> { order(:service_id) }

         scope :by_extension,
         -> { order(:service_id, :extension) }

scope :voicemailportal,
      -> { where(:voicemailportal => true) }

scope :voicemailnumber,
      -> { where(:voicemail_number => 'YES')}

scope :by_site_name,
      -> { joins(:site).order('sites.name ASC, services.service_id ASC, services.extension ASC') }

acts_as_encrypted_with_gpgme :fields => {
                                  :sip_password => {
                                      :recipients => [$SIP_GPGKEY],   # key ID for the key generated above
                                          #:key => $SIP_GPGKEY,
                                  }
                              }, :auto_decrypt => false

has_many   :callstats do
# returns scope or array, so really it should be "all"
# not clear yet how to refactor this to use above.
    def find_or_create_for_today(today = Time.now, hour = nil, stattype = 'CallCallstat', notify=false)
    sp = StatsPeriod.find_or_create_with_hour(today, hour)
    client = proxy_association.owner.try(:btn).try(:client) || proxy_association.owner.try(:site).try(:client)
return Callstat.create_for_period(sp, stattype, notify, proxy_association.owner, client)
    end
    end

    def btn_name
    if btn
    btn.billingtelephonenumber
    else
    service_id
    end
    end

    def find_rate_center
rate_center || btn.try(:find_rate_center)
    end

    def name(space=" ")
    if virtualdid
    if extension.blank?
    "#{btn_name}"
    else
    "#{btn_name}#{space}x#{extension}"
    end
    else
    service_id
    end
    end

    def identification
    unless firstname.blank? and lastname.blank?
    string = "#{firstname} #{lastname}"
    else
    string = notes
    end
    if string
    string = string[0..50]
    end
    return string || ""
    end

    def preload_site!
@site = self.site = Site.get_by_id(self[:site_id])
    rescue ActiveRecord::AssociationTypeMismatch
@site = self.site = Site.find(self[:site_id])
    end

    def site
    unless @site
    preload_site!
    end
    return @site
    end

    def client
    @client ||= btn.client || site.client
    rescue NoMethodError
    nil
    end

    def to_s
        "{#{id}}#{display_text} (#{service_id})"
    end

    def callerid
    return self[:callerid] || btn_name
    end

    def delete_with_references
    incoming_cdrs.find_each {|cdr| cdr.calledphone = nil; cdr.save}
    outgoing_cdrs.find_each {|cdr| cdr.callingphone = nil; cdr.save}
    clear_id_cache
    delete
    end





    ### Synonyms BEGIN #################################################################################
    # For class renames that happened 2018/03-2018/04
    def did_supplier
        self.supplier
    end


    def activated_at
        self.start_date
    end

    def activated_at=(value)
        self.start_date = value
    end

    def deactivated_at
        self.termination_date
    end

    def deactivated_at=(value)
        self.termination_date = value
    end

    def phone_number=(value)
        self.service_id=value
    end

    def did_supplier_id
        self.supplier.try(:id)
    end

    def foreign_pid
        self.service_pid
    end

    def phone_number
        self.service_id
    end
    ### Synonyms END ###################################################################################

    def phone_key
self.class.phone_key(service_id, client ? client.id : 0, termination_date)
    end

    def self.phone_key(phoneno, client_id = nil, termination_date = 'A')
    "phoneno:#{phoneno} #{termination_date}"
    end

    def write_cache!
    if client
Rails.cache.write(phone_key, self)
    #puts "Wrote #{phone_key} => #{id} to cache"
    end
    end

    # tests for properties.
    # voicemailportal?       -- true if this extension goes to voicemailportal.

    # true if this extension should get credited with calls involving it that
    # go to voicemail.
    def voicemail_number?
    'yes' == voicemail_number.try(:downcase)
    end

    # does this extension get credit (in a hunt group) for voicemail calls?
    def voicemail_credit?
    voicemail_number? and !voicemailportal?
    end

    def huntgroup!
    self.huntgroup = true
    save!
    end

    def self.preload_phones
    cachetotal = 0
    activated.where(:autocreate => false).includes([:site => :client, :btn => :client]).find_each do |phone|
    #phone.preload_site!
    phone.write_cache!
    cachetotal += 1
    $0 = sprintf("import preloading phone: %06u", cachetotal)
    end
    end

    # this just removed non-numeric items
def self.phonecanonify(phoneno)
    phoneno.gsub(/[\(\)\- ]/, '')
    end

    def self.find_phoneno(client, phoneno, termination_date)
    phoneno = phonecanonify(phoneno)
key     = phone_key(phoneno, client.id, termination_date)
    p1 = Rails.cache.fetch(key) do
    n1 = if termination_date
    client.services.where(service_id: phoneno, termination_date: deactivated_at).take
    else
    client.services.activated.where(service_id: phoneno).take
    end
    #puts "looking for ]#{key}[, found: ]#{n1.try(:id)}["
    n1
    end

    # do not cache items which are not found
    Rails.cache.delete(key) unless p1
    return p1
    end

def self.find_or_make(client, btn, phoneno, deactivated_at)
    deactivated_at = nil if deactivated_at.blank?
    if deactivated_at
p1 = find_phoneno(client, phoneno, deactivated_at)
    end
    unless p1
p1 = find_phoneno(client, phoneno, nil)
    end

    unless p1
p1 = create(service_id: phoneno,
        btn: btn, termination_date: deactivated_at)
    p1.save
    p1.clear_id_cache
    end
    p1
    end

    def self.autocreate(number)
vnumber = vdidify(number)
    now = DateTime.now
    p1 = lookup_vdid_record_for(number, DateTime.now, "autocreate")
    unless p1
p1 = create(:service_id => vnumber,
        :sip_username => vnumber,
        :activated_at => now.beginning_of_month,
        :btn    => Btn.lastresort,
        :site   => Btn.lastresort.try(:client).try(:billing_site),
        :autocreate => true)
    $NumberOfPhonesAutocreated += 1
    p1.save
    p1.write_cache!
    #byebug
    puts "auto-created phone: #{p1.service_id} #{p1.id}" if $AutoCreatePhoneDebug
    end
    return p1
    end

def self.vdidify(number)
    # first remove beginning and ending quotes, if any.
    return "" if number.blank?
number1 = phonecanonify(number)
    number1 = number1[/\d+/]

    return "" if number1.blank?
    # and leading +, code.
    if '+1' == number1[0..1]
    number1[0..1] = ""      # shortens string.
    end

    # and leading country code 1.  XXX should store the 1.
    if '1' == number1[0..0]
    number1[0..0] = ""      # shortens string.
    end
    return number1
    end

    def self.lookup_vdid_record_for(number, onday = DateTime.now, reason="")
    vnumber = vdidify(number)
p1 = Rails.cache.read phone_key(number, 0, nil)
    return p1 if p1

    # do not continue if we have a blank value.
    return nil if vnumber.blank?

    p1 = Service.activated(onday).where(sip_username: vnumber).take
    unless p1
    p1 = Service.activated(onday).where(service_id: vnumber).take
    end
    if p1
    #puts "lookup loading cache: #{p1.phone_key} to #{p1.id}"
    p1.write_cache!
    else
    puts "looked for #{number} #{reason} and failed" if @@lookupdebug
    end
    return p1
    end

    def self.start_import
    @@lastclient = nil
    @@lastsite   = nil
    @@lastbtn    = nil
    @@lastdept   = nil
    end

    def self.end_import
    # clears all the caches
    Rails.cache.clear
    end

    def self.yes_no_string(string)
case string.try(:downcase)
    when 'yes'
    true
    when 'no'
    false
    when 'y'
    true
    when 'n'
    false
    when 'oui'
    true
    when 'non'
    false
    else
    false
    end
    end

def self.increment_stats_and_return(stats, statname)
    stats[statname] ||= 0
    stats[statname] += 1
    return false
    end

    ########################################################################
    # Deactivate_at synonym of termination date 2018/04/11
    ########################################################################
    def deactivated_at
    return termination_date
    end

def self.import_row(row, lineno = 0, override_client = false, stats = Hash.new(0))
    # save this line, and a new line from Nick's database to a CSV file,
    # and examine it in a spreadsheet program for sane diff
    # Revendeur,Client,BTN,"PBX Type",DNS,"Department Number",Site,"Outgoing caller ID ",Numéro,"Virtuel DID","Poste tél.","Boite vocale",Access,"Appel International access","Services / Group","Non-Reachable Number",Prénom,"Nom de Famille",Notes,"Date d'activation","Fin de   service","Authentication Device Access User Name","Authentication Device Access Password",Paging,Licenses,"MAC address","Type device","SIP U/N",,Codec,"IP address","Page Multicast","Tested 911","VOIP Supplier Couverage","Courriel Contact","Type de connexion","Time allocated in Minutes 30 days","Time allocated in $",Palladion

    (provider, name,    btnname,    pbx_type,  dns_access, deptnum, sitename,
     callerid, phoneno, virtualdid, extension, voicemail,
     access,   international_access, service_group, nonreachablenumber,
     firstname, familyname,  notes, activated_at, deactivated_at,
     auth_dev_username, auth_dev_password, paging, licenses, mac_address,
     device_type, sip_username, sip_password, codec, ip_address,
     page_multicast, e911_notes, voip_supplier,     email,
     connection_type, time_allocation_monthly, time_allocated_cost,
     palladion) = row

    return increment_stats_and_return(stats,:headerline) if btnname == 'BTN'  # header column.
    return increment_stats_and_return(stats,:titleline)  if name == 'Liste de clients VoIP'   # title sequence
    # skip comments
    return increment_stats_and_return(stats,:comment)    if name =~ /^\#/;
    return increment_stats_and_return(stats,:phoneno_blank) if phoneno.blank?

    # now set some defaults from previous row.
    #mapping = Hash.new

    btn    = @@lastbtn          if btnname.blank? or btnname == 'n/a'
if(btnname.blank? && !btn)
    errorlog("[#{lineno}] Failed to import #{phoneno}, btnname is blank and no previous btn\n")
return increment_stats_and_return(stats, :btnnameblank)
    end

    unless btn
    cbtnname = Btn.canonical_btn(btnname)
    client = Client.find_by_crm_account_id(cbtnname)
btn = Btn.get_by_billingtelephonenumber(cbtnname)
    end

    unless client
client = btn.try(:client)
    end

    unless client
    errorlog("[#{lineno}] Failed to import #{phoneno}, client #{name} not found by btnname #{cbtnname}\n")
return increment_stats_and_return(stats, :clientnotfoundbybtn)
    end

if(@@lastclient != client)
    @@lastbtn = nil
    @@lastsite= nil
    @@lastdept= nil
    end

    maybelog("#{lineno}: client[#{client.id}] for #{name}")
    #puts "1 client: #{client} frozen=#{client.frozen?} changed=#{client.changed?} unsaved=#{client.unsaved?} id=#{client.object_id}"

    client.add_mycommunications_service!

    if deptnum.blank? and sitename.blank?
    deptnum = @@lastdept
    end

    unless deptnum.blank?
site   = client.sites.find_by_site_number(deptnum)
    if !site and !sitename.blank?
site   = client.sites.find_by_name(sitename)
    end
    end
    unless site
site   = @@lastsite         if (sitename.blank? and deptnum.blank?)
    end

    unless site
    maybelog(sprintf("new site: lineno %u client: %s sitename: %s\n", lineno, client, sitename))
    if ENV['INTERACTIVE_SITE_ASSIGNMENT'] && client.sites.count > 0
    # show list of sites
    sitelist = client.sites
    sitenum  = 1
    sitelist.each { |s|
        print sprintf("%3u. _%20s_ %s\n", sitenum, s.site_number, s.name)
            sitenum = sitenum + 1
    }
print sprintf("Pick: (1-%u)", sitenum-1)
number = nil
loop do
number = $stdin.gets.chomp
    number = number.to_i
break unless (number < 1 || number >= sitenum)
    end
    site = sitelist[number - 1]
    print sprintf("Picked site: %20s %s\n", site.site_number, site.name)
    else
site = client.clienthq(sitename, deptnum)
    end
    end
    maybelog("#{lineno}: site[#{site.id}] for #{sitename}")
    @@lastsite  = site
    @@lastdept  = site.site_number
    unless deptnum.blank?
    site.site_number = deptnum
    end
site.save!(validate: false)

    @@lastclient= client
    @@lastbtn   = btn

    return increment_stats_and_return(stats, :clientandbtnblank) unless client && btn

    # ignore line if phone number or sip_username is nil.
    return increment_stats_and_return(stats, :phoneno_na) if phoneno == "n/a"

    # make sure to update termination date as it marks the phone as still in use
    if deactivated_at.blank?
    term_date = nil
    else
    begin
    term_date=deactivated_at.to_date
    rescue ArgumentError
    term_date=Time.now
    end
    end

    ## Just a note that Phone class has been renamed to Service (t13331)
    ## I've kept variable names the same though
phone = find_or_make(client, btn, phoneno, term_date)
    unless phone
    phone = create
    end

    phone.service_id = phoneno
    phone.callerid     = callerid
    phone.extension   = extension
phone.virtualdid  = yes_no_string(virtualdid)
    phone.voicemail_number   = voicemail            # needs to be replaced with boolean!
    phone.access_info = international_access
    phone.notes       = notes
    phone.activated_at= activated_at

    phone.deactivated_at = term_date
    mac_address.downcase!     if mac_address
    if(mac_address != 'n/a')
    phone.phone_mac_addr = mac_address
    end

    case service_group
    when 'Voice Portal'
    phone.voicemailportal = true
    else
    phone.voicemailportal = false
    end

    #phone.phone_device_type= device_type    # needs to be mapped.
    phone.sip_username=sip_username
    phone.sip_password=sip_password
    phone.e911_notes  =e911_notes
    #phone.voip_supplier=voip_supplier       # needs to be mapped.
    if !email.blank? and email != 'n/a'
    phone.email       = email
    # now make sure that BTN is on list for this client.
    end
    #phone.ip_address  =ip_address
    #phone.connection_type=connection_type   # needs to be mapped.

    phone.btn    = btn
    if phone.site != site and phone.site and site
    errorlog("client(#{client.name}/#{btn.billingtelephonenumber}) moving #{phone.service_id} from '#{phone.site.site_number}' to #{site.site_number}")
    end

    phone.site   = site
    phone.save!

    # this is deferred until the phone is succesfully saved.
    #puts "2 client: #{client} frozen=#{client.frozen?} changed=#{client.changed?} unsaved=#{client.unsaved?} id=#{client.object_id}"
    # client is frozen because of JOIN done above.
    if client.frozen?
    if client.id
client = Client.find(client.id)
    else
    client = client.dup
    end
    end
    unless client.billing_site
    client.billing_site = site
    end
    #puts "3 client: #{client} frozen=#{client.frozen?} changed=#{client.changed?} unsaved=#{client.unsaved?} id=#{client.object_id}"
    client.client_crm_account_code_fix
    if phone.deactivated_at.blank? and !phone.email.blank? and !site.site_rep
    site.make_otrs_user if $OTRS
    end

    if client.changed?
    # fix the client/crm_account count ID, might have a BTN for the account code now.
    client.save!
    end

    phone
    end

    # create a single line for the phonecfg CSV file.
    # note, we need access to the GPG key to decrypt!
    # if there is no ENV['GPG_AGENT'] set, then do not retrieve password.
def phonecfg_csv(csv)
    return if sip_password.blank?
    pw = if ENV['GPG_AGENT_INFO']
    begin
    #GPGME::decrypt(sip_password, nil)
GPGME::decrypt(GPGME::Data.new(sip_password), nil)
    rescue GPGME::Error::NoData
    'emptypw'
    rescue
    #debugger
    'brokenpw'
    end
    else
    'fakepw'
    end
    return if mac_address.blank?
    csv << [extension, mac_address.upcase, sip_username, pw, 'voip.novavision.ca', 'French_France', notes]
    end

def self.import_csv(file, override_client = false, stats = Hash.new(0))
    Service.start_import

    count = 0
    lineno= 1

    handle = File.open(file, "r:UTF-8")

    CSV.foreach(handle) do |row|
if ph=Service.import_row(row, lineno, override_client, stats)
    count += 1
    end
    if (ENV['IMPORT_DEBUG'] || (lineno%16)==1) and not $NOLOG_TESTING
    @@newlineneeded = true
    $stderr.printf("\rprocessing line %04d - %s\r", lineno,
            (ph ? ph.try(:service_id)[0..19]:nil) || " " * 20)
    end
    lineno += 1
    end
    return count,stats
    end

    def has_service(service)
client_services.includes?(service)
    end

    def savefixturefw(fw)
return if save_self_tofixture(fw)

    # now for related items.
    self.btn.savefixturefw(fw) if self.btn
    #self.cdrs.each { |cdr| cdr.savefixturefw(fw) }
    end

def total_call_count(today = Time.now, hour = nil, recount = false, zerodrop = true, notify=false)
    return outgoing_call_count(today, hour, recount, zerodrop, notify) +
incoming_call_count(today, hour, recount, zerodrop, notify)
    end
    def call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
return total_call_count(today, hour, recount, zerodrop, notify)
    end

def value_recount(recount, stats)
    sf = stats.first
    if sf
    #puts "count: #{sf.id} sp:#{sf.stats_period_id} type:#{sf.type} value: #{sf.value}"
return sf.maybe_recount(recount)
    else
    #puts "value zero"
    return 0
    end
    end

    # too many options... time for options hash.
def outgoing_call_count(today = Time.now, hour = nil, recount = false, zerodrop = true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "OutgoingCallStat", notify))
    end

def incoming_call_count(today = Time.now, hour = nil, recount = false, zerodrop = true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "IncomingCallStat", notify))
    end

def didin_call_count(today = Time.now, hour = nil, recount = false, zerodrop = true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "DidCallstat", notify))
    end

def huntgroupin_call_count(today = Time.now, hour = nil, recount = false, zerodrop = true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "HuntGroupCallstat", notify))
    end

def incoming_voicemail_count(today = Time.now, hour = nil, recount = false, zerodrop = true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "VoiceMailCallstat", notify))
    end
    alias voicemail_count incoming_voicemail_count

def answered_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "AnsweredCallStat", notify))
    end

def internal_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "InternalCallstat", notify))
    end

def vmcollected_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "VMcollectedCallstat", notify))
    end

def incoming_abandoned_count(today = Time.now, hour = nil, recount = false, zerodrop = true, notify=false)
    value_recount(recount,
            callstats.find_or_create_for_today(today, hour, "AbandonedCallstat", notify))
    end
    alias abandoned_count incoming_abandoned_count

    def clear_id_cache
    Rails.cache.delete phone_key
    end

    # clean up duplicate phone entries caused by multiple import of deleted entries.
    def self.remove_duplicate_deleted!
    # select for a list of duplicates wh
    limit = 0
    self.connection.execute(<<-SQL).each { |thing|
        SELECT p1.id,p1.service_id,p1.count,p1.deactivated_at
            FROM (
                    SELECT MIN(id) AS id,service_id,
                    COUNT(service_id) AS count,
                    MIN(deactivated_at) AS deactivated_at
                    FROM #{table_name} -- NOTE: This could present a vulnerability
                    WHERE deactivated_at IS NOT NULL
                    GROUP BY service_id
                    ORDER BY count DESC) AS p1
            WHERE count > 1
            SQL
            # thing has records id, service_id, count and deactivated_at

            # first try a mass deletion, this will fail if some are referenced by foreign keys.
            begin
            self.where.not(deactivated_at: nil, id: thing['id']).where(service_id: thing['service_id']).delete_all

            rescue ActiveRecord::InvalidForeignKey
            true
            end

            # now try individual items, which is much slower.
            self.deactivated.where(service_id: thing['service_id']).find_each {|dupphone|
                if dupphone.id != thing['id'].to_i
                    begin
                        dupphone.delete
                        rescue ActiveRecord::InvalidForeignKey
                        true
                        end
                        end
            }
    }
end

# Rate Plans
    def local_plan
        local_rate_plan || btn.try(:local_rate_plan)
    end
    def national_plan
        national_rate_plan || btn.try(:national_rate_plan)
    end
    def world_plan
        world_rate_plan || btn.try(:world_rate_plan)
    end

    IMPORT_PRIMARY_KEY = :service_pid
    IMPORT_MAPPING = {
        #client: {
            #    :csvHeader => 'BTN',
            #    :code       => Proc.new {|csvValue| find_client_by_btn(strip_btn_leading_zeros(csvValue))}, },
        '#BTN' =>     'BTN',
        '#ServicePID' =>     'ServicePID',
        btn: { # Assocation
            :csvHeader => "BTN",
            :code       => Proc.new {|csvValue| Btn.find_by(billingtelephonenumber: csvValue)  }, },
        display_text: 'DisplayText',
        description: 'Description',
        comments:   'Comments',
        privacy: 'Privacy', #{
            #:csvHeader  => 'Privacy',
            #:code       => Proc.new {|csvValue| csvValue == "1" } },
        supplier: { # Assocation
              :csvHeader => 'Supplier',
              :code      => Proc.new {|csvValue|
                  sup = DIDSupplier.find_by(:pid => csvValue)
                      sup = DIDSupplier.find_by(:id => csvValue) if sup.nil?

                      sup
              } ,
          },
        phone_service_type: { # Assocation
            :csvHeader => 'ServiceTypePID',
            :code      => Proc.new {|csvValue| PhoneServiceType.find_by(:id => csvValue)}, },
        ipaddress: 'IPAddress',
        sip_password: 'Password',
        insert_app_user_pid: "InsertAppUserPID",
        update_app_user_pid: "UpdateAppUserPID",
        topic_pid:          "TopicPID",
        private_line:       "PrivateLine",
        owner_app_user_pid: "OwnerAppUserPID",

        default_service:    "DefaultService",
        close_finalised:    "CloseFinalised",
        billed:             "Billed",
        default_origination:"DefaultOrigination",
        default_destination:"DefaultDestination",

        provision_later: {
            :csvHeader  => 'ProvisionLater',
            :code       => Proc.new {|csvValue| csvValue == "1" } },
        validate_project_code: {
            :csvHeader => 'ValidateProjectCode',
            :code => Proc.new {|csvValue| csvValue == "1" } },
        phone_mac_addr: 'MacAddress',
        created_at: {
            :csvHeader  => 'CreatedOn',
            :code       => Proc.new {|csvValue| convert_to_date(csvValue)}, },
        updated_at: {
            :csvHeader  => 'UpdatedOn',
            :code       => Proc.new {|csvValue| convert_to_date(csvValue)}, },
                    #rate_center: {
                        #    :csvHeader  => nil #RatePlan.where(:Pid => ratecenter), # not implemented
                        #},
                    #defaultorigination: 'DefaultOrigination', ? From jjrh's code, but not a Phone attribute
                    #defaultdestination: 'DefaultDestination', ? From jjrh's code, but not a Phone attribute
        department_line_of_business_pid: "DepartmentLineOfBusinessPID",
        line_of_business: { # Assocation
            :csvHeader  => 'LineOfBusinessPID',
            :code       => Proc.new {|csvValue| LineOfBusiness.find_by(Pid: csvValue) }, },
        service_id: 'ServiceID',
        service_pid: 'ServicePID',
        service_assignment_type_pid: "ServiceAssignmentTypePID",
        department_pid: 'DepartmentPID',
        supplier_activation_date: {
            csvHeader: 'SupplierActivationDate',
            code:       Proc.new {|csvValue| (csvValue == "00:00.0" or csvValue.blank?) ? nil : Date.parse(csvValue)},
        },
        last_usage_date: 'LastUsageDate',
        service_usage_type_pid: 'ServiceUsageTypePID',
        usage_type: 'UsageTypePID',
        customer_category_pid: 'CustomerCategoryPID',
        current_account_status_type_pid: 'CurrentAccountStatusTypePID',
        rate_plan:  'RatePlanPID',
        resultant_rate_plan: 'ResultantRatePlanPID',
        start_date: {
            :csvHeader => 'StartDate',
            :code       => Proc.new {|csvValue| convert_to_date(csvValue)}, },
        termination_date: {
            :csvHeader => 'TerminationDate',
            :code       => Proc.new {|csvValue| convert_to_date(csvValue)}, },
        coverage: 'Coverage',
        customer_line_of_business_pid: "CustomerLineOfBusinessPID",
        imported_at: {
            :exec       => Proc.new {DateTime.now}
        },
  }


  IMPORT_DONT_UPDATE = true
  IMPORT_START_EMPTY = true


end
