class ProductType < ApplicationRecord
    IMPORT_MAPPING = {
        :name               => "Code",
        :product_type_pid    => "ProductTypePID"
    }

    IMPORT_PRIMARY_KEY = :product_type_pid
end
