class Ability
  include CanCan::Ability

  def initialize(user, format = nil)
    user ||= User.new # Guest user
    format ||= 'html'
    format = format.to_s

    Rails.logger.debug "Access ability setup for #{user.email}, format=#{format}"

    #debugger
    case format
    when 'json'
      if user.admin?
        can :manage, :all
      elsif user.agent?
        can :agent,  :all
        can :read,   User
      else
        can :read,   User,    :id => user.id
        can :update, User,    :id => user.id
      end

    when 'html'
      if user.admin?
        # Admins have all privileges
        can :manage, :all
      elsif user.agent?
        # Agents can see other users but not edit them
        can :read,   User
      else
        # Users should be able to see their own page
        can :read,   User,    :id => user.id
        can :update, User,    :id => user.id

        # Any user should be able to see the landing page
        can :read,   Dashboard
      end

    when 'pdf'
        if user.admin?
          # Admins have all privileges
          can :manage, :all
        elsif user.agent?
          # Agents can see other users but not edit them
          can :read,   User
        else
          # Users should be able to see their own page
          can :read,   User,    :id => user.id
          can :update, User,    :id => user.id

          # Any user should be able to see the landing page
          can :read,   Dashboard
        end

    else
      # raise unknown format
      raise "Unknown format: #{format}"
    end
  end
end
