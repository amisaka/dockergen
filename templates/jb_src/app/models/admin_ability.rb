class AdminAbility
  include CanCan::Ability

  def initialize(user, format)
    user ||= User.new # Guest user
    format ||= 'html'
    format = format.to_s

    Rails.logger.debug "Admin access ability setup for #{user.email}, format=#{format}"

    #debugger
    case format
    when 'json'
      if user.admin?
        can :manage, :all
      elsif user.agent?
        # nothing here.
      else
        # nothing here.
      end

    when 'html'
      if user.admin?
        can :manage, :all
      elsif user.agent?
        # nothing here
      else
        # nothing here.
      end

    else
      # nothing, set no permissions
      puts "Unknown format: #{format}\n"
    end
  end
end
