class GlAccount < ApplicationRecord
    IMPORT_MAPPING = {
        :code           => "Code",
        :description    => "Description",
        :active         => "Active",
        :gl_account_entry_type_pid => "GLAccountEntryTypePID",
        :gl_account_pid => "GLAccountPID",
    }

    IMPORT_PRIMARY_KEY = :gl_account_pid

    def gl_account_label
      if self.description != " "
        return "#{self.code} : #{self.description}"
      else
        return "#{self.code}"
      end
    end
end
