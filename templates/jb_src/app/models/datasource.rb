ANALYSIS_LEVEL=6

class Datasource < ApplicationRecord
  self.table_name = "cdrviewer_datasource"
  # self.primary_key = 'id'

  has_many :cdrs
  has_many :calls
  belongs_to :did_supplier

  scope :by_filename, -> { order(:datasource_filename) }
  scope :by_newest,   -> { order(datasource_filename: :desc) }

  def self.find_or_make(name)
    ds = self.find_by_datasource_filename(name)
    unless ds
      ds = self.new(:datasource_filename => name)
      ds.set_default_analysis_level!
      ds.save!
      # this is stupid. But, id= isn't getting set on the save!
      ds = self.find_by_datasource_filename(name)
    end
    ds
  end

  def name
    "#{datasource_filename}"
  end

  def set_default_analysis_level!
    self.analysis_level = ANALYSIS_LEVEL
  end

  def connection
    ActiveRecord::Base.connection
  end

  def calls_delete_all
    #calls.delete_all
    # above too slow, seems to load all the calls in rails 3.0
    ds_id = connection.quote(id)
    connection.execute "delete from calls where calls.datasource_id = #{ds_id}"
  end

  def cdrs_delete_all
    #calls.delete_all
    # above too slow, seems to load all the calls in rails 3.0
    ds_id = connection.quote(id)
    connection.execute "delete from cdrviewer_cdr where cdrviewer_cdr.datasource_id = #{ds_id}"
  end

  def redo_callstats
    # with_lock is available in rails 3.2+
    transaction do
      lock!
      puts "  ..deleting calls for datasource id#{id} (count=#{calls.count})"
      calls_delete_all

      puts "  ..creating new calls for datasource id#{id}"
      cdrs.find_each { |cdr|
	cdr.associate_to_call_save!
      }
      self.analysis_level = ANALYSIS_LEVEL
      puts "  ..call count for datasource id#{id} is count=#{calls.count}\n"
      save!
    end
  end

  # Update legs assocation into calls according to <2hr rule (legs must be less than 2hr apart)
  # Params:
  # +yearMonth+:: String in format YYYYMM to limit to a specific month
  def self.update_call_leg_association(yearMonth = nil, an_level = ANALYSIS_LEVEL)


    if /[[:digit:]]{6}/.match(yearMonth)
      whereClause = ["datasource_filename LIKE ? AND (analysis_level < ? OR analysis_level IS NULL)", "BW-CDR-#{yearMonth}%", an_level]
    else
      whereClause = ['analysis_level < ? OR analysis_level IS NULL', an_level]
    end


    puts "Running query " + self.where(whereClause).to_sql if not $NOLOG_TESTING
    datasourceTotal  = self.where(whereClause).count
    puts "Found #{datasourceTotal} records" if not $NOLOG_TESTING

    datasourceCount = cdrCount = 0
    startTime       = Time.now
    lastAnnounce    = Time.now

    query = self.lock.where(whereClause)
    #debugger
    query.find_each { |ds|
      ds.cdrs.find_each { |cdr|
          cdr.associate_to_call_save!

          cdrCount += 1

          if Time.now - lastAnnounce > 5
              timeElapsed = Time.now - startTime;

              ett = (datasourceTotal - datasourceCount)*(timeElapsed/[1, datasourceCount].max)
              eta = Time.now + ett

              if not $NOLOG_TESTING
                puts sprintf("%d cdrs, %d datasources (%.1f %%), %d seconds - %.1f cdrs/s, %.1f secs/ds - ETT: %.1f hrs, ETA: %s",
                             cdrCount, datasourceCount, datasourceCount / datasourceTotal * 100, timeElapsed,
                             (cdrCount/[1, timeElapsed].max).round(1), (timeElapsed/[1, datasourceCount].max).round(1),
                             ett/60/60 , eta
                            )
              end

              lastAnnounce = Time.now
          end

      }

      ds.analysis_level = an_level
      ds.save

      datasourceCount += 1
    }
  end

  def self.old_analysis
    where(['analysis_level < ? OR analysis_level IS NULL', ANALYSIS_LEVEL])
  end

  def self.update_analysis(active_day = DateTime.now.yday)
    newone = nil
    begin
      # within a transaction, find a DS which is not active today,
      # and which needs work.
      newone = nil

      self.transaction do
	self.lock.old_analysis.find_each { |ds|
	  unless ds.active_day == active_day
	    # found one that is not active today
	    ds.active_day = active_day
	    ds.save!
	    newone = ds
	    break
	  end
	}
      end

      if newone
	puts "Working on datasource id\##{newone.id} filename=#{newone.datasource_filename}"
	newone.redo_callstats
      end
    end while newone
  end

  def recalculate_toll_data
    dsprocessed = false
    cdrs.find_each { |cdr|
      oldcalltype = cdr.networkcalltype
      cdr.calculate_toll_type
      if oldcalltype == cdr.networkcalltype
        cdr.save!
        dsprocessed = true
      end
    }
    set_default_analysis_level!
    save!
    return dsprocessed
  end

  # point at a file where editing command can go.
  def increment_recalculate_count
    self.toll_recalculate_count ||= 0
    self.toll_recalculate_count +=  1
  end

  def calc_toll_edit_file(dir = nil)
    dir ||= ENV['CDR_OUTDIR'] || Rails.root.join('tmp','cdredit')
    @destdir = dir
    FileUtils.mkdir_p(dir)

    @basename = File.basename(datasource_filename, ".*")
    script   = File.join(dir, datasource_filename + ".sh")
  end
  def toll_edit_file(dir = nil)
    @edit_file_name ||= calc_toll_edit_file(dir)
  end

  # return a file handle to write commands to.
  def calc_toll_edit_handle(dir = nil)
    handle = File.open(toll_edit_file(dir), "w")
    handle.puts sprintf("#!/bin/sh\n# created %s datasource=%u\n\n",
                        DateTime.now.to_s, self.id)
    handle
  end

  def toll_edit_handle(dir = nil)
    @edit_file_handle ||= calc_toll_edit_handle(dir)
  end

  def write_edit_command(cdr, toll = nil)
    return unless @destdir
    unless @somecommand
      toll_edit_handle.puts sprintf("awk <${CDRSRC}%s -- 'BEGIN {FS=\",\";OFS=\",\"; n=20 }\n",
                                    datasource_filename)
    end
    @somecommand = true
    toll ||= cdr.networkcalltype
    toll_edit_handle.puts "NR==#{cdr.lineno} { $n = \"#{toll}\"; print; next; } # #{cdr.recordid} #{cdr.billingreason} #{cdr.callingnumber} #{cdr.callednumber} #{cdr.orig_networkcalltype}>#{toll}\n"
  end

  def edit_active?
    @destdir
  end

  def close_edit_handle
    if @destdir
      # if somet things went in, then finish the output
      outputfile = File.join(@destdir, @basename)
      if @somecommand
        toll_edit_handle.puts sprintf("'> %s.bs290\n", outputfile)
      else
        toll_edit_handle.puts sprintf("ln ${CDRSRC}%s %s.bs290\n",
                                      datasource_filename, outputfile)
      end
      toll_edit_handle.close
    end
  end

  def reprocess_cdrs(always_save = false)
    cnt = 0
    puts "#{File.basename(toll_edit_file)}"

    $0 = sprintf("beaumont: reprocessing file %s  %u/%u", datasource_filename, 0, cdrs_count)
    cdrs.find_each { |cdr|
      cdr.lookup_phones
      newcategory = cdr.record_toll_type_change(true)
      $0 = sprintf("beaumont: reprocessing file %50s %5u/%5u", datasource_filename, cnt, cdrs_count)
      if (newcategory and cdr.orig_networkcalltype != newcategory) or always_save
        cdr.save!
      end
      cnt += 1
    }
    close_edit_handle
    save!
    cnt
  end


  def self.do_analysis_six
    count = 0
    Datasource.old_analysis.find_each {|ds|
      # using unscoped keeps postgresql from retrieving all the CDRs, when we just care about
      # some CDR to get the startdatetime out of it.  Datasources usually have <15 minutes of data
      # and mostly we care about the day and hour of the datasource.
      a = ds.cdrs.reorder(nil).first

      if (count%100)==0
        puts sprintf('%05d analysis_six: #%05d', count, ds.id)
      end
      # there may be no cdrs for this datasource!
      if a
        ds.data_at = a.startdatetime
      end
      ds.set_default_analysis_level!
      ds.save!
      count += 1
    }
  end


  def self.reset_analysis
    transaction do
      connection.execute "update cdrviewer_datasource set active_day = 0;"
    end
    #self.lock.find_each(:conditions => ['analysis_level < ? OR analysis_level IS NULL', ANALYSIS_LEVEL]) { |ds|
    #  ds.active_day = nil
    #  ds.save!
    #}
  end

end
