class Lerg8 < ApplicationRecord
  # self.primary_key = "id"
  # self.table_name  = "public.lerg8s" if postgresql?

  def name
    "lerg#{id}"
  end

  def self.import_row(line, stats = Hash.new(0))
    (status, prov, shortName, fullName, entryNum) = line.unpack("@26A6@32A2A10@45A50@95A10")
    status.strip!
    prov.strip!
    prov = LergMethods::cleanProvince(prov)
    shortName.strip!
    fullName.strip!

    item = self.find_by_entryNum(entryNum)
    unless item
      # now look for a matching entry by prov+longName
      item = self.where(shortName: shortName, province: prov).first
      stats[:lerg8_updated] += 1 if item
    end

    unless item
      item = create(shortName: shortName, fullName: fullName,
                    province: prov, entryNum: entryNum)
      stats[:lerg8_created] += 1
    end
    return item
  end

  def self.import_file(lerg8file, stats = Hash.new(0))
    count = 0

    total = stats[:lerg8_total]

    File.readlines(lerg8file).each do |line|
      item = import_row(line, stats)
      count += 1
      if stats[:debug]
        if (count % 100) == 0
          print "\rImport at row: #{count}/#{total} created:#{stats[:lerg8_created]} updated: #{stats[:lerg8_updated]}"
        end
      end
    end

    return count
  end

end
