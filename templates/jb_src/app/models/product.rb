class Product < ApplicationRecord

  paginates_per 18

  has_many :billable_services

  # Hakka
  belongs_to :hakka_product, :class_name => 'Hakka::Product', :foreign_key => 'foreign_pid' if $CONNECTMSSQL


  belongs_to :product_class
  belongs_to :product_category
  belongs_to :product_type
  belongs_to :line_of_business
  belongs_to :gl_account

  def update_from_hakka
    hp = self.hakka_product
    self.update(hp.make_service_hash)
  end


  IMPORT_MAPPING = {
        product_pid: "ProductPID",
        code: "Code",
        name: "Name",
        description: "Description",
        active: "Active",
        multi_jurisdictional_taxing: "MultiJurisdictionalTaxing",
        amount: "Amount",
        unit_cost: "UnitCost",
        purchase_amount: "PurchaseAmount",
        product_class: "ProductClassPID",
        product_category: "ProductCategoryPID",
        product_type: "ProductTypePID",
        line_of_business: "LineOfBusinessPID"   ,
        tax_passthrough: "TaxPassthrough"  ,
        gl_account: "GLAccountPID"    ,
        allow_fractional_qty: "AllowFractionalQuantity" ,
        pp_product_pid: "PPProductPID"    ,
        bc_product_pid: "BCProductPID",

      }
    IMPORT_PRIMARY_KEY = :product_pid

  def self.update_from_products_csv(filePath)
    self.update_from_csv(filePath, self::IMPORT_MAPPING, self::IMPORT_PRIMARY_KEY)
  end

  class ANI < Product
  end

  class Dedicated < Product
  end

  class Travel < Product
  end

  class TollFreeSwitchedAndDedicated < Product
  end

  class VirtualDID < Product
  end

end
