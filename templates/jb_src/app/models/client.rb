require 'site_extension'
require 'cdn/province.rb'

# to get LocalRatePlan
require 'rate_plan'
require 'csv'

class Client < ApplicationRecord
  # adds `search` method
  include Searchable

  # allows enabling/disabling
  include Montbeau::Activatable


  COOP_HYDRO = :coophydro.freeze
  COMMERCANT_CHAUDIERE = :commercantChaudière.freeze


  has_many :client_line_of_businesses

  has_many :line_of_businesses,
    through: :client_line_of_businesses

  enum status: {
    unknown: 0,
    pending_start: 1,
    active: 2,
    pending_closed: 3,
    closed: 4
  }

  after_commit   :clear_id_cache
  after_destroy  :clear_id_cache
  before_update :before_update_hook
  has_many :phone_contacts
  has_many :email_contacts
  has_many :technical_contacts

  belongs_to :crm_account
  accepts_nested_attributes_for :crm_account


  has_one :billing
  accepts_nested_attributes_for :billing

  belongs_to :coverage

  has_one :btn
  accepts_nested_attributes_for :btn

  has_many :services, #Phones renamed to Services t13331
    through: :btn

  has_many :sites,
    extend: SiteExtension

  has_many :buildings,
    through: :sites

  has_many :payments

  belongs_to :billing_site,
    class_name: 'Site',
    foreign_key: :billing_site_id
  accepts_nested_attributes_for :billing_site

  has_many :calls
  belongs_to :hakka_customer, :class_name => 'Hakka::Customer', :foreign_key => 'foreign_pid' if $CONNECTMSSQL;
  belongs_to :hakku_customer, :class_name => 'Hakku::Customer', :foreign_key => 'foreign_pid' if $HAKKU;

  # this is a double nested association.
  has_many :incoming_cdrs,
    through: :btn
  has_many :outgoing_cdrs,
    through: :btn

  # a list of invoices
  has_many :invoices

  # Sales person
  belongs_to :sales_person

  has_many :billable_services

  has_many :statement_type_clients

  has_many :statement_types, through: :statement_type_clients

  has_one :invoice_template,
    through: :btn

  delegate :payment_method,
    to: :last_payment,
    allow_nil: true



  def human_validated?
    !!human_validation_date
  end

  scope :human_validated, -> { where.not(human_validation_date: nil) }

  validates :language_preference,
    presence: true,
    format: { with: /\A(French|English)\z/ }

  after_initialize -> { self.language_preference = 'French' },
    if: -> { self.language_preference.blank? }

  delegate :billingtelephonenumber, to: :btn, allow_nil: true

  # TODO: remove this
  # before_save -> { btn.save unless btn.nil? }

  include Montbeau::Client::SiteCollectionMethods
  include Montbeau::Client::PhoneCollectionMethods
  include Montbeau::Client::BillableServiceCollectionMethods

  scope :invoiceable, -> { joins(:btn).where('btns.invoicing_enabled = ? AND btns.activated = ?', true, true)}

  has_many   :callstats do
    def peakCallCount(period)
      find_all_or_create_for_statsperiod(period, 'PeakCallCountStat')
    end

    def count_for_statsperiod(sp, stattype, clientid)
      if clientid
        cs = proxy_association.owner.callstats.where(:client_id => clientid, :stats_period_id => sp.id, :type => stattype)
      else
        cs = proxy_association.owner.callstats.where(:stats_period_id => sp.id, :type => stattype)
      end
      return cs.try(:count) || 0
    end

    def find_all_or_create_for_statsperiod(sp, stattype='CallCallstat')
      cs = proxy_association.owner.callstats.where(:stats_period_id => sp.id, :type => stattype)

      unless cs.try(:first)
        cs1 = Callstat.new(:stats_period => sp,
			   :client => proxy_association.owner,
			   :type => stattype)
	cs = [cs1]
      end
      cs
    end

    def find_for_today(today, hour, stattype='CallCallstat')
      sp = StatsPeriod.find_with_hour(today, hour)
      return nil unless sp
      cs = proxy_association.owner.callstats.where(:stats_period_id => sp.id, :type => stattype)
      cs.try(:first)
    end

    # returns scope or array, so really it should be "all"
    # not clear yet how to refactor this to use above.
    def find_or_create_for_today(today = Time.now, hour = nil, stattype = 'CallCallstat', notify=false)
      #$stderr.puts "looking for callstat: #{stattype} for #{today}, #{hour}\n"
      sp = StatsPeriod.find_with_hour(today, hour) || StatsPeriod.create_with_hour(today, hour)
      return Callstat.create_for_period(sp, stattype, notify, nil, proxy_association.owner)
    end
  end

  scope :by_name,    -> { joins(:crm_account).order('crm_accounts.name ASC') }
  scope :by_revname, -> { joins(:crm_account).order('crm_accounts.name DESC') }

  def self.cache_name_key(name)
    "client_by_name:#{name}"
  end
  def cache_name_key
    self.class.cache_name_key(name)
  end
  def self.find_or_make(name)
    Rails.cache.fetch cache_name_key(name) do
      name.strip!
      c1 = find_by_name(name)
      unless c1
        # we want to use the billing BTN for this relationship, but we do not yet
        # know how, so have to manage the ID manually.
        crm1 = CrmAccount.create(:name => name, :id => CrmAccount.gen_id)
        crm1.save!
        c1 = create(:crm_account => crm1)
        $stderr.puts "Created new client: #{name}" if $LOG_NEW_CLIENTS;
        c1.save!
      end
      c1
    end
  end

  def self.cache_id_key(id)
    "client_by_id:#{id}"
  end

  def self.duplicate_list
    connection.select_all("SELECT MIN(id) AS id,MIN(billing_btn) AS btn,COUNT(billing_btn) AS count FROM clients GROUP BY billing_btn HAVING ( COUNT(billing_btn) > 1)").each { |thing|
      yield self.find(thing['id'])
    }
  end

  # this function used to just lookup the field, but with split into
  # crm_accounts, it needs to look that part up, and associate client.
  def self.find_by_name(name)
    crm = CrmAccount.find_by_name(name)
    crm.try(:client)
  end

  def self.nameLike(name)
    clients = []
    CrmAccount.where("name like '%#{name}#%'").all.each {|crm|
        clients << crm.try(:client)
    }
    Client.where("name like '%#{name}%'").all.each{ |c|
        clients << c
    }

    return clients

  end

  # find_by_billing_btn IS DEPRECATED, AFAIK.  clients.billing_btn is empty for all rows
  def self.make_customer(name, accountcode, sitename, email, site = nil)
    client = Client.find_by_billing_btn(accountcode) || Client.create(:name => name, :billing_btn => accountcode)
    client.billing_btn ||= accountcode
    client.assure_customer_sane(sitename, email, name, site)
    client.crm_account.name = name
    client.crm_account.save!
    client.save(validation: false)
    client
  end

  def self.find_by_btn(btnStr)
    btnStr.strip!
    btn = Btn.where('billingtelephonenumber like ?', "#{btnStr}")
    raise "More than one btn found for #{btnStr} (#{btn.count})" if btn.count > 1
    btn = btn.take
    return btn.client if not btn.nil?
    return nil
  end



  def find_or_make_site_byname(name)
    site   = sites.activated.where(name: name).take ||
             sites.activated.where(alt_name: name).take ||
             sites.create(name: name)
    site
  end

  def assure_customer_sane(sitename, email, name, site = nil)
    btn    = Btn.find_or_make(self.billing_btn, self)
    site   ||= clienthq(sitename, '0')
    site.building ||= Building.create
    self.billing_site ||= site
    self.client_crm_account_code_fix(self.billing_btn, name)
    save(validation: false)
    site.save(validation: false)
    site.make_otrs_user(false) if $OTRS
    if site.site_rep
      site.site_rep.email ||= email
      site.site_rep.login ||= email
      site.site_rep.save!
    end
    save(validation: false)
    btn.save!
    site.save(validation: false)
    return self
  end

  delegate :billing_address_1,
           :billing_address_2,
           :billing_address_3,
           :billing_address_4,
           :billing_address_street_2,
           :billing_address_city,
           :billing_address_province,
           :billing_address_country,
           :billing_address_postalcode,
           to: :crm_account,
           allow_nil: true

  delegate :name,
    to: :crm_account,
    allow_nil: true

  # things found on client.billing_site.building
  # but could also be on client.crm_account!
  def address
    if billing_site.try(:building)
      billing_site.building.streetaddr || ""
    elsif crm_account
      crm_account.billing_address_street || ""
    else
      return ""
    end
  end

  def city
    if billing_site.try(:building)
      billing_site.building.city || ""
    elsif crm_account
      crm_account.billing_address_city || ""
    else
      return ""
    end
  end

  def state
    if billing_site.try(:building)
      billing_site.building.city || ""
    elsif crm_account
      crm_account.billing_address_city || ""
    else
      return ""
    end
  end

  def postalcode
    if billing_site.try(:building)
      billing_site.building.postalcode || ""
    elsif crm_account
      crm_account.billing_address_postalcode || ""
    else
      return ""
    end
  end

  # later on, clients will get a time zone for reporting
  def timezone
    "EST5EDT"
  end
  def timezone_obj
    @timezone_obj ||= Timezone.fetch(timezone)
  end

  def cache_id_key
    self.class.cache_id_key(id)
  end

  def self.get_by_id(id)
    # note caches nil returns
    Rails.cache.fetch cache_id_key(id) do
      find_by_id(id)
    end
  end

  def to_s
    btnStr = ''
    btnStr = self.btn.billingtelephonenumber if not self.btn.nil?
    "{#{id}}#{name} (#{btnStr})"
  end

  def active!
    self.deactivated_at = nil
    save!
  end

  def terminated!(now = Time.now)
    self.deactivated_at = now
    save!
  end

  def terminated?
    !deactivated_at.blank?
  end

  def savefixturefw(fw)
    return if save_self_tofixture(fw)

    # now for related items.
    self.sites.each { |s| s.savefixturefw(fw) }
    self.billing_site.savefixturefw(fw) if self.billing_site
    self.crm_account.savefixturefw(fw)  if self.crm_account
  end

  def phone_count_by_site
    phones=0
    sites.each { |site| phones += site.services.count }
    phones
  end

  def phone_count_by_btn
    btn.services.count
  end

  # finds the last payment by client
  # @param year [Integer] the reference year : `2015`, `1999`
  # @param month [Integer] the reference month : `12` for December, `6` for June
  # @return [Payment, nil]
  def last_payment(year = nil, month = nil)
    most_recent_invoice(year, month).payments.order(payment_date: :desc).limit(1).first
  rescue NoMethodError
    nil
  end

  # finds invoice for particular year/month
  # NOTE: this appears to create an invoice every time
  # @param year [Integer] the year of invoice : `2017`, `1990`
  # @param month [Integer] the month of invoice : `9` for september,
  #   `1` for january
  # @return [Invoice, nil] the last invoice
  def last_invoice(year = nil, month = nil)
    if year.blank? && month.blank?
      date = last_billing_date || Date.today.beginning_of_month
      btn.invoice_for(date.year, date.month)
    elsif year.blank? && !month.blank?
      date = last_billing_date || Date.today.beginning_of_month
      btn.invoice_for(date.year, month)
    elsif !year.blank? && month.blank?
      date = last_billing_date || Date.today.beginning_of_month
      btn.invoice_for(year, date.month)
    else
      btn.invoice_for(year, month)
    end
  rescue NoMethodError
    nil
  end

  # finds most recent invoice, (?? and if not found, then creates one ??)
  # @param year [Integer] the year of invoice : `2017`, `1990`
  # @param month [Integer] the month of invoice : `9` for september,
  #   `1` for january
  # @return [Invoice, nil] the most recent invoice
  def most_recent_invoice(year = nil, month = nil)
    if year.blank? && month.blank?
      date = last_billing_date || Date.today.beginning_of_month
      btn.invoices.by_service_year_month(date.year, date.month).by_date.last || last_invoice(date.year, date.month)
    elsif year.blank? && !month.blank?
      date = last_billing_date || Date.today.beginning_of_month
      btn.invoices.by_service_year_month(date.year, month).by_date.last || last_invoice(date.year, month)
    elsif !year.blank? && month.blank?
      date = last_billing_date || Date.today.beginning_of_month
      btn.invoices.by_service_year_month(year, date.month).by_date.last || last_invoice(year, date.month)
    else
      btn.invoices.by_service_year_month(year, month).by_date.last || last_invoice(year, month)
    end
  rescue NoMethodError
    nil
  end

  # Returns the last *POSTED* invoice. Does not create one.a
  # Exception: it will return a unposted posted invoice IF:
  # -- There are no posted invoice
  # -- If there is an invoice before Invoice::POST_FEATURE_IMPLEMENTED
  def last_invoice_only()
    last_invoice = invoices.where.not(posted_at: nil).order(created_at: :desc).limit(1).take

    #Case where there are invoices but no posted invocies.  I think these lines defeat purpose and each line of code is going
    #to have to handle this case differently.
    #if last_invoice.nil?
    #    last_invoice = invoices.where('created_at < ?', Invoice::POST_FEATURE_IMPLEMENTED).order(created_at: :desc).limit(1).take
    #end

    return last_invoice
  end

  # Returns the last payment without creating invoices or payments
  def last_payment_only
    payments.order(created_at: :desc).limit(1).last.created_at
  end

  # returns the last billing date
  # @return [Date, nil] the last billing date or nil if no invoices
  def last_billing_date
    invoices.order(created_at: :desc).limit(1).last.created_at
  rescue NoMethodError
    nil
  end

  def account_code
    return billing_btn unless billing_btn.blank?
    self.billing_btn ||= UUID.new.generate(:compact)
  end

  def self.find_firstletter(x)
    activated.by_name.where('lower(crm_accounts.name) >= ?', x).limit(1).first
  end

  def find_or_make_site_by_bw_groupId(groupId)
    site = self.sites.activated.where(bw_groupId: groupId).take
    return site if site
    site = self.sites.activated.where(site_number: groupId).take
    return site if site
    return nil
  end

  def self.find_firstletters
    ('a'..'z').collect { |l|
      client = find_firstletter(l)
      if client and client.name and client.name.downcase[0] != l
        client = nil
      end
      [l,client]
    }
  end

  def self.firstletters
    @@firstletters ||= find_firstletters
  end

  def phonecfg(file)
    CredilCSV.open(file, "wb") do |csv|
      sites.each { |s| s.phonecfg_csv(csv) }
    end
    return true
  end

  # XXX use through table properly.
  # XXX need to make sure dates are in scope
  def has_service?(service, today=Time.now.utc)
    client_service.get_by_service_id(service.id)
  end

  ########################################################################
  # Returns :coop if the statement type includes the word "coop" (case insenstive)
  # Returns :commercant if the statement type includes the word "coommercant" (case insenitive)
  #
  # Is this the best way to determine revendor?  I had considered different ways
  # Using ID of statement types.   But what if the ID changed?
  # Using the exact spelling of the statement type might have been the best way, but what if the name slightly changes character?
  # Using a substring may create a tempalte for clients that are not using that statment type, but it seemed the easiest
  # What should happen is the data should be modified.  There should be a "revendor" boolean to statement types or at the very least,
  #    the name of the statement type should spell out mroe explicity the revendor
  # I like using class constants rather than magic strings because it avoids logic errors if there is a mistype
  ########################################################################
  def getRevendorForTemplate()
    statmentTypeNames = statement_types.reject{|st| ["Package", "Print"].include?(st.name)}.map {|st| st.name.downcase}

    revendors = statmentTypeNames.select {|name| name.include?("coop") or name.include?("commercant")}

    return :none if revendors.count == 0

    revendor = revendors[0]

    if revendor.include?("coop")
        return Client::COOP_HYDRO
    elsif revendor.include?("commercant")
        return Client::COMMERCANT_CHAUDIERE
    else
        return :none
    end
  end

  def provinces()
    conn = self.class.connection
    sql = "select distinct province from buildings b, sites s where s.building_id = b.id and s.client_id = #{self.id}";
    result = conn.exec_query(sql, 'provincesOfClient')

    # This resutls an ActiveRecord::Result with the row property having [["QC"], ["ON"], ["NB"], ["NS"], ["NL"]]
    # Using map we eliminate the inner aray
    result.rows.map{|a| a[0]}
  end

  def add_mycommunications_service!(today = Time.now.utc)
    unless has_service?(PortalService.mycommunications, today)
      client_service.create(:portal_service => PortalService.mycommunications, :startdate => today)
    end
  end

  def augment_call_count(today)
    incoming_calls = incoming_call_count(today, nil, false, false, false)
    outgoing_calls = outgoing_call_count(today, nil, false, false, false)
    return incoming_calls, outgoing_calls
  end

    # It is not clear if this method follows the buisness rules of how the data is being used.  Despite having an invoices table, it is not presented in the GUI.  Miranda does not think the invoices table   is used.
    # Data imported into biling on 2018/01/15 was built from payments table, so any discrepencies between billing and payments is human error
    # This method was written when biling data was reported in error for Hart Stores but I had no knowledge of another biling import to be done.  Thus it may not come into use in the future.
      # -- jlam@credil.org 2018/01/15
  def self.updateAllBilling
    ::Client.all.each { |c|
        next if c.billing.nil?
        next if c.terminated?
        c.billing.updateBilling
    }
  end

  ########################################################################
  # Returns true if the invoice cycle indicates it is a support client
  # Returns nil if the invoice cylce cannot be determined
  # TODO: redo this by doing a list of all the support clients
  # and join it with the current client
  # CAREFULL, 'not nil' == true
  ########################################################################
  def isSupport?
    # where.(:coverage_id => 9) means support
    if not btn.nil?
        if not btn.invoice_template.nil?
            if not btn.invoice_template.invoice_cycle.nil?
                return btn.invoice_template.invoice_cycle.downcase == 'support'
            end
        end
    end

    return nil
  end


  ########################################################################
  # This is for the invoices when the views and controllers are dealing
  # with a clientOrSite variable
  ########################################################################
  def client
    self
  end

  # Refactored for now... change this to a proper module
  # require "beaumont/daily_stats"
  # include Beaumont::DailyStats

  # Refactored for now... change this to a proper module
  # require "beaumont/client_importer"
  # include Beaumont::ClientImporter

  def english?
    language_preference == 'English'
  end

  def french?
    language_preference == 'French'
  end

  class ClientError < StandardError; end
  class HasNoBTNError < ClientError; end
  class HasNoCrmAccountError < ClientError; end
  class NoBillingSiteError < ClientError; end


  protected

    def clear_id_cache
       Rails.cache.delete cache_name_key
       Rails.cache.delete cache_id_key
    end

    def before_update_hook

      # check for 'activated' which will get called when 'activatable' runs.
      if self.changes["status"] == nil or self.changes["activated"] == nil
        return
      end
      if self.changes["status"][1] == "closed"
        self.deactivate
        self.deactivated_at = self.changes["deactivated_at"][1] if self.changes["deactivated_at"]

      elsif self.changes["status"][1] == "active"
        self.activate
        self.activated_at = self.changes["activated_at"][1] if self.changes["activated_at"]
        self.deactivated_at = nil
      else
        # what do I do for pending_start and pending_closed?
      end
    end

end
