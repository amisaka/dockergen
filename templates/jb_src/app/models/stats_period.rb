require 'sequence_allocator'

class StatsPeriod < ApplicationRecord
  has_many :callstats
  # self.primary_key = "id"
  include SequenceAllocator

  # this code pay attention to :type=> to make the right STI.
  class << self
    def new_with_cast(*a, &b)
      if (h = a.first).is_a? Hash and (type = h[:type] || h['type']) and (klass = type.constantize) != self
        raise "wtF hax!!"  unless klass < self  # klass should be a descendant of us
        return klass.new(*a, &b)
      end
      new_without_cast(*a, &b)
    end
    alias_method_chain :new, :cast
  end

  scope :today, lambda { |*args|
    day    = args.first || Time.now
    where(:day => day.to_date)
  }

  def self.find_or_create_with_hour(day, hour)
    find_with_hour(day,hour) || create_with_hour(day,hour)
  end

  def self.find_with_hour(day, hour)
    unless hour.blank?
      sp = all.where(:day => day, :hour => hour).first
    else
      sp = all.where(:day => day, :hour => nil).first
    end
    return sp
  end

  def self.create_with_hour(day, hour)
    sp = StatsPeriod.create(:day => day, :hour => hour)
    if hour.blank?
      sp.type = 'DailyStatsPeriod'
    else
      sp.type = 'HourlyStatsPeriod'
    end
    sp.save!
    sp.reload    # changes the type
  end


  def self.by_year(year)
    day1 = Time.gm(year, 1,1,0,0,0).to_datetime
    where(:day => day1)
  end

  def self.find_or_make(conditions)
    sp = self.where(conditions).first
    unless sp
      sp = create(conditions)
      sp.save!
    end
    sp
  end

  # find newest record.
  def self.last_period
    self.order("day DESC").first
  end

  #scope :within_period, lambda { |beginning, ending|
  def self.within_period(beginning, ending)
    where('stats_periods.day' => beginning..ending)
  end

  # maybe STI should take care of this.
  def start_hour
    self.hour || 0
  end
  def end_hour
    (self.hour || 23) + 1
  end

  def timezoneoffset
    self.day
  end

  # hard code to EST for now.
  # XXX THIS NEEDS TO REFERENCE CLIENT.TIMEZONE
  def timezone
    "EST5EDT"
  end

  def midnight
    return @midnight if @midnight

    # stupid that we can not make a time a particular zone.
    # http://stackoverflow.com/questions/5419148/rails-get-beginning-of-day-in-time-zone
    saved = Time.zone;
    Time.zone= timezone;
    @midnight = Time.zone.local(day.year,day.month,day.day);
    Time.zone= saved
    return @midnight
  end

  # note that this does not take into account time zones yet.
  def beginning_of_period
    @beginning_of_period ||= (midnight + start_hour.hours)
  end

  def end_of_period
    @end_of_period ||= (midnight + end_hour.hours)
  end

  # returns a scope of periods
  def within_period
    StatsPeriod.within_period(beginning_of_period, end_of_period)
  end

  # returns a scope for type which is below this kind
  def contained_period_types
    StatsPeriod.where(:type => 'DailyStatsPeriod')
  end

  def localtime(time)
    time.to_datetime.in_time_zone(self.timezone)
  end

  def self.for_day(ltime)
    return self.find_or_create_with_hour(ltime.to_date, nil)
  end
  def for_day(time)
    ltime = localtime(time)
    self.class.for_day(ltime)
  end

  def self.for_hour(ltime)
    return self.find_or_create_with_hour(ltime.to_date, ltime.hour)
  end
  def for_hour(time)
    ltime = localtime(time)
    self.class.for_hour(ltime)
  end

  def name
    "#{type}:#{day.to_s(:db)}"
  end

  def statcount
    0
  end


end
