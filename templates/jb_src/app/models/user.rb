require 'devise'

class User < ApplicationRecord
  include Devise
  devise :database_authenticatable, :confirmable, :recoverable, :rememberable,
    :trackable, :validatable, :registerable

  # if we want to have a login field instead of logging in with email, we should do this:
  # https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign_in-using-their-username-or-email-address

  # TODO email uniqueness is enforced by AR settings set up by Devise.  There are potential
  # race conditions there when you have more than one server up, and two people try
  # to register the same name at once (unlikely, but possible).
  # should we bother setting a "unique index" on that column?

  validates :fullname,
    presence: true


  def self.busyReport
    format = "%-30s %s"
    activeUsers = User.where.not(last_seen_at: nil).order(:last_seen_at).all
    puts format % ['Email', "Last seen (Hours ago) on #{`hostname`}"]
    activeUsers.each {|u| puts format % [u.email, u.lastSeenHours] };
  end

  def lastSeenHours()
    if last_seen_at.nil?
        Float::INFINITY
    else
        ((DateTime.now-last_seen_at.to_datetime)*24).to_f.round(2)
    end

  end


  def to_s
    "#{fullname} (#{email})"
  end

end
