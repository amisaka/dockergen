class Inventory < ApplicationRecord
  belongs_to :site
  belongs_to :services
  belongs_to :did_supplier
  belongs_to :product

  #def self.client(inventory_id)
  #        #Inventory.find("19963").site.name
  #        #Inventory.find("19963").site.client.name
  #        Inventory.find(inventory_id).site.client.btn.billingtelephonenumber
  #        Inventory.find(inventory_id).site.client.btn
  #end

  #def self.btn(inventory_id)
  #        Inventory.find(inventory_id).site.client.btn
  #end

  #def self.inventory_index
  #        result_array_of_hashes = Array.new

  #        Inventory.first(100).each do |item|
  #      	inventory_index_hash = Hash.new

  #         	#inventory_index_hash['btn'] = item.billingtelephonenumber
  #      	if(item.site != nil)    
  #         	    #inventory_index_hash['btn'] = item.site.client.btn.billingtelephonenumber
  #         	    inventory_index_hash['btn'] = item.site.name
  #      	else
  #      	    inventory_index_hash['btn'] = 'empty'
  #      	end

  #      	inventory_index_hash['customer_name'] = item.customer_name
  #      	inventory_index_hash['supplier'] = item.supplier
  #      	inventory_index_hash['site_number'] = item.site_number
  #      	inventory_index_hash['site_name'] = item.site_name
  #      	inventory_index_hash['expiry_date'] = item.expiry_date
  #      	inventory_index_hash['device'] = item.device

  #        	result_array_of_hashes.append(inventory_index_hash)
  #        end
  #        return result_array_of_hashes
  #end

  ################################################################################
  # QUERIED FIELDS FOR EACH INSTANCE OF INVENTORY
  ################################################################################

  # Variables to move to database maybe.
  # These are used for drop down menus, if they are in the database, then
  # there is potential to 
  @@inventory_status_data = {
    inactive: 		[ 1, "Inactive" ],
    active: 		[ 0, "Active" ]
  }

  def inventory_status(inventory_status_id)

	tmp_result = ""

	case inventory_status_id
	when @@inventory_status_data[:inactive][0]
		tmp_result = @@inventory_status_data[:inactive][1]
	when @@inventory_status_data[:active][0]
		tmp_result = @@inventory_status_data[:active][1]
	end 
	return tmp_result
  end

  def inventory_status_list()
	tmp_result = []

	@@inventory_status_data.each_pair do |key, value|
		tmp_result.append([ value[1],value[0] ])
	end
	return tmp_result
  end

  def btn_billingtelephonenumber(inventory_id)
	  #result = inventory_id
	  #result = Inventory.id
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      #tmp_result = Inventory.find(inventory_id).site.name
	      tmp_result = Inventory.find(inventory_id).site.client.btn.billingtelephonenumber
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def site_name(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
		  tmp_result = Inventory.find(inventory_id).site.name
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def site_number(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
		  tmp_result = Inventory.find(inventory_id).site.site_number
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def site_address_full(inventory_id)

	  tmp_result = []
	  if(Inventory.find(inventory_id).site != nil)    
		physical_address_1 	= Inventory.find(inventory_id).site.building.address1
		physical_address_2 	= Inventory.find(inventory_id).site.building.address2
		city 			= Inventory.find(inventory_id).site.building.city
		province 		= Inventory.find(inventory_id).site.building.province
		country 		= Inventory.find(inventory_id).site.building.country
		postalcode 		= Inventory.find(inventory_id).site.building.postalcode

		tmp_address_1 = (physical_address_1 ? physical_address_1 : "") + ", " + \
	       			(physical_address_2 ? physical_address_2 : "")
		tmp_address_2 = (city 		? city 	  	: "") + ", " + \
	    			(province 	? province	: "") + ", " + \
	    			(country   	? country 	: "") 

	    	tmp_result.append(tmp_address_1) 
	    	tmp_result.append(tmp_address_2) 
	    	tmp_result.append((postalcode ? postalcode : "")) 
	  else
	    	tmp_result.append('')
	    	tmp_result.append('')
	    	tmp_result.append('')
	  end
          result = tmp_result
  end

  def site_status(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
		  tmp_result = Inventory.find(inventory_id).site.activated ? "Active" : "In-Active"
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  # This is the supplier_name from the original inventory data. Eventually, remove this field.
  def supplier_name(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).did_supplier != nil)    
		  tmp_result = Inventory.find(inventory_id).did_supplier.name
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def did_supplier_list()
          tmp_result = []
          DIDSupplier.all.each do |item|
          	tmp_result.append([item.name,item.id])
          end
          tmp_result.append(['',0])
	  return tmp_result.sort
  end

  def product_name(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).product != nil)    
            tmp_result = Inventory.find(inventory_id).product.name
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_name(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.client.name
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_language_preference(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.client.language_preference
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_service_level(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.client.service_level
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_status(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.client.status
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_address_full(inventory_id)

	  # billing_address_1
	  # billing_address_2
	  # billing_address_3
	  # billing_address_4
	  # billing_address_city
	  # billing_address_province
	  # billing_address_country
	  # billiVg_address_postalcode

	  tmp_result = []
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      billing_address_1 = Inventory.find(inventory_id).site.client.billing_address_1 
	      billing_address_2 = Inventory.find(inventory_id).site.client.billing_address_2
	      billing_address_3 = Inventory.find(inventory_id).site.client.billing_address_3
	      billing_address_4 = Inventory.find(inventory_id).site.client.billing_address_4
	      tmp_result.append((billing_address_1 ? billing_address_1 : ""))
	      tmp_result.append((billing_address_2 ? billing_address_2 : ""))
	      tmp_result.append((billing_address_3 ? billing_address_3 : ""))
	      tmp_result.append((billing_address_4 ? billing_address_4 : "")) 
	    else
	      tmp_result.append('')
	      tmp_result.append('')
	      tmp_result.append('')
	      tmp_result.append('')
	    end
	  else
	    tmp_result.append('')
	    tmp_result.append('')
	    tmp_result.append('')
	    tmp_result.append('')
	  end
          result = tmp_result
  end

  def client_address1(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.building.address1
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result

  end

  def client_city(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.buildling.city
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_province(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.buildling.province
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_country(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.buildling.country
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def client_postalcode(inventory_id)
          tmp_result = ""
	  if(Inventory.find(inventory_id).site != nil)    
	    if(Inventory.find(inventory_id).site.client != nil)    
	      tmp_result = Inventory.find(inventory_id).site.buildling.postalcode
	    else
	      tmp_result = ''
	    end
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  def term_user_name(term_machine_name)
          tmp_result = ""
 	  if(InventoryTerm.where("machine_name = ?", term_machine_name)[0] != nil)
            tmp_result = InventoryTerm.where("machine_name = ?", term_machine_name)[0].user_name
	  else
	    tmp_result = ''
	  end
          result = tmp_result
  end

  # This is developed for the inventory widgets in site_inventory.
  def list_by_site_and_by_type(inventory_id,inventory_type)
          tmp_result = []
	  #Inventory.where("site_id = ? and inventory_type = ?", "10207", inventory_type).each do |item|
	  Inventory.where("site_id = ? and inventory_type = ?", Inventory.find(inventory_id).site_id, inventory_type).each do |item|
	    tmpHash = {}  
	    
	    tmpHash['id']			 = item.id
	    tmpHash['site_name'] 		 = item.site_name(inventory_id) 
	    tmpHash['site_inventory_type'] 	 = item.inventory_type  
	    tmpHash['adresse_ip_lan_data']       = item.adresse_ip_lan_data
	    tmpHash['adresse_ip_lan_voip']       = item.adresse_ip_lan_voip
	    tmpHash['adresse_ip_link_data']      = item.adresse_ip_link_data
	    tmpHash['adresse_ip_link_voip']      = item.adresse_ip_link_voip
	    tmpHash['adresse_ipv6_lan_data']     = item.adresse_ipv6_lan_data
	    tmpHash['adresse_ipv6_lan_voip']     = item.adresse_ipv6_lan_voip
	    tmpHash['adresse_ipv6_link_data']    = item.adresse_ipv6_link_data
	    tmpHash['adresse_ipv6_link_voip']    = item.adresse_ipv6_link_voip
	    tmpHash['agreement_num']             = item.agreement_num
	    tmpHash['auto_renewal_term']         = item.auto_renewal_term
	    tmpHash['circuit_num']               = item.circuit_num
	    tmpHash['circuit_num_logique']       = item.circuit_num_logique
	    tmpHash['circuit_num_physique']      = item.circuit_num_physique
	    tmpHash['compte_fttb']               = item.compte_fttb
	    tmpHash['customer_sn']               = item.customer_sn
	    tmpHash['description']               = item.description
	    tmpHash['device']                    = item.device
	    tmpHash['devicenamemonitored']       = item.devicenamemonitored
	    tmpHash['download_speed']            = item.download_speed
	    tmpHash['email_of_courrier']         = item.email_of_courrier
	    tmpHash['expiry_date']               = item.expiry_date
	    tmpHash['installed_on_line']         = item.installed_on_line
	    tmpHash['inventory_type']            = item.inventory_type
	    tmpHash['mac_address']               = item.mac_address
	    tmpHash['numerodereference']         = item.numerodereference
	    tmpHash['parent_agreement_num']      = item.parent_agreement_num
	    tmpHash['password']                  = item.password_decrypt(inventory_id)
	    tmpHash['quantity']                  = item.quantity
	    tmpHash['realsyncspeeddownload']     = item.realsyncspeeddownload
	    tmpHash['realsyncspeedupload']       = item.realsyncspeedupload
	    tmpHash['router_principal']          = item.router_principal
	    tmpHash['router_vrrp']               = item.router_vrrp
	    tmpHash['service_id']                = item.service_id
	    tmpHash['sous_interface_data']       = item.sous_interface_data
	    tmpHash['sous_interface_voip']       = item.sous_interface_voip
	    tmpHash['sous_interface_voip_vrrp']  = item.sous_interface_voip_vrrp
	    tmpHash['start_date']                = item.start_date
	    tmpHash['supplier_num']              = item.supplier_num
	    tmpHash['term']                      = item.term_user_name(item.term)
	    tmpHash['terminate_on']              = item.terminate_on
	    tmpHash['upload_speed']              = item.upload_speed
	    tmpHash['username']                  = item.username
	    tmpHash['vl_videotron']              = item.vl_videotron
	    tmpHash['vlan_data_q_in_q']          = item.vlan_data_q_in_q
	    tmpHash['vlan_supplier']             = item.vlan_supplier
	    tmpHash['vlan_voip_q_in_q']          = item.vlan_voip_q_in_q
	    tmpHash['vrrp_id']                   = item.vrrp_id
	    tmpHash['z_end']		         = item.z_end			 
	    tmpHash['site_id']		         = item.site_id			 
	    tmpHash['did_supplier_id']		 = item.did_supplier_id			 
	    tmpHash['inventory_status_id']	 = item.inventory_status_id			 

	    tmp_result.append(tmpHash)
	  end

	  # Sorting the result based on the inventory type.
  	  sorted_result = []
          if (inventory_type == "circuit")
	    sorted_result = tmp_result.sort_by { |hash_arr| [ hash_arr['inventory_status_id'], hash_arr['circuit_num_logique'], hash_arr['id'] ] }
          elsif (inventory_type == "core_interface_client_fibre")
	    sorted_result = tmp_result.sort_by { |hash_arr| [ hash_arr['inventory_status_id'], hash_arr['email_of_courrier'], hash_arr['adresse_ip_link_data'] ] }
          elsif (inventory_type == "hsa")
	    sorted_result = tmp_result.sort_by { |hash_arr| [ hash_arr['inventory_status_id'], hash_arr['email_of_courrier'], hash_arr['adresse_ip_lan_data'], hash_arr['adresse_ipv6_lan_data'] ] }
          elsif (inventory_type == "internet_services_cable")
	    sorted_result = tmp_result.sort_by { |hash_arr| [ hash_arr['inventory_status_id'], hash_arr['service_id'] ] }
          elsif (inventory_type == "internet_services_pppoe")
	    sorted_result = tmp_result.sort_by { |hash_arr| [ hash_arr['inventory_status_id'], hash_arr['username'], hash_arr['service_id'] ] }
          elsif (inventory_type == "nagios_client")
	    sorted_result = tmp_result.sort_by { |hash_arr| [ hash_arr['inventory_status_id'], hash_arr['devicenamemonitored']] }
          elsif (inventory_type == "nagios_ip4b")                                                                             
	    sorted_result = tmp_result.sort_by { |hash_arr| [ hash_arr['inventory_status_id'], hash_arr['devicenamemonitored']] }
	  end

	  # Test
	  #sorted_result = tmp_result.sort_by { |hash_arr| [hash_arr['email_of_courrier'], hash_arr['adresse_ip_link_data'], hash_arr['id'] ] }
	  # Test the sort feature with the following:
          #   select adresse_ip_link_data,customer_name,id,email_of_courrier,vlan_supplier,sous_interface_data from inventories where inventory_type = 'core_interface_client_fibre' and site_id = '2958';
	  # In rails console (pry):
 	  #   arr = []
	  #   Inventory.first.list_by_site_and_by_type('50634','core_interface_client_fibre').each do |item|
	  #     arr.append([ item['email_of_courrier'], item['id'], item['adresse_ip_link_data'] ])
	  #   end  

          tmp_result = sorted_result

          return tmp_result

  end

  # This is for the index page.
  def self.sites_uniq()

        tmp_result = []

	# This complexity is in this rails app because this inventory app has turned into 
	# a sites app from which the user can display, create, modify, and deactivate 
        # inventory items. There is a lot of bending over back wards to get this to work 
	# work properly.

	firstSiteID = 0

	Inventory.order(:site_id).first(100).each do |item|
	#Inventory.order(:site_id).all.each do |item|

  	  	tmpHash = {}

		# This is to get a single inventory ID for all occurances of a site_id in inventory.
		# This is because we want to have uniq sites, but if we populate the id field with
		# unique inventory IDs, then there will be duplicate sites. 
		if (firstSiteID != item.site_id)
	          	firstSiteID = item.site_id
		  	tmpHash['id'] = item.id  
	
			tmpHash['site_id'] = item.site_id  
		      	tmpHash['site_name'] = item.site_name(item.id)  
		  	tmpHash['client_name'] = item.client_name(item.id)  
		  	tmpHash['btn_billingtelephonenumber'] = item.btn_billingtelephonenumber(item.id)  

			if (item.site != nil)    
				if (item.site.building != nil)   
			      		tmpHash['address1'] = item.site.building.address1
			      		tmpHash['city'] = item.site.building.city 
	  			else
					tmpHash['address1'] = ''
					tmpHash['city'] = '' 
				end

				tmp_result.append(tmpHash)  
			end
		end
	end  

	
	result = tmp_result.uniq

	sorted_result = {}
	#sorted_result = result.sort_by { |hash_arr| hash_arr['btn_billingtelephonenumber'] }
	sorted_result = result.sort_by { |hash_arr| [hash_arr['client_name'], hash_arr['btn_billingtelephonenumber'], hash_arr['site_name']] }
	return sorted_result
  end

  def inventory_type_list()
	tmp_result = []

	InventoryType.all.each do |item|
		tmp_result.append([item.user_name,item.machine_name])
	end
	return tmp_result
  end

  def inventory_type_name_pretty(inventory_id)
	tmp_result = ''

	tmp_result = InventoryType.where("machine_name = ?", Inventory.find(inventory_id).inventory_type)[0].user_name

	return tmp_result
  end

  def inventory_term_list()
	tmp_result = []

	InventoryTerm.all.each do |item|
		tmp_result.append([item.user_name,item.machine_name])
	end
	return tmp_result
  end

  def inventory_auto_renewal_term_list()
	tmp_result = []

	InventoryAutoRenewalTerm.all.each do |item|
		tmp_result.append([item.user_name,item.machine_name])
	end
	return tmp_result
  end

  def inventory_router_principal_list()
	tmp_result = []

	InventoryRouterPrincipal.all.each do |item|
		tmp_result.append([item.user_name,item.machine_name])
	end
	return tmp_result
  end

  def inventory_router_vrrp_list()
	tmp_result = []

	InventoryRouterVrrp.all.each do |item|
		tmp_result.append([item.user_name,item.machine_name])
	end
	return tmp_result
  end

  def inventory_device_list()
	tmp_result = []

	InventoryDevice.all.each do |item|
		tmp_result.append([item.user_name,item.machine_name])
	end
	return tmp_result
  end

  def service_list()
          tmp_result = []
          DIDSupplier.all.each do |item|
          	tmp_result.append([item.name,item.id])
          end
          tmp_result.append(['',0])
	  return tmp_result.sort
  end

  def password_decrypt(pw)
	tmp_result = ''

        # Need to store this in .env or something.
        #key = SecureRandom.random_bytes(32) # Used this to generate random 32 bytes.
        #key_string = key.unpack('c*') # Used this to convert the key to string.
        #key_string.pack('c*') # Used this to convert the string back to 32 bytes.
        key_string = [45, 32, -60, -12, -33, 44, 24, 65, -6, 77, -15, -122, -28, 95, -60, 12, 72, 63, -114, 0, -81, -45, 70, 86, 67, -3, 112, -123, -39, -65, -19, 117]
        key = key_string.pack('c*') 
       
	# The passwords if not encrypted are less than 30 (probably, else they are over 100 chars).
	if (pw != nil)
	  if (pw.size > 30)
	    crypt = ActiveSupport::MessageEncryptor.new(key) 
	    decrypted_pw = crypt.decrypt_and_verify(pw)
            tmp_result = decrypted_pw
	  else
	    tmp_result = pw
          end
	else
	  tmp_result = ''
        end

	return tmp_result
  end

  def password_encrypt(pw)
	tmp_result = ''

	# Make sure this is the same as in password_decrypt().
        key_string = [45, 32, -60, -12, -33, 44, 24, 65, -6, 77, -15, -122, -28, 95, -60, 12, 72, 63, -114, 0, -81, -45, 70, 86, 67, -3, 112, -123, -39, -65, -19, 117]
        key = key_string.pack('c*') 
       
	crypt = ActiveSupport::MessageEncryptor.new(key) 
	encrypted_pw = crypt.encrypt_and_sign(pw)
        tmp_result = encrypted_pw

	return tmp_result
  end

  def to_export_hash
    # return a hash of values we want/need for the export.
    # the billable_services/exports view uses these values to build
    # resulting table

    siteName =      (Rails.env.test? ? self.site.to_s    : self.site.name) if self.site

    {
      "ID"      	       	 => self.id,
      "Product Name"             => siteName,
      "Description"              => self.description,
      "Department #"             => (self.site.site_number if self.site),
      "Supplier"                 => (self.did_supplier.name if self.did_supplier),
      "Supplier Activation Date" => (self.did_supplier.created_at.strftime "%Y-%m-%d" if self.did_supplier),
    }

  end


end
