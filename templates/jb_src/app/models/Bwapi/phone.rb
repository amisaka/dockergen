# coding: utf-8

# this interfaces to a Broadworks XSP1 installation using
# the XSI-ACTIONS API. It retrieves device profile information
#
# The username and password to use should be declared in
# config/environments/*.rb using:
#   $BwAPI_user = 'user@domain.example.com'
#   $BwAPI_password = 'thepw'
#   $BwAPI_url = 'http[s]://xsp1.yoursite.example.com/com.broadsoft.xsi-actions/v2.0'
#   $BwAPI_realm = "voip.example.com"
#
# This userid should have provisioning permissions, but may also need
# to be a super-user.
#
# we are looking at using cookie authentication.
#  http://www.rubydoc.info/gems/activeresource/4.0.0/frames
module Bwapi
end
class Bwapi::Phone
  #self.site = URI::join($BwAPI_url, "user")
  #self.element_name = "user"
  #self.user = $BwAPI_user
  #self.password = $BWAPI_password

  attr_accessor :url, :details

  def initialize(phoneext, realm, userId = nil)
    @user     = userId || sprintf("%s@%s", phoneext, realm)
    @url      = URI::join($BwAPI_url+"/", "user/")
    @url      = URI::join(@url, @user+"/")
    @url      = URI::join(@url, "profile")
    @resource = RestClient::Resource.new @url.to_s, $BwAPI_user, $BwAPI_password
  end

  def get
    doc = @resource.get
    h   = Hash.from_xml(doc)
    profile = h["Profile"]
    if profile
      details = profile["details"]
    end
    return details
  end

  def details
    @details ||= get
  end

  def copy_to_phone(phone)
    phone.firstname = details["firstName"]
    phone.lastname  = details["lastName"]
    phone.extension = details["extension"]
    phone.foreign_userId ||= details["userId"]

    groupId = details["groupId"]
    newsite = phone.btn.client.find_or_make_site_by_bw_groupId(groupId)
    if newsite
      phone.site = newsite
    else
      phone.site.bw_groupId = groupId
      phone.site.save!
    end
    phone.save!

  end

  def self.update_phone(phone)
    bwphone = new(phone.phone_number, $BwAPI_realm, phone.foreign_userId)
    begin
      bwphone.copy_to_phone(phone)
      return phone.foreign_userId

    rescue RestClient::Unauthorized, RestClient::NotFound
      bwphone = new(phone.phone_number, $BwAPI_realm2, phone.foreign_userId)
      begin
        bwphone.copy_to_phone(phone)
        return phone.foreign_userId
      rescue RestClient::Unauthorized
        return :unauthorized
      rescue RestClient::NotFound
        return :notFound
      end
    end
  end

end
