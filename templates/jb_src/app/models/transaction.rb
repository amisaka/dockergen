######################################################################
# As of 2018/06/04, this parent class of invoice and payments
# mostly takes care of updating beforeBalance, afterbalance and
# the balance in billing.
######################################################################
class Transaction < ApplicationRecord

  self.abstract_class = true

  belongs_to :client




    ######################################################################
    # Called from before save hook for transaction objects
    # Updates only before save balance
    #
    # Why a seperate function updateBeforeBalance?
    # It wasn't intentional, but seems to me it offers a
    # clean transition between the child hooks and the function that
    ######################################################################
    def beforeSaveBalanceUpdates
      if self.client.isSupport?
          # No balance tracking in SGE or billing object for support clients
          return nil
      else
          self.updateBeforeBalance
      end
    end

    ######################################################################
    # Called from save hook for transaction objects
    # Updates after balance and balance in billing object
    #
    # Why a seperate function updateBillingAndAfterBalance?
    # It wasn't intentional, but seems to me it offers a
    # clean transition between the child hooks and the function that
    ######################################################################
    def afterSaveBalanceUpdates
        # Don't update anything if the transaction object is already deleted
        # Since the buisness logic does support correcting future invoices, this
        # is probably the right thing to do
        return if self.deleted

      if self.client.isSupport?
          # No balance tracking in SGE or billing object for support clients
          return nil
      else
          self.updateBillingAndAfterBalance
      end

    end

    ######################################################################
    # Updates beforeBalance, that is the balance before the transaction
    # object was saved.
    ######################################################################
    def updateBeforeBalance
        b = self.getBillingObj()
        self.before_balance = b.balance
        # save should not be necessary, since this is called at pre_save hook
    end


    def updateBillingOnlyForDestroy
        b = self.getBillingObj

        self.transaction do
            if self.class == Payment
                p = self.client.last_payment_only
                b.last_payment_date = p.created_at
                b.last_payment_amount = p.amount
                b.balance = p.after_balance
            elsif self.class == Invoice
                i = self.client.last_invoice_only
                b.last_statement_date = i.invoice_date
                b.last_statement_amount = i.total_due
                b.balance = i.after_balance
            else
                raise "Dont't know what to do with a #{self.class} for balance udpates.  Payment / Invoice has been destroyed, but balance in billing and after_balance are not updated"
            end

            b.save!
        end
    end

    ######################################################################
    # Updates afterBalance (the balance after the object was saved)
    # and balance in billing.
    ######################################################################
    def updateBillingAndAfterBalance

        b = self.getBillingObj


        self.transaction do

            b.balance = 0 if b.balance.nil?


            if self.class == Payment

                if self.transaction_type.present? and self.transaction_type.id == 4
                    b.last_payment_date = self.date
                    b.last_payment_amount = self.amount.abs
                end

                #A customer payment to the service provider has a negative amount
                #so we add it to the balance t13642
                b.balance += self.amount

            elsif self.class == Invoice

                if not self.postable?
                    raise "Invoice object is already posted"
                end

                b.last_statement_date = self.invoice_date
                b.last_statement_amount = self.total_due
                b.balance += self.amountToApplyToBalance
            else
                raise "Dont't know what to do with a #{self.class} for balance udpates.  Payment / Invoice has been created, but balance in billing and after_balance are not updated"
            end

            self.update_column(:after_balance, b.balance)
            b.save!
        end

        return true
    end

    def getBillingObj
        b = self.client.billing

        if b.nil?
            if self.client.isSupport?
                return nil
            else
                b = Billing.create!()
                self.client.billing = b
                self.client.save!
            end
        end

        return b
    end


end
