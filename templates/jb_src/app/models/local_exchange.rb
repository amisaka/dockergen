require "csv"
require 'find'

class LocalExchange < ApplicationRecord
  # self.primary_key = "id"

  has_many :local_exchanges_north_american_exchange,
           :foreign_key => 'local_exchanges_id'
  has_many :north_american_exchanges,
           :through => :local_exchanges_north_american_exchange

  belongs_to :carrierplan,
             :foreign_key => 'carrierplan_id',
             :class_name => 'Carrier'

  belongs_to :rate_center

  scope :active, -> { where(deleted_at: nil) }

  def simplename
    "le#{id}"
  end

  def country
    "Canada"
  end

  def active?
    deleted_at.blank?
  end
  def inactive?(now = Time.now)
    !active? && deleted_at < now
  end

  def self.mapcarrierid
    where(carrierplan_id: nil).find_each { |le|
      le.carrierplan = Carrier.where(name: le.carrier).take
      le.save!
    }
  end

  def self.latestFootPrint(dir)
    latest_FootPrint = nil
    latest_mtime     = nil

    Find.find(dir) do |path|
      stat = File.stat(path)
      next if stat.directory?

      mtime = stat.mtime
      latest_mtime ||= mtime
      # look for most recent file ending with either
      # LERG8.DAT, or LERG*.ZIP.

      basename = File.basename(path)
      if (basename =~ /^.*Network-Footprint.*/)
        if mtime >= latest_mtime
          #puts "newer #{path} over #{latest_LERG}, as #{mtime} > #{latest_mtime}"
          latest_FootPrint = path
          latest_mtime     = mtime
        end
      end
    end

    tmpdir = nil
    workfile = latest_FootPrint
    if (latest_FootPrint =~ /.zip$/i)

      basepart = nil
      IO::popen(["unzip","-l", latest_FootPrint], "r") do |infile|
        infile.each { |lines|
          (size,date,time,file) = lines.split
          basepart = file if file =~ /.csv$/i
        }
      end

      if basepart
        workingtmp = Rails.root.join("tmp")
        if ENV['SG1_TMPDIR']
          workingtmp = ENV['SG1_TMPDIR']
        end
        tmpdir = File.join(workingtmp,"footprint#{$$}")
        cmd    = sprintf("mkdir -p %s && cd %s && unzip %s %s",
                         tmpdir, tmpdir, latest_FootPrint, basepart)
        code = system(cmd)

        workfile = File.join(tmpdir, basepart)
      end
    end

    return latest_FootPrint, tmpdir, workfile
  end

  def self.import_bell_row(carrier, row, debug=false, stats = Hash.new(0))
    # Extract and strip the field values of spaces
    (npanxx,lerg6name,crtc_name,prov,prov_crtc_rc,npa,nxx,sipavail,outbound,planned) = row

    if npanxx == "NPA-NXX"
      stats[:header] += 1
      return
    end

    # first see if we can find LocalExchange with the right name.
    foot = self.where(name: crtc_name.upcase, province: prov.upcase,
                      carrier: carrier).first

    unless foot
      foot = LocalExchange.create(name: crtc_name.upcase, province: prov.upcase,
                                  carrier: carrier)
      stats[:created] += 1
    else
      foot.updated_at = Time.now
      stats[:updated] += 1
    end

    # now connect to rate_center via NAE table.
    nae = NorthAmericanExchange.where(npa: npa, nxx: nxx).take
    if nae
      stats[:connected_to_existing_rc] += 1
      foot.rate_center = nae.rate_center
    else
      puts "Can not find #{npa}/#{nxx}"
      stats[:nae_not_found] += 1
    end

    if sipavail == 'AVAILABLE'
      stats[:sip_available] += 1
      foot.sip_availability = true
    end
    if outbound == 'ON-NET'
      stats[:on_net]        += 1
      foot.on_net           = true
    end

    foot.mapped_at  = Time.now
    foot.deleted_at = nil
    foot.save!
    foot
  end

  def self.import_row(carrier, row, debug=false, stats = Hash.new(0))
    # Extract and strip the field values of spaces
    (name, prov, lir, lirname, groupid, note) = row

    if prov == 'Province'
      # must be header line
      stats[:header] += 1
      stats[:exchange_province] = true
      return nil
    end
    if prov == 'Porting'
      # must be header line for new-style Footprint
      stats[:header] += 1
      stats[:name_province] = true
      return nil
    end

    if(stats[:name_province] == true)
      (name1, prov) = name.split(/,/)
      name = name1
    end

    # => LocalExchange(id: integer, province: string,
    #                  name: string, created_at: datetime, updated_at: datetime,
    #                  carrier: string)

    #Create (or update) the entry in LocalExchange exchange table
    foot = nil

    if name and prov and prov.length < 4
      prov = LergMethods::cleanProvince(prov)
      foot = self.where(name: name.upcase, province: prov.upcase,
                        carrier: carrier).first

      #byebug
      unless foot
        foot = LocalExchange.create(name: name.upcase, province: prov.upcase,
                                    carrier: carrier)
        stats[:created] += 1
      else
        foot.updated_at = Time.now
        stats[:unchanged] += 1
      end
      foot.connect_to_rate_center
      foot.deleted_at = nil
      foot.save!
      puts "#{foot.id} #{name} #{prov}" if debug



      if stats.key?(:messageTS)
        if ((Time.now - stats[:messageTS]) > (5 * 60))
          puts "#{ lines } lines read so far..." if debug
          stats[:messageTS] = Time.now
        end
      end
    end
    return foot
  end

  def self.import_bell_file(file, carrier, debug, stats = Hash.new(0))
    # might have to adjust for file encoding!
    File.open(file, "rb:bom|utf-8") do |handle|
      CSV.parse(handle, :encoding => 'utf-8') do |row|
        print "processing row #{stats[:line]}\r" if debug
        import_bell_row(carrier, row, debug, stats)
        stats[:line] += 1
      end
    end
  end

  def self.import_file(file, carrier, seperator,
                       debug, stats = Hash.new(0))


    case carrier.downcase
    when 'bell'
      return import_bell_file(file, carrier, debug, stats)
    end

    # Read the file one line at a time
    puts "\nReading file #{file}" #if quietLevel.in?(['none'])

    stats[:messageTS] = Time.now

    # anything that is older than this time, was not in the input
    # and should probably be removed.
    startedAt = Time.now

    # might have to adjust for file encoding!
    handle = File.open(file, "rb:bom|utf-8")
    unless handle
      puts "Can not open #{file}: $!"
      return
    end

    #puts "seperator: >#{seperator}<"
    CSV.parse(handle, :col_sep => seperator, :encoding => 'utf-8') do |row|
      print "processing row #{stats[:line]}\r" if debug
      import_row(carrier, row, debug, stats)
      stats[:line] += 1
    end

    puts "\nFinished loading" if debug

    # now remove anything that is older than startedAt.

    query = self.where(:carrier => carrier).where('updated_at < ?', startedAt)
    howmany = query.count

    if(howmany > 1000)
      stats[:failed_to_purge] = howmany
    else
      #query.delete_all
    end
  end

  def self.extend_footprint(carrier, stats = Hash.new(0))
    # look for new Local Exchanges, which are recognized by rate_center
    # being nil.

    active.where(carrier: carrier, rate_center_id: nil).find_each { |le|
      stats[:new_local_exgh] += 1

      # now for each LE, look for rate centers that have a matching
      # localname.
      # We might have to resort to different ways of doing the match.

      rc = RateCenter.find_by_localname_permuted(le.name, le.province, stats)

      if rc
        stats[:matched] += 1
        puts "Matched #{le.name}/#{le.province} to #{rc.lerg6name}"
        le.rate_center = rc
        le.save!
      end
    }
    return stats[:new_nae]
  end

  def connect_to_rate_center
    rc1 = RateCenter.find_by_localname(name, province).take
    if rc1
      self.rate_center = rc1
      save!
      return rc1
    end

    # if does not exist, then ???
    return nil
  end

  # LCA export code.
  def self.getRateCenters(carriername)
  end
  def self.writeLCAblock(rateCenterSet)
  end

  def self.writeLCAfile(filename, options)
    carrierlist = options[:carrier] || distinct.pluck(:carrier)

    carrierlist.each { |carriername|
      rateCenterSet = getRateCenters(carriername)
      writeLCAblock(rateCenterSet)
    }

  end

end
