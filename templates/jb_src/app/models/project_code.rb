class ProjectCode < ApplicationRecord
  belongs_to :client
  belongs_to :site
  belongs_to :service


  IMPORT_PRIMARY_KEY = :project_pid
  IMPORT_MAPPING = {
      '#BTN'    => 'BTN',
      '#ProjectPID' => 'ProjectPID',
      :client       => {
        :csvHeader  => 'BTN',
        :code       => Proc.new {|csvValue| find_client_by_btn(strip_btn_leading_zeros(csvValue))},
      },
      :project_pid  => 'ProjectPID',
      :code         => 'Code',
      :description  => 'Description',
      :active       => 'Active',
      :customer_pid => 'CustomerPID',
      :department_pid  => 'DepartmentPID',
      :service      => 'ServicePID',
      :assignment_level_pid     => 'AssignmentLevelPID',
      :owner_app_user_pid   => 'OwnerAppUserPID',
      :insert_app_user_pid  => 'InsertAppUserPID',
      :update_app_user_pid  => 'UpdateAppUserPID',
      :created_at   => 'CreatedOn',
      :updated_at   => 'UpdatedOn',
    }




end
