class YearlyStatsPeriod < StatsPeriod
  def beginning_of_period
    @beginning_of_period ||= self.day.beginning_of_year.to_date
  end
  def end_of_period
    @end_of_period       ||= self.day.end_of_year.end_of_day.to_date
  end

  # returns a scope for type which is below this kind
  def contained_period_types
    # if we add monthly periods, we might have to include them too
    StatsPeriod.where(:type => 'DailyStatsPeriod').where('stats_periods.hour IS NULL OR stats_periods.hour = 0')
  end

end
