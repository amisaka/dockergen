class ClientLineOfBusiness < ApplicationRecord

  # allows enabling/disabling
  include Montbeau::Activatable

  belongs_to :client

  belongs_to :line_of_business
end
