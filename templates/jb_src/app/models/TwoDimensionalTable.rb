require 'set'


# Start of a smarter way to handle complex reports for #13554
# But gave up quickly in favor of faster to code Monbeau::Helpers::arrayOfHashOutput
# The idea is that this calss would keep track of all headers, so there's a consistent number of columns for each row (array element)
# regardless of how many elements a hash has

class TwoDimensionalTable

    attr_reader :headers
    attr_reader :data

    def initialize()
        headers = Set.new
    end


    def addRow(row)
        headers.add(row.keys)
    end


    def toStr()

    end


    def toCsv()

    end
end
