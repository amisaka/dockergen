class Call < ApplicationRecord

    include ::Montbeau::Helpers

    include ::Montbeau::Invoicing::Tax

  scope :by_btn, -> (btn) { where(btn: btn) }
  scope :by_date, -> (date_begin = Date.today, date_end = Time.now) { where('call_date > ? AND call_date < ?', date_begin, date_end) }

   belongs_to :service

  def self.for(resource)
    case resource
    when String
      by_btn(resource)
    when Client
      by_btn(resource.btn.billingtelephonenumber)
    when Btn
      by_btn(resource.billingtelephonenumber)
    when Site
      self.joins(:service).where('services.site_id = ?',  resource.id)
    else
      raise "Cannot fetch calls for #{resource.class}"
    end
  end

  def client
    btn_object.client
  end

  def btn_object
    Btn.find_by billingtelephonenumber: btn
  end


  ########################################################################
  # Callculates the total duration of a hash or array of calls
  # Formatting is done in the CallDecorator
  ########################################################################
  def self.sumOfDuration(calls)
    calls = calls.values if calls.class == Hash

    raise "Did not get an array of calls" if ! [Call, CallDecorator].include?(calls.first.class)

    calls.sum{|c|
            c = c.object if c.class == CallDecorator
            c.duration
    }
  end



  def self.byService(calls)
    byService = {}
    calls.each {|c|
        c = c.object if c == CallDecorator
        if c.service.present?
            byService[c.service] = [] if byService[c.service].nil?

            byService[c.service] << c
        end
    }
    return byService
  end


  def self.byAreaCode(calls)
    byAreaCode = {}
    calls.each {|c|
        #byebug if c.destination_npa_nxx[0].nil?
        areaCode = c.destination_npa_nxx[0]

        next if areaCode.nil?

        byAreaCode[areaCode] = [] if byAreaCode[areaCode].nil?
        byAreaCode[areaCode] << c
    }

    return byAreaCode
  end

  def self.byInternationalArea(calls)
    byIntlArea = {}
    calls.select{|c| c.international?}.each {|c|
        intlArea = c.area
        byIntlArea[intlArea] = [] if byIntlArea[intlArea].nil?
        byIntlArea[intlArea] << c
    }

    return byIntlArea
  end

  def self.bySummaryType(calls)
    bySummaryType = {}
    calls.each { |c|
        if c.international?
            type = "INT'L CALLS"
        elsif c.toCanada?
            type = '1+ CANADA CALLS'
        elsif c.toUs?
            type = '1+ USA CALLS'
        elsif c.incomingTollFree?
            type = '800 CALLS'
        end

        bySummaryType[type] = [] if bySummaryType[type].nil?
        bySummaryType[type] << c
    }

    return bySummaryType
  end

  def incomingTollFree?
    service.try{:phone_service_type}.try{:toll_free?}
  end




  def self.bySite(calls)
    bySite = {}

    calls.each {|c|
        c = c.object if c == CallDecorator

        if c.service.try(:site).present?
            site = c.service.site
            bySite[site] = [] if bySite[site].nil?
            bySite[site] << c
        end
    }

    return bySite
  end


  def did_numeric_only
    did.tr('^0-9', '')
  end



  def destination_npa_nxx
    return ['']*2 if (called_number.blank? or international?)
    return self.class::guess_npa_nxx(called_number)
  end

  def did_npa_nxx
    return self.class::guess_npa_nxx(self.did)
  end


  def self.guess_npa_nxx(phoneNumber)

    if phoneNumber.include?(' ') and phoneNumber.split(' ')[0].length == 10
        phoneNumber = phoneNumber.split(' ')[0]
    end

    # If there are hyphens....
    if phoneNumber.include?('-')
        # Return the first set of 3 digits
        npanxx = phoneNumber.split('-').select{|s| s.tr('^0-9', '').length == 3}
        return [npanxx.first, npanxx.second]
    elsif phoneNumber.tr('^0-9', '').length == 10
        npanxx = phoneNumber.tr('^0-9', '')[0..5]
        return [npanxx[0..2], npanxx[3..5]]
    end

    return nil
  end


  def self.determineAndSetArea_allCalls
    Call.where(area: nil).or(where(province: nil)).all.map {|c| c.determineAndSetArea}
  end

  def determineAndSetArea
    if (area.nil? or province.nil?) and not international?
        npa, nxx = destination_npa_nxx
        nae = NorthAmericanExchange.where(npa: npa).where(nxx: nxx).take

        if nae.present? and nae.name.present?
            self.area = nae.name
            self.province = nae.province
            save!
        end
    end
    return self.area
  end

  def international?
    return (province == 'XN' or (province == '' and not ['DirAssist', ''].include?(area)))
  end

  def toCanada?
    NorthAmericanExchange::CDN_PROVINCES.include?(province)
  end

  def toUs?
    NorthAmericanExchange::US_STATES.include?(province)
  end




  ### This may be used in more than one method so it's here as a class constant
  CALL_TO_SERVICE_WHERE_CLAUSES = {
        exactMatch: 		         "s.service_id = c.did",  # Straight match
        extensions: 		         "s.service_id = split_part(c.did, ' ', 1)",  # Remove the extension after space
		addDigitOneToService: 	     "c.did = '1' || s.service_id",               # Add a one
        addPlusAndDigitOneToService: "c.did = '+1' || s.service_id",              # Add a one plus space
        addPlusAndDigitOneToCallDid: "'+1' || c.did = s.service_id",              # Add a one plus space
        tollfree:                    "s.service_id = c.called_number and (called_number like '8%' or called_number like '18%' or called_number like '+18%')",
  }


    ########################################################################
    # Makes a csv and / or to screen report of calls on a terminated service
    ########################################################################
    def self.callsOnTerminatedService(reportFilePath: nil, toScreen: true)
        sql = %Q[SELECT cl.id as client_id,
			       b.billingtelephonenumber,
			       cl.name as client_name,
			       s.id as internal_service_id,
			       s.service_id,
			       s.activated,
			       s.termination_date,
			       max(ca.call_date) last_call_date
			FROM services s,
			     calls ca,
			     clients cl,
			     btns b
			WHERE ca.call_date > s.termination_date
			  AND s.btn_id = b.id
			  AND cl.id = b.client_id
			  AND ca.service_id = s.id
			GROUP BY cl.id,
			         cl.name,
			         b.billingtelephonenumber,
			         s.id,
			         s.service_id,
			         s.activated,
			         s.termination_date,
			         s.display_text
		]

		callsOnTerminatedServices = self::connection.exec_query(sql)

        report = []
		callsOnTerminatedServices.each { |row|

            otherService = Service.where('start_date >= ?', row["termination_date"]).where(service_id: row["service_id"])

            state = "Non-assigné"
            if otherService.count == 1
                state = "Ré-assigné à #{otherService.take.client.to_s}"
            elsif otherService.count > 1
                state = "Ré-assigné à #{otherService.count} clients"
            end

            row["État du service"] = state

            report.push(row)

            #Instructed not to add this column to the report on 2018/05/23
            #reportLine = row.clone

            #reportLine[:assignedTo] = 'Non-assigné'

            #someoneElse = Service.where(activated: true, service_id: reportLine['service_id'])
            #reportLine[:assignedTo] = someoneElse.map{|s| s.btn.client.name}.join(', ') if someoneElse.count > 0

            #report.push(reportLine)

		}

        Montbeau::Helpers::arrayOfHashOutput(report: report, reportFilePath: reportFilePath, toScreen: toScreen, title: "Services terminated but active")


    end




    def self.serviceIdUnableToFind(reportFilePath:, toScreen: true)
        unassociatedCalls = Call.distinct.where(service: nil).order(:did).pluck(:did)

        unassociatedCalls = unassociatedCalls.map{|v| {:call_id => v}}
        Montbeau::Helpers::arrayOfHashOutput(report: unassociatedCalls, reportFilePath: reportFilePath, toScreen: toScreen, title: "Unique service ID unable to find")
    end


    ########################################################################
    # Prints or outputs to csv calls that are not associated to a service
    # Also includes service info for calls that have multiple services matching
    #
    # @param [Hash] opts Options for the report
    # @option [String] :reportFilePath Where the csv file will be saved will go
    # @option [Boolean] :toScreen Also print to screen
    ########################################################################
    def self.unassociatedCalls(reportFilePath: nil, toScreen: true)

        # Take the calls that don't have a service
        unassociatedCalls = Call.where(service: nil)

        report = []
        whereClauses = self::CALL_TO_SERVICE_WHERE_CLAUSES.clone

        baseSql = 'SELECT distinct s.* from calls c, services s WHERE true AND '

        i = 0
        unassociatedCalls.each_with_index {|call, i|
            Montbeau::Helpers::printProgress("Processing unassociated calls...", i, unassociatedCalls.count, true)
            callClause = " c.id = #{call.id} "

            # Go through each where clause, attempt to associate calls to services
            servicesFound = whereClauses.values.map {|wc|
                matchSql = baseSql + callClause + ' AND ' + wc

                Service.find_by_sql(matchSql)
            }.flatten

            # Now bulid your report!
            reportLine  = call.slice(:id, :btn, :did)
            reportLine  = reportLine.map { |k, v| [('call_' + k.to_s).to_sym, v] }.to_h

            reportLine.merge!({:service_count => servicesFound.count})

            report << reportLine if servicesFound.count == 0

	        lineForCall = 0
	        servicesFound.each {|sv|
	            lineForCall += 1
	            reportLine = {id: '', btn: '', did: '', service_count: ''} if lineForCall > 1 # Not the smartest way, I would prefer a table class that can handle array of hashes
	            reportLine.merge!(sv.slice(:id, :service_id, :display_text, :activated, :termination_date))
	            report << reportLine
	        }

            i+= 1
        }

        Montbeau::Helpers::arrayOfHashOutput(report: report, reportFilePath: reportFilePath, toScreen: toScreen, title: "Unmatched calls")


    end



  ########################################################################
  # Using some fancy SQL, associate calls to service
  # Using SQL rather than code presumably makes the association go faster
  # Indeed, we associated calls to services within 10 secs on production
  ########################################################################
  def self.associateCallsToService
	startMethodTime = Time.now

    ### Thesae are the different where clauses we will use to
    # attempt to associate a call to service using call.did = service.service_id
    whereClauses = self::CALL_TO_SERVICE_WHERE_CLAUSES.clone

    # We only want to oconsider the services that are not deleted
    # and calls that don't have a service
	serviceTableConstraints = %Q[
			     and s.deleted = false
		 		 and c.service_id is null
	]

    # This clause will get added on the first round.
    # Then we will consider not activated services
    # The having count(s.id) = 1 will constrain the search to un re-assigned services
    #activatedClause = %Q[ and s.activated = true ]

    # First round with activated services
    [true, false].each {|activated|

        # Go through each method of associating a call to service
	whereClauses.each {|whereClauseName, wc|
		startTime = Time.now

		# Add the constraint of which service and calls we are considering
		wcWithConstraints = wc + serviceTableConstraints

		# Add the activated clause if this is first round
		wcWithConstraints += " and s.activated = #{activated.to_s} "

        # We only waant to consider services and calls we can only associate to one service
	    oneMatchSql = %Q[
			SELECT c.id
			     FROM calls c,
			          services s
			     WHERE #{wcWithConstraints}
			     GROUP BY c.id
			     HAVING count(s.id) = 1
	    ]

	    # Build our match table of calls to services
		matchTableSql = %Q[
			SELECT c.id as call_id, s.id as service_id
				FROM calls c,
					services s
				WHERE #{wcWithConstraints}
				AND c.id IN (#{oneMatchSql})
		]

        # How many matches have we found
		matchCount = self::connection.exec_query(matchTableSql).count

        # Our update sql
		updateSql = %Q[
			UPDATE ONLY calls c
			SET service_id = lookup.service_id
			FROM (#{matchTableSql}) as lookup
			where c.id = lookup.call_id
		]

		self::runUpdate(updateSql, 'matchServiceBy_' + whereClauseName.to_s)
		self::reportManyCallsAssociated()

	    serviceStatus = (activated ? "activated" : "deactivated")
		endTime = Time.now
		puts "Where clause '#{wc}' matched #{matchCount} calls in #{(endTime-startTime).round} second with #{serviceStatus} services"

	}
    }

	endMethodTime = Time.now
	puts "Call::associateCallsToService run time: #{(endMethodTime-startMethodTime).round(1)} seconds"
  end


  def destination_rate_center()
    npa, nxx = self.destination_npa_nxx
    nae = NorthAmericanExchange.find_by_npa_nxx(npa, nxx).take
  end

  def did_rate_center()
    npa, nxx = self.did_npa_nxx
    nae = NorthAmericanExchange.find_by_npa_nxx(npa, nxx).take
  end

  def self.reportManyCallsAssociated()
	sleep 1
	callsCount = Call.count
	hasServiceIdCount = Call.where("service_id is not null").count
	puts "#{(hasServiceIdCount.to_f*100/callsCount).round(1)}% of calls associated to service. #{callsCount-hasServiceIdCount} not associated."
  end

  def self.runUpdate(sql, queryName = nil)
		numRowsChanged = self::connection.exec_update(sql, queryName)
		puts "#{numRowsChanged} rows changed"
  end

  def self.resetAllServiceIds
		self::runUpdate("UPDATE ONLY Calls SET service_id = null", 'resetServiceId')
  end

  def tax
    return 0 if self.international?

    prov = self.service.try(:site).try(:province)

    if prov.present?
        ctc = compute_taxes_canada({prov=> self.cost})
        taxes =  ctc[:CA].values.first
        total_taxes = taxes[:total] - taxes[:amount]
        return total_taxes
    else
        return nil
    end
  end

  def amount
    self.cost
  end

  def self.callDetailReport(clientObj:, reportFilePath: nil, toScreen: :sample, locale: :us)

    report = []

    calls = Call.for(clientObj)
    calls.each_with_index { |call, i|
        Montbeau::Helpers::printProgress("Processing cdrs...", i, calls.count, true)
        service = call.service


        taxAmount = call.tax
        totalAmount = taxAmount + call.amount

        row = {
            "Customer Name"         => service.try(:btn).billingtelephonenumber,
            "Department Number"     => service.try(:site).try(:site_number),
            "Service Number"        => service.try(:service_id),
            "Usage Date"            => I18n.localize(call.call_date.to_date, locale: locale, format: :short_numeric),
            "Usage Time"            => I18n.localize(call.call_date.to_time, locale: locale, format: :short_with_s),
            "Bill duration"         => CallDecorator.fmtDuration(call.duration),
            "Destination Country"   => '', #Always blank, it would seem
            "Destination City"      => '', #Always blank, it would seem
            "Destination Number"    => call.called_number,
            "Destination Place"     => call.international? ? '' : call.area,
            "Destination State"     => call.international? ? '' : call.province,
            "Exchange Type"         => '', #Not blank, missing data (IntraLATA, Interstate LATA)
            "Usage Cateogry"        => '', #Not blank, missing data (Toll Free, Sent Paid, Long Distance)
            "Usage Group"           => '', #Not blank, missing data (800 or Feature, NOR)
            "Origination Country"   => '', #Always blank, it would seem
            "Origination City"      => '', #Always blank, it would seem
            "Origination Number"    => call.did,
            "Origination Place"     => service.try(:site).try(:buliding).try(:city), #Not blank, probably missing real did isntead of vDID, could use service city?
            "Origination State"     => service.try(:site).try(:province),  #Not blank, probably missing real did isntead of vDID, could use service province?
            "Destination Number 2" => '', #Mostly blank (~95%) not always
            "Orination Number 2"   => '', #Many blank, many not (~50/50)
            "Total Rate Amount"     => call.amount,
            "Total tax"             => taxAmount,
            "Total Amount"          => totalAmount,
        }

        report << row
    }


    Montbeau::Helpers::arrayOfHashOutput(report: report, reportFilePath: reportFilePath, toScreen: toScreen, title: "Call detail report")
  end





  IMPORT_MAPPING = {
    btn:  "BTN",
    did: "DID",
    date: "Date",
    time: "Time",
    call_date: {
        :rowHandler => Proc.new {|row| convert_to_date_time([Date.today.year, row[2], row[3]].join(" "))}
    },
    called_number: "Called Number",
    area: "Area"  ,
    province: "Province",
    duration: {
        :csvHeader => "Duration",
        :code => Proc.new {|csvValue| parse_duration(csvValue)},
    },
    cost:  "Cost",
    imported_at: {
        :exec => Proc.new {DateTime.now}
    }
  }

    IMPORT_DONT_UPDATE = true
    IMPORT_START_EMPTY = true

end
