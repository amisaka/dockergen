# Callstat and subclasses:
#    1. CallCallstat       -- daily count of total calls.
#    2. VoiceMailCallstat  -- number of calls that went to voicemail
#    3. InternalCallstat   -- number of calls that were inside client
#    4. VMcollectedCallstat - number of calls that were just to the voicemail portal to listen to voicemail
#    5. AbandonedCallstat  -- number of calls where caller hung up before being answered, or within 20s of voice mail
#    6. IncomingCallStat   -- number of calls that went in.
#    7. AnsweredCallStat   -- number of calls that were answered (included in 6)
#    8. OutgoingCallStat   -- number of calls that went out.
#    9. PeakCallCountStat  -- maxium number of simultaneous calls that were occuring
#

require 'sequence_allocator'

class Callstat < ApplicationRecord
  belongs_to :stats_period
  belongs_to :client
  belongs_to :phone, class_name: Service
  self.primary_key = "id"

  include SequenceAllocator

  # this code pay attention to :type=> to make the right STI.
  class << self
    def new_with_cast(*a, &b)
      if (h = a.first).is_a? Hash and (type = h[:type] || h['type']) and (klass = type.constantize) != self
        raise "wtF hax!!"  unless klass < self  # klass should be a descendant of us
        return klass.new(*a, &b)
      end
      new_without_cast(*a, &b)
    end
    alias_method_chain :new, :cast
  end

  def self.create_for_period(sp, stattype, notify, phone, client)
    cs = where(:stats_period_id => sp.id, :type => stattype, :phone_id => phone.try(:id), :client_id => client.id)
    unless cs.try(:first)
        $stderr.puts sprintf("Creating     callstat: %20s for %s, %3s phone=%-16s",
                             stattype, sp.day, sp.hour, phone.try(:name), client.name) if notify
        cs1 = Callstat.new(:stats_period => sp,
			   :client => client,
			   :phone  => phone,
			   :type   => stattype)
        cs1.value = 0
	cs = [cs1]
    else
      $stderr.puts sprintf("Fo %9u callstat: %20s for %s, %3s phone=%-16s",
                           cs.first.id, stattype, sp.day, sp.hour, phone.try(:name), client.name) if notify
    end
    cs  # returns an array/association
  end

  scope :today, lambda { |*args|
    day    = args.first || Time.now
    joins(:stats_period).merge(StatsPeriod.today(day))
  }

  def self.within_period(period)
    joins(:stats_period).merge(period.within_period).merge(period.contained_period_types)
  end

  def self.increment_type_for_periods(sp1, sp2, type, notify, phone, client)
    g1s = create_for_period(sp1, type, notify, phone, client)
    if g1s.try(:first)
      g1s.first.increment(notify)
    end
    g2s = create_for_period(sp2, type, notify, phone, client)
    if g2s.try(:first)
      g2s.first.increment(notify)
    end
  end

  def name
    if(stats_period)
      "#{type}:#{stats_period.name}"
    else
      "#{type}:<invalid-period>"
    end
  end

  def day
    stats_period.try(:day)  || "noday"
  end
  def hour
    stats_period.try(:hour) || "allday"
  end

  def persist!
    save! if self.changed?
  end

  def recount
    do_counts
    persist!
    self.value
  end

  def count
    if self.id.nil? or self.value.blank?
      recount
    end
    self.value
  end

  def maybe_recount(recount)
    recount ? self.recount : self.count
  end


  def increment(notify = false)
    # consider using: https://github.com/seamusabshere/upsert instead!
    transaction do
      self.value += 1
      save!
    end
    $stderr.puts "  incrementing: #{id}/#{stats_period_id} #{type} -> #{value}" if notify
  end

  def calls_for_client_period
    client.calls.today(stats_period.beginning_of_period, stats_period.end_of_period)
  end

  def outgoing_calls_for_period
    calls = calls_for_client_period.outgoing
    if phone #(Class name of attribute phone is Service - t13331)
      calls = calls.joins(:cdrs).where(:cdrviewer_cdr => { :callingphone_id => phone.id })
    end
    return calls
  end

  def incoming_calls_for_period
    calls = calls_for_client_period.incoming
    if phone #(Class name of attribute phone is Service - t13331)
      # if phone is non-nil, then we have to filter cdrs
      # for ones which involve that extension.  We assume that
      # we only look for incoming calls.
      calls = calls.joins(:cdrs).where(:cdrviewer_cdr => { :calledphone_id => phone.id })
    end

    return calls
  end

  # Simmilar to incoming calls for period
  # but returns an array based on t10440 TelAide effort
  def incoming_calls_for_period_array
    callsIncoming = calls_for_client_period.reject { |call|
        call.outgoingTheirNumber?
    }

    return callsIncoming
  end

  # Simmilar to outgoing calls for period
  # but returns an array based on t10440 TelAide effort
  def outgoing_calls_for_period_array
    callsOutgoing = calls_for_client_period.find_all { |call|
        #(Class name of attribute phone is Service - t13331)
        call.outgoingTheirNumber?(phone) && !call.internal_call? && !call.collected_voicemail?
    }

    return callsOutgoing
  end

  def count_outgoing_calls_for_period
    count = 0
    calls_for_client_period.find_each { |call|
      if call.outgoingTheirNumber?(phone) && !call.internal_call? && !call.collected_voicemail?
        count += 1
      end
    }
    return count
  end


  def incoming_cdrs_for_period
    client.incoming_cdrs.today(stats_period.beginning_of_period, stats_period.end_of_period)
  end

  def outgoing_cdrs_for_period
    client.outgoing_cdrs.today(stats_period.beginning_of_period, stats_period.end_of_period)
  end

end

# daily call stats
class CallCallstat < Callstat

  def do_counts
    # ignores extension!
    self.value = calls_for_client_period.count
    save!
  end

end

# voicemail call stat: number of calls that went to voice mail.
class VoiceMailCallstat < Callstat
  def do_counts
    #How this was done previous to t10440
    #arel = incoming_calls_for_period.went_to_voicemail

    #How this is counted after t10440.  - jlam@credil.org
    #I can't explain off the top of my head why the previous method
    #is incorrect
    arel2 = incoming_calls_for_period_array.find_all  { |call|
        #(Class name of attribute phone is Service - t13331)
        call.oneLegToVoicemail?(phone)
    }
    self.value = arel2.count
    save!
  end

end

# internal call stat: connection between two extensions
class InternalCallstat < Callstat
  def do_counts
    self.value = 0
    #(Class name of attribute phone is Service - t13331)
    if phone.try(:virtualdid?) && !phone.voicemailportal?
      arel2 = incoming_calls_for_period_array.find_all  { |call|
        call.involved_phones.include?(phone) && call.internal_call? && !call.collected_voicemail?
      }
      self.value = arel2.count
    end
    # else answer is 0.
    save!
  end
end

# voicemail collected call stat: internal call that goes to voice mail portal
class VMcollectedCallstat < Callstat
  def do_counts
    self.value = 0
    #(Class name of attribute phone is Service - t13331)
    if phone.try(:virtualdid?) && !phone.voicemailportal?
      arel2 = incoming_calls_for_period_array.find_all  { |call|
        call.involved_phones.include?(phone) && call.internal_call? && call.collected_voicemail?
      }
      self.value = arel2.count
    end
    # else answer is 0.
    save!
  end
end

# abandoned call stat: number of calls that went were abandoned before being answered
#  (and did not even go to voicemail)
class AbandonedCallstat < Callstat

  def do_counts
    # phone is an attribute of the model
    count = 0
    incoming_calls_for_period_array.find_all{ |call|
      count +=1  if call.abandoned?(phone)
    }
    self.value = count
    save!
  end

end

# DID stat -- collects number of incoming calls for a specific (non-virtual) DID
class DidCallstat < Callstat

  def do_counts
    # phone is an attribute of the model
    count = 0
    incoming_calls_for_period_array.find_all{ |call|
      count +=1  if call.answered?(phone)
    }
    self.value = count
    save!
  end

end

# HuntGroup stat -- collects number of incoming calls for a specific virtual DID(handset)
#                   which are received via a hunt group call
class HuntGroupCallstat < Callstat
  def do_counts
    count = 0
    incoming_calls_for_period_array.find_all{ |call|
      count +=1  if call.huntgroup?
    }
    self.value = count
    save!
  end
end

class OutgoingNoAnswerCallstat < Callstat
end

class DailyCallstat < CallCallstat
end

class IncomingCallStat < Callstat
  def do_counts
    self.value = incoming_calls_for_period_array.uniq{|call| call.id }.count
    save!
  end
end

class AnsweredCallStat < Callstat
  def do_counts
    self.value = incoming_calls_for_period_array.uniq{|call| call.id}.find_all{ |call|
        ! call.oneLegToVoicemail? && call.answered?(phone)
    }.count
    save!
  end
end

class OutgoingCallStat < Callstat
  def do_counts
    self.value = count_outgoing_calls_for_period
    save!
  end
end

class IncomingCdrStat < Callstat
  def do_counts
    self.value = outgoing_cdrs_for_period.count(:id, :distinct => true)
    save!
  end
end

class OutgoingCdrStat < Callstat
  def do_counts
    # this is not the most efficient way to generate things.
    self.value = outgoing_cdrs_for_period.count(:id, :distinct => true)
    save!
  end

end


class PeakCallCountStat < Callstat
  def do_counts
    # need to find all the periods with given stats period
    self.value = client.callstats.within_period(stats_period).maximum(:value)
    save!
  end
end
