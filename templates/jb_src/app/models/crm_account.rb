require 'uuid'

class CrmAccount < ApplicationRecord
  self.primary_key = "id"
  before_create :create_id

  has_one :client

  belongs_to :crm_user, :class_name => SuiteCrm::User, :foreign_key => 'assigned_user_id'
  has_many :customer_users, :class_name => OTRS::CustomerUser, :foreign_key => 'customer_id'

  # validates :name,
  #   presence: true,
  #   length: { in: 0..255 },
  #   allow_blank: false

  def billing_address_1
    return billing_address_street
    # if billing_address_street_2 == nil or billing_address_street_2.length == 0
    #   return billing_address_street
    # else
    #   return [ billing_address_street, billing_address_street_2]
    # end
  end

  def billing_address_2
    ary = []

    # Instructed as per canada post mailing standards not to include the country (t13583)
    # See https://www.canadapost.ca/tools/pg/manual/PGaddress-e.asp?ecid=murl10006450#1416995
    #[billing_address_city, billing_address_state, billing_address_country].each do |m|
    [billing_address_city, billing_address_state].each do |m|
      ary << m unless m.blank?
    end
    ary.join(' ')
  end

  def billing_address_3
    billing_address_postalcode
  end

  def billing_address_4
  end

  def billing_province
    billing_address_state
  end

  def billing_country
    billing_address_country
  end

  def customer_users_site(site)
    OTRS::Customer_User.where(:customer_id => client.billing_btn + " / " + site.id)
  end

  def self.clean_unref
    transaction {
      CrmAccount.where(:deleted => 0).update_all(:deleted => 2)
      Client.activated.find_each {|client|
        if client.crm_account
          if client.crm_account.deleted == 2
            client.crm_account.deleted = 0
            client.crm_account.save
          end
        end
      }
      CrmAccount.where(:deleted => 2).delete_all
      CrmAccount.where(:deleted => 1).delete_all
    }
  end

  def terminated!
    self.deleted = 1
    save!
  end

  def merge(othercrm)
    attributes.each { |key, value|
      attributes[key] ||= othercrm[key]
    }
    save!
  end

  protected

  # this routine is not used when things go well, as the crm_accounts.id
  # is actually the customer BTN/Account Code.
  def self.gen_id
    UUID.new.generate(:compact)
  end

  def create_id
    self.id ||= self.class.gen_id
    return self
  end

  private
  def timestamp_attributes_for_create
    super << :date_entered
  end
  def timestamp_attributes_for_update
    super << :date_modified
  end

end
