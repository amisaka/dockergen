class PhoneContact < ApplicationRecord
  belongs_to :client

  def phone_types
    return [ "Home",
             "Work",
             "Fax",
             "Mobile",
             "Work1",
             "Work2",
             "Work3",
             "Primary",
             "Home 2",
             "Mobile 2",
             "Mobile 3",
             "Technical Lead",
             "General",
             "Billing" ]
  end
end
