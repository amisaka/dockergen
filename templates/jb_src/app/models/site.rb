require 'csv'

class Site < ApplicationRecord

  include Montbeau::Activatable

  belongs_to :building

  accepts_nested_attributes_for :building

  belongs_to :client

  has_many :services

  has_many :invoices

  has_many :active_phones,
    -> { activated },
    class_name: 'Service'

  has_one  :btn,
    foreign_key: "billing_site_id"

  # if $OTRS
  #   has_one  :otrs_customercompany,
  #     class_name: 'OTRS::CustomerCompany'
  #
  #   has_many :otrs_customeruser,
  #     class_name: 'OTRS::CustomerUser',
  #     through: 'otrs_customercompany',
  #     source: 'otrs_customerusers'
  #
  #   belongs_to :site_rep,
  #     class_name: 'OTRS::CustomerUser'
  #
  # end

  has_many :billable_services

  # if $HAKKU
  #   belongs_to :hakku_department,
  #     class_name: 'Hakku::Department',
  #     foreign_key: 'foreign_pid'
  # end

  validates :support_service_level,
    presence: true,
    allow_nil: true,
    numericality: true

  # validates :name,
  #   presence: true
  #
  # validate :name_cannot_be_missing
  #
  # def name_cannot_be_missing
  #   if name == '[MISSING]'
  #     errors.add(:name, "can't be blank")
  #   end
  # end

  # validates :site_number,
    # presence: true

  scope :by_site_number, -> { order(site_number: 'ASC') }

  # Site's address is building's adress
  delegate :address, to: :building, allow_nil: true

  # Delegate setters and getters to building with fallbacks
  [:address1, :address2, :city, :province, :country, :postalcode].each do |m|
    eval <<-RUBY
      def #{m}
        building.#{m}
      rescue NoMethodError
        nil
      end

      def #{m}=(val)
        building.#{m}=(val)
      rescue NoMethodError
        nil
      end
    RUBY
  end

  # remove this, don't create relations like this
  # before_save :create_building,
  #   unless: :has_building?

  # a site is valid if it hasn't got a deactivated_at.
  scope :activated, lambda {|*args|
    # time = args.first || Time.now.utc
    # where('(sites.activated_at IS NULL OR sites.activated_at <= ?) AND (sites.deactivated_at >= ? OR sites.deactivated_at IS NULL)',
    #     time, time)
    # with above sql we weren't showing reactivated sites.
    where('(sites.activated_at > sites.deactivated_at) or (sites.deactivated_at is null)')
  }

  #
  def self.cache_name_key(client, name)
    "client#{client.id}_site_by_name:#{name}"
  end

  def self.cache_id_key(id)
    "site_by_id:#{id}"
  end

  #
  def self.get_by_id(id)
    # note caches nil returns
    Rails.cache.fetch cache_id_key(id) do
      find_by_id(id)
    end
  rescue ActiveRecord::AssociationTypeMismatch
    find(id)
  end

  alias_attribute :site_name, :name
  # alias_attribute :activated_at,  :activated_at
  # alias_attribute :terminated_at, :deactivated_at

  # what is this?
  def phonecfg_csv(csv)
    services.each { |p| p.phonecfg_csv(csv) }
    #puts "Exported site:#{name}\n"
  end

  # NOTE: is this for CDR load process?
  # we redefine this so that we can take advantage of the Rails.cache
  # for this object.  There should be a better way, inside of AR, maybe.
  # def client
  #   @client ||= Client.get_by_id(self[:client_id])
  # end

  # @deprecated use `deactivated?`
  def terminated?
    !activated?
  end

  # @deprecated use `activate`
  def active!
    activate!
  end

  # @deprecated use `deactivated`
  def terminated!(now = Time.now)
    # self.site_rep.try(:terminated!) if $OTRS
    deactivate!
  end

  # @deprecated use `activated?`
  def active?(now = Time.now)
    activated?
  end

  # NOTE: extract
  # ##
  # ## importing
  # ##
  # def self.import_row(row)
  #   # "Customer PID","Account Number","Department Status","Department start date","Department Number","Department Name","Physical Address 1","Physical Address 2","Physical City","Physical Province","Physical Post Code","Termination Date"
  #   (foreign_pid,    account_code,    site_status,        site_activation_date,   site_number,        site_name,        physical_address_1,  physical_address_2,  physical_city,  physical_province,  physical_postalcode, site_termination_date, country) = row
  #
  #   # skip commend headers
  #   return :comment if foreign_pid == "Customer PID"
  #   return :blank   if foreign_pid == ''
  #
  #   # find the customer by foreign_pid first.
  #   client = Client.find_by_foreign_pid(foreign_pid)
  #   return :noclient unless client
  #
  #   site = client.sites.find_by_site_number(site_number)
  #   unless site
  #     site = client.sites.find_by_name(site_name)
  #     if site
  #       site.site_number = site_number
  #     end
  #   end
  #   unless site
  #     site = new(name: site_name, site_number: site_number)
  #     site.save(validate: false)
  #     client.sites << site
  #     site.client = client
  #     site.save(validate: false)
  #   else
  #     site.name = site_name
  #   end
  #
  #   if site.client.sites.count > 0
  #     maybelog("\n  client#{site.client.name}/#{site.client.id} adding site: #{site_number}\n")
  #   end
  #
  #   building = site.building
  #   unless building
  #     building = site.building = Building.new
  #   end
  #   building.address1 = physical_address_1
  #   building.address2 = physical_address_2
  #   building.city     = physical_city
  #   building.province = physical_province
  #   building.country  = country || "Canada"
  #   building.postalcode=physical_postalcode
  #   site.activated_at = site_activation_date
  #   unless site_termination_date.blank?
  #     site.deactivated_at = site_termination_date.to_date
  #   end
  #   building.save(validate: false)
  #
  #   # make_otrs_user will be done by site_renumber
  #   # site.make_otrs_user
  #   site.save(validate: false)
  #
  #   return site
  # end

  # TODO: move to decorator
  def billing_address_1
    if building
      unless unitnumber.blank?
        "Unit #{unitnumber}, "
      else
        ""
      end + (building.address1||"")
    else
      ""
    end
  end

  # TODO: move to decorator
  def billing_address_2
    building.try(:address2) or ''
  end

  # TODO: move to decorator
  def billing_province
    building.try(:province)
  end

  # TODO: move to decorator
  def billing_country
    building.try(:country)
  end

  # TODO: move to decorator
  def billing_address_3
    if building.try(:city) and building.try(:province)
      [building.city, building.province, building.postalcode||""].join(', ')
    else
      "city missing!"
    end
  end

  # TODO: move to decorator
  def billing_address_4
    building.try(:country) || "country missing"
  end

  # def support_service_level=(x)
  #   self[:support_service_level] = x
  #   if site_rep
  #     site_rep.support_service_level = x
  #     site_rep.save!
  #   end
  # end

  def make_otrs_user(logged = false)
    if $OTRS
      if !self.site_number.blank? and otrs_customercompany
        if site_rep
          site_rep.update_from_site(self, logged)
        else
          self.site_rep = OTRS::CustomerUser.make_from_site(self)
        end
      end
    end
  end

  # NOTE: extract
  # def self.import_file(options)
  #   defaults = { overwrite: false,
  #                verbose:   false,
  #                archive:   false}
  #   options = defaults.merge!(options)
  #   clientlist = options[:clientlist]
  #   filename   = options[:file]
  #   siterecalc_list = Hash.new
  #
  #   handle = File.open(filename, "r:ISO-8859-1:UTF-8")
  #   #handle = File.open(filename, "r:UTF-8")
  #   return nil unless handle
  #
  #   linecount=0
  #   loadedcount=0
  #
  #   debug = true if ENV['IMPORT_DEBUG']
  #   print "\n" if debug
  #
  #   CSV.parse(handle) do |row|
  #
  #     linecount += 1
  #     print " #{filename}:#{linecount} -- " if debug
  #     result = import_row(row)
  #     case result
  #     when :noclient
  #       raise "Can not import site at #{linecount}, no such client #{row[0]}"
  #
  #     when :comment
  #     when :blank
  #     else
  #       client = result.try(:client)
  #       if(client)
  #         print "#{client.name} site: #{result.name}                " if ENV['IMPORT_DEBUG']
  #         if clientlist
  #           clientlist[client.id] = nil
  #         end
  #         siterecalc_list[client.id] = true
  #       end
  #       loadedcount += 1
  #     end
  #     print "\n" if ENV['IMPORT_DEBUG']
  #   end
  #
  #   print "\nCreating sites for single-site clients\n" if ENV['IMPORT_DEBUG']
  #
  #   # if there are any further clients not touched, then we create a site for them too.
  #   if clientlist
  #     total = clientlist.size
  #     num   = 1
  #     clientlist.each { |key, client|
  #       if client
  #         print sprintf("%04d/%04d .. %s [#%d]\n", num, total, client.name, client.id) if ENV['IMPORT_DEBUG']
  #
  #         newsite = client.clienthq(nil)
  #         unless client.billing_site
  #           client.billing_site = newsite
  #           client.save!
  #         end
  #
  #         unless newsite.building
  #           newsite.building = Building.create
  #         end
  #         newsite.building.from_crm_account(client.crm_account)
  #         newsite.client = client
  #         newsite.make_otrs_user(debug) if $OTRS
  #         newsite.save!
  #       end
  #       num += 1
  #     }
  #   end
  #
  #   # now, for clients that had sites that were created/updated,
  #   # go through the list, and fix up the site names.
  #   # we do this last, because we need the complete new name of sites.
  #   total = siterecalc_list.size
  #   num = 1
  #   siterecalc_list.each { |clientid, avail|
  #     client = Client.find(clientid)
  #     print sprintf("%04d/%04d .. uniquify %s \n", num, total, client.name) if debug
  #     num += 1
  #
  #     client.site_renumber(100)
  #   }
  #
  #   return loadedcount
  # end

  def name
    @name ||= self[:name] || building.try(:name)
    if @name.blank?
      '[MISSING]'
    else
      @name
    end
  rescue NoMethodError
    '[MISSING BUILDING]'
  end

  def to_s_withSiteNum
    (self.site_number or '') + ' ' + self.name
  end

  def to_s
    "{#{id}}#{name} (#{site_number})"
  end

  def reload
    @name = nil
    super
  end

  # NOTE: extract
  # def merge_from(osite)
  #   osite.phones.each { |ph|
  #     ph.site = self
  #     ph.save!
  #   }
  #
  #   self.alt_name = osite.name
  #   osite.terminated!
  #   self.active!
  #
  #   save!
  #   osite.save!
  #   self
  #
  #   if self.client.billing_site == osite
  #     self.client.billing_site = self
  #     self.client.save!
  #   end
  # end

  # NOTE: extract
  # def self.mergesites(site1, site2)
  #   count1 = site1.phones.count
  #   count2 = site2.phones.count
  #   if count1 > count2
  #     keepsite = site1
  #     mergesite= site2
  #   else
  #     keepsite = site2
  #     mergesite= site1
  #   end
  #
  #   yield mergesite, keepsite
  #   keepsite.merge_from(mergesite)
  # end

  private

    def has_building?
      building.present?
    end

    def create_building
      self.building = Building.new
      self.building.save(validate: false)
    end

end
