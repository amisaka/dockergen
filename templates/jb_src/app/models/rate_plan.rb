class RatePlan < ApplicationRecord
    IMPORT_PRIMARY_KEY = :rate_plan_pid
    IMPORT_DELETE_NOT_UPDATED = true
    IMPORT_MAPPING = {
        rate_plan_pid: "RatePlanPID",
        code: "Code",
        description: "Description",
        active: "Active",
        usage_type: "UsageTypePID",
        owner_app_user_pid: "OwnerAppUserPID",
        owner_team_pid: "OwnerTeamPID",
        insert_app_user_pid: "InsertAppUserPID",
        update_app_user_pid: "UpdateAppUserPID",
        created_at: "CreatedOn",
        updated_at: "CreatedOn", # As instructed by Miranda on 2018/05/22 at 17:31 in F-3014
        bc_rate_plan_pid: "BCRatePlanPID",

        imported_at: {
            exec: Proc.new {DateTime.now}
        },

        # No longer in use
        effective_date: {
            literal: nil,
        },
        province: {
            literal: nil,
        },
        country: {
            literal: nil,
        },
        country_code: {
            literal: nil,
        },
        carrier_id: {
            literal: nil,
        },

        #usage_type: {
        #    :csvHeader => "Usage Type",
        #    :code   => Proc.new { |csvValue|
        #        UsageType.find_by(:name => csvValue)
        #    }
        #}
    }


  has_many :btns
  has_many :rates
  belongs_to :carrier
  belongs_to :usage_type


  ########################################################################
  # BEGIN SYNONYMS

  def name
    code
  end

  def name=(value)
    code = value
  end


  # END SYNONYMS
  ########################################################################


  def self.default
    find_by_name('ProvincePlan')
  end

  def acceptable_rate_center(rc)
    false
  end

  def plan_discount(rate, duration)
    # no systematic discounts at this time, but maybe *0.95 or something
    # in the future.
    rate.base_price(duration)
  end

  # inherit.
  def always_zero?
    false
  end

  def find_rate_for(rate_center)
    rates.where(rate_center: rate_center).take
  end

  def to_s
    name
  end

  def self.incoming_toll_rate_plan
    TollRatePlan.where(id: 800).take
  end


end

# singleton with id: 800.
class TollRatePlan < RatePlan
  def find_rate_for(rate_center)
    Rate.tollrate_plan
  end

  def acceptable_rate_center(rc)
    return true if rc.tollfree?
  end
end

class LocalRatePlan < RatePlan
  def self.default
    # hard code default from initialdata/rate_plans
    where(name: 'ProvincePlan').take
  end

  def self.canada_wide
    # hard code default from initialdata/rate_plans
    where(name: 'NationalPlan').take
  end

  def acceptable_rate_center(rc)
    return false unless rc
    return true  if rc.tollfree?

    anyprovince  = self.province.blank? || self.province == 'nil'
    sameprovince = self.province.try(:downcase) == rc.province.try(:downcase)
    samecountry  = self.country.try(:downcase) == rc.country.try(:downcase)

    case
    when (self.carrier and anyprovince and samecountry  and rc.includes_exchange?(self.carrier))
      # example of a national rate plan -- to all places
      true
    when (self.carrier and sameprovince and samecountry and rc.includes_exchange?(self.carrier))
      # example of a provincial rate plan -- to all places
      true
    when (self.carrier.nil? and anyprovince and samecountry)
      # example of a national rate plan -- to all places
      true
    when (self.carrier.nil? and sameprovince and samecountry)
      # example of a provincial rate plan -- to all places
      true
    else
      false
    end
  end

  def always_zero?
    true
  end
end

class NationalRatePlan < RatePlan
  def self.default
    # hard code default from initialdata/rate_plans
    where(name: 'VoIP-NorthAmerican-Bronze').take
  end

  # a national rate plan applies when the country code of the plan
  #   matches the country_code of the RC.
  def acceptable_rate_center(rc)
    return false unless rc
    self.country_code == rc.country_code # and self.country == rc.country
  end
end

class InternationalRatePlan < RatePlan
  def self.default
    # hard code default from initialdata/rate_plans
    where(name: 'VoIP-Country-Bronze').take
  end

  def acceptable_rate_center(rc)
    return true
  end
end
