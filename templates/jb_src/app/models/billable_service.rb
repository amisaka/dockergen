class BillableService < ApplicationRecord
  # include SetOperations
  include Montbeau::Activatable
  after_save :show_diff
  enum billing_period: {
    never: 0,
    monthly: 1,
    yearly: 2
  }

  enum recurrence_type: {
    "One-time": 1, #RecurrenceTypePID
    "Recurring": 2, #RecurrenceTypePID
  }

  enum assignment_level: {
    "Customer": 1,
    "Department": 2,
    "Service": 3
  }

  # TODO: remove this
  belongs_to :invoice

  # associates with client
  belongs_to :client
  alias_attribute :billable_service_pid, :charge_pid
  # hold global services description
  belongs_to :product, optional: true
  belongs_to :product_category

  belongs_to :service

  # XXX: could be associated with a site...
  belongs_to :site
  belongs_to :supplier,         :class_name => 'DIDSupplier'
  # a service description is mandatory
  validates :product,
    presence: true

  # it is associated with a service (aka phone - phone-changed to->service t13331)
  belongs_to :asset, class_name: 'Service'
  # belongs_to :service, class_name: 'Service'

  belongs_to :charge_mode

  belongs_to :prorate_start, :class_name => "Prorate", :foreign_key => "prorate_id_start"
  belongs_to :prorate_end, :class_name => "Prorate", :foreign_key => "prorate_id_end"

  def associations_attributes
    # BillableService.reflections.symbolize_keys.keys


  end

  def to_onetime_export_hash
    {
      "Reference Number"         => self.reference_number,
      "Department #"             => (self.site.site_number if self.site),
      "Department Name"          => (self.site.name if self.site),
      "Service ID"               => self.service,
      "Line of Business"         => (self.product.line_of_business.description if (self.product and self.product.line_of_business)),
      "Quantity"                 => self.qty,
      "Start Date"               => (self.billing_period_started_at.strftime "%Y-%m-%d" if self.billing_period_started_at),
      "Supplier"                 => (self.supplier.name if self.supplier)
    }

  end


  def to_s
    "{#{self.id.to_s}}#{self.name}"
  end

  def to_export_hash
    # return a hash of values we want/need for the export.
    # the billable_services/exports view uses these values to build
    # resulting table

    clientName =    (Rails.env.development? ? self.client.to_s  : self.client.name) if self.client
    siteName =      (Rails.env.development? ? self.site.to_s    : self.site.name) if self.site
    productName =   (Rails.env.development? ? self.to_s         : self.name)

    {
      "Active"                   => (self.activated? ? 'Active' : 'Inactive'),
      "Product Code"             => self.product_id,
      "Product Name"             => productName,
      "Description"              => self.description,
      "Reference Number"         => self.reference_number,
      "Customer Account #"       => (self.client.btn.billingtelephonenumber if (self.client and self.client.btn)),
      "Customer Name"            => clientName,
      "Department #"             => (self.site.site_number if self.site),
      "Department Name"          => siteName,
      "Service ID"               => self.service,
      "Line of Business"         => (self.product.line_of_business.description if (self.product and self.product.line_of_business)),
      "Quantity"                 => self.qty,
      "Amount ($)"               => self.unit_price,
      "Unit Cost ($)"            => (self.product.amount if self.product),
      "Start Date"               => (self.billing_period_started_at.strftime "%Y-%m-%d" if self.billing_period_started_at),
      "Terminate On"             => (self.billing_period_ended_at.strftime "%Y-%m-%d" if self.billing_period_ended_at),
      "Recurrence Type"          => self.recurrence_type,
      "Supplier"                 => (self.supplier.name if self.supplier),
      "Supplier Activation Date" => (self.supplier.created_at.strftime "%Y-%m-%d" if self.supplier),
    }

  end


  def is_terminated?
    if billing_period_ended_at
      return billing_period_ended_at < DateTime.now
    else
      return false
    end
  end

  #BillableService.where('billing_period_ended_at > ?', DateTime.now)
  #BillableService.where('billing_period_ended_at > ? and recurrence_type = ?', DateTime.now, 2)

  def startdate
    billing_period_started_at || invoice.try(:billing_period_started_at)
  end

  def startdate_s
    @startdate_s ||= startdate.strftime("%Y-%m-%d")
  rescue NoMethodError
    nil
  end

  def enddate
    billing_period_ended_at || invoice.try(:billing_period_ended_at)
  end

  def enddate_s
    @enddate_s ||= enddate.strftime("%Y-%m-%d")
  rescue NoMethodError
    nil
  end

  def name
    # Not sure where this product variable is populated.
    # It was named service before, and renamed product with the model rename (t13330)
    product.try(:name)
  end

  def description
    # Not sure where this product variable is populated.
    # It was named service before, and renamed product with the model rename (t13330)
    product.try(:description)
  end

  def date_range
    sprintf("%s - %s", startdate_s, enddate_s)
  end

  def unit_price
    # Not sure where this product variable is populated.
    # It was named service before, and renamed product with the model rename (t13330)
    unitprice || product.try(:unitprice)
  end

  def discount
    self[:discount] || 1
  end

  def total
    # - commented out because updateing qty doesn't appear to actually update this.
    #   where is this updated?? -jjrh
    # - I don't know yet but it's used in the view _monthlycharges.html.erb (line 39 at time of writting)
    #       - jlam 2018/07/04

    # return totalprice if totalprice

    return 0 if qty.blank?

    totalprice = (qty || 0) * (unit_price || 0) * (discount || 1)
    totalprice
  end


  ########################################################################
  # Find the billeable services for the lookahead month
  # AND one time charges of the previous month.
  # @param [ActiveRecord_Relation] :query The query to build upon
  # @param [Date] The date  of the look ahead month, that is the month
  #    used to select billed charges / billable services.
  #     The usage month (for calls and one time charges) will be calculated
  #     as the previous month previous to this one.
  #     To represent the month, it can be any date of that month
  #
  ########################################################################
  def self.invoiceable_for(query, lookAheadMonth)

    # First line if for one-time, second line for recurring charges
    # For second line: is the charge beginning before lookahead month or ending during the lookahead month
    whereClauseSQL = %Q[(recurrence_type = ? AND billing_period_started_at BETWEEN ? AND ?) OR
        (recurrence_type = ? AND (billing_period_started_at <= ? and (billing_period_ended_at is null or billing_period_ended_at >= ?)) OR (billing_period_ended_at BETWEEN ? AND ?)) ]


    usageMonth = lookAheadMonth.last_month # Usage mont his the month used for calls
    lookahead_month_end = lookAheadMonth.end_of_month

    # Set the args for the query above.
    # Must be in the order the params appear, as the query will be fed only the values of this Hash as an array
    args = {
        recurrence_type_one_time:   BillableService.recurrence_types["One-time"],
        usage_month_start:          usageMonth.beginning_of_month,
        usage_month_end:            usageMonth.end_of_month,
        recurrence_type_recurring:  BillableService.recurrence_types["Recurring"],
        billing_start_lookahead_month_end:        lookahead_month_end,
        billing_end_lookahead_month_start_1:        lookAheadMonth.beginning_of_month,
        billing_end_lookahead_month_start_2:      lookAheadMonth.beginning_of_month,
        billing_end_lookahead_month_end:        lookahead_month_end,
    }

    #If the lookahead month end is in january, no criteria
    #Else add where clause on billing_periods monthly (line 431 form invoicing.rb

    #

    return query.where(whereClauseSQL, *(args.values))

    #TODO: add 8 search terms https://richonrails.com/articles/multiple-where-conditions-using-active-record


    #returnQuery = query.where(recurrence_type: BillableService.recurrence_types["One-time"]).
    #    where(billing_period_started_at: (Date.today.last_month.beginning_of_month..Date.today.last_month.end_of_month))

    #returnQuery = returnQuery.or(query.where(recurrence_type: BillableService.recurrence_types["Recurring"]).
    #    where(billing_period_started_at: (Date.today.last_month.beginning_of_month..Date.today.last_month.end_of_month))

  end

  # def update_from_hakka
  #   ca1 = self.hakka_chargeassignment
  #   self.update(ca1.make_billable_service_hash)
  #   save!
  # end

  # populate the start/finish dates when first used.
  # def service_dates_force!
  #   self[:billing_period_started_at]    = invoice.billing_period_started_at
  #   self[:billing_period_ended_at]   = invoice.billing_period_ended_at
  #   save!
  # end

  # def renew(dflt_invoice)
  #
  #   return renewal if renewal
  #
  #   freq = frequency_months || 1
  #   newdate = self.billing_period_started_at.beginning_of_month + freq.months
  #
  #   i1 = dflt_invoice.btn.invoice_for_date(newdate)
  #
  #   bs1 = dup
  #   bs1.invoice = i1
  #   bs1[:billing_period_started_at] = i1.billing_period_started_at
  #   bs1[:billing_period_ended_at]   = i1.billing_period_ended_at
  #   bs1.renewal  = nil
  #   bs1.save!
  #
  #   self.foreign_pid    = nil
  #   self.renewal = bs1
  #   save!
  #
  #   bs1
  # end

  # def terminate
  #   update billing_period_ended_at: DateTime.now
  # end

  def phone_number
    asset.service_id
  rescue NoMethodError
    nil
  end

  class BillableServiceError < StandardError; end
  class HasNoServiceError < BillableServiceError; end
  class HasNoInvoiceError < BillableServiceError; end

  private
  def show_diff
    if File.basename($0) == 'rake'
      arr = self.changed
      arr.delete("updated_at")
      changes = self.changes
      changes.delete(:updated_at)
      l = Logger.new("#{Rails.root}/log/billable_services_diff.log")
      l.datetime_format = "%Y-%m-%d %H:%M:%S"
      if arr.length > 0
        l.debug("id:#{self.id} fields:#{arr},#{changes}")
      end
    end
  end
end
