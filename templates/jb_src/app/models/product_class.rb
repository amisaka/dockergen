class ProductClass < ApplicationRecord
    IMPORT_MAPPING = {
        :code           => "Code",
        :description    => "Description",
        :active         => "Active", # Carefull, "0" converts to true
        :product_class_pid => "ProductClassPID"
    }

    IMPORT_PRIMARY_KEY = :product_class_pid

    def product_class_label
      if self.description != " "
        return "#{self.code} : #{self.description}"
      else
        return "#{self.code}"
      end
    end

end
