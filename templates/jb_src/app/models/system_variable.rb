# well known System Variables
require "digest"

class SystemVariable  < ApplicationRecord
  @@cache = Hash.new

  def self.lookup(thing)
    where(:variable => thing.to_s).take
  end

  def self.number(thing)
    t = lookup(thing)
    return t.number if t
    return nil
  end

  def self.flush
    @@cache = Hash.new
  end

  def self.lookupstring(thing)
    v = self.lookup(thing)
    return v.value if v
    nil
  end

  def self.findormake(thing)
    v = self.lookup(thing)
    if v.nil?
      v = self.new
      v.variable = thing.to_s
    end
    v
  end

  def self.set_string(thing, string)
    v = self.findormake(thing)
    v.value = string
    v.save!
  end

  def self.filechanged(namesymbol, filename, sumsymbol, digest)
    last_lerg6file = SystemVariable.lookupstring(namesymbol)
    if last_lerg6file
      puts "Found previous load: #{last_lerg6file} vs new: #{filename}"

      if File.basename(filename) == File.basename(last_lerg6file)
        # file name matches, see if we have a sha2sum to double check.
        last_lerg6sum = SystemVariable.lookupstring(sumsymbol)
        puts "  with sha2sum: #{last_lerg6sum} vs new: #{digest}"

        if last_lerg6sum
          if last_lerg6sum == digest
            puts "    no file change"
            return false
          end
        end
      end
    end
    return true
  end

  def self.boolvalue?(thing)
    v = self.lookup(thing)
    return false if v.nil?
    return (v.number != 0)
  end

  def self.boolcache?(thing)
    @@cache[thing] ||= boolvalue?(thing)
  end

  def self.setnumber(thing, value)
    v = self.findormake(thing)
    v.number = value
    v.save
  end

  def self.nextval(thing)
    v = self.findormake(thing)
    if v.number.nil?
      v.number = 1
    end
    v.nextval
  end

  def self.get_uid
    return self.nextval(:unix_id)
  end

  def after_save
    @@cache.delete(self.variable)
  end

  def nextval
    n = nil
    begin
      transaction do
        n = self.number
        m = n + 1
        self.number = m
        self.save
      end
    #rescue ActiveRecord::Rollback
    #  logger.err "failed to get nextval for #{variable}"
    end
    n
  end

  def number=(x)
    self[:number] = x
    save!
  end

  def elidedvalue
    if value.blank?
      ""
    elsif value.length > 15
      value[0..7] + ".." + value[-7..-1]
    else
      value
    end
  end

end
