module StatsPeriodsHelper
  def stats_period_day_column(record, column)
    link_to(record.day, client_stats_period_callstats_path(@client, record))
  end

  def stats_period_statcount_column(record, column)
    if @client
      @client.callstats.where(:stats_period_id => record.id).count
    else
      ""
    end
  end
end
