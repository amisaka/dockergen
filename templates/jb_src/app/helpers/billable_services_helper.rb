module BillableServicesHelper
  # also used for phones.
  # I don't understand what this is for.  It just prints the site id in service listing --jlam 2018/05/16
  def service_site_selector(bs, choices)
    best_in_place bs, :site_id, place_holder: '-no-site-', as: :select, collection: choices
  end

  def change_current_url_type(page_format="html")
    ##########################################################################################
    # Change a url to have a specific extension while keeping the query params.
    #
    # This is to facilitate a work around where I (jjrh) was unable to request a specific page format
    # in the form.
    #
    # ex:
    #     <%= link_to "This page in CSV", change_current_url_type("json") %>
    #
    # from : billable_services/exports?..query..params..
    #
    # to   : billable_services/exports.json?..query..params..
    #
    # from : http://union:3000/clients/1357/billable_services/exports?utf8=%E2%9C%93&billable_service_export_search%...............
    # to   : http://union:3000/clients/1357/billable_services/exports.json?utf8=%E2%9C%93&billable_service_export_search%...............
    ##########################################################################################
    begin
      request.protocol+request.host_with_port+request.fullpath.split("?")[0]+".#{page_format}?"+request.fullpath.split("?")[1]
    rescue
      request.protocol+request.host_with_port+request.fullpath.split("?")[0]+".#{page_format}"
    end
  end


end
