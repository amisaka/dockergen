module ServicesHelper
  def value_to_checked(val)
    return 'checked' if val
    ''
  end

  def phones_title
    'Phone' +
      if @selected_site
        " from site: "+ @selected_site.name
      else
        " all sites"
      end  +
      sprintf(" (deleted: %03u)", @phone_del_count)
  end
end
