module CallstatsHelper
  def callstats_client_column(record, column)
    if record.client
      link_to(record.client.try(:name), client_path(record.client))
    else
      ""
    end
  end

end
