module CdrsHelper
  def cdr_recordid_column(record, column)
    h(".."+record.shortname+"..")
  end

  def cdr_calledclient_column(record, column)
    client = record.calledclient
    if client
      ("<span class=\"cdr_client\">" +
        link_to(h(client.name),client_path(client)) +
        "</span>").html_safe
    else
      ""
    end
  end

  def cdr_callingclient_column(record, column)
    client = record.callingclient
    if client
      ("<span class=\"cdr_client\">" +
        link_to(h(client.name),client_path(client)) #+
        "</span>").html_safe
    else
      ""
    end
  end

  def cdr_calledphone_column(record, column)
    if record.calledphone
      client = record.client
      record.calledphone.to_label
    else
      record.callednumber
    end
  end

  def cdr_callingphone_column(record, column)
    if record.callingphone
      record.callingphone.to_label
    else
      record.callingnumber
    end
  end

  def cdr_releasedatetime_column(record, column)
    unless record.releasedatetime.blank?
      record.releasedatetime.to_s(:db)
    else
      ""
    end
  end
end
