module BillingsHelper
  def payment_method_for(client = nil)
    return '' if client.nil?
    payment_method_name_for(client.payment_method)
  rescue NoMethodError
    ''
  end

  def payment_method_name_for(val = '')
    return '' if val.blank?
    case val
    when 'none_unknown' || :none_unknown
      'None'
    else
      titleize(val)
    end
  end

  def payment_methods_options_for(client = nil)
    return '' if client.nil?
    map_payment_methods_options_for(client)
  rescue NoMethodError
    []
  end

  def map_payment_methods_options_for(client)
    h = client.class.payment_methods
    h.keys.map{|k| [payment_method_name_for(k),k]}
  end

  def local_rate_plan_for(billing = nil)
    return '' if billing.nil?
    billing.local_rate_plan.name
  rescue NoMethodError
    ''
  end

  def national_rate_plan_for(billing = nil)
    return '' if billing.nil?
    billing.national_rate_plan.name
  rescue NoMethodError
    ''
  end

  def world_rate_plan_for(billing = nil)
    return '' if billing.nil?
    billing.world_rate_plan.name
  rescue NoMethodError
    ''
  end
end
