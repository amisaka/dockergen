module InvoicesHelper
  def monthly_charge_debug(monthlycharge)
    if $INVOICEDEBUG
      sprintf("(%u / %u)", monthlycharge.id,monthlycharge.service.try(:id))
    else
      ""
    end
  end

  def self.sanitize_filename(filename)
    filename.strip do |name|
     # NOTE: File.basename doesn't work right with Windows paths on Unix
     # get only the filename, not the whole path
     name.gsub!(/^.*(\\|\/)/, '')

     # Strip out the non-ascii character
     name.gsub!(/[^0-9A-Za-z.\-]/, '_')
    end
  end

  def seasonal_message(invoice)
    unless @interim_invoice.blank?
      today = @interim_invoice
      if @interim_invoice == true
        today = Time.now.to_s(:db)
      end
      ("<div style=\"font-size: 200%;\">\nInterim invoice for "+today+".\n</div>").html_safe
    else
      invoice.seasonal_message.html_safe
    end
  end
end
