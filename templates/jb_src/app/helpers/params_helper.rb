module ParamsHelper

  [:site, :client, :phone, :btn, :invoice].each do |m|
    eval <<-RUBY
      def has_#{m}_id?
        params.has_key?(:#{m}_id) && !params[:#{m}_id].blank? || \
          (params[:controller] == '#{m}s' && params.has_key?(:id) && !params[:id].blank?)
      end
    RUBY
  end

end
