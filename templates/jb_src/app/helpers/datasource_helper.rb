module DatasourceHelper
  def datasource_calls_column(record, column)
    link_to("calls (#{record.call_count})", datasource_calls_path(record))
  end

  def datasource_cdrs_column(record, column)
    if record.cdrs
      link_to("cdrs (#{record.cdr_count})", datasource_cdrs_path(record))
    else
      ""
    end
  end
end
