module ClientsHelper
  extend ApplicationHelper

  def myfoo
    debugger
    puts "Hello"
  end

  def client_name_column(record, column)
    link_to(h(record.name), client_path(record))
  end

  def status_for(client = nil)
    return '' if client.nil?
    titleize(client.status)
  rescue NoMethodError
    ''
  end

  def map_status_options_for(client)
    ary = [:active, :closed, :pending_start, :pending_closed ]
    ary.map{|k| [titleize(k),k]}
  end

  def showing_terminated_sites?
    return true if params[:terminated] == 'true'
    false
  end

end
