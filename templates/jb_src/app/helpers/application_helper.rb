module ApplicationHelper

  def default_to(resource, method, default)
    val = resource.send(method)
    return val unless val.blank?
    default
  rescue NoMethodError
    default
  end

  # This adds bold html tags to the matched query string in given text string
  def bold_match(string, query)
    return string if string.blank? || query.blank?
    queries = query.split(' ')
    queries.map! {|q| Regexp.quote(q)}
    string.gsub(/#{queries.join('|')}/i){"<b>#{$&}</b>"}.html_safe
  end

  def titleize(s)
    if s.kind_of? String
      s.titleize
    else
      s.to_s.titleize
    end
  rescue NoMethodError
    ''
  end

  def pluralize(s)
    if s.kind_of? String
      s.pluralize
    else
      s.to_s.pluralize
    end
  rescue NoMethodError
    ''
  end

  def date(d)
    d.strftime('%Y-%m-%d')
  rescue NoMethodError
    ''
  end

  def br_if_not_blank(msg)
    if msg.blank?
      ""
    else
      (msg + "<br />").html_safe
    end
  end

  def money(number = 0.0, precision: 2)
    # Use ActiveSupport currency helper to properly handle localized formats
    number ||= 0
    number = (number * 10**precision).ceil.to_f / 10**precision
    number_to_currency(number, precision: precision)
  end

  def errors_for(resource, field)
    if resource.errors.any?
      unless resource.errors[field].blank?
        output = '<ul class="errors">'
        resource.errors[field].each do |message|
          output << "<li class=\"error\"><span class=\"text-danger error-message\">#{message.capitalize}.</span></li>\n"
        end
        output << '</ul>'
        output.html_safe
      end
    end
  end

  def user_admin?
    current_user.try(:admin?)
  end

  def icon_for(resource)
    case resource.to_s
    when /new/, /create/
      'plus'
    when /edit/
      'wrench'
    when /show/
      'tag'
    when /destroy/, /delete/
      'trash'
    when /crm_account/
      'folder'
    when /client/
      'university'
    when /btn/
      'phone-square'
    when /phone/
      'phone'
    when /building/
      'building'
    when /site/
      'building-o'
    end
  end

  def name_for(resource)
    case resource.to_s
    when /new/
      'Create'
    when /show/
      'Display'
    when /edit/
      'Modify'
    when /crm_accounts/
      'Accounts'
    when /crm_account/
      'Account'
    when /btns/
      'BTNs'
    when /btn/
      'BTN'
    else
      resource.humanize
    end
  end

  def safe_url_for(*args)
    url_for(*args)
  rescue
    root_path
  end

  def has_invoice_cycle?(client)
    return false if client.nil?
    client = ClientDecorator.new(client) if client.kind_of?(Client)
    return false if client.invoice_cycle.blank? || client.invoice_cycle =~ /support/i
    true
  rescue NoMethodError
    false
  end

end
