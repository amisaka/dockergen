class ServiceSearch < ApplicationSearch #< Searchlight::Search

  def base_query()
    begin
      Client.find(options[:client]).services.order(:start_date)
    rescue
      return nil
    end
  end

  def search_activated
    if options[:activated] == "1" and options[:deactivated] == "1"
      query
    else
      if options[:activated] == "1"
        query.where(activated: options[:activated])
      else
        query
      end
    end
  end

  def search_deactivated
    if options[:activated] == "1" and options[:deactivated] == "1"
      query
    else
      if options[:deactivated] == "1"
        query.where(activated: false)
      else
        query
      end

    end

  end

  def search_rateplan
    query.joins(:rate_plan).where("rate_plans.code ILIKE ?", "%#{options[:rateplan]}%")
  end

  def search_didsupplier
    query.joins(:supplier).where("did_suppliers.name ILIKE ?", "%#{options[:didsupplier]}%")
  end

  def search_lineofbusiness
    query.joins(:line_of_business).where("line_of_businesses.id = ?", "#{options[:lineofbusiness]}")
  end

  def search_startdate
    query.where(:start_date => "#{options[:startdate]}")
  end

  def search_enddate
    query.where(:termination_date => "#{options[:enddate]}")
  end

  def search_site
    query.where(:site_id => "#{options[:site]}")
  end

  def search_servicetype
    query.where(:phone_service_type_id => "#{options[:servicetype]}")
  end

  def search_description
    query.where("description ILIKE ?", "%#{options[:description]}%")
  end

  def search_serviceid
    query.where("service_id ILIKE ?", "%#{options[:serviceid]}%")
  end


end
