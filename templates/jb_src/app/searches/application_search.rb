require "searchlight/adapters/action_view"

class ApplicationSearch < Searchlight::Search
  include Searchlight::Adapters::ActionView
end
