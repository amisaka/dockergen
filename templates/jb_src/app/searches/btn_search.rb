class BtnSearch < ApplicationSearch

  def base_query
    Btn.all
  end

  def search_billingtelephonenumber
    query.where("btns.billingtelephonenumber ILIKE ?", "%#{billingtelephonenumber}%")
      .order('btns.billingtelephonenumber ASC')
  end

  def search_status
    case status
    when 'activated', :activated
      query.active
    when 'active', :active, 'closed', :closed, 'pending_start', :pending_start, 'pending_close', :pending_close, 'unknown', :unknown
      query.select do |b|
        begin
          b.client.send("#{status}?".to_sym)
        rescue NoMethodError
          false
        end
      end
    when :all, 'all'
      query.all
    else
      query
    end
  end
end
