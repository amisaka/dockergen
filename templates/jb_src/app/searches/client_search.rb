class ClientSearch < ApplicationSearch
  attr_accessor :field

  def base_query
    Client.joins(:crm_account).order('crm_accounts.name ASC')
  end

  # Query by name
  def search_name
    query.joins(:crm_account).where("crm_accounts.name ILIKE ?", "%#{name}%")
  end

  def search_support
    if support == "false"
      query.where.not(:coverage_id => 9)
    else
      query
    end
  end
  # Query clients by BTN
  def search_billingtelephonenumber
    query.select(<<-SQL)
        clients.*, crm_accounts.name, btns.billingtelephonenumber
      SQL
      .joins(:btn).where(<<-SQL, "%#{billingtelephonenumber}%", Time.now)
        btns.billingtelephonenumber ILIKE ? AND
          btns.deactivated_at IS NULL OR btns.deactivated_at >= ?
      SQL
      .group('clients.id, btns.billingtelephonenumber, crm_accounts.name')
      .order('btns.billingtelephonenumber ASC')
  end

  def search_phone_number
    service_id = phone_number

    # query.select(<<-SQL).joins(:phones)
    query.select(<<-SQL).joins(:services)
          clients.*, crm_accounts.name
      SQL
      .where(<<-SQL, "%#{service_id}%", Time.now, Time.now)
        services.service_id ILIKE ? AND
          (services.start_date IS NULL OR services.start_date <= ?) AND
          (services.termination_date >= ? OR services.termination_date IS NULL)
      SQL
      .group('clients.id, crm_accounts.name')
      .having('COUNT(services.service_id ILIKE ?) > 0', "%#{service_id}%")


  end

  #
  # def search_invoice
  #   case invoice
  #   when :invoice, 'invoice'
  #     query.joins(:invoice_templates)
  #       .where('invoice_templates.invoice_cycle != ?', 'support')
  #       .group('clients.id, crm_accounts.name')
  #   when :support, 'support'
  #     query.joins(:invoice_templates)
  #       .where('invoice_templates.invoice_cycle = ?', 'support')
  #       .group('clients.id, crm_accounts.name')
  #   when :all, 'all'
  #     query
  #   else
  #     query
  #   end
  # end

  # Temporary measure
  def search_invoice
    case invoice
    when :invoice, 'invoice'
      query.joins(:btn)
        .where('btns.invoicing_enabled = ?', true)
        .group('clients.id, crm_accounts.name')
    when :support, 'support'
      query.joins(:btn)
        .where('btns.invoicing_enabled = ? OR btns.invoicing_enabled IS NULL', false)
        .group('clients.id, crm_accounts.name')
    when :all, 'all'
      query
    else
      query
    end
  end

  #  Search by status
  def search_status
    if status == 'either'
      query
    else
      query.where(status: status)
    end
  end
end
