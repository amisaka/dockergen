class BillableServiceExportSearch < ApplicationSearch
  def options
    super.tap { |opts|
      opts["limitrecords"] ||= "10"
    }
  end

  def base_query()
    BillableService.all #.limit(limitrecords)
  end

  def search_recurrence
    query.where(recurrence_type: options[:recurrence])
  end

  def search_assetid
    # query.joins(:asset).where("service_id = ?", "#{options[:assetid]}")
    query.joins(:asset).where("service_id ILIKE ?", "%#{options[:assetid]}%")
  end

  def search_recurrences
    query.where(recurrence_type: options[:recurrences])
  end

  def search_activated
    if options[:activated] == "1" and options[:deactivated] == "1"
      query
    else
      if options[:activated] == "1"
        query.where(activated: options[:activated])
      else
        query
      end
    end
  end

  def search_deactivated
    if options[:activated] == "1" and options[:deactivated] == "1"
      query
    else
      if options[:deactivated] == "1"
        query.where(activated: false)
      else
        query
      end

    end

  end

  def search_limitrecords
    query
  end

  def search_quanity
    query.where("qty = ?", "#{options[:quanity]}")
  end

  def search_servicedescription
    query.joins(:product).where("products.description ILIKE ?", "%#{options[:servicedescription]}%")
  end

  def search_servicename
    query.joins(:product).where("products.name ILIKE ?", "%#{options[:servicename]}%")
  end

  def search_servicecode
    query.joins(:product).where("products.code ILIKE ?", "%#{options[:servicecode]}%")
  end

  def search_producttext
    query.joins(:product).where("products.code ILIKE ? or products.description ILIKE ?", "%#{options[:producttext]}%","%#{options[:producttext]}%")
  end

  def search_product
    query.joins(:product).where("products.id = ?", "#{options[:product]}")
  end

  def search_onetime_startdate
    query
  end

  def search_onetime_enddate
    query
  end

  def search_onetime
    # query.where("recurrence_type = 1 and billing_period_started_at >= '2018-06-01' and billing_period_started_at <= '2018-06-30'")
    query.where("recurrence_type = 1 and billing_period_started_at >= ? and billing_period_started_at <= ?",
                options[:onetime_startdate],
                options[:onetime_enddate])

  end


  def search_startdate
    @testvar = true;
    if options[:enddate].nil?
      # query.where('billing_period_started_at BETWEEN ? AND ?', options[:startdate], DateTime.now)
      query.where('(billing_period_started_at <= ?
                        AND billing_period_ended_at <= ?
                        AND activated = TRUE)
                    OR
                    (billing_period_started_at <= ?
                        AND billing_period_ended_at is null
                        AND activated = TRUE)
                    OR
                    (billing_period_started_at = ?
                        AND activated = FALSE
                        AND billing_period_ended_at is NULL)',
                  options[:startdate],
                  DateTime.now,
                  options[:startdate],
                  options[:startdate])
    else
      query.where('(billing_period_started_at <= ?
                        AND billing_period_ended_at <= ?
                        AND activated = TRUE)
                    OR
                    (billing_period_started_at <= ?
                        AND billing_period_ended_at IS NULL
                        AND activated = TRUE)
                    OR
                    (billing_period_started_at >= ?
                        AND billing_period_started_at <= ?
                        AND activated = FALSE
                        AND billing_period_ended_at is NULL)',
                  options[:startdate],
                  options[:enddate],
                  options[:startdate],
                  options[:startdate],
                  options[:enddate])


      # query.where('billing_period_started_at BETWEEN ? AND ?', options[:startdate], options[:enddate])
    end

  end

  def search_enddate
    if options[:startdate].nil?
      query.where('billing_period_started_at <= ?', options[:enddate])
    else
      query
    end

    # query.where(:billing_period_ended_at => "#{options[:enddate]}")
  end

  def search_site
    query.where(:site_id => "#{options[:site]}")
  end

  def search_supplier
    query.joins(:supplier).where("did_suppliers.name ILIKE ?", "%#{options[:supplier]}%")
  end

  def search_supplierid
    query.joins(:supplier).where("did_suppliers.id = ?", "#{options[:supplierid]}")

  end

  def search_referencenumber
    query.where("billable_services.reference_number ILIKE ?", "%#{options[:referencenumber]}%")
  end
end
