class BillableServiceSearch < ApplicationSearch #< Searchlight::Search

def base_query()

    BillableService.all.order(:billing_period_started_at).where(:client_id => options[:client]) #.where('billing_period_ended_at > ? or billing_period_ended_at is NULL', DateTime.now)
  end

  def search_recurrence
    query.where(recurrence_type: options[:recurrence])
  end

  def search_assetid
    # query.joins(:asset).where("service_id = ?", "#{options[:assetid]}")
    query.joins(:asset).where("service_id ILIKE ?", "%#{options[:assetid]}%")
  end

  def search_recurrences
    query.where(recurrence_type: options[:recurrences])
  end

  def search_activated
    if options[:activated] == "1" and options[:deactivated] == "1"
      query
    else
      if options[:activated] == "1"
        query.where(activated: options[:activated])
      else
        query
      end
    end
  end

  def search_deactivated
    if options[:activated] == "1" and options[:deactivated] == "1"
      query
    else
      if options[:deactivated] == "1"
        query.where(activated: false)
      else
        query
      end

    end

  end

  def search_quanity
    query.where("qty = ?", "#{options[:quanity]}")
  end

  def search_servicedescription
    query.joins(:product).where("products.description ILIKE ?", "%#{options[:servicedescription]}%")
  end

  def search_servicename
    query.joins(:product).where("products.name ILIKE ?", "%#{options[:servicename]}%")
  end

  def search_servicecode
    query.joins(:product).where("products.code ILIKE ?", "%#{options[:servicecode]}%")
  end

  def search_startdate
    query.where(:billing_period_started_at => "#{options[:startdate]}")
  end

  def search_enddate
    query.where(:billing_period_ended_at => "#{options[:enddate]}")
  end

  def search_site
    query.where(:site_id => "#{options[:site]}")
  end

end
