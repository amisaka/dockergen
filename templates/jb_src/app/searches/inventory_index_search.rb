class InventoryIndexSearch < ApplicationSearch
  def options
    super.tap { |opts|
      opts["limitrecords"] ||= "10"
    }
  end

  def base_query()
    #Inventory.all #.limit(limitrecords)
    Inventory.limit(limitrecords)
  end

  def search_limitrecords
    query
  end

  def search_supplier
    query.joins(:did_supplier).where("did_suppliers.name ILIKE ?", "%#{options[:supplier]}%")
  end

  def search_supplierid
    query.joins(:did_supplier).where("did_suppliers.id = ?", "#{options[:supplierid]}")

  end
end

