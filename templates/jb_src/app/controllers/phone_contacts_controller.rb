# class PhoneContactsController < ApplicationController
class PhoneContactsController < AuthorizationController
  # include Montbeau::ActivatableControllerHelpers
  layout 'admin_lte'

  before_action :set_client, if: :has_client_id_param?
  before_action :set_phone_contact, only: [:index, :edit]

  respond_to :html

  load_and_authorize_resource



  # GET /phone_contacts
  # GET /phone_contacts.json
  def index
    @phone_contacts = PhoneContact.all
  end

  # GET /phone_contacts/1
  # GET /phone_contacts/1.json
  def show
  end

  # GET /phone_contacts/new
  def new
    @phone_contact = PhoneContact.new
  end

  # GET /phone_contacts/1/edit
  def edit
  end

  # POST /phone_contacts
  # POST /phone_contacts.json
  def create
    @phone_contact = PhoneContact.new(phone_contact_params)
    if @phone_contact.save
      redirect_to :controller => 'contacts', :action => 'index'
    else
      # something
    end
  end

  # PATCH/PUT /phone_contacts/1
  # PATCH/PUT /phone_contacts/1.json
  def update
    if @phone_contact.update(phone_contact_params)
      redirect_to :controller => 'contacts', :action => 'index'
    end

    # respond_to do |format|
    #   if @phone_contact.update(phone_contact_params)
    #     format.html { redirect_to @phone_contact, notice: 'Phone contact was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @phone_contact }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @phone_contact.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /phone_contacts/1
  # DELETE /phone_contacts/1.json
  def destroy
    @phone_contact.destroy
    redirect_to :controller => 'contacts', :action => 'index'

    # respond_to do |format|
    #   format.html { redirect_to phone_contacts_url, notice: 'Phone contact was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private

    def set_client
      @client = Client.find(params[:client_id])
    end

    def has_client_id_param?
      params[:client_id].present?
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_phone_contact
      @phone_contact = PhoneContact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def phone_contact_params
      params.require(:phone_contact).permit(:name, :phone_number, :phone_type, :client_id)
      #params.fetch(:phone_contact, {})
    end
end
