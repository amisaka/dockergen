class ContactsController < AuthorizationController

  layout 'admin_lte'

  before_action :set_contacts, only: [:index]

  respond_to :html

  def index
    if @client
      respond_with(@client, @phone_contacts, @email_contacts, @technical_contacts)
    else
      respond_with(@phone_contacts, @email_contacts, @technical_contacts)
    end
  end

  private

    def set_contacts
      if @client
        @phone_contacts = @client.phone_contacts
        @email_contacts = @client.email_contacts
        @technical_contacts = @client.technical_contacts
      else
        @phone_contacts = PhoneContact.all
        @email_contacts = EmailContact.all
        @technical_contacts = TechnicalContact.all
      end
    end

end
