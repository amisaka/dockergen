class BillableServicesController < AuthorizationController
  include Search::ControllerHelpers
  include Montbeau::ActivatableControllerHelpers

  layout 'admin_lte'

  before_action :set_billable_service, only: [:show, :edit, :update, :destroy, :remove]

  before_action :set_client, if: :has_client_id_param?

  before_action :set_btn, if: :has_btn_id_param?

  before_action :set_invoice, if: :has_invoice_id_param?

  respond_to :html

  def index
    # @billable_services = BillableService.all.page params[:page]
    @search = BillableServiceSearch.new(search_params)

    # @billable_services = @search.results.page(params[:page])
    @billable_services = @search.results.all #page(params[:page]) #Kaminari.paginate_array(@search.results).page(params[:page])
    @results_length = @billable_services.length
    @billable_services = @search.results.all.page params[:page]
    request.parameters.merge({:page => params[:page]})

    respond_with(@billable_services)
  end

  def to_csv
    CSV.generate({}) do |csv|
      csv << @billable_services_all[0].to_export_hash.keys
      @billable_services_all.each do |bs|
        csv << bs.to_export_hash.values
      end
    end
  end

  def to_onetime_csv
    CSV.generate({}) do |csv|
      csv << @billable_services_all[0].to_onetime_export_hash.keys
      @billable_services_all.each do |bs|
        csv << bs.to_onetime_export_hash.values
      end
    end
  end


  def one_time_exports

    @search = BillableServiceExportSearch.new(onetime_search_params)
    @billable_services_all = @search.results.all
    @results_length = @billable_services_all.length
    @billable_services = @search.results.all.page params[:page]

    request.parameters.merge({:page => params[:page]})

    respond_to do |format|
      format.html
      format.json {         send_data self.to_csv, filename: "Onetime_Charges_#{onetime_search_params[:onetime_startdate]}_#{onetime_search_params[:onetime_enddate]}.csv" } # for some reason csv doesn't work....
    end

  end

  def exports
    @search = BillableServiceExportSearch.new(search_params)
    @billable_services_all = @search.results.all
    @results_length = @billable_services_all.length
    @billable_services = @search.results.all.page params[:page]

    request.parameters.merge({:page => params[:page]})

    puts @search.results.to_sql

    respond_to do |format|
      format.html
      format.json {         send_data self.to_csv, filename: "Charges_#{DateTime.now.strftime "%Y-%m-%d_%H%M"}.csv" } # for some reason csv doesn't work....
    end
  end

  def show
    respond_with(@billable_service)
  end

  def new
    @billable_service = BillableService.new
    respond_with(@billable_service)
  end

  def edit
  end

  def create
    @billable_service = BillableService.new(billable_service_params)
    if(@billable_service.recurrence_type == "One-time")
      @billable_service.deactivate!
    end
    @billable_service.save

    respond_with(@billable_service) and return unless request.referer
    if URI(request.referer).path == new_billable_service_path
      respond_with(@billable_service)
    else
      respond_with @billable_service, location: request.referer
    end
  end

  def update
    @billable_service.update(billable_service_params.except(:human_validation_date))
    respond_to do |format|
      format.html {
        if billable_service_params[:human_validation_date] =~ /\A1\z/
          @billable_service.human_validation_date = Time.now
          @billable_service.save!
        elsif billable_service_params[:human_validation_date] =~ /\A0\z/
          @billable_service.human_validation_date = nil
          @billable_service.save!
        end

        respond_with(@billable_service) and return unless request.referer
        if URI(request.referer).path == edit_billable_service_path(@billable_service)
          respond_with(@billable_service)
        else
          respond_with @billable_service, location: request.referer
        end
      }
      format.json {
        respond_with_bip(@billable_service) }
    end
  end

  def destroy
    @billable_service.destroy
    respond_with(@billable_service)
  end

  # Adds this BillableService to an invoice
  def add
    @billable_service = BillableService.new(billable_service_params)
    @client.add_billable_service(@btn, @invoice, @billable_service)

    respond_with(@billable_service) and return unless request.referer
    if URI(request.referer).path == edit_billable_service_path(@billable_service)
      respond_with(@billable_service)
    else
      respond_with @billable_service, location: request.referer
    end
  end

  # Removes this BillableService from an invoice
  def remove
    @client.remove_billable_service(@btn, @invoice, @billable_service)

    if URI(request.referer).path == edit_billable_service_path(@billable_service)
      respond_with(@billable_service)
    else
      respond_with @billable_service, location: request.referer
    end
  end

  private


    def set_billable_service
      @billable_service = BillableService.find(params[:id])
    end


    def set_client
      @client = Client.find(params[:client_id])
    end

    def set_btn
      @btn = Btn.find(params[:btn_id])
    end

    def set_invoice
      @invoice = Invoice.find(params[:invoice_id])
    end

    def billable_service_params
      if params[:billable_service][:product_id] == ''
        params[:billable_service][:product_id] = nil
      end
      params.require(:billable_service).permit(
        :client_id, :invoice_id, :product_id, :site_id, :qty, :service_begin, :service_end, :discount,
        :renewable, :comments, :total_price, :product_name, :asset_id, :foreign_pid,
        :unit_price, :frequency_months, :active_month, :renewal_id, :human_validation_date, :billing_period_started_at, :billing_period_ended_at, :unitprice, :billing_period, :charge_mode_id, :recurrence_type, :assignment_level, :prorate_id_start, :prorate_id_end, :reference_number, :supplier_account_no, :supplier_id, :password1, :note)



    end

    def has_client_id_param?
      params[:client_id].present?
    end

    def has_btn_id_param?
      params[:btn_id].present?
    end

    def has_invoice_id_param?
      params[:invoice_id].present?
    end

  protected

    def onetime_search_params
      if params.include?("billable_service_export_search")
        @search_params ||= params.delete(:billable_service_export_search) || {}
      else
        @search_params = {}
      end

      if not @search_params.include?("onetime_startdate")
        @search_params[:onetime_startdate] = Date.today.beginning_of_month
      end

      if @search_params[:onetime_startdate] == ""
        @search_params[:onetime_startdate] = Date.today.beginning_of_month
      end

      if not @search_params.include?("onetime_enddate")
        @search_params[:onetime_enddate] = Date.today.end_of_month
      end

      if @search_params[:onetime_enddate] == ""
        @search_params[:onetime_enddate] = Date.today.end_of_month
      end


      if not @search_params.include?("onetime")
        @search_params[:onetime] = true
      end

      @search_params

    end

    def search_params
      if params.include?("billable_service_search")
        @search_params ||= params.delete(:billable_service_search) || {}
      elsif params.include?("billable_service_export_search")
        @search_params ||= params.delete(:billable_service_export_search) || {}
      else
        @search_params = {}
      end

      if @client
        @search_params[:client] = @client.id
      end
      @search_params
    end

end
