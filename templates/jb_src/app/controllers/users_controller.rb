class UsersController < AuthorizationController
  include Search::ControllerHelpers

  respond_to :html

  before_action :set_user, only: [:show]

  def show
    respond_with(@user)
  end

  private

    def set_user
      @user = User.find(current_user.id)
    end

end
