class Users::SessionsController < Devise::SessionsController
  skip_before_action :set_return_to

  layout 'login'

end
