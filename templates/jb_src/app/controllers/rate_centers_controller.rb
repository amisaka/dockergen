class RateCentersController < AuthorizationController
  include Search::ControllerHelpers

  # active_scaffold :rate_center do |config|
  #   #config.list.columns = [ :client, :name, :building, :unitnumber, :phones ]
  #   #config.actions.exclude :show
  #   #config.columns.exclude :updated_at
  #   #config.columns.exclude :created_at
  #   config.update.columns.exclude :cdrs
  #   config.update.columns.exclude :local_exchanges
  #   config.show.columns.exclude :cdrs
  #   config.show.columns.exclude :local_exchanges
  #   config.list.columns.exclude :local_exchanges_north_american_exchanges
  #   #config.columns[:client].form_ui = :select
  # end


  protected
  def new_site_params(params)
    params.permit(:building_id, :name, :unitnumber, :site_number, :activation_date, :termination_date)
  end

end
