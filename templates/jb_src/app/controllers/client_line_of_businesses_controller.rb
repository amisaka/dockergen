class ClientLineOfBusinessesController < ApplicationController
  def update
    @client_line_of_business = ClientLineOfBusiness.find(params[:id])

    @client_line_of_business.update(client_line_of_business_params)

    respond_with(@client_line_of_business, location: request.referer)
  end

  private

    def client_line_of_business_params
      params.require(:client_line_of_business).permit(:activated)
    end
end
