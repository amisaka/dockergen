class DatasourcesController < AuthorizationController
  include Search::ControllerHelpers

  # active_scaffold :datasource do |config|
  #   #config.list.columns.exclude :cdr
  #   #config.list.columns.exclude :call
  #   config.columns.exclude :cdrs
  #   config.columns.exclude :calls
  #   config.actions.exclude :show
  #   config.actions.exclude :edit
  # end

  def beginning_of_chain
    Datasource.by_filename
  end
end
