class QueryController < ApplicationController
  include Search::ControllerHelpers
  
  def reset
    session[:query] = nil
    url = URI(request.referrer)
    host_and_path = "#{url.scheme}://#{url.host}#{url.path}"
    new_url = "#{host_and_path}"
    redirect_to new_url || root_path
  end
end
