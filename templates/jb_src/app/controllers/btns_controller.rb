class BtnsController < AuthorizationController
  include Search::ControllerHelpers

  layout 'admin_lte'

  before_action :set_btn, only: [:show, :edit, :update, :destroy]

  respond_to :html, :json

  def index
    if query
      @btns = BtnDecorator.decorate_collection(Btn.search(search_query).page(params[:page]))
    else
      @btns = Btn.active.page(params[:page]).decorate
    end
    respond_with(@btns)
  end

  def show
    respond_with(@btn)
  end

  def new
    @btn = Btn.new.decorate
    respond_with(@btn)
  end

  def edit
  end

  def create
    @btn = Btn.new(btn_params).decorate
    @btn.save
    respond_with(@btn)
  end

  def update
    @btn.update(btn_params)

    respond_with(@btn) and return unless request.referer
    if URI(request.referer).path == edit_btn_path(@btn)
      respond_with(@btn)
    else
      respond_with @btn, location: request.referer
    end
  end

  def destroy
    @btn.destroy
    respond_with(@btn)
  end

  def typeahead
    if params[:client_id].blank?
      @btns = Btn.search(search_query).page params[:page]
    else
      if params[:query].blank?
        @btns = Client.find(params[:client_id]).btns.page params[:page]
      else
        @btns = Client.find(params[:client_id]).btns.search(search_query).page params[:page]
      end
    end
    respond_with(@btns)
  end

  private

    def set_btn
      @btn = Btn.find(params[:id]).decorate
    end

    def btn_params
      params.require(:btn).permit(:billingtelephonenumber, :activated_at, :deactivated_at, :invoicing_enabled)
    end

    def search_query
      h = {}
      h = h.merge(billingtelephonenumber: query) unless query.blank?
      h = h.merge(status: search_status) unless search_status.blank?
    end

end
