class BuildingsController < AuthorizationController
  include Search::ControllerHelpers

  layout 'admin_lte'

  before_action :set_building, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @buildings = Building.all.page(params[:page]).decorate
    respond_with(@buildings)
  end

  def show
    respond_with(@building)
  end

  def new
    @building = Building.new
    respond_with(@building)
  end

  def edit
  end

  def create
    @building = Building.new(building_params)
    @building.save
    if params[:site_id]
      @site = Site.find(params[:site_id])
      if @site
        @site.building = @building
        @site.save
        respond_with(@site) and return
      end
    end
    respond_with(@building)
  end

  def update
    @building.update(building_params)
    respond_with(@building)
  end

  def destroy
    @building.destroy
    respond_with(@building)
  end

  private

    def set_building
      @building = Building.find(params[:id])
    end

    def building_params
      params.require(:building).permit(:name, :address1, :address2, :city, :province, :country, :postalcode)
    end

end
