class DashboardsController < AuthorizationController
  include Search::ControllerHelpers

  layout 'admin_lte'

  before_action :set_client, :set_selectable_invoices, only: [:process_payment, :exports]

  def index
    @customers_count = Client.activated.count
    @btns_count = Btn.activated.count
    @sites_count = Site.activated.count
    @phones_count = Service.activated.count
  end

  def process_payment
    @payment = Payment.new(client: @client)

  end

  private

    def set_selectable_invoices
      @selectable_invoices = @client.invoices
    end

    def set_client
      @client = Client.find(params[:client_id])
    end

end
