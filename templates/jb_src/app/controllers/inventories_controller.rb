class InventoriesController < ApplicationController

  include Search::ControllerHelpers
  #include Search::ClientHelpers
  layout 'admin_lte'
  
  before_action :set_inventory, only: [:show, :edit, :update, :destroy]

  #before_action :set_inventory_search
 
  respond_to :html, :json
  
  ################################################################
  ##  clients#index
  ################################################################


  #include Montbeau::Controllers::Inventories::IndexHelpers
  # sets instances variables for `clients#index` view
  # @see Montbeau::Controllers::Clients::IndexHelpers
  #before_action :set_inventory, only: [:index]

  def index
	@inventories = Inventory.all
    	#@inventories = Inventory.first(10)
   	#@inventories = Inventory.inventory_index

	#@search = BillableServiceExportSearch.new(search_params)
	#@billable_services_all = @search.results.all
	#@results_length = @billable_services_all.length
	#@billable_services = @search.results.all.page params[:page]
	@search = InventoryIndexSearch.new(search_params)
	@inventories_all = @search.results.all
	@results_length = @inventories_all.length
	@inventory_index_search = @search.results.all.page params[:page]
	request.parameters.merge({:page => params[:page]})
	puts @search.results.to_sql
	respond_to do |format|
	  format.html
	  format.json {         send_data self.to_csv, filename: "Charges_#{DateTime.now.strftime "%Y-%m-%d_%H%M"}.csv" } # for some reason csv doesn't work....
	end

    	respond_with(@inventories)
  end

  ################################################################
  ##  inventories#show
  ################################################################

  def show
    # This will show the site listing all the inventory items 
    # associated to that site. 
       
    #@inventory = Inventory.find(params[:id])
    #@client = Inventory.client(params[:id])
    @inventory = Inventory.find(params[:id])
    respond_with(@inventory)
  end

  ################################################################
  ##  inventories#show
  ################################################################

  def new
    @inventory = Inventory.new
    respond_with(@inventory)
  end

  # The following is because I do not know how to pass an id to the 
  # new controller above.
  def mod_new
    @inventory = Inventory.new
    @inventory_back = Inventory.find(params[:id])
    @inventory.site_id = @inventory_back.site_id
    respond_with(@inventory)
  end

  ################################################################
  ##  inventories#show
  ################################################################

  def edit
    @inventory = Inventory.find(params[:id])
    respond_with(@inventory)
  end

  ################################################################
  ##  inventories#show
  ################################################################

  def create
    @inventory = Inventory.new(inventory_params)
    @inventory.save
    respond_with(@inventory)
  end

  ################################################################
  ##  inventories#show
  ################################################################

  def update
    #inventory_params[:password] = @inventory.password_encrypt(@inventory.password)
    ip = inventory_params
    ip[:password] = @inventory.password_encrypt(ip[:password])
    #byebug
    # (byebug) <ActionController::Parameters {"start_date"=>"2015-04-24", "description"=>"Internet FTTB 25M/25M", "quantity"=>"1", "terminate_on"=>"", "compte_fttb"=>"", "installed_on_line"=>"5146849488", "numerodereference"=>"", "password"=>"Canada01", "realsyncspeeddownload"=>"25 Mb", "realsyncspeedupload"=>"25 Mb", "username"=>"rpffh8x4", "inventory_type"=>"internet_services_pppoe", "did_supplier_id"=>"53", "site_id"=>"1942"} permitted: true>

    @inventory.update(ip)
    #@inventory.update(inventory_params)
    respond_with(@inventory)
  end

  ################################################################
  ##  inventories#show
  ################################################################

  def destroy
    @inventory.destroy
    respond_with(@inventory)
  end

  ################################################################
  ##  inventories#typeahead
  ################################################################

  
#  def typeahead
#
#    if params[:BTN] == "true"
#      puts ">>> #{params}"
#      @clients =  Inventory.search(billingtelephonenumber: client_search_query, status: :active).page params[:page]
#    else
#      @clients = Inventory.search(name: client_search_query, status: :active).page params[:page]
#    end
#
#    #puts ">>> #{@clients[0]}"
#
#    respond_with(@clients)
#  end

  private

    ################################################################
    ##  Setters
    ################################################################

    def set_inventory
      #@inventory = InventoryDecorator.new(Inventory.find(params[:id]))
      @inventory = Inventory.find(params[:id])
    end

    #def set_next_inventory
    #  @next_inventory = inventory_set.next(@inventory)
    #end

    #def set_previous_inventory
    #  @previous_inventory = inventory_set.previous(@inventory)
    #end

    #def inventory_set
    #  if !inventory_search_query.blank?
    #    InventorySet.search(inventory_search_field => inventory_search_query, billingtelephonenumber: inventory_search_btn, customer_name: inventory_search_customer_name)
    #  else
    #    #InventorySet.new(Inventory.activated)
    #  end
    #end

    ################################################################
    ## Parameters white list 
    ################################################################

    def inventory_params
      params.require(:inventory).permit(:btn, :customer_name, :start_date, :agreement_num, :auto_renewal_term, :circuit_num_logique, :circuit_num_physique, :download_speed, :expiry_date, :parent_agreement_num, :term, :upload_speed, :z_end, :supplier, :email_of_courrier, :adresse_ip_lan_data, :adresse_ip_lan_voip, :adresse_ip_link_data, :adresse_ip_link_voip, :adresse_ipv6_lan_data, :adresse_ipv6_lan_voip, :adresse_ipv6_link_data, :adresse_ipv6_link_voip, :device, :router_principal, :router_vrrp, :sous_interface_data, :sous_interface_voip, :sous_interface_voip_vrrp, :vlan_data_q_in_q, :vlan_supplier, :vlan_voip_q_in_q, :vrrp_id, :address1, :city, :billingtelephonenumber, :site_number, :site_name, :description, :product_id, :quantity, :service_id, :terminate_on, :vl_videotron, :supplier_num, :customer_sn, :mac_address, :compte_fttb, :installed_on_line, :numerodereference, :password, :realsyncspeeddownload, :realsyncspeedupload, :username, :circuit_num, :devicenamemonitored, :inventory_type, :did_supplier_id, :deleted, :deleted_at, :site_id, :inventory_status_id, :activated_at, :deactivated_at)
    end

    ################################################################
    ## Helper methods
    ################################################################

    #def set_inventory_search
    #  #@inventory_search = InventorySearch.new(inventory_search_params)
    #end

  protected

    def search_params
      if params.include?("inventory_index_search")
        @search_params ||= params.delete(:inventory_index_search) || {}
      else
        @search_params = {}
      end

      #if @client
      #  @search_params[:client] = @client.id
      #end
      @search_params
    end

end
