class CallsController < AuthorizationController
  include Search::ControllerHelpers

  before_action :load_datasource
  load_and_authorize_resource #:through => :current_user

  # active_scaffold :call do |config|
  #   config.list.pagination = :infinite
  #   #XXX config.list.page_links_window = 0
  #   config.columns.exclude :cdrs
  #   config.columns.exclude :updated_at
  #   config.columns.exclude :created_at
  #   config.action_links.delete :show
  #   config.action_links.delete :edit
  #   config.action_links.add    :show, :label => 'details', :page => true, :type => :member
  # end

  def show

  end

  def load_datasource
    if params[:datasource_id]
      @datasource = Datasource.find(params[:datasource_id])
    elsif params[:client_id]
      # XXX should load thie *THROUGH* current user
      @client      = Client.find(params[:client_id])
    elsif params[:calledphone_id]
      @calledphone = Service.find(params[:calledphone_id])
    elsif params[:callingphone_id]
      @callingphone = Service.find(params[:callingphone_id])
    end
  end

  def beginning_of_chain
    if @datasource
      @datasource.calls
    elsif @client
      @client.calls
    elsif @calledphone
      @calledphone.incoming_calls
    elsif @callingphone
      @callingphone.outgoing_calls
    else
      Call.by_date
    end
  end

end
