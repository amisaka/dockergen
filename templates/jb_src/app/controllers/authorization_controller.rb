class AuthorizationController < ApplicationController

  before_action :authenticate_user!

  check_authorization unless: :devise_controller?

  load_and_authorize_resource

  rescue_from CanCan::AccessDenied do |exception|
    Rails.logger.debug "Access denied on #{exception.action} #{exception.subject.inspect}"
    respond_to do |format|
      format.json { head :status => 403 }
      format.html { render :file => "#{Rails.root}/public/403.html", :status => 403, layout: false } # layout false is necessary unless we want unauthorized users to see all the UI and links
      format.pdf { raise exception }
    end
  end

end
