class ProductsController < AuthorizationController
  include Search::ControllerHelpers

  layout 'admin_lte'

  before_action :set_product, only: [:show, :edit, :update, :destroy]

  respond_to :html, :json

  def index
    # @products = Product.all #.page params[:page]
    # @products = Product.includes(:line_of_business).order("line_of_business.code DESC")
    @products = Product.includes(:line_of_business).order(:code)

    respond_with(@products)
  end

  def show
    respond_with(@product)
  end

  def new
    @product = Product.new
    respond_with(@product)
  end

  def edit
    @dependent_bs = BillableService.where(:product => @product)
    respond_with(@product, @dependent_bs)
  end

  def create
    @product = Product.new(product_params)
    @product.save
    respond_with(@product)
  end

  def update
    @product.update(product_params)
    respond_with(@product)
  end

  def destroy
    @product.destroy
    respond_with(@product)
  end

  private

    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:description, :name, :unitprice, :renewable, :available, :code, :amount, :purchase_amount, :product_category_id, :product_class_id, :product_type_id, :gl_account_id, :line_of_business_id)
    end

end
