class API::V1::PhonesController < API::V1::V1Controller

  respond_to :json

  before_action :set_btn

  before_action :set_phone, only: [:show]

  def index
    @phones = @btn.services

    respond_to do |format|
      format.json { render json: @phones }
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @phone }
    end
  end

  private

    def set_btn
      @btn = Btn.find_by billingtelephonenumber: params[:btn_slug]
    end

    def set_phone
      @phone = @btn.phones.find_by phone_number: params[:slug]
    end

end
