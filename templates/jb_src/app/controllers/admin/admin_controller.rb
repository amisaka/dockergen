class Admin::AdminController < AuthorizationController

  layout "administrator"

  before_action :ensure_admin

  def ensure_admin
    raise CanCan::AccessDenied, "user is not an admin" unless current_user.admin?
  end

  def current_ability
    @current_ability ||= AdminAbility.new(current_user, params[:format])
  end

end
