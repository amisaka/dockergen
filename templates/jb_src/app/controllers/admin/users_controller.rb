module Admin
  class UsersController < AdminController

    respond_to :html

    before_action :set_user, only: [:show, :edit, :update, :delete]

    def index
      @users = User.all
      respond_with(@users)
    end

    def new
      @user = User.new
      respond_with(@user)
    end

    def show
      respond_with(@user)
    end

    def edit
      respond_with(@user)
    end

    def create
      @user = User.create(user_params)
      respond_with(@user)
    end

    def update
      @user.update(user_params)
      respond_with(@user)
    end

    def delete
      @user.delete
      respond_with(@user)
    end

    private

      def set_user
        @user = User.find(params[:id])
      end

      def user_params
        params.require(:user).permit(:admin, :agent, :fullname, :email, :encrypted_password, :phone, :username, :password, :password_confirmation)
      end

  end
end
