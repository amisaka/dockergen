class StatsPeriodsController < AuthorizationController
  include Search::ControllerHelpers

  # active_scaffold :stats_period do |config|
  #   config.list.columns = [ :day, :type, :statcount ]
  #   config.actions.exclude :show
  #   config.actions.exclude :edit
  # end

  def begining_of_chain
    periods = StatsPeriod
    if @client
      periods = StatsPeriod.joins(:callstats).where("callstats.client_id" => @client.id)
    end
    periods
  end

end
