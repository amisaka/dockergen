class PaymentsController < AuthorizationController
  include Search::ControllerHelpers

  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  before_action :set_client

  before_action :set_selectable_btns, :set_selectable_invoices, only: [:new, :edit]

  layout 'admin_lte'

  respond_to :html

  def index
    if @client
      @payments = @client.payments
    else
      @payments = Payment.all
    end

    @payments = @payments.order(date: :desc)

    respond_with(@payments)
  end

  def show
    respond_with(@payment)
  end

  def new
    @payment = Payment.new
    respond_with(@payment)
  end

  def edit
  end

  def create
    @payment = Payment.create(payment_params)
    respond_with(@payment, location: client_payments_path(@payment.client))
  end

  def update
    @payment.update(payment_params)
    respond_with(@payment)
  end

  def destroy
    @payment.destroy
    respond_with(@payment)
  end

  private

    def set_payment
      @payment = Payment.find(params[:id])
    end

    def payment_params
      params.require(:payment).permit(:date, :amount, :type, :reason,
        :client_id, :posted_date, :balance, :description, :posted, :imported, :gl_account_id, :transaction_method_id, :transaction_type_id )
    end

    def set_selectable_btns
      begin
        @selectable_btns = @client.btns
      rescue
        @selectable_btns = @client.btn
      end
    end

    def set_selectable_invoices
      @selectable_invoices = @client.invoices
    end

    def set_client
      if @payment && @payment.client
        @client = @payment.client
      elsif has_client_id?
        @client = Client.find(params[:client_id])
      end
    end

    def has_client_id?
      !params[:client_id].blank?
    end

end
