# @author Maxime Gauthier <maxime@credil.org>
class ClientsController < AuthorizationController
  include Search::ControllerHelpers
  include Search::ClientHelpers

  layout 'admin_lte'

  before_action :set_client, :set_billing,
    only: [:show, :edit, :update, :destroy]

  before_action :set_next_client, :set_previous_client,
    only: [:show, :edit]

  before_action :set_client_search

  respond_to :html, :json


  ################################################################
  ##  clients#index
  ################################################################


  include Montbeau::Controllers::Clients::IndexHelpers
  # sets instances variables for `clients#index` view
  # @see Montbeau::Controllers::Clients::IndexHelpers
  before_action :set_clients, only: [:index]

  def index
    if @clients.size == 1
      redirect_to(@clients.first) and return
    end
    respond_with(@clients)
  end


  ################################################################
  ##  clients#show
  ################################################################


  include Montbeau::Controllers::Clients::ShowHelpers
  # sets instances variables for `clients#show` view and helper methods
  # @see Montbeau::Controllers::Clients::ShowHelpers
  before_action :set_client_btn, :set_client_sites, :set_singular_site,
    :selectable_sites, :selectable_services, :selectable_invoices,
    :selected_invoice, :selected_site, :selectable_buildings,
    :selectable_otrs_customer_users, :selectable_did_suppliers,
    :set_phones, :set_phone, :set_billable_services, :set_billable_service,
    :set_site, :set_client_client_line_of_businesses,
    :set_client_billable_services_count, :set_client_billable_services_total,
    :set_client_phones_count,
    only: [:show]

  helper_method :deactivated_sites?, :deactivated_phones?,
    :deactivated_billable_services?,:recurring_billable_services?, :client_show_safe_params

  def show

    # Rails calls before_action methods, most in
    # sge/lib/montbeau/controllers/clients/show_helpers.rb

    respond_with(@client)
  end


  ################################################################
  ##  clients#new
  ################################################################


  def new
    @client = Client.new.decorate
    respond_with(@client)
  end


  ################################################################
  ##  clients#edit
  ################################################################


  include Montbeau::Controllers::Clients::EditHelpers
  # fixes clients crm_account if needed
  # @see Montbeau::Controllers::Clients::EditHelpers
  before_action :fix_crm_account, only: [:edit]

  def edit
    respond_with(@client)
  end


  ################################################################
  ##  clients#create
  ################################################################


  def create
    # Bad practise to have transaction inside controller
    # according to https://stackoverflow.com/questions/30768344/using-transaction-in-ruby-on-rails-controller-method
    # but quick fix for t13292
    success = false
    ActiveRecord::Base.transaction do
	    @client = Client.create(client_params)
	    @billing = Billing.create!(client: @client, billing_start_date: (Date.today + 1.month).beginning_of_month, activated_at: Time.now)
	    @default_site = Site.create!(
	      client: @client, name: 'SS/HQ', site_number: '0', building: \
	        Building.new(address1: @client.billing_address_1,
	          address2: @client.crm_account.billing_address_street_2,
	          city: @client.billing_address_city,
	          province: @client.crm_account.billing_address_state,
	          country: @client.billing_address_country,
	          postalcode: @client.billing_address_postalcode))
	    @client.line_of_businesses << LineOfBusiness.find_by(code: "VOIP Services")
	    @client.btn.invoicing_enabled = true
        raise "client has no id" if @client.id.nil?
        p = Payment.create(:type => "OpeningBalance",
	                                       :description => "Opening Balance",
                                           :transaction_type_id => 1,
	                                       :amount => 0,
                                           :client => @client,
	                                       :posted_date => Date.today,
	                                       :date => Date.today,
                                               :gl_account => GlAccount.where(:code => 10110)[0]
                          )
        p.save!
	    @client.payments << p

	    success = @client.save!
    end

    # I don't think this is correct.
    # request.referer refers to the previous URL.  This is always set, no?
    # The previous referer is always /new .  Why would you go back to it?
    # And why, as of 2018/03/22, when creating a client, the user feedback says
    # "client not created" when it fact it has, and shows me an update button
    # The logic here is just wrong, it seems to me.
    # But I'm no rails controller expert, though I've mastered a bit models
    respond_with(@client) and return unless request.referer
    if URI(request.referer).path == new_client_path
      respond_with(@client)
    else
      respond_with @client, location: request.referer
    end
  end


  ################################################################
  ##  clients#update
  ################################################################


  def update
    @client.update(client_params)
    if client_params.has_key?(:human_validation_date)
      if client_params[:human_validation_date] =~ /\A1\z/
        @client.human_validation_date = Time.now
      elsif client_params[:human_validation_date] =~ /\A0\z/
        @client.human_validation_date = nil
      end
    end

    @client.save

    respond_with(@client) and return unless request.referer
    if URI(request.referer).path == edit_client_path(@client)
      respond_with(@client)
    else
      respond_with @client, location: request.referer
    end
  end


  ################################################################
  ##  clients#destroy
  ################################################################


  def destroy
    @client.destroy
    respond_with(@client)
  end


  ################################################################
  ##  clients#typeahead
  ################################################################


  def typeahead

    if params[:BTN] == "true"
      @clients =  Client.search(support: params[:SUPPORT], billingtelephonenumber: client_search_query, status: :active).page params[:page]
    else
      @clients = Client.search(name: client_search_query, status: :active).page params[:page]
    end
    respond_with(@clients)
  end

  private

    ################################################################
    ##  Setters
    ################################################################

    def set_client
      @client = ClientDecorator.new(Client.find(params[:id]))
    end

    def set_billing
      @billing = @client.billing
    end

    def set_next_client
      @next_client = client_set.next(@client)
    end

    def set_previous_client
      @previous_client = client_set.previous(@client)
    end

    def client_set
      if !client_search_query.blank?
        ClientSet.search(client_search_field => client_search_query, invoice: client_search_invoice, status: client_search_status)
      else
        ClientSet.new(Client.activated)
      end
    end

    ################################################################
    ##  Parameters white list
    ################################################################

    def client_params
      params.require(:client).permit(:name, :billing_site_id, :description, :status, :note,
        :logo, :url, :billing_btn, :billing_cycle_id, :activated_at, :needs_review,
        :deactivated_at, :language_preference, :stats_enabled, :human_validation_date,
        :coverage_id,
        :activated_at, :deactivated_at, :sales_person_id, :service_level, statement_type_ids:[],
        btn_attributes: [ :id, :billingtelephonenumber, :invoice_template_id, :invoicing_enabled],
        crm_account_attributes: [ :id, :name, :billing_address_street, :billing_address_street_2,
                                  :billing_address_city, :billing_address_state, :billing_address_country, :billing_address_postalcode],
        billing_attributes: [ :id, :billing_start_date, :activated_at ],

                                    )
    end

    ################################################################
    ## Helper methods
    ################################################################

    def set_client_search

      @client_search = ClientSearch.new(client_search_params)
    end

end
