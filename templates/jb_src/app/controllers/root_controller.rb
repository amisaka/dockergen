class RootController < ApplicationController
  include Search::ControllerHelpers

  skip_authorization_check

  def index
    if current_user
      redirect_to dashboards_path
    else
      redirect_to new_user_session_path
    end
  end
end
