class NorthAmericanExchangesController < AuthorizationController
  include Search::ControllerHelpers

  # active_scaffold :north_american_exchange do |config|
  #   #config.list.columns = [ :client, :name, :building, :unitnumber, :phones ]
  #   #config.actions.exclude :show
  #   #config.columns.exclude :updated_at
  #   #config.columns.exclude :created_at
  #   #config.update.columns.exclude :btns
  #   #config.update.columns.exclude :phones
  #   #config.columns[:client].form_ui = :select
  #   config.list.columns.exclude :local_exchanges_north_american_exchanges
  # end

protected
  def new_site_params(params)
    params.permit(:building_id, :name, :unitnumber, :site_number, :activation_date, :termination_date)
  end

end
