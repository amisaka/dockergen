class CallstatsController < AuthorizationController
  include Search::ControllerHelpers

  before_filter :load_parents
  load_and_authorize_resource #:through => :current_user

  # active_scaffold :callstat do |config|
  #   config.list.columns = [ :stats_period, :value, :type, :client ]
  #   config.actions.exclude :show
  #   config.actions.exclude :edit
  # end

  def beginning_of_chain
    things = Callstat
    if @client
      things = @client.callstats
    end
    if @statperiod
      things = things.where(:stats_period_id => @statperiod.id)
    end
    things
  end
end
