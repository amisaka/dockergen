class SearchesController < AuthorizationController
  include Search::ControllerHelpers

  layout 'admin_lte'

  def create
    redirect_to show_search_path(params: search_params)
  end

  def show
    @search = Search.new

    # only search if a query is present
    if !search_params[:query].blank?
      @btns = Btn.search(billingtelephonenumber: search_params[:query], status: search_params[:status])
      @clients = Client.search(name: search_params[:query], status: search_params[:status])

      # redirect to clients index if it only finds clients
      if @clients.empty? && !@btns.empty?
        redirect_to btns_path(params: search_params) and return

      # redirect to btns index if it only finds BTNs
      elsif @btns.empty? && !@clients.empty?
        redirect_to clients_path(params: search_params) and return

      # Append all search results
      else
        @search.results << @clients
        @search.results << @btns
        @search.results.flatten!
        @search.results.uniq!
      end
    end

    # paginate search results
    @search.results = Kaminari.paginate_array(@search.results).page(params[:page])

    # set @results for the view
    @results = @search.results

    # redirects to resource if results contains only 1 record
    if @results.size == 1
      query and redirect_to @results.first
    else
      respond_with @search
    end
  end

  private

    def search_params
      params.permit(:query, :status)
    end

end
