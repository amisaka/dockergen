class ServicesController < AuthorizationController
  include Search::ControllerHelpers
  include Montbeau::ActivatableControllerHelpers

  layout 'admin_lte'

  before_action :set_phone, only: [:show, :edit, :update, :destroy, :remove]

  before_action :set_client, if: :has_client_id_param?
  before_action :set_btn, if: :has_btn_id_param?

  before_action :set_selectable_btns, only: [:edit]
  before_action :set_selectable_sites, only: [:edit]

  respond_to :html

  def index
    # @services = Service.all.page(params[:page]).decorate
    # respond_with(@services)

    @search = ServiceSearch.new(search_params)
    @services = @search.results.all
    @results_length = @services.length
    @services = @search.results.all.page params[:page]
    request.parameters.merge({:page => params[:page]})

    respond_with(@services)
  end

  def show
    respond_with(@service)
  end

  def new
    @service = Service.new.decorate
    @client = Client.find(params[:client_id])
    @btn = @client.btn
    respond_with(@service)
  end

  def edit
    @btn    = @service.btn
    @client = @btn.client
    respond_with(@service)
  end

  def create
    @service = Service.new(service_params).decorate
    @service.save

    redirect_to client_path(@service.btn.client, :anchor => "servicesList")
    # This was here before.  I don't know if handling request.referer  is necessary in this application
    # To get 13434 implemented , we commented this out -- jlam 2018/04/23
    # They may help us go back to the service listing page no
    #respond_with(@service) and return unless request.referer
    #if URI(request.referer).path == new_service_path
    #  respond_with(@service)
    #else
    #  respond_with @service, location: request.referer
    #end

  end

  def update
    @service.update(service_params.except(:human_validation_date))

    if service_params[:human_validation_date] =~ /\A1\z/
      @service.human_validation_date = Time.now
      @service.save!
    elsif service_params[:human_validation_date] =~ /\A0\z/
      @service.human_validation_date = nil
      @service.save!
    else
      @service.save!
    end

    redirect_to client_path(@service.btn.client, :anchor => "servicesList")

    # This was here before.  I don't know if handling request.referer  is necessary in this application
    # To get 13434 implemented , we commented this out -- jlam 2018/04/23
    # They may help us go back to the service listing page no
    #respond_with(@service.btn.client) #unless request.referer

    #respond_with(@service) and return unless request.referer
    #if URI(request.referer).path == edit_service_path
    #  respond_with @service
    #else
    #  respond_with @service, location: request.referer
    #end
  end

  def destroy
    @service.destroy
    respond_with(@service)
  end

  # def add
  #   @service = Phone.create(phone_params.except(:human_validation_date))
  #   @client.add_phone(@btn, @service)
  #
  #   if phone_params[:human_validation_date] =~ /\A1\z/
  #     @service.human_validation_date = Time.now
  #     @service.save!
  #   elsif phone_params[:human_validation_date] =~ /\A0\z/
  #     @service.human_validation_date = nil
  #     @service.save!
  #   end
  #
  #   respond_with(@service) and return unless request.referer
  #   if URI(request.referer).path == phones_path
  #     respond_with @service
  #   else
  #     respond_with @service, location: request.referer
  #   end
  # end
  #
  # def remove
  #   @client.remove_phone(@btn, @service)
  #
  #   respond_with(@service) and return unless request.referer
  #   if URI(request.referer).path == phones_path
  #     respond_with @service
  #   else
  #     respond_with @service, location: request.referer
  #   end
  # end

  private

    def service_params
      params.require(:service).permit(:site_id, :btn_id, :activated, :description, :display_text,
        :phone_number, :service_id, :extension,
        :voicemail_number, :access_info, :notes, :activated_at, :start_date,
        :deactivated_at, :termination_date, :phone_mac_addr, :phone_type_id, :sip_username,
        :e911_notes, :e911_tested, :e911_tested_by_id, :e911_tested_date,
        :voip_supplier_id, :email, :ipaddress, :connectiontype_id, :sip_password,
        :virtualdid, :voicemailportal, :callerid, :huntgroup, :autocreate,
        :rate_plan_id, :line_of_businesses_id, :supplier_id, :phone_service_type_id,
        :local_rate_plan_id, :national_rate_plan_id, :world_rate_plan_id,
        :firstname, :lastname, :calling_firstname, :calling_lastname,
        :foreign_userId, :foreign_pid, :rate_center_id, :human_validation_date, :default_origination, :default_destination, :coverage_id)
    end

    def set_phone
      @service = Service.find(params[:id]).decorate
    end

    def set_client
      @client = Client.find(params[:client_id])
    end

    def set_btn
      @btn = Btn.find(params[:btn_id])
    end

    def set_selectable_btns
      if @client
        @selectable_btns = @client.btns
      else
        @selectable_btns = Btn.all
      end
    end

    def set_selectable_sites
      @selectable_sites = Service.find(params[:id]).btn.client.sites.order(:site_number)
      # commented out this code, I could see performance issues with Site.all
      #if @client
      #  @selectable_sites = @client.sites
      #else
      #  @selectable_sites = Site.all
      #end
    end

    def has_client_id_param?
      !params[:client_id].blank?
    end

    def has_btn_id_param?
      !params[:btn_id].blank?
    end

protected

    def search_params
      @search_params ||= params.delete(:service_search) || {}

      if @search_params != {}
        @search_params[:client] = @client.id
      else
        # set a default - can't figure out how to just return empty like billableservices
        @search_params[:client] = @client.id
        @search_params[:activated] = "1"
        @search_params[:lineofbusiness] = "12"
        @search_params[:servicetype] = "5"
      end

      @search_params
    end


    ## LEGACY CODE

    # active_scaffold :phone do |config|
    #   config.list.columns = [ :site, :btn, :email,
    #     :phone_number, :extension, :virtualdid,
    #     #:incoming_call_count, :outgoing_call_count,
    #     :sip_username, :phone_mac_addr]
    #   config.columns.exclude :sip_password
    #   config.columns[:site].form_ui = :select
    #   config.columns[:btn].form_ui = :select
    #   config.columns.exclude :outgoing_cdrs
    #   config.columns.exclude :incoming_cdrs
    #   config.columns.exclude :outgoing_calls
    #   config.columns.exclude :incoming_cdrs
    # end

    # def beginning_of_chain
    #   if @client
    #     @client.phones
    #   else
    #     Phone
    #   end
    # end

end
