class TechnicalContactsController < AuthorizationController

  layout 'admin_lte'

  before_action :set_client, if: :has_client_id_param?
  before_action :set_technical_contact, only: [:show, :edit, :update, :destroy]

  respond_to :html

  load_and_authorize_resource
  # GET /technical_contacts
  # GET /technical_contacts.json
  def index
    @technical_contacts = TechnicalContact.all
  end

  # GET /technical_contacts/1
  # GET /technical_contacts/1.json
  def show
  end

  # GET /technical_contacts/new
  def new
    @technical_contact = TechnicalContact.new
  end

  # GET /technical_contacts/1/edit
  def edit
  end

  # POST /technical_contacts
  # POST /technical_contacts.json
  def create
    @technical_contact = TechnicalContact.new(technical_contact_params)
    if @technical_contact.save
        redirect_to :controller => 'contacts', :action => 'index'
      else
        #something
    end

    # respond_to do |format|
    #   if @technical_contact.save
    #     redirect_to :controller => 'contacts', :action => 'index'
    #   else
    #     format.html { render :new }
    #     format.json { render json: @technical_contact.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /technical_contacts/1
  # PATCH/PUT /technical_contacts/1.json
  def update
    if @technical_contact.update(technical_contact_params)
      redirect_to :controller => 'contacts', :action => 'index'
    else
      #something
    end

    # respond_to do |format|
    #   if @technical_contact.update(technical_contact_params)
    #     redirect_to :controller => 'contacts', :action => 'index'
    #     # format.html { redirect_to @technical_contact, notice: 'Technical contact was successfully updated.' }
    #     # format.json { render :show, status: :ok, location: @technical_contact }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @technical_contact.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /technical_contacts/1
  # DELETE /technical_contacts/1.json
  def destroy
    @technical_contact.destroy
    redirect_to :controller => 'contacts', :action => 'index'
    # respond_to do |format|
    #   format.html { redirect_to technical_contacts_url, notice: 'Technical contact was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    def set_client
      @client = Client.find(params[:client_id])
    end

    def has_client_id_param?
      puts "has_client_id_param? #{      params[:client_id].present?}"
      params[:client_id].present?
    end


    # Use callbacks to share common setup or constraints between actions.
    def set_technical_contact
      @technical_contact = TechnicalContact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def technical_contact_params
      # params.fetch(:technical_contact, {})
      params.require(:technical_contact).permit(:name, :department, :email, :client_id)
    end
end
