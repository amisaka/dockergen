class BillingsController < ClientsController

  before_action :set_client, :set_billing

  rescue_from ActionController::ParameterMissing,
    with: :recover_from_missing_param

  def edit
    respond_with @billing
  end

  def update
    @billing.update(billing_params)
    respond_with @client, location: client_path(@client)
  end

  private

    def recover_from_missing_param(e)
      redirect_to edit_client_billing_path(@client)
    end

    def set_client
      @client = Client.find(params[:client_id])
    end

    def set_billing
      @billing = @client.billing
    end

    def billing_params
      params.require(:billing).permit(:balance, :activated_at, :deactivated_at,
        :billing_start_date, :last_usage_date, :last_statement_date,
        :last_statement_amount, :statement_type, :payment_method,
        :local_rate_plan_id, :national_rate_plan_id, :world_rate_plan_id)
    end

end
