#
# there are few routes that lead here (yet?), this is mostly only
# by the billing system to render invoices to HTML.
#
# generate updates the invoice
#
class InvoicesController < AuthorizationController
  include Search::ControllerHelpers

  layout 'admin_lte'

  respond_to :html, :pdf, :json

  before_action :set_invoice, only: [:show, :edit, :update, :destroy]

  def index

    case request.format
    when 'html'
	    if not params[:client_id].nil?
	        @client = Client.find(params[:client_id])
	    elsif not params[:btn].nil?
	        @client = Btn.where(billingtelephonenumber: params[:btn]).take.client
	    end

	    if @client.nil?
	        @invoices = Invoice.lastInvoiceOfEveryClient(limit: 3000)
	    else
	        @invoices = @client.invoices.order(created_at: :desc).limit(100).decorate
	    end
    when 'json'
	        @invoices = Invoice.lastInvoiceOfEveryClient(limit: 15000, onlyUnposted: true, includeChildInvoices: true)
    else
        raise "InvoicesController#index does not respond to format #{request.format}"
    end

    respond_with(@invoices)
  end

  def show
    @allowedTags     = ActionView::Base.sanitized_allowed_tags.clone
    @allowedTags << "span" << "a"


    @calls = @invoice.calls.decorate

    @clientOrSite = @invoice.ressource

    @billable_services = @invoice.billable_services


    I18n.locale = chooseInvoiceLanguage()

    respond_with(@invoice, layout: 'invoice')
  end

  def chooseInvoiceLanguage
    client = @clientOrSite.client

    return :en if client.nil?

    # Get an array of the non-nil locales of the statment types of the client
    locales = client.statement_types.pluck(:locale).select {|v| v}

    return :en if (locales.nil? or locales.count == 0)

    # Return the first non-enlish one or, if none, english
    return (locales.select{|l| l != "en"}[0] or "en").to_sym
  end

  def new
    @invoice = Invoice.new
    respond_with(@invoice)
  end

  def create
    @invoice = Invoice.create(invoice_params)
    respond_with(@invoice)
  end

  def edit
    respond_with(@invoice)
  end

  def update
    @invoice.update(invoice_params)
    respond_with(@invoice)
  end

  def destroy
    @invoice.destroy
    respond_with(@invoice)
  end

  private

    def invoice_params
      # Require the invoice parameter in the HTTP request, permit many others
      params.require(:invoice).permit(:client_id, :invoice_date, :service_start,
        :service_finish, :last_amount_invoiced, :last_amount_paid,
        :amount_invoiced, :pstbc, :pstalb, :pstsask, :hst_tvh, :gst_tps, :tvq,
        :invoicenumber, :invoice_path, :created_at, :updated_at, :btn_id,
        :invoice_run_id, :pstman, :call_pstbc, :call_pstalb, :call_pstsask,
        :call_pstman, :call_tvq, :call_gst_tps, :call_hst_tvh, :charges_start,
        :charges_finish, :services_subtotal, :charges_subtotal, :late_charges,
        :type, :site_id, :summary_invoice_id)
    end

    def set_invoice
      @invoice = Invoice.find(params[:id]).decorate
    end

end
