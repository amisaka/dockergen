class CdrsController < AuthorizationController
  include Search::ControllerHelpers

  before_filter :load_phone

  # active_scaffold :cdr do |config|
  #   config.list.columns = [ :recordid, :localcallid,
  #     :releasedatetime, :direction,
  #     :calledphone,  :calledclient,
  #     :callingphone, :callingclient,
  #   ]
  #   config.list.pagination = :infinite
  #   config.list.page_links_inner_window = 0
  #   config.columns[:calledphone].actions_for_association_links  = [:show]
  #   config.columns[:callingphone].actions_for_association_links = [:show]
  #   config.actions.exclude :delete
  #   config.actions.exclude :edit
  #   config.list.always_show_create = false
  #   #config.columns.exclude :sip_password
  # end

  def load_phone
    if params[:datasource_id]
      @datasource = Datasource.find(params[:datasource_id])
    elsif params[:calledphone_id]
      @calledphone = Service.find(params[:calledphone_id])
    elsif params[:callingphone_id]
      @callingphone = Service.find(params[:callingphone_id])
    elsif params[:client_id]
      @client = Client.find(params[:client_id])
    end
  end

  def beginning_of_chain
    if @datasource
      @datasource.cdrs
    elsif @calledphone
      @calledphone.incoming_cdrs
    elsif @callingphone
      @callingphone.outgoing_cdrs
    elsif @client
      if request.path =~ /incoming_cdrs/
        @client.incoming_cdrs
      else
        @client.outgoing_cdrs
      end
    else
      Cdr.byrecordid
    end
  end
end
