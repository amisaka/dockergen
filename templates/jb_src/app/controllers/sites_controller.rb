class SitesController < AuthorizationController
  include Search::ControllerHelpers
  include Montbeau::ActivatableControllerHelpers
  include Montbeau::Controllers::Clients::ShowHelpers
  layout 'admin_lte'

  before_action :set_site, only: [:show, :edit, :update, :destroy]
  before_action :set_client, if: :has_client?

  respond_to :html, :json

  include Montbeau::Controllers::Clients::ShowHelpers
  before_action :set_client_sites, only: [:index]
  helper_method :deactivated_sites?, :client_show_safe_params
  def index

    if @client
      @sites_count = @client.sites.count
      @sites_deactivated_count = @client.sites.deactivated.count
      @sites_activated_count = @client.sites.activated.count
      if request.format.json?
        @sites = @client.sites
      else
        @sites = @sites.page(params[:page]).decorate
      end
    else
      @sites = SiteDecorator.decorate_collection Site.all.activated.page(params[:page])
      @sites_count = Site.all.activated.count
    end


    respond_with(@sites)
  end

  def show
    respond_with(@site)
  end

  def new
    @site = Site.new.decorate

    respond_with(@site)
  end

  def edit
    respond_with(@site)
  end

  def create
    @site = Site.create #(site_params)
    if ! @site.building
      @site.building = Building.create
      @site.building.save
      @site.save
    end
    @site.update(site_params) # do the update here so it has a building

    respond_with(@site) and return unless request.referer
    if URI(request.referer).path == new_site_path
      respond_with(@site)
    else
      respond_with @site, location: request.referer
    end
  end

  def update
    @site.update(site_params)

    respond_with(@site, location: session.delete(:return_to)) and return if session[:return_to]
    respond_with(@site) and return unless request.referer
    if URI(request.referer).path == edit_site_path
      respond_with(@site)
    else
      respond_with @site, location: request.referer
    end
  end

  def destroy
    @site.destroy

    respond_with(@site)
  end

  def add
    @site = Site.create(site_params)
    @client.add_site(@site)

    respond_with(@site) and return unless request.referer
    if URI(request.referer).path == sites_path
      respond_with(@site)
    else
      respond_with @site, location: request.referer
    end
  end

  def remove
    @site.terminated!
    @site.save!

    respond_with(@site) and return unless request.referer
    if URI(request.referer).path == sites_path
      respond_with(@site)
    else
      respond_with @site, location: request.referer
    end
  end

  def set_service_level
    @site.update(service_params)
    @site.make_otrs_user
  end

  private

    def set_client
      @client = Client.find(params[:client_id])
    end

    def set_site
      @site = Site.find(params[:id])
    end

    def service_params
      params.require(:site).permit(:support_service_level)
    end

    def site_params
      params.require(:site).permit( :building_id, :name, :unitnumber,
                                    :site_number, :client_id, :site_rep_id,
                                    :support_service_level,
                                    :city,
                                    :postalcode,
                                    :province,
                                    :country,
                                    :address1,
                                    :address2,
        building_attributes: [ :city, :postalcode, :province, :country,
                               :address1, :address2 ])
    end

    def has_client?
      !params[:client_id].blank?
    end

end
