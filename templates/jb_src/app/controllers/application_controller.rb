require "application_responder"

class ApplicationController < ActionController::Base
  include Search::ControllerHelpers
  include ParamsHelper

  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :exception

  before_action :load_parents

  before_action :set_raven_context
  before_action :set_last_seen_at, if: proc { user_signed_in? }

  helper_method :redirect_back_path

  before_action :set_return_to, only: [:show, :new, :edit]

  # pass in the format to the initializer, so we can filter by what format is asked
  # for.
  def current_ability
    @current_ability ||= Ability.new(current_user, params[:format])
  end

  private

  def set_last_seen_at
    current_user.update_attribute(:last_seen_at, Time.current)
  end

  def set_return_to
    if request.referer && !(URI(request.referer).path == safe_url_for(controller: self.class.controller_name, action: :index, only_path: true))
      session[:return_to] = request.referer
    end
  rescue
    session[:return_to] = request.referer
  end

  def redirect_back_path
    return nil if request.referrer.blank?
    request.referrer
  end

  def store_location
    session[:return_to] = request.fullpath
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  def load_parents
    if params[:client_id]
      @client = Client.find(params[:client_id])
    end
    if params[:stats_period_id]
      @statperiod = StatsPeriod.find(params[:stats_period_id])
    end
  end

  def set_raven_context
    Raven.user_context(id: session[:current_user_id]) # or anything else in session
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

end
