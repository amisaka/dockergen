class CallDecorator < ApplicationDecorator
  delegate_all

  def duration
    Time.at(object.duration).utc.strftime('%H:%M:%S')
  end

  def area
    object.area.nil?     ? '' : object.area.mb_chars.upcase.to_s
  end


  def self.sumOfDuration(calls)
    self::fmtDuration(Call::sumOfDuration(calls))
  end

  def self.fmtDuration(duration)
    Time.at(duration).utc.strftime('%H:%M:%S')

  end

  def province
    return '' if object.province.nil?

    return 'QC' if object.province == 'PQ'

    return object.province.upcase
  end
end
