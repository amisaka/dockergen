class BuildingDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def address
    h.link_to build_address, h.building_path(object)
  end

  private

    def build_address
      h.content_tag :address do
        [model.address1, model.address2].join(' ')
      end
    end

    def missing_address
      logger.warn "Building : #{object.name} (#{object.id}) is missing an address."
      "[MISSING ADDRESS]"
    end

end
