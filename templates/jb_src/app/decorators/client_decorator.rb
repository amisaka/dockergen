class ClientDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def billing_address_city
    model.crm_account.billing_address_city
  rescue NoMethodError
    ''
  end

  def billing_address
    address = []

    [:billing_address_1, :billing_address_street_2, :billing_address_2, :billing_address_3, :billing_address_4].each do |line|

      value = object.send(line)
      address << value unless value.nil? || value.empty?
    end

    if address.empty?
      h.content_tag(:address, class: 'form-control', style: 'min-height: 3em; height: auto;') { missing_address }
    else
      h.content_tag(:address, class: 'form-control', style: 'min-height: 3em; height: auto;') { address.join('<br>').html_safe }
    end

  rescue NoMethodError
    h.content_tag(:address, class: 'form-control', style: 'min-height: 3em; height: auto;') { missing_address }
  end

  def invoice_cycle
    object.btn.invoice_template.invoice_cycle
  rescue
    ''
  end

  def sales_person
    model.sales_person.name
  rescue NoMethodError
    ''
  end

  def billingtelephonenumber
    model.btn.billingtelephonenumber
  rescue NoMethodError
    ''
  end

  def city
    billing_site.city
  rescue NoMethodError
    ''
  end

  def service_level
    (h.icon('star') * model.service_level).html_safe
  end

  private

    def missing_address
      logger.warn "Client : #{object.name} (#{object.id}) is missing a billing address."
      "[MISSING ADDRESS]"
    end

end
