class InvoiceDecorator < ApplicationDecorator
  delegate_all


  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def billing_address_1
    ressource.billing_address_1
  end

  def latePaymentDate()
    I18n.localize object.latePaymentDate, format: :long
  end

  def paymentsShownUntil()
    I18n.localize object.paymentsShownUntil, format: :long
  end

  def billing_address_2
    ressource.billing_address_2
  end

  def billing_address_3
    ressource.billing_address_3
  end

  def billing_address_4
    ressource.billing_address_4
  end

  def service_start
    return '' if object.service_start.nil?
    object.service_start
  rescue NoMethodError
    ''
  end

  def service_finish
    return '' if object.service_finish.nil?
    object.service_finish
  rescue NoMethodError
    ''
  end

  def charges_start
    return '' if object.charges_start.nil?
    object.charges_start
  rescue NoMethodError
    ''
  end

  def charges_finish
    return '' if object.charges_finish.nil?
    object.charges_finish
  rescue NoMethodError
    ''
  end

  def invoice_pdf_link
    if model
      (h.link_to 'PDF',h.viewpdf_invoice_path(model), :target => '_blank' ) + "|" + (h.link_to "View", h.viewhtml_invoice_path(model), :target => '_blank' )
    else
      h.content_tag :span, "PDF", class: "crossed-out"
    end
  end

  def date
    object.date.nil? ? object.created_at : object.date
  end

  ### Deprecated?  One usage removed from _pageone.html.erb -- jlam, 2018/03/05
  def last_amount_invoiced
    object.client.billing.last_statement_amount
  rescue NoMethodError
    0.0
  end

  ### Deprecated?  One usage removed from _pageone.html.erb -- jlam, 2018/03/05
  def last_amount_paid
    object.client.billing.last_payment_amount
  rescue NoMethodError
    0.0
  end

  ### Deprecated?  One usage removed from _pageone.html.erb -- jlam, 2018/03/05
  def late_charges
    #  TODO
    0.0
  end

  ### Deprecated?  One usage removed from _pageone.html.erb
  ### Rename as it conflicts with new Invoice attribute  due_immediately -- jlam, 2018/03/05
  def due_immediately_get
    object.client.billing.balance
  rescue NoMethodError
    0.0
  end

  def services_subtotal
    invoicing = Montbeau::Invoicing.new(object.ressource, chargesDates: object.chargesDates)
    invoicing.compute_recurring_charges
  end

  def billable_services_total
    invoicing = Montbeau::Invoicing.new(object.ressource, chargesDates: object.chargesDates)
    invoicing.compute_recurring_charges
  end

  def charges_subtotal
    raise "'charges_subtotal' is too much of an ambiguous term  and it's usage has been forbidden.  Don't use it.  Charges of what? calls? subtotal of what?"
    #Would call invoicing.compute_calls_charges in the past
  end

  def self.taxTypeList
    # There is no alberta PST, but at Miranda's request 2018/05/24, it was added as it is in the Kilmist invoices

    # These should be in their presentation order left to right.
    # I debated putting it as a hash witn integers as values representing left to right presentation order,
    #    but just using insert order as presetnation order is simpler
    attributes = [
        :bc_pst,
        :alberta_pst,
        :sask_pst,
        :manitoba_rst,
        :hst,
        :gst,
        :qst
    ]
  end

  # taxTypeSymbol: one of the symbols from the taxTypeList array
  def self.taxHeader_i18n(taxTypeSymbol)
    translationEntry = 'invoices._monthlycharges.taxes.' + taxTypeSymbol.to_s.downcase.tr(' ', '_')
    default = "<!-- Translation missing for #{translationEntry} -->" + taxTypeSymbol.to_s
    I18n.t(translationEntry, default: default).html_safe
  end




  ########################################################################
  # Though "amount" is a generic term, I've kept it given the lack
  # of a different term
  # It's the most important attribute of an invoice,
  # It is the total of recurring and call charges and
  # the amount by which the balance will be affected
  ########################################################################
  def amount_invoiced
    object.amount
  end

  def due_immediately
    # TODO
    self.past_due_balance
  end

  def total_due
    # TODO
    object.total_due
  end

  def due_date(format = :short_numeric)
    # TODO
    I18n.localize object.due_date, format: format
  end

  def invoice_version
    # TODO
    '0.0.1'
  end

  def cycle_code
    # TODO
    'XXX'
  end

  def customer_name
    ressource.name
  rescue NoMethodError
    ''
  end

  def account_code
    ressource.client.crm_account_id
  end

  def invoicenumber
    btnStr = 'NOBTN'
    clientIn = ressource.client.decorate
    btnStr = clientIn.btn.billingtelephonenumber if not clientIn.nil? and not clientIn.btn.nil?

    [btnStr, object.invoice_date.strftime('%Y-%m')].join('-')
  end

  # Date formats are defined in sge/config/locales/*.yml
  def invoice_date(format = :short_numeric)
    I18n.localize object.invoice_date, format: format
  end

  def department?
    false
  end

  def ressource_type()
    ressource.class.name.downcase
  end

  def to_json
    baseHash = object.slice(:id, :amount, :created_at, :posted_at)
    btnStr, invoice_cycle = object.btn_and_invoice_cycle
    addHash  = {
        ressource_type: ressource_type(),
        yearmonth: self.yearmonth,
        invoice_cycle: ressource.client.decorate.invoice_cycle,
        btn: btnStr,
        site_number: site_identifier,
    }
    return baseHash.merge(addHash).to_json
  end




  def site_identifier
    object.site.nil? ? '' : InvoicesHelper::sanitize_filename(site.site_number)
  end

  ########################################################
  # Prints out the month of the invoice date
  # like '2018-05'.  Foramt argument is ignored for now
  #######################################################
  def yearmonth(format: :monthNumeric)
    object.created_at.strftime('%Y-%m')
  end



  def total_calls_count
    # TODO
    invoicing = Montbeau::Invoicing.new(object.ressource, chargesDates: object.chargesDates)
    invoicing.compute_calls_count
  end

  ########################################################
  # Depercated? (as we want the summary tables to calculate
  # their own sums)
  ########################################################
  def total_calls_duration
    invoicing = Montbeau::Invoicing.new(object.ressource)
    Time.at(invoicing.compute_calls_duration).utc.strftime('%H:%M:%S')
  end

  def posted_at
    if object.posted_at.nil?
        if object.created_at > Invoice::POST_FEATURE_IMPLEMENTED
            I18n.t 'invoices.not_posted'
        else
            '*'
        end
    else
        I18n.localize object.posted_at, format: :medium
    end
  end

  def total_calls_cost
    invoicing = Montbeau::Invoicing.new(object.ressource)
    invoicing.compute_calls_cost
  end

  private

end
