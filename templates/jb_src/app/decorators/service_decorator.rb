class ServiceDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def phone_service_type
    object.phone_service_type.code
  rescue NoMethodError
    ''
  end

  def line_of_business
    object.line_of_business.code
  rescue NoMethodError
    ''
  end

  def description
    [object.description, object.display_text].join("<br>").html_safe
  rescue NoMethodError
    ''
  end

  def site
    if object.site
      h.link_to "#{object.site.to_s_withSiteNum}", object.site
    else
      missing_site
    end
  rescue NoMethodError
    missing_site
  end

  def service_id
    if object.service_id
      h.link_to object.service_id, object
    else
      ''
    end
  end

  def extension
    if object.extension
      h.link_to object.extension, object
    else
      ''
    end
  end

  def address
    if object.site
      if object.site.address
        h.link_to object.site.address, object.site
      else
        missing_address
      end
    else
      missing_site
    end
  rescue NoMethodError
    missing_address
  end

  private

    def missing_site
      logger.warn "Service : #{object.service_id} (#{object.id}) is missing a site."
      "[MISSING SITE]"
    end

    def missing_address
      logger.warn "Service : #{object.service_id} (#{object.id}) is missing an address."
      "[MISSING ADDRESS]"
    end

end
