class CallSummaryDecorator < ApplicationDecorator
  delegate_all

  attr_reader :totalCallCount
  attr_reader :totalDuration
  attr_reader :totalCost
  attr_reader :object

  ########################################################################
  # Remmember, the object is a Hash of an array of calls
  ########################################################################
  def initialize(object)
    if not (object.class == Hash and [Call, CallDecorator].include?(object.values.first.first.class))
        raise "Not a hash of arrray of calls"
        bject.values
    end
    @object = object

    # Unused for now
    @totalCallCount = @totalDuration = @totalCost = 0

  end

  def getSortedObject
    newHash = {}
    object.each {|bucket, callsForBucket|
        newKey = CallSummaryDecorator::bucketDisplayString(bucket)
        newHash[newKey] = callsForBucket
    }

    newHash = newHash.sort_by {|bucketName, calls|
        bucketName
    }.to_h

    return newHash
  end


  def self.bucketDisplayString(bucket)
    case bucket
    when Service
        return bucket.display_text.to_s
    when Site
        return bucket.name
    when String
        return bucket
    end
  end


end
