# coding: utf-8
class SiteDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def sla_stars
    star = "☆"
    slanum = object.support_service_level.to_i
    (star * slanum).html_safe
  end

  def to_s
    (self.site_number or '') + ' ' + self.name
  end

end
