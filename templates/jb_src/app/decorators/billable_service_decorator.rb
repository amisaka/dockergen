class BillableServiceDecorator < ApplicationDecorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def phone_number
    helpers.content_tag :span, class: 'bs-phone-number' do
      if object.phone_number.nil?
        missing_phone_number
      else
        object.phone_number
      end
    end
  rescue NoMethodError
    missing_phone_number
  end

  def description
    returnStr = object.product.present? ? object.product.description : ''
  end

  def identifier
    returnStr = "(BS #{billable_service.id} P #{object.product.try(:id)} S #{object.service.try(:id)})".html_safe
    returnStr = "<!-- #{returnStr} -->".html_safe if not Rails.env.development?
    return returnStr
  end

  def service_id
    object.asset.present? ? object.asset.display_text : ''
  end

  private

    def missing_phone_number
      #logger.warn "BillableService : #{object.name} (#{object.id}) is missing an associated phone."
      "-"
    end

end
