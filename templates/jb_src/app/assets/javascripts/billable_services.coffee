# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  # clients#index -- Updates form when a checkbox is clicked
  $('form.edit_billable_service > input[type=checkbox]').change ->
    $(this).parent().find('input[type=submit]').click()

  # clients#index -- Dislay modal when 'Add Service' is clicked
  $('#add-service-modal-fake-submit').click ->
    $('#add-service-modal').modal('toggle')
    $('form.new_billable_service').find('input[type=submit]').click()

  # clients#index -- Updates dislayed services on btns select tag change
  $('form.auto-submit').change -> $(this).closest("form").submit()