// this holds the source for clients related javascripts
var toggle_date_inputs;

$(document).ready(function() {
  $(".toggle_hidden").click(toggle_table_items);

  try{
    get_dashboard_page_status();
  } catch(e){}

  $('.human_validation_form input[type=checkbox]').change(function() {
    return $(this).parents('form').submit();
  });
  $('.needs_review_form input[type=checkbox]').change(function() {
    return $(this).parents('form').submit();
  });
  $('.invoicing_enabled_form input[type=checkbox]').change(function() {
    return $(this).parents('form').submit();
  });
  $('form.edit_client_line_of_business input[type=checkbox]').change(function() {
    return $(this).parents('form').submit();
  });
  // typeahead
  function selectize_renderer(item,escape){
	var html="";
	html+="<option value=\"" + escape(item.id) + "\">";
	html+="" + escape(item.btn) + "    (";
	html+=escape(item.name) + ") </option>";

	return html;
  };

  $('select#client_id.selectize.payments').selectize({
    valueField: 'id',
    labelField: 'name',
    searchField: 'btn',
    create: false,
    render: {
      item: selectize_renderer,
      option: selectize_renderer
    },

    load: function(query, callback) {
      if (query.length < 3) {
	return callback();
      }
      return $.ajax({
	url: '/clients/typeahead.json?SUPPORT=false&BTN=true&client_search[query]=' + encodeURIComponent(query),
	type: 'GET',
	error: function() {
	  return callback();
	},
	success: function(res) {
	  return callback(res.slice(0, 10));
	}
      });
    }
  });

  $('select#client_id.selectize').selectize({
    valueField: 'id',
    labelField: 'name',
    searchField: 'btn',
    create: false,
    render: {
      item: selectize_renderer,
      option: selectize_renderer
    },

    load: function(query, callback) {
      if (query.length < 3) {
	return callback();
      }
      return $.ajax({
	url: '/clients/typeahead.json?BTN=true&client_search[query]=' + encodeURIComponent(query),
	type: 'GET',
	error: function() {
	  return callback();
	},
	success: function(res) {
	  return callback(res.slice(0, 10));

	}
      });
    }
  });
  // clients#_form
  // run once when page loads
  toggle_date_inputs();
  // toggle date inputs when user changes select values
  return $('select#client_status').change(function() {
    return toggle_date_inputs();
  });



});

// Toggle the activation/termination date inputs on and off
toggle_date_inputs = function() {
  // console.log("");
  // console.log($('select#client_status').val());

  if ($('select#client_status').val() === 'pending_start' || $('select#client_status').val() === "active" ) {
    // console.log("pending_start");
    $('.activation-date').removeClass('hide');
    $('.activation-date input[type=text]').prop('disabled', false);
  } else {
    // console.log("pending_start_else");
    $('.activation-date').addClass('hide');
    $('.activation-date input[type=text]').prop('disabled', true);
  }

  if ($('select#client_status').val() === 'pending_closed' || $('select#client_status').val() === 'closed') {
    // console.log("pending_closed");
    $('.termination-date').removeClass('hide');
    $('.termination-date input[type=text]').prop('disabled', false);
  } else {
    // console.log("pending_close_else");
    $('.termination-date').addClass('hide');
    $('.termination-date input[type=text]').prop('disabled', true);
  }
};


function toggle_table_items(event){
    $(event.target).parents(".box").find('table.payment_history_table tr[hideme]').toggle();
    $(event.target).parents("div.toggle_hidden").find("div[hidden_label]").toggle()
    $(event.target).parents("div.toggle_hidden").find("div[showing_label]").toggle()
}


function box_change_cb(){
  try {
    var box = $(this).parents(".box").first();
    var my_id = box.attr('id');
    var showing = box.hasClass("collapsed-box");
    dashboard_collapse_status[window.location.pathname][my_id] = showing;
    localStorage.setItem("dashboard_collapse_status", JSON.stringify(dashboard_collapse_status));
  } catch(e){ }
}

var dashboard_collapse_status = null;
function get_dashboard_page_status(){
  dashboard_collapse_status = JSON.parse(localStorage.getItem("dashboard_collapse_status"));
  var key = window.location.pathname.toString();
  if(dashboard_collapse_status == null){
    dashboard_collapse_status = {}
  }
  if(dashboard_collapse_status[key] == undefined){
    dashboard_collapse_status[key] = {};
  }
  for(var k in dashboard_collapse_status[key]){
    if( dashboard_collapse_status[key][k] == false){
      try{ $("#"+k).find('[data-widget="collapse"]').click() }catch(e){}
    }
  }
  $("#billable_services_widget [data-widget='collapse']").click(box_change_cb);
  $("#services_widget [data-widget='collapse']").click(box_change_cb);

}
