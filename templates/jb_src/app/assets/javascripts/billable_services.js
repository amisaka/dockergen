
function highlight_form(selector){
  /* flashes a form item (looks at the parent) by adding 'has-success' then removing */
  var class_select = 'has-success';
  $($(selector).parent()).toggleClass(class_select).promise().done(function(e){
    setTimeout(function(){ $(e).toggleClass(class_select); }, 400);
  });
}

function highlight_element(selector){
  var class_select = 'highlight-success';
  $(selector).toggleClass(class_select).promise().done(function(e){
    setTimeout(function(){ $(e).toggleClass(class_select); }, 400);
  });
}

function recalculate_total_price(){
  //var unitprice = parseFloat($("#bs_unitprice_input").attr('placeholder').replace('$',''));
  var unitprice = parseFloat($("#billable_service_unitprice").val().replace('$',''));
  var qty = parseFloat($("#billable_service_qty").val());
  var total = unitprice * qty;
  $("#bs_totalprice_input").attr('placeholder',"$"+total.toFixed(2).toString() );
}

function recalculate_total_cost(){
  var unit_cost = parseFloat($("#bs_product_unit_cost_input").attr('placeholder').replace('$',''));
  var qty = parseFloat($("#billable_service_qty").val());
  var total = unit_cost * qty;
  $("#bs_total_cost_input").attr('placeholder', "$"+total.toFixed(2).toString());
}

function set_selectize_value(sel_id, value){
  $("#"+sel_id)[0].selectize.setValue(value,true);
  highlight_element($("#"+sel_id).next().children()[0] );
}

function set_product_edit_link(product_id){
  $(".product_edit_link_btn").attr("href", "/products/"+product_id+"/edit");
}

function item_add_func(value,item){

  var target_id=this.$input.attr('id');

  /* ********************************************/
  /* product name and description are connected */
  /* ********************************************/
  if(target_id == "billable_service_product_id"){
    set_selectize_value("product_description_select",value);
    set_product_edit_link(value);
    get_unit_cost(value);
  }

  if(target_id == "product_description_select"){
    set_selectize_value("billable_service_product_id",value);
    set_product_edit_link(value);
    get_unit_cost(value);
  }

  /* ********************************************/
  /* site number and site name are connected    */
  /* ********************************************/
  if(target_id == "billable_service_site_id"){
    set_selectize_value("_site_number",value);
  }

  if(target_id == "_site_number"){
    set_selectize_value("billable_service_site_id",value);
  }
}


var sel_desc = null;
var sel_name = null;
var sel_site_name = null;
var sel_site_number = null;
var sel_service = null;

$(function(){
  try{
    recalculate_total_price();
    recalculate_total_cost();


    sel_name = $("#billable_service_product_id").selectize({ create: true, sortField: 'text'});
    sel_name[0].selectize.on('change',item_add_func);

    sel_desc= $("#product_description_select").selectize({ create: true, sortField: 'text' });
    sel_desc[0].selectize.on('change',item_add_func);

    sel_site_name = $("#billable_service_site_id").selectize({ create: true, sortField: 'text'});
    sel_site_name[0].selectize.on('change',item_add_func);

    sel_site_number = $("#_site_number").selectize({ create: true, sortField: 'text'});
    sel_site_number[0].selectize.on('change',item_add_func);

    sel_service = $("#billable_service_asset_id").selectize({ create: true, sortField: 'text'});
    sel_service[0].selectize.on('change',item_add_func);

    setup_mutation_observer();

    $("#billable_service_qty").change(function(e) {
      recalculate_total_price();
      recalculate_total_cost();
    });

    $("#billable_service_unitprice").change(function(e){
      recalculate_total_price();
    });

    $("#billable_service_recurrence_type").change(set_reoccurence_defaults);

  } catch(e){
    // wrap this all in a try/catch - things break becuase rails includes this code where it shouldn't.
  }
});

function set_reoccurence_defaults(){
  if ( $("#billable_service_recurrence_type").val() == "Recurring" ){
    /* make sure user hasn't changed any of the defaults */
    // if ( $("#billable_service_charge_mode_id").val() == "" &&
    // 	 $("#billable_service_prorate_id_start").val() == "1" &&
    // 	 $("#billable_service_prorate_id_end").val() == "1" ){

      $("#billable_service_charge_mode_id").val(2);
      $("#billable_service_prorate_id_start").val(2);
      $("#billable_service_prorate_id_end").val(3);

    // }
  }

  if ( $("#billable_service_recurrence_type").val() == "One-time" ){
    /* make sure user hasn't changed any of the defaults */
    // if ( $("#billable_service_charge_mode_id").val() == "" &&
    // 	 $("#billable_service_prorate_id_start").val() == "1" &&
    // 	 $("#billable_service_prorate_id_end").val() == "1" ){

      $("#billable_service_charge_mode_id").val(2);
      $("#billable_service_prorate_id_start").val(3);
      $("#billable_service_prorate_id_end").val(3);

    // }
  }

}

function get_unit_cost(product_id){
  $.ajax({
    url: "/products/" + product_id.toString(),
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    success: function(data) {
      $("#bs_product_unit_cost_input").attr('placeholder', "$" +parseFloat(data.unit_cost).toFixed(2).toString());
      recalculate_total_cost();
    },
    error: function(xhr,exception,status) {
      // fields are going to be wrong now so set to error
      $("#bs_service_unitprice_input").attr('placeholder', 'err');
      $("#bs_totalprice_input").attr('placeholder', 'err');
    }
  });
}


function setup_mutation_observer(){
  var obs_targets = [
    "bs_product_unit_cost_input",
    "product_description_select",
    "bs_totalprice_input",
    "bs_total_cost_input"
  ];

  var observer = new MutationObserver(mutation_cb);
  for(var i =0; i< obs_targets.length; i++){
    observer.observe(document.getElementById(obs_targets[i]), { attributes: true, childList: true, characterData: true });
  }
}

var mutation_cb = function(mutationsList,mutationObserver) {
  for(var i=0; i<mutationsList.length;i++){
    var mutation = mutationsList[i];
    if (mutation.type == 'attributes') {
      highlight_form(mutation.target);
    }
  }
};
