# # this holds the source for clients related javascripts
# # NOTE: indentation matters in coffeescript
# $(document).ready ->
#   $('.human_validation_form input[type=checkbox]').change -> $(this).parents('form').submit()

#   $('.needs_review_form input[type=checkbox]').change -> $(this).parents('form').submit()

#   $('.invoicing_enabled_form input[type=checkbox]').change -> $(this).parents('form').submit()

#   $('form.edit_client_line_of_business input[type=checkbox]').change -> $(this).parents('form').submit()

#   # typeahead
#   $('select#client_id.selectize').selectize
#     valueField: 'id'
#     labelField: 'name'
#     searchField: 'btn'
#     create: false
#     render: { option: (item,escape) -> return "<option value=\""+escape(item.id)+"\">"+escape(item.name)+"<span class=\"pull-right\"> "+escape(item.btn)+"</span></option>"  }
#     load: (query, callback) ->
#       return callback() if (query.length < 3)
#       $.ajax(
#         url: '/clients/typeahead.json?BTN=true&client_search[query]=' + encodeURIComponent(query)
#         type: 'GET'
#         error: ->
#           callback();
#         success: (res) ->
#           callback(res.slice(0, 10))
#       )

#   # clients#_form
#   # run once when page loads
#   toggle_date_inputs()

#   # toggle date inputs when user changes select values
#   $('select#client_status').change -> toggle_date_inputs()

# # Toggle the activation/termination date inputs on and off
# toggle_date_inputs = ->
#   if $('select#client_status').val() == 'pending_start'
#     $('.activation-date').removeClass('hide')
#     $('.activation-date input[type=text]').prop('disabled', false)
#   else
#     $('.activation-date').addClass('hide')
#     $('.activation-date input[type=text]').prop('disabled', true)

#   if $('select#client_status').val() == 'pending_close'
#     $('.termination-date').removeClass('hide')
#     $('.termination-date input[type=text]').prop('disabled', false)
#   else
#     $('.termination-date').addClass('hide')
#     $('.termination-date input[type=text]').prop('disabled', true)
