json.extract! technical_contact, :id, :created_at, :updated_at
json.url technical_contact_url(technical_contact, format: :json)
