#!/bin/bash

shopt -s expand_aliases # Must set this option, else script will not expand aliases.
source .env

if [ -z "$1" ]
  then
    echo "No argument supplied"
    echo "usage: scripts/load_production_sql.sh montbeau.data.montbeau.20180402-190502.sql"
    exit 1
fi

sleepfor(){
    # give user a chance to cancel
    sleeptime=$1
    printf "\ttminus : "
    for i in $(seq $sleeptime -1 0); do
	sleep 1
	printf "\033[1D%s" "$i"
    done
    printf "\033[1D%s\n" "go!"
    sleep 1
}



printf "loading production data from $1\n"
sleepfor 5
PGPASSWORD=$DATABASE_PASSWORD  psql -h localhost -d $DATABASE_NAME -U $DATABASE_USERNAME < $1

printf "creating dev admin account\n"
sleepfor 5

PATH=$PATH:$HOME/.rvm/bin
rvm use 2.3.1
rvm 2.3.1 do rake jdev:create_dev_admin
echo "-----------------------"
echo "> user: admin@example.com"
echo "> pw:   test111"
echo ""
