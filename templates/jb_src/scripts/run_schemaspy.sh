#!/bin/bash

shopt -s expand_aliases # Must set this option, else script will not expand aliases.
source .env

SCHEMA_SPY_JAR=scripts/jars/schemaspy-6.0.0-rc2.jar
POSTGRESQL_JAR=scripts/jars/postgresql-42.2.1.jar
OUTPUT_DIR=doc/schemaspy_vis

alias java8='/usr/lib/jvm/java-8-openjdk-amd64/bin/java'

NOTFOUND=0
if [ ! -f $SCHEMA_SPY_JAR ]; then
    printf " jar: %-16s not found.\n" "schemaspy"
    NOTFOUND=1
fi


if [ ! -f $POSTGRESQL_JAR ]; then
    printf " jar: %-16s not found.\n" "postgresql"
    NOTFOUND=1
fi

if [ $NOTFOUND -eq 1 ]; then
    printf "\tsee: doc/db_diagram_output.txt\n"
    exit
fi

printf "running schemaspy with the following options...........\n"
printf "using:\n"
printf "\tSCHEMA_SPY_JAR    = %s\n" "$SCHEMA_SPY_JAR"
printf "\tPOSTGRESQL_JAR    = %s\n" "$POSTGRESQL_JAR"
printf "\tDATABASE_USERNAME = %s\n" "$DATABASE_USERNAME"
printf "\tDATABASE_NAME     = %s\n" "$DATABASE_NAME"
printf "\tDATABASE_HOST     = %s\n" "$DATABASE_HOST"

printf "\n"
printf "outputing to: $OUTPUT_DIR\n"


if [ ! -d "$OUTPUT_DIR" ]; then
    mkdir $OUTPUT_DIR
fi

runschemaspy(){
    java8 -jar $SCHEMA_SPY_JAR -t pgsql -u $DATABASE_USERNAME -p $DATABASE_PASSWORD -o $OUTPUT_DIR -db $DATABASE_NAME -host $DATABASE_HOST -dp $POSTGRESQL_JAR -hq -rails
    printf "\n\n"
    printf "============================\n"
    printf "results:\n"
    printf "\t$OUTPUT_DIR\n"
}

read -r -p "Continue? [y/N] " response
case "$response" in
    [yY][eE][sS]|[yY])
	runschemaspy
        ;;
    *)
	printf "entered 'no', doing nothing\n"
        exit
        ;;
esac

#java8 -jar $SCHEMA_SPY_JAR -t pgsql -u $DATABASE_USERNAME -p $DATABASE_PASSWORD -o $OUTPUT_DIR -db $DATABASE_NAME -host $DATABASE_HOST -dp $POSTGRESQL_JAR -hq -rails
