// Somewhat usefull documentation on PDF generation: https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md
const puppeteer = require('puppeteer');
const jsonQuery = require('json-query');
const mkdirp    = require('mkdirp');
const os        = require('os');

if (process.env.SGE_USERNAME == null || process.env.SGE_PASSWORD == null || process.env.SGE_HOST == null) {
    console.log ("SGE_USERNAME, SGE_PASSWORD and SGE_HOST must be set on the command line or in bash ENV")
    process.exit(1)
}

const user = process.env.SGE_USERNAME;
const pass = process.env.SGE_PASSWORD;
const host = process.env.SGE_HOST;
const dir_output = '/home/montbeau/tmp/invoices_v02/'
const hostname = 'http://' + host + '/'
const headers = new Map();


if (host == 'sge.credil.org') {
   if (os.hostname != 'montbeau') {
    console.log("Seems like you are pointing this script at production while not on dev. Exiting")
    process.exit(1)
   } else {
       console.log("WARNING: pointing script to prod...")
   }

}

console.log("Starting " + new Date())

headers.set(
  'Authorization',
  `Basic ${new Buffer(`${user}:${pass}`).toString('base64')}`
);

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setExtraHTTPHeaders(headers);
  page.setDefaultNavigationTimeout(10000)
  console.log(`navigating to ${hostname}/users/sign_in`);
  await page.goto(hostname+'/users/sign_in');
  await page.type('#user_email', user)
  await page.type('#user_password', pass)
  await page.click('[name="commit"]')
  console.log("logging in....");

  // Example: https://github.com/GoogleChrome/puppeteer/issues/2029#issuecomment-367541549
  //await page.waitForNavigation({waitUntil: 'domcontentloaded'});
  await page.waitForNavigation({waitUntil: 'networkidle2'});

  // Detect if we have succesfull login or not
  console.log("Detecting login....");
  const loginResult = await page.evaluate(() => {
          alertNode = document.querySelector('.login-box-msg');

          if (alertNode == null) {
            return ''
            } else {
                let alertText = alertNode.innerText;
                return alertText
                }
          })
  console.log(loginResult)

  if (loginResult != "") {
    console.log("Authentication not succesfull. Message: " + loginResult)
    process.exit(1)
  }

  page.setDefaultNavigationTimeout(5*60*60*1000)
  path=hostname+'/invoices.json';
  console.log(`Going to ${path}` + ' on host ' + host + ' for invoice list. This may take a while....');
  await page.goto(path)


  var bodyHandle = await page.$('body')
  jsonData = await page.evaluate(body => body.innerText, bodyHandle)

  // Parsing JSON: https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/JSON/parse
  objs = await JSON.parse(jsonData)


  // Using for as it is faster: https://coderwall.com/p/kvzbpa/don-t-use-array-foreach-use-for-instead
  for (var i=0, len = objs.length; i<len; i++) {

      var invoice_cycle     = objs[i].invoice_cycle
      var yearmonth         = objs[i].yearmonth
      var btn               = objs[i].btn
      var type              = objs[i].ressource_type
      var site_number       = objs[i].site_number
      var invoice_dir_path  = dir_output + yearmonth + '/' + invoice_cycle + '/'

      var invoice_file_name = btn + '-' + yearmonth
      if (type == 'site') {
          invoice_file_name = invoice_file_name + '-' + site_number
      }
      var invoice_file_name = invoice_file_name + '.pdf'

      mkdirp.sync(invoice_dir_path)
      var pdf_filename =  invoice_dir_path + invoice_file_name



      var invId = objs[i].id

      console.log('Going to invoice ' + invId + ' (' + (i+1) + ' of ' + objs.length + ')')
      var invoice_path = hostname + '/invoices/' + invId
      await page.goto(invoice_path)

      console.log('Generating invoice ' + pdf_filename)
      //await page.pdf({path: pdf_filename, format: 'Letter'}); // https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagepdfoptions
      await page.pdf({path: pdf_filename,  displayHeaderFooter: false, margin: { left: '0.75in', top: '0.75in', right: '0.75in', bottom: '0.75in' } }); // https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagepdfoptions

      //For PNG:
      //await page.screenshot({path: screenshot_filename, fullPage: true});
  }

  bodyHandle.dispose()
  browser.close();
  console.log("Done " + new Date());

})()
