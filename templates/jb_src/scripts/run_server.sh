#!/bin/bash

shopt -s expand_aliases # Must set this option, else script will not expand aliases.
source .env

if [ "$SENTRY_ENV" != "development" ]; then
	echo -e "\e[1mERR: \e[0mThis script is intended to be run on \e[36mDEVELOPMENT \e[4m\e[39mNOT\e[0m \e[31mPRODUCTION\e[39m - exiting!"
	if [ "$FORCE_SERVER_RUN" != "1" ]; then
		exit 1
	fi
fi

if [ -z "$RAILS_SERVER_ADDR" ]; then
	RAILS_SERVER_ADDR="[$(ip -6 addr | grep 2620 | sed -e 's/^[[:space:]]*//' | cut -f2 -d ' ' | cut -f1 -d '/')]"
fi

if [ -z "$RVM_VERSION" ]; then
	RVM_VERSION="2.3.1"
fi

for i in {255..232} ; do echo -en "\e[38;5;${i}m####\e[0" ; done 
echo -e "\e[0m"
printf "Using running on  : %-32s - set RAILS_SERVER_ADDR to change.\n" $RAILS_SERVER_ADDR
printf "Using RVM_VERSION : %-32s - set in .env to change.\n" $RVM_VERSION
for i in {232..255} ; do echo -en "\e[38;5;${i}m####\e[0" ; done ; echo
echo -e "\e[0m"

DATABASE_PORT=$DATABASE_PORT rvm $RVM_VERSION do bin/rails server -b $RAILS_SERVER_ADDR
