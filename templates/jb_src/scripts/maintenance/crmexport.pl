#!/usr/bin/perl -w

use utf8;
use strict;
use SG::SQL qw(do_connect);
use Data::Dumper;
use Text::CSV;

binmode(STDOUT, ":utf8");
my $dbh = do_connect();

my @columns = qw(btn client_name service_id extension supplier coverage_name coverage_id rate_plan_id code resultant_rate_plan_id start_date termination_date);
my $sql = "select 
	btns.billingtelephonenumber as btn,
	crm_accounts.name as client_name,
	services.service_id, services.extension,
	did_suppliers.name as supplier,
	coverages.name_french as coverage_name,
	clients.coverage_id, services.rate_plan_id,
	rate_plans.code, services.resultant_rate_plan_id,
	concat(btns.billingtelephonenumber, '-', services.service_id, '-', services.id) as key,
	start_date,
	termination_date
	from services 
	join btns on services.btn_id = btns.id
	join clients on btns.client_id = clients.id
	join coverages on clients.coverage_id = coverages.id
	join crm_accounts on clients.crm_account_id = crm_accounts.id
	left join did_suppliers on services.supplier_id = did_suppliers.id
	left join rate_plans on services.rate_plan_id = rate_plans.id";
my $data = $dbh->selectall_hashref($sql, 'key');
$dbh->disconnect;

open (my $fh, ">:utf8", "/tmp/status.csv") || die;
my $csv = new Text::CSV ({sep_char => '|'});
do_export();
close ($fh);

# ---------- subroutines

sub do_export {
	print_line (@columns);

	foreach my $btn (sort keys %$data) {
		my $record = $$data{$btn};
		my @values;
		foreach my $column (@columns) {
			push (@values, $$record{$column});
		}
		print_line (@values);	
	}
}

sub print_line {
	my (@columns) = @_;

	my $status = $csv->combine(@columns);
	print $fh $csv->string(), "\n";

	return $status;
}
