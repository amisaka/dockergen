package SG::SQL;

# --------------------------------------------------------------------------

use strict;
use DBI;
use Exporter;

use parent qw (Exporter);
our @EXPORT_OK = qw (do_connect);

# --------------------------------------------------------------------------
# Subroutines

sub do_connect {
	my ($config) = @_;

	if (! $config) {
		$config = "/home/montbeau/montbeau/.env";
	}

	my %config;

	open (my $fh, "<:utf8", $config) || die "$0: cannot open database configuration file $config: $!\n";
	while (my $line = <$fh>) {
		chomp $line;
		my ($key, $value) = split (/=/, $line, 2);
		#print "[$key]=][$value]\n";
		$config{$key} = $value;
	}
	close ($fh);

	my $dbname = $config{DATABASE_NAME};
	my $username = $config{DATABASE_USERNAME};
	my $password = $config{DATABASE_PASSWORD};
	my $host = $config{DATABASE_HOST} || "localhost";
	my $port = $config{DATABASE_PORT} || 5432;

	my $dbh = DBI -> connect("dbi:Pg:dbname=$dbname;host=$host;port=$port",
	      $username,
	      $password,
	      {AutoCommit => 0, RaiseError => 1}
	) or die $DBI::errstr;
}
