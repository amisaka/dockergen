package SG::Report;

# --------------------------------------------------------------------------

use strict;
use DBI;
use Exporter;

use parent qw (Exporter);
our @EXPORT_OK = qw (format_entry format_heading generate_section client_list run_data_report);

# --------------------------------------------------------------------------
# Subroutines

sub client_list {
	my ($dbh) = @_;

	my $sqlx = "select clients.id as client_id, crm_accounts.name, billingtelephonenumber as btn, invoice_cycle, clients.status != 4 as client_active, clients.status
		from clients
		left join crm_accounts on crm_accounts.id = clients.crm_account_id
		left join btns on btns.client_id = clients.id
		left join statement_type_clients on statement_type_clients.client_id = clients.id
		left join statement_types on statement_type_id = statement_types.id
		left join invoice_templates on invoice_template_id = invoice_templates.id
		where clients.deleted = false";

	my $clientsx = $dbh->selectall_hashref($sqlx, 'client_id');

	return $clientsx;
}

sub format_entry {
	my ($data, %options) = @_;

	my @lines;

	if ($options{client}) {
		my $invoice_cycle = $options{invoice_cycle};
		my $client_active = $options{client_active};
		no warnings;
		push (@lines, sprintf("%s%s %6d: %12s: %s", $invoice_cycle, $client_active, $$data{id}, $$data{btn}, $$data{name}));
	} else {
		if (exists $$data{id} && defined $$data{id}) {
			no warnings;
			push (@lines, sprintf("%7d: %s", $$data{id}, "!name_missing!"));
		}

		if (exists $$data{name} && defined $$data{name}) {
			no warnings;
			push (@lines, sprintf("%7d: %s", undef, $$data{name}));
		}
	}

	foreach my $key (sort keys %$data) {
		next if ($key eq 'id');
		next if ($key eq 'name');
		next if ($key eq 'btn');

		my $value = $$data{$key};
		next if (! defined $value);

		$value =~ s/\r\n/!/sg;
		$value =~ s/\n/!/sg;

		if ($key eq 'updated_at') {
			# remove seconds, microseconds
			$value =~ s/:\d\d\..*$//;
		}
		push (@lines, "$key: $value");
	}
	
	return @lines;
}

sub format_heading {
	my (%options) = @_;

	my $name = $options{name};
	my $count = $options{count};
	my $track = $options{track};
	my $sql = $options{sql};
	my $tofix = $options{tofix};
	my $limit = $options{limit};
	my $client_filter_type = $options{client_filter_type};
	my $client_filter_active = $options{client_filter_active};

	my $string = '';
	$string .= "$name\n";
	$string .= "-" x length ($name) . "\n";
	$string .= "Record count: ". scalar ($count) . "\n";
	$string .= "Tracking: $track\n" if ($track);
	$string .= "SQL: $sql\n" if ($sql);
	$string .= "Client Filter Type: $client_filter_type\n" if ($client_filter_type);
	$string .= "Client Filter Active: $client_filter_active\n" if ($client_filter_active);
	$string .= "To Fix: $tofix\n" if ($tofix);
	$string .= "Warning: limit of $limit exceded; remaining records supressed\n" if ($limit && $limit < $count);
	$string .= "\n";

	return $string;
}

sub generate_section {
	my (%options) = @_;

	my $name = $options{name};
	my $data = $options{data};
	my $track = $options{track};
	my $sql = $options{sql};
	my $tofix = $options{tofix};
	my $limit = $options{limit};
	my $count = scalar (@$data);
	my $suppress_if_empty = $options{suppress_if_empty};
	my $client_filter_type = $options{client_filter_type};
	my $client_filter_active = $options{client_filter_active};

	return '' if (! $count && $suppress_if_empty);

	my $string = '';

	if ($limit && $count > $limit) {
		# truncate list
		$#$data = $limit;
	}
	$string .= format_heading (name => $name, track => $track, sql => $sql, tofix => $tofix, count => $count, limit => $limit, client_filter_type => $client_filter_type, client_filter_active => $client_filter_active);
	foreach my $line (@$data) {
		$string .= "$line\n";
	}
	$string .= "\n";

	return $string;
}

sub run_data_report {
	my (%options) = @_;

	my $dbh = $options{dbh};
	my $clientsx = $options{clientsx};
	my $name = $options{name};
	my $track = $options{track};
	my $sql = $options{sql};
	my $tofix = $options{tofix};
	my $client = $options{client};
	my $client_filter_type = $options{client_filter_type};
	my $client_filter_active = $options{client_filter_active};

	#print ">> $sql\n";
	my $records = $dbh->selectall_arrayref($sql, { Slice => {} });

#	return if (! @$records);

	my @data;
	foreach my $record (@$records) {
		if ($client) {
			$$record{name} = $$clientsx{$$record{id}}{name};
			$$record{btn} = $$clientsx{$$record{id}}{btn};
		}
	}

	if ($options{sort_btn}) {
		@$records = sort {$$a{btn} cmp $$b{btn}} @$records;
	}

	foreach my $record (@$records) {
		# ----- filter type

		my $invoice_cycle = $$clientsx{$$record{id}}{invoice_cycle};
		my $cycle = 'X';
		if ((defined $invoice_cycle) && ($invoice_cycle eq 'Support')) {
			$cycle = 'S';
		} elsif (defined $invoice_cycle) {
			$cycle = 'I';
		}

		next if ($client_filter_type && $client_filter_type ne $cycle);

		# ----- filter active

		my $active = $$clientsx{$$record{id}}{client_active};
		my $client_active = 'A';
		$client_active = 'I' if ($active == 0);
		next if ($client_filter_active && $client_filter_active eq 'A' && $active == 0);
		next if ($client_filter_active && $client_filter_active eq 'I' && $active == 1);

		# ----- go

		push (@data, join (" | ", format_entry ($record, client => $client, invoice_cycle => $cycle, client_active => $client_active)));
	}

	print generate_section (name => $name, track => $track, sql => $sql, tofix => $tofix, client_filter_type => $client_filter_type, client_filter_active => $client_filter_active, data => \@data);
}
