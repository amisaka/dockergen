#!/usr/bin/perl -w

use utf8;
use strict;
use SG::SQL qw(do_connect);
use Data::Dumper;
use SG::Report qw(client_list);

binmode(STDOUT, ":utf8");
my $dbh = do_connect();

my $clients = client_list ($dbh);

update_voip_did_ani();
$dbh->commit;

update_billings();
$dbh->commit;

update_lobs();
$dbh->commit;

#update_charges_siteid();  # to be completed
#$dbh->commit;

update_charges_active();
$dbh->commit;

update_services_active();
$dbh->commit;

update_annual();
$dbh->commit;

$dbh->disconnect;

# ---------- subroutines

sub update_voip_did_ani {
	my $sql = 'update services set phone_service_type_id = 5 where phone_service_type_id = 1 and service_id ~ \'^\d\d\d555\d\d\d\d$\'';;
	$dbh->do($sql);
}

sub update_billings {
	foreach my $client_id (sort {$a <=> $b } keys %$clients) {
		my $client_active = $$clients{$client_id}{client_active};
		my $invoice_cycle = $$clients{$client_id}{invoice_cycle};

#		next if ($client_active == 0);
		next if ($invoice_cycle eq 'Support');

#		print ">> $client_id: $client_active, $invoice_cycle\n";

		# todo: should check on posted_date!!!!

		my %data;
		my $charges = $dbh->selectrow_hashref("select * from payments where client_id = $client_id and transaction_type_id = 5 order by date desc limit 1");
		$data{last_usage_date} = $$charges{date};
		$data{last_statement_date} = $$charges{date};
		$data{last_statement_amount} = $$charges{amount};
		my $payments = $dbh->selectrow_hashref("select * from payments where client_id = $client_id and transaction_type_id = 4 order by date desc limit 1");
		$data{last_payment_date} = $$payments{date};
		$data{last_payment_amount} = ($$payments{amount} || 0) * -1;
		my $balance = $dbh->selectrow_hashref("select sum(amount) as balance from payments where client_id = $client_id");
		$data{balance} = $$balance{balance};

		my @params;
		foreach my $key (keys %data) {
			push (@params, "$key = " . $dbh->quote($data{$key}));
		}

		my $sql = "update billings set " . join (',', @params) . " where client_id = $client_id";
		#print ">> $sql\n";
		$dbh->do($sql);

#		print Dumper (\%data);
	}
}

sub update_lobs {
	my $old_sql = "select * from client_line_of_businesses";
	my $old_lobs = $dbh->selectall_hashref($old_sql, ['client_id', 'line_of_business_id']);

	my $new_sql = "select distinct client_id, line_of_businesses_id from services join sites on services.site_id = sites.id where not line_of_businesses_id is NULL and (services.termination_date > now() or services.termination_date is NULL) and services.deleted = false";
	my $new_lobs = $dbh->selectall_hashref($new_sql, ['client_id', 'line_of_businesses_id']);

	foreach my $client_id (sort {$a <=> $b } keys %$clients) {
		my $client_active = $$clients{$client_id}{client_active};
		my $invoice_cycle = $$clients{$client_id}{invoice_cycle};

#		next if ($client_active == 0);
		next if ($invoice_cycle eq 'Support');

#		print ">> $client_id: $client_active, $invoice_cycle\n";

		foreach my $lob_id (keys %{$$new_lobs{$client_id}}) {
			next if (exists $$old_lobs{$client_id}{$lob_id});
			print ">> add record: $client_id, $lob_id\n";
			$dbh->do("insert into client_line_of_businesses (client_id, line_of_business_id, deleted, created_at, updated_at, activated, activated_at) values ($client_id, $lob_id, false, now(), now(), true, now())");
		}

		foreach my $lob_id (keys %{$$old_lobs{$client_id}}) {
			next if (exists $$new_lobs{$client_id}{$lob_id});
			print ">> delete record: $client_id, $lob_id\n";
			$dbh->do("delete from client_line_of_businesses where client_id = $client_id and line_of_business_id = $lob_id");
		}
	}
}

sub update_charges_siteid {
	my $services_sql = "select id, site_id from services";
	my $services = $dbh->selectall_hashref($services_sql, 'id');

	my $charges_sql = "select id, site_id, asset_id from billable_services where not asset_id is null";
	my $charges = $dbh->selectall_hashref($charges_sql, 'id');

	foreach my $id (sort keys %$charges) {
		my $charge_site_id = $$charges{$id}{site_id};
		my $charge_asset_id = $$charges{$id}{asset_id};
		my $service = $$services{$charge_asset_id};
		my $service_site_id = $$service{site_id};
		my $xcharge_site_id = $charge_site_id || 'NULL';
		my $xservice_site_id = $service_site_id || 'NULL';

		if (! $$service{id}) {
			print ">> cannot find in services: charge id=<$id> site_id=<$xcharge_site_id> asset_id=<$charge_asset_id>\n";	
		} elsif (($charge_site_id || 0) != ($service_site_id || 0)) {
			print ">> charge id=<$id> site_id=<$xcharge_site_id> asset_id=<$charge_asset_id> where as services_site_id=<$xservice_site_id>\n";
		}
	}
}

sub update_charges_active {
	$dbh->do("update billable_services set activated = false, billing_period_ended_at = null where recurrence_type = 1");
	$dbh->do("update billable_services set deactivated_at = now() where recurrence_type = 1 and deactivated_at is null");
	$dbh->do("update billable_services set activated_at = billing_period_started_at where recurrence_type = 1 and activated_at is null");
	$dbh->do("update billable_services set activated = true, activated_at = billing_period_started_at, deactivated_at = NULL where (billing_period_ended_at is NULL or billing_period_ended_at > now()) and activated = false and recurrence_type = 2");
	$dbh->do("update billable_services set activated = false, deactivated_at = billing_period_ended_at where billing_period_ended_at < now() and activated = true");
}

sub update_services_active {
	$dbh->do("update services set activated = true where (termination_date is NULL or termination_date > now()) and activated = false");
	$dbh->do("update services set activated = false where termination_date < now() and activated = true");
}

sub update_annual {
	my $sql = "select billable_services.id from billable_services join products on products.id = product_id where (products.name = 'JCare Plus' or products.name like 'Jcare-VA%' or products.name like 'Kakapomaint&supp S-F') and billing_period != 2";
	my $ids = $dbh->selectall_hashref($sql, 'id');

	foreach my $charge_id (sort {$a <=> $b} keys %$ids) {
		my $update_sql = "update billable_services set billing_period = 2 where id = $charge_id";
		$dbh->do($update_sql);
	}
}
