#!/usr/bin/perl -w

use utf8;
use strict;
use SG::SQL qw(do_connect);
use SG::Report qw(format_entry generate_section client_list run_data_report);
use Data::Dumper;

binmode(STDOUT, ":utf8");
my $dbh = do_connect();
my $clientsx = client_list ($dbh);

error_data();

# ---------- subroutines

sub run_data {
        my (%options) = @_;

	run_data_report (dbh => $dbh, clientsx => $clientsx, %options);
}

sub error_data {
	my $common_no = "crm_account_id = crm_accounts.id and clients.deleted = false";
	my $common = "$common_no order by clients.id";
	my $commonx = "clients.deleted = false order by clients.id";
	my $commonx_no = "clients.deleted = false";
	my $sitescommon = "sites.activated = true and sites.deleted = false";
	my $services_is_active = "(termination_date is NULL or termination_date > now())";
	my $charges_is_active = "(billing_period_ended_at is NULL or billing_period_ended_at > now())";

#	run_data (
#		client => 1,
#		name => "TEMP: payments on 2018-04-10",
#		sql => "select clients.id, date(payments.date) as date, payments.amount, payments.transaction_type_id, payments.description
#from clients, crm_accounts, payments
#where payments.client_id = clients.id and payments.date = '2018-04-10' and $common"
#	);

	run_data (
		client => 1,
		name => "Matthew notes",
		sql => "select clients.id, note, updated_at
from clients
where note ~ 'Matthew'
and $commonx"
	);

	run_data (
		client => 1,
		name => "non-blank notes",
		sql => "select clients.id, note, updated_at
from clients
where note != ''
and note !~ '^!'
and note !~ 'Matthew'
and $commonx"
	);

# 2018-04-04 plan is to delete the extranious client_name column
#	run_data (
#		client => 1,
#		name => "not-matching client name",
#		sql => "select clients.id, crm_accounts.name, clients.name as client_name, updated_at
#from clients, crm_accounts
#where (crm_accounts.name != clients.name or clients.name is NULL)
#and $common"
#	);


	# ----- database errors

	run_data (
		client => 1,
		name => "database inconsistency: crm_accounts is missing",
		sql => "select clients.id
from clients
left join crm_accounts on crm_accounts.id = clients.crm_account_id
where crm_accounts.id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "database inconsistency: billings is missing",
		sql => "select clients.id
from clients
left join billings on billings.client_id = clients.id
where billings.id is NULL
and $commonx",
		tofix => "insert into billings (client_id, balance, activated_at) (select id, 0, created_at
from clients
where id = ?); update note to miranda to review"
	);

	run_data (
		client => 1,
		client_filter_type => 'S',
		name => "database inconsistency: billings is extra",
		sql => "select clients.id
from clients
left join billings on billings.client_id = clients.id
where not billings.id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "database inconsistency: btns is missing",
		sql => "select clients.id
from clients
left join btns on btns.client_id = clients.id
where btns.id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "database inconsistency: sites is missing",
		sql => "select clients.id
from clients
left join sites on sites.client_id = clients.id
and sites.deleted = false
where sites.id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "database inconsistency: payments is missing",
		sql => "select clients.id
from clients
left join payments on payments.client_id = clients.id
and payments.deleted = false
where payments.id is NULL
and $commonx",
		tofix => "insert into payments (client_id, transaction_type_id, balance, created_at, updated_at, posted_date, gl_account_id) values (10833, 1, 0, now(), now(), now(), 1)"
	);

	run_data (
		client => 1,
		name => "database inconsistency: billable services is messed up (incorrect site_id)",
		sql => "select clients.id, sites.id as site_id, sites.name as site_name, billable_services.id as billable_services_id, sites.client_id as sites_client_id, billable_services.client_id as billable_services_client_id
from clients 
join sites on sites.client_id = clients.id 
join billable_services on sites.id = billable_services.site_id
where sites.client_id != billable_services.client_id
and $commonx"
	);

	run_data (
		client => 1,
		name => "database inconsistency: services is messed up (incorrect site_id)",
		sql => "select clients.id, sites.id as site_id, sites.name as site_name, services.id as services_id, btns.id as btn_id, sites.client_id as sites_client_id, services.btn_id as services_btn_id
from clients
join btns on btns.client_id = clients.id
join sites on sites.client_id = clients.id 
join services on sites.id = services.site_id
where btns.id != services.btn_id
and $commonx"
	);

	run_data (
		client => 1,
		name => "database inconsistency: billable_services.asset_id links to a different client than billable_services.client_id",
		sql => "select billable_services.client_id as billable_services_client_id, btns.client_id as id, service_id, charge_pid
from billable_services
join services on billable_services.asset_id = services.id 
join btns on btns.id = services.btn_id 
where billable_services.client_id != btns.client_id"
	);

#	run_data (
#		client => 1,
#		name => "database inconsistency: department_pid mismatch between services and billable services",
#		sql => "select services.department_pid as service_department_pid, billable_services.department_pid as billable_services_department_pid, service_id, charge_pid, billable_services.client_id as id
#from billable_services
#join services on services.id = asset_id
#where (not services.department_pid is null
#and billable_services.department_pid is null)
#or (services.department_pid is null and not billable_services.department_pid is null)
#or (services.department_pid != billable_services.department_pid)"
#	);
#
#	run_data (
#		client => 1,
#		name => "database inconsistency: department_pid mismatch between services and billable services (billable_services.site_id is null)",
#		sql => "select services.department_pid as service_department_pid, billable_services.department_pid as billable_services_department_pid, service_id, charge_pid, billable_services.client_id as id
#from billable_services
#join services on services.id = asset_id
#where (not services.department_pid is null
#and billable_services.department_pid is null)"
#	);
#
#	run_data (
#		client => 1,
#		name => "database inconsistency: department_pid mismatch between services and billable services (services.site_id is null)",
#		sql => "select services.department_pid as service_department_pid, billable_services.department_pid as billable_services_department_pid, service_id, charge_pid, billable_services.client_id as id
#from billable_services
#join services on services.id = asset_id
#where services.department_pid is null
#and not billable_services.department_pid is null"
#	);

	run_data (
		client => 1,
		name => "database inconsistency: site_id mismatch between services and billable services (site_id not match)",
		sql => "select services.site_id as service_site_id, billable_services.site_id as billable_services_site_id, service_id, charge_pid, billable_services.client_id as id
from billable_services
join services on services.id = asset_id
where services.site_id != billable_services.site_id"
	);

	run_data (
		client => 1,
		name => "database inconsistency: asset_id is missing from billable services",
		sql => "select client_id as id, billable_services.id as billable_services_id, asset_id
from billable_services
left join services on services.id = billable_services.asset_id
where services.id is null
and not billable_services.asset_id is null"
	);

	# ----- client reports

	run_data (
		client => 1,
		sort_btn => 1,
		name => "national accounts",
		sql => "select clients.id
from clients
where coverage_id = 2
and $commonx"
	);

	run_data (
		client => 1,
		sort_btn => 1,
		name => "statement_type = Print",
		sql => "select clients.id
from statement_type_clients, invoice_templates, btns, statement_types, clients
where clients.id = btns.client_id
and invoice_template_id = invoice_templates.id
and clients.id = statement_type_clients.client_id
and statement_type_id = statement_types.id
and statement_types.description like '%Print%'
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		client_filter_active => 'A',
		name => "missing email address where statement_type != Print",
		sql => "select distinct clients.id
from statement_type_clients, invoice_templates, btns, statement_types, clients
left join email_contacts on email_type = 'Billing'
and clients.id = email_contacts.client_id
where clients.id = btns.client_id
and invoice_template_id = invoice_templates.id
and clients.id = statement_type_clients.client_id
and statement_type_id = statement_types.id
and statement_types.description not like '%Print%'
and email is null and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
			client_filter_active => 'A',
		name => "statement_type missing",
		sql => "select clients.id
from clients left join statement_type_clients on statement_type_clients.client_id = clients.id
where statement_type_clients.client_id is NULL
and $commonx"
	);

	foreach my $x (qw (default_origination coverage_id)) {
		run_data (
			client => 1,
			client_filter_type => 'I',
			client_filter_active => 'A',
			name => "Broadworks missing vDIDs with missing $x; Natasha to fix as time permits",
			sql => "select clients.id, service_id, did_suppliers.name as supplier_name
from btns, clients, services, did_suppliers
where clients.id = btns.client_id
and service_id ~ '^\\d\\d\\d555\\d\\d\\d\\d\$'
and services.$x is NULL
and services.btn_id = btns.id
and did_suppliers.id = supplier_id
and supplier_id in (3, 19)
and $services_is_active is NULL
and $commonx"
		);
	}

	foreach my $x (qw (default_destination)) {
		run_data (
			client => 1,
			client_filter_type => 'I',
			client_filter_active => 'A',
			name => "Broadworks missing vDIDs with missing $x; Natasha to fix as time permits",
			sql => "select clients.id, service_id, did_suppliers.name as supplier_name
from btns, clients, services, did_suppliers
where clients.id = btns.client_id
and service_id ~ '^(800|899|888|877|866|855|844|833|822|880|881|882|883|884|885|886|887|889)'
and services.$x is NULL
and services.btn_id = btns.id
and did_suppliers.id = supplier_id
and supplier_id in (3, 19)
and $services_is_active
and $commonx"
		);
	}

	run_data (
		client => 1,
		client_filter_active => 'A',
		name => "Service not associated to a phone_service_type_id (service is active)", 
		sql => "select clients.id, services.id as service_id_int, service_id
from services
join btns on btns.id = services.btn_id
join clients on btns.client_id = clients.id
where phone_service_type_id is NULL
and $services_is_active
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_active => 'A',
		name => "Service not associated to a phone_service_type_id (service is not active)", 
		sql => "select clients.id, services.id as service_id_int, service_id
from services
join btns on btns.id = services.btn_id
join clients on btns.client_id = clients.id
where phone_service_type_id is NULL
and not $services_is_active
and $commonx"
	);

	run_data (
		client => 1,
		name => "clients having the same service active more than once",
		sql => "select client_id as id, service_id, count(*) as count 
from services
join btns on btns.id = services.btn_id
join clients on btns.client_id = clients.id
where $services_is_active
and $commonx_no
group by service_id, client_id having count(*) > 1
order by client_id"
	);

#	run_data (
#		client => 1,
#		name => "clients having more than one site",
#		sql => "select clients.id, count(*) as count
#from clients
#left join sites on sites.client_id = clients.id group by clients.id having count(*) > 1"
#	);


	run_data (
		client => 1,
		name => "Services with no LOB",
		sql => "select clients.id, service_id
from btns, clients, services
where clients.id = btns.client_id
and services.btn_id = btns.id
and line_of_businesses_id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		track => 'https://code.credil.org/issues/13402',
		name => "VOIP DIDs flagged as ANI Service type",
		sql => "select clients.id, service_id
from btns, clients, services
where clients.id = btns.client_id
and phone_service_type_id = 1
and service_id ~ '^\\d\\d\\d555\\d\\d\\d\\d\$'
and services.btn_id = btns.id
and $commonx",
		tofix => "update services set phone_service_type_id = 5
where phone_service_type_id = 1
and service_id ~ '^\\d\\d\\d555\\d\\d\\d\\d\$'"
	);

	run_data (
		client => 1,
		name => "Jcare Annual not flagged as annual (automated fix, should be empty)",
		sql => "select client_id as id, product_id, products.name as product_name, products.description as product_description
from billable_services
join products on products.id = product_id
where (products.name = 'JCare Plus' or products.name like 'Jcare-VA%' or products.name like 'Kakapomaint&supp S-F')
and billing_period != 2"
	);


# not a valid check, disabled 2018-06-04
#	run_data (
#		client => 1,
#		client_filter_type => 'I',
#		client_filter_active => 'A',
#		name => "BTN not in service list",
#		sql => "select clients.id, billingtelephonenumber, service_id
#from btns
#left join services on btns.id = services.btn_id
#and billingtelephonenumber = service_id
#join clients on clients.id = btns.client_id
#where btns.deleted = false
#and service_id is NULL
#and $commonx"
#	);

	run_data (
		client => 1,
		name => "Charges active on service that is not active",
		sql => "select client_id as id, services.service_id as service_id, services.id as service_int_id, billable_services.id as billable_service_int_id, termination_date as services_end_date, billing_period_ended_at as billable_services_end_at
from services
join billable_services on services.id = billable_services.asset_id
join clients on billable_services.client_id = clients.id
where not $services_is_active
and $charges_is_active
and $commonx"
	);

	# ----

	run_data (
		client => 1,
		name => "start date or billing start date is missing",
		sql => "select client_id as id, activated_at, billing_start_date
from billings
where (billing_start_date is null or activated_at is null)"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings unknown address1",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where (address1 is NULL or address1 = 'unknown' or address1 = '')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings unknown address2",
		sql => "select clients.id, buildings.id as _building_id, address1 address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where (address2 = 'unknown')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings suspicious address2",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where (address2 like '%,%QC%')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings unknown city",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where (city is NULL or city = 'unknown' or city = '')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings invalid city",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where city in ('Montreal', 'Quebec')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings unknown province",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where (province is NULL or province = 'unknown' or province = '')
and country not in ('France')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings suspicious province",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where length(province) > 2
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings unknown postal code",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where (postalcode is NULL or postalcode = 'unknown' or postalcode = '')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings with invalid postal code",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where city != 'Anjou'
and postalcode in ('H1J 2Y7', 'H1J2Y7')
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "buildings with invalid postal code format",
		sql => "select clients.id, buildings.id as _building_id, address1, address2, city, province, postalcode, buildings.updated_at
from clients, sites, buildings
where postalcode !~ '^([A-Z][0-9][A-Z] [0-9][A-Z][0-9]|[0-9]{5}|[0-9]{5}-[0-9]{4})\$'
and postalcode != ''
and sites.client_id = clients.id
and sites.building_id = buildings.id
and $sitescommon
and $commonx"
	);

	# ----

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "unknown billing_address1",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where (billing_address_street is NULL or billing_address_street = 'unknown')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "unknown billing_address2",
		sql => "select clients.id, billing_address_street billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where (billing_address_street_2 = 'unknown')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "suspicious billing_address2",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where (billing_address_street_2 like '%,%QC%')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "unknown billing_address_city",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where (billing_address_city is NULL or billing_address_city = 'unknown')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "invalid billing_address_city",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where billing_address_city in ('Montreal', 'Quebec')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "unknown billing_address_province",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where (billing_address_state is NULL or billing_address_state = 'unknown')
and billing_address_country not in ('France')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "suspicious billing_address_province",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where length(billing_address_state) > 2
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "unknown billing_address_postal code",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where (billing_address_postalcode is NULL or billing_address_postalcode = 'unknown' or billing_address_postalcode = '')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "with invalid billing_address_postal code",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where billing_address_city != 'Anjou'
and billing_address_postalcode in ('H1J 2Y7', 'H1J2Y7')
and $common"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "with invalid billing_address_postal code format",
		sql => "select clients.id, billing_address_street, billing_address_street_2, billing_address_city, billing_address_state, billing_address_postalcode
from clients, crm_accounts
where billing_address_postalcode !~ '^([A-Z][0-9][A-Z] [0-9][A-Z][0-9]|[0-9]{5}|[0-9]{5}-[0-9]{4})\$'
and billing_address_postalcode != ''
and $common"
	);

	# ----

	run_data (
		client => 1,
		name => "statement_type = English and language_preference = French",
		sql => "select clients.id, statement_type_clients.updated_at, language_preference, statement_types.description, invoice_cycle
from statement_type_clients, invoice_templates, btns, statement_types, clients, crm_accounts
where clients.id = btns.client_id
and invoice_template_id = invoice_templates.id
and clients.id = statement_type_clients.client_id
and statement_type_id = statement_types.id
and language_preference = 'French'
and statement_types.description like '%English%'
and $common"
	);

	run_data (
		client => 1,
		name => "statement_type = French and language_preference = English",
		sql => "select clients.id, statement_type_clients.updated_at, language_preference, statement_types.description, invoice_cycle
from statement_type_clients, invoice_templates, btns, statement_types, clients, crm_accounts
where clients.id = btns.client_id
and invoice_template_id = invoice_templates.id
and clients.id = statement_type_clients.client_id
and statement_type_clients.client_id = clients.id
and statement_type_id = statement_types.id
and language_preference = 'English'
and statement_types.description like '%French%'
and $common"
	);

# this is allowed: not an error
#	run_data (
#		client => 1,
#		name => "BTN contains a space",
#		sql => "select clients.id
#from btns, clients, crm_accounts
#where clients.id = btns.client_id
#and billingtelephonenumber ~ ' '
#and $common"
#	);

# 2018-03-29 seems data is really wrong so far.. wait until more things are loaded
#	run_data (
#		client => 1,
#		track => "https://code.credil.org/issues/12028", 
#		name => "calls with no entry in btn table",
#		sql => "select calls.id as call_id, btn, did, call_date
#from calls
#left join btns on billingtelephonenumber = btn
#where billingtelephonenumber is NULL
#and calls.deleted = false"
#	);


	run_data (
		client => 1,
		client_filter_type => 'I',
		client_filter_active => 'A',
		name => "services with missing display_text",
		sql => "select clients.id, sites.name as site_name, sites.site_number, service_id, services.description as service_description
from clients
join sites on sites.client_id = clients.id
and sites.deleted = false
join services on services.site_id = sites.id
and services.deleted = false
where services.display_text is NULL
and $commonx"
	);

# 2018-06-04 combined into query below
#	run_data (
#		client => 1,
#		client_filter_type => 'I',
#		client_filter_active => 'A',
#		name => "sites with no services",
#		sql => "select clients.id, sites.name as site_name, sites.site_number, service_id
#from clients
#join sites on sites.client_id = clients.id
#and sites.deleted = false
#left join services on services.site_id = sites.id
#and services.deleted = false
#where services.id is NULL
#and $commonx"
#	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		client_filter_active => 'A',
		name => "sites with no services and no billable_services and site name != Billing",
		sql => "select clients.id, sites.name as site_name, sites.site_number
from clients
join sites on sites.client_id = clients.id
and sites.deleted = false
and sites.activated = true
left join services on services.site_id = sites.id
and services.deleted = false
left join billable_services on billable_services.site_id = sites.id
and billable_services.deleted = false
where services.id is NULL
and billable_services.id is NULL
and sites.name != 'Billing'
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		client_filter_active => 'A',
		name => "sites with no services and no billable_services and site name = Billing (hide these eventually?)",
		sql => "select clients.id, sites.name as site_name, sites.site_number
from clients
join sites on sites.client_id = clients.id
and sites.deleted = false
and sites.activated = true
left join services on services.site_id = sites.id
and services.deleted = false
left join billable_services on billable_services.site_id = sites.id
and billable_services.deleted = false
where services.id is NULL
and billable_services.id is NULL
and sites.name = 'Billing'
and $commonx"
	);

# 2018-06-04 combined into query above
#	run_data (
#		client => 1,
#		client_filter_type => 'I',
#		client_filter_active => 'A',
#		name => "sites with no billable_services",
#		sql => "select clients.id, sites.name as site_name, sites.site_number
#from clients
#join sites on sites.client_id = clients.id
#and sites.deleted = false
#left join billable_services on billable_services.site_id = sites.id
#and billable_services.deleted = false
#where billable_services.id is NULL
#and $commonx"
#	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		client_filter_active => 'I',
		name => "closed sites with active services",
		sql => "select clients.id, sites.name as site_name, sites.site_number, services.service_id
from clients
join sites on sites.client_id = clients.id and sites.deleted = false and sites.activated = false
join services on services.site_id = sites.id and services.deleted = false
where $services_is_active
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		client_filter_active => 'I',
		name => "closed sites with active billable_services",
		sql => "select clients.id, sites.name as site_name, sites.site_number, billing_period_ended_at, charge_pid, service_id
from clients
join sites on sites.client_id = clients.id and sites.deleted = false and sites.activated = false
join billable_services on billable_services.site_id = sites.id and billable_services.deleted = false
left join services on billable_services.asset_id = services.id
where not billable_services.id is NULL
and recurrence_type != 1
and $charges_is_active
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "coverage inconsistency",
		sql => "select clients.id, coverage_id, updated_at
from clients
where coverage_id not in (1,2)
and $commonx"
	);		

	run_data (
		client => 1,
		client_filter_type => 'S',
		name => "coverage inconsistency",
		sql => "select clients.id, coverage_id, updated_at
from clients
where coverage_id not in (9)
and $commonx"
	);		

	run_data (
		client => 1,
		name => "last payment date inconsistency",
		sql => "select max(clients.id) as id, max(date(date)) as payment_date, max(date(last_payment_date)) as last_payment_date
from payments, billings, clients
where billings.client_id = clients.id
and transaction_type_id = 4
and billings.client_id = payments.client_id
and $commonx_no
and payments.deleted = false
group by payments.client_id having max(date(date)) != max(date(last_payment_date))
order by max(clients.id)"
	);

# leave this out for now until we decide how payments should be posted, 2018-05-24
#	run_data (
#		client => 1,
#		name => "payment posted date is blank",
#		sql => "select clients.id, payments.id as payment_id, payments.date, payments.amount, payments.transaction_type_id, payments.description
#from clients, payments
#where payments.client_id = clients.id
#and posted_date is Null
#and $commonx",
#		tofix => "update payments set posted_date = date
#where posted_date is NULL"
#	);

	run_data (
		client => 1,
		client_filter_active => 'A',
		name => "Service not associated to a rate_plan", 
		sql => "select clients.id, services.id as service_id_int, service_id, department_pid, service_pid, phone_service_types.code as phone_service_type_code
from services
join btns on services.btn_id = btns.id
join clients on btns.client_id = clients.id
left join phone_service_types on phone_service_type_id = phone_service_types.id
where services.rate_plan_id is NULL
and $services_is_active
and $commonx, service_id_int"
	);

# disabled for now as there are too many errors 2018-05-31
#	run_data (
#		client => 1,
#		name => "Service not associated to a coverage", 
#		sql => "select clients.id, services.id as service_id_int, service_id, department_pid, service_pid
#from services
#join btns on services.btn_id = btns.id
#join clients on btns.client_id = clients.id
#where services.coverage_id is NULL
#and $commonx"
#	);

	run_data (
		client => 1,
		client_filter_active => 'A',
		name => "Service not associated to a site (service is active)", 
		sql => "select clients.id, services.id as service_id_int, service_id, services.department_pid, service_pid, termination_date
from services
join btns on services.btn_id = btns.id
join clients on btns.client_id = clients.id
where site_id is NULL
and $services_is_active
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_active => 'A',
		name => "Service not associated to a site (service is inactive)", 
		sql => "select clients.id, services.id as service_id_int, service_id, services.department_pid, service_pid, termination_date
from services
join btns on services.btn_id = btns.id
join clients on btns.client_id = clients.id
where site_id is NULL
and termination_date < now()
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_active => 'A',
		name => "Billable service not associated to a site (billable service is active)", 
		sql => "select clients.id, billable_services.id as billable_service_id_int, billable_services.department_pid, charge_pid, billing_period_ended_at, services.service_id
from billable_services
join clients on billable_services.client_id = clients.id
join services on billable_services.asset_id = services.id
where billable_services.site_id is NULL
and $charges_is_active
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_active => 'A',
		name => "Billable service not associated to a site (billable service is inactive)", 
		sql => "select clients.id, billable_services.id as billable_service_id_int, billable_services.department_pid, charge_pid, billing_period_ended_at, services.service_id
from billable_services
join clients on billable_services.client_id = clients.id
join services on billable_services.asset_id = services.id
where billable_services.site_id is NULL
and billing_period_ended_at < now()
and $commonx"
	);

	run_data (
		client => 1,
		name => "invoicing_enabled not set",
		sql => "select clients.id, invoicing_enabled, btns.updated_at
from btns, clients
where invoicing_enabled is NULL
and btns.client_id = clients.id
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'S',
		name => "extra invoicing_enabled",
		sql => "select clients.id
from clients, btns
where invoicing_enabled = true
and clients.id = btns.client_id
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "missing invoicing_enabled",
		sql => "select clients.id
from clients, btns
where invoicing_enabled = false
and clients.id = btns.client_id
and $commonx"
	);
		
	run_data (
		client => 1,
		name => "Clients in the midst of closing: Status = closed and cycle != 999",
		sql => "select clients.id
from statement_type_clients, invoice_templates, btns, statement_types, clients, crm_accounts
where clients.id = btns.client_id
and invoice_template_id = invoice_templates.id
and clients.id = statement_type_clients.client_id
and statement_type_clients.client_id = clients.id
and statement_type_id = statement_types.id
and status = 4
and invoice_cycle = '999'
and $commonx_no group by clients.id order by clients.id"
	);

	run_data (
		client => 1,
		name => "Clients in the midst of closing: Status = closed and dactivated_at not set",
		sql => "select clients.id, clients.deactivated_at
from clients
where status = 4
and (clients.deactivated_at is NULL or clients.deactivated_at > now())
and $commonx"
	);

	run_data (
		client => 1,
		name => "Clients in the midst of closing: Status != closed and dactivated_at set",
		sql => "select clients.id, clients.deactivated_at
from clients
where status != 4
and clients.deactivated_at < now()
and $commonx"
	);

	run_data (
		client => 1,
		name => "Clients in the midst of closing: Status = closed and balance is not 0",
		sql => "select clients.id, sum(payments.amount)
from clients
join payments on payments.client_id = clients.id
where status = 4
and $commonx_no
group by clients.id
having sum(payments.amount) != 0"
	);

	run_data (
		client => 1,
		name => "Clients in the midst of closing: Status = pending closed and balance is 0",
		sql => "select clients.id, sum(payments.amount)
from clients
join payments on payments.client_id = clients.id
where status = 3
and $commonx_no
group by clients.id
having sum(payments.amount) = 0"
	);

	run_data (
		client => 1,
		name => "Clients in the midst of closing: client pending_closed and site is activated",
		sql => "select clients.id, sites.name as site_name
from sites, clients
where sites.client_id = clients.id
and clients.status in (3)
and sites.activated = true
and sites.deleted = false
and $commonx"
	);

	run_data (
		client => 1,
		name => "Clients in the midst of closing: client closed and site is activated",
		sql => "select clients.id, sites.name as site_name
from sites, clients
where sites.client_id = clients.id
and clients.status in (4)
and sites.activated = true
and sites.deleted = false
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "client is not support-only and missing site",
		sql => "select clients.id
from clients, btns
left join sites on btns.client_id = sites.client_id
where clients.id = btns.client_id
and sites.client_id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "client is not support-only and missing active site",
		sql => "select clients.id
from clients, btns
left join sites on btns.client_id = sites.client_id
and sites.activated = true
where clients.status not in (3,4)
and clients.activated = true
and clients.id = btns.client_id
and sites.client_id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'S',
		name => "client is support-only and has a site",
		sql => "select clients.id
from clients, btns
left join sites on btns.client_id = sites.client_id
and sites.deleted = false
where clients.id = btns.client_id
and not sites.client_id is NULL
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "client has no lines_of_business",
		sql => "select clients.id
from clients
left join client_line_of_businesses on client_id = clients.id group by clients.id having count(*) = 0"
	);

	run_data (
		client => 1,
		client_filter_type => 'S',
		name => "client has extra lines_of_business",
		sql => "select clients.id, count(*) as count, array_to_string(array_agg(line_of_business_id), ',') as lines_of_bus
from clients
join client_line_of_businesses on client_id = clients.id group by clients.id"
	);

	# ---------- calls

	run_data (
		client => 1,
		name => "calls linked to a service which is missing site_id",
		sql => "select services.service_id, btns.client_id as id, called_number
from calls
join services on calls.service_id = services.id 
join btns on services.btn_id = btns.id where site_id is NULL"
	);

	run_data (
		client => 1,
		name => "calls with called_number starting with *",
		sql => "select services.service_id, btns.client_id as id, called_number
from calls
join services on calls.service_id = services.id 
join btns on services.btn_id = btns.id where called_number like '*%'"
	);

	run_data (
		client => 1,
		name => "calls with called_number of length 3 (eg 411)",
		sql => "select services.service_id, btns.client_id as id, called_number
from calls
join services on calls.service_id = services.id 
join btns on services.btn_id = btns.id where length(called_number) = 3"
	);

	run_data (
		client => 1,
		name => "calls with called_number of length 7",
		sql => "select services.service_id, btns.client_id as id, called_number
from calls
join services on calls.service_id = services.id 
join btns on services.btn_id = btns.id where length(called_number) = 7"
	);
}
