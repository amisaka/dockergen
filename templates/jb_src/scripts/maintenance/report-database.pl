#!/usr/bin/perl -w

use utf8;
use strict;
use SG::SQL qw(do_connect);
use SG::Report qw(generate_section);
use Data::Dumper;

binmode(STDOUT, ":utf8");
my $dbh = do_connect();

my %test;

error_suspicious();
get_tables();
error_key();
missing_timestamp();
unused_columns();

# ---------- subroutines

sub error_suspicious {
	run_suspicious (
		name => "clients",
		sql => "select * from clients" # status: { unknown: 0, pending_start: 1, active: 2, pending_closed: 3, closed: 4 }
	);

	run_suspicious (
		name => "sites",
		sql => "select * from sites"
	);

	run_suspicious (
		name => "buildings",
		sql => "select * from buildings"
	);

	run_suspicious (
		name => "crm_accounts",
		sql => "select * from crm_accounts"
	);

	#run_suspicious (
	#	name => "btns",
	#	sql => "select * from btns"
	#);

	#run_suspicious (
	#	name => "payments",
	#	sql => "select * from payments"
	#);

	run_suspicious (
		name => "billings",
		sql => "select * from billings"
	);

	run_suspicious (
		name => "statement_types",
		sql => "select * from statement_type_clients"
	);

	run_suspicious (
		name => "billable_services",
		sql => "select * from billable_services"
	);

	#run_suspicious (
	#	name => "services",
	#	sql => "select * from services"
	#);
}

sub get_tables {
	my $tables = $dbh->selectall_hashref("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'", 'table_name');

	foreach my $table (sort keys %$tables) {
		next if ($table eq 'lavenshtein_exchanges');
		next if ($table eq 'schema_migrations');
		next if ($table eq 'ar_internal_metadata');
		next if ($table eq 'calls');
		$test{$table} = $dbh->selectall_hashref("select * from $table", 'id');
	}
}

sub error_key {
	my %map;
	$map{billing_sites} = 'sites';
	$map{sales_persons} = 'sales_people';
	$map{line_of_businesss} = 'line_of_businesses';
	$map{local_rate_plans} = 'rate_plans';
	$map{national_rate_plans} = 'rate_plans';
	$map{world_rate_plans} = 'rate_plans';
	$map{assets} = 'services';
	$map{suppliers} = 'did_suppliers';

	foreach my $key (sort keys %test) {
		my $dataset = $test{$key};
		my @data;
		foreach my $record (sort {$a cmp $b} keys %$dataset) {
			foreach my $column (sort keys %{$$dataset{$record}}) {
				next if ($column !~ /_id$/);
				next if ($column =~ /^load_id$/);
				my $value = $$dataset{$record}{$column};
				next if (! defined $value);
				my $table = $column;
				$table =~ s/_id$/s/;
				my $xtable = $table;
				if ($map{$table}) {
					#print ">>> $table => $map{$table}\n";
					$xtable = $map{$table};
				}
				if ((! exists $test{$table}{$value}) && (! exists $test{$xtable}{$value})) {
					push (@data, sprintf("[%20s][%25s][%10s]", $record, $column, $value));
				}
			}
		}

		print generate_section (name => "Missing key: table=$key", data => \@data, limit => 100, suppress_if_empty => 1);
	}
}

sub missing_timestamp {
	my %xreverse;
	$xreverse{activated} = 'deactivated';

	foreach my $key (sort keys %test) {
		my $dataset = $test{$key};
		my @data;
		foreach my $record (sort {$a cmp $b} keys %$dataset) {
			foreach my $column (qw (activated deleted)) {
				if (exists ($$dataset{$record}{$column})
					&& (exists $$dataset{$record}{"${column}_at"}) 
					&& ($$dataset{$record}{$column} == 1) 
					&& (! defined $$dataset{$record}{"${column}_at"})) {
					push (@data, sprintf("[%20s][%25s]", $record, "${column}_at"));
				}
				my $reversecol = $xreverse{$column};
				if (exists ($$dataset{$record}{$column})
					&& $reversecol
					&& (exists $$dataset{$record}{"${column}_at"}) && (defined $$dataset{$record}{"${column}_at"})
					&& (exists $$dataset{$record}{"${reversecol}_at"}) && (defined $$dataset{$record}{"${reversecol}_at"})
					&& ($$dataset{$record}{$column} == 1) 
					&& ($$dataset{$record}{"${column}_at"} lt $$dataset{$record}{"${reversecol}_at"})) {
					push (@data, sprintf("[%20s][%25s]", $record, "${column}_at lt ${reversecol}_at while $column = true"));
				}
				if (exists ($$dataset{$record}{$column})
					&& $reversecol
					&& (exists $$dataset{$record}{"${column}_at"}) && (defined $$dataset{$record}{"${column}_at"})
					&& (exists $$dataset{$record}{"${reversecol}_at"}) && (defined $$dataset{$record}{"${reversecol}_at"})
					&& ($$dataset{$record}{$column} == 0) 
					&& ($$dataset{$record}{"${column}_at"} gt $$dataset{$record}{"${reversecol}_at"})) {
					push (@data, sprintf("[%20s][%25s]", $record, "${column}_at lt ${reversecol}_at while $column = false"));
				}
			}
		}

		print generate_section (name => "Missing timestamp: table=$key", data => \@data, limit => 100, suppress_if_empty => 1);
	}
}

sub unused_columns {
	foreach my $key (sort keys %test) {
		my $dataset = $test{$key};
		my @data;
		my %values;
		foreach my $record (keys %$dataset) {
			foreach my $column (keys %{$$dataset{$record}}) {
				next if ($column eq 'deleted');
				next if ($column eq 'deleted_at');
				next if ($column eq 'activated');
				next if ($column eq 'activated_at');
				next if ($column eq 'deactivated_at');
				my $value = $$dataset{$record}{$column};
				$value = 'undef' if (! defined $value);
				$values{$column}{$value} = 1;
			}
		}

		foreach my $column (keys %values) {
			my $count = scalar (keys %{$values{$column}});
			if ($count < 3) {
				push (@data, sprintf("column=<$column> has count=<$count> unique values: " . join (' ', (map {"[$_]"} sort keys %{$values{$column}}))));
			}
		}

		print generate_section (name => "Under-used columns: table=$key", data => \@data, limit => 100, suppress_if_empty => 1);
	}
}

sub run_suspicious {
	my (%options) = @_;

	my $name = $options{name};
	my $sql = $options{sql};

	my $records = $dbh->selectall_arrayref("$sql order by id", { Slice => {} });

	return if (! @$records);
	my @data;
	foreach my $record (@$records) {
		my $record_id = $$record{id};

		foreach my $column (sort keys %$record) {
			next if ($column eq 'note');
			my $value = $$record{$column};
			next if (! defined $value);

			$value =~ s/\r\n/!!/sg;
			$value =~ s/\n/!!/sg;

			my $xvalue = Dumper($value);
			chomp ($xvalue);
			$xvalue =~ s/^\$VAR1 = "(.*)";$/$1/;
			$xvalue =~ s/^\$VAR1 = "(.*)";$/$1/;
			$xvalue =~ s/^\$VAR1 = (.*);$/$1/;
			my $code = 0;
			$code = 1 if ($xvalue =~ /{c3}/);
			$code = 1 if ($xvalue =~ /{a8}/);
			$code = 1 if ($xvalue =~ /{a9}/);
			$code = 1 if ($xvalue =~ /\s\s/);
			$code = 1 if ($xvalue =~ /`/);
			$code = 1 if ($xvalue =~ /!!/);
			$code = 1 if ($xvalue =~ /^\s/);
			$code = 1 if ($xvalue =~ /\s$/);
			$code = 1 if ($xvalue =~ /\s,/);
			$code = 1 if ($xvalue =~ /,[^\s]/);
			$code = 1 if ($xvalue =~ /,$/);
			$code = 1 if ($value =~ /unknown/ && $value ne 'unknown');
			if ($column eq 'postalcode' || $column eq 'billing_address_postalcode') {
				$code = 1 unless ($value =~ /^([A-Z][0-9][A-Z] [0-9][A-Z][0-9]||[0-9]{5}||[0-9]{5}-[0-9]{4})$/);
			}
			if ($code) {
				push (@data, sprintf("[%20s][%25s][%45s]", $record_id, $column, $value));
			}
		}
	}

	print generate_section (name => "Suspicious data: table=$name", data => \@data);
}
