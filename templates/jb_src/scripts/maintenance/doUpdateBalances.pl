#!/usr/bin/perl -w

#Originally doupdate.pl, this perl script only updates balances in billings *AND* the balance of the last posted invoice

use utf8;
use strict;
use SG::SQL qw(do_connect);
use Data::Dumper;
use SG::Report qw(client_list);

binmode(STDOUT, ":utf8");
my $dbh = do_connect();

my $clients = client_list ($dbh);

#update_last_posted_invoice(@ARGV);
#$dbh->commit;

$dbh->disconnect;

# ---------- subroutines

#######################################################################################
# Update the balance of the last posted invoiceen
# Needs one arg: the date from which future payments are ignored
# Date is inclusive ("2018-06-30" means payments & charges on 2018-06-30 are included)
# Modified balance is always on the last posted invoice regardless of supplied date
#######################################################################################
sub update_last_posted_invoice() {
    if (! $_[0]) {
        exit 1
    }

    $date = $_[0]

	foreach my $client_id (sort {$a <=> $b } keys %$clients) {
		my $client_active = $$clients{$client_id}{client_active};
		my $invoice_cycle = $$clients{$client_id}{invoice_cycle};

        #next if ($client_active == 0);
		next if ($invoice_cycle eq 'Support');

        #print ">> $client_id: $client_active, $invoice_cycle\n";


		my %data;

        # Calculate the balance
		my $balance = $dbh->selectrow_hashref("select sum(amount) as balance from payments where client_id = $client_id and date <= '$date 23:59:59'");
		$data{balance} = $$balance{balance};

        # Find the last posted invoice
        my $lastPostedInvoiceId = $dbh->selectrow_hashref("select max(id) as id from invoices where posted_at is not nil and client_id = $client_id");
        $lastPostedInvoiceId = $$lastPostedInvoiceId{id}


		my @params;
		foreach my $key (keys %data) {
			push (@params, "$key = " . $dbh->quote($data{$key}));
		}

		my $sql = "update billings set " . join (',', @params) . " where client_id = $client_id";
		$dbh->do($sql);



	}
}

