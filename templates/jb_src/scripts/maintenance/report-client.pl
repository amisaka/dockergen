#!/usr/bin/perl -w

use utf8;
use strict;
use SG::SQL qw(do_connect);
use SG::Report qw(format_entry generate_section client_list run_data_report);
use Data::Dumper;

binmode(STDOUT, ":utf8");
my $dbh = do_connect();
my $clientsx = client_list ($dbh);

error_data();

# ---------- subroutines

sub run_data {
        my (%options) = @_;

	run_data_report (dbh => $dbh, clientsx => $clientsx, %options);
}

sub error_data {
	my $common_no = "crm_account_id = crm_accounts.id and clients.deleted = false";
	my $common = "$common_no order by clients.id";
	my $commonx = "clients.deleted = false order by clients.id";
	my $commonx_no = "clients.deleted = false";
	my $sitescommon = "sites.activated = true and sites.deleted = false";
	my $services_is_active = "(termination_date is NULL or termination_date > now())";
	my $charges_is_active = "(billing_period_ended_at is NULL or billing_period_ended_at > now())";

	run_data (
		client => 1,
		name => "Invalid GL Account, type=OpeningBalance",
		sql => "select clients.id, \"gl_account_id\", date(payments.date) as date, payments.transaction_type_id, payments.amount
from clients, payments
where clients.id = payments.client_id
and (gl_account_id not in(1) OR gl_account_id is NULL)
and transaction_type_id = 1
and $commonx, date",
		tofix => "update payments set gl_account_id = 1
where transaction_type_id = 1
and (gl_account_id not in(1) or gl_account_id is NULL)"
	);

	run_data (
		client => 1,
		name => "Invalid GL Account, type=Payment",
		sql => "select clients.id, \"gl_account_id\", date(payments.date) as date, payments.transaction_type_id, payments.amount
from clients, payments
where clients.id = payments.client_id
and (gl_account_id not in(1) OR gl_account_id is NULL)
and transaction_type_id = 4
and $commonx, date",
		tofix => "update payments set gl_account_id = 1
where transaction_type_id = 4
and (gl_account_id not in(1) or gl_account_id is NULL)"
	);

	run_data (
		client => 1,
		name => "Invalid GL Account, type=CreditMemo",
		sql => "select clients.id, \"gl_account_id\", date(payments.date) as date, payments.transaction_type_id, payments.amount
from clients, payments
where clients.id = payments.client_id
and (gl_account_id = 1 OR gl_account_id is NULL)
and transaction_type_id = 2
and $commonx, date",
		tofix => "update payments set gl_account_id = 13???
where transaction_type_id = 2
and (gl_account_id = 1 or gl_account_id is NULL)"
	);

	run_data (
		client => 1,
		name => "Invalid GL Account, type=DebitMemo",
		sql => "select clients.id, \"gl_account_id\", date(payments.date) as date, payments.transaction_type_id, payments.amount
from clients, payments
where clients.id = payments.client_id
and (gl_account_id = 1 OR gl_account_id is NULL)
and transaction_type_id = 3
and $commonx, date",
		tofix => "update payments set gl_account_id = 13???
where transaction_type_id = 3
and (gl_account_id = 1 or gl_account_id is NULL)"
	);

	run_data (
		client => 1,
		name => "Informational: Duplicate payments on the same day",
		sql => "select clients.id, p1.id as p1_id, p2.id as p2_id, date(p1.date) as date, p1.amount, p2.amount
from payments as p1, payments as p2, clients
where p1.client_id = clients.id
and p1.date = p2.date
and p1.client_id = p2.client_id
and p1.deleted = false
and p2.deleted = false
and p1.id != p2.id
and p1.amount = p2.amount
and p1.amount != 0
and p2.amount != 0
and p1.transaction_type_id = p2.transaction_type_id
and p1.id < p2.id
and p1.deleted = false
and p2.deleted = false
and p1.date > (now() - interval '30 day')
and $commonx"
	);

	run_data (
		client => 1,
		name => "Informational: Duplicate payments within 5 days of each other",
		sql => "select clients.id, p1.id as p1_id, p2.id as p2_id, date(p1.date) as p1_date, date(p2.date) as p2_date, p1.amount, p2.amount
from payments as p1, payments as p2, clients
where p1.client_id = clients.id
and p1.date != p2.date
and (abs(cast(p1.date as date) - cast(p2.date as date)) <= 5)
and p1.client_id = p2.client_id
and p1.deleted = false
and p2.deleted = false
and p1.id != p2.id
and p1.amount = p2.amount
and p1.amount !=0
and p2.amount != 0
and p1.transaction_type_id = p2.transaction_type_id
and p1.id < p2.id
and p1.deleted = false
and p2.deleted = false
and p1.date > (now() - interval '30 day')
and $commonx"
	);

	run_data (
		client => 1,
		track => "https://code.credil.org/issues/11998", 
		name => "accounts with no payment in last 31-60 days",
		sql => "select clients.id, balance, date(last_payment_date) as last_payment_date
from billings, clients
where client_id = clients.id
and balance > 1
and last_payment_date <= NOW() - INTERVAL '31 days'
and last_payment_date >= NOW() - INTERVAL '60 days'
and $commonx"
	);

	run_data (
		client => 1,
		track => "https://code.credil.org/issues/11998", 
		name => "accounts with no payment in last 61-90 days < 200",
		sql => "select clients.id, balance, date(last_payment_date) as last_payment_date
from billings, clients
where client_id = clients.id
and balance > 1
and balance < 200
and last_payment_date <= NOW() - INTERVAL '61 days'
and last_payment_date >= NOW() - INTERVAL '90 days'
and $commonx"
	);

	run_data (
		client => 1,
		track => "https://code.credil.org/issues/11998", 
		name => "accounts with no payment in last 61-90 days, >= 200",
		sql => "select clients.id, balance, date(last_payment_date) as last_payment_date
from billings, clients
where client_id = clients.id
and balance >= 200
and last_payment_date <= NOW() - INTERVAL '61 days'
and last_payment_date >= NOW() - INTERVAL '90 days'
and $commonx"
	);

	run_data (
		client => 1,
		track => "https://code.credil.org/issues/11998", 
		name => "accounts with no payment in last 91 days",
		sql => "select clients.id, balance, date(last_payment_date) as last_payment_date
from billings, clients
where client_id = clients.id
and balance > 1
and last_payment_date <= NOW() - INTERVAL '91 days'
and $commonx"
	);

	run_data (
		client => 1,
		track => "https://code.credil.org/issues/11998", 
		name => "accounts with no payment in last 90 days (credit balance)",
		sql => "select clients.id, balance, date(last_payment_date) as last_payment_date
from billings, clients
where client_id = clients.id
and balance < 0
and last_payment_date <= NOW() - INTERVAL '90 days'
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'S',
		name => "extra payments",
		sql => "select clients.id
from clients, btns
left join payments on btns.client_id = payments.client_id
where not payments.id is NULL
and clients.id = btns.client_id
and payments.deleted = false
and $commonx"
	);

	run_data (
		client => 1,
		client_filter_type => 'I',
		name => "no payments ever",
		sql => "select clients.id
from clients, billings, btns
left join payments on btns.client_id = payments.client_id
where payments.id is Null
and clients.id = billings.client_id
and clients.id = btns.client_id
and billings.activated = true
and $commonx"
	);

	run_data (
		client => 1, 
		name => "Multiple Opening Balances",
		sql => "select client_id as id, count(*)
from payments
where transaction_type_id = 1 group by client_id having count(*) > 1"
	);

}
