module Search::ControllerHelpers

  def self.included(base)
    if base == ApplicationController
      base.class_eval { include ApplicationControllerHelpers }
    else
      base.class_eval { include ActionControllerHelpers }
    end
  end

  private

    module ApplicationControllerHelpers
      def self.included(base)
        base.class_eval { helper_method :query, :search_status }
      end

      def query
        return params[:query] unless params[:query].blank?
        nil
      end

      def search_status
        return params[:status].to_sym unless params[:status].blank?
        nil
      end
    end

    module ActionControllerHelpers
      def query
        return session[:query] = super unless super.blank?
        return session[:query] if !session[:query].blank? && super.blank?
        nil
      end

      def search_status
        return session[:status] = super.to_sym unless super.blank?
        return session[:status].to_sym if !session[:status].blank? && super.blank?
        nil
      end
    end
end
