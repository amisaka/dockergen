module Search::ClientHelpers

  DEFAULT_FIELD_VALUE  = 'name'.freeze
  DEFAULT_QUERY_VALUE  = nil
  DEFAULT_STATUS_VALUE = 'active'.freeze
  DEFAULT_INVOICE_VALUE = 'invoice'.freeze

  def self.included(base)
    base.class_eval do
      helper_method :client_search_field, :client_search_query,
        :client_search_params, :client_search_status, :client_search_invoice
    end
  end

  def reset
    session[:client_search] = nil
    if request && request.referrer
      url = URI(request.referrer)
      new_url = "#{url.scheme}://#{url.host}"
      new_url << ":#{url.port}" unless url.port.blank?
      new_url << url.path
      redirect_to new_url
    else
      redirect_to clients_path
    end
  end

  def client_search_field
    if client_search_params['field'].blank?
      if client_search_session['field'].blank?
        client_search_params['field'] = client_search_session['field'] = DEFAULT_FIELD_VALUE
      else
        client_search_params['field'] = client_search_session['field']
      end
    else
      client_search_session['field'] = client_search_params['field']
    end
  end

  def client_search_query
    if client_search_params['query'].blank?
      if client_search_session['query'].blank?
        client_search_params['query'] = client_search_session['query'] = DEFAULT_QUERY_VALUE
      else
        client_search_params['query'] = client_search_session['query']
      end
    else
      client_search_session['query'] = client_search_params['query']
    end
  end

  def client_search_invoice
    if client_search_params['invoice'].blank?
      if client_search_session['invoice'].blank?
        client_search_params['invoice'] = client_search_session['invoice'] = DEFAULT_INVOICE_VALUE
      else
        case client_search_session['invoice']
        when 'invoice'
          client_search_params['invoice'] = 'invoice'
        when 'support'
          client_search_params['invoice'] = 'support'
        when 'all'
          client_search_params['invoice'] = 'all'
        else
          client_search_params['invoice'] = 'invoice'
        end
      end
    else
      case client_search_params['invoice']
      when 'invoice'
        client_search_session['invoice'] = 'invoice'
      when 'support'
        client_search_session['invoice'] = 'support'
      when 'all'
        client_search_session['invoice'] = 'all'
      else
        client_search_session['invoice'] = 'invoice'
      end
    end
  end

  def client_search_status
    # is params present?
    if client_search_params['status'].blank?
      # then look in session
      if client_search_session['status'].blank?
        # Return nil
        client_search_params['status'] = client_search_session['status'] = DEFAULT_STATUS_VALUE
      else
        # session has a saved param
        case client_search_session['status']
        when 'active'
          client_search_params['status'] = 'active'
        when 'closed'
          client_search_params['status'] = 'closed'
        when 'either'
          client_search_params['status'] = 'either'
        else
          client_search_params['status'] = 'active'
        end
      end
    else
      case client_search_params['status']
      when 'active'
        client_search_session['status'] = 'active'
      when 'closed'
        client_search_session['status'] = 'closed'
      when 'either'
        client_search_session['status'] = 'either'
      else
        client_search_session['status'] = 'active'
      end
    end
  end

  def client_search_params
    params.fetch(:client_search, {}).permit('query', 'field', 'invoice', 'status')
  end

  def client_search_session
    session['client_search'] ||= {}
  end
end
