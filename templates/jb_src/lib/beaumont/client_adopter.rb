module Beaumont::ClientAdopter
  # convert a client into a site of another client.
  # maps the site and btn over, and marks original client as deprecated.
  # if the site moved over was "0", then it is renamed to the original
  # name of the client.
  def adopt_client_as_site(adoptee)
    adoptee.btns.each { |btn|
      btn.billing_site ||= adoptee.billing_site
      btn.client = self
      btn.save!
    }
    adoptee.sites.each { |site|
      site.client = self
      site.save!
    }
    if adoptee.billing_site.try(:name) == "0" || adoptee.billing_site.try(:name) == "SS/HQ"
      site = adoptee.billing_site
      site.name = adoptee.name
      if site.site_number == "0"
        site.site_number = sprintf("%04u", site.id)
      end
      site.save!
    end
    adoptee.terminated!(DateTime.now - 1.minute)
    adoptee.crm_account.terminated!
    adoptee.save!
    adoptee.reload
    self
  end

  def adopt_client_as_site_and_merge(adoptee, mergetosite)
    adoptee_sites = adoptee.sites
    adopt_client_as_site(adoptee)

    adoptee_sites.each { |site|
      mergetosite.merge_from(site)
    }
  end
end
