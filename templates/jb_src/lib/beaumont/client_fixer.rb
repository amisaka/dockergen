module Beaumont::ClientFixer

  def fix_btn
    if btn.blank? && !account_code.blank?
      save!(validate: false)
      nbtn = self.btn.create(:billingtelephonenumber => account_code)
      nbtn.save!(validate: false)
    end
  end

  # the account code can not be fixed up front, it needs the BTN, etc. to exist.
  def client_crm_account_code_fix(accountcode = nil, provided_name = nil)
    if billing_btn.blank? || btn.blank?
      self.billing_btn = accountcode || btn.try(:billingtelephonenumber) if billing_btn.blank?

      fix_btn
    end

    # if billing_btn is still blank, then we can not tweak the ids.
    if billing_btn.blank?
      return
    end

    unless self.crm_account
      if self[:name].blank?
        if provided_name
          self.crm_account = CrmAccount.create(:name => provided_name)
        else
          self.crm_account = CrmAccount.create(:name => '[MISSING NAME]')
        end
      else
        self.crm_account = CrmAccount.create(:name => self[:name])
      end
    end
    if self.crm_account.name.blank?
      self.crm_account.name = read_attribute(:name) || provided_name
      self.crm_account.save!
    end
    if self.crm_account_id != self.billing_btn

      # get the two objects
      crm = self.crm_account

      # if there is an existing crm with the same account code,
      # then we will use it, we may have duplicates.
      crm2 = CrmAccount.find_by_id(self.billing_btn)

      if crm2 and crm2.client.nil?
        crm2.delete

      elsif crm2 and crm.id != crm2.id
        c2 = crm2.client

        if c2.id != self.id
          if c2.phones.count > self.phones.count
            # we need to keep the other client, not this one.
            # this one might have some items.
            c1 = self
          else
            b1 = c2
            c2 = self
            c1 = b1
          end

          c1.transferto(c2)
          c2.crm_account = crm2
          c2.save!
          return c2
        end
      end
      crm.id = self.billing_btn
      crm2 = crm

      crm.save!
      self.crm_account = crm
    end
    save(validate: false)
    return self
  end
end
