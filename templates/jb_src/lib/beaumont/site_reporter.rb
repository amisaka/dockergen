module Beaumont::SiteReporter
  def site_report_to_file(extra="")
    n = self.billing_btn
    File.open("report-#{n}#{extra}.txt", "w") do |f|
      f.puts sprintf("%5s %4s %6s %6s %4s %14s %s\n",
                     'num', 'id', 'count', 'incount', 'rep_id', 'login', 'name')
      self.sites.order(:site_number).each{ |site|
        f.puts sprintf("%5s %4d %6d %6d %4s %14s %s\n",
                       site.site_number, site.id,
                       site.phones.count,
                       site.phones.activated.count,
                       site.site_rep_id, site.site_rep.try(:login),
                       site.name)
      };
    end
    true
  end
end
