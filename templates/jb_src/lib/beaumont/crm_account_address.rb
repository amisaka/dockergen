class CrmAccountAddress
  # repairs all case of addresses containing "\\n"
  def self.repair_all
    crms = []

    CrmAccount.where('billing_address_street IS NOT NULL').each do |c|
      crms << c if c.billing_address_street.include?("\\n")
    end

    crms.each do |c|
      ary = c.billing_address_street.split("\\n")
      c.update(billing_address_street: ary.join(' '))
    end
  end

  # repair a single case
  def self.repair(id)
    crm = CrmAccount.find(id)
    raise "crm account billing_address_street is nil" if crm.billing_address_street.nil?
    ary = crm.billing_address_street.split("\\n")
    crm.update(billing_address_street: ary.join(' '))
  end
end
