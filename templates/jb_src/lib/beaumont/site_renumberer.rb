module Beaumont::SiteRenumberer
  def site_renumber(highestname = 0)
    # now, number sites that were nil.
    # first, need to fix all of the associated CustomerUser's login to
    # unique values so that we can renumber them properly.
    #maybelog "set sites to LOGIN-temp"
    sites.each { |site|
      if sr = site.site_rep
        sr.login = "LOGIN\##{sr.id}"
        sr.save!
      end
    }

    #maybelog "set sites to LOGIN"
    sites.activated.each { |site|
      #maybelog "processing site\##{site.id}"
      if site.site_number.blank?
        #site1 = Site.find(site.id)
        #site = site1
        site.site_number = sprintf("%u", highestname)
        maybelog "site\##{site.id} to #{site.site_number}"
        highestname += 1
        site.save!
      end

      site.make_otrs_user if $OTRS
    }

    highestname
  end
end
