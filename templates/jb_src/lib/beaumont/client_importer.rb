require 'active_support/concern'

module Beaumont::ClientImporter
  extend ActiveSupport::Concern

  class_methods do
    #  "Numéro","Nom","AS","Adresse","Ville","CodePostal",
    #  "PaysISO","Téléphone","Poste","Télécopieur",
    #  "Commentaires","AdresseWeb","EMail","Tri","FacturerANuméro","FacturerANom","NuméroVendeur"
    def import_csv_acomba_row(row, lineno, stats, options)

      (accountcode,shortname,contact, address1, cityprov, postalcode, paysiso,
       contactphone, extension, faxnumber, notes,  webaddress, email, tri,
       factureranumero, factureranom) = row

      if accountcode.blank? or shortname == 'Nom' or shortname.blank?
        return increment_stats_and_return(stats, :comment)
      end

      return import_acomba_hash(lineno,
                                {
                                  accountcode: accountcode,
                                  billingcode: factureranumero,
                                  name: shortname,
                                  address1: address1,
                                  cityprov: cityprov,
                                  postalcode: postalcode,
                                  country: paysiso,
                                }, stats, options)
    end


    # imports an array that came from a fixed field export from Acomba.
    # format is:
    #       accountcode,shortname,longname,contact,address1,cityprov,postalcode,country,
    #       salescontact,empty1,contactphone,extension,contact2,contact3,
    #       empty2,contact4,empty3,empty4,empty5,empty6,empty7,empty8,empty9,empty10,
    #       notes,empty11,empty12,empty13,empty14,empty15,empty16,empty17,empty18,
    #       empty19,empty20,empty21,contact5,startdate,notes2,empty22,limits,
    #       value,value2,language,price,standard,zero1,zero2,yesno1,yesno2,yesno3,yesno4,
    #       empty23,empty24,amountowing,unknown1,date2
    #
    #
    def import_acomba_row(row, lineno, stats, options)
      defaults = { overwrite: false,
                   btnfilter: true }
      options = defaults.merge!(options)

      (accountcode,shortname,longname,contact,address1,cityprov,postalcode,country,
       salescontact,empty1,contactphone,extension,contact2,contact3,
       empty2,contact4,empty3,empty4,empty5,empty6,empty7,empty8,empty9,empty10,
       notes,empty11,empty12,empty13,empty14,empty15,empty16,empty17,empty18,
       empty19,empty20,empty21,contact5,startdate,notes2,empty22,limits,
       value,value2,language,price,standard,zero1,zero2,yesno1,yesno2,yesno3,yesno4,
       empty23,empty24,amountowing,unknown1,date2) = row

      # skip commend headers
      if accountcode.blank? or (shortname.blank? and longname.blank?)
        return increment_stats_and_return(stats, :comment)
      end

      name = longname || shortname
      return import_acomba_hash(lineno,
                                {
                                  accountcode: accountcode,
                                  name: name,
                                  address1: address1,
                                  cityprov: cityprov,
                                  postalcode: postalcode,
                                  country: country,
                                }, stats, options)
    end

    def import_acomba_hash(lineno, values, stats, options)
      defaults = { overwrite: false,
                   updatename: false,
                   btnfilter: true }
      options = defaults.merge!(options)
      overwrite = options[:overwrite]
      btnfilter = options[:btnfilter]
      create_new_site = options[:newsite]

      newclient = false
      client = nil
      site   = nil

      # only import/update if there is an existing BTN object that has the
      # "receives_invoice" bit set.

      btn1 = Btn.get_by_billingtelephonenumber(values[:accountcode])
      if !btn1 and values[:billingcode]
        btn1 = Btn.get_by_billingtelephonenumber(values[:billingcode])
        if btn1
          # if we find it by billingcode rather than account code, then it must be
          # a site...
          stats[:sitecreate] += 1
          create_new_site = true
        end
      end

      if !btn1 and btnfilter
        stats[:unwantedbtn] += 1
        puts "#{lineno}: skipped #{values[:accountcode]}, #{values[:name]}, unwanted btn" if $LOG_NEW_CLIENTS
        return nil
      end

      if btn1
        client = btn1.client

        if create_new_site
          site   = client.find_or_make_site_byname(values[:name])
          site.site_number ||= sprintf("%02d", site.id)
        else
          site   = btn1.find_billing_site
        end
      else
        # now lookup by accountcode
        client = self.find_by_billing_btn(values[:accountcode])
      end

      if client
        client.billing_btn ||= values[:accountcode]

        client.assure_customer_sane("SS/HQ", '0', values[:name], site)
        stats[:oldcustomer] += 1
      else
        client = self.make_customer(values[:name], values[:accountcode], '0', 'nonsense@example.ip4b.net')
        puts "#{lineno}: creating new customer: #{client.id}/#{client.billing_btn} #{values[:name]}" if $LOG_NEW_CLIENTS;
        stats[:newcustomer] += 1
        newclient = true

        if btn1
          btn1.client = client
          btn1.save!
        end
      end

      if options[:updatename] && !create_new_site
        client.crm_account.name = values[:name]
      end

      # province is usually the last two letters of cityprov
      prov = values[:cityprov][-2..-1]
      city = values[:cityprov][0..-3].strip

      province = Cdn::Province.canonicalize_province(prov) if prov
      unless province
        (city, prov) = values[:cityprov].split(',')
        city.try(:strip!)
        prov.try(:strip!)

        province = Cdn::Province.canonicalize_province(prov) if prov

        unless province
          # give up
          city = values[:cityprov].strip
          province = ''
        end
      end

      site ||= client.billing_site

      # client.billing_site is always set by assure_customer_sane.. isn't it?
      # always update if the address1 is blank.
      if site.building
        if site.building.address1.blank? or overwrite or newclient
          site.building.address1 = values[:address1]
          site.building.city     = city
          site.building.province = province
          site.building.postalcode= values[:postalcode]
          site.building.country = values[:country] || "Canada"

          site.building.sanitize_postalcode
          site.building.save(validate: false)
        end
      end

      site.save(validate: false)
      client.save(validate: false)

      return client
    end

    def import_acomba_file(options)
      defaults = { overwrite: false,
                   format:    false,
                   filterbad: false,
                   verbose:   false}
      options = defaults.merge!(options)
      filename   = options[:file]
      handle     = options[:handle]
      overwrite  = options[:overwrite]
      verbose    = options[:verbose]
      legacy     = options[:format] == :legacy
      stats      = options[:stats] || Hash.new(0)
      lineno     = 0

      # file arrives in latin-1 format
      handle   ||= File.open(filename, "r:ISO-8859-1:UTF-8")

      # note that quote_char is not really ~, but CSV does not let us
      # disable it, and the file is not "-quoted properly.
      params = { col_sep: ",", quote_char: '"'}
      if legacy
        params = { col_sep: "\t", quote_char: '~'}
      end

      CSV.parse(handle, params) do |row|
        lineno += 1
        if legacy
          result = import_acomba_row(row, lineno, stats, options)
        else
          result = import_csv_acomba_row(row, lineno, stats, options)
        end

        name = if result
                 result.try(:name)
               else
                 "comment"
               end
        statusline=sprintf("importing row=%03d: client=%s", lineno, name)
        $0=statusline
      end
      return stats
    end

    # imports an array that probably came a CVS import operation
    # format is:
    # "Customer PID","Account Status",
    #                "Account start date","Account Number",
    #                "Account Name",
    #                "Physical Address 1","Physical Address 2",
    #                "Physical City","Physical Province","Physical Post Code",
    #                "Termination Date", "Physical Country"
    #
    # Some of these items do not go directly into the clients table, but instead
    # to the associated crm_accounts table which has physical info.
    #
    # Additionally, if the client.billing_site_id is non-nil, then the physical
    # information will update the site, and associated building as well.

    def import_row(row, lineno)
      (foreign_pid, status, activated_at, account_code,
       account_name, address1, address2, address_city, address_province,
       address_postalcode, deactivated_at, address_country) = row

      # skip commend headers
      return :comment if foreign_pid == "Customer PID"
      return :blank   if foreign_pid == ''

      address_country ||= "Canada"

      # now lookup by foreign_pid
      client = self.find_by_foreign_pid(foreign_pid)

      unless client
        client = self.find_by_billing_btn(account_code)
        if client and client.foreign_pid.nil?
          client.foreign_pid = foreign_pid
        end
      end

      unless client
        client = self.new(foreign_pid: foreign_pid,
                             billing_btn: account_code)
        client.save(validate: false)
        client.activated_at = activated_at.try(:to_date) || DateTime.now
      end

      unless client.crm_account
        # importing, maybe new, so must have account_code
        if status == 'Closed' or status == 'Pending Closed'
          crm1 = CrmAccount.where(:id => account_code,
                                  :deleted => 1).first
        elsif status == 'Active'
          crm1 = CrmAccount.where(:id => account_code,
                                  :deleted => 0).first
        elsif status == 'Pending Start'
          crm1 = CrmAccount.where(:id => account_code,
                                  :deleted => 0).first
          client.activated_at ||= DateTime.now + 1.week
        else
          print "Unknown status: #{status} at line #{lineno}"
          if Rails.env == 'development'
            byebug
          end
          return :error
        end
        client.crm_account = crm1
      end

      unless client.crm_account
        crm1 = CrmAccount.new(:name => account_name, :id => account_code)
        crm1.save!(validate: false)
        client.crm_account = crm1
        client.save!(validate: false)
      else
        crm1 = client.crm_account
        client.name = account_name
        crm1.name   = account_name
      end

      if status == 'Closed'
        client.crm_account.deleted = 1
      end

      # the account code could change -- take care of relationship
      if client.crm_account_id != account_code
        # there count be a crm_account with the same ID already, so we have
        # to deal with merging.
        crm0 = CrmAccount.where(id: account_code).take
        if crm0
          crm0.merge(crm1)
          crm1.delete
          crm1 = crm0
        end
        crm1.id = account_code
        crm1.save!(validate: false)
        client.crm_account = crm1
      end
      client.billing_btn = account_code

      unless client.btn
        client.btn = b1 = Btn.new(billingtelephonenumber: account_code,
                    activated_at: DateTime.now,
                    local_rate_plan: LocalRatePlan.default,
                    national_rate_plan: NationalRatePlan.default,
                    world_rate_plan: InternationalRatePlan.default)
        b1.save(validate: false)
      end

      unless client.billing_site
        if client.sites.first
          client.billing_site = client.sites.first
        else
          client.billing_site = s = Site.new(name: 'HQ',
                                            client: client,
                                            unitnumber: '',
                                            site_number: '0',
                                            activated_at: DateTime.now)
          s.save(validate: false)
        end
      end

      unless client.billing_site.building
        client.billing_site.building = bldg = Building.new(address1: address1,
                                                        address2: address2,
                                                        city: address_city,
                                                        province: address_province,
                                                        country: address_country,
                                                        postalcode: address_postalcode)
        bldg.save(validate: false)
      end

      crm1.billing_address_street = address1;
      unless address2.blank?
        crm1.billing_address_street += "\n" + address2;
      end
      crm1.billing_address_city   = address_city;
      crm1.billing_address_state  = address_province;
      crm1.billing_address_postalcode = address_postalcode;
      crm1.billing_address_country = address_country || "Canada"

      unless deactivated_at.blank?
        client.deactivated_at = deactivated_at.to_date
      end

      if(client.deactivated_at && client.deactivated_at < TestableDateTime.now)
        crm1.deleted = true
      end

      client.save!(validate: false)
      crm1.save!(validate: false)

      return client
    end

    def import_file(options)
      defaults = { overwrite: false,
                   verbose:   false,
                   archive:   false}
      options = defaults.merge!(options)
      filename   = options[:file]
      clientlist = options[:clientlist]

      #handle = File.open(filename, "r:UTF-8")
      handle = File.open(filename, "r:ISO-8859-1:UTF-8")
      return nil unless handle

      linecount=0
      loadedcount=0

      print "starting client file #{filename} \n" if ENV['IMPORT_DEBUG']
      CSV.parse(handle) do |row|

        linecount += 1
        print "processing client #{filename} line #{linecount} -- " if ENV['IMPORT_DEBUG']
        result = import_row(row, linecount)
        case result
        when :comment
          true
        when :blank
          true

        when :error
          true

        when nil
          true

        else
          print "name: #{result.name}                 " if ENV['IMPORT_DEBUG']
          if clientlist
            clientlist[result.id] = result
          end
          loadedcount += 1
        end
        print "\r" if ENV['IMPORT_DEBUG']
      end
      print "\n" if ENV['IMPORT_DEBUG']
      return loadedcount

    end
  end
end
