class BuildingAddress
  # repairs all case of addresses containing "\\n"
  def self.repair_all
    bldgs = []

    Building.where('address1 IS NOT NULL').each do |b|
      bldgs << b if b.address1.include?("\\n")
    end

    bldgs.each do |b|
      ary = b.address1.split("\\n")
      b.update(address1: ary[0], address2: ary[1])
    end
  end

  # repair a single case
  def self.repair(id)
    building = Building.find(id)
    raise "building address1 is nil" if building.address1.nil?
    ary = building.address1.split("\\n")
    building.update(address1: ary[0], address2: ary[1])
  end
end
