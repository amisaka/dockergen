module Beaumont::DailyStats
  def calc_daily_stats(today = Time.now, hour = nil, notify = false)
    sp = StatsPeriod.find_or_create_with_hour(today, hour)

    count = 0
    calls = self.calls
    calls = calls.uncategorized
    calls = calls.today(sp.beginning_of_period, sp.end_of_period)
    calls.find_each {|call|
      call.maybe_update_stats(sp, notify)
      count += 1
    }
    count
  end

  def gen_daily_stats(today = Time.now, hour = nil, notify = false)
    return if call_count_exists?(today, hour)

    # if hour = nil, then we do this for all hours.
    if hour
      hourrange = hour..hour
    else
      hourrange = 0..23
    end
    hourrange.each {|h| calc_daily_stats(today,h,notify)}

    # do daily totals
    total = call_count(today, nil, false, false, notify)
    incoming_calls = incoming_call_count(today, nil, false, false, notify)
    outgoing_calls = outgoing_call_count(today, nil, false, false, notify)
    incoming_cdr_count(today, nil, notify)
    outgoing_cdr_count(today, nil, notify)

    unless total == 0
      self.sites.each { |site|
        # this does lots of single record pulls, and could be sped up
        # by a) identifying three statsperiods involved, and then
        #       pull all records for the phones relating to a client,
        #       and then sorting by phone id.
        #    b) doing it all in one loop for all sites.

        site.phones.by_extension.each { |phone|
          outgoing = phone.outgoing_call_count(today, nil, false, true, notify)
          incoming = phone.incoming_call_count(today, nil, false, true, notify)
          voicemail= phone.incoming_voicemail_count(today, nil, false, true, notify)
          abanoned = phone.incoming_abandoned_count(today, nil, false, true, notify)

          unless(outgoing == 0 &&
                 incoming == 0 &&
                 voicemail ==0)

            # never mine about clients who have no activity.
            hourrange.each { |hour|
              phone.outgoing_call_count(today, hour, false, true, notify)
              phone.incoming_call_count(today, hour, false, true, notify)
              phone.incoming_voicemail_count(today, hour, false, true, notify)
              phone.incoming_abandoned_count(today, hour, false, true, notify)
            }
          end
        }
      }
    end
  end

  def do_daily_stats(options)
    options = { today:   Time.now,
      enddate: Time.now,
      notify:  false,
      hour:    nil,
      output:  $stdout}.merge!(options)

    today  = options[:today]
    enddate= options[:enddate]
    notify = options[:notify]
    output = options[:output]
    hour   = options[:hour]

    calc_daily_stats(today, hour, notify)

    # this needs to be adjusted for timezones!
    #
    daybegin = today.beginning_of_day.to_date
    dayend   = enddate.end_of_day.to_date
    if dayend.future?
      dayend = Time.now.yesterday.end_of_day.to_date
    end

    output.puts "Statisques quoitidiennes pour:, #{self.name}, (id: #{id})"
    output.puts "                          du: #{daybegin.to_s(:db)}, (#{$Days_of_week[daybegin.wday]})"
    output.puts "                           à: #{dayend.to_s(:db)}"
    output.puts "                pour l'heure: #{hour}" if hour
    output.puts "Généré le :, #{Time.now.to_s(:db)}, par, #{ENV['LOGNAME']}"

    if sites.activated.count > 1
      output.puts "\nRapports sur :, #{sites.activated.count}, emplacements"
    end

    day_count = 0
    (daybegin..dayend).each { |oneday|
      day_count += call_count(oneday.to_datetime, hour, notify)
    }

    output.puts "Nombre total d'appels:           #{day_count}"
    if false
      # these are not recorded as stats.
      btn_counts = Hash.new
      btn_counts.default = 0

      number_of_incoming_calls = 0
      number_of_outgoing_calls = 0
      self.btns.find_each { |btn|
        btn_in = btn.incoming_call_count(today, hour, false)
        btn_out= btn.outgoing_call_count(today, hour, false)
        btn_counts[btn.billingtelephonenumber] = btn_in + btn_out
        number_of_incoming_calls += btn_in
        number_of_outgoing_calls += btn_out
      }

      output.puts "Nombre total d'appels entrants: #{number_of_incoming_calls}"
      output.puts "Nombre total d'appels sortants: #{number_of_outgoing_calls}"

      output.puts "Break down by BillingTelephoneNumber"
      output.puts sprintf("  %-16s    %7s","number","calls")
      btn_counts.keys.sort.each { |number|
        callcount = btn_counts[number]
        output.puts sprintf("    %-16s    %7u", number, callcount)
      }
    end

    # set up some formats for the report.
    # good place to screw this into CSV format if you need that.
    #         /--------------------------------- extension
    #         |    /---------------------------- outgoing/sortant
    #         |    |   /------------------------ did answered
    #         |    |   |   /-------------------- huntgroup answered
    #         |    |   |   |   /---------------- went to vm
    #         |    |   |   |   |   /------------ abandoned
    #         |    |   |   |   |   |   /-------- internal call
    #         |    |   |   |   |   |   |   /---- vmcollected
    #         |    |   |   |   |   |   |       /- total
    fmt1a= "%-17s %9s %9s %9s %9s %9s %9s %9s %7s"
    fmt1b= "%10s [%4s]"
    fmt2a= "%-17s %9u %9u %9u %9u %9u %9u %9u "
    fmt2b= "%7s \"%s [%4u]\""

    # Keeping documentation above because it's so nice
    # However, we do need to implement
    # the option of different field seperators

    seperationType = 'comma'
    seperationType = ENV['SEPERATION'] if ENV['SEPERATION']

    fmt = ''
    case seperationType
        when 'space'
            # Nothing required because already set
        else
            # Default comma option
            fmt1a= "%s,%s,%s,%s,%s,%s,%s,%s,%s,"
            fmt1b= "%s [%4s]"
            fmt2a= "%s,%u,%u,%u,%u,%u,%u,%u,"
            fmt2b= "%s,\"%s [%4u]\""
    end


    # Combinining the two strings together
    fmt1 = fmt1a+fmt1b
    fmt2 = fmt2a + fmt2b

    # make a list of all handsets, then sort them, and show just them.
    handsets_total = Hash.new(0)
    huntgroup_phones = self.sortedHuntGroup()

    output.puts "\nRépartition par extension:"
    output.puts sprintf(fmt1a,
                        "", "", "SDA", "groupe",
		        "Boîte",   "",         "",        "Messages",        "");
    output.puts sprintf(fmt1,
                        "extension", "sortant", "répondu", "répondu",
		        "vocale", "Abandonné", "interne", "b. voc.", "total", "Nom","phone_id")

    huntgroup_phones.each { |phone|
      phone_out = 0;      incoming = 0;       voicemail_in = 0;
      internal  = 0;      vmcollected = 0;
      abandoned_in = 0;   phone_total = 0;
      did_in = 0; huntgroup_in = 0;

      range = daybegin..dayend
      range.each { |oneday|
        phone_out     += phone.outgoing_call_count(oneday, hour, false, true, notify)
        voicemail_in  += phone.incoming_voicemail_count(oneday, hour, false, true, notify)
        abandoned_in  += phone.incoming_abandoned_count(oneday, hour, false, true, notify)
	internal      += phone.internal_call_count(oneday, hour, false, true, notify)
	vmcollected   += phone.vmcollected_call_count(oneday, hour, false, true, notify)
	huntgroup_in  += phone.huntgroupin_call_count(oneday, hour, false, true, notify)
	did_in        += phone.didin_call_count(oneday, hour, false, true, notify)
        phone_total    = phone_out + voicemail_in + abandoned_in +
                         internal + vmcollected + did_in + huntgroup_in
      }
      extid = phone.identification
      output.puts sprintf(fmt2,
		          phone.extension,
                          phone_out, did_in, huntgroup_in, voicemail_in, abandoned_in, internal, vmcollected, phone_total, extid,
		          phone.id)

      handsets_total[:phone_out]    += phone_out
      handsets_total[:did_in]       += did_in
      handsets_total[:huntgroup_in] += huntgroup_in
      handsets_total[:voicemail_in] += voicemail_in
      handsets_total[:abandoned_in] += abandoned_in
      handsets_total[:internal]     += internal
      handsets_total[:vmcollected]  += vmcollected
    }

    output.puts "\n\n"
    output.puts sprintf(fmt1a,
                        "", "", "SDA", "groupe",
		        "Boîte",   "",         "",        "Messages",        "")
    output.puts sprintf(fmt1a,
                        "extension", "sortant", "répondu", "répondu",
		        "vocale", "Abandonné", "interne", "b. voc.", "")
    output.puts sprintf(fmt2a,
                        "TOTAL",      handsets_total[:phone_out],
                        handsets_total[:did_in], handsets_total[:huntgroup_in],
                        handsets_total[:voicemail_in],
                        handsets_total[:abandoned_in],
                        handsets_total[:internal], handsets_total[:vmcollected])

    #### now by site.

    site_total = Hash.new(0)
    output.puts ""

    self.sites.activated.each { |site|
      output.puts "\nRépartition par extension pour #{site.name}"
      output.puts sprintf(fmt1a,
                   "", "", "SDA", "groupe",
		   "Boîte",   "",         "",        "Messages",        "");
      output.puts sprintf(fmt1,
                   "extension", "sortant", "répondu", "répondu",
		   "vocale", "Abandonné", "interne", "b. voc.", "total", "Nom","phone_id")

      # this does lots of single record pulls, and could be sped up
      # by a) identifying three statsperiods involved, and then
      #       pull all records for the phones relating to a client,
      #       and then sorting by phone id.
      #    b) doing it all in one loop for all sites.
      site.phones.activated.by_extension.each { |phone|
        STDERR.printf "For #{phone.id}: " if ENV['DEBUG']
        phone_out = 0;      incoming = 0;       voicemail_in = 0;
        internal  = 0;      vmcollected = 0;
        abandoned_in = 0;   phone_total = 0;
        did_in = 0; huntgroup_in = 0;
        range = daybegin..dayend
        range.each { |oneday|
          STDERR.printf "Outgoing... "  if ENV['DEBUG']
          phone_out     += phone.outgoing_call_count(oneday, hour, false, true, notify)
          #incoming     += phone.incoming_call_count(oneday, hour, false, true, notify)

          STDERR.printf "voicemail... "  if ENV['DEBUG']
          voicemail_in  += phone.incoming_voicemail_count(oneday, hour, false, true, notify)

          STDERR.printf "abandoned... "  if ENV['DEBUG']
          abandoned_in  += phone.incoming_abandoned_count(oneday, hour, false, true, notify)

          STDERR.printf "internal... "  if ENV['DEBUG']
	  internal      += phone.internal_call_count(oneday, hour, false, true, notify)

          STDERR.printf "vmcollected... "  if ENV['DEBUG']
	  vmcollected   += phone.vmcollected_call_count(oneday, hour, false, true, notify)

          STDERR.printf "did... "  if ENV['DEBUG']
	  huntgroup_in  += phone.huntgroupin_call_count(oneday, hour, false, true, notify)

          STDERR.printf "hunt.. "  if ENV['DEBUG']
	  did_in        += phone.didin_call_count(oneday, hour, false, true, notify)

          phone_total    = phone_out + voicemail_in + abandoned_in +
                           internal + vmcollected + did_in + huntgroup_in

          STDERR.puts " " if ENV['DEBUG']

        }
        extid = phone.identification
        output.puts sprintf(fmt2,
		     phone.name,
                     phone_out, did_in, huntgroup_in, voicemail_in, abandoned_in, internal, vmcollected, phone_total, extid,
		     phone.id)

        site_total[:phone_out]    += phone_out
        site_total[:did_in]       += did_in
        site_total[:huntgroup_in] += huntgroup_in
        site_total[:voicemail_in] += voicemail_in
        site_total[:abandoned_in] += abandoned_in
        site_total[:internal]     += internal
        site_total[:vmcollected]  += vmcollected
      }

    }

    output.puts "\n\n"
      output.puts sprintf(fmt1a,
                   "", "", "SDA", "groupe",
		   "Boîte",   "",         "",        "Messages",        "")
      output.puts sprintf(fmt1a,
                   "extension", "sortant", "répondu", "répondu",
		   "vocale", "Abandonné", "interne", "b. voc", "")
    output.puts sprintf(fmt2a,
                        "TOTAL",      site_total[:phone_out],
                        site_total[:did_in], site_total[:huntgroup_in],
                        site_total[:voicemail_in],
                        site_total[:abandoned_in],
                        site_total[:internal], site_total[:vmcollected])

  end

  def sortedHuntGroup()
    huntgroup_phones = []
    self.sites.activated.each { |site|
      site.phones.activated.by_extension.each { |phone|
        if phone.virtualdid? and phone.huntgroup?
            huntgroup_phones << phone
        end
      }
    }
    huntgroup_phones.sort! { |a,b|
        case
            when a.extension && b.extension
                a.extension.to_i<=>b.extension.to_i
            # Sort by phone number if both are null
            when ! a.extension && ! b.extension
                a.phone_number <=> b.phone_number
            # Treat no extension as lowest value (thus extension non-nil as highest)
            when a.extension
                1
            when b.extension
                -1
        end
    }

    return huntgroup_phones
  end

  def incoming_cdr_count(today = Time.now, hour = nil, notify=false)
    ic = callstats.find_or_create_for_today(today, hour, 'IncomingCdrStat', notify)
    if ic.first
      return ic.first.count
    end
  end

  def outgoing_cdr_count(today = Time.now, hour = nil, notify=false)
    oc = callstats.find_or_create_for_today(today, hour, 'OutgoingCdrStat', notify)
    if oc.first
      return oc.first.count
    end
  end

  def cdr_count(today = Time.now, hour = nil, notify=false)
    incoming_cdr_count(today, hour, notify) + outgoing_cdr_count(today, hour, notify)
  end

  def call_count_exists?(today, hour)
    callstats.find_for_today(today, hour, "CallCallstat")
  end

  def return_zero_if_future(today, notify = false)
    if(today.to_date > (Time.now + 1.day).to_date)
      #$stderr.puts "returning 0 for future date: #{today}" if notify
      return true
    else
      return false
    end
  end

  def value_recount(recount, stats)
    sf = stats.first
    if sf
      #puts "count: #{sf.id} sp:#{sf.stats_period_id} type:#{sf.type} value: #{sf.value}"
      return sf.maybe_recount(recount)
    else
      #puts "value zero"
      return 0
    end
  end

  def call_count_or_zero(today = Time.now, hour = nil)
    return 0 if return_zero_if_future(today, false)

    value_recount(recount,
                  callstats.find_for_today(today, hour, "CallCallstat"))
  end

  def call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "CallCallstat", notify))
  end

  def outgoing_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "OutgoingCallStat", notify))
  end

  def incoming_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "IncomingCallStat", notify))
  end

  def answered_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "AnsweredCallStat", notify))
  end

  def huntgroupin_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "HuntGroupCallstat", notify))
  end

  def didin_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "DidCallstat", notify))
  end

  def voicemail_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "VoiceMailCallstat", notify))
  end

  def internal_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "InternalCallstat", notify))
  end

  def vmcollected_call_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "VMcollectedCallstat", notify))
  end


  def abandoned_count(today = Time.now, hour = nil, recount=false, zerodrop=true, notify=false)
    return 0 if return_zero_if_future(today, notify)
    value_recount(recount,
                  callstats.find_or_create_for_today(today, hour, "AbandonedCallstat", notify))
  end

  def call_max(today = Time.now)
    return @call_max if @call_max

    sp = StatsPeriod.find_or_make(:type => 'YearlyStatsPeriod',
				  :day => today.to_date.beginning_of_year)
    cs1 = self.callstats.peakCallCount(sp)
    if cs1.try(:first)
      @call_max = cs1.first.count
    else
      return 'not-available'
    end
  end

  def fmt_year_line(*stuff)
    num = stuff.size
    fmt = ""
    (1..num).each { fmt += ",%6s" }
    sprintf(fmt, *stuff)
  end

  def self.do_perclient_yearly_stats(year = Time.now.year)
    # get period for report
    period = YearlyStatsPeriod.by_year(year).first
    unless period
      period = YearlyStatsPeriod.by_year(year).build
    end

    daybegin = period.beginning_of_period.to_date
    dayend   = period.end_of_period.to_date

    alldays = (daybegin..dayend)

    if dayend > Time.now.to_date
      dayend = Time.now.to_date
    end

    puts "Per-Day Statistics for, all client, "
    puts "                from, #{daybegin.to_s(:db)}"
    puts "                  to, #{dayend.to_s(:db)}"
    puts "Generated on, #{Time.now.to_s(:db)}, by, #{ENV['LOGNAME']}"

    titles = alldays.to_a
    titles.unshift("Client")
    titles.unshift("Id")
    num = titles.size
    fmt = ""
    (1..num).each { fmt += "%12s," }
    puts sprintf(fmt, *titles)

    # with some SQL joints, this query can be very efficient, but it is
    # hard to get it right in rails scopes, but pulling big cuts out of
    # the callstats table should be easy.
    clientdata = Hash.new

    alldays.each { |day|
      dayofyear = day.yday
      dayperiod = StatsPeriod.find_with_hour(day, nil)
      if dayperiod
        dayperiod.callstats.where(:type => 'CallCallstat').find_each { |cs|
          clientdata[cs.client_id] ||= Array.new(367, 0)
          clientdata[cs.client_id][dayofyear] = cs.count
        }
      end
    }

    Client.order(:id).find_each { |c|
      print sprintf("%u,%20s,",c.id, c.name[0..19])

      carray = clientdata[c.id] || Array.new(367,0)
      alldays.each { |day|
	dayofyear = day.yday
        print sprintf("%12u,", carray[dayofyear])
      }

      #counts = c.callstats
      #	joins("join stats_periods AS sp1 on sp1.id = callstats.stats_period_id").
      #	where("sp1.type = 'DailyStatsPeriod'").
      #	where(:sp1 => { :day => [daybegin, dayend] })
      #
      #counts.find_each { |stat|
      #  print sprintf("%12u,",stat.count)
      #}
      print "\n"
    }
  end

  def do_year_stats(year = Time.now.year)
    # get period for report
    period = YearlyStatsPeriod.by_year(year).first
    unless period
      period = YearlyStatsPeriod.by_year(year).build
    end

    daybegin = period.beginning_of_period
    dayend   = period.end_of_period
    do_detailed_hourly_stats(self, daybegin, dayend)
  end

  def do_year_extension_stats(year = Time.now.year, phone)
    # get period for report
    period = YearlyStatsPeriod.by_year(year).first
    unless period
      period = YearlyStatsPeriod.by_year(year).build
    end

    daybegin = period.beginning_of_period
    dayend   = period.end_of_period
    do_detailed_hourly_stats(phone, daybegin, dayend)
  end

  def do_detailed_extension_stats(day = Time.now, phone)
    # get period for report

    daybegin = day.beginning_of_week.to_date
    dayend   = day.end_of_week.to_date

    # if phone is defined, use it, otherwise use client.
    phone ||= self
    do_detailed_hourly_stats(phone, daybegin, dayend)
  end

  # Calls on do_summary_stats to do summary stats for the client
  # Params:
  # - dayend: the last day the report should cover
  # - phone: Phone on which to run stats.  If nil, will run summary for all phones, by running
  #          it for all phones attached to the client object.
  # - daybegin: The first day the report should cover, if nil will be for 7 days previous to dayend
  def do_extension_stats(dayend = Time.now.to_date, phone = nil, daybegin = nil, hour = nil, notify = false, calcstats = Hash.new(0), output = $stdout)
    # get period for report

    # first walk through all calls and make sure that they have been classified.


    daybegin ||= dayend - 7
    phone    ||= self
    do_summary_stats(phone, daybegin, dayend, hour, notify, calcstats, output)
  end

  def calculate_period_totals(origin, range, hour = nil)
    notify = false
    zerodrop = false

    total_count = 0
    incoming_total_count =0
    outgoing_total_count =0
    internal_total_count =0

    range.each { |oneday|
      total_count += origin.call_count(oneday, hour, false, zerodrop, notify)
      incoming_total_count += origin.incoming_call_count(oneday, hour, false, zerodrop, notify)
      outgoing_total_count += origin.outgoing_call_count(oneday, hour, false, zerodrop, notify)
      internal_total_count += origin.internal_call_count(oneday, hour, false, zerodrop, notify)
      internal_total_count += origin.vmcollected_call_count(oneday, hour, false, zerodrop, notify)
    }
    return total_count, incoming_total_count, outgoing_total_count, internal_total_count
  end

  def report_period_totals(origin, range, hour = nil)
    total_count, incoming_total_count, outgoing_total_count, internal_total_count = calculate_period_totals(origin, range, hour)
    puts "Total pour tout les jours:                    #{total_count}\n"
    puts "Total d'appels entrants pour tous les jours:  #{incoming_total_count}\n"
    puts "Total d'appels sortants pour tous les jours:  #{outgoing_total_count}\n"
    puts "Total d'appels interne  pour tous les jours:  #{internal_total_count}\n"
  end


  # origin is a polymorphic input, it can be a phone or a client, as both accept the same
  # relationship to calls.
  def do_summary_stats(origin, daybegin, dayend, hour = nil, notify = false, calcstats = Hash.new(0), output = $stdout)
    zerodrop = false

    if dayend.future?
      dayend = Time.now.yesterday.end_of_day.to_date
    end
    daybegin = daybegin.to_date
    dayend   = dayend.to_date

    # insert a BOM so that Excel knows to do the right thing with accents.
    output.puts "\uFEFF"
    output.puts "Statistiques sommaires pour: #{self.name} (id: #{id})"
    output.puts "                         du: #{daybegin.to_s(:db)},(#{$Days_of_week[daybegin.wday]})"
    output.puts "                          à: #{dayend.to_s(:db)}"
    output.puts "               pour l'heure: #{hour}" if hour
    output.puts ""
    output.puts "Généré le:   #{Time.now.to_s(:db)}, par, #{ENV['LOGNAME']}"
    output.puts ""
    output.puts "Rapport pour: #{origin.name}\n"
    if sites.count > 1
      output.puts "\nRapports sur: #{sites.activated.count}, emplacements"
    end
    output.puts ""

    range = (daybegin)..(dayend)
    dayrange = daybegin..(dayend)
    dayrange.each { |day|
      starttime = Time.now
      $stderr.puts "#{starttime.to_s(:db)} day calculating #{day} #{hour}"
      count = calc_daily_stats(day, hour, notify)
      endtime   = Time.now
      elapsed   = endtime - starttime
      calcstats[:elapsed] += elapsed
      calcstats[:calc_times] += 1
      calcstats[:calc_call_count] = count
      $stderr.puts "    ..done #{endtime.to_s(:db)} (#{count})"
    }

    report_period_totals(origin, range, hour)

    seperationType = 'comma'
    seperationType = ENV['SEPERATION'] if ENV['SEPERATION']

    fmt = ''
    case seperationType
        when 'space'
            fmt  = "%-10s %10s  %7s %8s %9s |%3s %5s %6s %7s %10s | %7s %9s"
        else
            fmt  = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s"
    end

    output.puts sprintf(fmt, "", "", '',
                '',
                '',
                       '','', '',
                'Messages',
                '',
		        '',
                       'Appels');

    output.puts sprintf(fmt, "Jour", "Date", "Appels",
		       'Sortants',
		       'Entrants',
                       '','SDA', 'Appels collectifs',
		       'b. voc.',
		       'Abandonné',
                       'Internes',
                       'à la bv');

    counts = Hash.new(0)

    dayrange.each { |oneday|
      day_count = origin.call_count(oneday, hour, false, zerodrop, notify)

      total    = origin.call_count(oneday,  hour, false, zerodrop, notify)
      incoming = origin.incoming_call_count(oneday, hour, false, zerodrop, notify)
      didanswered = origin.didin_call_count(oneday, hour, false, zerodrop, notify)
      huntgroupin = origin.huntgroupin_call_count(oneday, hour, false, zerodrop, notify)
      answered    = didanswered + huntgroupin
      outgoing = origin.outgoing_call_count(oneday, hour, false, zerodrop, notify)
      voicemail= origin.voicemail_count(oneday, hour, false, zerodrop, notify)
      abandoned= origin.abandoned_count(oneday, hour, false, zerodrop, notify)
      internal = origin.internal_call_count(oneday, hour, false, zerodrop, notify)
      vmcollected= origin.vmcollected_call_count(oneday, hour, false, zerodrop, notify)
      intotal  = ""  # below can help with debug
      #intotal  = didanswered+huntgroupin+voicemail+abandoned

      #puts fmt_year_line(oneday.to_s(:db),
      output.puts sprintf(fmt, $Days_of_week[oneday.wday], oneday.to_date,
		   total, outgoing, incoming, intotal, didanswered, huntgroupin, voicemail, abandoned, internal, vmcollected);
      counts[:total]    += total
      counts[:answered] += answered
      counts[:outgoing] += outgoing
      counts[:incoming] += incoming
      counts[:huntgroupin] += huntgroupin
      counts[:didin]       += didanswered
      counts[:voicemail]+= voicemail
      counts[:abandoned]+= abandoned
      counts[:internal]    += internal
      counts[:vmcollected] += vmcollected
    }
    output.puts "\n"
    output.puts sprintf(fmt, "TOTAL", "",   counts[:total], counts[:outgoing], counts[:incoming],
                 '', counts[:didin], counts[:huntgroupin], counts[:voicemail], counts[:abandoned],
                 counts[:internal], counts[:vmcollected])
  end


  # Prints a legend explaning the column headers printed by do_summary_stats
  def self.print_summary_stats_legend()
    defHash = {
        "Appels" => "Nombre total d'appels",
        "Sortants" => "Appels de l'organisation dont la destination est à l'extérieur",
        "Entrants" => "Appels dont l'origine est à l'extérieur de l'organisation",
        "SDA" => "Sélection directe à l'arrivée. Dans ce contexte, le nombre d'appels qui sont allés directement à un combiné et pas un appel collectif",
        "Appels collectifs" => "Nombres d'appels qui est allé à la ligne collective",
        "Messages b. voc" => "Nombre d'appels qui ont été enregistré par la boîte vocale",
        "Abandonné" => "Appels dont l'appelant a raccroché avant que le message automatisé de la boîte vocale démarre",
        "Interne" => "Nombre d'appels dont l'origine et la destination sont à l'intérieur de l'organisation",
        "Appels à la bv" => "Nombres d'appels vers la boîte vocale pour, en autre, retirer les messages d'un utilisateur",
    }

    seperationType = 'comma'
    seperationType = ENV['SEPERATION'] if ENV['SEPERATION']

    fmt = ''
    case seperationType
        when 'space'
            longestTerm = defHash.keys.max_by{ |s| s.length }.length + 2
            fmt  = "%-#{longestTerm + 2}s: %s"

        # Default comma seperated
        else
            fmt  = "%s:,%s"
    end

    defHash.each { |key, value|
        printf(fmt + "\n", key, value)
    }; true

  end


  def do_detailed_hourly_stats(origin, daybegin, dayend)
    notify = false
    zerodrop = false

    if dayend.future?
      dayend = Time.now.yesterday.end_of_day.to_date
    end

    puts "Detailed Statistics for, #{self.name} (id: #{id})"
    puts "                 from, #{daybegin.to_s(:db)},(#{$Days_of_week[daybegin.wday]})"
    puts "                   to, #{dayend.to_s(:db)}"
    puts "Generated on, #{Time.now.to_s(:db)}, by, #{ENV['LOGNAME']}"
    puts ""
    puts "Reporting for: ,#{origin.name}\n"
    if sites.count > 1
      puts "\nReporting on, #{sites.count}, locations"
    end
    puts ""

    report_period_totals(origin, daybegin..dayend)

    puts fmt_year_line("Day", "Calls",
		       'Incoming',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
		       'Outgoing',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
		       'Voicemail',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
                       '','',
		       'Abandoned');

    puts fmt_year_line("Day", "Calls",
		       '06:00', 		       '07:00',
		       '08:00', 		       '09:00',
		       '10:00', 		       '11:00',
		       '12:00', 		       '13:00',
		       '14:00', 		       '15:00',
		       '16:00', 		       '17:00',
		       '18:00', 		       '19:00',
		       '20:00', 		       '21:00',
		       '22:00', 		       '23:00',
		       '24:00',
		       '06:00', 		       '07:00',
		       '08:00', 		       '09:00',
		       '10:00', 		       '11:00',
		       '12:00', 		       '13:00',
		       '14:00', 		       '15:00',
		       '16:00', 		       '17:00',
		       '18:00', 		       '19:00',
		       '20:00', 		       '21:00',
		       '22:00', 		       '23:00',
		       '24:00',
		       '06:00', 		       '07:00',
		       '08:00', 		       '09:00',
		       '10:00', 		       '11:00',
		       '12:00', 		       '13:00',
		       '14:00', 		       '15:00',
		       '16:00', 		       '17:00',
		       '18:00', 		       '19:00',
		       '20:00', 		       '21:00',
		       '22:00', 		       '23:00',
		       '24:00',
		       '06:00', 		       '07:00',
		       '08:00', 		       '09:00',
		       '10:00', 		       '11:00',
		       '12:00', 		       '13:00',
		       '14:00', 		       '15:00',
		       '16:00', 		       '17:00',
		       '18:00', 		       '19:00',
		       '20:00', 		       '21:00',
		       '22:00', 		       '23:00',
		       '24:00');
    incoming  = Array.new
    outgoing  = Array.new
    voicemail = Array.new
    abandoned = Array.new

    (daybegin..dayend).each { |oneday|
      day_count = origin.call_count(oneday, nil, false, zerodrop, notify)

      (6..24).each { |hour| incoming[hour]=0 }
      (6..24).each { |hour| outgoing[hour]=0 }
      (6..24).each { |hour| voicemail[hour]=0 }
      (6..24).each { |hour| abandoned[hour]=0 }
      if day_count > 0
	(6..24).each { |hour|

	  incoming[hour]  = origin.incoming_call_count(oneday, hour, false, zerodrop, notify)
	  outgoing[hour]  = origin.outgoing_call_count(oneday, hour, false, zerodrop, notify)
	  voicemail[hour] = origin.voicemail_count(oneday, hour, false, zerodrop, notify)
	  abandoned[hour] = origin.abandoned_count(oneday, hour, false, zerodrop, notify)
	}
      end

      puts fmt_year_line(oneday.to_s(:db),
			 origin.call_count(oneday, nil, false, zerodrop, notify),
			 incoming[6],  			 incoming[7],
			 incoming[8],  			 incoming[9],
			 incoming[10], 			 incoming[11],
			 incoming[12], 			 incoming[13],
			 incoming[14], 			 incoming[15],
			 incoming[16], 			 incoming[17],
			 incoming[18], 			 incoming[19],
			 incoming[20], 			 incoming[21],
			 incoming[22], 			 incoming[23],
			 incoming[24],
			 outgoing[6],  			 outgoing[7],
			 outgoing[8],  			 outgoing[9],
			 outgoing[10], 			 outgoing[11],
			 outgoing[12], 			 outgoing[13],
			 outgoing[14], 			 outgoing[15],
			 outgoing[16], 			 outgoing[17],
			 outgoing[18], 			 outgoing[19],
			 outgoing[20], 			 outgoing[21],
			 outgoing[22], 			 outgoing[23],
			 outgoing[24],
			 voicemail[6],voicemail[7],
			 voicemail[8],voicemail[9],
			 voicemail[10],voicemail[11],
			 voicemail[12],voicemail[13],
			 voicemail[14],voicemail[15],
			 voicemail[16],voicemail[17],
			 voicemail[18],voicemail[19],
			 voicemail[20],voicemail[21],
			 voicemail[22],voicemail[23],
			 voicemail[24],
			 abandoned[6],abandoned[7],
			 abandoned[8],abandoned[9],
			 abandoned[10],abandoned[11],
			 abandoned[12],abandoned[13],
			 abandoned[14],abandoned[15],
			 abandoned[16],abandoned[17],
			 abandoned[18],abandoned[19],
			 abandoned[20],abandoned[21],
			 abandoned[22],abandoned[23],
			 abandoned[24]
                         );
    }

  end


  def numbers()
    phoneNumbers = Set.new
    self.phones.each { |ph| phoneNumbers.add(ph.phone_number) ; }

    return phoneNumbers;
  end

  def do_weekly_stats(year = Time.now.year, phone = nil)
    # get period for report
    period = YearlyStatsPeriod.by_year(year).first
    unless period
      period = YearlyStatsPeriod.by_year(year).build
    end

    daybegin = period.beginning_of_period
    dayend   = period.end_of_period

    puts "Weekly Statistics for, #{self.name} (id: #{id})"
    puts "                 from, #{daybegin.to_s(:db)} (#{$Days_of_week[daybegin.wday]})"
    puts "                   to, #{dayend.to_s(:db)}"
    if phone
      puts "                   extension, #{phone.name}"
      phones = [phone]
    else
      phones = self.phones
    end
    puts "Generated on, #{Time.now.to_s(:db)}, by, #{ENV['LOGNAME']}"

    day_count = 0

    # possible 53 weeks in the year by ISO8601 specification of week number, counted from 1.
    phone_week = Hash.new(Array.new(54, 0))
    (daybegin..dayend).each { |oneday|
      weeknum = oneday.strftime("%V").to_i

      # could be done faster with SQL to yank an entire week of data at a time, by extension
      # and then populate the entire array.
      phones.each { |phone|
        count = phone.total_call_count(oneday, nil, false, false, true)
        phone_week[phone][weeknum] += count
        day_count += count
      }
    }

    puts "Total for all days:,#{day_count}\n"

    weeks = (0..53).to_a
    weeks[0]="Extension"

    puts fmt_year_line(*weeks)

    phones.each { |phone|
      week = phone_week[phone]
      week[0] = phone.name
      puts fmt_year_line(*week)
    }
  end

  def clienthq(sitename, deptnum = nil)
    if sitename.blank? or sitename == 'SS/HQ'
      if self.sites.count == 1
        site = self.sites.first

      else
        # this sequence tries to find a site with a useful name.
        ["Quebec", "Quebec, Montreal", "Quebec, Montreal",
                "Montreal, Quebec", "Montreal", "SS/HQ"].find {|sitename|
          site = self.sites.find_by_name(sitename)
        }
        unless site
          sitename="SS/HQ"
          site = self.sites.find_or_make(sitename)
        end
      end
    else
      site = self.sites.find_or_make(sitename)
    end
    site.site_number ||= deptnum || site_uniquify.to_s
    return site
  end

  def self.increment_stats_and_return(stats, statname)
    stats[statname] ||= 0
    stats[statname] += 1
    return false
  end
end
