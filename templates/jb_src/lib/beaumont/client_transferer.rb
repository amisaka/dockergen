module Beaumont::ClientTransferer

  # TODO: extract
  def transferto(newclient)
    maybelog("Transfering sites and btns from: #{self.name}/#{self.name}/#{self.id} to #{newclient.name}/#{newclient.id}")
    self.sites.each { |s|
      s.client = newclient
      s.save!
    }
    self.btns.each { |b|
      b.client = newclient
      b.save!
    }

    # think we will abandon the calls?
    if false
      self.calls.find_each { |c|
        c.client = newclient
        c.save!
      }
    end
  end
  
end
