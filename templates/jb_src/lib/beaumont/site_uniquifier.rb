module Beaumont::SiteUniquifier
  def site_uniquify
    highest_number = 1
    sitenumbers = Hash.new
    sites.each { |site|
      begin
        sitenum = site.site_number.to_i
        if sitenum >= highest_number
          highest_number = sitenum+1
        end
      end
    }

    sites.each { |site|
      sitenumber = site.site_number

      if sitenumbers[sitenumber]
        osite = sitenumbers[sitenumber]

        maybelog "client #{self.name}/\##{self.id} has conflict on sitenumber: #{sitenumber}. Site \##{site.id} vs \##{osite.id}"


        # conflict!!!
        if site.phones.count == 0
          site.delete
          site = osite
        elsif osite.phones.count == 0
          osite.delete
          sitenumbers[site.site_number] = site
        else
          # else, we need to renumber one of them.
          site.site_number = sprintf("%u_n", highest_number)
          site.save!
          sitenumbers[site.site_number] = site
          highest_number += 1
        end
      else
        sitenumbers[sitenumber] = site
      end
    }

    highest_number = site_renumber(highest_number)

    # return highest_number
    highest_number
  end
end
