module Beaumont::SiteCleaner
  def cleanoutdeadsites
    sitenames = Hash.new
    highestname = 0

    sites.each { |site|
      if site.phones.count == 0 and site.site_number.blank?
        if site.site_rep
          site.site_rep.delete
        end
        site.delete
      elsif !site.site_number.blank?
        if sitenames[site.site_number]
          # what repeated site_number? set to new one to unique name.
          sitenum = site.site_number
          newname = sprintf("%s_%d", sitenum, site.id)
          maybelog("   Client #{self.name}/#{self.id} has duplicate site_number (#{sitenum}): \##{site.id}, \##{sitenames[site.site_number].id} fixed to #{newname}")
          site.site_number = newname
          site.save!
        end
        sitenames[site.site_number] = site
        if site.site_number.to_i > highestname
          highestname = site.site_number.to_i
        end
      end
    }

    highestname
  end
end
