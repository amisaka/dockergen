module Statable

  # Returns the status of a client
  # @param now [DateTime] specify the time reference or defaults to Time.now
  #   it can be used to query the client status in the past or future
  # @return [Symbol] the status of the client , `:active`, `:closed`,
  #   `:pending_start`, `:pending_close`
  def status(now = Time.now)
    if termination_date.nil? && !activation_date.nil?
      if now < activation_date
        :pending_start
      else
        :active
      end
    elsif !termination_date.nil? && activation_date.nil?
      if now < termination_date
        :pending_close
      else
        :closed
      end
    elsif termination_date.nil? && activation_date.nil?
      # unknown if no dates are set
      :unknown
    elsif !termination_date.nil? && !activation_date.nil?
      if now < termination_date
        :pending_close
      else
        :closed
      end
    end
  end

  # Assign the status of a client
  #
  # @param val [Symbol, String] the value to set, `:active`, `:closed`, `:pending_start`,
  #   `:pending_close`
  # @return [Symbol] the value being set
  #
  def status=(val)
    set_status_with_dates(val)
  end

  # Assign the status of a client
  #
  # @param val [Symbol, String] the value to set, `:active`, `:closed`, `:pending_start`,
  #   `:pending_close`
  # @return [Symbol] the value being set
  #
  def set_status_with_dates(val, now = Time.now, h = {activation_date: nil, termination_date: nil})

    if self.class == Client
      btn.set_status_with_dates(val,now,h) unless btn.nil?
    end

    # handles only values of active, closed, pending_start or pending_close
    case val

    # we're setting the status to active
    when 'active', :active

      # an active client never has a termination_date date
      unless termination_date.nil?
        self.termination_date = nil
      end

      # set activation_date to now, or to optional activation_date
      if activation_date.nil? || activation_date > now
        if h[:activation_date].blank?
          self.activation_date = now
        else
          self.activation_date = h[:activation_date]
        end
      end

    # we're setting the status to active
    when 'closed', :closed

      # set termination_date to now or optional termination_date
      if termination_date.nil? || termination_date > now
        if h[:termination_date].blank?
          self.termination_date = now
        else
          self.termination_date = h[:termination_date]
        end
      end

    # we're setting the status to pending_close
    when 'pending_close', :pending_close

      # set termination_date to the end of the month or optional termination_date
      if termination_date.nil? || termination_date < now
        if h[:termination_date].blank?
          self.termination_date = now.end_of_month
        else
          self.termination_date = h[:termination_date]
        end
      end

    # we're setting the status to pending_start
    when 'pending_start', :pending_start

      # set activation_date to the beginning of next month or optional activation_date
      if activation_date.nil? || activation_date < now
        if h[:activation_date].blank?
          self.activation_date = (now + 1.month).beginning_of_month
        else
          self.activation_date = h[:activation_date]
        end
      end

    # we don't know how to handle other values
    else
      raise "Handling #{val} is not supported."
    end

    save!
  end
end
