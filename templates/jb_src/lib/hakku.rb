module Hakku
  autoload :Table, 'hakku/table'

  def self.sanitize(src_path, dst_path)
    headers = nil
    rows = []

    # row_lengths = {}

    File.open(src_path, 'rb:CP863:UTF-8') do |file|
      file.each_line do |line|
        begin
          # treat each line independently to rescue from CSV::MalformedCSVError
          # use windows format as row separator
          CSV.parse(line, col_sep: ',', row_sep: "\r\n") do |row|
            # ChargeAssignment rows should have length 46
            raise CSV::MalformedCSVError, "ChargeAssignment rows should have length 46 (line 0)" if row.length != 46

            # row_lengths[row.length] ||= []
            # row_lengths[row.length] << file.lineno

            # first line is headers
            if file.lineno == 1
              headers = row
            else
              rows << row
            end
            # puts ("\"%s\"," * row.length % row).gsub(/,$/,'') #{ |row| model.create! row.to_hash.delete_if{|k,v| k.blank?} }
          end
        rescue CSV::MalformedCSVError => e
          # the line number is wrong because of previous hack
          # it will always say the error is on line 1 or 2
          msg = e.message.gsub(/line \d/, "line #{file.lineno}")
          Rails.logger.error msg
          File.open(File.join(Rails.root, 'tmp', 'bad_records.txt'), "a+") {|f| f << msg << "\n" }
        end
      end
    end

    # puts row_lengths.map{|k,v| "#{k} size: #{v.length}"}
    # raise "interupt"

    # Sanitize all rows
    sanitizer_regex = /(\r|\n|"|'|,|\x00)+/
    headers.each do |h|
      h.gsub!(sanitizer_regex, '') unless h.nil?
    end

    # Remove newlines and carriage returns
    rows.each do |row|
      row.each do |r|
        r.gsub!(sanitizer_regex, '') unless r.nil?
      end
    end

    CSV.open(dst_path, 'wb:UTF-8', headers: headers, col_sep: ',', row_sep: "\n", quote_char: '"', write_headers: true, force_quotes: true) do |csv|
      rows.each do |r|
        csv << r
      end
    end
  end
end
