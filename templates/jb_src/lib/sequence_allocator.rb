module SequenceAllocator

  def self.included(klass)
    klass.extend(ClassMethods)
  end

  module ClassMethods
    # postgresql specific.
    def allocate_id(count)
      primaryidseq = "#{table_name}_#{primary_key}_seq"
      
      newvalues = connection.execute "select nextval('#{primaryidseq}') AS value;"
      oldvalue = (newvalues.try(:first)["value"]).to_i
      newvalue = oldvalue + count
      connection.execute "select setval('#{primaryidseq}', #{newvalue}, true);"
      oldvalue
    end
  end
end
