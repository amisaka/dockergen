module SiteExtension
  def find_or_make(name)
    client = proxy_association.owner
    site_key = Site.cache_name_key(client, name)

    Rails.cache.fetch site_key do
      name.strip!
      s1 = find_by_name(name)
      unless s1
        s1 = find_by_name(name.strip)
      end
      unless s1
        s1 = create(:name => name.strip)
        $stderr.puts "#{proxy_association.owner.name} created new site: #{name}" if $LOG_NEW_CLIENTS;
      end
      # if it's the first site, then make it the billing address.
      proxy_association.owner.billing_site ||= s1
      s1.save!(validate: false)
      s1
    end
  end
end
