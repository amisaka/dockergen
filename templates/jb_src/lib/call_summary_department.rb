# temporary class used to contain summaries of calls by type, for a Department.
# must increment the parent object.
class CallSummaryDepartment < CallSummary
  attr_accessor :parent

  def initialize(thing, client, parent = nil)
    super(thing, client)
    @parent = parent
  end

  def add_duration(nDuration)
    @duration += nDuration
    @parent.add_duration(nDuration)
  end
  def add_cost(nCost)
    @cost    += nCost
    @parent.add_cost(nCost)
  end
  def add_call
    @count += 1
    @parent.add_call
  end
end
