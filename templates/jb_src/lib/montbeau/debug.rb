require 'active_support/concern'

module Montbeau::Debug
  extend ActiveSupport::Concern

  def maybelog(msg)
    self.maybelog(msg)
  end

  class_methods do
    def maybelog(msg)
      errorlog(msg) if ENV['IMPORT_DEBUG']
    end

    def newlineneeded
      @@newlineneeded ||= false
    end

    def errorlog(msg)
      @@logoutput=true
      if not $NOLOG_TESTING
        $stdout.print "\n" if self.newlineneeded
        @@newlineneeded = false
        $stdout.puts msg
      end
    end
  end
end
