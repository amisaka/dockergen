require 'active_record/dumpable'

module Montbeau::Export::Exportable
  include ActiveRecord::Dumpable

  alias_method :export_to_csv, :dump_to_csv
end
