module Montbeau::ActivatableControllerHelpers

  def self.included(base)
    base.before_action :set_activatable_resource, only: [:activate, :deactivate]
  end

  def activate
    _activatable.activate
    respond_with(_activatable, location: request.referer)
  end

  def deactivate
    _deactivatable.deactivate
    respond_with(_deactivatable, location: request.referer)
  end

  protected

    def get_activatable_resource
      instance_variable_get(:"@#{controller_name.singularize}")
    end

    def set_activatable_resource
      instance_variable_set(:"@#{controller_name.singularize}", activatable_model.find(params[:"#{activatable_name}_id"]))
    end

    def activatable_model
      activatable_name.camelize.constantize
    end

    def activatable_name
      controller_name.singularize
    end

  private

    def _activatable
      get_activatable_resource
    end

    def _deactivatable
      get_activatable_resource
    end

end
