require 'timeout'
# this class runs the data migration strategy automatically for 20171125 data.
class Montbeau::Import::DataMigration20171208::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'

      # Remove old data
      ::Site.delete_all
      ::Building.delete_all

      # Reset sequences to ensure no errors from sequence ids
      ::ApplicationRecord.reset_all_sequences!

      # Override `Model.import_from_csv` with custom matcher
      ::Site.extend Montbeau::Import::DataMigration20171208::Site

      # Import data
      ::Site.import_from_csv "db/data/20171208/sites.csv"

      # Reset sequences to ensure it starts from the highest id on each table
      ::ApplicationRecord.reset_all_sequences!

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
