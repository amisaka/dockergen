require 'csv'

module Montbeau::Import::DataMigration20171213::TechnicalContact
  def import_from_csv(file)
    technical_contacts = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        technical_contacts << {
          client: find_client_or_nil(row['SG3ID']),
          name: row['Company'],
          email: row['Email']
        }
      end
    end

    technical_contacts.each do |technical_contact|
      begin
        ::TechnicalContact.create!(technical_contact)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, technical_contact)
        next
      end
    end
  end

  private

    def find_client_or_nil(id)
      ::Client.find(id)
    rescue ActiveRecord::ActiveRecordError
      nil
    end

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_technical_contacts.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
