require 'timeout'
# this class runs the data migration strategy automatically for 20171125 data.
class Montbeau::Import::DataMigration20171213::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'

      # Remove old data
      ::PhoneContact.delete_all
      ::EmailContact.delete_all
      ::TechnicalContact.delete_all

      # Reset sequences to ensure no errors from sequence ids
      ::ApplicationRecord.reset_all_sequences!

      # Override `Model.import_from_csv` with custom matcher
      ::PhoneContact.extend Montbeau::Import::DataMigration20171213::PhoneContact
      ::EmailContact.extend Montbeau::Import::DataMigration20171213::EmailContact
      ::TechnicalContact.extend Montbeau::Import::DataMigration20171213::TechnicalContact

      # Import data
      ::PhoneContact.import_from_csv "db/data/20171213/phone_contacts.csv"
      ::EmailContact.import_from_csv "db/data/20171213/email_contacts.csv"
      ::TechnicalContact.import_from_csv "db/data/20171213/technical_contacts.csv"

      # Reset sequences to ensure it starts from the highest id on each table
      ::ApplicationRecord.reset_all_sequences!

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
