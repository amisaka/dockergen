require 'csv'

module Montbeau::Import::DataMigration20171213::EmailContact
  def import_from_csv(file)
    email_contacts = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        email_contacts << {
          client: find_client_or_nil(row['SG3ID']),
          name: row['Description'],
          email: row['EAddress']
        }
      end
    end

    email_contacts.each do |email_contact|
      begin
        ::EmailContact.create!(email_contact)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, email_contact)
        next
      end
    end
  end

  private

    def find_client_or_nil(id)
      ::Client.find(id)
    rescue ActiveRecord::ActiveRecordError
      nil
    end

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_email_contacts.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
