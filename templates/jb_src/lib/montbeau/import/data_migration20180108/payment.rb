require 'csv'

module Montbeau::Import::DataMigration20180108::Payment
  def import_from_csv(file)
    payments = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        payments << {
          id: row['id'],
          client: ::Btn.find_by_billingtelephonenumber(row['btn']).client,
          date: row['date'],
          posted_date: row['posted_date'],
          amount: row['amount'].gsub('$', '').gsub(' ', '').gsub(',', '.').gsub("\u00A0", ""),
          balance: row['balance'].gsub('$', '').gsub(' ', '').gsub(',', '.').gsub("\u00A0", ""),
          type: row['type'].gsub(' ', ''),
          description: row['description'],
          reason: row['reason'],
          posted: row['posted'],
          imported: row['imported'],
          reference_number: row['reference_number'],
          check_number: row['check_number'],
          payment_method: row['payment_method'],

        }
      end
    end

    payments.each do |payment|
      begin
        ::Payment.create!(payment)
      rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => e
        File.open('log/import.log', "ab") do |f|
          f.puts "Could not import payment with ID=#{payment[:id]}"
          f.puts e.message
        end
        next
      end
    end
  end

end
