require 'csv'

module Montbeau::Import::DataMigration20171205::BillableService

  def import_from_csv(file)
    billable_services = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        billable_services << {
          # invoice: nil, # not in their data
          client: find_client_or_nil(row),
          service: find_or_create_service(row),
          qty: row['Quantity'],
          billing_period_started_at: datetime_or_nil(row['StartDate']),
          billing_period_ended_at: datetime_or_nil(last_charge_date_if_inactive(row)),
          # discount: row[''], # not in their data
          # comments: row[''], # not in their data
          totalprice: row['TotalPrice'],
          service_name: row['Code'],
          asset_id: find_phone_or_nil(row),
          foreign_pid: row['ChargeAssignmentPID'],
          unitprice: row['UnitPrice'],
          activated: row['Active'],
          activated_at: datetime_or_nil(row['AssignedOn']),
          deactivated_at: datetime_or_nil(last_charge_date_if_inactive(row)),
          billing_period: billing_period_from_recurrence(row)
        }
      end
    end

    billable_services.each do |billable_service|
      begin
        ::BillableService.create!(billable_service)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, billable_service)
        next
      end
    end

  end

  private

    def datetime_or_nil(dt)
      return nil if dt.blank?
      DateTime.parse(dt)
    rescue TypeError, ArgumentError
      begin
        DateTime.strptime(dt, "%m/%d/%Y")
      rescue TypeError, ArgumentError
        begin
          DateTime.strptime(dt, "%d/%m/%Y")
        rescue TypeError, ArgumentError
          nil
        end
      end
    end

    def billing_period_from_recurrence(row)
      if row['RecurrenceTypePID'] == '1'
        :yearly
      elsif row['RecurrenceTypePID'] == '2'
        :monthly
      else
        nil
      end
    end

    def last_charge_date_if_inactive(row)
      if row['Active'] == '0'
        row['LastChargeEndDate']
      else
        nil
      end
    end

    def find_client_or_nil(row)
      ::Client.find(row['SG3ID'])
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
      nil
    end

    def find_phone_or_nil(row)
      ::Phone.find_by(foreign_pid: row['ServicePID'])
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
      nil
    end

    def find_or_create_service(row)
      ::Service.find_or_create_by!(foreign_pid: row['ProductPID']) do |service|
        service.name = row['Code']
        service.description = row['Description']
        service.unitprice = row['UnitPrice']
      end
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
    end

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_billable_services.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
