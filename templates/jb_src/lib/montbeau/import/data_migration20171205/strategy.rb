require 'timeout'

# this class runs the data migration strategy automatically for 20171125 data.
class Montbeau::Import::DataMigration20171205::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'

      # Clean up all phones
      ::BillableService.delete_all
      ::Service.delete_all

      # Reset sequences to a clean state
      ::ApplicationRecord.reset_all_sequences!

      # load billable services data
      ::BillableService.extend ::Montbeau::Import::DataMigration20171205::BillableService
      ::BillableService.import_from_csv "db/data/20171205/billable_services.csv"

      # Reset sequences to ensure it starts from the highest id on each table
      ::ApplicationRecord.reset_all_sequences!

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
