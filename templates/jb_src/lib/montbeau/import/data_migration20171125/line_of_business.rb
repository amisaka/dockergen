require 'csv'

module Montbeau::Import::DataMigration20171125::LineOfBusiness

  def import_from_csv(file)
    lines_of_business = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        lines_of_business << {
          id:           row['LineOfBusinessPID'],
          code:         row['Code'],
          description:  row['Description']
        }
      end
    end

    LineOfBusiness.create(lines_of_business)
  end

  private

end
