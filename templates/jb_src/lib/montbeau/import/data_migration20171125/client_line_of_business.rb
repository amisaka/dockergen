require 'csv'

module Montbeau::Import::DataMigration20171125::ClientLineOfBusiness

  def import_from_csv(file)
    client_lines_of_business = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        if row['LoB01'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 1
          }
        end

        if row['LoB02'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 2
          }
        end

        if row['LoB03'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 3
          }
        end

        if row['LoB04'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 4
          }
        end

        if row['LoB05'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 5
          }
        end

        if row['LoB06'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 6
          }
        end

        if row['LoB07'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 7
          }
        end

        if row['LoB11'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 11
          }
        end

        if row['LoB12'] == '1'
          client_lines_of_business << {
            client_id: row['SG3ID'],
            line_of_business_id: 12
          }
        end
      end
    end

    ClientLineOfBusiness.create(client_lines_of_business)
  end

  private

end
