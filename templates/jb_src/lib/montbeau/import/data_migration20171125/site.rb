require 'csv'

module Montbeau::Import::DataMigration20171125::Site
  def import_from_csv(file)
    sites = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        sites << {
          id: row["SiteID"],
          client_id: row["SG3ID"],
          site_number: row["site_number"],
          name: row["SiteName"],
          building: build_building(row),
          activated_at: datetime_or_nil(row["activation_date"]),
          deactivated_at: datetime_or_nil(row["termination_date"]),
          support_service_level: row["support_service_level"]
        }
      end
    end

    sites.each do |site|
      begin
        ::Site.create!(site)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, site)
        next
      end
    end
  end

  private

    def datetime_or_nil(dt)
      return nil if dt.blank?
      DateTime.parse(dt)
    rescue TypeError, ArgumentError
      begin
        DateTime.strptime(dt, "%m/%d/%Y")
      rescue TypeError, ArgumentError
        begin
          DateTime.strptime(dt, "%d/%m/%Y")
        rescue TypeError, ArgumentError
          nil
        end
      end
    end

    def build_building(row)
      ::Building.new(
        address1: row["address1"],
        address2: row["address2"],
        city: row["city"],
        province: row["province"],
        country: row["country"],
        postalcode: row["postalcode"]
      )
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
    end

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_sites.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
