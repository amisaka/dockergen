require 'csv'

module Montbeau::Import::DataMigration20171125::Billing
  def import_from_csv(file)
    billings = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        billings << {
          client: find_client_by_btn(row['BTN']),
          balance: row['AccountBalance'],
          activated_at: datetime_or_nil(row['StartDate']),
          billing_start_date: datetime_or_nil(row['FirstStatementDate']),
          last_usage_date: datetime_or_nil(row['LastStatementDate']),
          last_statement_date: datetime_or_nil(row['LastStatementDate']),
          last_statement_amount: row['LastStatementAmount'],
          last_payment_amount: row['LastPaymentAmount'],
          last_payment_date: datetime_or_nil(row['LastPaymentDate']),
          deactivated_at: datetime_or_nil(row['TerminatedOn'])
        }
      end
    end

    billings.each do |billing|
      begin
        ::Billing.create!(billing)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, billing)
        next
      end
    end
  end

  private

    def datetime_or_nil(dt)
      return nil if dt.blank?
      DateTime.parse(dt)
    rescue TypeError, ArgumentError
      begin
        DateTime.strptime(dt, "%m/%d/%Y")
      rescue TypeError, ArgumentError
        begin
          DateTime.strptime(dt, "%d/%m/%Y")
        rescue TypeError, ArgumentError
          nil
        end
      end
    end

    def find_client_by_btn(btn)
      ::Btn.find_by_billingtelephonenumber(btn).client
    rescue NoMethodError
      nil
    end

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_billings.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
