require 'csv'

module Montbeau::Import::DataMigration20171125::SalesPerson

  def import_from_csv(file)
    sales_people = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        sales_people << {
          id: row['ID'],
          foreign_pid: row['CustomerGroupPID'],
          name: row['Description']
        }
      end
    end

    SalesPerson.create(sales_people)
  end

  private

end
