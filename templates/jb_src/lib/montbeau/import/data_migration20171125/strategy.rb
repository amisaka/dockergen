require 'timeout'
# this class runs the data migration strategy automatically for 20171125 data.
class Montbeau::Import::DataMigration20171125::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'

      # Remove old data
      ::Client.delete_all
      ::Btn.delete_all
      ::Site.delete_all
      ::Building.delete_all
      ::CrmAccount.delete_all
      ::SalesPerson.delete_all
      ::LineOfBusiness.delete_all
      ::ClientLineOfBusiness.delete_all
      ::Billing.delete_all

      # Reset sequences to ensure no errors from sequence ids
      ::ApplicationRecord.reset_all_sequences!

      # Override `Model.import_from_csv` with custom matcher
      ::Client.extend Montbeau::Import::DataMigration20171125::Client
      ::LineOfBusiness.extend Montbeau::Import::DataMigration20171125::LineOfBusiness
      ::ClientLineOfBusiness.extend Montbeau::Import::DataMigration20171125::ClientLineOfBusiness
      ::SalesPerson.extend Montbeau::Import::DataMigration20171125::SalesPerson
      ::Billing.extend Montbeau::Import::DataMigration20171125::Billing
      ::Site.extend Montbeau::Import::DataMigration20171125::Site

      # Import data
      ::SalesPerson.import_from_csv "db/data/20171125/sales_people.csv"
      ::LineOfBusiness.import_from_csv "db/data/20171125/lines_of_business.csv"
      ::Client.import_from_csv "db/data/20171125/clients.csv"
      ::ClientLineOfBusiness.import_from_csv "db/data/20171125/clients_lines_of_business.csv"
      ::Billing.import_from_csv "db/data/20171125/billings.csv"

      # Ugly strategy to import sites -- easier this way...
      ::Site.delete_all
      ::Building.delete_all
      ActiveRecord::Base.connection.reset_pk_sequence!('sites')
      ActiveRecord::Base.connection.reset_pk_sequence!('buildings')
      ::Site.import_from_csv "db/data/20171125/sites.csv"

      # Reset sequences to ensure it starts from the highest id on each table
      ::ApplicationRecord.reset_all_sequences!

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
