To preserve the documentation of migrations and data correction, I'm continuing creating these directories as I import and correct data.

However, for some data correction, we will simply run in pry from console rather than create a class.   But we will document here.

Billing data is kept in a seperate table from payments and invoices.

Today we will run Client.updateAllBilling to update the biling tables.
