require 'csv'

module Montbeau::Import::DataMigration20180115::Billing
  def import_from_csv(file)
    puts "Test change"; sleep 1;
    billings = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        btn = row['BTN']
        clientFound =  find_client_by_btn(btn)
        loadId = row['LoadID']
        if clientFound.nil?
            log_import_error( "No client found for loadId #{loadId}, btn #{btn}", "line: #{__LINE__}")
        else
            if clientFound.class.name == "Array" and clientFound.count >= 1
                #if clientFound.count == 1
                if clientFound.count > 1
                    log_import_error("#{clientFound.count} Client found for loadId #{loadId}, btn #{btn}", "line #{__LINE__}, taking first from #{clientFound.to_s}", clientFound)
                end
                clientFound = clientFound[0]
            end
	        billings << {
	            # Load ID?
	            # Status?
	          id: loadId,
	          client: clientFound,
	          balance: row['Balance'],
	          activated_at: datetime_or_nil(row['StartDate']),
	          billing_start_date: datetime_or_nil(row['BillingStartDate']),
	          last_usage_date: datetime_or_nil(row['LastStatementDate']),
	          last_statement_date: datetime_or_nil(row['LastStatementDate']),
	          last_statement_amount: row['LastStatementAmount'],
	          last_payment_amount: row['LastPaymentAmount'],
	          last_payment_date: datetime_or_nil(row['LastPaymentDate']),
	          deactivated_at: datetime_or_nil(row['TerminatedOnDate'])
	              # LegID: Ignored
	              # :activated
	        }
        end
      end
    end

    billings.each do |billing|
      begin
        ::Billing.create!(billing)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, "line: #{__LINE__}", billing)
        next
      end
    end

    log_import_error("test logging", "line: #{__LINE__}")
  end

  private

    def datetime_or_nil(dt)
      return nil if dt.blank?
      DateTime.parse(dt)
    rescue TypeError, ArgumentError
      begin
        DateTime.strptime(dt, "%m/%d/%Y")
      rescue TypeError, ArgumentError
        begin
          DateTime.strptime(dt, "%d/%m/%Y")
        rescue TypeError, ArgumentError
          nil
        end
      end
    end

    def find_client_by_btn(btn)
      btnObj = ::Btn.find_by_billingtelephonenumber(btn)
      if btnObj.nil?
        return nil
      else
        client = btnObj.client
        #log_import_error("Class is #{client.class.name}", "line #{__LINE__}")
        if client.class.name == "Client"
           return client
        elsif client.class.name == "Array"
            log_import_error("WARNING: more than one client for btn #{btn}", "line: #{__LINE__}") if client.count > 1
           return btnObj.client[0]
        else
            log_import_error("Don't know what to do with a #{client.class.name}", "line: #{__LINE__}")
           return nil
        end
      end
    end

    def log_import_error(error, row = nil, model = nil, nothing = nil)
      File.open('log/import_billings.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        if error.class.name == "String"
            f.puts error
        else
            f.puts error.message
        end
        f.puts "------------------"
      end
    end

end
