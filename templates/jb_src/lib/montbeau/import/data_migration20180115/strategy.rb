require 'timeout'
# this class runs the data migration strategy automatically for 20180108 data.
class Montbeau::Import::DataMigration20180115::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'
      ActiveRecord::Base.connection.execute("TRUNCATE billings RESTART IDENTITY")
      ::Billing.extend Montbeau::Import::DataMigration20180115::Billing

      # Import data
      ::Billing.import_from_csv "db/data/20180115/billing.csv"

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
