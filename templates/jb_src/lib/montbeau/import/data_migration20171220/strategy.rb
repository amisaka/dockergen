require 'timeout'
# this class runs the data migration strategy automatically for 20171125 data.
class Montbeau::Import::DataMigration20171220::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'
      ::Client.extend Montbeau::Import::DataMigration20171220::Client

      # Import data
      ::Client.import_from_csv "db/data/20171220/clients.csv"

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
