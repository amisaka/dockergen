require 'csv'

module Montbeau::Import::DataMigration20171220::Client

  def import_from_csv(file)
    clients = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        clients << {
          # id: row['SG3ID'],
          btn: find_or_create_btn(row),
          name: row['Name'],
          crm_account: find_or_create_crm_account(row),
          billing_site: @site = find_or_create_site(row),
          sites: [@site],
          language_preference: row['Language'],
          status: row["Status"].gsub(' ', '_').downcase,
          sales_person: find_sales_person_or_nil(row),
          service_level: row['StarRating'],
          needs_review: row['NeedsReview']
        }
      end
    end

    clients.each do |client|
      begin
        ::Client.create!(client)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, client)
        next
      end
    end

  end

  private

    def find_sales_person_or_nil(row)
      SalesPerson.find_by(foreign_pid: row['SalesPersonID'])
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
      nil
    end

    def find_or_create_btn(row)
      ::Btn.find_or_create_by!(billingtelephonenumber: row['BTN']) do |btn|
        btn.billingtelephonenumber = row['BTN']
        btn.invoice_template = find_or_create_invoice_template(row)
      end
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
      nil
    end

    def find_or_create_invoice_template(row)
      invoice_template = ::InvoiceTemplate.find_by_invoice_cycle(row['Cycle'])
      if invoice_template.nil?
        invoice_template = ::InvoiceTemplate.create(invoice_cycle: row['Cycle'])
      end
      invoice_template
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
    end

    def find_or_create_crm_account(row)
      ::CrmAccount.find_or_create_by!(id: row['BTN']) do |crm_account|
        crm_account.id = row['BTN']
        crm_account.name = row['Name']
        crm_account.billing_address_street = row['billing_address_street']
        crm_account.billing_address_city = row['billing_address_city']
        crm_account.billing_address_state = row['billing_address_state']
        crm_account.billing_address_postalcode = row['billing_address_postalcode']
        crm_account.billing_address_country = row['billing_address_country']
      end
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
    end

    def find_or_create_site(row)
      ::Site.find_or_create_by!(client_id: row['SG3ID']) do |site|
        site.client_id = row['SG3ID']
        site.name = "Billing"
        site.site_number = "0"
        if site.building
          site.building.address1 = row["billing_address_street"]
          site.building.city = row["billing_address_city"]
          site.building.province = row["billing_address_state"]
          site.building.postalcode = row["billing_address_postalcode"]
          site.building.country = row['billing_address_country']
        else
          site.building = ::Building.new(
            address1: row["billing_address_street"],
            city: row["billing_address_city"],
            province: row["billing_address_state"],
            postalcode: row["billing_address_postalcode"],
            country: row['billing_address_country']
          )
        end
        site.building.save!
      end
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
    end

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_clients.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
