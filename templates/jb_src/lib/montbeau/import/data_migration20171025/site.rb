require 'csv'

# @deprecated this was used to import sites widget data as prepared by merg on date 2017-10-25
module Montbeau::Import::DataMigration20171025::Site
  def import_from_csv(file)
    sites = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        sites << {
          id: row["SiteID"],
          client_id: row["SG3ID"],
          #  row["BTN"])],
          # row["ClientName"],
          site_number: row["site_number"],
          name: row["SiteName"],
          building: Building.create({
            # id: row["building_id"],
            address1: row["address1"],
            address2: row["address2"],
            city: row["city"],
            province: row["province"],
            country: row["country"],
            postalcode: row["postalcode"]
          }),
          activation_date: row["activation_date"],
          termination_date: row["termination_date"],
          support_service_level: row["support_service_level"]
        }
      end
    end

    sites.each do |site|
      begin
        ::Site.create(site)
      rescue ActiveRecord::RecordNotFound, ActiveRecord::RecordNotUnique => e
        File.open('log/import.log', "ab") do |f|
          f.puts "Could not append #{site}"
          f.puts e.message
        end
        next
      end
    end
  end
end
