require 'csv'

# @deprecated this specific data migration was never ran, use a later date instead
module Montbeau::Import::DataMigration20171107::Client

  # not implemented
  def import_from_csv(file)
    raise NotImplementedError, 'import_from_csv is not implemented, use update_from_csv'
  end

  # this contains specific code to data was was valid on date of 2017-10-25
  # if used at a later date, update the following code
  def update_from_csv(file)
    clients = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        clients << {
          # row['LoadID'], # What is this?
          id: row['SG3ID'],
          # row['LegID'], # What is this?
          btns: [ find_or_create_btn(row) ],
          name: row['Name'],
          # row['PreviousName'], # We don't have a db column or use for this column
          crm_account: find_or_create_crm_account(row),
          billing_site: @site = find_or_create_site(row),
          # sites: [@site],
          language_preference: row['Language'],
          status: row["Status"].gsub(' ', '_').downcase,
          sales_person_id: row['SalesPersonID'],
          service_level: row['StarRating'],
          # row['IsInvoiceable'],
          needs_review: row['NeedsReview']
          # row['IsValidated']
        }
      end
    end

    clients.each do |client|
      begin
        if ::Client.exists? client[:id]
          ::Client.find(client[:id]).update!(client)
        else
          ::Client.create!(client)
        end
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e)
        next
      end
    end
  end

  private

    def find_or_create_btn(row)
      ::Btn.find_or_create_by!(billingtelephonenumber: row['BTN']) do |btn|
        btn.billingtelephonenumber = row['BTN']
        btn.invoice_template = ::InvoiceTemplate.find_by_invoice_cycle!(row['Cycle'])
      end
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row)
    end

    def find_or_create_crm_account(row)
      ::CrmAccount.find_or_create_by!(id: row['BTN']) do |crm_account|
        crm_account.id = row['BTN']
        crm_account.name = row['Name']
        crm_account.billing_address_street = row['billing_address_street']
        crm_account.billing_address_city = row['billing_address_city']
        crm_account.billing_address_state = row['billing_address_state']
        crm_account.billing_address_postalcode = row['billing_address_postalcode']
      end
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row)
    end

    def find_or_create_site(row)
      ::Site.find_or_create_by!(client_id: row['SG3ID']) do |site|
        site.client_id = row['SG3ID']
        site.name = "Billing"
        site.site_number = "0"
        if site.building
          site.building.address1 = row["billing_address_street"]
          site.building.city = row["billing_address_city"]
          site.building.province = row["billing_address_state"]
          site.building.postalcode = row["billing_address_postalcode"]
          site.building.country = "Canada"
        else
          site.building = ::Building.new(
            address1: row["billing_address_street"],
            city: row["billing_address_city"],
            province: row["billing_address_state"],
            postalcode: row["billing_address_postalcode"],
            country: "Canada"
          )
        end
        site.building.save!
      end
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row)
    end

    def log_import_error(error, row = nil)
      File.open('log/import_clients.log', "ab") do |f|
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts error.message
      end
    end

end
