require 'timeout'
# this class runs the data migration strategy automatically for 20171125 data.
class Montbeau::Import::DataMigration20171204::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'

      # Clean up
      ::Phone.delete_all
      ::PhoneServiceType.delete_all

      # Reset sequences to a clean state
      ::ApplicationRecord.reset_all_sequences!

      # Override `Model.import_from_csv` with our custom matchers
      ::Phone.extend Montbeau::Import::DataMigration20171204::Phone
      ::PhoneServiceType.extend Montbeau::Import::DataMigration20171204::PhoneServiceType

      # Import all data migrations
      ::Phone.import_from_csv "db/data/20171204/phones.csv"
      ::PhoneServiceType.import_from_csv "db/data/20171204/phone_service_types.csv"

      # Reset sequences to ensure it starts from the highest id on each table
      ::ApplicationRecord.reset_all_sequences!

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
