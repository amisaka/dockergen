require 'csv'

module Montbeau::Import::DataMigration20171204::PhoneServiceType

  def import_from_csv(file)
    phone_service_types = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        phone_service_types << {
          id: row['ServiceTypePID'],
          code: row['Code'],
          description: row['Description'],
          active: row['Active']
        }
      end
    end

    phone_service_types.each do |phone_service_type|
      begin
        ::PhoneServiceType.create!(phone_service_type)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, phone_service_type)
        next
      end
    end

  end

  private

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_phone_service_types.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
