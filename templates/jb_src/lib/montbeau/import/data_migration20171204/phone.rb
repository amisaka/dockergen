require 'csv'

module Montbeau::Import::DataMigration20171204::Phone

  def import_from_csv(file)
    phones = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        phones << {
          btn: find_or_nil_btn(row),
          foreign_pid: row['CustomerPID'],
          department_pid: row['DepartmentPID'],
          phone_number: row['ServiceID'],
          activated_at: datetime_or_nil(row['StartDate']),
          deactivated_at: datetime_or_nil(row['TerminationDate']),
          phone_service_type_id: row['ServiceTypePID'],
          display_text: row['DisplayText'],
          description: row['Description'],
          line_of_business: find_or_nil_line_of_business(row),
          couverture: row['Couverture']
        }
      end
    end

    phones.each do |phone|
      begin
        ::Phone.create!(phone)
      rescue ActiveRecord::ActiveRecordError => e
        log_import_error(e, nil, phone)
        next
      end
    end

  end

  private

    def datetime_or_nil(dt)
      return nil if dt.blank?
      DateTime.parse(dt)
    rescue TypeError, ArgumentError
      begin
        DateTime.strptime(dt, "%m/%d/%Y")
      rescue TypeError, ArgumentError
        begin
          DateTime.strptime(dt, "%d/%m/%Y")
        rescue TypeError, ArgumentError
          nil
        end
      end
    end

    def find_or_nil_btn(row)
      find_or_nil_client(row).btn
    rescue NoMethodError => e
      log_import_error(e, row, nil)
      nil
    end

    def find_or_nil_client(row)
      ::Client.find(row['SG3ID'])
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
      nil
    end

    def find_or_nil_line_of_business(row)
      ::LineOfBusiness.find(row['LineOfBusinessPID'])
    rescue ActiveRecord::ActiveRecordError => e
      log_import_error(e, row, nil)
      nil
    end

    def log_import_error(error, row = nil, model = nil)
      File.open('log/import_phones.log', "ab") do |f|
        f.puts DateTime.now
        f.puts "Error importing around line no : #{$.}" unless row.nil?
        f.puts row unless row.nil?
        f.puts model unless model.nil?
        f.puts error.message
        f.puts "------------------"
      end
    end

end
