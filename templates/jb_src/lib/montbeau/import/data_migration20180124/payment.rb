require 'csv'

module Montbeau::Import::DataMigration20180124::Payment
  def convert_to_date(d)
    begin
      return DateTime.strptime(d, '%d-%m-%Y')
    rescue TypeError
      return d
    end
  end

  def import_from_csv(file)
    payments = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        # if row['BTN'] == "5142287384"
        payments << {
          # id: row['id'],
          client: ::Btn.find_by_billingtelephonenumber(row['BTN']).client,
          date: convert_to_date(row['ReceiptDate']), #, '%d-%m-%Y') || "", #row['Date'],
          posted_date: convert_to_date(row['PostedDateTime']), #'%d-%m-%Y') || "", #row['PostedDateTime'],
          amount: row['Amount'], #.gsub('$', '').gsub(' ', '').gsub(',', '.').gsub("\u00A0", ""),
          balance: row['Balance'], #.gsub('$', '').gsub(' ', '').gsub(',', '.').gsub("\u00A0", ""),
          type: row['TransactionType'].gsub(' ',''),
          description: row['Description'],
          reason: row['CreditMemoReason'],
          posted: row['Posted'],
          imported: row['imported'],
          reference_number: row['ReferenceNumber'],
          check_number: row['CheckNo'],
          payment_method: row['DebitMemoReasonPID'],
          created_at: convert_to_date(row['CreatedOn']), #row['CreatedOn'],
          GLAccount: row['GLAccount']

        }
        # end
      end
    end

    payments.each do |payment|
      begin
          ::Payment.create!(payment)

      rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => e
        File.open('log/import.log', "ab") do |f|
          f.puts "Could not import payment with ID=#{payment[:id]}"
          f.puts e.message
        end

        # begin
        #   ::Payment.update(payment)
        # rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => ee
        #   File.open('log/import.log', "ab") do |f|
        #     f.puts "Could not update payment with ID=#{payment[:id]}"
        #     f.puts ee.message
        #   end

        next
      end
    end
  end

end
