require 'timeout'
# this class runs the data migration strategy automatically for 20180108 data.
class Montbeau::Import::DataMigration20180124::Strategy
  def migrate
    puts "Destructive operation, proceed? Type 'Y' within 5 seconds to continue, or anything else to cancel."
    prompt = ::Timeout::timeout(5) { gets.chomp }
    if prompt == 'Y'
      ::Payment.extend Montbeau::Import::DataMigration20180124::Payment

      # Import data
      ::Payment.import_from_csv "db/data/20180124/payments.csv"

      true
    else
      false
    end
  rescue ::Timeout::Error
    false
  end
end
