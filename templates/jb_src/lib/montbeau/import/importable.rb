require 'active_record/loadable'

module Montbeau::Import::Importable
  include ActiveRecord::Loadable

  alias_method :import_from_csv, :load_from_csv
end
