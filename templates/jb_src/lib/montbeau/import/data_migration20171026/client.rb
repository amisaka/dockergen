require 'csv'

# @deprecated this was used to import sites widget data as prepared by merg on date 2017-10-26
module Montbeau::Import::DataMigration20171026::Client
  def import_from_csv(file)
    clients = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        clients << {
          id: row["SG3ID"],
          btns: [Btn.create!(billingtelephonenumber: row["BTN"])],
          name: row["Name"],
          # prev_name: row["PreviousName"],
          crm_account: CrmAccount.create!({
            id: row["BTN"],
            name: row["Name"],
            billing_address_street: row["billing_address_street"],
            billing_address_city: row["billing_address_city"],
            billing_address_state: row["billing_address_state"],
            billing_address_postalcode: row["billing_address_postalcode"]
          }),
          billing_site: @site = ::Site.create!({
            name: "Billing",
            site_number: "0",
            building: Building.create!({
              address1: row["billing_address_street"],
              city: row["billing_address_city"],
              province: row["billing_address_state"],
              postalcode: row["billing_address_postalcode"],
              country: "Canada"
            })
          }),
          sites: [@site],
          language_preference: row["Language"],
          # status: row["Status"],
          # cycle: row["Cycle"],
          # sales_person_id: row["SalesPersonID"],
          # star_rating: row["StarRating"],
          # is_invoiceable: row["IsInvoiceable"],
          needs_review: row["NeedsReview"],
          # is_validated: row["IsValidated"]
        }
      end
    end

    clients.each do |client|
      begin
        ::Client.create!(client)
      rescue ActiveRecord::RecordNotUnique => e
        File.open('log/import.log', "ab") do |f|
          f.puts "Could not append #{client}"
          f.puts e.message
        end
        next
      end
    end
  end

  # this contains specific code to data was was valid on date of 2017-10-25
  # if used at a later date, update the following code
  def update_from_csv(file)
    clients = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        clients << {
          id: row["SG3ID"],
          status: row["Status"].gsub(' ', '_').downcase,
          cycle: row["Cycle"],
          sales_person_id: row["SalesPersonID"],
          # star_rating: 1, # they're all 1s, but I have no idea what star rating is
          needs_review: false, # they're all 0s
          human_validation_date: nil # they're all 0s
        }
      end
    end

    clients.each do |client|
      begin
        # Find corresponding client
        real_client = ::Client.find(client[:id])

        # Find the invoice cycle (invoice template)
        cid = client.delete(:cycle)
        cycle = ::InvoiceTemplate.find_by_invoice_cycle(cid)

        # Find the clients btn (all clients should only have 1 btn at this point)
        real_btn = real_client.btns.first
        if !real_btn.nil?
          real_btn.invoice_template = cycle
          real_btn.save!
        end

        # Save changes
        real_client.update!(client)
      rescue ActiveRecord::RecordNotFound
        next
      end
    end
  end

end
