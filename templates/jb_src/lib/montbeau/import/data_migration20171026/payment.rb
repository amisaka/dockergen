require 'csv'

module Montbeau::Import::DataMigration20171026::Payment
  def import_from_csv(file)
    payments = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        payments << {
          id: row['id'],
          client: ::Btn.find_by_billingtelephonenumber(row['btn']).client,
          date: row['date'],
          posted_date: row['posted_date'],
          amount: row['amount'].gsub('$', ''),
          balance: row['balance'].gsub('$', ''),
          type: row['type'].gsub(' ', ''),
          description: row['description'],
          reason: row['reason'],
          posted: row['posted'],
          imported: row['imported']
        }
      end
    end

    payments.each do |payment|
      begin
        ::Payment.create!(payment)
      rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => e
        File.open('log/import.log', "ab") do |f|
          f.puts "Could not import payment with ID=#{payment[:id]}"
          f.puts e.message
        end
        next
      end
    end
  end

end
