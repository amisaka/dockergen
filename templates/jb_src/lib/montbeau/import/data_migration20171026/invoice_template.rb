require 'csv'

module Montbeau::Import::DataMigration20171026::InvoiceTemplate
  def update_from_csv(file)
    invoice_templates = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        invoice_templates << {
          invoice_cycle: row['Billing Cycle'],
          name: row['Description']
        }
      end
    end

    invoice_templates.each do |invoice_template|
      real_invoice_template = ::InvoiceTemplate.find_by_invoice_cycle(invoice_template[:invoice_cycle]) unless invoice_template[:invoice_cycle].blank?
      real_invoice_template.update!(invoice_template) unless real_invoice_template.nil?
    end
  end


end
