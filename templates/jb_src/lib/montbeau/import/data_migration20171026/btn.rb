require 'csv'

module Montbeau::Import::DataMigration20171026::Btn
  def update_from_csv(file)
    btns = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        btns << {
          id: row['id'],
          billingtelephonenumber: (row['billingtelephonenumber'].blank? ? nil : row['billingtelephonenumber'].gsub(/\A(\+)?1/,'')),
          created_at: row['created_at'],
          updated_at: row['updated_at'],
          start_date: row['start_date'],
          end_date: row['end_date'],
          rate_plan_id: row['rate_plan_id'],
          lastresort: row['lastresort'],
          local_rate_plan_id: row['local_rate_plan_id'],
          national_rate_plan_id: row['national_rate_plan_id'],
          world_rate_plan_id: row['world_rate_plan_id'],
          rate_center_id: row['rate_center_id'],
          invoice_template_id: row['invoice_template_id'],
          department_invoice: row['department_invoice'],
          invoicing_enabled: row['invoicing_enabled']
        }
      end
    end

    btns.each do |btn|
      real_btn = ::Btn.find_by_billingtelephonenumber(btn[:billingtelephonenumber]) unless btn[:billingtelephonenumber].blank?
      real_btn.update!(btn) unless real_btn.nil?
    end
  end

end
