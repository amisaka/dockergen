require 'csv'

module Montbeau::Import::DataMigration20171026::Invoice
  def import_from_csv(file)
    invoices = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        invoices << {
          id: row['id'],
          client_id: row['client_id'],
          document: nil,
          amount: calc_amount(row['services_subtotal'], row['charges_subtotal']),
          total: parse_total(row['amount_invoiced']),
          tax_province: nil,
          tax_country: nil,
          created_at: row['created_at'],
          updated_at: row['updated_at']
        }
      end
    end

    invoices.each do |invoice|
      begin
        ::Invoice.create!(invoice)
      rescue ActiveRecord::RecordNotFound => e
        File.open('log/import.log', "ab") do |f|
          f.puts "Could not append #{invoice}"
          f.puts e.message
        end
        next
      end
    end
  end

  private

    def calc_amount(services_subtotal, charges_subtotal)
      st = BigDecimal.new(services_subtotal || 0)
      ct = BigDecimal.new(charges_subtotal || 0)
      BigDecimal.new(st + ct)
    end

    def parse_total(amount_invoiced)
      BigDecimal.new(amount_invoiced || 0)
    end

end
