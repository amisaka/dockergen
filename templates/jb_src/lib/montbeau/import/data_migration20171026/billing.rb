require 'csv'

# @deprecated this was used to import sites widget data as prepared by merg on date 2017-10-26
module Montbeau::Import::DataMigration20171026::Billing
  def import_from_csv(file)
    billings = []

    CSV.open(file, 'rb', headers: :first_row, encoding: 'UTF-8') do |csv|
      csv.each do |row|
        billings << {
          id: row['id'],
          client: find_client_by_btn(row['btn']),
          balance: row['balance'],
          start_date: row['start_date'],
          billing_start_date: row['billing_start_date'],
          last_usage_date: row['last_usage_date'],
          last_statement_date: row['last_statement_date'],
          last_statement_amount: row['last_statement_amount'],
          last_payment_amount: row['last_payment_amount'],
          last_payment_date: row['last_payment_date'],
          terminated_on_date: row['terminated_on_date'],
          leg_pid: row['leg_pid']
        }
      end
    end

    billings.each do |billing|
      begin
        ::Billing.create!(billing)
      rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => e
        File.open('log/import.log', "ab") do |f|
          f.puts "Could not import billing with ID=#{billing[:id]}"
          f.puts e.message
        end
        next
      end
    end
  end

  private

    def find_client_by_btn(btn)
      ::Btn.find_by_billingtelephonenumber(btn).client
    rescue NoMethodError
      nil
    end

end
