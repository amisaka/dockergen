require 'csv'
require 'bigdecimal'

module Montbeau::Import::DataMigration20171026::Call
  def import_from_csv(file)
    calls = []

    CSV.open(file, 'rb', encoding: 'UTF-8', col_sep: '|') do |csv|
      csv.each do |row|
        calls << {
          btn: row[0],
          did: row[1],
          date: row[2],
          time: row[3],
          call_date: DateTime.parse([row[2], row[3]].join(" ")),
          called_number: row[4],
          area: row[5],
          province: row[6],
          duration: parse_duration(row[7]),
          cost: BigDecimal.new(row[8])
        }
      end
    end

    calls.each do |call|
      ::Call.create(call)
    end
  end

  private

    def parse_duration(duration_str)
      return 0 if duration_str.blank?
      hh, mm, ss = duration_str.split(':').map(&:to_i)
      3600 * hh + 60 * mm + ss
    end

end
