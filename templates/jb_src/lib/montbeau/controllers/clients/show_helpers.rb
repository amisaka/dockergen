module Montbeau::Controllers::Clients::ShowHelpers
  def deactivated_phones?
    return true if params[:deactivated_phones] == '1'
    false
  end

  def deactivated_billable_services?
    return true if params[:deactivated_billable_services] == '1'
    false
  end

  def recurring_billable_services?
    return true if params[:billable_service_recurrence] == '2' or params[:billable_service_recurrence] == nil
    false
  end

  def deactivated_sites?
    return true if params[:deactivated_sites] == '1'
    false
  end

  def set_client_btn
    @btn = @client.btn
  end

  def set_client_sites
    if params[:deactivated_sites] == '1'
      @sites = @client.sites.deactivated.includes(:building).by_site_number #.order(:site_number)
    else
      @sites = @client.sites.activated.includes(:building).by_site_number #.order(:site_number)
    end
    @sites
  end

  def set_singular_site
    @singular_site = (@client.sites.activated.count == 1)
  end

  def set_site
    @site = Site.new.decorate
  end

  def set_phones
    if selected_site
      @phones = @client.services.where(site: selected_site)
    else
      @phones = @client.services
    end

    if deactivated_phones?
      @phones = @phones.deactivated.order(service_id: 'ASC', extension: 'ASC')
        .page(params[:phones_page]).per(10)
        .decorate
    else
      @phones = @phones.activated.order(service_id: 'ASC', extension: 'ASC')
        .page(params[:phones_page]).per(10)
        .decorate
    end
  end

  def set_phone
    @phone = Service.new
  end

  def selectable_buildings
    @selectable_buildings ||= @client.buildings
  end

  def selectable_sites
    @selectable_sites ||= @client.sites.activated
  end

  def set_selected_site
    if has_site_id? and params[:site_id] != 'all'
      selectable_sites.find(params[:site_id])
    else
      # no selected site, means show them all.
      nil
    end
  rescue
    nil
  end

  def selected_site
    @selected_site ||= set_selected_site
  end

  def selectable_services
    @selectable_services ||= @@services ||= Product.limit(25)
  end

  def selectable_otrs_customer_users
    @selectable_otrs_customer_users ||= @@otrs_customer_users ||= []
  end

  def selectable_did_suppliers
    @selectable_did_suppliers ||= @@did_suppliers ||= DIDSupplier.limit(25)
  end

  def selectable_invoices
    @selectable_invoices ||= @client.invoices.decorate
  rescue NoMethodError
    @selectable_invoices = []
  end

  def selected_invoice
    if has_invoice_id?
      @selected_invoice ||= selectable_invoices.find(params[:invoice_id]).try(:decorate)
    else
      @selected_invoice ||= selectable_invoices.last.try(:decorate)
    end
  rescue ActiveRecord::RecordNotFound
    @selected_invoice = nil
  end

  def set_billable_services
    # @billable_services = BillableServiceAccessor.new(@client, @selected_site, params)
    #   .billable_services.page(params[:services_page]).per(30)

    @billable_services = BillableServiceAccessor.new(@client, @selected_site, params).billable_services
    if params[:billable_service_recurrence]
      @billable_services = @billable_services.where(:recurrence_type => params[:billable_service_recurrence])
    else
      @billable_services = @billable_services.where(:recurrence_type => 2)
    end

    if params[:deactivated_billable_services] != "1"
      @billable_services = @billable_services.where('billing_period_ended_at > ? or billing_period_ended_at is NULL', DateTime.tomorrow)
    end

    @billable_services_count2 = @billable_services.length


    # @billable_services = @billable_services.joins(:asset).order(Service.arel_table[:service_id])
    @billable_services = @billable_services.left_outer_joins(:asset).order(Service.arel_table[:service_id])
    @billable_services = @billable_services.page(params[:services_page]).per(30)

    @billable_services = @billable_services.decorate unless @billable_services.empty?

  rescue NoMethodError
    @billable_services = Kaminari.paginate_array([])
      .page(params[:services_page]).per(10)
  end

  def set_billable_service
    @billable_service = BillableService.new
  end

  def set_client_client_line_of_businesses
    @client_line_of_businesses = @client.client_line_of_businesses
  end

  def set_client_phones_count
    if deactivated_phones?
      @phones_count = @client.services.deactivated.count
    else
      @phones_count = @client.services.activated.count
    end
  end

  def set_client_billable_services_count

    if deactivated_billable_services?
      @billable_services_count = @client.billable_services.deactivated.count
    else
      @billable_services_count = @client.billable_services.activated.count
    end
  end

  def set_client_billable_services_total
    if deactivated_billable_services?
      @billable_services_total = @client.billable_services.deactivated.sum(:totalprice)
    else
      @billable_services_total = @client.billable_services.activated.sum(:totalprice)
    end
  end

  def client_show_safe_params
    params.permit(:page, :deactivated_sites, :deactivated_phones, :deactivated_billable_services, :services_page, :billable_service_recurrence)
  end
end
