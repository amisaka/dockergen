module Montbeau::Controllers::Clients::EditHelpers
  def fix_crm_account
    @crm_account = @client.crm_account
    @btn = @client.btn || Btn.new

    if @crm_account.nil? || @btn.nil?
      @client.client_crm_account_code_fix
      @client.save!
      @client.reload

      @crm_account = @client.crm_account
      @btn = @client.btn
    end
  end
end
