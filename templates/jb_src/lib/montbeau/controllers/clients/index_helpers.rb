module Montbeau::Controllers::Clients::IndexHelpers
  def set_clients
    if !client_search_query.blank?
      @clients = ClientDecorator.decorate_collection(client_set.all.page params[:page])
    else
      @clients = ClientDecorator.decorate_collection(Client.activated.by_name.order('crm_accounts.name ASC').page params[:page])
    end
  end
end
