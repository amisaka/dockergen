# This class standardize the activation and deactivation of resources in
# Montbeau. It is meant as a clean and standard interface to be reused on any
# resource that could be activated or deactivated by users. An activated resource
# is assumed to be a record that can be shown to users. A deactivated resource
# is hidden from users. A deactivated resource should never be used in any computation.
#
# This module expects attributes `activated_at:datetime`, `deactivated_at:datetime` and
# `activated:boolean` to be defined on your model.
#
# @author Maxime Gauthier <maxime@credil.org>

require 'active_support/concern'

module Montbeau::Activatable
  extend ActiveSupport::Concern

  included do
    scope :activated,   -> { where activated: true  }
    scope :deactivated, -> { where activated: false }
  end

  class_methods do

  end

  # activates the resource, it will be returned by the `activated` scope
  # @return [Boolean] returns true if success, false if not
  def activate(now = nil)
    return update(options_for_activation(now)) unless activated?
    true
  end

  # activates the resource, it will be returned by the `activated` scope
  # raises validation errors
  # @return [Boolean] returns true if success, false if not
  def activate!(now = nil)
    return update!(options_for_activation(now)) unless activated
    true
  end

  # deactivates the resource, it will be returned by the `deactivated` scope
  # @return [Boolean] returns true if success, false if not
  def deactivate(now = nil)
    return update(options_for_deactivation(now)) unless deactivated?
    true
  end

  # deactivates the resource, it will be returned by the `deactivated` scope
  # raises validation errors
  # @return [Boolean] returns true if success, false if not
  def deactivate!(now = nil)
    return update(options_for_deactivation(now)) unless deactivated?
    true
  end

  # @return [Boolean] returns true if this resource is deactived, false if not
  def deactivated?
    !activated?
  end

  protected

    # `options_for_activation` method is meant to be overriden on models when
    # necessary to customize behavior.
    # @return [Hash] a hash containing the parameters passed to `activate` method
    def options_for_activation(now = nil)
      if now
        { activated: true, activated_at: now }
      else
        { activated: true, activated_at: Time.now }
      end
    end

    # `options_for_deactivation` method is meant to be overriden on models when
    # necessary to customize behavior.
    # @return [Hash] a hash containing the parameters passed to `deactivate` method
    def options_for_deactivation(now = nil)
      if now
        { activated: false, deactivated_at: now }
      else
        { activated: false, deactivated_at: Time.now }
      end
    end

end
