module Montbeau
module Helpers

    ########################################################################
    # Just print a line to indicate progress
    # @param [String] doingWhatStr What do you want to tell the user you are doing
    ########################################################################
    def self.printProgress(doingWhatStr, count, total, zeroBased = false)
        return if total <= 2
        count += 1 if zeroBased == true
        puts "Started #{DateTime.now.strftime('%H:%M:%S')}" if count == 1
        progressStr = "#{count} / #{total} #{(count.to_f * 100/total).round} %"
        print ("\u001b[1000D" + progressStr + ' : ' + doingWhatStr)
        if count == total
            puts "\n"
            puts "Done #{DateTime.now.strftime('%H:%M:%S')}"
            puts "\n"
        else
            print '... '
        end
    end



    def self.getNumWaitingProcs()
        if @numWaitingProcs.nil? or (DateTime.now - @lastCheck)*24*60*60.to_f > 2
            @numWaitingProcs = `top -b  -n 1 | head -n 3 | tail -n 1 | tr -s ' '`.split(" ")[9].to_f
            @lastCheck = DateTime.now
        end

        return @numWaitingProcs
    end

    def self.checkWaitingProcsLoad(waitProcTH = 10)
        numWaitingProcs = Montbeau::Helpers::getNumWaitingProcs


        loopCount = 0
        while getNumWaitingProcs > waitProcTH
            puts "Waiting on waitingProcs (#{numWaitingProcs}) to go back down to #{waitProcTH}" if loopCount == 0
            numWaitingProcs = Montbeau::Helpers::getNumWaitingProcs
            print '.'
            print "(#{numWaitingProcs})" if loopCount % 20 == 0
            sleep 1
            loopCount += 1
        end
    end




    ########################################################################
    # Append a string or symbol to all keys of a hash
    ########################################################################
    def self.suffixToKeys(hash, appendStr)
        hash.map {|k, v|

			keyClass = k.class
			newKey = k.to_s + appendStr.to_s
			newKey = newKey.to_sym if keyClass == Symbol

			[newKey, v]
		}.to_h
    end


    ########################################################################
    #
    ########################################################################
    def self.arrayOfHashOutput(report:, reportFilePath: nil, toScreen: true, title: nil)
        if toScreen == true
            puts report.first.keys.join("\t")
            report.each{|rLine|
                puts rLine.values.join("\t")
            }
        end

        if not reportFilePath.nil?
            CSV.open(reportFilePath, "wb") do |csv|
                csv << [title] if title
                csv << report.first.keys
                report.each {|rLine|
                    csv << rLine.values
                }
            end
        end
    end


    ########################################################################
    # Determine the file path of a timestamped file based on the directory
    # Eventually I would like it to insert the timestmap if given a file path
    ########################################################################
    def self.timeStampFileName(dirOrFile, baseFile = nil, suffix = 'csv')
        dirOrFile = File.expand_path(dirOrFile)
        if Dir.exists?(dirOrFile)
            path = dirOrFile + '/' + baseFile + '-' +  DateTime.now.strftime('%Y%m%d-%H%M%S') + '.' + suffix
        else
            raise "Dir #{dirOrFile} does not exist"
        end

        return path
    end


end
end

