# I don't know why Montbeau::Invoicing methods isn't in Invoice model.
# I'm assuming there's a reason.
# Not considering myself a rails expert, I decided to leave the architecture as such.
# -- Julien 2018/03/02
#
# I still don't understand why this was done this way.
# One could have had a module that the invoice class implements
# But it DOES NOT make sense for this to be it's own class, and take a client as a parameter
# Stupid, stupid, stupid.
# Wish I had time to refactor everything into invoice
# -- Julien 2018/06/13
class Montbeau::Invoicing
  include Montbeau::Invoicing::Tax
  include Montbeau::Invoicing::Renderer


  attr_reader :lastInvoice
  attr_reader :clientOrSite
  attr_reader :chargesDates

  def initialize(clientOrSite, chargesDates: nil)
    case clientOrSite.class.name
    when 'Client', 'Site'
        @clientOrSite = clientOrSite
    when 'ClientDecorator', 'SiteDecorator'
      @clientOrSite = clientOrSite.model
    else
      byebug if Rails.env.development?
      raise "Needs one of Client, Site, ClientDecorator or SiteDecorator.  Got #{clientOrSite.class}"
    end

    if clientOrSite.class == Client
        @lastInvoice = @clientOrSite.last_invoice_only
    end

	@chargesDates = chargesDates
  end

  def self.generate(clientId: nil, daysTh: nil, paymentsSince: nil, paymentsShownUntil: nil, latePaymentDate: nil, siteCollection: nil, lookAheadMonth: Invoice::LOOK_AHEAD_MONTH_DEFAULTS_TO)
    invoices = []

    invoiceableClientsOrSites = Montbeau::Invoicing::onWhat(clientId: clientId, siteCollection: siteCollection)

    latePaymentDate, paymentsShownUntil, paymentsSince, chargesDates = Montbeau::Invoicing::determineDates(paymentsSince: paymentsSince, paymentsShownUntil: paymentsShownUntil, latePaymentDate: latePaymentDate)

    childStr = (siteCollection.class == {}.class ? '*child*' : '')

    i = 0
    total = invoiceableClientsOrSites.count
    errors = {}
    invoiceableClientsOrSites.each_with_index do |clientOrSite,i|

      ### LOOP HEADER BEGIN ###################################################
      skip = false


      message = "Calculating #{childStr} invoice data for #{clientOrSite.class} #{clientOrSite.id}:#{clientOrSite.name}"
      message +=" (client #{clientOrSite.client.to_s})" if clientOrSite.class == Site

      if not daysTh.nil?
        if clientOrSite.invoices.where("created_at > now() - INTERVAL '#{daysTh} DAY'").count > 0
           skip = true
           message = "Skipping: Recent invoice for #{clientOrSite.class} #{clientOrSite.to_s}"

        end
      end

      Montbeau::Helpers::printProgress(message, i, total, true)
      next if skip
      ### LOOP HEADER END ###################################################

      begin
	      invoicing = Montbeau::Invoicing.new(clientOrSite, chargesDates: chargesDates)

          ### BEGIN Dates ######################################################
	      dueDay            =  Invoice::CUTOFF_PAYMENTS_DAY_OF_MONTH #(The 25th)
	      theNextDueDate    =  Date.today.change(day:dueDay)
	      theNextDueDate    += 1.month if theNextDueDate <= Date.today

	      dateTo            = Date.today
	      dateFrom          = dateTo - 1.month
          ### END Dates ######################################################


          ### BEGIN Charges ####################################################
	      perTaxTypeCharges            = invoicing.compute_per_tax_type(:charges)
	      perTaxTypeCalls              = invoicing.compute_per_tax_type(:calls)
          total_taxes                  = perTaxTypeCharges[:tax_total_charges] + perTaxTypeCalls[:tax_total_calls]
          amount                       = invoicing.compute_amount   # compute_amount: reuccring + call cahrges, ie the amount by which the balance will change
          totalCurrentChargesWithTax   = amount + total_taxes
          ### END Charges ######################################################


          ### BEGIN get payments ###############################################
          paymentsTotal = 0
          lastPayment   = nil

          # only a Client has Payments
          if clientOrSite.class == Client
	          lastPayment       = clientOrSite.last_payment

	          if paymentsSince == paymentsShownUntil
	            $stderr.puts "WARNING: paymentsSince == paymentsShownUntil for #{clientOrSite.class} #{clientOrSite.to_s}"
	          end

	          if paymentsSince > paymentsShownUntil
	            raise "paymentsSince (#{paymentsSince}) > paymentsShownUntil #{paymentsShownUntil}."
	          end

	          if paymentsSince.blank? or paymentsShownUntil.blank?
	            raise "paymentsSince (#{paymentsSince}) or paymentsShownUntil #{paymentsShownUntil} is blank."
	          end

	          if total <= 10
	            puts "Invoices for #{clientOrSite.class} #{clientOrSite.to_s} from #{paymentsSince} to #{paymentsShownUntil}"
	          end

              payemnts = []; paymentsTotal = 0
              if clientOrSite.class == Client

                  byebug if (Invoice::paymentsForPeriod(clientOrSite,paymentsSince,paymentsShownUntil).count <= 1 and Rails.env.development?)

payments     = Invoice::paymentsForPeriod(clientOrSite,
    	                  paymentsSince,
    	                  paymentsShownUntil,
    	                  )
                  paymentsTotal     = payments.sum(:amount) or 0
              end
          end
          ### END get payments #################################################

          ### BEGIN Balances ###################################################
          totalDue = pastDue  = previousBalance = 0
          if clientOrSite.class == Client
		      previousBalance = (invoicing.lastInvoice.total_due or invoicing.lastInvoice.amount or 0) if not invoicing.lastInvoice.nil?

	          # Money given by customer has a negative signum, so we add it to previous balance
	          # Should we simply use Math.abs here?  No, because some payments have positive signums
	          # (such as credits from service provider)

	          # Past due is the unpaid portion of the previous balance
		      pastDue  = previousBalance + paymentsTotal

	          # Total due is adding the current charges
		      totalDue = previousBalance + totalCurrentChargesWithTax + paymentsTotal
          end
          ### END Balances #####################################################


	      invoiceHash = {
            amount:                 amount,
            due_date:               theNextDueDate,
            due_immediately:        pastDue,  # The term "due immediately" is not used in kilmist invoice.  It was invented during the 2016/2017 dev effort. Deprecated in 2018
            late_fee_charges:       0,
            late_payment_date:      latePaymentDate,
            monthly_charges:        invoicing.compute_recurring_charges, #Should be an optional date range fed to this method?
            past_due_balance:       pastDue,
            payment_received:       lastPayment,
            payments:               (payments or []),
            payments_shown_since:   paymentsSince,
            payments_shown_until:   paymentsShownUntil,
            previous_balance:       previousBalance,
            total_current_charges:  totalCurrentChargesWithTax,  # Contrary to compute_amount, includes taxes
            total_due_by_amount:    totalDue,
            total_due:              totalDue,
            usage_charges_from:     dateFrom,
            usage_charges:          invoicing.compute_calls_charges, #Should be an optional date range fed to this method?
            usage_charges_to:       dateTo,
	      }

          case clientOrSite
          when Client
            invoiceHash[:client] = clientOrSite
          when Site
            invoiceHash[:site]   = clientOrSite

            # If the function has been fed a site => parentInvoice hash, use it
            invoiceHash[:parent_invoice] = Invoice.find(siteCollection[clientOrSite.id]) if siteCollection.class == {}.class
          end

		  invoiceHash.merge!({
            look_ahead_charges_begin:   chargesDates[:look_ahead_begin],
            look_ahead_charges_end:     chargesDates[:look_ahead_end],
            usage_charges_begin:        chargesDates[:usage_begin],
            usage_charges_end:          chargesDates[:usage_end],
          })

	      invoiceHash.merge!(perTaxTypeCharges)
	      invoiceHash.merge!(perTaxTypeCalls)

	      invoices << invoiceHash
      rescue => e
        byebug if (errors.count <= 1 and Rails.env.development?)
        errors[clientOrSite] = e.message
      end
    end
    puts

    print "Creating #{invoices.count} #{childStr} invoices.  This may take a while... "
    invoices = ::Invoice.create!(invoices)
    puts "Invoice ID #{invoices.map {|i| i.id}.join(' ')} created" if invoices.count <= 10

    puts "#{childStr} invoices creation failures for:" if errors.count > 0
    errors.each {|clientOrSite, errMsg|
        $stderr.puts  "%-70s %s" % [clientOrSite.to_s, errMsg]
    }




    # If we ran invoices for Clients, now generate the child invoices
    if invoiceableClientsOrSites.first.class == Client

        # Join the invoices produced with the sites of the clients
        # That have department statement types
        # And make a site => parent invoice hash
        siteToParentInvoiceHash = Client.joins(:sites).joins(:statement_types).joins(:invoices).
            where('statement_types.name ilike ?', '%department%').where('invoices.id in (?)',  invoices.map{|i| i.id}).
            pluck('sites.id', 'invoices.id').map{|row|  # Make hash
                [row[0], row[1]]
            }.to_h

        Montbeau::Invoicing::generate(daysTh: daysTh, paymentsSince: paymentsSince, paymentsShownUntil: paymentsShownUntil, latePaymentDate: latePaymentDate, siteCollection: siteToParentInvoiceHash)
    end


    # Map it to a hash
    # Feed it to generate

    puts "Done gnereating #{childStr} invoices."

    #Disabling this for now
    #puts "Generating HTML files.... "
    #invoices.each_with_index {|invoice, i|
    #    render
    #}
    #puts "Done."

  end


  ### Subfuction of generate ###########################################
  # Determines what we are going to generate invocies on
  ########################################################################
  def self.onWhat(clientId: nil, siteCollection: nil)
    # default behavior.  Invoice for all clients invoiceable
    if clientId.nil? and siteCollection.nil?
        invoiceableClientsOrSites = ::Client.activated.invoiceable

    elsif (clientId.nil? and not siteCollection.nil?)
    # We are doing sties

        if siteCollection.class == [].class
        # siteCollection is an array of sites
            invoiceableClientsOrSites = siteCollection

        elsif siteCollection.class == {}.class
        # siteCollection is a hash of siteId => parent invoice collection
            invoiceableClientsOrSites = siteCollection.keys
        end

        # If siteCollection is a bunch of integers (Fixnum), find their corresponding objects
        invoiceableClientsOrSites = Site.find(invoiceableClientsOrSites) if invoiceableClientsOrSites.first.class == Fixnum

    elsif (not clientId.nil? and siteCollection.nil?)
    # One specific client

        client = ::Client.find(clientId)
        raise "Client #{client.to_s} not invoiceable" if not ::Client.activated.invoiceable.include?(client)
        invoiceableClientsOrSites = [client]
    else
    # Unspecified behavior

        byebug if Rails.env.development?
        raise "Both site collection and clientId cannot be set"
    end

    return invoiceableClientsOrSites
  end


  ### Determine dates for self.generate ################################
  #
  ########################################################################
  def self.determineDates(paymentsSince: nil, paymentsShownUntil: nil, latePaymentDate: nil, lookAheadMonth: Invoice::LOOK_AHEAD_MONTH_DEFAULTS_TO)

    if paymentsSince.nil? ^ paymentsShownUntil.nil?
        raise "paymentsSince and paymentsShownUntil must both be set or both be null"
    end

    allDefaults = [paymentsSince, paymentsShownUntil, latePaymentDate].all?{|var| var.nil?}

    ### Determined dates ############################################################################
    # There's got to be a smarter way to repeat the same instruction for different variable that includes setting it
    latePaymentDate     = Date.parse(latePaymentDate)    if (latePaymentDate.class    == String and not latePaymentDate.empty?)
    paymentsShownUntil  = Date.parse(paymentsShownUntil) if (paymentsShownUntil.class == String and not paymentsShownUntil.empty?)
    paymentsSince       = Date.parse(paymentsSince)      if (paymentsSince.class      == String and not paymentsSince.empty?)

    latePaymentDate     = Invoice::latePaymentDate()    if latePaymentDate.nil?
    paymentsShownUntil  = Invoice::paymentsShownUntil() if paymentsShownUntil.nil?
    paymentsSince       = Invoice::paymentsShownFrom()  if paymentsSince.nil?

    if not [latePaymentDate, paymentsShownUntil, paymentsSince].all? {|var| [Date, DateTime].include?(var.class)}
        byebug if Rails.env.development?
        raise "One of the date variables is not a Date"
    end

    ### Check payement selection date ###############################################################
    if (paymentsShownUntil > Date.today)
        if allDefaults
            msg = "paymentsShownUntil > today."
            puts $stderr.puts ("NOTICE: #{msg}.  Adjusting.")
            paymentsShownUntil = paymentsShownUntil.last_month
            #paymentsSince = paymentsSince.last_month if (paymentsShownUntil - paymentsSince < Invoice::MIN_PAYMENTS_RANGE_DEFAULT)
            #byebug if Rails.env.development?
            nil
        else
            $stderr.puts ("WARNING: " + msg)
        end
    end

    if (paymentsShownUntil <= paymentsSince)
        msg = "paymentsShownUntil <= paymentsSince"
        if allDefaults
            byebug if Rails.env.development?
            raise msg
        else $stderr.puts ("WARNING: " + msg)
        end
    end

    ### Now determine dates for usages ###########################################################
    look_ahead_month = case lookAheadMonth
        when :created_at
            Date.today
        when :next
            Date.today.next_month
        else
            raise "Invalid value for lookAheadMonth (#{lookAheadMonth})"
        end
    usage_month = look_ahead_month.last_month

	chargesDates = {
    	look_ahead_begin:	look_ahead_month.beginning_of_month,
    	look_ahead_end: 	look_ahead_month.end_of_month,
    	usage_begin:     	usage_month.beginning_of_month,
    	usage_end:  		usage_month.end_of_month,
	}

    return latePaymentDate, paymentsShownUntil, paymentsSince, chargesDates

  end


  def compute_per_tax_type(chargeType)
    computeWhat = case chargeType
        when :charges
        compute_recurring_charges_per_province()
        when :calls
        compute_call_costs_per_province()
        else
        raise "Undefined chargeType in compute_per_tax_type in #{__FILE__}"
    end

    h = compute_taxes_canada(computeWhat)

    # Default the key values to BigDecimal 0
    taxTypesForInvoiceModel = InvoiceDecorator::taxTypeList()
    returnHash = Hash[taxTypesForInvoiceModel.product([0])]

    returnHash[:bc_pst]        = h[:CA][:BC][:PST] if not h[:CA][:BC].nil?
    returnHash[:alberta_pst]   = h[:CA][:AB][:PST] if not h[:CA][:AB].nil?
    returnHash[:sask_pst]      = h[:CA][:SK][:PST] if not h[:CA][:SK].nil?
    returnHash[:qst]           = h[:CA][:QC][:QST] if not h[:CA][:QC].nil?
    returnHash[:manitoba_rst]  = h[:CA][:MB][:RST] if not h[:CA][:MB].nil? ### CAUTION: RST, not PST in Manitoba


    # If the GST/HST is specified, keep it for array and caculate sum
    [:HST, :GST].each {|taxType|
        #byebug if taxType == :HST
        returnHash[taxType.downcase] = h[:CA].values.map {|data|
            data[taxType] or BigDecimal.new(0)
        }.sum
    }

    returnHash[:tax_total] = returnHash.values.sum

    returnHash = Montbeau::Helpers::suffixToKeys(returnHash, '_' + chargeType.to_s)

    return returnHash

  end

  ##############################################################################
  # Recurring + calls charges, which should give you the invoice amount
  # ie, by how much your balance is affected
  ##############################################################################
  def compute_amount
    [ compute_recurring_charges, compute_calls_charges ].sum
  end

  ##############################################################################
  # Sum of all charges (billable_services) regardless of province
  ##############################################################################
  def compute_recurring_charges
    compute_recurring_charges_per_province.values.sum
  end

  ##############################################################################
  # Sum of all charges (billable_services) per province
  ##############################################################################
  def compute_recurring_charges_per_province(createdAtDate = nil)
    if @chargesPerProvince.nil?
        activeQuery = billableServicesForClientAsPerMonth().joins(site: :building).select('billable_services.qty', 'billable_services.qty', 'billable_services.unitprice', 'billable_services.discount', 'buildings.province')

        # Compute charges based on province
        @chargesPerProvince = {}
        activeQuery.each {|bs|
            @chargesPerProvince[bs.province] = BigDecimal.new(0) if @chargesPerProvince[bs.province].nil?
            @chargesPerProvince[bs.province] += bs.total
        }

    end

    return @chargesPerProvince
  end

  ##############################################################################
  # Returns an active query to get the billable services based on the specified
  # client and eventually date.
  #
  # Kind of disgusting that we display on calculation
  # but upper mgmnt KISS & impossible deadlines are keeping things this way
  # Eventually I would like a "invoiced_items" table
  ##############################################################################
  def billableServicesForClientAsPerMonth(createdAtDate: nil)
    # Set date if not specified
    createdAtDate = Date.today if createdAtDate.nil?

    # Set the active query to the activated services of the client
    activeQuery = @clientOrSite.billable_services

    # When, the lookahead month end is in january, no criteria
    # Otherwise, exclude yearly
    if @chargesDates[:look_ahead_end].month > 1
        activeQuery = activeQuery.where('billing_period != ?', BillableService.billing_periods[:yearly])
    end

    # First line if for one-time, second line for recurring charges
    # For second line: is the charge beginning before lookahead month or ending during the lookahead month
    whereClauseSQL = %Q[(recurrence_type = ? AND billing_period_started_at BETWEEN ? AND ?) OR
        (recurrence_type = ? AND (billing_period_started_at <= ? and (billing_period_ended_at is null or billing_period_ended_at >= ?)) OR (billing_period_ended_at     BETWEEN ? AND ?)) ]

    # Set the args for the query above.
    # Must be in the order the params appear, as the query will be fed only the values of this Hash as an array
    args = {
        # One time
        recurrence_type_one_time:   BillableService.recurrence_types["One-time"],
        usage_month_start:          @chargesDates[:usage_begin],
        usage_month_end:            @chargesDates[:usage_end],

        # Recurring
        recurrence_type_recurring:  BillableService.recurrence_types["Recurring"],
        billing_start_lookahead_month_end:        @chargesDates[:look_ahead_end],
        billing_end_lookahead_month_start_1:        @chargesDates[:look_ahead_begin],
        billing_end_lookahead_month_start_2:      @chargesDates[:look_ahead_begin],
        billing_end_lookahead_month_end:        @chargesDates[:look_ahead_end],
    }


    activeQuery = activeQuery.where(whereClauseSQL, *(args.values))
    byebug

    #TODO:
    # Add code from BillableService::invoiceable_for

    return activeQuery
  end


  def compute_call_costs_per_province
    if @callCostsByProvince.nil?
	    @callCostsByProvince = {}
	    Call.byService(::Call.for(@clientOrSite)).each {|serviceId, calls|
            service = Service.find(serviceId)

            if service.site.nil?
                raise "Service #{service.to_s} for client #{service.client.to_s} does not have a site"
            end

	        provinceOfService = service.site.building.province

	        @callCostsByProvince[provinceOfService] = 0 if @callCostsByProvince[provinceOfService].nil?

	        @callCostsByProvince[provinceOfService] += calls.map{|c| c.cost}.sum
	    }
    end

    return @callCostsByProvince

  end


  def compute_calls_charges
    ::Call.for(@clientOrSite).sum(:cost)
  end

  def compute_calls_count
    ::Call.for(@clientOrSite).count
  end

  def compute_calls_duration
    ::Call.for(@clientOrSite).sum(:duration)
  end

  def compute_calls_cost
    ::Call.for(@clientOrSite).sum(:cost)
  end



  ##############################################################################
  # For each non-deleted client, delete invoices where there is more than once
  # invoice per month per client, except for the latest invoice of that month
  #
  # Probably a fancy SQL way of doing this, but I think I will use
  # go through rails to respect undeletable module
  ##############################################################################
  def self.cleanUp(testRun = true, clientId = nil)
        beginTS = DateTime.now
	    format = "%-80s %4s %20s\n"
        reportStr = format % ['Client', 'Count', 'Status']

        clients = (clientId.nil? ? ::Client.all : [::Client.find(clientId)] )

        clientCount = clients.count
        clients.each_with_index {|c, i|
            Montbeau::Helpers::printProgress("Examing #{c.name or c.id} for excess invoice", i, clientCount, true)
            toDelete = self::olderInvoicesPerMonth(c)
            status = 'NOTHING'
            if toDelete.count > 0
	            if self::testCleanup(c, toDelete.count)
	                if testRun == false
	                    toDelete.each {|i| i.destroy}
	                    status = 'DELETED = TRUE'
	                else
	                    status = 'WOULD DELETE'
	                end
	            else
	                status = 'ERROR, NOT DELETED'
	            end
            else
                next
            end
            reportLine = format % [c.to_s, toDelete.count, status]
            reportStr += reportLine
        }

        puts reportStr
        endTS = DateTime.now
        puts "#{((endTS - beginTS) * 60*24).to_f.round(2)} minutes for cleanup"

  end

  ##############################################################################
  # Test if olderInvoicesPerMonth returns the same number of excess invoices
  # Returns true or false so cleanup can determine if to proceed or not
  ##############################################################################
  def self.testCleanup(clientObj, countToDelete)
      # Add up count of invoices per month, subtracting one invoice per month
      countExcessInvoices = clientObj.invoices.all.group_by {|i| i.invoice_date}.map{|k,v| [k, v.count-1]}.sum{|k,v| v}

      if countExcessInvoices == countToDelete
        return true
      else
        return false
      end

  end

  def self.olderInvoicesPerMonth(clientObj = nil)
    toDelete = []

    if clientObj == nil
        Client.class.all.foreach {|c|
            toDelete << self::olderInvoicesPerMonth(clientId)
        }
    else
        clientObj.invoices.all.group_by {|i| i.invoice_date}.each {|invoiceDate, invoices|
            iMax = invoices.max_by {|i| i.created_at};
            invoices.delete_at(invoices.index(iMax))
            toDelete << invoices if invoices.count > 0
            toDelete.flatten!
        }
    end

    return toDelete

  end

end
