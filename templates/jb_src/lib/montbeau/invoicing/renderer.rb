module Montbeau::Invoicing::Renderer

  def render(invoice, warden=nil)

    if invoice.class != 'InvoiceDecorator'
        invoice = InvoiceDecorator.new(invoice)
    end

    Warden.test_mode!


    env = ::InvoicesController.renderer.instance_variable_get(:@env)
    env["warden"] = warden if not warden.nil?
    ::InvoicesController.renderer.instance_variable_set(:@env, env)

    ::InvoicesController.render :show, assigns: {
      calls: Call.for(invoice.client).decorate,
      billable_services: invoice.client.billable_services.activated,
      invoice: invoice,
      # taxlist: invoice.taxlist
    }
  end

end
