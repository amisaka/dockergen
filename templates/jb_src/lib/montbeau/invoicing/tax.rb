module Montbeau::Invoicing::Tax


  ##########################################################################################
  # The returned hash is a bit convoluted, but in the end I think it's a good re-use of
  # compute_taxes_for_jurisdiction
  # Top hash element :allTaxTotal has your total of all taxes
  ##########################################################################################
  def compute_taxes_canada(amountPerProvince)
    # Check if we implement country and province

    country = 'CA'; h = {CA: {}}
    amountPerProvince.each {|province, amount|
        if province.present? and province != "XN"
            h[country.to_sym][province.to_sym] = compute_taxes_for_jurisdiction(amount, province, country)
        else
            $stderr.puts "WARNING: nil, empty or XN province detected in compute_taxes_canada.  Ignoring."
        end
    }

    #h[:tax_total] = h.map {|country, perProvinceData| perProvinceData.map{|province, data| data[:total]}}.sum

    return h

  end

  def compute_taxes_for_jurisdiction(amount, province = 'QC', country = 'CA')

    # Check if we implement country and province
    raise "tax table for country: #{country} is not implemented" \
      if !Montbeau::Invoicing::TaxTable::TAXES.keys.include?(country.to_sym)

    raise "tax table for province: #{province} is not implemented" \
      if !Montbeau::Invoicing::TaxTable::TAXES[country.to_sym].keys.include?(province.to_sym)

    h = {amount: amount}
    # For the specified country and province, lookup the table in tax_table.rb and compute taxes
    Montbeau::Invoicing::TaxTable::TAXES[country.to_sym][province.to_sym].each do |k,v|
      h[k] = amount * v
    end
    h.merge(total: h.values.sum, province: province, country: country)
  end

end
