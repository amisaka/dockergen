module Montbeau::Invoicing::TaxTable
  GST = 0.05000
  HST = 0.15000

  TAXES = {
    CA: {
      AB: { GST: GST, PST: 0.0 },
      BC: { GST: GST, PST: 0.07000 },
      MB: { GST: GST, RST: 0.08000 },
      NB: { HST: HST },
      NL: { HST: HST },
      NT: { GST: GST },
      NS: { HST: HST },
      NU: { GST: GST },
      ON: { HST: 0.13000 },
      PE: { HST: HST },
      QC: { GST: GST, QST: 0.09975 },
      SK: { GST: GST, PST: 0.06000 },
      YT: { GST: GST },
    }
  }

    def self.allKeysOfHash(h)
      s = Set.new

      h.map { |hk, hv|
          if hv.class == Hash
              s += self.allKeysOfHash(hv)
          else
              s.add(hk)
          end
      }

      return s
    end
end
