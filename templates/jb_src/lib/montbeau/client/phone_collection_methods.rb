module Montbeau::Client::PhoneCollectionMethods
  def add_phone(phone)
    btn.phones << phone
  end

  def remove_phone(phone)
    btn.phones.delete(phone)
  end
end
