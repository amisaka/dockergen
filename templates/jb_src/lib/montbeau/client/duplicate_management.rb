# For ticket https://code.credil.org/issues/13032

require 'csv'

class Montbeau::Client::DuplicateManagement

  def getClientsWithNoBTNs()
    ::Client.where('id not in (?)', Btn.unscoped.select(:client_id))
  end

  def checkCounts()
    {
      "clients" => ::Client.count,
      "payments" => ::Payment.count,
      "invoices" => ::Invoice.count,
      "btns" => ::Btn.count,
      "sites" => ::Site.count,
      "billings" => ::Billing.count
    }
  end

  def deleteNoBTNs
    countsBefore = checkCounts;
    getClientsWithNoBTNs().destroy_all;
    countsAfter = checkCounts;

    puts "Sanity check"
    puts "Counts before"
    puts countsBefore
    puts "Counts after"
    puts countsAfter
  end

  def exportToCsv(fileOutputPath)

    noBTNs = getClientsWithNoBTNs()

    csv = CSV.open(fileOutputPath, "wb")
    csv << ["id", "BTN", "name", "address", "city" "status", "invoice cycle", "site count", "action"]

    noBTNs.each { |c|
      if ! c.name.nil? and c.address.nil?
        next
      end

      duplicatesOf = ::Client.where(:name =>c.name).to_a


      csv << printClientRow(c, "delete")

      duplicatesOf.each { |d|
        next if d.billingtelephonenumber.nil? or d.address != c.address
        csv << printClientRow(d, "keep")
      }
 
    }

    csv.close
  end

  def printClientRow(c, action)
    invoiceCycle = ""
    c.btn.invoice_template.invoice_cycle if ! c.btn.nil?
    [c.id, c.billingtelephonenumber, c.name, c.address, c.city, c.status, invoiceCycle, c.sites.count, action]
  end
end
