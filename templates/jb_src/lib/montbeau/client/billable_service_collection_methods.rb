module Montbeau::Client::BillableServiceCollectionMethods
  # Ensures billable service is added to this client's BTN-billed invoice
  def add_billable_service(invoice, billable_service)
    btn.add_billable_service(invoice, billable_service)
  end

  # Ensures billable service is removed from this client's BTN-billed invoice
  def remove_billable_service(invoice, billable_service)
    btn.remove_billable_service(invoice, billable_service)
  end
end
