module Montbeau::Client::SiteCollectionMethods
  def add_site(site)
    sites << site
  end

  def remove_site(site)
    sites.delete(site)
  end
end
