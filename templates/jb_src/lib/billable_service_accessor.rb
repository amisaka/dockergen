class BillableServiceAccessor

  def initialize(client, site = nil, params = {})
    if client.kind_of? ClientDecorator
      @client = client.object
    elsif client.kind_of? Client
      @client = client
    else
      raise "Accessor needs one of Client or ClientDecorator"
    end

    if site
      if site.kind_of? SiteDecorator
        @site = site.object
      elsif site.kind_of? Site
        @site = site
      end
    end

    @params = params
  end

  def billable_services
    if @site
      @billable_services = @client.billable_services.where(site: @site)
    else
      @billable_services = @client.billable_services
    end

    if deactivated_billable_services?
      @billable_services = @billable_services.deactivated
    else
      @billable_services = @billable_services.activated
    end

    @billable_services = @billable_services.order(billing_period_started_at: :desc)

  rescue NoMethodError
    fallback_to_empty_array
  end

  private

    def deactivated_billable_services?
      return true if @params[:deactivated_billable_services] == '1'
      false
    end

    def fallback_to_empty_array
      Kaminari.paginate_array(BillableService.none)
    end

end
