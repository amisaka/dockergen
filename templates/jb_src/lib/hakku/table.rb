module Hakku
  class Table

    def self.create(model, **cols)
      model.connection.execute "CREATE TABLE IF NOT EXISTS %s (%s);" % [model.table_name, cols.map{|k,v| [k,v].join(' ')}.join(', ')]
    end

    def self.import(model, path, encoding = 'CP863:UTF-8')
      CSV.foreach path, headers: :first_row, encoding: encoding do |row|
        begin
          model.create! row.to_hash.delete_if{|k,v| k.blank?} # it's likely preferable to use rails "create" to insert record, but it doesn't work with corrupted data
        rescue CSV::MalformedCSVError => e # Encoding::UndefinedConversionError, RangeError => e
          Rails.logger.error e.message
        end
      end
    end

    # import_row(name, row.to_hash.delete_if{|k,v| k.blank?})
    # begin

    # def self.import_row(name, cols)
    #   connection.execute "INSERT INTO %s (%s) VALUES (%s);" % [name, cols.map{|k,v| k.to_s}.join(', '), cols.map{|k,v| "\"#{v}\""}.join(', ')]
    # rescue ActiveRecord::StatementInvalid, SQLite3::SQLException => e
    #   Rails.logger.error e.message
    # end

    class ServiceType < Table
      def self.create
        super( Hakku::ServiceType,
          ServiceTypePID: 'integer',
          Code: 'text',
          Description: 'text',
          Active: 'integer',
          OwnerAppUserPID: 'integer',
          OwnerTeamPID: 'integer',
          InsertAppUserPID: 'integer',
          UpdateAppUserPID: 'integer',
          CreatedOn: 'datetime',
          UpdatedOn: 'datetime')
      end

      def self.import
        super( Hakku::ServiceType, File.join($HakkuBackup, "dbo.ServiceType.csv"))
      end
    end

    class Service < Table
      def self.create
        super( Hakku::Service,
          ServicePID: 'integer',
          BillToServicePID: 'integer',
          ID: 'text',
          ServiceTypePID: 'int',
          DisplayText: 'text',
          Description: 'text',
          ParentServicePID: 'integer',
          ServiceAssignmentTypePID: 'integer',
          DepartmentLineOfBusinessPID: 'integer',
          CustomerLineOfBusinessPID: 'integer',
          CustomerPID: 'integer',
          DepartmentPID: 'integer',
          LineOfBusinessPID: 'integer',
          GLAccountGroupPID: 'integer',
          CustomerCategoryPID: 'integer',
          CurrentAccountStatusTypePID: 'integer',
          CreatedOn: 'datetime',
          StartDate: 'datetime',
          TerminationDate: 'datetime',
          LanguagePreference: 'varchar(20)',
          ValidateProjectCode: 'integer',
          ProvisionLater: 'integer',
          Comments: 'integer',
          Privacy: 'integer',
          ResultantRound2Decimal: 'integer',
          ResultantRoundingMethod: 'integer',
          InsertAppUserPID: 'integer',
          UpdateAppUserPID: 'integer',
          TopicPID: 'integer',
          UsageRecordRateThresholdAmount: 'integer',
          AutoCapUsageRecordRate: 'integer',
          MonthlyThresholdCasualService: 'integer',
          UsageRecordQuantityThreshold: 'integer',
          AutoCapUsageRecordQuantity: 'integer',
          CurrencyConversionFactorPID: 'integer',
          PrivateLine: 'integer',
          OwnerAppUserPID: 'integer',
          OwnerTeamPID: 'integer',
          DefaultService: 'integer',
          UpdatedOn: 'datetime',
          CloseFinalised: 'integer',
          Billed: 'integer',
          PPServiceLinePID: 'integer')
      end

      def self.import(file)
        path  = file || File.join($HakkuBackup, "dbo.Service.csv")
        Hakku::Service.all.delete_all

        puts "Importing from #{path}"
        cnt = Hakku::Service.import_file(path)
        puts "... resulting in #{cnt} records"
      end
    end

    class Department < Table
      def self.create
        super( Hakku::Department,
               DepartmentPID: 'integer',
               SeparateInvoice: 'integer',
               Code: 'text',
               DepartmentName: 'text',
               ParentDepartmentPID: 'integer',
               CustomerPID: 'integer',
               GLAccountGroupPID: 'integer',
               CustomerCategoryPID: 'integer',
               CurrentAccountStatusTypePID: 'integer',
               CreatedOn: 'datetime',
               StartDate: 'datetime',
               TerminationDate: 'datetime',
               LanguagePreference: 'text',
               DiscountChargesinInvoiceOnly: 'text',
               Comments: 'text',
               Privacy: 'integer',
               TopicPID: 'integer',
               Round2Decimal: 'text',
               ResultantRound2Decimal: 'text',
               RoundingMethod: 'text',
               ResultantRoundingMethod: 'text',
               UsageRecordRateThresholdAmount: 'text',
               AutoCapUsageRecordRate: 'text',
               MonthlyThresholdCasualService: 'text',
               UsageRecordQuantityThreshold: 'text',
               AutoCapUsageRecordQuantity: 'text',
               CurrencyConversionFactorPID: 'text',
               OwnerAppUserPID: 'integer',
               OwnerTeamPID: 'integer',
               InsertAppUserPID: 'integer',
               UpdateAppUserPID: 'integer',
               UpdatedOn: 'datetime',
               CloseFinalised: 'integer',
               PPDepartmentPID: 'integer')
      end

      def self.import(file)
        path  = file || File.join($HakkuBackup, "dbo.Department.csv")
        Hakku::Department.all.delete_all

        puts "Importing from #{path}"
        cnt = Hakku::Department.import_file(path)
        puts "... resulting in #{cnt} records"
      end
    end

    class Product < Table
      def self.create
        super( Hakku::Product,
               ProductPID: 'integer',
               Code: 'text',
               Description: 'text',
               Active: 'text',
               MultiJurisdictionalTaxing: 'text',
               Amount: 'text',
               PurchaseAmount: 'text',
               ProductClassPID: 'integer',
               ProductCategoryPID: 'integer',
               ProductTypePID: 'integer',
               LineOfBusinessPID: 'integer',
               TaxPassthrough: 'text',
               GLAccountPID: 'integer',
               AllowFractionalQuantity: 'text',
               OwnerAppUserPID: 'integer',
               OwnerTeamPID: 'integer',
               InsertAppUserPID: 'integer',
               UpdateAppUserPID: 'integer',
               CreatedOn: 'datetime',
               UpdatedOn: 'datetime',
               UnitCost: 'text',
               PPProductPID: 'integer',
               BCProductPID: 'integer',
               Name: 'text')
      end

      def self.import(file)
        path  = file || File.join($HakkuBackup, "dbo.Product.csv")
        Hakku::Product.all.delete_all

        puts "Importing from #{path}"
        cnt = Hakku::Product.import_file(path)
        puts "... resulting in #{cnt} records"
      end
    end

    class Customer < Table
      def self.create
        super( Hakku::Customer,
          CustomerPID: 'integer',
          Code: 'text',
          CustomerName: 'text',
          NameTitle: 'text',
          FirstName: 'text',
          MiddleName: 'text',
          LastName: 'text',
          NameSuffix: 'text',
          County: 'text',
          BillingCyclePID: 'integer',
          CreatedOn: 'datetime',
          StartDate: 'datetime',
          TerminationDate: 'datetime',
          CurrentAccountStatusTypePID: 'integer',
          GLAccountGroupPID: 'integer',
          CustomerCategoryPID: 'integer',
          LanguagePreference: 'text',
          BillingInfoPID: 'integer',
          DiscountChargesinInvoiceOnly: 'integer',
          Privacy: 'integer',
          TopicPID: 'integer',
          ThirdPartyBilling: 'integer',
          TriggerDirectBilling: 'integer',
          UsageRecordRateThresholdAmount: 'integer',
          AutoCapUsageRecordRate: 'integer',
          Round2Decimal: 'integer',
          ResultantRound2Decimal: 'integer',
          RoundingMethod: 'integer',
          ResultantRoundingMethod: 'integer',
          MonthlyThresholdCasualService: 'integer',
          UsageRecordQuantityThreshold: 'integer',
          AutoCapUsageRecordQuantity: 'integer',
          CurrencyConversionFactorPID: 'integer',
          ParentCustomerPID: 'integer',
          MigrationDate: 'datetime',
          OwnerAppUserPID: 'integer',
          OwnerTeamPID: 'integer',
          InsertAppUserPID: 'integer',
          UpdateAppUserPID: 'integer',
          UpdatedOn: 'datetime',
          ActiveServiceCount: 'integer',
          CloseFinalised: 'integer',
          AccountPID: 'integer',
          PPCustomerPID: 'integer')
      end

      def self.import
        file = File.join($HakkuBackup, "dbo.Customer.csv")
        Hakku::Customer.all.delete_all

        puts "Importing from #{file}"
        cnt = Hakku::Customer.import_file(file)
        puts "... resulting in #{cnt} records"
      end
    end

    class LineOfBusiness < Table
      def self.create
        super( Hakku::LineOfBusiness,
          LineOfBusinessPID: 'integer',
          Code: 'text',
          Description: 'text',
          Active: 'integer',
          OwnerAppUserPID: 'integer',
          OwnerTeamPID: 'integer',
          InsertAppUserPID: 'integer',
          UpdateAppUserPID: 'integer',
          CreatedOn: 'datetime',
          UpdatedOn: 'datetime',
          PPLineOfBusinessPID: 'integer',
          BCLineOfBusinessPID: 'integer')
      end

      def self.import
        super(Hakku::LineOfBusiness, File.join($HakkuBackup, "dbo.LineOfBusiness.csv"))
      end
    end

    class ChargeAssignment < Table
      def self.create
        super( Hakku::ChargeAssignment,
          ChargeAssignmentPID: 'integer',
          ProductPID: 'integer',
          AssignmentLevelPID: 'integer',
          Active: 'boolean',
          AssignedOn: 'datetime',
          StartDate: 'datetime',
          Quantity: 'integer',
          UnitPrice: 'decimal',
          ChargeModePID: 'integer',
          ReferenceCode: 'string',
          Memo: 'text',
          RecurrenceTypePID: 'integer',
          RecurrenceInfoPID: 'integer',
          LastAppliedDate: 'datetime',
          EquipmentAssignmentTypePID: 'integer',
          MaxCummulativeAmount: 'string',
          CummulativeAmount: 'string',
          PurchaseOrderPID: 'integer',
          ContractPID: 'integer',
          CustomerPID: 'integer',
          DepartmentPID: 'integer',
          ServicePID: 'integer',
          BundleAssignmentPID: 'integer',
          Description: 'string',
          OverrideGLAccount: 'boolean',
          GLAccountPID: 'integer',
          OwnerAppUserPID: 'integer',
          OwnerTeamPID: 'integer',
          InsertAppUserPID: 'integer',
          UpdateAppUserPID: 'integer',
          CreatedOn: 'datetime',
          UpdatedOn: 'datetime',
          MinimumBillingRequirementPID: 'integer',
          InvoicePID: 'integer',
          TotalOccurrences: 'integer',
          LastChargeStartDate: 'datetime',
          LastChargeEndDate: 'datetime',
          LastInvoicePID: 'integer',
          ARDBName: 'string',
          TaxLocalePID: 'integer',
          UnitCost: 'decimal',
          TotalPrice: 'decimal',
          PPProductAssignmentPID: 'integer',
          ParentChargeAssignmentPID: 'integer',
          RevChargeAssignmentPID: 'integer',
          TotalPriceMode: 'boolean')
      end

      def self.import
        file = File.join($HakkuBackup, "dbo.ChargeAssignment.csv")
        Hakku::ChargeAssignment.all.delete_all

        puts "Importing from #{file}"
        cnt = Hakku::ChargeAssignment.import_file(file)
        puts "... resulting in #{cnt} records"
      end
    end
  end
end
