class Report

  def self.telus
    query('telus')
  end

  def self.drapper
    query('drapper')
  end

  def self.rogers
    query('rogers')
  end

  def self.bell
    query('bell')
  end

  def self.troop
    query('troop')
  end

  def self.obm
    query('obm')
  end

  def self.find_cdrs_with_default_rate_center_or_none
    Cdr.international.where(called_rate_center: [RateCenter.default_rate_center, nil])
  end

  def self.find_cdrs_without_country_code
    Cdr.international.joins(:called_rate_center).where("rate_centers.country_code IS NULL").order('rate_centers.country_code, cdrviewer_cdr.callednumber')
  end

  def self.find_area_codes_without_country
    AreaCode.where(country: [nil, ''])
  end

  private

    def self.query(supplier)
      Cdr.select(selected_columns)
         .from('did_suppliers')
         .joins('INNER JOIN cdrviewer_datasource ON did_suppliers.id = cdrviewer_datasource.did_supplier_id')
         .joins('INNER JOIN cdrviewer_cdr ON cdrviewer_datasource.id = cdrviewer_cdr.datasource_id')
         .where('did_suppliers.name ILIKE ?', "%#{supplier}%")
         .count('cdrviewer_cdr.id')
    end

    def self.selected_columns
      'cdrviewer_cdr.id, cdrviewer_cdr.datasource_id, cdrviewer_datasource.id, cdrviewer_datasource.did_supplier_id, did_suppliers.id'
    end

  def self.no_dur_and_yes_call_answer
    Cdr.select('id, duration, answerindicator')
	.from('cdrviewer_cdr')
	.where('answerindicator ILIKE ? and duration > ? and duration < ?', 'yes', 0, 1)
	.count('id')
  end

  # Not sure the right path to determine that a rate was not assigned to a call.
  def self.confirm_all_calls_got_rated
    RateCenter.select('cdrviewer_cdr.id, cdrviewer_cdr.called_rate_center_id, rate_centers.id')
	.from('rate_centers')
        .joins('INNER JOIN cdrviewer_cdr ON rate_centers.id = cdrviewer_cdr.called_rate_center_id')
	.count('cdrviewer_cdr.id')
  end

  # This is 'list of DIDs where Beaumont could not find a BTN match', not sure if this is the right query.
  def self.all_cdrs_have_a_btn_assigned
    Phone.select('id, did_supplier_id, btn_id')
	.from('phones')
	.where('did_supplier_id IS ?', nil)
	.count('id')
  end
end
