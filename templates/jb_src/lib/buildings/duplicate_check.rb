module Buildings
  class DuplicateCheck

    def self.find_all_duplicates
      h = {}

      Building.all.each do |b|
        check = new(b.id)
        dups = check.find_duplicates
        h[b] = dups unless dups.blank?
      end

      h
    end

    attr_accessor :building, :duplicates

    def initialize(id)
      self.building = Building.find(id)
      self.duplicates = []
    end

    def find_duplicates
      columns = []

      [:address1, :address2, :city, :province, :country, :postalcode].each do |c|
        columns << c unless @building.send(c).blank?
      end

      self.duplicates = eval build_queries(*columns) unless columns.empty?
    end

    [:address1, :address2, :city, :province, :country, :postalcode].each do |m|
      eval <<-METHOD
        def find_duplicates_by_#{m}(rel = Building)
          query(rel, :#{m})
        end
      METHOD
    end

    private

      def build_queries(*args)
        return if args.empty?
        "find_duplicates_by_#{args.pop}(#{build_queries(*args)})"
      end

      def query(rel, column)
        rel.where("id <> #{@building.id} AND (#{column} ILIKE ? OR NULL)", "#{@building.public_send(column)}")
      end

  end
end
