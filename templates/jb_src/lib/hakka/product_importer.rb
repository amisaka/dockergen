class Hakka::ProductImporter

  class << self
    def import(product_pid, dryrun: false)
      product = Hakka::Product.find(product_pid)

      if dryrun
        product.make_service_hash
      else
        Service.create(product.make_service_hash)
      end
    end

    def import!(product_pid, dryrun: false)
      product = Hakka::Product.find(product_pid)

      if dryrun
        product.make_service_hash
      else
        Service.create!(product.make_service_hash)
      end
    end

    def import_all(dryrun: false)
      products = []

      Hakka::Product.find_each do |product|
        products << product.make_service_hash
      end

      if dryrun
        products
      else
        Service.create(products)
      end
    end

    def import_all!(dryrun: false)
      products = []

      Hakka::Product.find_each do |product|
        products << product.make_service_hash
      end

      if dryrun
        products
      else
        Service.create!(products)
      end
    end
  end
end
