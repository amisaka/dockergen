# temporary class used to contain summaries of calls by type
class CallSummary
  attr_accessor :count, :duration, :cost
  attr_accessor :desc
  attr_accessor :leg_record
  attr_accessor :client, :parent

  def initialize(thing, client, parent = nil)
    @desc   = thing
    @client = client
    @cost   = 0
    @duration = 0
    @count  = 0
    @leg_record = []
  end

  def cost_s
    sprintf("$%.2f", cost)
  end

  def self.fmt_duration(x)
    mm, ss = x.divmod(60)
    hh, mm = mm.divmod(60)
    sprintf("%02d:%02d:%02d", hh,mm,ss)
  end

  def duration_s
    self.class.fmt_duration(duration)
  end

  def add_duration(nDuration)
    @duration += nDuration
  end
  def add_cost(nCost)
    @cost    += nCost
  end
  def add_call
    @count += 1
  end

  def add_leg(cdr)
    return if cdr.no_bill?
    add_duration(cdr.billed_duration)
    add_cost(cdr.price)
    add_call
    self
  end


  def record_leg(cdr)
    add_leg(cdr)
    @leg_record << cdr
  end

  def subtotal_call_count
    @count
  end

  def subtotal_call_duration_s
    duration_s
  end

  def subtotal_call_cost_s
    cost_s
  end


end
