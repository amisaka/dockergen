class ClientSet
  attr_accessor :clients

  def self.search(query)
    new(Client.search(query))
  end

  def initialize(clients)
    self.clients = clients
  end

  def all
    clients
  end

  def previous(client = nil)
    return nil if client.nil?
    client = client.object if client.kind_of?(ClientDecorator)
    case clients
    when Array
      i = clients.find_index(client)
      return nil if i.nil? or i == 0
      clients[i-1]
    when ActiveRecord::Associations::CollectionProxy, Client::ActiveRecord_Relation
      clients.by_name.where('crm_accounts.name < ?', client.name).last
    else
      raise NotImplementedError, "Ordering of #{clients.class} is not yet implemented."
    end
  end

  def next(client = nil)
    return nil if client.nil?
    client = client.object if client.kind_of?(ClientDecorator)
    case clients
    when Array
      i = clients.find_index(client)
      return nil unless i
      clients[i+1]
    when ActiveRecord::Associations::CollectionProxy, Client::ActiveRecord_Relation
      clients.by_name.where('crm_accounts.name > ?', client.name).first
    else
      raise NotImplementedError, "Ordering of #{clients.class} is not yet implemented."
    end
  end

end
