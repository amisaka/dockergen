require 'fileutils'
require 'csv'

class CSVReport

  REPORTS_PATH = File.join ['tmp', 'reports']
  SUPPORT_ONLY_TEMPLATE_ID = InvoiceTemplate.find_by(name: "Support Only Invoices")

  class << self
    def generate_all
      generate_clients_with_btns_not_like_phones
      generate_clients_with_duplicate_names
      generate_clients_with_more_than_one_btn
      generate_clients_with_more_than_one_site
      generate_clients_closed_without_cycles
      generate_validated_clients_without_cycles
      generate_unvalidated_clients_with_support
      generate_clients_not_closed_with_needs_review
      generate_clients_not_closed_province_null
      generate_distinct_cities_not_null_not_empty
      generate_invalid_buildings
      generate_counts
      true
    end

    def generate_clients_with_btns_not_like_phones
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'clients_with_btns_not_like_phones.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'BTN ID', 'BTN']
        Client.activated.each do |client|
          client.btns.active.where('billingtelephonenumber NOT SIMILAR TO ?', '\+?\d{10,11}').each do |btn|
            csv << [client.id, client.name, client.status, btn.id, btn.billingtelephonenumber]
          end
        end
      end
      true
    end

    def generate_clients_with_duplicate_names
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'clients_with_duplicate_names.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status']
        clients_with_duplicate_names.each do |client|
          csv << [client.id, client.name, client.status]
        end
      end
      true
    end

    def generate_clients_with_more_than_one_btn
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'clients_with_more_than_one_btn.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'BTN', 'Invoice Cycle']
        clients_with_more_than_one_btn.each do |client|
          client.btns.active.each do |btn|
            csv << [client.id, client.name, client.status, btn.billingtelephonenumber, btn.invoice_template_id]
          end
        end
      end
      true
    end

    def generate_clients_with_more_than_one_site
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'clients_with_more_than_one_site.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'Site Number', 'Site Name', 'Address']
        clients_with_more_than_one_site.each do |client|
          client.sites.activated.each do |site|
            csv << [client.id, client.name, client.status, site.site_number, site.name, site.address]
          end
        end
      end
      true
    end

    def generate_clients_closed_without_cycles
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'clients_closed_without_cycles.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'BTN', 'Invoice Cycle']
        clients_closed_without_cycles.each do |client|
          client.btns.where('invoice_template_id IS NULL').each do |btn|
            csv << [client.id, client.name, client.status, btn.billingtelephonenumber, btn.invoice_template_id]
          end
        end
      end
      true
    end

    def generate_validated_clients_without_cycles
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'validated_clients_without_cycles.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'Validated', 'BTN', 'Invoice Cycle']
        human_validated_clients_without_cycles.each do |client|
          client.btns.where('invoice_template_id IS NULL').each do |btn|
            csv << [client.id, client.name, client.status, client.human_validation_date, btn.billingtelephonenumber, btn.invoice_template_id]
          end
        end
      end
      true
    end

    def generate_unvalidated_clients_with_support
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'unvalidated_clients_with_support.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'Validated', 'BTN', 'Invoice Cycle']
        unvalidated_clients_with_support.each do |client|
          client.btns.where(invoice_template_id: SUPPORT_ONLY_TEMPLATE_ID).each do |btn|
            csv << [client.id, client.name, client.status, client.human_validation_date, btn.billingtelephonenumber, btn.invoice_template_id]
          end
        end
      end
      true
    end

    def generate_clients_not_closed_with_needs_review
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'clients_not_closed_with_needs_review.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'Needs Review']
        clients_not_closed_with_needs_review.each do |client|
          csv << [client.id, client.name, client.status, client.needs_review]
        end
      end
      true
    end

    def generate_clients_not_closed_province_null
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'clients_not_closed_province_null.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'Province']
        clients_not_closed_province_null.each do |client|
          csv << [client.id, client.name, client.status, client.province]
        end
      end
      true
    end

    def generate_distinct_cities_not_null_not_empty
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'distinct_cities_not_null_not_empty.csv']), 'wb' do |csv|
        csv << ['City']
        distinct_cities_not_null_not_empty.each do |building|
          csv << [building.city]
        end
      end
      true
    end

    def generate_invalid_buildings
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'invalid_buildings.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Building ID', 'Building Name', 'Postal Code']
        invalid_buildings.each do |building|
          begin
            client = building.sites.first.client
          rescue NoMethodError
            client = Struct.new("ClientMock", :id, :name).new(nil, nil)
          ensure
            csv << [client.id, client.name, building.id, building.name, building.postalcode]
          end
        end
      end
      true
    end

    def generate_all_clients_balance
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'all_clients_balance.csv']), 'wb' do |csv|
        csv << ['ID', 'Name', 'Status', 'BTN', 'Balance']
        Client.activated.each do |client|
          csv << [client.id, client.name, client.status, client.billingtelephonenumber, client.current_balance]
        end
      end
      true
    end

    def generate_counts
      FileUtils.mkdir_p REPORTS_PATH
      CSV.open File.join([REPORTS_PATH, 'counts.csv']), 'wb' do |csv|
        csv << ['Clients', 'Count']
        csv << ['Closed', Client.closed.count]
        csv << ['Pending Start', Client.pending_start.count]
        csv << ['Pending Close', Client.pending_close.count]
        csv << ['Unknown', Client.unknown.count]
        csv << ['Active', Client.active.count]
        csv << ['In Service', Client.activated.count]
        csv << ['Total Records in Database', Client.count]
        csv << ['BTNS', 'Count']
        csv << ['Active', Btn.active.count]
        csv << ['Closed', Btn.count - Btn.active.count]
        csv << ['Total Records in Database', Btn.count]
        csv << ['Sites', 'Count']
        csv << ['Active', Site.count - Site.select(&:terminated?).count]
        csv << ['Terminated', Site.select(&:terminated?).count]
        csv << ['Total Records in Database', Site.count]
      end
      true
    end

    private

      def clients_with_duplicate_names
        Client.find_by_sql <<-SQL
        SELECT clients.*
        FROM clients
        INNER JOIN crm_accounts cma ON cma.id = clients.crm_account_id
        INNER JOIN (
          SELECT name, COUNT(*)
          FROM crm_accounts
          GROUP BY name
          HAVING COUNT(*) > 1
        ) dupes ON cma.name = dupes.name
        ORDER BY cma.name ASC
        SQL
      end

      def clients_with_more_than_one_btn
        Client.activated.joins(:btns).group('clients.id').having('count(btns.client_id) > 1')
      end

      def clients_with_more_than_one_site
        Client.activated.joins(:sites).group('clients.id').having('count(sites.client_id) > 1')
      end

      def clients_closed_without_cycles
        Client.joins(:btns).where('btns.invoice_template_id IS NULL').closed
      end

      def human_validated_clients_without_cycles
        Client.joins(:btns).where('clients.human_validation_date IS NOT NULL AND btns.invoice_template_id IS NULL')
      end

      def unvalidated_clients_with_support
        Client.joins(:btns).where(human_validation_date: nil, :"btns.invoice_template_id" => SUPPORT_ONLY_TEMPLATE_ID)
      end

      def clients_not_closed_with_needs_review
        Client.where(needs_review: true).select{|c|c.status != :closed}
      end

      def clients_not_closed_province_null
        Client.select{|c|c.status != :closed && c.province.blank?}
      end

      def distinct_cities_not_null_not_empty
        Building.find_by_sql <<-SQL
          SELECT DISTINCT(buildings.city)
          FROM buildings
          WHERE buildings.city IS NOT NULL AND LENGTH(buildings.city) > 0
          ORDER BY buildings.city ASC
        SQL
      end

      def invalid_buildings
        Building.select(&:invalid?)
      end

  end
end
