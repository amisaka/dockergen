require 'ffaker'

class FakerMapper
  attr_accessor :phone_map, :name_map, :cdr_map, :date_offset, :btn_map, :site_map
  attr_accessor :client
  attr_accessor :fixtureWriter, :rename_phone

  def initialize
    @number_map = Hash.new  # FFakerr::PhoneNumber.phone_number
    @phone_map = Hash.new  # FFakerr::PhoneNumber.phone_number
    @name_map  = Hash.new  #     FFakerr::Name.name
    @cdr_map   = Hash.new
    @site_map  = Hash.new
    @btn_map   = Hash.new
    @number_map   = Hash.new
    @callid_map   = Hash.new
  end

  def dup_object(thing, map)
    return nil unless thing
    return map[thing.id] if map[thing.id]
    newthing = thing.dup
    newthing.save!
    map[thing.id] = newthing
    yield newthing
    newthing
  end

  def map_number(number)
    return nil if number.blank?
    return @number_map[number] if @number_map[number]
    @number_map[number] = FFaker::PhoneNumber.short_phone_number
  end

  def map_callid(callid)
    return nil if callid.blank?
    return @callid_map[callid] if @callid_map[callid]
    @callid_map[callid] = FFaker::Internet.mac
  end

  def map_datetime(datetime)
    return nil if datetime.blank?
    datetime += date_offset
  end

  def map_btn(obtn)
    return nil unless obtn
    dup_object(obtn, @btn_map) { |btn|
      btn.billingtelephonenumber = map_number(btn.billingtelephonenumber)
      btn.client = client
      btn.save!
      btn.savefixturefw(fixtureWriter) if fixtureWriter
    }
  end

  def map_site(osite)
    return nil unless osite
    dup_object(osite, @site_map) { |site|
      site.name = FFaker::Company.name
      site_site_number = "0"
      site.client = client
      site.savefixturefw(fixtureWriter) if fixtureWriter
    }
  end

  def map_phone(ophone)
    return nil unless ophone
    dup_object(ophone, @phone_map) { |phone|
      phone.notes        = "generated from #{ophone.id}"
      phone.phone_number = map_number(phone.phone_number)
      phone.sip_username = map_number(phone.sip_username)
      phone.site         = map_site(phone.site)
      phone.email        = FFaker::Internet.email
      phone.sip_password = nil
      phone.phone_mac_addr = FFaker::Internet.mac
      phone.btn          = map_btn(phone.btn)
      phone.save!
      phone.savefixturefw(fixtureWriter) if fixtureWriter
    }
  end

  def dup_cdr(cdr, ds2)
    dup_object(cdr, @cdr_map) { |cdr2|
      # now go through each part of the CDR and duplicate it.
      cdr2.recordid     = FFaker::Guid.guid
      cdr2.starttime = nil
      cdr2.startdatetime = map_datetime(cdr.startdatetime)

      if @rename_phones
        cdr2.calledbtn    = map_btn(cdr.calledbtn)
        cdr2.callingbtn   = map_btn(cdr.callingbtn)
        cdr2.calledphone  = map_phone(cdr.calledphone)
        cdr2.callingphone = map_phone(cdr.callingphone)

        cdr2.usernumber    = map_number(cdr.usernumber)
        cdr2.groupnumber   = map_number(cdr.groupnumber)
        cdr2.callednumber  = map_number(cdr.callednumber)
        cdr2.callingnumber = map_number(cdr.callingnumber)
        cdr2.dialeddigits  = map_number(cdr.dialeddigits)
        cdr2.networktranslatednumber = map_number(cdr.networktranslatednumber)
        cdr2.redirectingnumber = map_number(cdr.redirectingnumber)
        cdr2.originatenumber   = map_number(cdr.originatenumber)
        cdr2.destinationnumber = map_number(cdr.destinationnumber)
      end

      cdr2.localcallid   = map_callid(cdr.localcallid)
      cdr2.remotecallid  = map_callid(cdr.remotecallid)
      cdr2.releasedatetime = map_datetime(cdr.releasedatetime)
      cdr2.otherpartyname  = FFaker::Name.name
      cdr2.relatedcallid   = map_callid(cdr.relatedcallid)
      cdr2.transfer_relatedcallid = map_callid(cdr.transfer_relatedcallid)
      cdr2.broadworksanywhere_relatedcallid = map_callid(cdr.broadworksanywhere_relatedcallid)
      cdr2.answerdatetime = map_datetime(cdr.answerdatetime)
      cdr2.datasource = ds2
      cdr2.call = nil
      cdr2.invoice = nil
      cdr2.billingreason = nil
      begin
        cdr2.save!
      rescue #PG::StringDataRightRuncation
        byebug
        puts "hello"
      end
      cdr2
    }
  end

end
