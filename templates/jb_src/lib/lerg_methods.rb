# LERG files have americanized names for some provinces.
require 'find'

module LergMethods
  def self.cleanProvince(prov)
    prov = prov.strip
    return (case prov
            when 'PQ'
              "QC"
            when 'NF'
              "NL"
            when "VU"
              "NU"
            else
              prov
            end
           )
  end

  def self.latestLergOrZipFile(dir, regex)
    latest_LERG = nil
    latest_mtime = nil

    puts "Processing #{dir}..."
    begin
      Find.find(dir) do |path|
        stat = File.stat(path)
        next if stat.directory?

        mtime = stat.mtime
        latest_mtime ||= mtime
        # look for most recent file ending with either
        # LERG8.DAT, or LERG*.ZIP.

        basename = File.basename(path)
        if (regex.match(basename) || basename =~ /^LERG.*\.ZIP/)
          if mtime >= latest_mtime
            #puts "newer #{path} over #{latest_LERG}, as #{mtime} > #{latest_mtime}"
            latest_LERG = path
            latest_mtime = mtime
          end
        end
      end
    rescue Errno::ENOENT
      puts "can not find #{dir}"
      return nil
    end

    return latest_LERG
  end

  def self.latestLerg6ZipFile(dir)
    return self.latestLergOrZipFile(dir, /^LERG6\.DAT/)
  end

  def self.latestLerg8ZipFile(dir)
    return self.latestLergOrZipFile(dir, /^LERG8\.DAT/)
  end

  def self.findlerg_file(lergbase, type)
    tmpdir = nil

    if(lergbase =~ /LERG.*\.ZIP/)
      workingtmp = Rails.root.join("tmp")
      if ENV['SG1_TMPDIR']
        workingtmp = ENV['SG1_TMPDIR']
      end
      tmpdir = File.join(workingtmp,"lerg#{$$}")
      cmd    = sprintf("mkdir -p %s && cd %s && unzip %s %s",
                       tmpdir, tmpdir, lergbase, type)
      code = system(cmd)

      lergbase = File.join(tmpdir, type)
    end

    return lergbase,tmpdir
  end

  def self.findlerg6(lerg6file)
    return findlerg_file(lerg6file, "LERG6.DAT")
  end

  def self.findlerg8(lerg8file)
    return findlerg_file(lerg8file, "LERG8.DAT")
  end

end


