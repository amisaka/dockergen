class CallDetailPage
  attr_accessor :pagenum
  attr_accessor :callsummary_by_service

  def initialize(num)
    @pagenum = num
    @callsummary_by_service = []
  end

  def pagenum_id
    "invoice-page-#{@pagenum}"
  end

  def add_service(callsummary)
    @callsummary_by_service << callsummary
  end

end
