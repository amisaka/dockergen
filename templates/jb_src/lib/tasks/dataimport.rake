require 'timeout'
require 'date'
def convert_to_date_time(d)
  begin
    dateTime = DateTime.strptime(d, '%Y %d/%m %H:%M:%S')
  rescue => e
    $stderr.puts e.message
    byegbug
    nil
  end
end

def convert_to_date(d)
  begin
    if d.blank?
        return nil
    elsif d.length > 10
        return DateTime.parse(d)
    elsif not /^20[0-9]{2}/.match(d).nil?
     return DateTime.strptime(d, '%Y-%m-%d')
    else
     return DateTime.strptime(d, '%d-%m-%Y')
    end
  rescue TypeError
    return d
  rescue ArgumentError
    $stderr.puts "Invalid argument for #{d}"
    raise
  end
end


def get_prompt(question)
  STDOUT.print "#{question}>"

  begin
    prompt = Timeout::timeout(20) { STDIN.gets.chomp }
    if prompt == 'Y'
      return true
    else
      return false
    end
  rescue Timeout::Error
    puts "\nTimeout exceeded..... doing nothing!"
    return false
  end
end


    def log_not_imported(hashes)
        logLocation = 'log/import_clients.log'
      File.open(logLocation, "ab") do |f|
        hashes.each {|hash| f.puts "Could not import #{hash.to_s}"}
      end

        return logLocation
    end




def formatCurrency(amountStr)
    return nil if amountStr.nil?
    return amountStr.gsub('$', '').gsub(' ', '').gsub(',', '.').gsub("\u00A0", "")
end

def parse_duration(duration_str)
  return 0 if duration_str.blank?
  hh, mm, ss = duration_str.split(':').map(&:to_i)
  3600 * hh + 60 * mm + ss
end

def strip_btn_leading_zeros(btn)
  idx = 0
  btn.split("").each do |c|
    if c != "0"
      btn=btn[idx..-1]
      break
    else
      idx+=1
    end
  end
  return btn

end


def find_client_by_btn(btn)
  btn = Btn.find_by_billingtelephonenumber(btn)
  return btn.client if not btn.nil?
  return nil
end


#####################################################
####  billing balance only                       ####
#####################################################
namespace :dataimport do
    desc %Q(Update the balances in billing dataimport:billingBalanceOnly[db/data/20180130/someCsvFile,[setZero]])
    task :billingBalanceOnly, [:file,:setZero] => [:environment] do |t, args|
        include ActiveRecord::Loadable
        checkFile(args, 'dataimport:billingBalanceOnly')

        if not args[:setZero].nil? and args[:setZero] == 'setZero'
            puts "Fetching non support clients...."
            nonSupportClients = Client.select {|c| print '.'; not c.isSupport? }
            puts "\n"

            nonSupportClients.each_with_index {|c,i|
                printProgress("Setting balances to zero", i, nonSupportClients.count)
                if c.billing.nil?
                    $stderr.puts "No billing for client #{c.to_s}"
                    next
                end
                c.billing.balance = 0
            }
        end

        # Notes:
        # Btn.find_by_billingtelephonenumber('4164222732').client
        # Btn.find_by_billingtelephonenumber('4164222732').client.billing
        mapping = {
            :client_id => {
                :csvHeader  => "BTN",
                :code       => Proc.new { |csvValue|
                    client = Client.find_by(:crm_account_id => csvValue)
                    $stderr.puts "No client found for BTN #{csvValue}" if client.nil?
                    if client.nil?
                        nil
                    else
                        client.id
                    end

                },
            },
            :balance => {
                :csvHeader => "Balance",
                :code   => Proc.new {|csvValue| formatCurrency(csvValue)}
            },
            '#BTN'  => "BTN",
        }

        primaryKey = :client_id

        Billing.update_from_csv(args[:file], mapping, primaryKey, {:dontCreate => true})
    end
end

#####################################################
####  calls                                      ####
#####################################################
namespace :dataimport do
  desc "Import 'call' data ex: dataimport:calls[\"db/data/20180130/2017_SEP_Call-list-2.txt\"] "
  task :calls, [:file] =>  [:environment] do |t,args|
    puts ""
    year=args[:file].split('/')[-1].split('_')[0]
    begin
      DateTime.strptime(year,'%Y')
    rescue DateTime::ArgumentError
      puts "Tried to get year from filename, #{year} does not appear to be valid. example: '2018_SEP_Call-list.txt'"
      next
    end

    puts "importing call data from: '#{args[:file]}' using '#{year}' as year."
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")

    puts "here we go...."
    calls = []
    CSV.open(args[:file], 'rb', encoding: 'UTF-8', col_sep: '|') do |csv|
         csv.each do |row|
          byebug if row[8].nil?
           calls << {
             btn: row[0],
             did: row[1],
             date: row[2],
             time: row[3],
             call_date: convert_to_date_time([year, row[2], row[3]].join(" ")),
             called_number: row[4],
             area: row[5],
             province: row[6],
             duration: parse_duration(row[7]),
             cost: BigDecimal.new(row[8])
        }
      end
    end
    c = 0
    total = calls.length
    print "\n"
    calls.each do |call|
      print ("\u001b[1000D#{c}/#{total}")
      Call.create(call)
      c+=1
    end
    print "\n"

  end

end




#####################################################
####  lineofbusiness                             ####
#####################################################
namespace :dataimport do
  desc "Import 'call' data ex: dataimport:lineofbusiness[\"db/data/20180214/lineofbusiness.csv\"] "
  task :lineofbusiness, [:file] =>  [:environment] do |t,args|
    puts ""

    puts "importing lineofbusiness data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")

    puts "here we go...."
    lobs = {}
    LineOfBusiness.all.each do |lob|
      lobs["#{lob.Pid}"] = lob
    end

    clients_to_update = []
    # CSV.open(args[:file], 'rb', encoding: 'UTF-8', col_sep: ',') do |csv|
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|
      lob_headers = nil
      count = 0
      csv.each do |row|
        print ("\u001b[1000D#{count}")
        lob_headers = row.headers[4..-1] if lob_headers == nil # get all the line of business pid's
           # puts "#{row['BTN']}"
           # puts "#{row['1']},#{row['2']} "
        c = Client.find_by(:crm_account_id => row['BTN'])
        if c == nil # something up with this btn - check if there is leading '0's and try again.
          idx = 0
          row['BTN'].split("").each do |chr|
            if chr != "0"
              c = Client.find_by(:crm_account_id => row['BTN'][idx..-1])
              c = row['BTN'] if c == nil # set c to the btn so we can later see what failed (dev data might be bad)
              #raise "error: Could not find clients with: '#{row['BTN'][idx..-1]}' , row['BTN'] == #{row['BTN']}" if c == nil
              break
            else
              idx+=1
            end
          end
        end

        clobs = []
        lob_headers.each do |lob|
          clobs << lobs[lob] if row[lob] == "1"
        end

        clients_to_update << [c, clobs]
        count+=1
      end
    end

    print ("\n")
    print ("updating line_of_businesses....\n")

    count = 0
    total = clients_to_update.length
    not_loaded = []
    clients_to_update.each do |ctu|
      begin
        print ("\u001b[1000D#{count}/#{total}")
        count+=1
        ctu[0].line_of_businesses = ctu[1]
        ctu[0].save
      rescue
        not_loaded << ctu
        # debugger
      end
    end

    # list the clients that didn't get saved for whatever reason.
    if not_loaded.length > 0
      puts "not loaded:"
      not_loaded.each do |ctu|
        puts "ctu[0] = '#{ctu[0]}' , ctu[1] = '#{ctu[1]}'"
      end
    end
    print "\n"

  end

end


def checkFile(args, taskName, errorMsg = nil?)
    $stderr.puts "\n"
    if args[:file].nil? or args[:file].empty?
        $stderr.puts (errorMsg or "You need to specify the file path like this: #{taskName}[\$relativeProjectPath]")
        return false
    end

    filePath = args[:file]

    ### Try again by removing sge/ out of the path
    if not File.exist?(filePath) and not filePath.index('sge/').nil?
        filePath  = filePath.slice((filePath.index('sge/') +4)..filePath.length)
    end

    if not File.exist?(filePath)
        $stderr.puts "There is no file at #{filePath}"
        return false
    end

    return filePath
end

#####################################################
# "Services" (which are actually products)
#####################################################
namespace :dataimport do
    desc "Import products spreadsheet into services dataimport:services[db/data/20180205/productsQuery.csv]"
    task :services, [:file] =>  [:environment] do |t,args|
        exit 1 if not checkFile(args, "dataimport:services")

        Service.update_from_products_csv(args[:file])
    end
end


#####################################################
# Update whatever class from whatever csv file
#####################################################
namespace :dataimport do
    desc "Import specified spreadsheet into specified class using it's IMPORT_MAPPING constant; Can use DONT_UPDATE=true."
    task :generic, [:class, :file] => [:environment] do |t, args|
        errorMsg = "File path must be specified like this: dataimport:generic[$className,$filePath])"
        exit 1 if not filePath = checkFile(args, "dataimport:generic", errorMsg)

        classObject = Object.const_get(args[:class])

        options = {}
        options[:dontUpdate] = true if not ENV['DONT_UPDATE'].nil? and ENV['DONT_UPDATE']=="true"

        #truncate(classObject) if ENV['TRUNCATE'] == 'TRUE'

        classObject.update_from_csv(filePath, nil, nil, options)
    end
end



#####################################################
####  billing                                    ####
#####################################################
namespace :dataimport do
  desc "Import 'Billing data' data ex: dataimport:billing[db/data/20180205/billingdata.csv]"
  task :billing, [:file] =>  [:environment] do |t,args|
    puts "importing billing data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")

    billings = []
    not_loaded = []
    count = 0
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|
      csv.each do |row|
        count+=1
        print ("\u001b[1000DParsing CSV: #{count}")



        begin
            columnsWithDates = ['StartDate', 'BillingStartDate', 'LastUsageDate', 'LastStatementDate', 'LastPaymentDate']
            validateDateFormat(row, columnsWithDates, 'yearFirst')

          billings << {
            client: find_client_by_btn(strip_btn_leading_zeros(row['BTN'])),
            balance: formatCurrency(row['Balance']),
            activated_at: row['StartDate'],
            billing_start_date: row['BillingStartDate'],
            last_usage_date: row['LastUsageDate'],
            last_statement_date: row['LastStatementDate'],
            last_statement_amount: formatCurrency(row['LastStatementAmount']),
            last_payment_amount: formatCurrency(row['LastPaymentAmount']),
            last_payment_date: row['LastPaymentDate'],
            deactivated_at: row['TerminatedOnDate'],
            load_id: row['LoadID'],
            leg_pid: row['LegPID']
          }
        rescue => e
          if not_loaded.count < 4
            $stderr.puts e
            byebug
          end
          not_loaded << {
            client: row['BTN'],
            balance: row['Balance'],
            activated_at: row['StartDate'],
            billing_start_date: row['BillingStartDate'],
            last_usage_date: row['LastUsageDate'],
            last_statement_date: row['LastStatementDate'],
            last_statement_amount: row['LastStatementAmount'],
            last_payment_amount: row['LastPaymentPmount'],
            last_payment_date: row['LastPaymentDate'],
            terminated_on_date: row['TerminatedOnDate'],
            leg_pid: row['LegPID'],
            load_id: row['LoadID'],
            error_msg: e.message
          }
        end
      end
    end
    print( "\n")


    puts "#{Billing.count} billing objects in database before update"
    count=0
    billingsCreated = []
    billingsUpdated = []
    total=billings.length
    billings.each do |billing|
      print ("\u001b[1000DUpdating data:#{count}/#{total}")
      begin
        # If the client is nil, that means we couldn't even find it by it's BTN. Skip
        if billing[:client].nil?
            not_loaded << billing
            next
        end

        # Find a billing for this client_id
        bObj = Billing.find_by("client_id = ?", billing[:client].id)

        if bObj.nil?
            # If there isn't one, then we are certain we have to create it
            bObj = Billing.create!(billing) # we do a create if it doesn't exist
            billingsCreated.push(bObj)

            # ... and assign it to the client
            billing[:client].billing = bObj
        else
            bObj.update(billing)
            billingsUpdated.push(bObj)
            bObj.save!
        end

      rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => e
        puts "Could not import billing with ID=#{billing[:id]}"
        billing[:error_msg] = e.message
        puts "\t#{e.message}"
      end
      count+=1
      next
    end
    print("\n")
    print("\n")

    puts "#{Billing.count} billing objects in database after update"
    puts "#{billingsCreated.count} billing objects created"
    puts "#{billingsUpdated.count} billing objects updated"

    if not_loaded.length > 0
      print("\n")
      not_loaded.each do |nl|
        puts "#{nl[:client]} was not loaded.\n\t #{nl}\n"
      end
      print("\n")
      puts "#{not_loaded.length}/#{total+not_loaded.length} not loaded!"
    else
      puts "#{total} loaded!"
    end

  end

end



def validateDateFormat(row, elementKeysWithDates, format = 'yearNotFirst')

        elementKeysWithDates.each{ |index|
            valueToCheck = (row[index] or '')

            next if valueToCheck.empty?

            case format
                when 'yearNotFirst'
                    if valueToCheck.strip[0..2] == '201'  # Does the cell begin with a year? If so, raise error
                        # Todo: change above to regex
                        raise "Date format must be DD-MM-YYYY. Got #{row[index]} for column #{index} in row \n#{row}"
                    end
                when 'yearFirst'
                    if /^[0-9]{4}/.match(valueToCheck).nil?  # if the date does not start with a year, raise error
                        # Todo: improve regex
                        raise "Date format must be YYYY-MM-DD. Got #{row[index]} for column #{index} in row \n#{row}"
                    end
                else
                    "date format specification #{format} not valid for validateDateFormat method"
            end
        }
end

#####################################################
####  payment                                    ####
#### Date formats must be DD-MM-YYYY
#### user will be prompted for old data destruction,
#### but will continue import if that is not desired
#####################################################
namespace :dataimport do
  desc "Import 'Payments data' data ex: dataimport:payment[db/data/20180205/billingdata.csv]"
  task :payment, [:file] =>  [:environment] do |t,args|
    puts "importing payment data from: '#{args[:file]}'"
    #next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")
    payments = []
    not_loaded = []
    count = 0
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|
      csv.each do |row|
        print ("\u001b[1000DParsing CSV: #{count}")

        columnsWithDates = ['ReceiptDate', 'PostedDateTime', 'CreatedOn']
        validateDateFormat(row, columnsWithDates)

        begin
          payments << {
            client: find_client_by_btn(strip_btn_leading_zeros(row['BTN'])),
            date: convert_to_date(row['ReceiptDate']), #, '%d-%m-%Y') || "", #row['Date'],
            posted_date: convert_to_date(row['PostedDateTime']), #'%d-%m-%Y') || "", #row['PostedDateTime'],
            amount:  formatCurrency(row['Amount']),
            balance: formatCurrency(row['Balance']),
            type: ( row['TransactionType'].gsub(' ','') if row['TransactionType'] ) ,
            description: row['Description'],
            reason: row['CreditMemoReason'],
            posted: row['Posted'],
            imported: row['imported'],
            reference_number: row['ReferenceNumber'],
            check_number: row['CheckNo'],
            payment_method: row['DebitMemoReasonPID'],
            created_at: convert_to_date(row['CreatedOn']), #row['CreatedOn'],
            GLAccount: row['GLAccount'],
            #btn: row['BTN'],

          }

            #Check if the client got found, cause you know it's not going to load
            lastPayment = payments.last
            if lastPayment[:client].nil?
                lastPayment = payments.pop
                lastPayment[:btn] = row['BTN']
                lastPayment[:error_msg] = "client is nil, preemptive removal from remaining import"
                not_loaded << lastPayment
            end

        rescue => e

          not_loaded << {
            date: convert_to_date(row['ReceiptDate']), #, '%d-%m-%Y') || "", #row['Date'],
            posted_date: convert_to_date(row['PostedDateTime']), #'%d-%m-%Y') || "", #row['PostedDateTime'],
            amount: row['Amount'], #.gsub('$', '').gsub(' ', '').gsub(',', '.').gsub("\u00A0", ""),
            balance: row['Balance'], #.gsub('$', '').gsub(' ', '').gsub(',', '.').gsub("\u00A0", ""),
            type: ( row['TransactionType'].gsub(' ','') if row['TransactionType'] ) ,
            description: row['Description'],
            reason: row['CreditMemoReason'],
            posted: row['Posted'],
            imported: row['imported'],
            reference_number: row['ReferenceNumber'],
            check_number: row['CheckNo'],
            payment_method: row['DebitMemoReasonPID'],
            created_at: convert_to_date(row['CreatedOn']), #row['CreatedOn'],
            GLAccount: row['GLAccount'],
            btn: row['BTN'],
            error_msg: e.message,

          }
        end

        count+=1
      end
    end
    print( "\n")

    #### Prompt twice to ask if payments should be deleted
    puts "Delete all payments before load? (THIS IS VERY DESTRUCTIVE)"
    res = get_prompt("DESTRUCTIVE OPERATION: enter 'Y' to nuke payments.")
    if res
      puts "are you really sure?"
      res = get_prompt("DESTRUCTIVE OPERATION: enter 'Y' to nuke payments")
      if res
        payments_total = Payment.all.length
        count = 0
        Payment.all.each do |p|
          print ("\u001b[1000D Deleted:#{count}/#{payments_total}")
          p.delete
          count+=1
        end
        print("\n")
      end
    end

    puts "#{Payment.count} before load"
    count=0
    total=payments.length
    payments.each do |payment|
      print ("\u001b[1000DCreated data:#{count}/#{total}")
      begin
        Payment.create!(payment) # we do a create if it doesn't exist
      rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => e
        puts "Could not import payment with ID=#{payment[:id]}"
        puts "\t#{e.message}"
        payment[:error_msg] = e.message
        not_loaded << payment
      end
      count+=1
      next
    end
    print("\n")
    if not_loaded.length > 0
      print("\n")
      not_loaded.each do |nl|
        puts "#{nl[:client]} was not loaded.\n\t #{nl}\n"
      end
      log_not_imported(not_loaded)
      print("\n")
      puts "#{not_loaded.length}/#{total+not_loaded.length} not loaded!"
    else
      puts "#{total} loaded!"
    end
    puts "#{Payment.count} after load"

  end

end


#####################################################
####  statementtype                              ####
#####################################################

namespace :dataimport do
  desc "Import 'StatementType' data ex: dataimport:StatementType[db/data/20180205/statement_types.csv]"
  task :StatementType, [:file] =>  [:environment] do |t,args|
    puts "importing StatementType data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")

    statementtypes = []
    CSV.foreach(args[:file], :headers => true) do |row|
           statementtypes << {
             name: row[2],
             description: row[3]
        }
    end

    statementtypes.each do |st|
      StatementType.create(st)
    end
  end

end




#####################################################
####  InvoiceTemplateTypes                       ####
#####################################################

namespace :dataimport do
  desc "Import 'InvoiceTemplatetypes' data ex: dataimport:InvoiceTemplateTypes[db/data/20180205/invoice_template.csv]"
  task :InvoiceTemplateTypes, [:file] =>  [:environment] do |t,args|
    puts "importing InvoiceTemplateTypes data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")

    count = 0
    total = 0
    CSV.foreach(args[:file], :headers => true) do |row|
      iv = InvoiceTemplate.find_by(:invoice_cycle => row["Billing Cycle"])
      total+=1
      if(not iv)
        count+=1
        InvoiceTemplate.create(:name => row["Description"], :invoice_cycle => row["Billing Cycle"])
      else
        # iv.name.include? "old" # InvoiceTemplate doesn't have activate/inactive so we can't do this part yet
      end
    end
    puts "#{count}/#{total} invoicetemplates added."
  end
end


namespace :dataimport do
  desc "Delete all InvoiceTemplate with 'old' in the invoice_cycle"
  task InvoiceTemplate_Delete_Old: :environment do # |t|
    puts ""
    next unless get_prompt("Deleting all InvoiceTemplate with 'old' in the invoice_cycle, enter 'Y' to continue.")

    count = 0
    total = InvoiceTemplate.all.length
    deleted_count = 0
    puts ""
    InvoiceTemplate.all.each do |iv|
      count+=1
      if iv.invoice_cycle.include? "old"
        puts "#{iv.as_json}"
        puts ""
        iv.delete

        deleted_count+=1
      end
    end
    puts "#{deleted_count}/#{total} deleted. #{count-deleted_count}/#{total} not deleted"
  end

end


#####################################################
####  FIX LineOfBusiness PID                     ####
#####################################################

namespace :dataimport do
  desc "assign LineOfBusiness.pid to id"
  task AssignLineOfBusinessPID: :environment do # |t|
    puts "Assigning Line of business"
    next unless get_prompt("Assigning PID to ID for LineOfBusiness , enter 'Y' to continue. - won't update if PID not nil")

    count = 0
    total = LineOfBusiness.all.length
    updated_count = 0

    LineOfBusiness.all.each do |lob|
      count+=1
      if(not lob.Pid)
        lob.Pid = lob.id
        lob.save
        updated_count+=1
      end
    end
    puts "#{updated_count}/#{total} updated. #{count-updated_count}/#{total} not updated"
  end

end

#####################################################
####  FIX PhoneServicetype PID                   ####
#####################################################

namespace :dataimport do
  desc "assign PhoneServiceType.pid to id"
  task AssignPhoneServiceTypePID: :environment do # |t|
    puts "Assigning Line of business"
    next unless get_prompt("Assigning PID to ID for PhoneServiceType , enter 'Y' to continue. - won't update if PID not nil")

    count = 0
    total = PhoneServiceType.all.length
    updated_count = 0

    PhoneServiceType.all.each do |pst|
      count+=1
      if(not pst.Pid)
        pst.Pid = pst.id
        pst.save
        updated_count+=1
      end
    end
    puts "#{updated_count}/#{total} updated. #{count-updated_count}/#{total} not updated"
  end

end



#####################################################
####  DIDSupplier                                ####
#####################################################

namespace :dataimport do
  desc "Import 'DIDSupplier' data ex: dataimport:DIDSupplier[db/data/20180205/didsuppliers.csv]"
  task :DIDSupplier, [:file] =>  [:environment] do |t,args|
    puts "importing DIDSuppliers data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")

    count = 0
    total = 0
    updated_count = 0
    CSV.foreach(args[:file], :headers => true) do |row|
      ds = DIDSupplier.find_by(:name => row["Supplier"])
      total+=1
      if(not ds)
        count+=1
        DIDSupplier.create(:name => row["Supplier"], :Pid => row["SupplierPID"])
      else
        if(not ds.Pid)
          ds.Pid = row["SupplierPID"]
          ds.save
          updated_count+=1
        end
      end
    end
    puts "#{count}/#{total} DIDSupplier added! #{updated_count}/#{total} updated!"
  end

end


#####################################################
####  Phones qryPhones 2018-02-20.xlsx (3.27mb)  ####
####  INPROGRESS DO NOT USE                      ####
#####################################################
namespace :dataimport do
  desc "Import 'Phones data' data ex: dataimport:phones[db/data/20180205/billingdata.csv]"
  task :phones, [:file] =>  [:environment] do |t,args|

    puts "importing phones data from: '#{args[:file]}'"
    #next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")
    phones = []
    not_loaded = []
    count = 0
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|
      csv.each do |row|
        print ("\u001b[1000DParsing CSV: #{count}")

        ratecenter = row['RatePlanPID'] || row['ResultantRatePlanPID']
        if ratecenter != nil
          begin
            rc = RateCenter.find(ratecenter)
          rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique, ActiveRecord::RecordNotFound => e
            # what do I do here?
            puts "#{count} ratecenter with id: #{ratecenter} not found!  row['BTN']=#{row['BTN']}"
          end
        else
          # what do I do here?
          puts "#{count} - ratecenter is nil! row['BTN']=#{row['BTN']} #{row['TerminationDate']}"
        end

        begin
          phones << {
            client: find_client_by_btn(strip_btn_leading_zeros(row['BTN'])),
            display_text: row['DisplayText'],
            description: row['Description'],
            did_supplier: DIDSupplier.where(:Pid => row['Supplier']) ,
            phone_service_type: PhoneServiceType.where(:Pid => row['ServiceTypePID']),
            ipaddress: row['IPAddress'],
            sip_password: row['Password'],
            phone_mac_addr: row['MacAddress'],
            created_at: convert_to_date(row['CreatedOn']),
            updated_at: convert_to_date(row['UpdatedOn']),
            rate_center: RatePlan.where(:Pid => ratecenter), # not implemented
            defaultorigination: row['DefaultOrigination'],
            defaultdestination: row['DefaultDestination'],
            line_of_businesses: LineOfBusiness.find(row['LineOfBusinessPID']),

          }
        rescue
          # debugger
        end
        count+=1
      end
    end
    print( "\n")
  end

end

#####################################################
####  Prorate                                    ####
#####################################################

namespace :dataimport do
  desc "Import 'Prorate data' data ex: dataimport:prorates[db/data/20180416/tblProrate.csv]"
  task :prorates, [:file] =>  [:environment] do |t,args|

    puts "importing Prorate data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")
    prorates = []
    count = 0
    if Prorate.all.length > 0
      next unless get_prompt("Prorate models already exist! Prorate.all.length:#{Prorate.all.length} , enter 'Y' to continue.")
    end
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|
      csv.each do |row|
        print ("\u001b[1000DParsing CSV: #{count}")
        prorates << {
          prorate_pid: row['ProratePID'],
          meaning: row['Meaning']
        }
        count+=1
      end
    end
    prorates.each do |pr|
      Prorate.create(pr)
    end

    print("\n")
    puts "#{count} Prorate added!"
  end

end

#####################################################
####  Chargemode                                 ####
#####################################################

namespace :dataimport do
  desc "Import 'Chargemode data' data ex: dataimport:chargemodes[db/data/20180416/tblChargemode.csv]"
  task :chargemodes, [:file] =>  [:environment] do |t,args|

    puts "importing Chargemode data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")
    chargemodes = []
    count = 0
    if ChargeMode.all.length > 0
      next unless get_prompt("ChargeMode models already exist! ChargeMode.all.length:#{ChargeMode.all.length} , enter 'Y' to continue.")
    end
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|
      csv.each do |row|
        print ("\u001b[1000DParsing CSV: #{count}")
        chargemodes << {
          charge_mode_pid: row['ChargeModePID'],
          mode: row['ChargeMode']
        }
        count+=1
      end
    end
    chargemodes.each do |pr|
      ChargeMode.create(pr)
    end

    print("\n")
    puts "#{count} ChargeMode added!"
  end

end

#####################################################
####  Billable Services (aka charges)            ####
#####################################################

namespace :dataimport do
  desc "Import 'Billable Services (aka charges) data' data ex: dataimport:billable_services[db/data/20180416/qryExportChargeAssignment.csv]"
  task :billable_services, [:file] =>  [:environment] do |t,args|

    puts "importing billable_service data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")

    count = 0
    total = 0
    already_loaded = 0
    billable_services_created = 0
    not_loaded = []
    billable_services = []
    bad_chargeassignment = []
    csv_length = CSV.read(args[:file]).length
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|


      csv.each do |row|

        printf "\u001b[1000DParsing CSV: %-4s (#{count}/#{csv_length}) updated:#{already_loaded}/#{csv_length} created:#{billable_services_created}/#{csv_length}", "#{((count.to_f/csv_length)*100).round(2)}%"
        total+=1

        if(row['BTN'] == "8581118" || row['BTN'] == "6693789")
          client = find_client_by_btn("000#{row['BTN']}")
        else
          client = find_client_by_btn(strip_btn_leading_zeros(row['BTN']))
        end

        product = Product.where(:product_pid => row['ProductPID'])[0] if row['ProductPID'] != nil
        qty = row['Quantity'].to_i

        unitprice = row['UnitPrice'].gsub('$','')
        unitprice = unitprice.gsub(',','')
        reference_number = row['ReferenceCode']
        activated = row['Active'].to_i.zero?
        # chargemode = ChargeMode.where(:charge_mode_pid => row['ChargeModePID'])[0] if row['ChargeModePID'] !=nil

        chargemode = ChargeMode.where(:charge_mode_pid => row['ChargeModePID'])[0] if row['ChargeModePID'] !=nil
        if chargemode == nil
          bad_chargeassignment << "pid: #{row['ChargeAssignmentPID']} has a bad charge assignment '#{row['ChargeModePID']}'"
        end
        assignmentlevel = row['AssignmentLevelPID'].to_i
        recurrence_type = row['RecurrenceTypePID'].to_i

        prorate_start = Prorate.where(:prorate_pid => row["StartProrateTypePID"])[0] if row["StartProrateTypePID"] != nil


        prorate_end = Prorate.where(:prorate_pid => row["TermProrateTypePID"])[0] if row["TermProrateTypePID"] != nil
        charge_pid = row["ChargeAssignmentPID"]
        asset = Service.where(:service_pid => row["ServicePID"])[0] if row["ServicePID"] !=nil

        billing_period_started_at = convert_to_date(row["StartDate"])
        billing_period_ended_at = convert_to_date(row["TermDate"])
        department_pid = row["SiteID"]

        supplier_account_no = row["SupplierAccountNo"]
        password1 = row["Password1"]
        supplier = DIDSupplier.where(:pid => row["Supplier"])[0] if row["Supplier"] != nil

        bs_hash = {
          client: client,
          product: product,
          qty: qty,
          department_pid: department_pid,
          activated: activated,
          assignment_level: assignmentlevel,
          recurrence_type: recurrence_type,
          prorate_start: prorate_start,
          prorate_end: prorate_end,
          unitprice: unitprice,
          charge_pid: charge_pid,
          reference_number: reference_number,
          billing_period_started_at: billing_period_started_at,
          billing_period_ended_at: billing_period_ended_at,
          asset: asset,
          supplier_account_no: supplier_account_no,
          password1: password1,
          supplier: supplier,
          charge_mode: chargemode
        }

        if(BillableService.where(:charge_pid => charge_pid).length == 0)
          begin
            bs = BillableService.create!(bs_hash)
            billable_services_created+=1
            if bs.recurrence_type == "One-time"
              bs.deactivate
              bs.save!
            end

          rescue ActiveRecord::RecordInvalid
            not_loaded << row
          end

        else
          begin
            bs = BillableService.where(:charge_pid => charge_pid)[0]
            bs.update!(bs_hash)
            if bs.recurrence_type == "One-time"
              bs.deactivate
              bs.save!
            end

            already_loaded+=1
          rescue ActiveRecord::RecordInvalid
            not_loaded << row
          end

        end
        count+=1
      end
    end
    print("\n")
    puts "#{billable_services_created}/#{total} new billable_services"
    puts "#{already_loaded}/#{total} already loaded"
    print( "\n")

    if not_loaded.length > 0
      failure_file = "billable_services_failure_#{DateTime.now.strftime("%s")}.csv"
      CSV.open(failure_file, 'w',
               :write_headers=> true,
               :headers => not_loaded[0].headers,
               encoding: 'UTF-8', col_sep: ',') do |csv|
        not_loaded.each do |nl|
          csv << nl
        end
       end
      puts "not loaded due to ActiveRecord::RecordInvalid: #{not_loaded.length}/#{total} , see #{failure_file}"
    end
  end

end


#####################################################
####  EmailContacts                              ####
#####################################################

namespace :dataimport do
  desc "Import 'EmailContact data' data ex: dataimport:emailcontacts[db/data/20180718/tblEmailListUpdate_2019-07-18.csv]"
  task :emailcontacts, [:file] =>  [:environment] do |t,args|

    puts "importing EmailContact data from: '#{args[:file]}'"
    next unless get_prompt("Loading:#{args[:file]} , enter 'Y' to continue.")
    emailcontacts = []
    count = 0
    if EmailContact.all.length > 0
      next unless get_prompt("EmailContact models already exist! EmailContact.all.length:#{EmailContact.all.length} , enter 'Y' to continue.")
      puts ""
      #### Prompt twice to ask if EmailContact should be deleted
      puts "Delete all EmailContact before load? (THIS IS VERY DESTRUCTIVE)"
      res = get_prompt("DESTRUCTIVE OPERATION: enter 'Y' to nuke EmailContact.")
      if res
        puts "are you really sure?"
        res = get_prompt("DESTRUCTIVE OPERATION: enter 'Y' to nuke EmailContact")
        if res
          EmailContact_total = EmailContact.all.length
          count = 0
          EmailContact.all.each do |ec|
            print ("\u001b[1000D Deleted:#{count}/#{EmailContact_total}")
            ec.delete
            count+=1
          end
          print("\n")
        end
      end
    end

    count = 0
    CSV.open(args[:file], 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: ',') do |csv|
      csv.each do |row|
        print ("\u001b[1000DParsing CSV: #{count}")

        if(row['BTN'] == "8581118" || row['BTN'] == "6693789")
          client = find_client_by_btn("000#{row['BTN']}")
        else
          client = find_client_by_btn(strip_btn_leading_zeros(row['BTN']))
        end

        if client.nil?
          debugger
          puts ("#{row['BTN']}")
        end

        emailcontacts << {
          email_address_pid: row['EmailAddressPID'],
          email_type: row['EmailType'],
          email: row['EAddress'],
          subscribed: row['Subscribe'],
          client: client,
          name: row['Description']
        }
        count+=1
      end
    end
    print("\n")

    count =0
    emailcontacts.each do |ec|
      print ("\u001b[1000DCreated: #{count}/#{emailcontacts.length}")
      EmailContact.create(ec)
      count+=1
    end

    print("\n")
    puts "#{count} EmailContact added!"
  end

end
