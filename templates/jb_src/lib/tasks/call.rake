namespace :call do
  desc "Associate calls to service, no args"
  task associateCallsToService: :environment do
    Call.associateCallsToService
  end


  desc "Additional fix for putting a rate center according to local NAE data. No args."
  task fixArea: :environment do
    Call.determineAndSetArea_allCalls
  end

  desc "Does all call data cleaning tasks. No args."
  task fixAll: :environment do
    Call.determineAndSetArea_allCalls
    Call.associateCallsToService
  end
end

