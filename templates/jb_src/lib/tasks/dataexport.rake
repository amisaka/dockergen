namespace :dataexport do
  desc "Import 'call' data ex: dataimport:calls[\"db/data/20180130/2017_SEP_Call-list-2.txt\"] "
  task :sitesdata, [:file] =>  [:environment] do |t,args|
    filename = args[:file]
    puts "#{filename}"
    CSV.open(filename, "wb") do |csv|
      s = Site.find(650)
      l = { site_id: s[:id],
            site_name: s[:name],
            site_client_id: s[:client_id],
            site_activated_at: s[:activated_at],
            site_activated: s[:activated],
            site_site_number: s[:site_number],
            client_id: (s.client[:id] if s.client),
            client_name: (s.client[:name] if s.client),
            client_btn_billingtelephonenumber: (s.client.btn[:billingtelephonenumber] if s.client),
            building_id: s.building[:id],
            building_address1: s.building[:address1],
            building_address2: s.building[:address2],
            building_city: s.building[:city],
            building_province: s.building[:province],
            building_country: s.building[:country],
            building_postalcode: s.building[:postalcode]
          }
      csv << l.keys
      Site.all.each do |s|
        begin
        l = { site_id: s[:id],
              site_name: s[:name],
              site_client_id: s[:client_id],
              site_activated_at: s[:activated_at],
              site_activated: s[:activated],
              site_site_number: s[:site_number],
              client_id: (s.client[:id] if s.client),
              client_name: (s.client[:name] if s.client),
              client_btn_billingtelephonenumber: (s.client.btn[:billingtelephonenumber] if s.client),
              building_id: (s.building[:id] if s.building),
              building_address1: (s.building[:address1] if s.building),
              building_address2: (s.building[:address2] if s.building),
              building_city: (s.building[:city] if s.building),
              building_province: (s.building[:province] if s.building),
              building_country: (s.building[:country] if s.building),
              building_postalcode: (s.building[:postalcode] if s.building)
            }

          csv << l.values
        rescue
          puts "#{s.id} #{s}"
        end
      end

    end
  end



end

namespace :dataexport do
  desc "export 'billable_services' data ex: dataexport:billable_services[\"billable_services.csv\",\"clientid\"] "
  task :billable_services, [:file, :clientid] =>  [:environment] do |t,args|
    filename = args[:file]
    clientid = args[:clientid]
    begin
      client = Client.find(clientid) or nil
    rescue ActiveRecord::RecordNotFound => e
      puts "#{e}"
      next
    end

    puts "#{filename} #{clientid}"
    billableservices = client.billable_services

    puts "#{billableservices.length}"
    CSV.open(filename, "wb") do |csv|

      csv << BillableService.attribute_names #column_names

      billableservices.each do |bs|
        puts "#{bs.attributes.values}"
        csv << bs.attributes.values
      end
    end
  end
end

namespace :dataexport do
  desc "export ALL 'charges (billable_services)' data ex: dataexport:billable_services[\"billable_services.csv\"] "
  task :export_billable_services, [:file, :clientid] =>  [:environment] do |t,args|
    filename = args[:file]
    # billableservices = BillableService.all
    billableservices = BillableService.all
    filename = "Charges_#{DateTime.now.strftime "%Y-%m-%d_%H%M"}.csv" if filename.nil?
    data=[]
    count=0
    CSV.open(filename, "wb") do |csv|
      csv << billableservices[0].to_export_hash.keys # column names
      t1 = Time.now
      billableservices.each do |bs|
        print ("\u001b[1000DWriting: #{count}/#{billableservices.length} #{'%.2f' % ((Time.now - t1) / 60)} minutes.")
        csv << bs.to_export_hash.values
        count+=1
      end
      puts ""
      puts "process took: #{(Time.now - t1)/60} minutes"
      puts "Wrote to: #{filename}"
    end
  end
end
