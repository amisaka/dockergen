namespace :report do
    desc "Report on active calls on terminated services, args: report:callsOnTerminatuedService[$filePath,[skipCallToService=*]]"
    task :callsOnTerminatedService, [:dirOutputPath, :skipCallToService] =>  [:environment] do |t, args|

        filePath = reportProcessArgs(args, 'callsOnTerminatedService')

        ::Call.callsOnTerminatedService(reportFilePath: filePath)

        puts "Report in #{filePath}"
    end

    desc "Calls per client"
    task :callsPerClient, [] => [:environment] do |t, args|
        callCountHash = {}
        callIntlCountHash = {}
        Call.connection.exec_query("select client_id, count(ca.id) c from calls ca, btns b, services s where ca.service_id = s.id and s.btn_id = b.id group by client_id order by c desc").rows.each {|row|
            callCountHash[row[0]] = row[1]
        }

        Call.connection.exec_query("select client_id, count(ca.id) c from calls ca, btns b, services s where ca.service_id = s.id and s.btn_id = b.id and ca.province = 'XN' group by client_id order by c desc").rows.each {|row|
            callIntlCountHash[row[0]] = row[1]
        }


        strFmt = "%-60s\t%10s\t%18s\t%18s"
        puts strFmt % ["Client", "Call count", "Intl call count", "Last invoice ID"]
        callCountHash.each {|client_id, call_count|
            c = Client.find(client_id)
            puts strFmt % [c.to_s, call_count, (callIntlCountHash[client_id] or 0), c.invoices.last.try(:id)]
        }
    end


    desc "Call detail report"
    task :callDetailReport, [:dirOutputPath, :skipCallToService] =>  [:environment] do |t, args|
        filePath = reportProcessArgs(args, 'callDetailReport')

		raise "Need CLIENTID=" if ENV['CLIENTID'].blank?

		clientId = ENV['CLIENTID'].to_i
		c = Client.find(clientId)

		Call.callDetailReport(clientObj: c, reportFilePath: filePath)

        puts "Report in #{filePath}"
    end




    desc "Sites per client"
    task :sitesPerClient, [:limit] => [:environment] do |t, args|
        sql = %Q[SELECT s.client_id, st.name as Statement_Type, count(s.id) c from sites s, statement_type_clients stc, statement_types st where s.client_id = stc.client_id and stc.statement_type_id = st.id and (st.name not ilike '%print%') and (st.name not ilike '%package') group by s.client_id, st.name order by c desc]
        sql += " limit #{args[:limit]}" if not args[:limit].nil?
        rows = Site.connection.exec_query(sql)

        strFmt = "%-60s\t%6s\t%-30s"
        puts strFmt % ["Name", "Count", "Statement Type"]
        rows.each {|row|
            c = Client.find(row["client_id"])
            puts strFmt % [c.to_s, row["c"], row["statement_type"]]
        }


    end


    desc "Billable services (Charges) per client with optional [:limit] in square brackets."
    task :bsPerClient, [:limit] => [:environment] do |t, args|
        q = BillableService.group(:client).order('count_id desc')
        q = q.limit(args[:limit]) if not args[:limit].nil?
        q = q.count(:id).map{|c,v| [c.to_s, v]}.to_h

        strFmt = "%-60s\t%20s"
        puts strFmt % ["Client", "BS count (all states)"]
        q.each {|c, count|
            puts strFmt % [c, count]
        }
    end

    desc "Payments per client, optional [limit], {begin=_date_ end=_date_}"
    task :paymentPerClient, [:limit] => [:environment] do |t, args|

        if ENV['begin'].present? and ENV['end'].present?
            start = Date.parse(ENV['begin'])
            finish = Date.parse(ENV['end'])

            hashResult = {}
            clients = Client.invoiceable


            clients.each_with_index{|c, i|
                Montbeau::Helpers::printProgress("Counting payments per client", i, clients.count,  true)
                payments = Invoice::paymentsForPeriod(c, start, finish)
                hashResult[c.to_s] = payments.count
            }
            hashResult = hashResult.sort_by{|k,v| v}.reverse
            if not args[:limit].nil?
                limit = args[:limit].to_i
                hashResult = hashResult[0..limit]
            end
            hashResult = hashResult.to_h

            q = hashResult

        else
            q = Payment.group(:client).order('count_id desc')
            q = q.where('date > ?', Date.today - 1.month)
            q = q.limit(args[:limit]) if not args[:limit].nil?
            q = q.count(:id).map{|c,v| [c.to_s, v]}.to_h
        end


        strFmt = "%-60s\t%20s"
        puts strFmt % ["Client", "Payment"]
        q.each {|c, count|
            puts strFmt % [c, count]
        }
    end

    desc "Report on calls not associated to a service, args: report:unassociatedCalls[$filePath,[skipCallToService=*]]"
    task :unassociatedCalls, [:dirOutputPath, :skipCallToService] =>  [:environment] do |t, args|

        filePath = reportProcessArgs(args, 'unassociatedCalls')

        ::Call.unassociatedCalls(reportFilePath: filePath)

        puts "Report in #{filePath}"
    end

    desc "Prints one column of Call dids not matched"
    task :serviceIdsNotFound, [:dirOutputPath, :skipCallToService] =>  [:environment] do |t, args|

        filePath = reportProcessArgs(args, 'serviceIdsNotFound')

        ::Call.serviceIdUnableToFind(reportFilePath: filePath)

        puts "Report in #{filePath}"

    end

    def reportProcessArgs(args, baseFileName)
        if args[:dirOutputPath].nil?
            $stderr.puts "Need a specified file: report:$task[$filePath,[skipCallToService=*]"
            exit 1
        end

        ::Call.associateCallsToService if args[:skipCallToService].nil?

        filePath = Montbeau::Helpers::timeStampFileName(args[:dirOutputPath], baseFileName, 'csv')

        return filePath

    end

end

