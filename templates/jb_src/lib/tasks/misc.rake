def get_prompt(question)
  STDOUT.print "#{question}>"

  begin
    prompt = Timeout::timeout(5) { STDIN.gets.chomp }
    if prompt == 'Y'
      return true
    else
      return false
    end
  rescue Timeout::Error
    puts "\nTimeout exceeded..... doing nothing!"
    return false
  end
end

namespace :misc do
  desc "set 'Pid' of LineOfBusiness to 'id' if 'Pid' == nil"
  task setlineofbusinesspid: :environment do
    puts "csv data has a 'PID' value, which in the past has relied on the actual model id which will break in many situations."
    puts "Added a new field 'Pid' to LineOfBusiness this task copies 'id' to the 'Pid' field."
    puts "Will only update if 'Pid' == nil"
    puts ""
    next unless get_prompt("continue?")
    updated = 0
    LineOfBusiness.all.each do |lob|
      if not lob.Pid
        lob.Pid = lob.id
        puts "LineOfBusiness(#{lob.id}) Pid set."
        lob.save
        updated+=1
      end
    end
    puts "#{updated}/#{LineOfBusiness.all.length} updated."
  end
end
