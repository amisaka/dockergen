namespace :jdev do
  desc "creates a admin account for dev, [email=] [password=] [name=]"
  task create_dev_admin: :environment do
    email = (ENV['email'] or 'admin@example.com')
    password = (ENV['password'] or 'test111')
    fullname = (ENV['name'] or 'Admin Login')
    #email = 'admin@example.com'
    #password = 'test111'
    #fullname = 'Admin Login'

    admin = (User.find_by_email(email) || User.new(:admin => true,
                                                                :fullname => fullname,
                                                                :confirmed_at => Time.now,
                                                                :email => email) )
    admin.password = password
    admin.save! and puts "#{admin.to_s} created or found. Password re/set"
  end
end
