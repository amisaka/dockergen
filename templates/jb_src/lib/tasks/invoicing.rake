namespace :invoicing do
  desc "generates invoices (optional CLIENTID=$clientid, BTN=$phoneNumber, DAYS_TH=, since=, from=, help=, SITEID=)"
  task generate: :environment do


    # Eventually I'd like to replace the usage of ENV[] with OptionParser
	# http://cobwwweb.com/4-ways-to-pass-arguments-to-a-rake-task#method-4-ruby-optionparser
    if ENV['help'].present?
        std.puts %Q[
            All optional:
            CLIENTID= or BTN=   to generate invocie for one client
            SITEID=             generate invoice for one site
            DAYS_TH=            skip clients that have invoices less than X days old
            from= and until=    Specify date range for payments. None or both must be specified.  until= is inclusive.
            help=               This help
        ]
        exit 0
    end

    clientId = determineClientId()[0] if (ENV['CLIENTID'].present? or ENV['BTN'].present?)

    # If SITEID is specified, wrap it in an array
    site     = [Site.find(ENV['SITEID'])] if ENV['SITEID'].present?

    daysTh      = ENV['DAYS_TH']    if ENV['DAYS_TH'].present?
    fromDate    = ENV['from']   if ENV['from'].present?
    untilDate   = ENV['until']  if ENV['until'].present?

    Montbeau::Invoicing.generate(clientId: clientId, daysTh: daysTh, paymentsSince: fromDate, paymentsShownUntil: untilDate, siteCollection: site)
  end


  desc "list invoices from specified CLIENTID=$clientid or BTN=$phoneNumber.  Either can be comma seperated.  LIMIT=[10]"
  task list: :environment do
    #include Rails.application.routes.url_helpers
    limit = 10
    limit = ENV['LIMIT'] if ENV['LIMIT']

    clientIds=determineClientId()
    format="%-60s\t%-30s\t%-20s\t%-5s"
	puts format % "Client\tCreated\tURL\tInvoice cycle".split("\t")
    clientIds.each {|clientId|
	    c = Client.find(clientId)
	    invoices = c.invoices.order(created_at: :desc).limit(limit)

	    invoices.each {|i|
	        puts format % "#{c.to_s}\t#{i.created_at}\tinvoices/#{i.id}\t#{c.invoice_template.invoice_cycle}".split("\t")
	    }
    }
  end

  desc "Clean execess invoices if DELETE=true, test run otherwise.  Can use CLIENTID= or BTN= for ONE client. Report only prints clients with excess invoices"
  task cleanUp: :environment do
    clientIds = determineClientId()
    doDelete = (ENV['DELETE'].present? and ENV['DELETE'] == 'true')
    testRun = ! doDelete
    Montbeau::Invoicing::cleanUp(testRun, clientIds[0])
  end


  task post: :environment do
    Invoice.postInvoices
  end



  def determineClientId(nullParamsAccepted = true)
    clientIds = []
    if ENV['BTN']
        ENV['BTN'].dup.split(',').each {|b|
            clientObj = Client.find_by_btn(b)
            raise "No client found for btn #{b}" if clientObj.nil?
            clientIds << clientObj.id
        }
    elsif ENV['CLIENTID']
        clientIds = ENV['CLIENTID'].split(',')
    elsif nullParamsAccepted == false
        raise "Must specify at least CLIENTID= or BTN="
    end

    return clientIds
  end

end
