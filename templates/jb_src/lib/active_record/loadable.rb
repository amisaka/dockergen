require 'csv'

module ActiveRecord
  module Loadable

  include ::Montbeau::Helpers
    ## Deprecatated.  Use update_from_csv
    def load_from_csv(model = self, filepath)
      CSV.open filepath, 'rb', headers: :first_row, encoding: 'UTF-8' do |csv|
        csv.each do |row|
          model.create! row.to_hash
        end
      end
    end

    # Ideally I'd like this to be in sge/lib/prompt.rb,
    # but I was having trouble loading it (circular dependency)
    # Returns false if the user does not enter Y or timeouts
	def get_prompt(question, timeoutSecs = 10)
	  require 'timeout'
	  STDOUT.print "#{question} (Y/n) > "

	  begin
	    prompt = Timeout::timeout(timeoutSecs) { STDIN.gets.chomp }
	    if prompt == 'Y'
	      return true
	    else
	      return false
	    end
	  rescue Timeout::Error
	    puts "\nTimeout exceeded..... doing nothing!"
	    return false
	  end
	end


    def log_not_imported(hashes)
        require 'pp'
        logLocation = "log/import_clients#{DateTime.now.strftime('-%Y%m%d-%H%M%S')}.log"
      File.open(logLocation, "ab") do |f|
        f.puts "\n\n"
        f.puts ["===", DateTime.now, '=' * 100].join(' ')
        hashes.each_with_index {|hash, i|
            Montbeau::Helpers::printProgress("Logging not imported... ", i, hashes.count)
            f.puts "Could not import #{hash.pretty_inspect}"
        }
        f.puts "\n\n"
      end

      return logLocation
    end



    ########################################################################
    # Detect what seperator is most likely used in a csv file
    ########################################################################
    def self.detectSeperator(filePath)
        seperators = [';', ':', "\t", ',', '|']
        firstLine = File.open(filePath, &:readline)

        seperators.max_by{ |s|
            firstLine.split(s).count
        }
    end

    def checkMapping(mapping)
        cloneWithoutCommentKeys(mapping).each {|attribute, csvHandler|
            # Is the attribute in the table column keys  or is in the assocations
            if (not self.columns_hash.keys.include?(attribute.to_s)) and self.reflect_on_association(attribute).nil?
                raise "#{self} does not respond to #{attribute}"
            end
        }
    end


    ########################################################################
    # Update data in models according to csv file
    # @param [String] filePath The path of the file
    # @param [Hash] mapping Mapping of model attribute (in the form of a symbol) => csv header (string)
    #               If you need to feed it an object, use
    #                    :modelAttribute => {
    #                       :csvHeader  => "$csvHeaderName",
    #                       :code       => Proc.new { |csvValue| Client.find(csvValue) }]
    #                     }
    #               If you need to feed it either two values, give it
    #                   :modelAttribute => ["csvHeader1", "$csvHeader2"]
    #               If you need to feed it a constant, give it
    #                   :modelAttribute => {:literal => false}
    #                   :modelAttribute => {:literal => 3}
    #                   :modelAttribute => {:literal => "a string"}
    #               The method will take care of plucking the csvValue in the right spot.
    #               The value between || needs to be somewhere in your code block
    # @param [Hash] primaryKeyColumn A hash with {csvColumn => :modelAttribute}
    #               used to match row to object for updating
    #               nil : defaults to using self.IMPORT_PRIMARY_KEY
    #               false : Don't update, just create (not implemented)
    # @param [Hash] options Give the follow options
    # @opts [boolean] :dontCreate Report object as error if can't be found
    # @opts [boolean] :truncate trunate Truncate the table before. Not implemented.
    ########################################################################
    def update_from_csv(filePath, mapping = nil, primaryKeyColumn = nil, options = {})

        if mapping.nil? and self::IMPORT_MAPPING.nil?
            raise "No mapping defined: both mapping and #{self.name}.IMPORT_MAPPING constant are nil"
        end

        mapping = self::IMPORT_MAPPING if mapping.nil?

        if not mapping.is_a?({}.class)
            raise "Somehow I did not end up with a hash for mapping.  mapping was a #{mapping.class}: #{mapping}"
        end

        beforeCount = self.count
        if (defined?(self::IMPORT_START_EMPTY) and self::IMPORT_START_EMPTY == true and beforeCount > 0)
            raise "#{self}.IMPORT_START_EMPTY is true but there are still rows in the table"
        end

        # Add a mapping to :imported_at if nil. This forces timestamping the import for better data import management
        if mapping[:imported_at].nil?
            mapping = mapping.merge({:imported_at => {
                exec: Proc.new {DateTime.now()}
            }})
        end

        checkMapping(mapping)

        seperator = options[:seperator]
        seperator = Loadable::detectSeperator(filePath) if seperator.nil?

        notLoaded   = []
        dataInHashes = []
        CSV.open(filePath, 'rb', headers: :first_row, encoding: 'UTF-8', col_sep: seperator) do |csv|
            totalLines = `wc -l "#{filePath}"`.strip.split(' ')[0].to_i - 1
            next unless get_prompt("Load #{filePath} (#{totalLines} lines)?")

            beginImportTS = DateTime.now

            count = 0
            csv.rewind
            csv.each do |row|
                count += 1
                pctError = ((notLoaded.count*100.to_f)/totalLines).round
                Montbeau::Helpers::printProgress("Reading #{filePath} (#{pctError}% errors)", count, totalLines)
                Montbeau::Helpers::checkWaitingProcsLoad()
                begin
                    entry = processMappingAndRow(row, mapping)
                    dataInHashes << entry
                rescue => e
                    row[:errorMsg] = e.message
                    notLoaded << row
                    if notLoaded.count < 4
                        $stderr.puts "Could not read\n#{row}"
                        byebug
                    end
                end
            end
            puts "\n"

            total = dataInHashes.length
            count = 0

            #Create a hash of stats
            desiredStats = ["Objects created", "Objects updated"]
            stats = Hash[desiredStats.collect {|stat| [stat, 0]}]


            dataInHashes.each do |dataEntry|
                count += 1
                pctError = ((notLoaded.count*100.to_f)/totalLines).round
                Montbeau::Helpers::printProgress("Updating / creating data (#{pctError}% errors)", count, dataInHashes.count)
                Montbeau::Helpers::checkWaitingProcsLoad()
                object = nil

                begin

                    object = nil

                    if (options[:dontUpdate].nil? or options[:dontUpdate] != true) and (not defined?(self::IMPORT_DONT_UPDATE) or self::IMPORT_DONT_UPDATE != true)
	                    primaryKeyColumn = self::IMPORT_PRIMARY_KEY if primaryKeyColumn.nil?

		                if primaryKeyColumn.is_a?({}.class)
		                    key = primaryKeyColumn.keys.first
	                    elsif primaryKeyColumn.is_a?(Symbol)
	                        key = primaryKeyColumn
	                    elsif not primaryKeyColumn.nil?
	                        raise "Don't know what to do with a #{primaryKeyColumn.class}"
		                end

	                    raise "Not sure I should have a nil or empty key.  Primary key column was #{primaryKeyColumn}" if (key.nil? or key.empty?)

	                    object = findObject(dataEntry, key)
                    end

	                if object.nil?
	                    # Object not found lets create
                        raise "Can't find #{dataEntry}, not creating" if options[:dontCreate]  == true
                        self.create!(cloneWithoutCommentKeys(dataEntry))
                        stats["Objects created"] += 1
	                else
                        # Object found, lets update
	                    object.update(cloneWithoutCommentKeys(dataEntry))
                        object.save!
                        stats["Objects updated"] += 1
	                end
                rescue => e
                    dataEntry[:errorMsg] = e.message
                    notLoaded << dataEntry
                    $stderr.puts "Could not update or create #{dataEntry}" if notLoaded.count < 4
                    $stderr.puts e.backtrace if notLoaded.count <= 2
                end
            end
            puts "\n"

            endImportTS = DateTime.now

            if (defined?(self::IMPORT_DELETE_NOT_UPDATED) and self::IMPORT_DELETE_NOT_UPDATED)
                toDelete = self.where(imported_at: nil)
                stats["Rows 'deleted' or *deleted*"] = toDelete.count
                toDelete.delete_all
            end


            puts "#{beforeCount} #{self.name} objects in database before creation"
            puts "#{self.count} #{self.name} objects in database after creation"
            puts "#{((endImportTS - beginImportTS) * 60*24).to_f.round(2)} minutes for import"


            stats["Rows not imported"] = notLoaded.count
            stats.each {|label, number| puts "#{label}: #{number} (#{((number*100.to_f)/totalLines).round(2)} %)"}

            if notLoaded.count > 0
                logLocation = log_not_imported(notLoaded)
                $stderr.puts "See #{Rails.root}/#{logLocation} for details on not loaded objects"
            end

        end

    end

    protected

    def cloneWithoutCommentKeys(hash)
        copy = hash.clone

        copy.each { |key, value|
            if key.is_a?(String) and key.start_with?('#')
                copy.delete(key)
            end
        }
        return copy
    end

    def findObject(dataEntry, key)
        raise "No way to find primary key #{key} in #{dataEntry}" if not dataEntry.keys.include?(key)
        value = dataEntry[key]
        raise "Null primary key with key #{key} and data #{dataEntry}" if value.nil?
	    object = self.find_by("#{key.to_s} = ?", value)
    end

    ########################################################################
    # See documentation for update_from_csv for sturcture of args
    ########################################################################
    def processMappingAndRow(row, mapping)
        returnHash = {}
        mapping.each { |classAttribute, csvHandler|
            value = nil

            case csvHandler.class.name
            when "String"
                #byebug if classAttribute == :supplier
                raise "Row does not have entry #{csvHandler}:\n#{row.headers}\n#{row}\n" if not row.has_key?(csvHandler)


                # Here, we just want the value of the cell from the column
                # that has the value in csvHandler as a column
                value = row[csvHandler]

                # TODO:
                # Detect if it's an assocation, and if it is, use
                # reflection to get the class
                # See https://stackoverflow.com/questions/3234991/what-is-the-class-of-an-association-based-on-the-foreign-key-attribute-only
                if not self.reflect_on_association(classAttribute).nil? and (not value.nil?) and (not value.empty?)
                     associationClassName = self.reflect_on_association(classAttribute).class_name
                     assocClass = Object.const_get(associationClassName)
                     assocImportKey = assocClass::IMPORT_PRIMARY_KEY
                     valueObj = assocClass.find_by(assocImportKey => value)

                     if valueObj.nil?
                        raise "No object found for rails assocation for attribute #{classAttribute} using #{assocClass}.find_by(#{assocImportKey} => #{value})"
                    else
                        value = valueObj
                    end
                end

                columnData = self.columns_hash[classAttribute.to_s]
                if (not columnData.nil? )
                    case columnData.sql_type_metadata.type
                        when :boolean
                            value = ([true, "1", "TRUE", "YES", "Yes", "yes"].include?(value.strip))
                        when :datetime
                            value = convert_to_date(value)
                        when :decimal
                            value = BigDecimal.new(value) if not value.nil?
                        else
                            nil # value keeps it value
                    end
                end



            when "Array"
                # This should probably changed to a hash, where you
                # can feed it the keys :columns and :default

                # We are picking the first non false value of the array
                value = row.to_h.select {|k,v|
                    #Keep the elements of the row specified in csvHandler
                    csvHandler.include?(k)
                 }

                # select the ones that return true, and pick the first value
                value = value.select {|k,v| v}.values.first

                if value.nil?
                    value = csvHandler.last
                end

            when "Hash"
                if csvHandler.keys.first == :literal
                    value = csvHandler.values.first

                elsif csvHandler.keys.include?(:code)
                    value = csvHandler[:code].call(row[csvHandler[:csvHeader]])
                elsif csvHandler.keys.include?(:rowHandler)
                    value = csvHandler[:rowHandler].call(row)
                elsif csvHandler.keys.include?(:exec)
                    value = csvHandler[:exec].call()
                else
                    raise "Don't know what to do with the hash #{csvHandler.to_s}"
                end
            else
                raise "Don't know what to do with a csvHandler of type #{csvHandler.class.name}:\n#{csvHandler.to_s}"
            end


            returnHash[classAttribute]  = value
        }

        returnHash
    end


  end
end
