require 'csv'

module ActiveRecord
  module Dumpable
    def dump_to_csv(model = self, filepath)
      CSV.open filepath, 'wb', write_headers: true, headers: model.column_names, encoding: 'UTF-8', force_quotes: true do |csv|
        model.unscoped.all.each do |m|
          csv << m.attributes.values
        end
      end
    end
  end
end
