require 'active_support/concern'

# this module prevents active record object from being deleted or destroyed by
# overriding the delete and destroy methods
# deleted objects are removed from the default scope, but are accessible using
# the `deleted` scope
module ActiveRecord
  module Undeletable
    extend ActiveSupport::Concern

    included do
      default_scope { persisted }

      scope :deleted,   -> {  unscoped.where deleted: true  }
      scope :persisted, -> {  unscoped.where deleted: false }
    end

    class_methods do
      # ...
    end

    # overrides destroy to prevent real deletions
    def destroy
      # destroy specifically calls callbacks and doesn't touch readonly records
      _raise_readonly_record_error if readonly?
      fake_delete
      run_callbacks :destroy
      self # return self instead of a frozen instance
    end

    def undestroy
      _undelete
      self
    end

    #
    def delete
      # delete doesn't care about callbacks or readonly records
      fake_delete
      self # return self instead of a frozen instance
    end

    def undelete
      _undelete
      self
    end

    # check super in case the record is really deleted
    def destroyed?
      super || deleted?
    end

    private

      def fake_delete
        update(options_for_fake_delete)
      end

      def _undelete
        update(options_for_undelete)
      end

    protected

      def options_for_undelete
        { deleted: false, deleted_at: nil }
      end

      def options_for_fake_delete
        { deleted: true, deleted_at: Time.now }
      end

  end
end
