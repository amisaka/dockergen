  def dump_stats(stats)
    if ENV['quiet'].blank?
      puts "  #{stats[:line]} lines processed"
      puts "  #{stats[:created]} new centers added"
      puts "  #{stats[:updated]} centers updated"

      stats.each { | key, value |
        puts "  #{key} = #{value}"
      }
    end
  end

