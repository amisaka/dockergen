#!/usr/bin/env bash

START=$(date +%s.%N)

#exit when command fails (use || true when a command can fail)
set -o errexit

#exit when your script tries to use undeclared variables
set -o nounset

# in scripts to catch mysqldump fails
set -o pipefail

# Resolve first directory of script
set -x
PRG="$BASH_SOURCE"
progname=`basename "$BASH_SOURCE"`


while [ -h "$PRG" ] ; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`"/$link"
    fi
done


# Set magic variables for current file & dir
__dir=$(dirname "$PRG")
if [[ "$__dir" == '.' ]]; then __dir=$PWD; fi
__root="$(cd "$(dirname "${__dir}")" && pwd)"           # Dir of the dir of the script
set +x
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"       # Full path of the script
__base="$(basename ${__file})"                          # Name of the script
ts=`date +'%Y%m%d-%H%M%S'`
ds=`date +'%Y%m%d'`

#Set the config file
configFile="$HOME/.binJlam/templateConfig"

#=== BEGIN Unique instance ============================================
#Ensure only one copy is running
pidfile=$HOME/.${__base}.pid
if [ -f ${pidfile} ]; then
   #verify if the process is actually still running under this pid
   oldpid=`cat ${pidfile}`
   result=`ps -ef | grep ${oldpid} | grep ${__base} || true`

   if [ -n "${result}" ]; then
     echo "Script already running! Exiting"
     exit 255
   fi
fi

#grab pid of this process and update the pid file with it
pid=`ps -ef | grep ${__base} | grep -v 'vi ' | head -n1 |  awk ' {print $2;} '`
echo ${pid} > ${pidfile}

# Create trap for lock file in case it fails
trap "rm -f $pidfile" INT QUIT TERM EXIT
#=== END Unique instance ============================================


#Capture everything to log
mkdir -p ~/log
log=~/log/$__base-${ts}.log
#exec >  >(tee -a $log)
#exec 2> >(tee -a $log >&2)
#touch $log
#chmod 600 $log


#Check that the config file exists
#if [[ ! -f "$configFile" ]] ; then
#        echo "I need a file at $configFile with ..."
#        exit 1
#fi

export DISPLAY=:0

echo Begin `date`  .....

echo; echo; echo;

### BEGIN SCRIPT ###############################################################

firstArg=${1:-''}
perlArg=''; rakeArg=''

if [[ "$firstArg" == "short" ]]; then
    perlArg=''
    rakeArg=' nameType=short '
elif [[ "$firstArg" == "long" ]]; then
    perlArg=' -l8 LERG8.DAT -useL8 '
    rakeArg=' nameType=long '
else
    echo "First argument MUST be either the string 'short' or 'long'"
    exit 1
fi


#(a.k.a set -x) to trace what gets executed

createdFiles=''

formerDir=`pwd`

beaumontRoot=$__root

echo beaumontRoot is $beaumontRoot

scriptOutputsDir=$beaumontRoot/doc/scriptOutputs/$ds
mkdir -p $scriptOutputsDir

readmePath=$scriptOutputsDir/README.txt
if [[ -f $readmePath ]]; then rm -vf $readmePath; fi

echo "These files were created on `date`" >> $readmePath

echo; echo; echo;
ticketNumber=${2:-''}
if [[ -z "$ticketNumber" ]]; then
    if [[ -f /tmp/.$__base.lastTicketNumber ]]; then
        lastTicketNumber=`cat /tmp/.$__base.lastTicketNumber`
        echo The last ticket number was $lastTicketNumber
    fi
    read -p "Would you like to specify a related ticket number?  [${lastTicketNumber:-''}]" ticketNumber
    if [[ -z "$ticketNumber" ]]; then
        ticketNumber=$lastTicketNumber
    fi
    echo $ticketNumber > /tmp/.$__base.lastTicketNumber
fi
echo >> $readmePath
echo "The related ticket number is $ticketNumber" >>  $readmePath
echo >> $readmePath
echo; echo; echo;

echo Checking if your LERG files are in /tmp
allFound=1
for lergNum in 6 7 8; do
    find /tmp -name "LERG${lergNum}.DAT" -mtime -30 || allFound=0
done

if [[ ! "$allFound" -eq "0" ]]; then
    echo Extracting your LERG files
    cd $beaumontRoot/doc/LERGfiles
    ./extractToTmp.bash
fi

cd /tmp



echo; echo; echo;
echo Creating your canada only LERG files

lergNum=6; spaces=77
grep -E "^.{$spaces}(PQ|AB|BC|YK|MB|NB|NF|NL|NS|NT|NU|VU|ON|PE|QC|SK|YT|LB|QB)" LERG${lergNum}.DAT | sort  -k 1.78 > /tmp/LERG${lergNum}.canadaOnly.DAT
lergNum=8; spaces=32
grep -E "^.{$spaces}(PQ|AB|BC|YK|MB|NB|NF|NL|NS|NT|NU|VU|ON|PE|QC|SK|YT|LB|QB)" LERG${lergNum}.DAT | sort  -k 1.78 > /tmp/LERG${lergNum}.canadaOnly.DAT



echo; echo; echo;
echo Building the NNACL file

cd $beaumontRoot/build-NNACL-scipts/
if [ -f /tmp/NNACL.DAT ]; then
    rm -i /tmp/NNACL.DAT
fi

perl build-NNACL-modified.pl -sDir /tmp -tDir /tmp -l6 LERG6.DAT -l7 LERG7.DAT $perlArg


echo; echo; echo;
echo Adding timestamp to filename

mv -v /tmp/NNACL.DAT /tmp/NNACL-$ds.DAT


echo; echo; echo;
read -p "Create compressed archived in the output dir $scriptOutputsDir? " yn
gzip -9vc /tmp/NNACL-$ds.DAT > $scriptOutputsDir/NNACL-$ds.DAT.gz
ls -lh $scriptOutputsDir/NNACL-$ds.DAT.gz


echo; echo; echo;
read -p "Update LERG6? y/n > " yn
if [ "$yn" == "y" ]; then
    cd $beaumontRoot
    set -x
    bundle exec rake novavision:updateLerg6 /tmp/LERG6.canadaOnly.DAT
    set +x
fi

echo; echo; echo;
read -p "Update LERG8? y/n > " yn
if [ "$yn" == "y" ]; then
    cd $beaumontRoot
    set -x
    bundle exec rake novavision:updateLerg8 /tmp/LERG8.canadaOnly.DAT
    set +x
fi


echo "Matching rate centers..... "
bundle exec rake novavision:matchRateCenters


echo; echo; echo;
read -p "Write LCA file as well? " yn
if [ "$yn" == "y" ]; then
    lcaConfigDir=$beaumontRoot/doc/lcaConfig
    cd $lcaConfigDir
    configFile=`ls -1 *.conf | tail -n 1`
    set -x
    rake novavision:writeLCAfile config=$lcaConfigDir/$configFile NNACL=/tmp/NNACL-$ds.DAT $rakeArg > $scriptOutputsDir/lca-$ds.xml
    set +x

    echo; echo; echo;
    cd $scriptOutputsDir/..
    echo Line number sanity check
    for file in `find -name 'lca*.xml'`; do
        date=`echo $file | grep -oP '\d{8}'`;
        echo `echo $date | cut -d ' ' -f 1` $file ;
    done | sort -n | cut -f 2 -d ' ' | xargs wc -l
fi



read -p "Generate NPA NXX list for Thinktel QC and Thinktel National? " yn
if [ "$yn" == "y" ]; then
    cd $lcaConfigDir
    rake novavision:listNPANXX CARRIER=Thinktel SEP=' '             > $scriptOutputsDir/NPANXX-ThinktelNat-$ds.txt
    rake novavision:listNPANXX CARRIER=Thinktel SEP=' ' PROVINCE=QC > $scriptOutputsDir/NPANXX-ThinktelQc-$ds.txt
fi


echo; echo; echo;

if [[ -f ~/.$__base.lastComment ]]; then
    echo Last comment was:
    cat ~/.$__base.lastComment
    echo
    read -p "Use last comment for the readme file? [y/n]" yn

    if [[ "$yn" == 'y' ]]; then
        cat     ~/.$__base.lastComment >> $readmePath
    else
        rm     ~/.$__base.lastComment
    fi
fi


if [[ ! -f ~/.$__base.lastComment ]]; then
    echo "Care to add commentary for the readme file? (ticket number, etc.)? End with Crtl-D . "
    echo >> $readmePath
    cat   > ~/.$__base.lastComment
    cat     ~/.$__base.lastComment >> $readmePath
fi


echo; echo; echo;
echo The followiong files have been created
ls -lh /tmp/NNACL-$ds.DAT  $scriptOutputsDir

echo; echo; echo;
echo To check if your NNACL files have changed, decompress them in /tmp/ and run
echo 'spaces=98; diff /tmp/NNACL-20160* | grep -E "^.{$spaces}(PQ|AB|BC|YK|MB|NB|NF|NL|NS|NT|NU|VU|ON|PE|QC|SK|YT|LB|QB)"'



cd $formerDir


### END SCIPT ##################################################################

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo; echo; echo;
echo Done.  `date` - $DIFF seconds

#=== BEGIN Unique instance ============================================
if [ -f ${pidfile} ]; then
    rm ${pidfile}
fi
#=== END Unique instance ============================================
