#!/usr/bin/env bash

# From jlam@credil.org
# This script loads data for LCA from backup SQL filea in beaumont/sql
# It also uses scripts from https://github.com/cyclingzealot/bin
# It is meant to be run on rousseau
# Normally I would not add such an environment dependant script 
# to a repo.  
# However, it documents well what I would do to generate an LCA file
# and I'm not sure how to make it less enviornment dependant


START=$(date +%s.%N)

#exit when command fails (use || true when a command can fail)
set -o errexit

#exit when your script tries to use undeclared variables
set -o nounset

#(a.k.a set -x) to trace what gets executed
#set -o xtrace

# in scripts to catch mysqldump fails 
set -o pipefail

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"   # Dir of the script
__root="$(cd "$(dirname "${__dir}")" && pwd)"           # Dir of the dir of the script
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"       # Full path of the script
__base="$(basename ${__file})"                          # Name of the script
ts=`date +'%Y%m%d-%H%M%S'`

#Set the config file
configFile="$HOME/.binJlam/templateConfig"

#Ensure only one copy is running
pidfile=$HOME/.${__base}.pid
if [ -f ${pidfile} ]; then
   #verify if the process is actually still running under this pid
   oldpid=`cat ${pidfile}`
   result=`ps -ef | grep ${oldpid} | grep ${__base} || true`  

   if [ -n "${result}" ]; then
     echo "Script already running! Exiting"
     exit 255
   fi
fi

#grab pid of this process and update the pid file with it
pid=`ps -ef | grep ${__base} | grep -v 'vi ' | head -n1 |  awk ' {print $2;} '`
echo ${pid} > ${pidfile}

# Create trap for lock file in case it fails
trap "rm -vf $pidfile" INT QUIT TERM EXIT


#Capture everything to log
mkdir -p ~/log
log=~/log/$__base-${ts}.log
exec >  >(tee -a $log)
exec 2> >(tee -a $log >&2)
touch $log
chmod 600 $log


#Check that the config file exists
#if [[ ! -f "$configFile" ]] ; then
#        echo "I need a file at $configFile with ..."
#        exit 1
#fi

export DISPLAY=:0

echo Begin `date`  .....

echo; echo; echo;

### BEGIN SCRIPT ###############################################################

cd /home/jlam/beaumont/sql

echo "Drop, re-create and populate LCA related tables? (y/*)"
read response
if [ "$response" == 'y' ] ; then
    headline.bash "Chanigng owner to beaumont_development"
    changeOwnerPostgres.bash clientportal beaumont_development '-h /home/mcr/clientportaltest/run'

    for sqlFile in   local_exchanges.20151014.sql  north_american_exchanges.sql local_exchanges_north_american_exchanges.sql  lerg8s.sql   exchanges_manual_matches.sql levenshtein.sql; do 
        bn=`basename $sqlFile`
        headline="Doing $bn"
        headline.bash "$headline"

        set -x
        psql -v ON_ERROR_STOP=1 -U clientportal -h /home/mcr/clientportaltest/run clientportal -f $sqlFile
        set +x

        echo; echo;
    done
fi

headline.bash "Chanigng owner to beaumont_development"
changeOwnerPostgres.bash clientportal beaumont_development '-h /home/mcr/clientportaltest/run'

cd ~/beaumont/
echo; echo; echo
headline.bash "Running matchRateCenters task"
set -x 
bundle exec rake --trace novavision:matchRateCenters
set +x


headline.bash "Writting LCA file"
target=/tmp/lca.$ts.xml
echo File written to $target

bundle exec rake --trace novavision:writeLCAfile | tee $target

echo LCA write done.  See $target . 

### END SCIPT ##################################################################

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo; echo; echo;
echo Done.  `date` - $DIFF seconds

if [ -f ${pidfile} ]; then
    rm ${pidfile}
fi
