#!/bin/bash

#exit when command fails (use || true when a command can fail)
set -o errexit

#exit when your script tries to use undeclared variables
set -o nounset


# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"   # Dir of the script
__root="$(cd "$(dirname "${__dir}")" && pwd)"           # Dir of the dir of the script
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"       # Full path of the script
__base="$(basename ${__file})"                          # Name of the script
ts=`date +'%Y%m%d-%H%M%S'`

set -x
cd $__dir/..


read -p "Clear /tmp of LERG files? y/* > " CONDITION;

if [ "$CONDITION" == "y" ]; then
    rm -r /tmp/LERG*
fi


cd doc/LERGfiles/

./extractToTmp.bash

grep -E '^.{77}(PQ|AB|BC|YK|MB|NB|NF|NL|NS|NT|NU|VU|ON|PE|QC|SK|YT|LB|QB)' /tmp/LERG6.DAT | sort  -k 1.78 >/tmp/LERG6.canadaOnly.DAT

ls -lh /tmp/LERG6.canadaOnly.DAT


cd -

