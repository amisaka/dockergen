#!/usr/bin/env bash

START=$(date +%s.%N)



#exit when command fails (use || true when a command can fail)
set -o errexit

#exit when your script tries to use undeclared variables
set -o nounset

#(a.k.a set -x) to trace what gets executed
#set -o xtrace

# in scripts to catch mysqldump fails
set -o pipefail

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"   # Dir of the script
__root="$(cd "$(dirname "${__dir}")" && pwd)"           # Dir of the dir of the script
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"       # Full path of the script
__base="$(basename ${__file})"                          # Name of the script
ts=`date +'%Y%m%d-%H%M%S'`

#Set the config file
configFile="$HOME/.binJlam/templateConfig"


#Check that the config file exists
#if [[ ! -f "$configFile" ]] ; then
#        echo "I need a file at $configFile with ..."
#        exit 1
#fi

export DISPLAY=:0

echo Begin `date`  .....

echo; echo; echo;

### BEGIN SCRIPT ###############################################################


pid=`ps -ef | grep ${__base} | grep -v 'vi ' | head -n1 |  awk ' {print $2;} '`

patternsFile=/tmp/$pid.scratch
touch $patternsFile
chmod 600 $patternsFile

trap "rm -f $patternsFile" INT QUIT TERM EXIT

function warning() {
    echo About to run sed for files `ls *.yml`
    for i in `seq 5 -1 0`; do printf "$i... " ; sleep 1; done
}


grep -hvi cdr *.yml | grep -vi recordid | grep -vi remotecallid | grep -vi relatedcallid | grep -vi transfer_relatedcallid | grep -vi networkcallid | grep -vi accesscallid | grep -vi master_callid | grep -v localcallid |  grep -ho "\(([2-9][0-9]\{2\})\|[2-9][0-9]\{2\}\)[ -]\?[0-9]\{3\}[ -]\?[0-9]\{4\}" | sort | uniq | grep -v 555555 | grep -v '^$'  > $patternsFile

warning

< $patternsFile xargs -I {} sh -c 'echo Doing {} ; fourDigit=`shuf -i 1000-9999 -n 1`; sed -i -e "/recordid\|remotecallid\|relatedcallid\|transfer_relatedcallid\|networkcallid\|accesscallid\|master_callid\|localcallid\|time\|date/!  s/{}/555555$fourDigit/g" *.yml'

echo

function randomThreeWords() {
    for i in `seq 1 3`; do sed `perl -e "print int rand(99999)"`"q;d" /usr/share/dict/words ; done | tr '\n' ' '
    return 0
}
export -f randomThreeWords

#rtw=`randomThreeWords`
#echo "rtw is $rtw"

grep -e serviceprovider -e otherpartyname *.yml | sort | uniq | cut -d: -f 2- | sort | uniq | cut -c 2- | grep  -v -e '^[[:space:]]*$' > $patternsFile

warning

< $patternsFile xargs -I {} bash -c 'rtw=`randomThreeWords`; sed -i "s/{}/$rtw/g" *.yml'

rm -v $patternsFile

### END SCIPT ##################################################################

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo; echo; echo;
echo Done.  `date` - $DIFF seconds
