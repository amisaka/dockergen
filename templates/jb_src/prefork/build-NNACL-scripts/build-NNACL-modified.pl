#!/usr/bin/perl -w

# #############################################################################
#  Copyright c 2001, 2005 Broadsoft, Inc.                                    #
#  This software and its documentation are protected by copyright law and    #
#  international treaties. Unauthorized reproduction or distribution of this #
#  software, or any part thereof, may result in severe civil and criminal    #
#  penalties, and will be prosecuted to the maximum extent possible          #
#  under the law.                                                            #
#  BroadWorks is a trademark of BroadSoft, Inc. Gaithersburg, MD             #
#                                                                            #
# Title: build-NNACL.pl                                                      #
# Author: BroadSoft, Inc. June 2003                                          #
#                                                                            #
# Description:                                                               #
#        This command should be used to derive the content of the Telcordia  #
#        NNACL from the LERG files. This command uses the LERG6 and LERG7    #
#        files for input.                                                    #
#                                                                            #
# Usage:                                                                     #
#        build-NNACL.pl -help                                                #
#                                                                            #
# History of changes:                                                        #
#        April 2004 - Added support for the status changes                   #
#                                                                            #
##############################################################################

system("logger -i -plocal2.notice -tBROADWORKS \"\`id\` COMMAND=$0 @ARGV\"");


# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: validate_usage
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////// #
sub validate_usage
{
  my $valid_op=1;

  # Verify if any attributes were passed in
  while($arg = shift @ARGV)
  {
    if ($arg eq "-help" or $arg eq "-h")
    {
      print_usage();
      exit(1);
    }
    if ($arg eq "-sDir")
    {
      $arg = shift @ARGV;
      if (! defined $arg)
      {
        print_usage("No source directory specified");
        exit(2);
      }
      $sourceDirectory = $arg;
      next;
    }
    if ($arg eq "-tDir")
    {
      $arg = shift @ARGV;
      if (! defined $arg)
      {
        print_usage("No target directory specified");
        exit(3);
      }
      $destinationDirectory = $arg;
      next;
    }
    if ($arg eq "-l6")
    {
      $arg = shift @ARGV;
      if (! defined $arg)
      {
        print_usage("No LERG6 data file name specified");
        exit(4);
      }
      $l6FileName = $arg;
      next;
    }
    if ($arg eq "-l7")
    {
      $arg = shift @ARGV;
      if (! defined $arg)
      {
        print_usage("No LERG7 data file name specified");
        exit(5);
      }
      $l7FileName = $arg;
      next;
    }
    if ($arg eq "-l8")
    {
      $arg = shift @ARGV;
      if (! defined $arg)
      {
        print_usage("No LERG8 data file name specified");
        exit(9);
      }
      $l8FileName = $arg;
      next;
    }
    if ($arg eq "-tFile")
    {
      $arg = shift @ARGV;
      if (! defined $arg)
      {
        print_usage("No target filename specified");
        exit(6);
      }
      $targetFileName = $arg;
      next;
    }
    if ($arg eq "-date")
    {
      $arg = shift @ARGV;
      if (! defined $arg)
      {
        print_usage("No date value specified");
        exit(7);
      }
      $date = $arg;
      if (length "$date" ne 6)
      {
        print_usage("Date format is MMDDYY");
        exit(8);
      }
      next;
    }
    if ($arg eq "-noprompt")
    {
      $noprompt = 1;
      next;
    }
    if ($arg eq "-useL8")
    {
      $useL8 = 1;
      next;
    }
    
    print_usage("Unknown argument \"$arg\"");
    exit(10);
  }
}

# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: print_usage [<errorMsg>]
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////// #
sub print_usage
{
    my $errorMsg = shift;
    
    if (defined $errorMsg)
    {
      print("\nERROR: $errorMsg\n");
    }
    print("
build-NNACL.pl [<-help> | [<-sDir> <sourceDirectory> | 
                <-oDir> <outputDirectory> | <-l6> <lerg6FileName> | 
                <-l7> <lerg7FileName> | -useL8 | [<-l8> <lerg8FileName>] | 
                <-date> <applicationDate> | <-noprompt>]]

Description: This command should be used to derive the content of the Telcordia
             NNACL from the LERG files. This command uses the LERG6 and LERG7
             files for input.
             The LERG6 file contains the information related to all NPA-NXX in 
             North America. The LERG7 and LERG8 contains the switching equipment 
             information including V&H coordinates.
             Using LERG7, Rate Center abbreviated name is put in the resulting NNACL.
             Using LERG8, Rate Center full name is put in the resulting NNACL.
             When using LERG8, LERG7 is still required. 
             
             This command uses the application date as the trigger to 
             manage modifications. Note that the status of the lines in the 
             LERG6 files are handled as follow:
               - E(Effective): Date is ignored, the line is added.
               - D(Deleted):   Date comparaison. If the line date is older than the 
                               -date, the row to be deleted is ignored.
               - M(Modified):  Date comparaison. If the line with the effective date
                               is kept.          
             All status related decisions are output to STDOUT.

List of options:
  -help                     : Returns this help page.
  -sDir <sourceDirectory>   : Specify the source directory for the LERG6 and 
                              LERG7 files.
                              By default, this command uses the current 
                              directory.
  -tDir <outputDirectory>   : Specify the target directory for the resulting 
                              NNACL file.
                              By default, this command uses the current 
                              directory.
  -tFile <ouputFileName>    : Specify the target file name for the resulting
                              NNACL file.
                              By default, this command uses the NNACL.DAT 
                              as the default filename.
  -useL8                    : Indicate that lerg8 should be used to specify 
                              the rate center name.
  -l6 <lerg6FileName>       : The name of the lerg6 data file.
                              By default, this command uses LERG6.DAT
  -l7 <lerg7FileName>       : The name of the lerg7 data file.
                              By default, this command uses LERG7.DAT
  -l8 <lerg8FileName>       : The name of the lerg8 data file can be optionaly added.
                              By default, this command uses LERG8.DAT
  -date <MMDDYY>            : The date on which the NNACL file is to be loaded.
                              The date is used to include or exclude line based
                              on the status indicator from LERG6.
                              By default, this command uses the current date.
  -noprompt                 : No y/n prompt, always assume default answer.
\n");
}

# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: print_error_msg <errorMsg>
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////// #
sub print_error_msg
{
  my $errorMsg = shift;
  my $rc = shift;
    
  ($rc=1000) if (! defined $rc);

  if (defined $errorMsg)
  {
    print("\n*** ERROR $rc: $errorMsg\n");
  }
  else
  {
    print("\n*** ERROR $rc: Unknown message\n");
  }
  exit($rc);
}


# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: printUsage [<errorMsg>]
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////// #
sub validate_data
{
  if (! -e "$sourceDirectory")
  {
    print_error_msg("Source directory does not exist: \"$sourceDirectory\"", 20);
  }
  if (! -e "$destinationDirectory")
  {
    print_error_msg("Source directory does not exist: \"$destinationDirectory\"", 21);
  }

  my $localFileName = $sourceDirectory."/".$l6FileName;
  if (! -e "$localFileName")
  {
    print_error_msg("LERG6 file not found: \"$localFileName\"", 22);
  }
  $localFileName = $sourceDirectory."/".$l7FileName;
  if (! -e "$localFileName")
  {
    print_error_msg("LERG7 file not found: \"$localFileName\"", 23);
  }
  
  $localFileName = $sourceDirectory."/".$targetFileName;
  if (-e "$localFileName")
  {
    print "... The target NNACL file already exists on this server \"$localFileName\"\n";
    if (! $noprompt)
    {
      my $rc = "";
      while ($rc eq "")
      {
        print "... Do you want to overwrite it? (y/n) [y] ";
        chomp($rc = <STDIN>);
        print "\n";
        if ($rc eq "y" or $rc eq "Y")
        {
          next;
        }
        elsif ($rc eq "n" or $rc eq "N")
        {
          print_error_msg("Stopping script", 24);
        }
        elsif ($rc eq "")
        {
          $rc = "y";
        }
        else
        {
          $rc = "";
        }
      }
    }
  }
  print "... Using the following destination file: \"$localFileName\"\n";
  
  if ($date ne "")
  {
    $date=~m/(.{2})(.{2})(.{2})/g;
    $valueDate = ((int $3+100) * 10000) + (int $1 * 100) + (int $2);
  }
  
  print "Using the following date comparating value: $valueDate\n";
}

# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: isInEffect
#
# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
sub isInEffect
{
  my $targetDate = shift;
  $targetDate=~m/(.{2})(.{2})(.{2})/g;
  my $lValue=((int $3+100) * 10000) + (int $1 * 100) + (int $2);
   
   # DEBUG TRACE
   # print "*** $targetDate $lValue $valueDate ***\n";
  
  return (int $lValue le int $valueDate);
}

# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: load_lerg7
#
# Fields position:
#   1- Lata(5)                2- Lata Name(20)            3- Status(1)
#   4- Effective Date(6)      5- Switch(11)               6- Equipement Type(5)
#   7- Administrative OCN(4)  8- Filer(1)                 9- Operating Company Number OCN(4)
#  10- Vertical Code(VC)(5)  11- Horizontal Code (HC)(5) 12- Intenational DDD (IDDD)(Y/N)(1)
#  13- Street(60)            14- City(30)                15- State/Province/Terr/Country(2)
#  16- Zip Code(9)           17- Point Code Flag(1)      18- Class 4/5 Switch (11)
#  19 & up :: Switch Office Functionality (SOF) Indicators
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////// #
sub load_lerg7
{
  my $localFileName = $sourceDirectory."/".$l7FileName;
  open(LERG7, "< $localFileName") or die "Could not open $localFileName for read\n";
  my $nbOfLines=0;
  while(<LERG7>)
  {
    $nbOfLines++;
    #   1     2      3     4     5      6     7     8     9     10    11    12    13     14     15    16    17    18
    m/(.{5})(.{20})(.{1})(.{6})(.{11})(.{5})(.{4})(.{1})(.{4})(.{5})(.{5})(.{1})(.{60})(.{30})(.{2})(.{9})(.{1})(.{11})/g;
    $switch     = $5;
    $SWITCH_INFO{$switch} = {
         lata => $1,
         lataName => $2,
         status => $3,
         effectiveDate => $4,
         equipmentType => $6,
         verticalCode => $10,
         horizontalCode => $11
    };
  }
  close(LERG7);
  print "... Total of lines processed = $nbOfLines\n";
}

# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: load_lerg8
#
# Fields position:
#   1- Lata(5)                   2- Lata Name(20)                   3- Status(1)
#   4- Effective Date(6)         5- State/Province/Terr/Country(2)  6- RC Name Abbrv(10)
#   7- RC Type(1)                8- RC Name Full(50)                9- Major Vertical Code(VC)(5)
#  10- Major Horizontal Code(5) 11- Minor Vertical Code(5)         12- Minor Horizontal Code(5)
#  13- NPA1 (Area code)(3)      14- NPA2(3)                        15- NPA3(3)
#  16- NPA4(3)                  17- NPA5(3)                        18- NPA6(3)
#  19- NPA7(3)                  20- NPA8(3)                        21- NPA9(3)
#  22- NPA10(3)                 23- filler(4)                      24- Split indicator [y/n](1)
#  25- Embedded overlay NPA1(3) 26- Embedded overlay NPA2(3)       27- Embedded overlay NPA3(3)
#  28- Embedded overlay NPA4(3) 26- filler(10)
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////// #
sub load_lerg8
{
  my $localFileName = $sourceDirectory."/".$l8FileName;
  open(LERG8, "< $localFileName") or die "Could not open $localFileName for read\n";
  my $nbOfLines=0;
  while(<LERG8>)
  {
    $nbOfLines++;
    #   1     2      3     4     5     6      7     8      9     10    11    12    13    14    15    16    17    18
    m/(.{5})(.{20})(.{1})(.{6})(.{2})(.{10})(.{1})(.{50})(.{5})(.{5})(.{5})(.{5})(.{3})(.{3})(.{3})(.{3})(.{3})(.{3})/g;
    $province		= $5;
    $abbrvRCName     = $6;
    $RC_INFO{$province . $abbrvRCName} = {
         rcLongName => $8
    };
#    print "$abbrvRCName\n";
  }
  close(LERG8);
  print "... Total of lines processed = $nbOfLines\n";
}

# //////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ #
#
# Usage: build_nnacl_from_lerg6
#
# LERG6 fields position:
#   1- Lata(5)                2- Lata Name(20)              3- Status(1)
#   4- Effective Date(6)      5- NPA(3)                     6- NXX(3)
#   7- Block ID (A, 0-9) [only use A](1)                    8- filler(4)
#   9- COC Type(3)           10- Special Service Code(4)   11- Diable Indicator (DIND)(Y/N)(1)
#  12- End Office(EO)(2)     13- Access Tandem (AC)(2)     14- Portable Indicator (Y/N)(1)
#  15- Administrative OCN(4) 16- filler(1)                 17- Operating Company Numnber OCN(4)
#  18- Locality Name(10)     19- Locality Country(2)       20- Locality State/Prob/Terr/Counry(2)
#  21- RC Name Abbrv(10)     22- RC Type(1)                23- Lines from #(0000)(4)
#  24- Lines to #(0000)(4)   25- Switch(11)                26- Switch homing arrangement (SHA)(2)
#  27 & up :: Misc stuff
#
# NNACL fields position:
#   1- NPA(3)                 2- NXX(3)                     3- Filler(7)
#   4- COC Type(3)            5- Special Service Code(4)    6- Switch(11)
#   7- Filler(3)              8- OperatingCompanyNumnber(4) 9- Lata(5)
#  10- RC Name(30)           11- Filler(23)                12- State/Prov/Country(2)
#  13- Portable Indicator(1) 12- Filler(1)
#
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////// #
sub build_nnacl_from_lerg6
{
  my $localFileName = $sourceDirectory."/".$l6FileName;
  my $localNNACLFilename = $destinationDirectory."/".$targetFileName;
  open(LERG6, "< $localFileName") or die "Could not open $localFileName for read\n";
  open(NNACL, "> $localNNACLFilename") or die "Could not open $localNNACLFilename for read\n";
  my $nbOfLines=0;
  my $preceedingLine="";
  my $preceedingNPANXX="";
  print "The following lines were ignored due to a status change:\n";
  my $numberOfExceptions=0;
  while(<LERG6>)
  {
    $nbOfLines++;
    #   1     2      3     4     5     6     7     8     9     10    11    12    13    14    15    16     17    18    19     20    21    22    23     24   25     26
    m/(.{5})(.{20})(.{1})(.{6})(.{3})(.{3})(.{1})(.{4})(.{3})(.{4})(.{1})(.{2})(.{2})(.{1})(.{4})(.{1})(.{4})(.{10})(.{2})(.{2})(.{10})(.{1})(.{4})(.{4})(.{11})(.{2})/g;
    $status=$3;
    $effectiveDay=$4;
    $npa = $5;
    $nxx = $6;
    $filler1 = ' 'x7;
    $cocType = $9;
    $scs = $10;
    $switch = $25;
    $filler2 = ' 'x3;
    $ocn = $17;
    $lata = $1;
    $rcStateProvCountry = $20;
    $rcName = $21;
    if (exists $RC_INFO{$rcStateProvCountry . $rcName})
    {
      $rcName = $RC_INFO{$rcStateProvCountry . $rcName}->{rcLongName};
    }
    if (exists $SWITCH_INFO{$switch})
    {
      $vCode = $SWITCH_INFO{$switch}->{verticalCode};
      $hCode = $SWITCH_INFO{$switch}->{horizontalCode};
    }
    else
    {
      $vCode = "00000";
      $hCode = "00000";
    }
    $filler3 = ' 'x13;
    $portableIndicator = $14;
    $filler4 = ' 'x1;
    while (length($rcName)<30)
    {
      $rcName = $rcName.' ';
    }
    $rcName = substr($rcName, 0, 30);
    $nnaclLine=$npa.$nxx.$filler1.$cocType.$scs.$switch.$filler2.$ocn.$lata.$rcName.$vCode.$hCode.$filler3.$rcStateProvCountry.$portableIndicator.$filler4;
    $blockid = $7;
    
    # The following handles the date changes....
    if ($preceedingLine ne "")
    {
      if ($status eq "M")
      {
        if (isInEffect($effectiveDay))
        {
          print NNACL "$nnaclLine\n";
        }
	else        
        {
          print NNACL "$preceedingLine\n";
          print "***Not MODIFIED --- Effective date $effectiveDay:\n    $nnaclLine\n";
          $numberOfExceptions++;
        }      
      }
      else
      {
        print NNACL "$preceedingLine\n";
      }
      $preceedingLine="";
    }
    if ($blockid eq "A")
    {
      if ($status eq "D" || !($nxx =~ /\d+/))
      {
      	# Don't do anything here... Just ignore the line...
      }
      elsif ($status eq "E")
      {
        print NNACL "$nnaclLine\n";
      }
      elsif ($status eq "M")
      {
      	# Don't do anything here...
      }
      else
      {
        $preceedingLine=$nnaclLine;
      }
      next;
    }
  }
  if ($preceedingLine ne "")
  {
    print NNACL "$preceedingLine\n";
  }
  close(LERG6);
  close(NNACL);
  print "   ***None\n" if ($numberOfExceptions eq 0);
  print "\n... Total of lines processed = $nbOfLines\n";
}

#####
##
## MAIN
##
#####

$sourceDirectory =".";
$destinationDirectory = ".";
$l6FileName = "LERG6.DAT";
$l7FileName = "LERG7.DAT";
$l8FileName = "LERG8.DAT";
$useL8 = 0;
$targetFileName = "NNACL.DAT";
$noprompt = 0;
$date = "";
($tmp, $tmp, $tmp, $mday, $mon, $year, $tmp, $tmp, $tmp) = localtime(time());
$mon = int $mon + 1;
$valueDate = (int $year * 10000) + (int $mon * 100) + (int $mday);

validate_usage();

print("************************************************************\n");
print("*           BroadWorks LERG converter command              *\n");
print("*                                                          *\n");
print("* Version: 1.0                                             *\n");
print("************************************************************\n");

print ("\nValidating data\n");
validate_data();

if ($useL8) {
  print ("\nLoading LERG8\n");
  load_lerg8();
}

print ("\nLoading LERG7\n");
load_lerg7();

print ("\nBuilding NNACL from LERG6\n");
build_nnacl_from_lerg6();

print ("\nDone\n");

exit(0);
