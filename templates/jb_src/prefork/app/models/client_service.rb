class ClientService < ApplicationRecord
  self.table_name = "portal_customerservice"
  self.primary_key = 'id'

  belongs_to :client,         :foreign_key => "customer_id"
  belongs_to :portal_service, :foreign_key => "service_id"
end
