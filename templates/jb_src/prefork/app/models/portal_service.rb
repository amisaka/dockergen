class PortalService < ApplicationRecord
  self.table_name = "portal_service"
  # self.primary_key = 'id'

  has_many :client_services, :class_name => 'ClientService', :foreign_key => "customer_id"

  def self.mycommunications
    @@mycommunications ||= PortalService.find_by_name('My communications')
  end

end
