require 'rails_helper'

describe ClientService do

  describe "relations" do
    it "should have a related client" do
      nvcomm = portal_customerservice(:nvcomm)
      expect( nvcomm.client ).to_not be_nil
    end

    it "should have a related service" do
      nvcomm = portal_customerservice(:nvcomm)
      expect( nvcomm.portal_service ).to_not be_nil
    end

  end

end
