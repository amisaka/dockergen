require 'rails_helper'

describe Service do
  if $PORTAL

    it "should have a service called MyCommunications" do
      comm1=PortalService.find_by_name('My communications')
      expect(comm1).not_to be_nil

      expect(comm1.client_services).not_to be_nil
    end
  end
end
