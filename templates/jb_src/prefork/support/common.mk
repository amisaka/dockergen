TOP ?= .
UP  = ..

PKG  ?= $(shell grep Package DEBIAN/control | cut -d " " -f2)
WEEK ?= $(shell date +%Y%V)
REV  :=$(shell date +%Y%m%d.t%H%M)
BASEVER?=4.0
VER  ?= ${BASEVER}.$(REV)
DEB  ?= $(PKG)_$(VER).deb
INCOMING ?= sg1@citadel.gatineau.credil.org:/home/sg1/incoming

