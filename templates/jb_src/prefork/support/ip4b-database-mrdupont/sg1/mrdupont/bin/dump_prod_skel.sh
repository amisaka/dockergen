#!/bin/sh

set -x

count=${1-01}
shift

PATH=/var/lib/gems/1.9.1/bin:$PATH export PATH
LC_ALL=en_CA.UTF-8
export LC_ALL

export RAILS_ENV=production

#schemas="callstats stats_periods"
sequences="callstats_id_sequence stats_periods_id_sequence"

pg_dump --clean --file=nvdb${count}_base.sql --format=plain \
   --column-inserts \
   --table=btns \
   --table=buildings \
   --table=cdrviewer_areacode \
   --table=cdrviewer_datasource \
   --table=users \
   --table=clients \
   --table=phones \
   --table=sites  \
   --table=callstats_id_seq \
   --table=stats_periods_id_seq \
   $*


#dir=/sg1/billing/current
#cd ~beaumont/beaumont/current
#bundle exec rake novavision:makeslavesqlinit

