#!/bin/sh

#schemas="callstats stats_periods"
sequences="callstats_id_sequence stats_periods_id_sequence"

HOSTNAME=`hostname`

pg_dump --format=custom -a --file=callstats_${HOSTNAME}.pgdump --format=plain \
   --table=callstats \
   --table=stats_periods \
	--username=sg1admin clientportal \
   $*

# pg_restore --format=custom -j 3 -h localhost --username=beaumont -W 

