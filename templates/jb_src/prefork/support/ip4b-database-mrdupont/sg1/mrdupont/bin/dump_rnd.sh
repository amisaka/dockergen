#!/bin/sh

set -x

PATH=/var/lib/gems/1.9.1/bin:$PATH export PATH
LC_ALL=en_CA.UTF-8
export LC_ALL
today=$(date +%Y%m%d)

export RAILS_ENV=production

pg_dump --clean --file=rnd_${today}.sql --format=plain \
   --column-inserts \
   --exclude-table=callstats \
   --exclude-table=cdrviewer_cdr \
   --exclude-table=cdrviewer_cdrall \
   --exclude-table=django_session \
   --exclude-table=callstats \
   --exclude-table=calls \
   $*


