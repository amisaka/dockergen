--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.lerg8s DROP CONSTRAINT lerg8s_pkey;
ALTER TABLE public.lerg8s ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.lerg8s_id_seq;
DROP TABLE public.lerg8s;
SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: lerg8s; Type: TABLE; Schema: public; Owner: beaumont_development
--

CREATE TABLE lerg8s (
    id integer NOT NULL,
    "shortName" character varying,
    "fullName" character varying,
    province character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE lerg8s OWNER TO beaumont_development;

--
-- Name: lerg8s_id_seq; Type: SEQUENCE; Schema: public; Owner: beaumont_development
--

CREATE SEQUENCE lerg8s_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE lerg8s_id_seq OWNER TO beaumont_development;

--
-- Name: lerg8s_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: beaumont_development
--

ALTER SEQUENCE lerg8s_id_seq OWNED BY lerg8s.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: beaumont_development
--

ALTER TABLE ONLY lerg8s ALTER COLUMN id SET DEFAULT nextval('lerg8s_id_seq'::regclass);


--
-- Data for Name: lerg8s; Type: TABLE DATA; Schema: public; Owner: beaumont_development
--

INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20083, 'ACTON VALE', 'ACTON VALE', 'QC', '2015-10-13 19:02:54.973448', '2015-10-13 19:02:54.973448');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20084, 'AGUANISH', 'AGUANISH', 'QC', '2015-10-13 19:02:54.987873', '2015-10-13 19:02:54.987873');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20085, 'AKULIVIK', 'AKULIVIK', 'QC', '2015-10-13 19:02:55.005253', '2015-10-13 19:02:55.005253');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20086, 'ALBANEL', 'ALBANEL', 'QC', '2015-10-13 19:02:55.03545', '2015-10-13 19:02:55.03545');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20087, 'ALMA', 'ALMA', 'QC', '2015-10-13 19:02:55.056972', '2015-10-13 19:02:55.056972');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20088, 'AMOS', 'AMOS', 'QC', '2015-10-13 19:02:55.067373', '2015-10-13 19:02:55.067373');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20089, 'AMQUI', 'AMQUI', 'QC', '2015-10-13 19:02:55.076978', '2015-10-13 19:02:55.076978');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20090, 'ANGLIERS', 'ANGLIERS', 'QC', '2015-10-13 19:02:55.0893', '2015-10-13 19:02:55.0893');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20091, 'ANSESTJEAN', 'L''ANSE-SAINT-JEAN', 'QC', '2015-10-13 19:02:55.101118', '2015-10-13 19:02:55.101118');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20092, 'ARMAGH', 'ARMAGH', 'QC', '2015-10-13 19:02:55.113087', '2015-10-13 19:02:55.113087');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20093, 'ARTHABASKA', 'ARTHABASKA', 'QC', '2015-10-13 19:02:55.125016', '2015-10-13 19:02:55.125016');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20094, 'ARUNDEL', 'ARUNDEL', 'QC', '2015-10-13 19:02:55.135317', '2015-10-13 19:02:55.135317');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20095, 'ASBESTOS', 'ASBESTOS', 'QC', '2015-10-13 19:02:55.144675', '2015-10-13 19:02:55.144675');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20096, 'ASTON JCT', 'ASTON-JONCTION', 'QC', '2015-10-13 19:02:55.15126', '2015-10-13 19:02:55.15126');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20097, 'AUPALUK', 'AUPALUK', 'QC', '2015-10-13 19:02:55.164782', '2015-10-13 19:02:55.164782');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20098, 'AYERSCLIFF', 'AYER''S CLIFF', 'QC', '2015-10-13 19:02:55.171373', '2015-10-13 19:02:55.171373');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20099, 'AYLMER', 'AYLMER', 'QC', '2015-10-13 19:02:55.179875', '2015-10-13 19:02:55.179875');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20100, 'BAIDFEBVRE', 'BAIE-DU-FEBVRE', 'QC', '2015-10-13 19:02:55.188563', '2015-10-13 19:02:55.188563');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20101, 'BAIECOMEAU', 'BAIE-COMEAU', 'QC', '2015-10-13 19:02:55.195044', '2015-10-13 19:02:55.195044');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20102, 'BAIESABLES', 'BAIE-DES-SABLES', 'QC', '2015-10-13 19:02:55.199042', '2015-10-13 19:02:55.199042');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20103, 'BAIESTPAUL', 'BAIE-SAINT-PAUL', 'QC', '2015-10-13 19:02:55.202858', '2015-10-13 19:02:55.202858');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20104, 'BAIETRINIT', 'BAIE-TRINITE', 'QC', '2015-10-13 19:02:55.208672', '2015-10-13 19:02:55.208672');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20105, 'BAJOHNBETZ', 'BAIE-JOHAN-BEETZ', 'QC', '2015-10-13 19:02:55.216729', '2015-10-13 19:02:55.216729');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20106, 'BARACHOIS', 'BARACHOIS', 'QC', '2015-10-13 19:02:55.224742', '2015-10-13 19:02:55.224742');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20107, 'BARRAUTE', 'BARRAUTE', 'QC', '2015-10-13 19:02:55.230714', '2015-10-13 19:02:55.230714');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20108, 'BATISCAN', 'BATISCAN', 'QC', '2015-10-13 19:02:55.236574', '2015-10-13 19:02:55.236574');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20109, 'BEARNTCMNG', 'BEARN (TEMISCAMING)', 'QC', '2015-10-13 19:02:55.245184', '2015-10-13 19:02:55.245184');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20110, 'BEAUCANTON', 'BEAUCANTON', 'QC', '2015-10-13 19:02:55.255589', '2015-10-13 19:02:55.255589');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20111, 'BEAUCEVL', 'BEAUCEVILLE', 'QC', '2015-10-13 19:02:55.263893', '2015-10-13 19:02:55.263893');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20112, 'BEAUHARNOS', 'BEAUHARNOIS', 'QC', '2015-10-13 19:02:55.272059', '2015-10-13 19:02:55.272059');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20113, 'BECANCOUR', 'BECANCOUR', 'QC', '2015-10-13 19:02:55.280522', '2015-10-13 19:02:55.280522');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20114, 'BEDFORD', 'BEDFORD', 'QC', '2015-10-13 19:02:55.288861', '2015-10-13 19:02:55.288861');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20115, 'BELLETERRE', 'BELLETERRE', 'QC', '2015-10-13 19:02:55.29497', '2015-10-13 19:02:55.29497');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20116, 'BELOEIL', 'BELOEIL', 'QC', '2015-10-13 19:02:55.299075', '2015-10-13 19:02:55.299075');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20117, 'BERGERONNS', 'BERGERONNES', 'QC', '2015-10-13 19:02:55.303019', '2015-10-13 19:02:55.303019');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20118, 'BERTHIERVL', 'BERTHIERVILLE', 'QC', '2015-10-13 19:02:55.312825', '2015-10-13 19:02:55.312825');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20119, 'BIC', 'BIC', 'QC', '2015-10-13 19:02:55.319153', '2015-10-13 19:02:55.319153');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20120, 'BIENCOURT', 'BIENCOURT', 'QC', '2015-10-13 19:02:55.323321', '2015-10-13 19:02:55.323321');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20121, 'BISHOPTON', 'BISHOPTON', 'QC', '2015-10-13 19:02:55.329626', '2015-10-13 19:02:55.329626');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20122, 'BLACK LAKE', 'BLACK LAKE', 'QC', '2015-10-13 19:02:55.364671', '2015-10-13 19:02:55.364671');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20123, 'BLANCSBLON', 'BLANC-SABLON', 'QC', '2015-10-13 19:02:55.38117', '2015-10-13 19:02:55.38117');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20124, 'BOISCHATEL', 'BOISCHATEL', 'QC', '2015-10-13 19:02:55.400604', '2015-10-13 19:02:55.400604');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20125, 'BONAVNTURE', 'BONAVENTURE', 'QC', '2015-10-13 19:02:55.41674', '2015-10-13 19:02:55.41674');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20126, 'BONNEEPRNC', 'BONNE-ESPERANCE', 'QC', '2015-10-13 19:02:55.427085', '2015-10-13 19:02:55.427085');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20127, 'BOUCHERVL', 'BOUCHERVILLE', 'QC', '2015-10-13 19:02:55.441534', '2015-10-13 19:02:55.441534');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20128, 'BOUCHETTE', 'BOUCHETTE', 'QC', '2015-10-13 19:02:55.451802', '2015-10-13 19:02:55.451802');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20129, 'BROMONT', 'BROMONT', 'QC', '2015-10-13 19:02:55.460376', '2015-10-13 19:02:55.460376');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20130, 'BROMPTONVL', 'BROMPTONVILLE', 'QC', '2015-10-13 19:02:55.469406', '2015-10-13 19:02:55.469406');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20131, 'BROWNSBURG', 'BROWNSBURG', 'QC', '2015-10-13 19:02:55.479565', '2015-10-13 19:02:55.479565');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20132, 'BSTCATHRIN', 'BAIE-SAINTE-CATHERINE', 'QC', '2015-10-13 19:02:55.488066', '2015-10-13 19:02:55.488066');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20133, 'BUCKINGHAM', 'BUCKINGHAM', 'QC', '2015-10-13 19:02:55.496437', '2015-10-13 19:02:55.496437');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20134, 'BURY', 'BURY', 'QC', '2015-10-13 19:02:55.504743', '2015-10-13 19:02:55.504743');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20135, 'CABANO', 'CABANO', 'QC', '2015-10-13 19:02:55.514935', '2015-10-13 19:02:55.514935');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20136, 'CADILLAC', 'CADILLAC', 'QC', '2015-10-13 19:02:55.519456', '2015-10-13 19:02:55.519456');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20137, 'CAMPBELSBY', 'CAMPBELL''S BAY', 'QC', '2015-10-13 19:02:55.525483', '2015-10-13 19:02:55.525483');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20138, 'CAP CHAT', 'CAP-CHAT', 'QC', '2015-10-13 19:02:55.535001', '2015-10-13 19:02:55.535001');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20139, 'CAPLAN', 'CAPLAN', 'QC', '2015-10-13 19:02:55.543988', '2015-10-13 19:02:55.543988');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20140, 'CAPROSIERS', 'CAP-DES-ROSIERS', 'QC', '2015-10-13 19:02:55.62425', '2015-10-13 19:02:55.62425');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20141, 'CARLETON', 'CARLETON', 'QC', '2015-10-13 19:02:55.640399', '2015-10-13 19:02:55.640399');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20142, 'CAUSAPSCAL', 'CAUSAPSCAL', 'QC', '2015-10-13 19:02:55.647004', '2015-10-13 19:02:55.647004');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20143, 'CHAMBLY', 'CHAMBLY', 'QC', '2015-10-13 19:02:55.653275', '2015-10-13 19:02:55.653275');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20144, 'CHAMBORD', 'CHAMBORD', 'QC', '2015-10-13 19:02:55.659251', '2015-10-13 19:02:55.659251');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20145, 'CHAMPLAIN', 'CHAMPLAIN', 'QC', '2015-10-13 19:02:55.667591', '2015-10-13 19:02:55.667591');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20146, 'CHANDLER', 'CHANDLER', 'QC', '2015-10-13 19:02:55.68067', '2015-10-13 19:02:55.68067');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20147, 'CHAPAIS', 'CHAPAIS', 'QC', '2015-10-13 19:02:55.686793', '2015-10-13 19:02:55.686793');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20148, 'CHAPEAU', 'CHAPEAU', 'QC', '2015-10-13 19:02:55.690778', '2015-10-13 19:02:55.690778');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20149, 'CHARETTE', 'CHARETTE', 'QC', '2015-10-13 19:02:55.694904', '2015-10-13 19:02:55.694904');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20150, 'CHARNY', 'CHARNY', 'QC', '2015-10-13 19:02:55.699107', '2015-10-13 19:02:55.699107');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20151, 'CHARTIERVL', 'CHARTIERVILLE', 'QC', '2015-10-13 19:02:55.703096', '2015-10-13 19:02:55.703096');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20152, 'CHATEAGUAY', 'CHATEAUGUAY', 'QC', '2015-10-13 19:02:55.711269', '2015-10-13 19:02:55.711269');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20153, 'CHATEARCHR', 'CHATEAU-RICHER', 'QC', '2015-10-13 19:02:55.715096', '2015-10-13 19:02:55.715096');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20154, 'CHELSEA', 'CHELSEA', 'QC', '2015-10-13 19:02:55.720813', '2015-10-13 19:02:55.720813');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20155, 'CHENEVILLE', 'CHENEVILLE', 'QC', '2015-10-13 19:02:55.728416', '2015-10-13 19:02:55.728416');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20156, 'CHESTERVL', 'CHESTERVILLE', 'QC', '2015-10-13 19:02:55.73639', '2015-10-13 19:02:55.73639');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20157, 'CHEVERY', 'CHEVERY', 'QC', '2015-10-13 19:02:55.745777', '2015-10-13 19:02:55.745777');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20158, 'CHIBOUGMAU', 'CHIBOUGAMAU', 'QC', '2015-10-13 19:02:55.771842', '2015-10-13 19:02:55.771842');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20159, 'CHICOUTIMI', 'CHICOUTIMI', 'QC', '2015-10-13 19:02:55.780554', '2015-10-13 19:02:55.780554');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20160, 'CHISASIBI', 'CHISASIBI', 'QC', '2015-10-13 19:02:55.795497', '2015-10-13 19:02:55.795497');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20161, 'CHOMEDEY', 'CHOMEDEY', 'QC', '2015-10-13 19:02:55.803516', '2015-10-13 19:02:55.803516');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20162, 'CHTAXOTRDS', 'CHUTE-AUX-OUTARDES', 'QC', '2015-10-13 19:02:55.811776', '2015-10-13 19:02:55.811776');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20163, 'CHUTEPASSS', 'CHUTE-DES-PASSES', 'QC', '2015-10-13 19:02:55.820634', '2015-10-13 19:02:55.820634');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20164, 'CLARENCEVL', 'CLARENCEVILLE', 'QC', '2015-10-13 19:02:55.831186', '2015-10-13 19:02:55.831186');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20165, 'CLARKECITY', 'CLARKE CITY', 'QC', '2015-10-13 19:02:55.840769', '2015-10-13 19:02:55.840769');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20166, 'CLERICY', 'CLERICY', 'QC', '2015-10-13 19:02:55.847162', '2015-10-13 19:02:55.847162');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20167, 'CLERMONT', 'CLERMONT (CHARLEVOIX)', 'QC', '2015-10-13 19:02:55.85104', '2015-10-13 19:02:55.85104');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20168, 'CLORIDORME', 'CLORIDORME', 'QC', '2015-10-13 19:02:55.856889', '2015-10-13 19:02:55.856889');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20169, 'CLOVA', 'CLOVA', 'QC', '2015-10-13 19:02:55.871648', '2015-10-13 19:02:55.871648');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20170, 'COATICOOK', 'COATICOOK', 'QC', '2015-10-13 19:02:55.879926', '2015-10-13 19:02:55.879926');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20171, 'COLOMBIER', 'COLOMBIER', 'QC', '2015-10-13 19:02:55.887954', '2015-10-13 19:02:55.887954');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20172, 'COMPTON', 'COMPTON', 'QC', '2015-10-13 19:02:55.896511', '2015-10-13 19:02:55.896511');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20173, 'CONTRECOER', 'CONTRECOEUR', 'QC', '2015-10-13 19:02:55.909542', '2015-10-13 19:02:55.909542');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20174, 'COOKSHIRE', 'COOKSHIRE', 'QC', '2015-10-13 19:02:55.917597', '2015-10-13 19:02:55.917597');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20175, 'COTEAU LDG', 'COTEAU LANDING', 'QC', '2015-10-13 19:02:55.931805', '2015-10-13 19:02:55.931805');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20176, 'COTEUDULAC', 'COTEAU-DU-LAC', 'QC', '2015-10-13 19:02:55.948975', '2015-10-13 19:02:55.948975');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20177, 'COURCELLES', 'COURCELLES', 'QC', '2015-10-13 19:02:55.961292', '2015-10-13 19:02:55.961292');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20178, 'COWANSVL', 'COWANSVILLE', 'QC', '2015-10-13 19:02:55.971967', '2015-10-13 19:02:55.971967');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20179, 'CPSTIGNACE', 'CAP-SAINT-IGNACE', 'QC', '2015-10-13 19:02:55.983361', '2015-10-13 19:02:55.983361');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20180, 'CPXMLLMDLN', 'CAP-AU-MEULES (ILES-DE-LA-MADELEINE)', 'QC', '2015-10-13 19:02:55.991737', '2015-10-13 19:02:55.991737');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20181, 'CRABTREE', 'CRABTREE', 'QC', '2015-10-13 19:02:56.00083', '2015-10-13 19:02:56.00083');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20182, 'DANVILLE', 'DANVILLE', 'QC', '2015-10-13 19:02:56.012999', '2015-10-13 19:02:56.012999');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20183, 'DAVELUYVL', 'DAVELUYVILLE', 'QC', '2015-10-13 19:02:56.032811', '2015-10-13 19:02:56.032811');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20184, 'DEAUVILLE', 'DEAUVILLE', 'QC', '2015-10-13 19:02:56.044921', '2015-10-13 19:02:56.044921');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20185, 'DELISLE', 'DELISLE', 'QC', '2015-10-13 19:02:56.064711', '2015-10-13 19:02:56.064711');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20186, 'DESBIENS', 'DESBIENS', 'QC', '2015-10-13 19:02:56.080721', '2015-10-13 19:02:56.080721');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20187, 'DESCHALLNS', 'DESCHAILLONS', 'QC', '2015-10-13 19:02:56.092014', '2015-10-13 19:02:56.092014');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20188, 'DISRAELI', 'DISRAELI', 'QC', '2015-10-13 19:02:56.105608', '2015-10-13 19:02:56.105608');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20189, 'DOLBEAU', 'DOLBEAU', 'QC', '2015-10-13 19:02:56.115699', '2015-10-13 19:02:56.115699');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20190, 'DONNACONA', 'DONNACONA', 'QC', '2015-10-13 19:02:56.123258', '2015-10-13 19:02:56.123258');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20191, 'DRUMMONDVL', 'DRUMMONDVILLE', 'QC', '2015-10-13 19:02:56.144135', '2015-10-13 19:02:56.144135');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20192, 'DUBUISSON', 'DUBUISSON', 'QC', '2015-10-13 19:02:56.15712', '2015-10-13 19:02:56.15712');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20193, 'DUNHAM', 'DUNHAM', 'QC', '2015-10-13 19:02:56.177657', '2015-10-13 19:02:56.177657');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20194, 'DUPARQUET', 'DUPARQUET', 'QC', '2015-10-13 19:02:56.193066', '2015-10-13 19:02:56.193066');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20195, 'DUPUY', 'DUPUY', 'QC', '2015-10-13 19:02:56.204689', '2015-10-13 19:02:56.204689');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20196, 'E HEREFORD', 'EAST HEREFORD', 'QC', '2015-10-13 19:02:56.22111', '2015-10-13 19:02:56.22111');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20197, 'EAST ANGUS', 'EAST ANGUS', 'QC', '2015-10-13 19:02:56.231113', '2015-10-13 19:02:56.231113');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20198, 'EASTMAIN', 'EASTMAIN', 'QC', '2015-10-13 19:02:56.253168', '2015-10-13 19:02:56.253168');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20199, 'EASTMAIN 1', 'EASTMAIN 1', 'QC', '2015-10-13 19:02:56.269405', '2015-10-13 19:02:56.269405');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20200, 'EASTMAN', 'EASTMAN', 'QC', '2015-10-13 19:02:56.287659', '2015-10-13 19:02:56.287659');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20201, 'EBROUGHTON', 'EAST BROUGHTON', 'QC', '2015-10-13 19:02:56.295519', '2015-10-13 19:02:56.295519');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20202, 'ESCOUMINS', 'LES ESCOUMINS', 'QC', '2015-10-13 19:02:56.30333', '2015-10-13 19:02:56.30333');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20203, 'ESPRIT ST', 'ESPRIT-SAINT', 'QC', '2015-10-13 19:02:56.311417', '2015-10-13 19:02:56.311417');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20204, 'FABRE', 'FABRE', 'QC', '2015-10-13 19:02:56.328609', '2015-10-13 19:02:56.328609');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20205, 'FARNHAM', 'FARNHAM', 'QC', '2015-10-13 19:02:56.339544', '2015-10-13 19:02:56.339544');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20206, 'FERLAND', 'FERLAND', 'QC', '2015-10-13 19:02:56.353364', '2015-10-13 19:02:56.353364');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20207, 'FERMENEUVE', 'FERME-NEUVE', 'QC', '2015-10-13 19:02:56.375868', '2015-10-13 19:02:56.375868');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20208, 'FERMONT', 'FERMONT', 'QC', '2015-10-13 19:02:56.396609', '2015-10-13 19:02:56.396609');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20209, 'FORESTVL', 'FORESTVILLE', 'QC', '2015-10-13 19:02:56.407827', '2015-10-13 19:02:56.407827');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20210, 'FORTIERVL', 'FORTIERVILLE', 'QC', '2015-10-13 19:02:56.452855', '2015-10-13 19:02:56.452855');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20211, 'FRAMPTON', 'FRAMPTON', 'QC', '2015-10-13 19:02:56.492614', '2015-10-13 19:02:56.492614');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20212, 'FRANKLICTR', 'FRANKLIN CENTRE', 'QC', '2015-10-13 19:02:56.509588', '2015-10-13 19:02:56.509588');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20213, 'FRELIGHSBG', 'FRELIGHSBURG', 'QC', '2015-10-13 19:02:56.525084', '2015-10-13 19:02:56.525084');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20214, 'FRONTENAC', 'FRONTENAC', 'QC', '2015-10-13 19:02:56.70886', '2015-10-13 19:02:56.70886');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20215, 'FTCOULONGE', 'FORT-COULONGE', 'QC', '2015-10-13 19:02:56.736405', '2015-10-13 19:02:56.736405');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20216, 'FUGEREVL', 'FUGEREVILLE', 'QC', '2015-10-13 19:02:56.756436', '2015-10-13 19:02:56.756436');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20217, 'GARTHBY', 'GARTHBY', 'QC', '2015-10-13 19:02:56.767826', '2015-10-13 19:02:56.767826');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20218, 'GASPE', 'GASPE', 'QC', '2015-10-13 19:02:56.78934', '2015-10-13 19:02:56.78934');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20219, 'GATINEAU', 'GATINEAU', 'QC', '2015-10-13 19:02:56.807924', '2015-10-13 19:02:56.807924');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20220, 'GENTILLY', 'GENTILLY', 'QC', '2015-10-13 19:02:56.844749', '2015-10-13 19:02:56.844749');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20221, 'GIRARDVL', 'GIRARDVILLE', 'QC', '2015-10-13 19:02:56.921974', '2015-10-13 19:02:56.921974');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20222, 'GODBOUT', 'GODBOUT', 'QC', '2015-10-13 19:02:56.937504', '2015-10-13 19:02:56.937504');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20223, 'GRACEFIELD', 'GRACEFIELD', 'QC', '2015-10-13 19:02:56.952928', '2015-10-13 19:02:56.952928');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20224, 'GRANBY', 'GRANBY', 'QC', '2015-10-13 19:02:56.96314', '2015-10-13 19:02:56.96314');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20225, 'GRAND MERE', 'GRAND-MERE', 'QC', '2015-10-13 19:02:56.973317', '2015-10-13 19:02:56.973317');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20226, 'GRANDE RIV', 'GRANDE-RIVIERE', 'QC', '2015-10-13 19:02:56.989253', '2015-10-13 19:02:56.989253');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20227, 'GRANDEVALL', 'GRANDE-VALLEE', 'QC', '2015-10-13 19:02:56.999135', '2015-10-13 19:02:56.999135');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20228, 'GRANDRMOUS', 'GRAND-REMOUS', 'QC', '2015-10-13 19:02:57.008899', '2015-10-13 19:02:57.008899');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20229, 'GRDRLSMDLN', 'GRANDE-ENTREE (ILES-DE-LA-MADELEINE)', 'QC', '2015-10-13 19:02:57.021299', '2015-10-13 19:02:57.021299');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20230, 'GRENVILLE', 'GRENVILLE', 'QC', '2015-10-13 19:02:57.033478', '2015-10-13 19:02:57.033478');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20231, 'GUIGUES', 'SAINT-BRUNO-DE-GUIGUES', 'QC', '2015-10-13 19:02:57.043243', '2015-10-13 19:02:57.043243');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20232, 'HAM NORD', 'HAM-NORD', 'QC', '2015-10-13 19:02:57.052177', '2015-10-13 19:02:57.052177');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20233, 'HARITNHRBR', 'HARRINGTON HARBOUR', 'QC', '2015-10-13 19:02:57.064157', '2015-10-13 19:02:57.064157');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20234, 'HAUTERIVE', 'HAUTERIVE', 'QC', '2015-10-13 19:02:57.07148', '2015-10-13 19:02:57.07148');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20235, 'HEBERTVL', 'HEBERTVILLE', 'QC', '2015-10-13 19:02:57.080728', '2015-10-13 19:02:57.080728');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20236, 'HEBETVLSTA', 'HEBERTVILLE STATION', 'QC', '2015-10-13 19:02:57.092632', '2015-10-13 19:02:57.092632');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20237, 'HEMMINGFD', 'HEMMINGFORD', 'QC', '2015-10-13 19:02:57.099252', '2015-10-13 19:02:57.099252');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20238, 'HENRYVILLE', 'HENRYVILLE', 'QC', '2015-10-13 19:02:57.108536', '2015-10-13 19:02:57.108536');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20239, 'HOWICK', 'HOWICK', 'QC', '2015-10-13 19:02:57.115413', '2015-10-13 19:02:57.115413');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20240, 'HUDSON', 'HUDSON', 'QC', '2015-10-13 19:02:57.124157', '2015-10-13 19:02:57.124157');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20241, 'HUNTINGDON', 'HUNTINGDON', 'QC', '2015-10-13 19:02:57.131356', '2015-10-13 19:02:57.131356');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20242, 'HVRBLSMDLN', 'HAVRE-AUBERT (ILES-DE-LA-MADELEINE)', 'QC', '2015-10-13 19:02:57.140891', '2015-10-13 19:02:57.140891');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20243, 'HVRSTPIRRE', 'HAVRE-SAINT-PIERRE', 'QC', '2015-10-13 19:02:57.152389', '2015-10-13 19:02:57.152389');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20244, 'HVXMSLMDLN', 'HAVRE-AUX-MAISONS (ILES-DE-LA-MADELEINE)', 'QC', '2015-10-13 19:02:57.159158', '2015-10-13 19:02:57.159158');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20245, 'ILAXCOUDRS', 'ILE-AUX-COUDRES', 'QC', '2015-10-13 19:02:57.168168', '2015-10-13 19:02:57.168168');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20246, 'ILE PERROT', 'ILE-PERROT', 'QC', '2015-10-13 19:02:57.175186', '2015-10-13 19:02:57.175186');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20247, 'INUKJUAK', 'INUKJUAK', 'QC', '2015-10-13 19:02:57.17938', '2015-10-13 19:02:57.17938');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20248, 'INVERNESS', 'INVERNESS', 'QC', '2015-10-13 19:02:57.193003', '2015-10-13 19:02:57.193003');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20249, 'IVUJIVIK', 'IVUJIVIK', 'QC', '2015-10-13 19:02:57.203951', '2015-10-13 19:02:57.203951');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20250, 'JOLIETTE', 'JOLIETTE', 'QC', '2015-10-13 19:02:57.212821', '2015-10-13 19:02:57.212821');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20251, 'JONQUIERE', 'JONQUIERE', 'QC', '2015-10-13 19:02:57.219285', '2015-10-13 19:02:57.219285');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20252, 'JOUTEL', 'JOUTEL', 'QC', '2015-10-13 19:02:57.227753', '2015-10-13 19:02:57.227753');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20253, 'KANGIQSJUQ', 'KANGIQSUJUAQ', 'QC', '2015-10-13 19:02:57.236611', '2015-10-13 19:02:57.236611');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20254, 'KANGIQSLJQ', 'KANGIQSUALUJJUAQ', 'QC', '2015-10-13 19:02:57.243002', '2015-10-13 19:02:57.243002');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20255, 'KANGIRSUK', 'KANGIRSUK', 'QC', '2015-10-13 19:02:57.249415', '2015-10-13 19:02:57.249415');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20256, 'KAZABAZUA', 'KAZABAZUA', 'QC', '2015-10-13 19:02:57.260785', '2015-10-13 19:02:57.260785');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20257, 'KINGSEYFLS', 'KINGSEY FALLS', 'QC', '2015-10-13 19:02:57.26728', '2015-10-13 19:02:57.26728');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20258, 'KNOWLTON', 'KNOWLTON', 'QC', '2015-10-13 19:02:57.27352', '2015-10-13 19:02:57.27352');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20259, 'KUJUARAPIK', 'KUUJJUARAAPIK', 'QC', '2015-10-13 19:02:57.284548', '2015-10-13 19:02:57.284548');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20260, 'KUUJJUAQ', 'KUUJJUAQ', 'QC', '2015-10-13 19:02:57.297514', '2015-10-13 19:02:57.297514');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20261, 'L AVENIR', 'L''AVENIR', 'QC', '2015-10-13 19:02:57.308817', '2015-10-13 19:02:57.308817');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20262, 'L ISLET', 'L''ISLET', 'QC', '2015-10-13 19:02:57.315128', '2015-10-13 19:02:57.315128');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20263, 'L''ILEVERTE', 'L''ILE-VERTE', 'QC', '2015-10-13 19:02:57.319133', '2015-10-13 19:02:57.319133');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20264, 'LA CORNE', 'LA CORNE', 'QC', '2015-10-13 19:02:57.325129', '2015-10-13 19:02:57.325129');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20265, 'LA DORE', 'LA DORE', 'QC', '2015-10-13 19:02:57.333614', '2015-10-13 19:02:57.333614');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20266, 'LA MALBAIE', 'LA MALBAIE', 'QC', '2015-10-13 19:02:57.344285', '2015-10-13 19:02:57.344285');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20267, 'LA MARTRE', 'LA MARTRE', 'QC', '2015-10-13 19:02:57.351047', '2015-10-13 19:02:57.351047');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20268, 'LA MINERVE', 'LA MINERVE', 'QC', '2015-10-13 19:02:57.357395', '2015-10-13 19:02:57.357395');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20269, 'LA PATRIE', 'LA PATRIE', 'QC', '2015-10-13 19:02:57.367839', '2015-10-13 19:02:57.367839');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20270, 'LA REINE', 'LA REINE', 'QC', '2015-10-13 19:02:57.376175', '2015-10-13 19:02:57.376175');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20271, 'LA ROMAINE', 'LA ROMAINE', 'QC', '2015-10-13 19:02:57.384832', '2015-10-13 19:02:57.384832');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20272, 'LA SARRE', 'LA SARRE', 'QC', '2015-10-13 19:02:57.393194', '2015-10-13 19:02:57.393194');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20273, 'LA TUQUE', 'LA TUQUE', 'QC', '2015-10-13 19:02:57.401286', '2015-10-13 19:02:57.401286');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20274, 'LABELLE', 'LABELLE', 'QC', '2015-10-13 19:02:57.409396', '2015-10-13 19:02:57.409396');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20275, 'LAC DROLET', 'LAC-DROLET', 'QC', '2015-10-13 19:02:57.419843', '2015-10-13 19:02:57.419843');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20276, 'LACBOUCHTT', 'LAC-BOUCHETTE', 'QC', '2015-10-13 19:02:57.428658', '2015-10-13 19:02:57.428658');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20277, 'LACDU CERF', 'LAC-DU-CERF', 'QC', '2015-10-13 19:02:57.434857', '2015-10-13 19:02:57.434857');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20278, 'LACECORCES', 'LAC-DES-ECORCES', 'QC', '2015-10-13 19:02:57.439115', '2015-10-13 19:02:57.439115');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20279, 'LACEDOUARD', 'LAC-EDOUARD', 'QC', '2015-10-13 19:02:57.443123', '2015-10-13 19:02:57.443123');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20280, 'LACETCHMIN', 'LAC-ETCHEMIN', 'QC', '2015-10-13 19:02:57.447179', '2015-10-13 19:02:57.447179');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20281, 'LACFRONTIR', 'LAC-FRONTIERE', 'QC', '2015-10-13 19:02:57.455605', '2015-10-13 19:02:57.455605');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20282, 'LACHINE', 'LACHINE', 'QC', '2015-10-13 19:02:57.4729', '2015-10-13 19:02:57.4729');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20283, 'LACHUTE', 'LACHUTE', 'QC', '2015-10-13 19:02:57.479053', '2015-10-13 19:02:57.479053');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20284, 'LACMGANTIC', 'LAC-MEGANTIC', 'QC', '2015-10-13 19:02:57.483041', '2015-10-13 19:02:57.483041');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20285, 'LACMSTSSNI', 'LAC-MISTASSINI', 'QC', '2015-10-13 19:02:57.487152', '2015-10-13 19:02:57.487152');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20286, 'LACOLLE', 'LACOLLE', 'QC', '2015-10-13 19:02:57.493241', '2015-10-13 19:02:57.493241');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20287, 'LAFORGE', 'LAFORGE', 'QC', '2015-10-13 19:02:57.504247', '2015-10-13 19:02:57.504247');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20288, 'LAGUADLOUP', 'LA GUADELOUPE', 'QC', '2015-10-13 19:02:57.512772', '2015-10-13 19:02:57.512772');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20289, 'LAMBTON', 'LAMBTON', 'QC', '2015-10-13 19:02:57.518764', '2015-10-13 19:02:57.518764');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20290, 'LANNONCTIN', 'L''ANNONCIATION', 'QC', '2015-10-13 19:02:57.52493', '2015-10-13 19:02:57.52493');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20291, 'LANORAIE', 'LANORAIE', 'QC', '2015-10-13 19:02:57.531205', '2015-10-13 19:02:57.531205');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20292, 'LAPOCATIRE', 'LA POCATIERE', 'QC', '2015-10-13 19:02:57.539754', '2015-10-13 19:02:57.539754');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20293, 'LAPRAIRIE', 'LAPRAIRIE', 'QC', '2015-10-13 19:02:57.548362', '2015-10-13 19:02:57.548362');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20294, 'LATERRIERE', 'LATERRIERE', 'QC', '2015-10-13 19:02:57.579902', '2015-10-13 19:02:57.579902');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20295, 'LATULIPE', 'LATULIPE', 'QC', '2015-10-13 19:02:57.604162', '2015-10-13 19:02:57.604162');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20296, 'LAURIERVL', 'LAURIERVILLE', 'QC', '2015-10-13 19:02:57.621417', '2015-10-13 19:02:57.621417');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20297, 'LAVAL EST', 'LAVAL EST', 'QC', '2015-10-13 19:02:57.632386', '2015-10-13 19:02:57.632386');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20298, 'LAVALOUEST', 'LAVAL OUEST', 'QC', '2015-10-13 19:02:57.640951', '2015-10-13 19:02:57.640951');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20299, 'LAVALTRIE', 'LAVALTRIE', 'QC', '2015-10-13 19:02:57.647267', '2015-10-13 19:02:57.647267');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20300, 'LAVERLOCHR', 'LAVERLOCHERE', 'QC', '2015-10-13 19:02:57.656098', '2015-10-13 19:02:57.656098');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20301, 'LAWRENCEVL', 'LAWRENCEVILLE', 'QC', '2015-10-13 19:02:57.664667', '2015-10-13 19:02:57.664667');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20302, 'LBCHCOUTMI', 'LA BAIE (CHICOUTIMI)', 'QC', '2015-10-13 19:02:57.67273', '2015-10-13 19:02:57.67273');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20303, 'LBLSRQVLLN', 'LEBEL-SUR-QUEVILLON', 'QC', '2015-10-13 19:02:57.688715', '2015-10-13 19:02:57.688715');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20304, 'LCAUSAUMON', 'LAC-AU-SAUMON', 'QC', '2015-10-13 19:02:57.700777', '2015-10-13 19:02:57.700777');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20305, 'LCAXSABLES', 'LAC-AUX-SABLES', 'QC', '2015-10-13 19:02:57.708605', '2015-10-13 19:02:57.708605');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20306, 'LE GARDEUR', 'LE GARDEUR', 'QC', '2015-10-13 19:02:57.716323', '2015-10-13 19:02:57.716323');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20307, 'LEEDS', 'LEEDS', 'QC', '2015-10-13 19:02:57.725308', '2015-10-13 19:02:57.725308');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20308, 'LEPNLASPTN', 'L''EPIPHANIE-L''ASSOMPTION', 'QC', '2015-10-13 19:02:57.752265', '2015-10-13 19:02:57.752265');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20309, 'LES BOULES', 'LES BOULES', 'QC', '2015-10-13 19:02:57.760729', '2015-10-13 19:02:57.760729');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20310, 'LES CEDRES', 'LES CEDRES', 'QC', '2015-10-13 19:02:57.781152', '2015-10-13 19:02:57.781152');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20311, 'LESEBLMNTS', 'LES EBOULEMENTS', 'QC', '2015-10-13 19:02:57.793396', '2015-10-13 19:02:57.793396');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20312, 'LESMECHINS', 'LES MECHINS', 'QC', '2015-10-13 19:02:57.809698', '2015-10-13 19:02:57.809698');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20313, 'LEVIS', 'LEVIS', 'QC', '2015-10-13 19:02:57.82284', '2015-10-13 19:02:57.82284');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20314, 'LONGUEUIL', 'LONGUEUIL', 'QC', '2015-10-13 19:02:57.828784', '2015-10-13 19:02:57.828784');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20315, 'LORETTEVL', 'LORETTEVILLE', 'QC', '2015-10-13 19:02:57.836963', '2015-10-13 19:02:57.836963');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20316, 'LORRAINVL', 'LORRAINVILLE', 'QC', '2015-10-13 19:02:57.843019', '2015-10-13 19:02:57.843019');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20317, 'LOUISEVL', 'LOUISEVILLE', 'QC', '2015-10-13 19:02:57.847016', '2015-10-13 19:02:57.847016');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20318, 'LOUVICOURT', 'LOUVICOURT', 'QC', '2015-10-13 19:02:57.850837', '2015-10-13 19:02:57.850837');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20319, 'LOW', 'LOW', 'QC', '2015-10-13 19:02:57.854944', '2015-10-13 19:02:57.854944');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20320, 'LUCEVILLE', 'LUCEVILLE', 'QC', '2015-10-13 19:02:57.85934', '2015-10-13 19:02:57.85934');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20321, 'LUSKVILLE', 'LUSKVILLE', 'QC', '2015-10-13 19:02:57.865165', '2015-10-13 19:02:57.865165');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20322, 'LYSTER', 'LYSTER', 'QC', '2015-10-13 19:02:57.87365', '2015-10-13 19:02:57.87365');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20323, 'MACAMIC', 'MACAMIC', 'QC', '2015-10-13 19:02:57.884183', '2015-10-13 19:02:57.884183');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20324, 'MAGOG', 'MAGOG', 'QC', '2015-10-13 19:02:57.892952', '2015-10-13 19:02:57.892952');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20325, 'MALARTIC', 'MALARTIC', 'QC', '2015-10-13 19:02:57.90566', '2015-10-13 19:02:57.90566');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20326, 'MANICOUGN5', 'MANICOUAGAN 5', 'QC', '2015-10-13 19:02:57.916634', '2015-10-13 19:02:57.916634');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20327, 'MANIWAKI', 'MANIWAKI', 'QC', '2015-10-13 19:02:57.948329', '2015-10-13 19:02:57.948329');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20328, 'MANOUANE', 'MANOUANE', 'QC', '2015-10-13 19:02:58.016155', '2015-10-13 19:02:58.016155');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20329, 'MANSEAU', 'MANSEAU', 'QC', '2015-10-13 19:02:58.08518', '2015-10-13 19:02:58.08518');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20330, 'MANSONVL', 'MANSONVILLE', 'QC', '2015-10-13 19:02:58.429684', '2015-10-13 19:02:58.429684');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20331, 'MARIA', 'MARIA', 'QC', '2015-10-13 19:02:58.856385', '2015-10-13 19:02:58.856385');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20332, 'MARIEVILLE', 'MARIEVILLE', 'QC', '2015-10-13 19:02:58.892354', '2015-10-13 19:02:58.892354');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20333, 'MASCOUCHE', 'MASCOUCHE', 'QC', '2015-10-13 19:02:58.916249', '2015-10-13 19:02:58.916249');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20334, 'MASKINONGE', 'MASKINONGE', 'QC', '2015-10-13 19:02:58.944835', '2015-10-13 19:02:58.944835');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20335, 'MATAGAMI', 'MATAGAMI', 'QC', '2015-10-13 19:02:58.965449', '2015-10-13 19:02:58.965449');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20336, 'MATANE', 'MATANE', 'QC', '2015-10-13 19:02:58.996752', '2015-10-13 19:02:58.996752');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20337, 'MATAPEDIA', 'MATAPEDIA', 'QC', '2015-10-13 19:02:59.008796', '2015-10-13 19:02:59.008796');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20338, 'METABTCHON', 'METABETCHOUAN', 'QC', '2015-10-13 19:02:59.020804', '2015-10-13 19:02:59.020804');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20339, 'MILOT', 'MILOT', 'QC', '2015-10-13 19:02:59.035976', '2015-10-13 19:02:59.035976');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20340, 'MIRABLARPT', 'MIRABEL AEROPORT', 'QC', '2015-10-13 19:02:59.059756', '2015-10-13 19:02:59.059756');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20341, 'MOISIE', 'MOISIE', 'QC', '2015-10-13 19:02:59.069421', '2015-10-13 19:02:59.069421');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20342, 'MONT JOLI', 'MONT-JOLI', 'QC', '2015-10-13 19:02:59.080877', '2015-10-13 19:02:59.080877');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20343, 'MONT LOUIS', 'MONT-LOUIS', 'QC', '2015-10-13 19:02:59.089605', '2015-10-13 19:02:59.089605');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20344, 'MONTEBELLO', 'MONTEBELLO', 'QC', '2015-10-13 19:02:59.100916', '2015-10-13 19:02:59.100916');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20345, 'MONTLAURIR', 'MONT-LAURIER', 'QC', '2015-10-13 19:02:59.111768', '2015-10-13 19:02:59.111768');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20346, 'MONTMAGNY', 'MONTMAGNY', 'QC', '2015-10-13 19:02:59.125716', '2015-10-13 19:02:59.125716');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20347, 'MONTREAL', 'MONTREAL', 'QC', '2015-10-13 19:02:59.135268', '2015-10-13 19:02:59.135268');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20348, 'MORIN HTS', 'MORIN HEIGHTS', 'QC', '2015-10-13 19:02:59.141585', '2015-10-13 19:02:59.141585');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20349, 'MRLSTAGSTN', 'MIRABEL: SAINT-AUGUSTIN', 'QC', '2015-10-13 19:02:59.153099', '2015-10-13 19:02:59.153099');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20350, 'MRSTSCLSTQ', 'MIRABEL: SAINTE-SCHOLASTIQUE', 'QC', '2015-10-13 19:02:59.164255', '2015-10-13 19:02:59.164255');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20351, 'MURDOCHVL', 'MURDOCHVILLE', 'QC', '2015-10-13 19:02:59.170983', '2015-10-13 19:02:59.170983');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20352, 'MUTTON BAY', 'MUTTON BAY', 'QC', '2015-10-13 19:02:59.179625', '2015-10-13 19:02:59.179625');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20353, 'NANTES', 'NANTES', 'QC', '2015-10-13 19:02:59.188758', '2015-10-13 19:02:59.188758');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20354, 'NAPIERVL', 'NAPIERVILLE', 'QC', '2015-10-13 19:02:59.195279', '2015-10-13 19:02:59.195279');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20355, 'NATASHQUAN', 'NATASHQUAN', 'QC', '2015-10-13 19:02:59.203839', '2015-10-13 19:02:59.203839');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20356, 'NEDELEC', 'NEDELEC', 'QC', '2015-10-13 19:02:59.213215', '2015-10-13 19:02:59.213215');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20357, 'NEMASKA', 'NEMASKA', 'QC', '2015-10-13 19:02:59.223826', '2015-10-13 19:02:59.223826');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20358, 'NEUVILLE', 'NEUVILLE', 'QC', '2015-10-13 19:02:59.232887', '2015-10-13 19:02:59.232887');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20359, 'NEWCARLISL', 'NEW CARLISLE', 'QC', '2015-10-13 19:02:59.248026', '2015-10-13 19:02:59.248026');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20360, 'NEWPORT', 'NEWPORT', 'QC', '2015-10-13 19:02:59.256361', '2015-10-13 19:02:59.256361');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20361, 'NEWRICHMND', 'NEW RICHMOND', 'QC', '2015-10-13 19:02:59.264543', '2015-10-13 19:02:59.264543');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20362, 'NICOLET', 'NICOLET', 'QC', '2015-10-13 19:02:59.272814', '2015-10-13 19:02:59.272814');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20363, 'NO HATLEY', 'NORTH HATLEY', 'QC', '2015-10-13 19:02:59.278948', '2015-10-13 19:02:59.278948');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20364, 'NOMININGUE', 'NOMININGUE', 'QC', '2015-10-13 19:02:59.284928', '2015-10-13 19:02:59.284928');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20365, 'NORANDA', 'NORANDA', 'QC', '2015-10-13 19:02:59.290705', '2015-10-13 19:02:59.290705');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20366, 'NORBERTVL', 'NORBERTVILLE', 'QC', '2015-10-13 19:02:59.296686', '2015-10-13 19:02:59.296686');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20367, 'NORMANDIN', 'NORMANDIN', 'QC', '2015-10-13 19:02:59.304654', '2015-10-13 19:02:59.304654');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20368, 'NORMETAL', 'NORMETAL', 'QC', '2015-10-13 19:02:59.311032', '2015-10-13 19:02:59.311032');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20369, 'NOTRDAMDLC', 'NOTRE-DAME-DU-LAC', 'QC', '2015-10-13 19:02:59.315073', '2015-10-13 19:02:59.315073');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20370, 'NOTRDAMPAX', 'NOTRE-DAME-DE-LA-PAIX', 'QC', '2015-10-13 19:02:59.319274', '2015-10-13 19:02:59.319274');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20371, 'NOUVELLE', 'NOUVELLE', 'QC', '2015-10-13 19:02:59.329866', '2015-10-13 19:02:59.329866');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20372, 'NRDMLDSMGC', 'NOTRE-DAME-DE-LOURDES (MEGANTIC)', 'QC', '2015-10-13 19:02:59.34069', '2015-10-13 19:02:59.34069');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20373, 'NTDMDBNCNL', 'NOTRE-DAME-DU-BON-CONSEIL', 'QC', '2015-10-13 19:02:59.347112', '2015-10-13 19:02:59.347112');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20374, 'NTDMLRNTDS', 'NOTRE-DAME-DES-LAURENTIDES', 'QC', '2015-10-13 19:02:59.351105', '2015-10-13 19:02:59.351105');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20375, 'NTDMSTNBDG', 'NOTRE-DAME-DE-STANBRIDGE', 'QC', '2015-10-13 19:02:59.355248', '2015-10-13 19:02:59.355248');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20376, 'NTRDAMDLAS', 'NOTRE-DAME-DU-LAUS', 'QC', '2015-10-13 19:02:59.363527', '2015-10-13 19:02:59.363527');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20377, 'NTRDAMDNRD', 'NOTRE-DAME-DU-NORD', 'QC', '2015-10-13 19:02:59.372182', '2015-10-13 19:02:59.372182');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20378, 'NTRDMSALTT', 'NOTRE-DAME-DE-LA-SALETTE', 'QC', '2015-10-13 19:02:59.380908', '2015-10-13 19:02:59.380908');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20379, 'OBEDJIWAN', 'OBEDJIWAN', 'QC', '2015-10-13 19:02:59.387206', '2015-10-13 19:02:59.387206');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20380, 'OKA', 'OKA', 'QC', '2015-10-13 19:02:59.393477', '2015-10-13 19:02:59.393477');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20381, 'ORMSTOWN', 'ORMSTOWN', 'QC', '2015-10-13 19:02:59.403723', '2015-10-13 19:02:59.403723');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20382, 'OTTAWAHULL', 'OTTAWA-HULL', 'QC', '2015-10-13 19:02:59.412487', '2015-10-13 19:02:59.412487');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20383, 'PALMAROLLE', 'PALMAROLLE', 'QC', '2015-10-13 19:02:59.419249', '2015-10-13 19:02:59.419249');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20384, 'PAPINEAUVL', 'PAPINEAUVILLE', 'QC', '2015-10-13 19:02:59.425313', '2015-10-13 19:02:59.425313');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20385, 'PARCLRNTDS', 'PARC-DES-LAURENTIDES', 'QC', '2015-10-13 19:02:59.435561', '2015-10-13 19:02:59.435561');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20386, 'PARCVRNDRY', 'PARC-DE-LA-VERENDRYE', 'QC', '2015-10-13 19:02:59.443799', '2015-10-13 19:02:59.443799');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20387, 'PARENT', 'PARENT', 'QC', '2015-10-13 19:02:59.459108', '2015-10-13 19:02:59.459108');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20388, 'PENTECOTE', 'PENTECOTE', 'QC', '2015-10-13 19:02:59.468233', '2015-10-13 19:02:59.468233');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20389, 'PERCE', 'PERCE', 'QC', '2015-10-13 19:02:59.479883', '2015-10-13 19:02:59.479883');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20390, 'PERIBONKA', 'PERIBONKA', 'QC', '2015-10-13 19:02:59.487757', '2015-10-13 19:02:59.487757');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20391, 'PERKINS', 'PERKINS', 'QC', '2015-10-13 19:02:59.496288', '2015-10-13 19:02:59.496288');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20392, 'PIERREVL', 'PIERREVILLE', 'QC', '2015-10-13 19:02:59.504716', '2015-10-13 19:02:59.504716');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20393, 'PLESSISVL', 'PLESSISVILLE', 'QC', '2015-10-13 19:02:59.510885', '2015-10-13 19:02:59.510885');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20394, 'PONT ROUGE', 'PONT-ROUGE', 'QC', '2015-10-13 19:02:59.515765', '2015-10-13 19:02:59.515765');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20395, 'PONT VIAU', 'PONT-VIAU', 'QC', '2015-10-13 19:02:59.523879', '2015-10-13 19:02:59.523879');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20396, 'PORTDANIEL', 'PORT-DANIEL', 'QC', '2015-10-13 19:02:59.532595', '2015-10-13 19:02:59.532595');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20397, 'PORTNEUF', 'PORTNEUF', 'QC', '2015-10-13 19:02:59.538874', '2015-10-13 19:02:59.538874');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20398, 'PRINCEVL', 'PRINCEVILLE', 'QC', '2015-10-13 19:02:59.54296', '2015-10-13 19:02:59.54296');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20399, 'PT CARTIER', 'PORT-CARTIER', 'QC', '2015-10-13 19:02:59.547024', '2015-10-13 19:02:59.547024');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20400, 'PT CLAIRE', 'POINTE-CLAIRE', 'QC', '2015-10-13 19:02:59.553172', '2015-10-13 19:02:59.553172');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20401, 'PT CROIX', 'POINTE-A-LA-CROIX', 'QC', '2015-10-13 19:02:59.559294', '2015-10-13 19:02:59.559294');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20402, 'PTMNRNTCSS', 'PORT-MENIER (ANTICOSTI ISLAND)', 'QC', '2015-10-13 19:02:59.565493', '2015-10-13 19:02:59.565493');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20403, 'PTRVSTFRCS', 'PETITE-RIVIERE-SAINT-FRANCOIS', 'QC', '2015-10-13 19:02:59.57725', '2015-10-13 19:02:59.57725');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20404, 'PUVIRNITUK', 'PUVIRNITUK', 'QC', '2015-10-13 19:02:59.621385', '2015-10-13 19:02:59.621385');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20405, 'QUAQTAQ', 'QUAQTAQ', 'QC', '2015-10-13 19:02:59.645268', '2015-10-13 19:02:59.645268');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20406, 'QUEBEC', 'QUEBEC', 'QC', '2015-10-13 19:02:59.653568', '2015-10-13 19:02:59.653568');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20407, 'QUYON', 'QUYON', 'QC', '2015-10-13 19:02:59.663739', '2015-10-13 19:02:59.663739');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20408, 'RADISSON', 'RADISSON', 'QC', '2015-10-13 19:02:59.672452', '2015-10-13 19:02:59.672452');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20409, 'RAWDON', 'RAWDON', 'QC', '2015-10-13 19:02:59.680981', '2015-10-13 19:02:59.680981');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20410, 'REMIGNY', 'REMIGNY', 'QC', '2015-10-13 19:02:59.687304', '2015-10-13 19:02:59.687304');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20411, 'RICHMOND', 'RICHMOND', 'QC', '2015-10-13 19:02:59.693497', '2015-10-13 19:02:59.693497');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20412, 'RIGAUD', 'RIGAUD', 'QC', '2015-10-13 19:02:59.703805', '2015-10-13 19:02:59.703805');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20413, 'RIMOUSKI', 'RIMOUSKI', 'QC', '2015-10-13 19:02:59.712166', '2015-10-13 19:02:59.712166');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20414, 'RIV BLEUE', 'RIVIERE-BLEUE', 'QC', '2015-10-13 19:02:59.720601', '2015-10-13 19:02:59.720601');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20415, 'RIV HEVA', 'RIVIERE-HEVA', 'QC', '2015-10-13 19:02:59.728966', '2015-10-13 19:02:59.728966');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20416, 'RIV PIERRE', 'RIVIERE-A-PIERRE', 'QC', '2015-10-13 19:02:59.734929', '2015-10-13 19:02:59.734929');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20417, 'RIVBEAUDET', 'RIVIERE-BEAUDETTE', 'QC', '2015-10-13 19:02:59.738983', '2015-10-13 19:02:59.738983');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20418, 'RIVDU LOUP', 'RIVIERE-DU-LOUP', 'QC', '2015-10-13 19:02:59.743576', '2015-10-13 19:02:59.743576');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20419, 'RIVST JEAN', 'RIVIERE-SAINT-JEAN', 'QC', '2015-10-13 19:02:59.751752', '2015-10-13 19:02:59.751752');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20420, 'ROBERVAL', 'ROBERVAL', 'QC', '2015-10-13 19:02:59.76018', '2015-10-13 19:02:59.76018');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20421, 'ROCHEBAUCT', 'ROCHEBAUCOURT', 'QC', '2015-10-13 19:02:59.768766', '2015-10-13 19:02:59.768766');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20422, 'ROCKISLAND', 'ROCK ISLAND', 'QC', '2015-10-13 19:02:59.775233', '2015-10-13 19:02:59.775233');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20423, 'ROXBORO', 'ROXBORO', 'QC', '2015-10-13 19:02:59.783409', '2015-10-13 19:02:59.783409');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20424, 'ROXTON FLS', 'ROXTON FALLS', 'QC', '2015-10-13 19:02:59.792372', '2015-10-13 19:02:59.792372');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20425, 'RVATONNRRE', 'RIVIERE-AU-TONNERRE', 'QC', '2015-10-13 19:02:59.800739', '2015-10-13 19:02:59.800739');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20426, 'RVAURENARD', 'RIVIERE-AU-RENARD', 'QC', '2015-10-13 19:02:59.806736', '2015-10-13 19:02:59.806736');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20427, 'SACRECOEUR', 'SACRE-COEUR', 'QC', '2015-10-13 19:02:59.810839', '2015-10-13 19:02:59.810839');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20428, 'SALLUIT', 'SALLUIT', 'QC', '2015-10-13 19:02:59.815751', '2015-10-13 19:02:59.815751');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20429, 'SALTAMOUTN', 'SAULT-AU-MOUTON', 'QC', '2015-10-13 19:02:59.823704', '2015-10-13 19:02:59.823704');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20430, 'SANMAUR', 'SANMAUR', 'QC', '2015-10-13 19:02:59.832532', '2015-10-13 19:02:59.832532');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20431, 'SAWYERVL', 'SAWYERVILLE', 'QC', '2015-10-13 19:02:59.838855', '2015-10-13 19:02:59.838855');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20432, 'SAYABEC', 'SAYABEC', 'QC', '2015-10-13 19:02:59.842809', '2015-10-13 19:02:59.842809');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20433, 'SCHEFFERVL', 'SCHEFFERVILLE', 'QC', '2015-10-13 19:02:59.851186', '2015-10-13 19:02:59.851186');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20434, 'SCOTSTOWN', 'SCOTSTOWN', 'QC', '2015-10-13 19:02:59.855012', '2015-10-13 19:02:59.855012');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20435, 'SENNETERRE', 'SENNETERRE', 'QC', '2015-10-13 19:02:59.858912', '2015-10-13 19:02:59.858912');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20436, 'SEPT ILES', 'SEPT-ILES', 'QC', '2015-10-13 19:02:59.86314', '2015-10-13 19:02:59.86314');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20437, 'SFRNMNMGNY', 'SAINT-FRANCOIS-MONTMAGNY', 'QC', '2015-10-13 19:02:59.867056', '2015-10-13 19:02:59.867056');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20438, 'SHAWBRIDGE', 'SHAWBRIDGE', 'QC', '2015-10-13 19:02:59.871057', '2015-10-13 19:02:59.871057');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20439, 'SHAWINIGAN', 'SHAWINIGAN', 'QC', '2015-10-13 19:02:59.876836', '2015-10-13 19:02:59.876836');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20440, 'SHAWVILLE', 'SHAWVILLE', 'QC', '2015-10-13 19:02:59.884996', '2015-10-13 19:02:59.884996');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20441, 'SHERBROOKE', 'SHERBROOKE', 'QC', '2015-10-13 19:02:59.89083', '2015-10-13 19:02:59.89083');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20442, 'SLPHSRDRGZ', 'SAINT-ALPHONSE-DE-RODRIGUEZ', 'QC', '2015-10-13 19:02:59.894813', '2015-10-13 19:02:59.894813');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20443, 'SO DURHAM', 'SOUTH DURHAM', 'QC', '2015-10-13 19:02:59.898874', '2015-10-13 19:02:59.898874');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20444, 'SOREL', 'SOREL', 'QC', '2015-10-13 19:02:59.904681', '2015-10-13 19:02:59.904681');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20445, 'SQUATEC', 'SQUATEC', 'QC', '2015-10-13 19:02:59.910916', '2015-10-13 19:02:59.910916');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20446, 'SSBSNFRNNC', 'SAINT-SEBASTIEN (FRONTENAC)', 'QC', '2015-10-13 19:02:59.915056', '2015-10-13 19:02:59.915056');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20447, 'ST ADELPHE', 'SAINT-ADELPHE', 'QC', '2015-10-13 19:02:59.919118', '2015-10-13 19:02:59.919118');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20448, 'ST AGAPIT', 'SAINT-AGAPIT', 'QC', '2015-10-13 19:02:59.923404', '2015-10-13 19:02:59.923404');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20449, 'ST AIME', 'SAINT-AIME', 'QC', '2015-10-13 19:02:59.929201', '2015-10-13 19:02:59.929201');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20450, 'ST ALBERT', 'SAINT-ALBERT', 'QC', '2015-10-13 19:02:59.939734', '2015-10-13 19:02:59.939734');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20451, 'ST ANDRE', 'SAINT-ANDRE', 'QC', '2015-10-13 19:02:59.947848', '2015-10-13 19:02:59.947848');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20452, 'ST ANSELME', 'SAINT-ANSELME', 'QC', '2015-10-13 19:02:59.956215', '2015-10-13 19:02:59.956215');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20453, 'ST BARNABE', 'SAINT-BARNABE', 'QC', '2015-10-13 19:02:59.965075', '2015-10-13 19:02:59.965075');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20454, 'ST BLAISE', 'SAINT-BLAISE', 'QC', '2015-10-13 19:02:59.977226', '2015-10-13 19:02:59.977226');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20455, 'ST BRUNO', 'SAINT-BRUNO', 'QC', '2015-10-13 19:03:00.009731', '2015-10-13 19:03:00.009731');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20456, 'ST CASIMIR', 'SAINT-CASIMIR', 'QC', '2015-10-13 19:03:00.029958', '2015-10-13 19:03:00.029958');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20457, 'ST CESAIRE', 'SAINT-CESAIRE', 'QC', '2015-10-13 19:03:00.05369', '2015-10-13 19:03:00.05369');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20458, 'ST CLET', 'SAINT-CLET', 'QC', '2015-10-13 19:03:00.169132', '2015-10-13 19:03:00.169132');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20459, 'ST DENIS', 'SAINT-DENIS', 'QC', '2015-10-13 19:03:00.31332', '2015-10-13 19:03:00.31332');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20460, 'ST EULALIE', 'SAINTE-EULALIE', 'QC', '2015-10-13 19:03:00.505719', '2015-10-13 19:03:00.505719');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20461, 'ST FAUSTIN', 'SAINT-FAUSTIN', 'QC', '2015-10-13 19:03:00.55359', '2015-10-13 19:03:00.55359');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20462, 'ST FEREOL', 'SAINT-FEREOL', 'QC', '2015-10-13 19:03:00.77375', '2015-10-13 19:03:00.77375');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20463, 'ST FLAVIEN', 'SAINT-FLAVIEN', 'QC', '2015-10-13 19:03:00.885325', '2015-10-13 19:03:00.885325');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20464, 'ST GEDEON', 'SAINT-GEDEON', 'QC', '2015-10-13 19:03:00.932397', '2015-10-13 19:03:00.932397');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20465, 'ST HUGUES', 'SAINT-HUGUES', 'QC', '2015-10-13 19:03:00.985787', '2015-10-13 19:03:00.985787');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20466, 'ST IRENEE', 'SAINT-IRENEE', 'QC', '2015-10-13 19:03:01.161476', '2015-10-13 19:03:01.161476');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20467, 'ST JACQUES', 'SAINT-JACQUES', 'QC', '2015-10-13 19:03:01.525592', '2015-10-13 19:03:01.525592');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20468, 'ST JEAN', 'SAINT-JEAN', 'QC', '2015-10-13 19:03:01.585664', '2015-10-13 19:03:01.585664');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20469, 'ST JEROME', 'SAINT-JEROME', 'QC', '2015-10-13 19:03:01.840047', '2015-10-13 19:03:01.840047');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20470, 'ST JOVITE', 'SAINT-JOVITE', 'QC', '2015-10-13 19:03:01.989584', '2015-10-13 19:03:01.989584');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20471, 'ST JUDE', 'SAINT-JUDE', 'QC', '2015-10-13 19:03:02.007266', '2015-10-13 19:03:02.007266');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20472, 'ST JUSTINE', 'SAINTE-JUSTINE', 'QC', '2015-10-13 19:03:02.015219', '2015-10-13 19:03:02.015219');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20473, 'ST LAMBERT', 'SAINT-LAMBERT', 'QC', '2015-10-13 19:03:02.024018', '2015-10-13 19:03:02.024018');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20474, 'ST LIBOIRE', 'SAINT-LIBOIRE', 'QC', '2015-10-13 19:03:02.031871', '2015-10-13 19:03:02.031871');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20475, 'ST LIN', 'SAINT-LIN', 'QC', '2015-10-13 19:03:02.039807', '2015-10-13 19:03:02.039807');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20476, 'ST LUDGER', 'SAINT-LUDGER', 'QC', '2015-10-13 19:03:02.048214', '2015-10-13 19:03:02.048214');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20477, 'ST MALO', 'SAINT-MALO', 'QC', '2015-10-13 19:03:02.056062', '2015-10-13 19:03:02.056062');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20478, 'ST MOISE', 'SAINT-MOISE', 'QC', '2015-10-13 19:03:02.06358', '2015-10-13 19:03:02.06358');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20479, 'ST NICOLAS', 'SAINT-NICOLAS', 'QC', '2015-10-13 19:03:02.069786', '2015-10-13 19:03:02.069786');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20480, 'ST OURS', 'SAINT-OURS', 'QC', '2015-10-13 19:03:02.077666', '2015-10-13 19:03:02.077666');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20481, 'ST PACOME', 'SAINT-PACOME', 'QC', '2015-10-13 19:03:02.087992', '2015-10-13 19:03:02.087992');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20482, 'ST PASCAL', 'SAINT-PASCAL', 'QC', '2015-10-13 19:03:02.095985', '2015-10-13 19:03:02.095985');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20483, 'ST PAULIN', 'SAINT-PAULIN', 'QC', '2015-10-13 19:03:02.103688', '2015-10-13 19:03:02.103688');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20484, 'ST PIE', 'SAINT-PIE', 'QC', '2015-10-13 19:03:02.113274', '2015-10-13 19:03:02.113274');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20485, 'ST PRIME', 'SAINT-PRIME', 'QC', '2015-10-13 19:03:02.12069', '2015-10-13 19:03:02.12069');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20486, 'ST RAYMOND', 'SAINT-RAYMOND', 'QC', '2015-10-13 19:03:02.128205', '2015-10-13 19:03:02.128205');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20487, 'ST REGIS', 'SAINT-REGIS', 'QC', '2015-10-13 19:03:02.135909', '2015-10-13 19:03:02.135909');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20488, 'ST REMI', 'SAINT-REMI', 'QC', '2015-10-13 19:03:02.14398', '2015-10-13 19:03:02.14398');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20489, 'ST ROSALIE', 'SAINTE-ROSALIE', 'QC', '2015-10-13 19:03:02.149575', '2015-10-13 19:03:02.149575');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20490, 'ST SAUVEUR', 'SAINT-SAUVEUR', 'QC', '2015-10-13 19:03:02.154756', '2015-10-13 19:03:02.154756');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20491, 'ST SYLVERE', 'SAINT-SYLVERE', 'QC', '2015-10-13 19:03:02.157933', '2015-10-13 19:03:02.157933');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20492, 'ST THECLE', 'SAINTE-THECLE', 'QC', '2015-10-13 19:03:02.167381', '2015-10-13 19:03:02.167381');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20493, 'ST THERESE', 'SAINTE-THERESE', 'QC', '2015-10-13 19:03:02.170854', '2015-10-13 19:03:02.170854');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20494, 'ST TITE', 'SAINT-TITE', 'QC', '2015-10-13 19:03:02.174001', '2015-10-13 19:03:02.174001');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20495, 'ST UBALDE', 'SAINT-UBALDE', 'QC', '2015-10-13 19:03:02.179414', '2015-10-13 19:03:02.179414');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20496, 'ST ULRIC', 'SAINT-ULRIC', 'QC', '2015-10-13 19:03:02.182782', '2015-10-13 19:03:02.182782');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20497, 'ST URBAIN', 'SAINT-URBAIN', 'QC', '2015-10-13 19:03:02.188', '2015-10-13 19:03:02.188');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20498, 'ST ZENON', 'SAINT-ZENON', 'QC', '2015-10-13 19:03:02.19577', '2015-10-13 19:03:02.19577');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20499, 'ST-ALEXDRE', 'SAINT-ALEXANDRE', 'QC', '2015-10-13 19:03:02.200833', '2015-10-13 19:03:02.200833');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20500, 'ST-DAMASE', 'SAINT-DAMASE', 'QC', '2015-10-13 19:03:02.208021', '2015-10-13 19:03:02.208021');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20501, 'ST-FIDELE', 'SAINT-FIDELE', 'QC', '2015-10-13 19:03:02.213263', '2015-10-13 19:03:02.213263');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20502, 'ST-MARC', 'SAINT-MARC', 'QC', '2015-10-13 19:03:02.2203', '2015-10-13 19:03:02.2203');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20503, 'ST-MATHIEU', 'SAINT-MATHIEU', 'QC', '2015-10-13 19:03:02.225558', '2015-10-13 19:03:02.225558');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20504, 'ST-SIMEON', 'SAINT-SIMEON', 'QC', '2015-10-13 19:03:02.230748', '2015-10-13 19:03:02.230748');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20505, 'STADPHDHWD', 'SAINT-ADOLPHE-D''HOWARD', 'QC', '2015-10-13 19:03:02.235965', '2015-10-13 19:03:02.235965');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20506, 'STADPHDSWL', 'SAINT-ADOLPHE-DE-DUDSWELL', 'QC', '2015-10-13 19:03:02.241515', '2015-10-13 19:03:02.241515');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20507, 'STAGHLTBNR', 'SAINTE-AGATHE-DE-LOTBINIERE', 'QC', '2015-10-13 19:03:02.249149', '2015-10-13 19:03:02.249149');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20508, 'STAGTNDPLS', 'SAINT-AUGUSTIN-DE-DUPLESSIS', 'QC', '2015-10-13 19:03:02.255237', '2015-10-13 19:03:02.255237');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20509, 'STAGTNDSRS', 'SAINT-AUGUSTIN-DE-DESMAURES', 'QC', '2015-10-13 19:03:02.259191', '2015-10-13 19:03:02.259191');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20510, 'STALXSMNTS', 'SAINT-ALEXIS-DES-MONTS', 'QC', '2015-10-13 19:03:02.26346', '2015-10-13 19:03:02.26346');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20511, 'STALXSMTPD', 'SAINT-ALEXIS-DE-MATAPEDIA', 'QC', '2015-10-13 19:03:02.268881', '2015-10-13 19:03:02.268881');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20512, 'STAMRSCHTM', 'SAINT-AMBROISE-DE-CHICOUTIMI', 'QC', '2015-10-13 19:03:02.275089', '2015-10-13 19:03:02.275089');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20513, 'STANDRAVLN', 'SAINT-ANDRE-AVELLIN', 'QC', '2015-10-13 19:03:02.279371', '2015-10-13 19:03:02.279371');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20514, 'STANDREEST', 'SAINT-ANDRE-EST', 'QC', '2015-10-13 19:03:02.285015', '2015-10-13 19:03:02.285015');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20515, 'STANGLLVAL', 'SAINTE-ANGELE-DE-LAVAL', 'QC', '2015-10-13 19:03:02.291086', '2015-10-13 19:03:02.291086');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20516, 'STANNBEPRE', 'SAINTE-ANNE-DE-BEAUPRE', 'QC', '2015-10-13 19:03:02.295122', '2015-10-13 19:03:02.295122');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20517, 'STANNDULAC', 'SAINTE-ANNE-DU-LAC', 'QC', '2015-10-13 19:03:02.299127', '2015-10-13 19:03:02.299127');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20518, 'STANNMONTS', 'SAINTE-ANNE-DES-MONTS', 'QC', '2015-10-13 19:03:02.303048', '2015-10-13 19:03:02.303048');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20519, 'STANNPLANS', 'SAINTE-ANNE-DES-PLAINES', 'QC', '2015-10-13 19:03:02.309425', '2015-10-13 19:03:02.309425');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20520, 'STANNPRTNF', 'SAINTE-ANNE-DE-PORTNEUF', 'QC', '2015-10-13 19:03:02.319766', '2015-10-13 19:03:02.319766');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20521, 'STANPERADE', 'SAINTE-ANNE-DE-LA-PERADE', 'QC', '2015-10-13 19:03:02.331995', '2015-10-13 19:03:02.331995');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20522, 'STANTNTLLY', 'SAINT-ANTOINE-DE-TILLY', 'QC', '2015-10-13 19:03:02.339761', '2015-10-13 19:03:02.339761');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20523, 'STAPOLLINR', 'SAINT-APOLLINAIRE', 'QC', '2015-10-13 19:03:02.347928', '2015-10-13 19:03:02.347928');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20524, 'STBARTHLMY', 'SAINT-BARTHELEMY', 'QC', '2015-10-13 19:03:02.355972', '2015-10-13 19:03:02.355972');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20525, 'STBLANDINE', 'SAINTE-BLANDINE', 'QC', '2015-10-13 19:03:02.364198', '2015-10-13 19:03:02.364198');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20526, 'STBNCSHNGN', 'SAINT-BONIFACE-DE-SHAWINIGAN', 'QC', '2015-10-13 19:03:02.372359', '2015-10-13 19:03:02.372359');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20527, 'STBRDDRSTR', 'SAINT-BERNARD-DE-DORCHESTER', 'QC', '2015-10-13 19:03:02.380221', '2015-10-13 19:03:02.380221');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20528, 'STBRGTTLVL', 'SAINTE-BRIGITTE-DE-LAVAL', 'QC', '2015-10-13 19:03:02.39606', '2015-10-13 19:03:02.39606');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20529, 'STBSLPRTNF', 'SAINT-BASILE-DE-PORTNEUF', 'QC', '2015-10-13 19:03:02.403447', '2015-10-13 19:03:02.403447');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20530, 'STCELESTIN', 'SAINT-CELESTIN', 'QC', '2015-10-13 19:03:02.409625', '2015-10-13 19:03:02.409625');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20531, 'STCHLSBLHS', 'SAINT-CHARLES-DE-BELLECHASSE', 'QC', '2015-10-13 19:03:02.417108', '2015-10-13 19:03:02.417108');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20532, 'STCHRYSSTM', 'SAINT-CHRYSOSTOME', 'QC', '2015-10-13 19:03:02.423053', '2015-10-13 19:03:02.423053');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20533, 'STCLDRHSTR', 'SAINTE-CLAIRE-DE-DORCHESTER', 'QC', '2015-10-13 19:03:02.427181', '2015-10-13 19:03:02.427181');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20534, 'STCLTKLKNY', 'SAINT-CALIXTE-DE-KILKENNY', 'QC', '2015-10-13 19:03:02.432858', '2015-10-13 19:03:02.432858');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20535, 'STCMKNNBEC', 'SAINT-COME-DE-KENNEBEC', 'QC', '2015-10-13 19:03:02.438847', '2015-10-13 19:03:02.438847');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20536, 'STCMLBLCHS', 'SAINTE-CAMILLE-DE-BELLECHASSE', 'QC', '2015-10-13 19:03:02.443083', '2015-10-13 19:03:02.443083');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20537, 'STCONSTANT', 'SAINT-CONSTANT', 'QC', '2015-10-13 19:03:02.447071', '2015-10-13 19:03:02.447071');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20538, 'STCRXLTBNR', 'SAINTE-CROIX (LOTBINIERE)', 'QC', '2015-10-13 19:03:02.451283', '2015-10-13 19:03:02.451283');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20539, 'STCYLWNDVR', 'SAINT-CYRILLE-DE-WENDOVER', 'QC', '2015-10-13 19:03:02.459838', '2015-10-13 19:03:02.459838');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20540, 'STDMNBCKLD', 'SAINT-DAMIEN-DE-BUCKLAND', 'QC', '2015-10-13 19:03:02.46779', '2015-10-13 19:03:02.46779');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20541, 'STDNMNTCLM', 'SAINT-DONAT-DE-MONTCALM', 'QC', '2015-10-13 19:03:02.47565', '2015-10-13 19:03:02.47565');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20542, 'STE ADELE', 'SAINTE-ADELE', 'QC', '2015-10-13 19:03:02.483669', '2015-10-13 19:03:02.483669');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20543, 'STE AGATHE', 'SAINTE-AGATHE', 'QC', '2015-10-13 19:03:02.491619', '2015-10-13 19:03:02.491619');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20544, 'STE MARTHE', 'SAINTE-MARTHE', 'QC', '2015-10-13 19:03:02.497476', '2015-10-13 19:03:02.497476');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20545, 'STE ROSE', 'SAINTE-ROSE', 'QC', '2015-10-13 19:03:02.504939', '2015-10-13 19:03:02.504939');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20546, 'STE-CATHRN', 'SAINTE-CATHERINE', 'QC', '2015-10-13 19:03:02.51634', '2015-10-13 19:03:02.51634');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20547, 'STE-SABINE', 'SAINTE-SABINE', 'QC', '2015-10-13 19:03:02.523719', '2015-10-13 19:03:02.523719');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20548, 'STEDDLTBNR', 'SAINT-EDOUARD-DE-LOTBINIERE', 'QC', '2015-10-13 19:03:02.53173', '2015-10-13 19:03:02.53173');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20549, 'STEGNGUGUS', 'SAINT-EUGENE-DE-GUIGUES', 'QC', '2015-10-13 19:03:02.539621', '2015-10-13 19:03:02.539621');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20550, 'STELETHERE', 'SAINT-ELEUTHERE', 'QC', '2015-10-13 19:03:02.545608', '2015-10-13 19:03:02.545608');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20551, 'STEMARTINE', 'SAINTE-MARTINE', 'QC', '2015-10-13 19:03:02.553291', '2015-10-13 19:03:02.553291');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20552, 'STEMLSFFLK', 'SAINT-EMILE-DE-SUFFOLK', 'QC', '2015-10-13 19:03:02.559012', '2015-10-13 19:03:02.559012');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20553, 'STEPHRMBCE', 'SAINT-EPHREM-DE-BEAUCE', 'QC', '2015-10-13 19:03:02.56304', '2015-10-13 19:03:02.56304');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20554, 'STEUSTACHE', 'SAINT-EUSTACHE', 'QC', '2015-10-13 19:03:02.566864', '2015-10-13 19:03:02.566864');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20555, 'STFABPANET', 'SAINT-FABIEN-DE-PANET', 'QC', '2015-10-13 19:03:02.572212', '2015-10-13 19:03:02.572212');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20556, 'STFBNRMOSK', 'SAINT-FABIEN-DE-RIMOUSKI', 'QC', '2015-10-13 19:03:02.579577', '2015-10-13 19:03:02.579577');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20557, 'STFELICIEN', 'SAINT-FELICIEN', 'QC', '2015-10-13 19:03:02.583002', '2015-10-13 19:03:02.583002');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20558, 'STFELICITE', 'SAINTE-FELICITE', 'QC', '2015-10-13 19:03:02.588378', '2015-10-13 19:03:02.588378');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20559, 'STFLXKNGSY', 'SAINT-FELIX-DE-KINGSEY', 'QC', '2015-10-13 19:03:02.593607', '2015-10-13 19:03:02.593607');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20560, 'STFLXVLOIS', 'SAINT-FELIX-DE-VALOIS', 'QC', '2015-10-13 19:03:02.598889', '2015-10-13 19:03:02.598889');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20561, 'STFRNDDHLX', 'SAINT-FERDINAND D''HALIFAX', 'QC', '2015-10-13 19:03:02.604149', '2015-10-13 19:03:02.604149');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20562, 'STFULGENCE', 'SAINT-FULGENCE', 'QC', '2015-10-13 19:03:02.611485', '2015-10-13 19:03:02.611485');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20563, 'STGBLBRNDN', 'SAINT-GABRIEL-DE-BRANDON', 'QC', '2015-10-13 19:03:02.615068', '2015-10-13 19:03:02.615068');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20564, 'STGBRLRMSK', 'SAINT-GABRIEL-DE-RIMOUSKI', 'QC', '2015-10-13 19:03:02.62033', '2015-10-13 19:03:02.62033');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20565, 'STGDENBECE', 'SAINT-GEDEON-DE-BEAUCE', 'QC', '2015-10-13 19:03:02.625566', '2015-10-13 19:03:02.625566');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20566, 'STGNEVIEVE', 'SAINTE-GENEVIEVE', 'QC', '2015-10-13 19:03:02.631006', '2015-10-13 19:03:02.631006');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20567, 'STGRGRNCLT', 'SAINT-GREGOIRE-DE-NICOLET', 'QC', '2015-10-13 19:03:02.634646', '2015-10-13 19:03:02.634646');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20568, 'STGRGSBECE', 'SAINT-GEORGES-DE-BEAUCE', 'QC', '2015-10-13 19:03:02.64', '2015-10-13 19:03:02.64');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20569, 'STGRNGRTHM', 'SAINT-GERMAIN-DE-GRANTHAM', 'QC', '2015-10-13 19:03:02.655865', '2015-10-13 19:03:02.655865');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20570, 'STGRRDNCLT', 'SAINTE-GERTRUDE (NICOLET)', 'QC', '2015-10-13 19:03:02.66572', '2015-10-13 19:03:02.66572');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20571, 'STGUILLAUM', 'SAINT-GUILLAUME', 'QC', '2015-10-13 19:03:02.671195', '2015-10-13 19:03:02.671195');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20572, 'STHENEDINE', 'SAINTE-HENEDINE', 'QC', '2015-10-13 19:03:02.676318', '2015-10-13 19:03:02.676318');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20573, 'STHILARION', 'SAINT-HILARION', 'QC', '2015-10-13 19:03:02.68363', '2015-10-13 19:03:02.68363');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20574, 'STHIPPOLYT', 'SAINT-HIPPOLYTE', 'QC', '2015-10-13 19:03:02.687301', '2015-10-13 19:03:02.687301');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20575, 'STHLNEBGOT', 'SAINTE-HELENE-DE-BAGOT', 'QC', '2015-10-13 19:03:02.691361', '2015-10-13 19:03:02.691361');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20576, 'STHNRBEACE', 'SAINT-HONORE (BEAUCE)', 'QC', '2015-10-13 19:03:02.697263', '2015-10-13 19:03:02.697263');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20577, 'STHNRCHCTM', 'SAINT-HONORE (CHICOUTIMI)', 'QC', '2015-10-13 19:03:02.703064', '2015-10-13 19:03:02.703064');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20578, 'STHNRILVIS', 'SAINT-HENRI-DE-LEVIS', 'QC', '2015-10-13 19:03:02.707185', '2015-10-13 19:03:02.707185');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20579, 'STHNRTMSCT', 'SAINT-HONORE (TEMISCOUATA)', 'QC', '2015-10-13 19:03:02.711159', '2015-10-13 19:03:02.711159');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20580, 'STHYACINTH', 'SAINT-HYACINTHE', 'QC', '2015-10-13 19:03:02.71941', '2015-10-13 19:03:02.71941');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20581, 'STJEANDIEU', 'SAINT-JEAN-DE-DIEU', 'QC', '2015-10-13 19:03:02.725384', '2015-10-13 19:03:02.725384');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20582, 'STJENMATHA', 'SAINT-JEAN-DE-MATHA', 'QC', '2015-10-13 19:03:02.735215', '2015-10-13 19:03:02.735215');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20583, 'STJENPTJOL', 'SAINT-JEAN-PORT-JOLI', 'QC', '2015-10-13 19:03:02.739135', '2015-10-13 19:03:02.739135');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20584, 'STJLVRCHRS', 'SAINTE-JULIE-DE-VERCHERES', 'QC', '2015-10-13 19:03:02.743235', '2015-10-13 19:03:02.743235');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20585, 'STJNILDORS', 'SAINT-JEAN-ILE-D''ORLEANS', 'QC', '2015-10-13 19:03:02.747123', '2015-10-13 19:03:02.747123');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20586, 'STJSPHBECE', 'SAINT-JOSEPH-DE-BEAUCE', 'QC', '2015-10-13 19:03:02.751233', '2015-10-13 19:03:02.751233');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20587, 'STJSTBRNRS', 'SAINT-JUST-DE-BRETENIERES', 'QC', '2015-10-13 19:03:02.757517', '2015-10-13 19:03:02.757517');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20588, 'STJSTNNWTN', 'SAINTE-JUSTINE-DE-NEWTON', 'QC', '2015-10-13 19:03:02.765089', '2015-10-13 19:03:02.765089');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20589, 'STJULIENNE', 'SAINTE-JULIENNE', 'QC', '2015-10-13 19:03:02.771262', '2015-10-13 19:03:02.771262');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20590, 'STLCBRGARD', 'SAINTE-LUCIE-DE-BEAUREGARD', 'QC', '2015-10-13 19:03:02.777127', '2015-10-13 19:03:02.777127');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20591, 'STLCDRCHSR', 'SAINT-LUC (DORCHESTER)', 'QC', '2015-10-13 19:03:02.783138', '2015-10-13 19:03:02.783138');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20592, 'STLENLGRND', 'SAINT-LEON-LE-GRAND', 'QC', '2015-10-13 19:03:02.78725', '2015-10-13 19:03:02.78725');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20593, 'STLMBRTLZN', 'SAINT-LAMBERT-DE-LAUZON', 'QC', '2015-10-13 19:03:02.792978', '2015-10-13 19:03:02.792978');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20594, 'STLNDDASTN', 'SAINT-LEONARD-D''ASTON', 'QC', '2015-10-13 19:03:02.798766', '2015-10-13 19:03:02.798766');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20595, 'STMADLLINE', 'SAINTE-MADELEINE', 'QC', '2015-10-13 19:03:02.802777', '2015-10-13 19:03:02.802777');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20596, 'STMAGLOIRE', 'SAINT-MAGLOIRE', 'QC', '2015-10-13 19:03:02.808427', '2015-10-13 19:03:02.808427');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20597, 'STMALACHIE', 'SAINT-MALACHIE', 'QC', '2015-10-13 19:03:02.816047', '2015-10-13 19:03:02.816047');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20598, 'STMARGURIT', 'SAINTE-MARGUERITE', 'QC', '2015-10-13 19:03:02.823962', '2015-10-13 19:03:02.823962');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20599, 'STMCHLSNTS', 'SAINT-MICHEL-DES-SAINTS', 'QC', '2015-10-13 19:03:02.832055', '2015-10-13 19:03:02.832055');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20600, 'STMCLBLCHS', 'SAINT-MICHEL-DE-BELLECHASSE', 'QC', '2015-10-13 19:03:02.840128', '2015-10-13 19:03:02.840128');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20601, 'STMONQNCLT', 'SAINTE-MONIQUE-DE-NICOLET', 'QC', '2015-10-13 19:03:02.848209', '2015-10-13 19:03:02.848209');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20602, 'STMRBLNDFD', 'SAINTE-MARIE-DE-BLANDFORD', 'QC', '2015-10-13 19:03:02.856464', '2015-10-13 19:03:02.856464');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20603, 'STMRCCRRRS', 'SAINT-MARC-DES-CARRIERES', 'QC', '2015-10-13 19:03:02.864378', '2015-10-13 19:03:02.864378');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20604, 'STMRIBEUCE', 'SAINTE-MARIE-DE-BEAUCE', 'QC', '2015-10-13 19:03:02.872334', '2015-10-13 19:03:02.872334');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20605, 'STMRTNBECE', 'SAINT-MARTIN-DE-BEAUCE', 'QC', '2015-10-13 19:03:02.880093', '2015-10-13 19:03:02.880093');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20606, 'STMTDFRTNC', 'SAINT-METHODE-DE-FRONTENAC', 'QC', '2015-10-13 19:03:02.88764', '2015-10-13 19:03:02.88764');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20607, 'STNAZARCTN', 'SAINT-NAZAIRE-D''ACTON', 'QC', '2015-10-13 19:03:02.89331', '2015-10-13 19:03:02.89331');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20608, 'STODNCRBRN', 'SAINT-ODILON-DE-CRANBOURNE', 'QC', '2015-10-13 19:03:02.899109', '2015-10-13 19:03:02.899109');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20609, 'STOKE', 'STOKE', 'QC', '2015-10-13 19:03:02.902938', '2015-10-13 19:03:02.902938');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20610, 'STONEHAM', 'STONEHAM', 'QC', '2015-10-13 19:03:02.908656', '2015-10-13 19:03:02.908656');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20611, 'STPAMPHILE', 'SAINT-PAMPHILE', 'QC', '2015-10-13 19:03:02.91635', '2015-10-13 19:03:02.91635');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20612, 'STPERPETUE', 'SAINTE-PERPETUE', 'QC', '2015-10-13 19:03:02.924179', '2015-10-13 19:03:02.924179');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20613, 'STPHLIPPNR', 'SAINT-PHILIPPE-DE-NERI', 'QC', '2015-10-13 19:03:02.931794', '2015-10-13 19:03:02.931794');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20614, 'STPIEGUIRE', 'SAINT-PIE-DE-GUIRE', 'QC', '2015-10-13 19:03:02.939563', '2015-10-13 19:03:02.939563');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20615, 'STPLABTSFD', 'SAINT-PAUL-D''ABBOTSFORD', 'QC', '2015-10-13 19:03:02.945429', '2015-10-13 19:03:02.945429');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20616, 'STPLMNTMNY', 'SAINT-PAUL-DE-MONTMINY', 'QC', '2015-10-13 19:03:02.951327', '2015-10-13 19:03:02.951327');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20617, 'STPOLYCARP', 'SAINT-POLYCARPE', 'QC', '2015-10-13 19:03:02.965208', '2015-10-13 19:03:02.965208');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20618, 'STPRLSBCTS', 'SAINT-PIERRE-LES-BECQUETS', 'QC', '2015-10-13 19:03:02.980329', '2015-10-13 19:03:02.980329');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20619, 'STPRRDRSTR', 'SAINT-PROSPER-DE-DORCHESTER', 'QC', '2015-10-13 19:03:02.991866', '2015-10-13 19:03:02.991866');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20620, 'STPRRWKFLD', 'SAINT-PIERRE-DE-WAKEFIELD', 'QC', '2015-10-13 19:03:02.997524', '2015-10-13 19:03:02.997524');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20621, 'STPTRCBRVG', 'SAINTE-PATRICE-DE-BEAURIVAGE', 'QC', '2015-10-13 19:03:03.012806', '2015-10-13 19:03:03.012806');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20622, 'STPTRONILL', 'SAINTE-PETRONILLE', 'QC', '2015-10-13 19:03:03.024546', '2015-10-13 19:03:03.024546');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20623, 'STRATFORD', 'STRATFORD', 'QC', '2015-10-13 19:03:03.035794', '2015-10-13 19:03:03.035794');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20624, 'STRCHAULNS', 'SAINT-ROCH-DES-AULNAIES', 'QC', '2015-10-13 19:03:03.043557', '2015-10-13 19:03:03.043557');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20625, 'STRCHMKINC', 'SAINT-ROCH-DE-MEKINAC', 'QC', '2015-10-13 19:03:03.048784', '2015-10-13 19:03:03.048784');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20626, 'STRNEMATAN', 'SAINT-RENE-DE-MATANE', 'QC', '2015-10-13 19:03:03.056494', '2015-10-13 19:03:03.056494');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20627, 'STROSDNORD', 'SAINTE-ROSE-DU-NORD', 'QC', '2015-10-13 19:03:03.064163', '2015-10-13 19:03:03.064163');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20628, 'STROSWATFD', 'SAINTE-ROSE-DE-WATFORD', 'QC', '2015-10-13 19:03:03.072284', '2015-10-13 19:03:03.072284');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20629, 'STRPLBLCHS', 'SAINT-RAPHAEL-DE-BELLECHASSE', 'QC', '2015-10-13 19:03:03.080431', '2015-10-13 19:03:03.080431');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20630, 'STSMNRMOSK', 'SAINT-SIMON-DE-RIMOUSKI', 'QC', '2015-10-13 19:03:03.088153', '2015-10-13 19:03:03.088153');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20631, 'STSMONBGOT', 'SAINT-SIMON-DE-BAGOT', 'QC', '2015-10-13 19:03:03.095775', '2015-10-13 19:03:03.095775');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20632, 'STSPHLVRRD', 'SAINTE-SOPHIE-DE-LEVRARD', 'QC', '2015-10-13 19:03:03.103828', '2015-10-13 19:03:03.103828');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20633, 'STSTLSCHLN', 'SAINT-STANISLAS-DE-CHAMPLAIN', 'QC', '2015-10-13 19:03:03.111651', '2015-10-13 19:03:03.111651');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20634, 'STTHEPHILE', 'SAINT-THEOPHILE', 'QC', '2015-10-13 19:03:03.121399', '2015-10-13 19:03:03.121399');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20635, 'STTHMSDAQN', 'SAINT-THOMAS-D''AQUIN', 'QC', '2015-10-13 19:03:03.128935', '2015-10-13 19:03:03.128935');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20636, 'STTITECAPS', 'SAINT-TITE-DES-CAPS', 'QC', '2015-10-13 19:03:03.134846', '2015-10-13 19:03:03.134846');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20637, 'STVCTRBECE', 'SAINT-VICTOR-DE-BEAUCE', 'QC', '2015-10-13 19:03:03.144326', '2015-10-13 19:03:03.144326');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20638, 'STVICTOIRE', 'SAINTE-VICTOIRE', 'QC', '2015-10-13 19:03:03.152373', '2015-10-13 19:03:03.152373');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20639, 'STVNCNTPAL', 'SAINT-VINCENT-DE-PAUL', 'QC', '2015-10-13 19:03:03.160226', '2015-10-13 19:03:03.160226');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20640, 'STWNCESLAS', 'SAINT-WENCESLAS', 'QC', '2015-10-13 19:03:03.167813', '2015-10-13 19:03:03.167813');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20641, 'STZACHARIE', 'SAINT-ZACHARIE', 'QC', '2015-10-13 19:03:03.175563', '2015-10-13 19:03:03.175563');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20642, 'STZEPHIRIN', 'SAINT-ZEPHIRIN', 'QC', '2015-10-13 19:03:03.181378', '2015-10-13 19:03:03.181378');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20643, 'SUTTON', 'SUTTON', 'QC', '2015-10-13 19:03:03.187476', '2015-10-13 19:03:03.187476');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20644, 'TADOUSSAC', 'TADOUSSAC', 'QC', '2015-10-13 19:03:03.192686', '2015-10-13 19:03:03.192686');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20645, 'TASCHEREAU', 'TASCHEREAU', 'QC', '2015-10-13 19:03:03.198743', '2015-10-13 19:03:03.198743');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20646, 'TASIUJAQ', 'TASIUJAQ', 'QC', '2015-10-13 19:03:03.202846', '2015-10-13 19:03:03.202846');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20647, 'TEMISCAMNG', 'TEMISCAMING', 'QC', '2015-10-13 19:03:03.206928', '2015-10-13 19:03:03.206928');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20648, 'TERREBONNE', 'TERREBONNE', 'QC', '2015-10-13 19:03:03.211197', '2015-10-13 19:03:03.211197');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20649, 'TETEBALENE', 'TETE-A-LA-BALEINE', 'QC', '2015-10-13 19:03:03.215301', '2015-10-13 19:03:03.215301');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20650, 'THETFDMINS', 'THETFORD MINES', 'QC', '2015-10-13 19:03:03.221267', '2015-10-13 19:03:03.221267');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20651, 'THURSO', 'THURSO', 'QC', '2015-10-13 19:03:03.235345', '2015-10-13 19:03:03.235345');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20652, 'TINGWICK', 'TINGWICK', 'QC', '2015-10-13 19:03:03.239328', '2015-10-13 19:03:03.239328');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20653, 'TRINGJNCTN', 'TRING-JONCTION', 'QC', '2015-10-13 19:03:03.243296', '2015-10-13 19:03:03.243296');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20654, 'TROISPSTLS', 'TROIS-PISTOLES', 'QC', '2015-10-13 19:03:03.248851', '2015-10-13 19:03:03.248851');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20655, 'TROISRVIRS', 'TROIS-RIVIERES', 'QC', '2015-10-13 19:03:03.254843', '2015-10-13 19:03:03.254843');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20656, 'UMIUJAQ', 'UMIUJAQ', 'QC', '2015-10-13 19:03:03.25888', '2015-10-13 19:03:03.25888');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20657, 'UPTON', 'UPTON', 'QC', '2015-10-13 19:03:03.264538', '2015-10-13 19:03:03.264538');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20658, 'VAL ALAIN', 'VAL-ALAIN', 'QC', '2015-10-13 19:03:03.270928', '2015-10-13 19:03:03.270928');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20659, 'VAL BOIS', 'VAL-DES-BOIS', 'QC', '2015-10-13 19:03:03.27516', '2015-10-13 19:03:03.27516');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20660, 'VAL D OR', 'VAL-D''OR', 'QC', '2015-10-13 19:03:03.279231', '2015-10-13 19:03:03.279231');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20661, 'VAL DAVID', 'VAL-DAVID', 'QC', '2015-10-13 19:03:03.283418', '2015-10-13 19:03:03.283418');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20662, 'VALBRILLNT', 'VAL-BRILLANT', 'QC', '2015-10-13 19:03:03.28882', '2015-10-13 19:03:03.28882');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20663, 'VALCARTIER', 'VALCARTIER', 'QC', '2015-10-13 19:03:03.294919', '2015-10-13 19:03:03.294919');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20664, 'VALCOURT', 'VALCOURT', 'QC', '2015-10-13 19:03:03.299031', '2015-10-13 19:03:03.299031');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20665, 'VALLEEJCTN', 'VALLEE-JONCTION', 'QC', '2015-10-13 19:03:03.303037', '2015-10-13 19:03:03.303037');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20666, 'VALLEYFLD', 'VALLEYFIELD', 'QC', '2015-10-13 19:03:03.307159', '2015-10-13 19:03:03.307159');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20667, 'VARENNES', 'VARENNES', 'QC', '2015-10-13 19:03:03.311166', '2015-10-13 19:03:03.311166');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20668, 'VAUDREUIL', 'VAUDREUIL', 'QC', '2015-10-13 19:03:03.315309', '2015-10-13 19:03:03.315309');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20669, 'VERCHERES', 'VERCHERES', 'QC', '2015-10-13 19:03:03.323462', '2015-10-13 19:03:03.323462');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20670, 'VICTORIAVL', 'VICTORIAVILLE', 'QC', '2015-10-13 19:03:03.329575', '2015-10-13 19:03:03.329575');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20671, 'VILLEMARIE', 'VILLE MARIE', 'QC', '2015-10-13 19:03:03.335453', '2015-10-13 19:03:03.335453');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20672, 'VILLEROY', 'VILLEROY', 'QC', '2015-10-13 19:03:03.341048', '2015-10-13 19:03:03.341048');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20673, 'VL DEGELIS', 'VILLE DEGELIS', 'QC', '2015-10-13 19:03:03.346948', '2015-10-13 19:03:03.346948');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20674, 'VNISENQUBC', 'VENISE-EN-QUEBEC', 'QC', '2015-10-13 19:03:03.350955', '2015-10-13 19:03:03.350955');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20675, 'WAKEFIELD', 'WAKEFIELD', 'QC', '2015-10-13 19:03:03.355035', '2015-10-13 19:03:03.355035');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20676, 'WARWICK', 'WARWICK', 'QC', '2015-10-13 19:03:03.358811', '2015-10-13 19:03:03.358811');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20677, 'WASKAGNISH', 'WASKAGANISH', 'QC', '2015-10-13 19:03:03.364982', '2015-10-13 19:03:03.364982');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20678, 'WASWANIPI', 'WASWANIPI', 'QC', '2015-10-13 19:03:03.371214', '2015-10-13 19:03:03.371214');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20679, 'WATERLOO', 'WATERLOO', 'QC', '2015-10-13 19:03:03.375291', '2015-10-13 19:03:03.375291');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20680, 'WATERVILLE', 'WATERVILLE', 'QC', '2015-10-13 19:03:03.381172', '2015-10-13 19:03:03.381172');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20681, 'WEEDON', 'WEEDON', 'QC', '2015-10-13 19:03:03.387096', '2015-10-13 19:03:03.387096');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20682, 'WEMINDJI', 'WEMINDJI', 'QC', '2015-10-13 19:03:03.392872', '2015-10-13 19:03:03.392872');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20683, 'WICKHAM', 'WICKHAM', 'QC', '2015-10-13 19:03:03.400605', '2015-10-13 19:03:03.400605');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20684, 'WINDSOR', 'WINDSOR', 'QC', '2015-10-13 19:03:03.408096', '2015-10-13 19:03:03.408096');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20685, 'WOBURN', 'WOBURN', 'QC', '2015-10-13 19:03:03.41607', '2015-10-13 19:03:03.41607');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20686, 'WOTTON', 'WOTTON', 'QC', '2015-10-13 19:03:03.423959', '2015-10-13 19:03:03.423959');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20687, 'YAMACHICHE', 'YAMACHICHE', 'QC', '2015-10-13 19:03:03.432098', '2015-10-13 19:03:03.432098');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (20688, 'YAMASKA', 'YAMASKA', 'QC', '2015-10-13 19:03:03.440147', '2015-10-13 19:03:03.440147');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (22312, 'DIR ASST', 'DIRECTORY ASSISTANCE', 'QC', '2015-10-13 19:03:34.09292', '2015-10-13 19:03:34.09292');
INSERT INTO lerg8s (id, "shortName", "fullName", province, created_at, updated_at) VALUES (22313, 'XXXXXXXXXX', 'RATE CENTER NOT APPLICABLE', 'QC', '2015-10-13 19:03:34.100548', '2015-10-13 19:03:34.100548');


--
-- Name: lerg8s_id_seq; Type: SEQUENCE SET; Schema: public; Owner: beaumont_development
--

SELECT pg_catalog.setval('lerg8s_id_seq', 22358, true);


--
-- Name: lerg8s_pkey; Type: CONSTRAINT; Schema: public; Owner: beaumont_development
--

ALTER TABLE ONLY lerg8s
    ADD CONSTRAINT lerg8s_pkey PRIMARY KEY (id);


--
-- Name: lerg8s_id_seq; Type: ACL; Schema: public; Owner: beaumont_development
--

REVOKE ALL ON SEQUENCE lerg8s_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE lerg8s_id_seq FROM beaumont_development;
GRANT ALL ON SEQUENCE lerg8s_id_seq TO beaumont_development;
GRANT ALL ON SEQUENCE lerg8s_id_seq TO sg1;


--
-- PostgreSQL database dump complete
--

