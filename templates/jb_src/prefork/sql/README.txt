sql files to build the LCA XML file

Do levenshtein.sql last .

Description of the files in this folder:

exchanges_manual_matches.sql
Contains the manual matches between LERG files and peer footprint. Matches is by string, not by id, in case ID of LERG6 or peer footprint rate center changes.

lerg8s.sql
Contains the data from LERG8

levenshtein.sql
Contains lavenshtein distances between combinations of North American rate centers and peer footprint

local_exchanges.20151014.sql
List of peer footprints of the client on 2015/10/14

local_exchanges_north_american_exchanges.sql
The final matching of LERG6 entries to peer footprint.  This one matches by ID.

local_exchanges.sql
List of peer footprints of the client. Date unknown.

north_american_exchanges.sql
Contains LERG6 data




jlam has a script on rousseau called loadLCAData.bash that goes like this :

================================================================================
cd /home/jlam/beaumont/sql

for sqlFile in *.sql; do
    bn=`basename $sqlFile`
    if [ "$bn" == "levenshtein.sql" ]; then
        headline="Skipping $sqlFile"
    else 
        headline="Doing $sqlFile"
    fi
    
    headline.bash "$headline"
    
    set -x
    psql -U clientportal -h /home/mcr/clientportaltest/run clientPortal -f $file
    set +x
    
    echo; echo;
done


headline.bash "Now doing levenshtein.sql"

file=levenshtein.sql
set -x
psql -U clientportal -h /home/mcr/clientportaltest/run clientPortal -f $file
set +x
================================================================================




jlam@credil.org
2016/02/10


