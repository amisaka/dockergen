--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.exchanges_manual_matches DROP CONSTRAINT exchanges_manual_matches_pkey;
ALTER TABLE public.exchanges_manual_matches ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.exchanges_manual_matches_id_seq;
DROP TABLE public.exchanges_manual_matches;
SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: exchanges_manual_matches; Type: TABLE; Schema: public; Owner: beaumont_development
--

CREATE TABLE exchanges_manual_matches (
    id integer NOT NULL,
    province character varying(5),
    localname character varying(40),
    lerg6name character varying(10)
);


ALTER TABLE exchanges_manual_matches OWNER TO beaumont_development;

--
-- Name: exchanges_manual_matches_id_seq; Type: SEQUENCE; Schema: public; Owner: beaumont_development
--

CREATE SEQUENCE exchanges_manual_matches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE exchanges_manual_matches_id_seq OWNER TO beaumont_development;

--
-- Name: exchanges_manual_matches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: beaumont_development
--

ALTER SEQUENCE exchanges_manual_matches_id_seq OWNED BY exchanges_manual_matches.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: beaumont_development
--

ALTER TABLE ONLY exchanges_manual_matches ALTER COLUMN id SET DEFAULT nextval('exchanges_manual_matches_id_seq'::regclass);


--
-- Data for Name: exchanges_manual_matches; Type: TABLE DATA; Schema: public; Owner: beaumont_development
--

INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (1, 'QC', 'WASKAGANISH', 'WASKAGNISH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (2, 'AB', 'BEAVERLODGE', 'BEAVERLODG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (3, 'NB', 'PETITCODIAC', 'PETITCODIC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (4, 'NS', 'CHEZZETCOOK', 'CHEZZETCOK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (5, 'NS', 'SOUTHAMPTON', 'SOUTHAMPTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (6, 'ON', 'BISCOTASING', 'BISCOTASNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (7, 'QC', 'BERGERONNES', 'BERGERONNS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (8, 'ON', 'SOUTHAMPTON', 'SOUTHAMPTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (9, 'QC', 'STE THERESE', 'ST THERESE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (10, 'QC', 'BEAUHARNOIS', 'BEAUHARNOS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (11, 'NB', 'FREDERICTON', 'FREDERICTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (12, 'ON', 'HARROWSMITH', 'HARROWSMTH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (13, 'ON', 'PLANTAGENET', 'PLANTAGNET');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (14, 'ON', 'WAUBAUSHENE', 'WAUBAUSHNE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (15, 'QC', 'CHATEAUGUAY', 'CHATEAGUAY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (16, 'QC', 'CHIBOUGAMAU', 'CHIBOUGMAU');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (17, 'ON', 'BRIDGENORTH', 'BRIDGENRTH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (18, 'NB', 'CAMPBELLTON', 'CAMPBELLTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (19, 'QC', 'LAVAL EAST', 'LAVAL EST');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (20, 'QC', 'ST AGATHE', 'STE AGATHE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (21, 'AB', 'FORT MACLEOD', 'FT MACLEOD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (22, 'AB', 'MEDICINE HAT', 'MEDICINHAT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (23, 'AB', 'SPRUCE GROVE', 'SPRUCE GRV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (24, 'BC', 'FORT LANGLEY', 'FT LANGLEY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (25, 'BC', 'GULF ISLANDS', 'GULFISLNDS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (26, 'BC', 'JORDAN RIVER', 'JORDAN RIV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (27, 'BC', 'PORT RENFREW', 'PT RENFREW');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (28, 'NB', 'HILLSBOROUGH', 'HILLSBORGH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (29, 'NB', 'PETIT ROCHER', 'PETITROCHR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (30, 'NL', 'HARBOUR MAIN', 'HARBORMAIN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (31, 'NL', 'LONG HARBOUR', 'LONGHRBOUR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (32, 'NL', 'MUSGRAVETOWN', 'MUSGRAVETN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (33, 'PE', 'MURRAY RIVER', 'MURRAY RIV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (34, 'PE', 'VERNON RIVER', 'VERNON RIV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (35, 'QC', 'BLANC-SABLON', 'BLANCSBLON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (36, 'QC', 'DESCHAILLONS', 'DESCHALLNS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (37, 'QC', 'FRELIGHSBURG', 'FRELIGHSBG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (38, 'QC', 'GRAND-REMOUS', 'GRANDRMOUS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (39, 'QC', 'LAC MEGANTIC', 'LACMGANTIC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (40, 'QC', 'LAC-MEGANTIC', 'LACMGANTIC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (41, 'QC', 'MONT-LAURIER', 'MONTLAURIR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (42, 'QC', 'STE-JULIENNE', 'STJULIENNE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (43, 'QC', 'ST GENEVIEVE', 'STGNEVIEVE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (44, 'QC', 'ST HIPPOLYTE', 'STHIPPOLYT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (45, 'QC', 'ST-HIPPOLYTE', 'STHIPPOLYT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (46, 'QC', 'ST HYACINTHE', 'STHYACINTH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (47, 'QC', 'ST-HYACINTHE', 'STHYACINTH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (48, 'QC', 'ST POLYCARPE', 'STPOLYCARP');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (49, 'QC', 'ST-POLYCARPE', 'STPOLYCARP');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (50, 'QC', 'VAL-BRILLANT', 'VALBRILLNT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (51, 'ON', 'BORDEN ANGUS', 'BORDENANGS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (52, 'ON', 'BORDEN-ANGUS', 'BORDENANGS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (53, 'ON', 'CAMPBELLFORD', 'CAMPBELLFD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (54, 'ON', 'PETERBOROUGH', 'PETERBROGH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (55, 'ON', 'PORT CARLING', 'PT CARLING');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (56, 'ON', 'PORT LAMBTON', 'PT LAMBTON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (57, 'ON', 'WASAGA BEACH', 'WASAGA BCH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (58, 'ON', 'WILLIAMSBURG', 'WILLIAMSBG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (59, 'PE', 'HUNTER RIVER', 'HUNTER RIV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (60, 'QC', 'VALLEYFIELD', 'VALLEYFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (61, 'NS', 'COLLINGWOOD', 'COLLINGWD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (62, 'QC', 'HEMMINGFORD', 'HEMMINGFD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (63, 'QC', 'STE-THERESE', 'ST THERESE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (64, 'NB', 'SPRINGFIELD', 'SPRINGFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (65, 'NS', 'SPRINGFIELD', 'SPRINGFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (66, 'ON', 'SILVERWATER', 'SILVERWTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (67, 'ON', 'GRAVENHURST', 'GRAVENHST');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (68, 'ON', 'MALLORYTOWN', 'MALLORYTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (69, 'NS', 'BRIDGEWATER', 'BRIDGEWTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (70, 'ON', 'AMHERSTBURG', 'AMHERSTBG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (71, 'AB', 'SEVEN PERSONS', 'SEVENPRSNS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (72, 'AB', 'SHERWOOD PARK', 'SHERWOODPK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (73, 'AB', 'TURNER VALLEY', 'TURNER VLY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (74, 'BC', 'PRINCE GEORGE', 'PRINCEGRGE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (75, 'NB', 'SAINT ANDREWS', 'ST ANDREWS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (76, 'NB', 'SAINT ISIDORE', 'ST ISIDORE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (77, 'NB', 'SAINT MARTINS', 'ST MARTINS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (78, 'NB', 'SAINT STEPHEN', 'ST STEPHEN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (79, 'NL', 'ARNOLD''S COVE', 'ARNOLDS CV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (80, 'NL', 'NEWMAN''S COVE', 'NEWMANS CV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (81, 'NL', 'PORTUGAL COVE', 'PORTUGALCV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (82, 'NL', 'TERRENCEVILLE', 'TERRENCEVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (83, 'NS', 'KETCH HARBOUR', 'KETCHHRBOR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (84, 'NS', 'MOUNT UNIACKE', 'MT UNIACKE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (85, 'NS', 'PROSPECT ROAD', 'PROSPECTRD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (86, 'NS', 'SHEET HARBOUR', 'SHEETHRBOR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (87, 'ON', 'BRIGHTS GROVE', 'BRIGHTSGRV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (88, 'ON', 'CAMPBELLVILLE', 'CAMPBELLVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (89, 'ON', 'CONSTANCE BAY', 'CONSTANCBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (90, 'ON', 'DEUX RIVIERES', 'DEUXRIVIRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (91, 'ON', 'DUBREUILVILLE', 'DUBREUILVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (92, 'ON', 'HARRIETSVILLE', 'HARRIETSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (93, 'ON', 'HONEY HARBOUR', 'HONEYHRBOR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (94, 'ON', 'MOUNT BRYDGES', 'MT BRYDGES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (95, 'ON', 'NIAGARA FALLS', 'NIAGARAFLS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (96, 'ON', 'NORTH AUGUSTA', 'NO AUGUSTA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (97, 'ON', 'PLEASANT PARK', 'PLEASANTPK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (98, 'ON', 'PORT COLBORNE', 'PTCOLBORNE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (99, 'ON', 'PORT MCNICOLL', 'PTMCNICOLL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (100, 'ON', 'PORT ROBINSON', 'PTROBINSON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (101, 'ON', 'RICHMOND HILL', 'RICHMONDHL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (102, 'ON', 'SAINT ISIDORE', 'ST ISIDORE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (103, 'ON', 'SEVERN BRIDGE', 'SEVERN BDG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (104, 'ON', 'VANKLEEK HILL', 'VANKLEEKHL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (105, 'PE', 'CHARLOTTETOWN', 'CHARLOTTTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (106, 'PE', 'MOUNT STEWART', 'MT STEWART');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (107, 'QC', 'BERTHIERVILLE', 'BERTHIERVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (108, 'QC', 'BROMPTONVILLE', 'BROMPTONVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (109, 'QC', 'CLARENCEVILLE', 'CLARENCEVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (110, 'QC', 'COTEAU DU LAC', 'COTEUDULAC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (111, 'QC', 'COTEAU-DU-LAC', 'COTEUDULAC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (112, 'QC', 'DRUMMONDVILLE', 'DRUMMONDVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (113, 'QC', 'EAST HEREFORD', 'E HEREFORD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (114, 'QC', 'FORT COULONGE', 'FTCOULONGE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (115, 'QC', 'FORT-COULONGE', 'FTCOULONGE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (116, 'QC', 'LAC-AU-SAUMON', 'LCAUSAUMON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (117, 'QC', 'LAC BOUCHETTE', 'LACBOUCHTT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (118, 'QC', 'LAC-BOUCHETTE', 'LACBOUCHTT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (119, 'QC', 'METABETCHOUAN', 'METABTCHON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (120, 'QC', 'PAPINEAUVILLE', 'PAPINEAUVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (121, 'QC', 'ROCHEBAUCOURT', 'ROCHEBAUCT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (122, 'QC', 'SAINT-MATHIEU', 'ST-MATHIEU');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (123, 'QC', 'ST BARTHELEMY', 'STBARTHLMY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (124, 'QC', 'ST-BARTHELEMY', 'STBARTHLMY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (125, 'QC', 'STE-CATHERINE', 'STE-CATHRN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (126, 'QC', 'STE-GENEVIEVE', 'STGNEVIEVE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (127, 'QC', 'VICTORIAVILLE', 'VICTORIAVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (128, 'ON', 'SAINT EUGENE', 'ST EUGENE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (129, 'ON', 'STONEY CREEK', 'STONEYCRK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (130, 'NL', 'SAINT ALBANS', 'ST ALBANS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (131, 'NL', 'SAINT BRIDES', 'ST BRIDES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (132, 'ON', 'STONEY POINT', 'STONEY PT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (133, 'ON', 'SPENCERVILLE', 'SPENCERVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (134, 'QC', 'LORRAINVILLE', 'LORRAINVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (135, 'QC', 'BOUCHERVILLE', 'BOUCHERVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (136, 'QC', 'SAINT-DAMASE', 'ST-DAMASE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (137, 'NB', 'SAINT GEORGE', 'ST GEORGE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (138, 'QC', 'PORT-CARTIER', 'PT CARTIER');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (139, 'ON', 'WEST LINCOLN', 'W LINCOLN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (140, 'ON', 'STEVENSVILLE', 'STEVENSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (141, 'QC', 'FORTIERVILLE', 'FORTIERVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (142, 'ON', 'CALEDON EAST', 'CALEDON E');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (143, 'QC', 'SOUTH DURHAM', 'SO DURHAM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (144, 'ON', 'CHESTERVILLE', 'CHESTERVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (145, 'QC', 'LORETTEVILLE', 'LORETTEVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (146, 'NL', 'CLARENVILLE', 'CLARENVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (147, 'QC', 'NAPIERVILLE', 'NAPIERVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (148, 'QC', 'COWANSVILLE', 'COWANSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (149, 'NL', 'SAINT MARYS', 'ST MARYS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (150, 'NS', 'CLARKSVILLE', 'CLARKSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (151, 'QC', 'SAWYERVILLE', 'SAWYERVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (152, 'QC', 'PIERREVILLE', 'PIERREVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (153, 'ON', 'BOWMANVILLE', 'BOWMANVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (154, 'ON', 'BRACEBRIDGE', 'BRACEBDG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (155, 'QC', 'FUGEREVILLE', 'FUGEREVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (156, 'BC', 'PORT COQUITLAM', 'PTCOQUITLM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (157, 'BC', 'SOUTH KAMLOOPS', 'SOKAMLOOPS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (158, 'BC', 'WEST VANCOUVER', 'WVANCOUVER');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (159, 'BC', 'WINTER HARBOUR', 'WINTERHRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (160, 'NB', 'HARVEY STATION', 'HARVEY STA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (161, 'QC', 'ST-SIMON', 'ST-SIMEON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (162, 'QC', 'WOLTON', 'WOTTON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (163, 'ON', 'WAREN', 'WARREN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (164, 'NS', 'RIVER HEBERT', 'RIV HEBERT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (165, 'ON', 'ADOLPHUSTOWN', 'ADOLPHUSTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (166, 'NL', 'OLD PERLICAN', 'OLDPRLICAN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (167, 'NS', 'SHUBENACADIE', 'SHUBENCADI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (168, 'NS', 'MUSQUODOBOIT', 'MUSQUDOBOT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (169, 'ON', 'SAUBLE BEACH', 'SAUBLE BCH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (170, 'NS', 'ST MARGARETS', 'STMARGARTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (171, 'QC', 'GRANDE-MERE', 'GRAND MERE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (172, 'ON', 'COLLINGWOOD', 'COLLINGWD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (173, 'ON', 'WALLACEBURG', 'WALLACEBG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (174, 'ON', 'WELLANDPORT', 'WELLANDPT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (175, 'ON', 'VANKLEEK', 'VANKLEEKHL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (176, 'ON', 'AJAX-PICKERING', 'AJAXPCKRNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (177, 'ON', 'ALGONQUIN PARK', 'ALGONQUNPK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (178, 'ON', 'BLEZARD VALLEY', 'BLEZARDVLY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (179, 'ON', 'BRIGHT''S GROVE', 'BRIGHTSGRV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (180, 'ON', 'BURLEIGH FALLS', 'BURLEGHFLS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (181, 'QC', 'L''ANNONCIATION', 'LANNONCTIN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (182, 'ON', 'LITTLE CURRENT', 'LTLCURRENT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (183, 'ON', 'MOUNT PLEASANT', 'MTPLEASANT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (184, 'BC', 'OKANAGAN FALLS', 'OKANGANFLS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (185, 'NL', 'SAINT BRENDANS', 'STBRENDANS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (186, 'QC', 'SAINTE-MARTINE', 'STEMARTINE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (187, 'QC', 'SAINT-FELICIEN', 'STFELICIEN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (188, 'QC', 'SAINT-FULGENCE', 'STFULGENCE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (189, 'NL', 'SAINT LAWRENCE', 'STLAWRENCE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (190, 'QC', 'SAINT-ZEPHIRIN', 'STZEPHIRIN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (191, 'QC', 'TROIS RIVIERES', 'TROISRVIRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (192, 'QC', 'TRING JONCTION', 'TRINGJNCTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (193, 'QC', 'THETFORD MINES', 'THETFDMINS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (194, 'QC', 'CAMPBELL''S BAY', 'CAMPBELSBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (195, 'ON', 'CARLETON PLACE', 'CARLETONPL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (196, 'ON', 'CLARENCE CREEK', 'CLARENCCRK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (197, 'ON', 'STURGEON FALLS', 'STURGENFLS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (198, 'QC', 'STE-PETRONILLE', 'STPTRONILL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (199, 'QC', 'COTEAU LANDING', 'COTEAU LDG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (200, 'QC', 'EAST BROUGHTON', 'EBROUGHTON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (201, 'AB', 'FORT CHIPEWYAN', 'FTCHIPEWYN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (202, 'NS', 'FRENCH VILLAGE', 'FRENCH VLG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (203, 'ON', 'GLEN ROBERTSON', 'GLENRBRTSN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (204, 'AB', 'GRANDE PRAIRIE', 'GRANDEPRRI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (205, 'BC', 'PENDER ISLAND', 'PENDER IS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (206, 'BC', 'CORTES ISLAND', 'CORTES IS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (207, 'NS', 'GREAT VILLAGE', 'GREAT VLG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (208, 'QC', 'LES ESCOUMINS', 'ESCOUMINS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (209, 'QC', 'POINTE CLAIRE', 'PT CLAIRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (210, 'QC', 'STE-MADELEINE', 'STMADLLINE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (211, 'QC', 'BAIE0DES-SABLES', 'BAIESABLES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (212, 'NB', 'BLACK''S HARBOUR', 'BLACKSHRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (213, 'QC', 'BONNE-ESPERANCE', 'BONNEEPRNC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (214, 'BC', 'BRITANNIA BEACH', 'BRITANNBCH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (215, 'QC', 'FRANKLIN CENTRE', 'FRANKLICTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (216, 'BC', 'FULFORD HARBOUR', 'FULFD HRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (217, 'NL', 'HEART''S CONTENT', 'HEARTSCTNT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (218, 'NL', 'HEART''S DELIGHT', 'HEARTSDGHT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (219, 'QC', 'ILE-AUX-COUDRES', 'ILAXCOUDRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (220, 'QC', 'LAC-DES-ECORCES', 'LACECORCES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (221, 'ON', 'POINTE AU BARIL', 'PTAU BARIL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (222, 'QC', 'SAINT-GUILLAUME', 'STGUILLAUM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (223, 'NS', 'SAINT MARGARETS', 'STMARGARTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (224, 'QC', 'SAULT-AU-MOUTON', 'SALTAMOUTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (225, 'ON', 'SOUTH PICKERING', 'SOPICKERNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (226, 'QC', 'ST PIE DE GUIRE', 'STPIEGUIRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (227, 'QC', 'ST-PIE DE GUIRE', 'STPIEGUIRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (228, 'NS', 'UPPER STEWIACKE', 'UPSTEWIACK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (229, 'QC', 'RIVIERE DU LOUP', 'RIVDU LOUP');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (230, 'QC', 'SAINTE-FELICITE', 'STFELICITE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (231, 'QC', 'SAINTE-HENEDINE', 'STHENEDINE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (232, 'QC', 'SAINTE-JULIENNE', 'STJULIENNE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (233, 'QC', 'THERFORD MINES', 'THETFDMINS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (234, 'QC', 'COTEAU-LANDING', 'COTEAU LDG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (235, 'ON', 'HAWK JUNCTION', 'HAWK JCT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (236, 'QC', 'POINTE-CLAIRE', 'PT CLAIRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (237, 'QC', 'SAINTE-THECLE', 'ST THECLE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (238, 'QC', 'VAL-DES-BOIS', 'VAL BOIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (239, 'QC', 'RIVIERE-HEVA', 'RIV HEVA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (240, 'QC', 'ST-TITE DES CAPS', 'STTITECAPS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (241, 'QC', 'BAIE-JOHAN-BEETZ', 'BAJOHNBETZ');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (242, 'ON', 'CHRISTIAN ISLAND', 'CHRISTINIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (243, 'QC', 'CHUTE-DES-PASSES', 'CHUTEPASSS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (244, 'QC', 'MIRABEL AEROPORT', 'MIRABLARPT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (245, 'QC', 'MIRABEL-AEROPORT', 'MIRABLARPT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (246, 'QC', 'SAINTE-CATHERINE', 'STE-CATHRN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (247, 'QC', 'STE-ROSE-DU-NORD', 'STROSDNORD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (248, 'QC', 'ST JEAN DE MATHA', 'STJENMATHA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (249, 'QC', 'ST-JEAN-DE-MATHA', 'STJENMATHA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (250, 'QC', 'VENISE-EN-QUEBEC', 'VNISENQUBC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (251, 'AB', 'FORT MCMURRAY', 'FTMCMURRAY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (252, 'AB', 'PICTURE BUTTE', 'PICTUREBTE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (253, 'QC', 'ST CHRYSOSTOME', 'STCHRYSSTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (254, 'QC', 'LAC-AUX-SABLES', 'LCAXSABLES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (255, 'QC', 'LAC-MISTASSINI', 'LACMSTSSNI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (256, 'QC', 'ST-CHRYSOSTOME', 'STCHRYSSTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (257, 'QC', 'VALLEE-JONCTION', 'VALLEEJCTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (258, 'QC', 'LES EBOULEMENTS', 'LESEBLMNTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (259, 'ON', 'WHITEFISH FALLS', 'WHITESHFLS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (260, 'BC', 'NORTH VANCOUVER', 'NOVANCOUVR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (261, 'BC', 'NEW WESTMINSTER', 'NEWWTMNSTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (262, 'ON', 'PENETANGUISHENE', 'PENETNGSHN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (263, 'BC', 'PITT MEADOWS', 'PITT MDW');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (264, 'ON', 'COLD SPRINGS', 'COLD SPG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (265, 'AB', 'IRON SPRINGS', 'IRON SPG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (266, 'NB', 'YOUNGS COVE ROAD', 'YOUNGSCVRD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (267, 'ON', 'BLUEWATER BEACH', 'BLUEWATER');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (268, 'QC', 'L''ANSE-SAINT-JEAN', 'ANSESTJEAN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (269, 'QC', 'TETE-A-LA-BALEINE', 'TETEBALENE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (270, 'QC', 'ST-THOMAS-D''AQUIN', 'STTHMSDAQN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (271, 'QC', 'RIVIERE BEAUDETTE', 'RIVBEAUDET');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (272, 'AB', 'FORT SASKATCHEWAN', 'FTSASKAHWN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (273, 'QC', 'HARRINGTON HARBOR', 'HARITNHRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (274, 'NL', 'UPPER ISLAND COVE', 'UP IS COVE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (275, 'NL', 'HICKMAN''S HARBOUR', 'HICKMAHRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (276, 'AB', 'SUNCHILD O''CHIESE', 'SNCHLDOCHS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (277, 'QC', 'ST VINCENT DE PAUL', 'STVNCNTPAL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (278, 'QC', 'ST-VINCENT-DE-PAUL', 'STVNCNTPAL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (279, 'QC', 'BAIE-STE-CATHERINE', 'BSTCATHRIN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (280, 'ON', 'KANATA-STITTSVILLE', 'KANATASSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (281, 'ON', 'KANATA STITTSVILLE', 'KANATASSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (282, 'NL', 'LITTLE HEARTS EASE', 'LTLHRTSEAS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (283, 'QC', 'ST-FELIX-DE-VALOIS', 'STFLXVLOIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (284, 'QC', 'ST-ROCH-DE-MEKINAC', 'STRCHMKINC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (285, 'ON', 'KITCHENER-WATERLOO', 'KITCHEWTRL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (286, 'QC', 'ST FELIX DE VALOIS', 'STFLXVLOIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (287, 'QC', 'SAINTE-ANNE-DU-LAC', 'STANNDULAC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (288, 'QC', 'NOTRE-DAME-DU-NORD', 'NTRDAMDNRD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (289, 'QC', 'NOTRE-DAME-DU-LAUS', 'NTRDAMDLAS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (290, 'QC', 'HAVRE-SAINT-PIERRE', 'HVRSTPIRRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (291, 'QC', 'BAIE-STE-CATHERINE', 'STE-CATHRN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (292, 'QC', 'HEBERTVILLE STATION', 'HEBETVLSTA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (293, 'NB', 'GRAND BAY WESTFIELD', 'GRDBYWSFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (294, 'ON', 'NIAGARA ON THE LAKE', 'NIGARAONLK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (295, 'PE', 'MORELL SAINT PETERS', 'MRLLSTPTRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (296, 'QC', 'HEBERTVILLE-STATION', 'HEBETVLSTA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (297, 'QC', 'LEBEL-SUR-QUEVILLON', 'LBLSRQVLLN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (298, 'QC', 'MIRABEL ST-AUGUSTIN', 'MRLSTAGSTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (299, 'QC', 'RIVIERE-AU-TONNERRE', 'RVATONNRRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (300, 'QC', 'SAINT-ANDRE-AVELLIN', 'STANDRAVLN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (301, 'QC', 'SAINTE-ROSE-DU-NORD', 'STROSDNORD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (302, 'QC', 'SAINT-LEON-LE-GRAND', 'STLENLGRND');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (303, 'QC', 'SAINT-TITE-DES-CAPS', 'STTITECAPS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (304, 'QC', 'ST-ADOLPHE-D''HOWARD', 'STADPHDHWD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (305, 'QC', 'STE-ANNE DE BEAUPRE', 'STANNBEPRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (306, 'QC', 'STE-HELENE-DE-BAGOT', 'STHLNEBGOT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (307, 'QC', 'ST FELIX DE KINGSEY', 'STFLXKNGSY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (308, 'AB', 'ROCKY MOUNTAIN HOUSE', 'ROCKYMT HS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (309, 'NB', 'FREDERICTON JUNCTION', 'FREDETNJCT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (310, 'NL', 'ENGLISH HARBOUR EAST', 'ENLSHHRBRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (311, 'NS', 'MUSQUODOBOIT HARBOUR', 'MUSQUTHRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (312, 'QC', 'LA BAIE (CHICOUTIMI)', 'LBCHCOUTMI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (313, 'QC', 'PARC DE LA VERANDRYE', 'PARCVRNDRY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (314, 'QC', 'PARC-DES-LAURENTIDES', 'PARCLRNTDS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (315, 'QC', 'SAINT-RENE-DE-MATANE', 'STRNEMATAN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (316, 'QC', 'ST-DONAT-DE-MONTCALM', 'STDNMNTCLM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (317, 'QC', 'STE-ANNE-DE-PORTNEUF', 'STANNPRTNF');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (318, 'QC', 'ST MICHEL DES SAINTS', 'STMCHLSNTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (319, 'QC', 'ST-MICHEL-DES-SAINTS', 'STMCHLSNTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (320, 'QC', 'STE-BRIGITTE DE LAVAL', 'STBRGTTLVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (321, 'QC', 'STE-JUSTINE-DE-NEWTON', 'STJSTNNWTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (322, 'QC', 'STE-SOPHIE DE LEVRARD', 'STSPHLVRRD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (323, 'QC', 'ST GABRIEL DE BRANDON', 'STGBLBRNDN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (324, 'QC', 'ST-GABRIEL-DE-BRANDON', 'STGBLBRNDN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (325, 'QC', 'ST JEAN ILE D ORLEANS', 'STJNILDORS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (326, 'QC', 'ST-JEAN ILE D''ORLEANS', 'STJNILDORS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (327, 'QC', 'ST JULIE DE VERCHERES', 'STJLVRCHRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (328, 'QC', 'ST-PAUL D''ABBOTTSFORD', 'STPLABTSFD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (329, 'ON', 'ST CATHARINES THOROLD', 'STCTNSTHLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (330, 'ON', 'ST CATHARINES-THOROLD', 'STCTNSTHLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (331, 'QC', 'NOTRE-DAME-DE-LA-PAIX', 'NOTRDAMPAX');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (332, 'QC', 'SAINTE-ANNE-DES-MONTS', 'STANNMONTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (333, 'QC', 'SAINT-NAZAIRE-D''ACTON', 'STNAZARCTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (334, 'QC', 'ST-ALPHONSE-RODRIGUEZ', 'SLPHSRDRGZ');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (335, 'QC', 'ST-CALIXTE DE KILKENNY', 'STCLTKLKNY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (336, 'QC', 'ST SEBASTIEN FRONTENAC', 'SSBSNFRNNC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (337, 'QC', 'ST-PIERRE LES BECQUETS', 'STPRLSBCTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (338, 'QC', 'ST-PIERRE-DE-WAKEFIELD', 'STPRRWKFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (339, 'QC', 'ST PIERRE DE WAKEFIELD', 'STPRRWKFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (340, 'QC', 'ST-FERDINAND-D''HALIFAX', 'STFRNDDHLX');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (341, 'QC', 'STE-MONIQUE DE NICOLET', 'STMONQNCLT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (342, 'QC', 'STE-JULIE-DE-VERCHERES', 'STJLVRCHRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (343, 'QC', 'ST CALIXTE DE KILKENNY', 'STCLTKLKNY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (344, 'QC', 'ST-ADOLPHE DE DUDSWELL', 'STADPHDSWL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (345, 'QC', 'ST ADOLPHE DE DUDSWELL', 'STADPHDSWL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (346, 'QC', 'SAINT-JOSEPH-DE-BEAUCE', 'STJSPHBECE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (347, 'QC', 'SAINT-FELIX-DE-KINGSEY', 'STFLXKNGSY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (348, 'QC', 'SAINT-EMILE-DE-SUFFOLK', 'STEMLSFFLK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (349, 'QC', 'SAINTE-MARIE-DE-BEAUCE', 'STMRIBEUCE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (350, 'QC', 'SAINTE-ANNE-DE-BEAUPRE', 'STANNBEPRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (351, 'QC', 'SAINTE-ANNE-DES-PLAINES', 'STANNPLANS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (352, 'QC', 'MIRABEL: SAINT-AUGUSTIN', 'MRLSTAGSTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (353, 'NL', 'SEAL COVE (FORTUNE BAY)', 'SLCVFRTNBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (354, 'QC', 'SAINT-PAUL-D''ABBOTSFORD', 'STPLABTSFD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (355, 'QC', 'SAINT-GEORGES-DE-BEAUCE', 'STGRGSBECE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (356, 'QC', 'SAINT-EUGENE-DE-GUIGUES', 'STEGNGUGUS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (357, 'QC', 'L''EPIPHANIE-L''ASSOMPTION', 'LEPNLASPTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (358, 'QC', 'SAINTE-ANNE-DE-LA-PERADE', 'STANPERADE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (359, 'QC', 'SAINTE-BRIGITTE-DE-LAVAL', 'STBRGTTLVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (360, 'QC', 'SAINTE-JUSTINE-DE-NEWTON', 'STJSTNNWTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (361, 'QC', 'SAINTE-SOPHIE-DE-LEVRARD', 'STSPHLVRRD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (362, 'QC', 'SAINT-MARC-DES-CARRIERES', 'STMRCCRRRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (363, 'QC', 'ST ALPHONSE DE RODRIGUEZ', 'SLPHSRDRGZ');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (364, 'QC', 'MIRABEL STE-SCHOLASTIQUE', 'MRSTSCLSTQ');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (365, 'QC', 'NOTRE-DAME-DE-LA-SALETTE', 'NTRDMSALTT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (366, 'ON', 'OTTAWA', 'OTTAWAHULL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (367, 'QC', 'ST-AMBROISE-DE-CHICOUTIMI', 'STAMRSCHTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (368, 'QC', 'PETIE-RIVIERE-ST-FRANCOIS', 'PTRVSTFRCS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (369, 'QC', 'NOTRE-DAME DES LAURENTIDES', 'NTDMLRNTDS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (370, 'QC', 'SAINT-BERNARD-DE-DORCHESTER', 'STBRDDRSTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (371, 'QC', 'SAINTE-AGATHE-DE-LOTBINIERE', 'STAGHLTBNR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (372, 'QC', 'SAINT-PATRICE-DE-BEAURIVAGE', 'STPTRCBRVG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (373, 'QC', 'RIVIERE-SAINT-JEAN', 'RIVST JEAN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (374, 'QC', 'SAINT-BONIFACE-DE-SHAWINIGAN', 'STBNCSHNGN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (375, 'QC', 'SAINT-SEBASTIEN-DE-FRONTENAC', 'SSBSNFRNNC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (376, 'QC', 'STE-ANNE-DES-PLAINES', 'STANNPLANS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (377, 'QC', 'ROUYN-NORANDA', 'NORANDA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (378, 'QC', 'LA BAIE', 'LBCHCOUTMI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (379, 'QC', 'ST-HONORE', 'STHNRCHCTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (380, 'QC', 'SAINT-STANISLAS', 'STSTLSCHLN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (381, 'QC', 'HULL', 'OTTAWAHULL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (382, 'QC', 'PORT-MENIER', 'PTMNRNTCSS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (383, 'QC', 'SAINT-AUGUSTIN', 'STAGTNDPLS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (384, 'QC', 'ST-JEAN-SUR-RICHELIEU', 'ST JEAN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (385, 'QC', 'BEARN', 'BEARNTCMNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (386, 'QC', 'EVAIN', 'NORANDA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (387, 'QC', 'LACERLOCHERE', 'LAVERLOCHR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (388, 'QC', 'MISTISSINI', 'LACMSTSSNI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (389, 'QC', 'ROLLET', 'NORANDA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (390, 'QC', 'SAINT-BRUNO-DE-GUIGUES', 'GUIGUES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (391, 'NL', 'FRESHWATER (PLACENTIA BAY)', 'FRWTRPLTBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (392, 'NL', 'LITTLE HARBOUR EAST (PLACENTIA BAY)', 'CHANCECOVE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (393, 'QC', 'MIRABEL: SAINTE-SCHOLASTIQUE', 'MRSTSCLSTQ');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (394, 'QC', 'NOTRE DAME DES LAURENTIDES', 'NTDMLRNTDS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (395, 'QC', 'SAINT-AMBROISE-DE-CHICOUTIMI', 'STAMRSCHTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (396, 'QC', 'SAINT-CYRILLE-DE-WENDOVER', 'STCYLWNDVR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (397, 'QC', 'SAINTE-MADELEINE', 'STMADLLINE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (398, 'QC', 'SAINTE-MONIQUE-DE-NICOLET', 'STMONQNCLT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (399, 'QC', 'SAINT-GERMAIN-DE-GRANTHAM', 'STGRNGRTHM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (400, 'QC', 'SAINT-HONORE (CHICOUTIMI)', 'STHNRCHCTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (401, 'QC', 'SAINT-PIERRE-LES-BECQUETS', 'STPRLSBCTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (402, 'AB', 'SASKATCHEWAN RIVER CROSSING', 'SKHWNRVCRG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (403, 'QC', 'ARNTFIELD', 'NORANDA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (404, 'AB', 'MACKAY', 'NITON JCT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (405, 'QC', 'SAINT-THEODORE-DE-CHERTSEY', 'RAWDON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (406, 'NL', 'CHARLOTTETOWN (BONAVISTA BAY)', 'CHTNBNSTBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (407, 'BC', 'TUMBLER RIDGE', 'TUMBLERRDG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (408, 'BC', 'HUDSON''S HOPE', 'HUDSOSHOPE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (409, 'MB', 'POPLAR POINT', 'POPLAR PT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (410, 'AB', 'GRAND CENTRE', 'GRAND CTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (411, 'ON', 'SILVER WATER', 'SILVERWTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (412, 'ON', 'MOUNT FOREST', 'MOUNT FOR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (413, 'AB', 'LAC LA BICHE', 'LAC BICHE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (414, 'AB', 'FORT VERMILION', 'FTVERMILIN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (415, 'BC', 'CHRISTINA LAKE', 'CHRISTINLK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (416, 'AB', 'CHIPEWYAN LAKE', 'CHIPEWYNLK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (417, 'AB', 'CROWSNEST PASS', 'CROWSNPASS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (418, 'AB', 'WANDERING RIVER', 'WANDENGRIV');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (419, 'AB', 'PARADISE VALLEY', 'PARADISVLY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (420, 'BC', 'OKANAGAN MISSION', 'OKANGNMSSN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (421, 'BC', 'LAKEVIEW HEIGHTS', 'LAKEVW HTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (422, 'ON', 'SHAKESPEARE', 'SHAKESPERE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (423, 'AB', 'MANYBERRIES', 'MANYBERRIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (424, 'MB', 'CRYSTAL CITY', 'CRYSTAL CY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (425, 'AB', 'GRANDE CACHE', 'GRANDECACH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (426, 'AB', 'HIGH PRAIRIE', 'HIGHPRAIRI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (427, 'BC', 'PORT COQUILTHAM', 'PTCOQUITLM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (428, 'AB', 'JEAN D’OR PRAIRIE', 'JENDPRAIRI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (429, 'MB', 'ST FRANCOIS XAVIER', 'STFRNCSXVR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (430, 'ON', 'NIAGARA-ON-THE-LAKE', 'NIGARAONLK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (431, 'AB', 'LITTLE BUFFALO LAKE', 'LTLBFFLOLK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (432, 'ON', 'KANATA - STITTSVILLE', 'KANATASSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (433, 'PQ', 'GRENVILLE', 'GRENVILLE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (434, 'QC', 'GRAND-MèRE', 'GRAND MERE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (435, 'QC', 'LéVIS', 'LEVIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (436, 'QC', 'ÎLE-PERROT', 'ILE PERROT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (437, 'QC', 'TROIS-RIVIèRES', 'TROISRVIRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (438, 'QC', 'MéTABETCHOUAN', 'METABTCHON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (439, 'QC', 'SAINT-HYACINTHE', 'STHYACINTH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (440, 'QC', 'SAINT-POLYCARPE', 'STPOLYCARP');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (441, 'QC', 'SAINT-HIPPOLYTE', 'STHIPPOLYT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (442, 'QC', 'COWMANSVILLE', 'COWANSVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (443, 'QC', 'HéBERTVILLE', 'HEBERTVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (444, 'QC', 'MORIN-HEIGHTS', 'MORIN HTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (445, 'QC', 'SAINT-ANDRé EST', 'STANDREEST');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (446, 'QC', 'RIVIèRE-BEAUDETTE', 'RIVBEAUDET');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (447, 'QC', 'SAINTE-PéTRONILLE', 'STPTRONILL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (448, 'QC', 'SAINTE-MARGUERITE', 'STMARGURIT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (449, 'QC', 'LES CèDRES', 'LES CEDRES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (450, 'QC', 'LATERRIèRE', 'LATERRIERE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (451, 'QC', 'MASKINONGé', 'MASKINONGE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (452, 'QC', 'BOISCHâTEL', 'BOISCHATEL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (453, 'QC', 'JONQUIèRE', 'JONQUIERE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (454, 'QC', 'VERCHèRES', 'VERCHERES');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (455, 'QC', 'MONTRéAL', 'MONTREAL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (456, 'QC', 'LAC-MéGANTIC', 'LACMGANTIC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (457, 'QC', 'QUéBEC', 'QUEBEC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (458, 'AB', 'LLOYDMINSTER', 'LOYDMINSTR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (459, 'QC', 'CHâTEAUGUAY', 'CHATEAGUAY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (460, 'QC', 'MIRABLE-AéROPORT', 'MIRABLARPT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (461, 'QC', 'SAINT-BARTHéLEMY', 'STBARTHLMY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (462, 'QC', 'SAINT-CéSAIRE', 'ST CESAIRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (463, 'QC', 'SAINT-CHRYSOSTOME', 'STCHRYSSTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (464, 'QC', 'SAINTE-ADèLE', 'STE ADELE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (465, 'QC', 'SAINTE-GENEVIèVE', 'STGNEVIEVE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (466, 'MB', 'PORTAGE LA PRAIRIE', 'PORTAGPRRI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (467, 'QC', 'SAINT-ZéNON', 'ST ZENON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (468, 'QC', 'SAINTE-THéRèSE', 'ST THERESE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (469, 'QC', 'SAINT-JEAN-DE-MARTHA', 'STJENMATHA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (470, 'QC', 'SAINT-RéMI', 'ST REMI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (471, 'QC', 'SAINT-JéRôME', 'ST JEROME');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (472, 'QC', 'SAINT-GéDéON', 'ST GEDEON');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (473, 'QC', 'SAINT-FéLIX-DE-VALOIS', 'STFLXVLOIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (474, 'QC', 'SAINT-VINCENT-DE-PAUL', 'STVNCNTPAL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (475, 'QC', 'HéBERTVILLE-STATION', 'HEBETVLSTA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (476, 'QC', 'SAINT-ADOLPHE-D''HOWARD', 'STADPHDHWD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (477, 'QC', 'SAINT-FéLIX-DE-KINGSEY', 'STFLXKNGSY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (478, 'QC', 'MIRABEL-SAINT-AUGUSTIN', 'MRLSTAGSTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (479, 'QC', 'SAINT-MICHEL-DES-SAINTS', 'STMCHLSNTS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (480, 'QC', 'SAINT-DONAT-DE-MONTCALM', 'STDNMNTCLM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (481, 'QC', 'SAINT-GABRIEL-DE-BRANDON', 'STGBLBRNDN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (482, 'QC', 'SAINTE-SOPHIE-DE-LéVRARD', 'STSPHLVRRD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (483, 'QC', 'SAINTE-ANNE-DE-BEAUPRé', 'STANNBEPRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (484, 'QC', 'SAINT-CALIXTE-DE-KILKENNY', 'STCLTKLKNY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (485, 'QC', 'SAINT-ADOLPHE-DE-DUDSWELL', 'STADPHDSWL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (486, 'QC', 'SAINT-PIERRE-DE-WAKEFIELD', 'STPRRWKFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (487, 'QC', 'SAINTE-JULIE-DE-VERCHèRES', 'STJLVRCHRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (488, 'QC', 'SAINT-JEAN-I''ÎLE-D''ORLéANS', 'STJNILDORS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (489, 'QC', 'MIRABEL-SAINTE-SCHOLASTIQUE', 'MRSTSCLSTQ');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (490, 'QC', 'L''ÉPIPHANIE-L''ASSOMPTION', 'LEPNLASPTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (491, 'QC', 'SAINT-ALPHONSE-DE-RODRIGUEZ', 'SLPHSRDRGZ');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (492, 'QC', 'SAINT-SéBASTIEN-DE-FRONTENAC', 'SSBSNFRNNC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (493, 'ON', 'MERLBOURNE', 'MELBOURNE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (494, 'ON', 'TRAVISTOCK', 'TAVISTOCK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (495, 'ON', 'ST ISIDORE DE PRESCOTT', 'ST ISIDORE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (496, 'QC', 'SAINTE-AGATHE-DES-MONTS', 'STE AGATHE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (497, 'QC', 'NOTRE-DAME-DES-LAURENTIDES', 'NTDMLRNTDS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (498, 'QC', 'SAINT-FERRéOL-LES-NEIGES', 'ST FEREOL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (499, 'QC', 'SAINT-AMBROISE', 'STAMRSCHTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (500, 'QC', 'SAINT-HONORé', 'STHNRCHCTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (501, 'AB', 'HARDISTRY', 'HARDISTY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (502, 'AB', 'OKOTOS', 'OKOTOKS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (503, 'MB', 'CHARLESWOOD', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (504, 'MB', 'GATEWAY', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (505, 'MB', 'FORT ROUGE', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (506, 'MB', 'NORWOOD', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (507, 'MB', 'NOTRE DAME DE LOURDES', 'NTRDMLORDS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (508, 'MB', 'ST. JOHN', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (509, 'MB', 'TRANSCONA', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (510, 'QC', 'SAINT-FLUGENCE', 'STFULGENCE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (511, 'AB', 'EDMONTON INTERNATIONAL AIRPORT', 'EDNINTNLAR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (512, 'MB', 'ALPINE', 'SWAN RIVER');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (513, 'MB', 'INKSTER PARK', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (514, 'MB', 'NIAGARA', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (515, 'MB', 'PEMBINA', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (516, 'MB', 'SHERBROOK', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (517, 'MB', 'EDISON', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (518, 'MB', 'MAIN AND WHITEHALL', 'WINNIPEG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (519, 'NS', 'BOULARDERIE', 'BOULARDRIE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (520, 'NS', 'GUYSBOROUGH', 'GUYSBOROGH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (521, 'NS', 'SALTSPRINGS', 'SALTSPRIGS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (522, 'NS', 'WHYCOCOMAGH', 'WHYCOCOMGH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (523, 'NT', 'YELLOWKNIFE', 'YELLOWKNIF');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (524, 'ON', 'KAPUSKASING', 'KAPUSKASNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (525, 'ON', 'SHEBANDOWAN', 'SHEBANDOWN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (526, 'QC', 'PéRIBONKA', 'PERIBONKA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (527, 'NS', 'CHéTICAMP', 'CHETICAMP');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (528, 'QC', 'NORMéTAL', 'NORMETAL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (529, 'NB', 'LAMèQUE', 'LAMEQUE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (530, 'QC', 'NéMASKA', 'NEMASKA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (531, 'QC', 'CLéRICY', 'CLERICY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (532, 'QC', 'NéDELEC', 'NEDELEC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (533, 'QC', 'RéMIGNY', 'REMIGNY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (534, 'QC', 'LA DORé', 'LA DORE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (535, 'QC', 'LAVERLOCHèRE', 'LAVERLOCHR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (536, 'BC', 'HUDSONS HOPE', 'HUDSOSHOPE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (537, 'NB', 'PETIT-ROCHER', 'PETITROCHR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (538, 'NB', 'PLASTER ROCK', 'PLASTER RK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (539, 'NS', 'TATAMAGOUCHE', 'TATAMGOUCH');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (540, 'NT', 'TSIIGEHTCHIC', 'TSIGEHTCHC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (541, 'ON', 'NEW LISKEARD', 'NEWLISKERD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (542, 'QC', 'ST-FéLICIEN', 'STFELICIEN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (543, 'QC', 'TéMISCAMING', 'TEMISCAMNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (544, 'QC', 'LAC-ÉDOUARD', 'LACEDOUARD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (545, 'NB', 'ST-LéONARD', 'ST LEONARD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (546, 'NS', 'MARION BRIDGE', 'MARION BDG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (547, 'NS', 'NEW WATERFORD', 'NEWWATERFD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (548, 'NS', 'WOODS HARBOUR', 'WD HARBOUR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (549, 'ON', 'SIOUX LOOKOUT', 'SIOUXLOKOT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (550, 'NB', 'BAIE-STE-ANNE', 'BAIESTANNE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (551, 'NB', 'PERTH-ANDOVER', 'PERTHANDVR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (552, 'NS', 'GRAND NARROWS', 'GRAND NRWS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (553, 'ON', 'DEUX-RIVIèRES', 'DEUXRIVIRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (554, 'ON', 'KAMINISTIQUIA', 'KAMINSTQUI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (555, 'ON', 'SIOUX NARROWS', 'SIOUX NRWS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (556, 'ON', 'BEARS PASSAGE', 'BEARSPSSAG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (557, 'ON', 'ALGOMA MILLS', 'ALGOMA ML');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (558, 'ON', 'LAC LA CROIX', 'LAC CROIX');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (559, 'NB', 'CAP-PELé', 'CAP PELE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (560, 'NL', 'COME BY CHANCE', 'COMBYCHANC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (561, 'BC', 'SPENCES BRIDGE', 'SPENCESBDG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (562, 'BC', '﻿70 MILE HOUSE', '70 MILE HS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (563, 'NB', 'BLACKS HARBOUR', 'BLACKSHRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (564, 'NS', 'LAKE CHARLOTTE', 'LKCHARLOTT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (565, 'ON', 'BATCHAWANA BAY', 'BATCHAWNBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (566, 'QC', 'TRING-JONCTION', 'TRINGJNCTN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (567, 'YT', 'PELLY CROSSING', 'PELLYCRSNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (568, 'BC', 'GRASSY PLAINS', 'GRASSY PL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (569, 'SK', 'FORT QU''APPELLE', 'FTQAPPELLE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (570, 'NS', 'COUNTRY HARBOUR', 'COUNTRHRBR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (571, 'NS', 'PORT HAWKESBURY', 'PTHAWKESBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (572, 'ON', 'SAULT STE MARIE', 'SALTSTMARI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (573, 'ON', 'SOUTH PORCUPINE', 'SOPORCUPIN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (574, 'NS', 'MILL VILLAGE', 'MILL VLG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (575, 'YT', 'HAINES JUNCTION', 'HAINES JCT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (576, 'NL', 'PLATE COVE EAST', 'PLATE CV E');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (577, 'NS', 'ANNAPOLIS ROYAL', 'ANAPLSROYL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (578, 'QC', 'SAINT-ZéPHIRIN', 'STZEPHIRIN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (579, 'QC', 'SAINT-JEAN-DE-MATHA', 'STJENMATHA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (580, 'QC', 'LEBEL-SUR-QUéVILLON', 'LBLSRQVLLN');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (581, 'NL', 'LITTLE HEART''S EASE', 'LTLHRTSEAS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (582, 'QC', 'RIVIèRE-HéVA', 'RIV HEVA');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (583, 'QC', 'PARC DE LA VERENDRYE', 'PARCVRNDRY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (584, 'QC', 'FUGèREVILLE', 'FUGEREVL');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (585, 'NB', 'ST-LOUIS-DE-KENT', 'STLOUSKENT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (586, 'QC', 'MIRABEL-AéROPORT', 'MIRABLARPT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (587, 'PE', 'MORELL-ST PETERS', 'MRLLSTPTRS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (588, 'NB', 'YOUNG''S COVE ROAD', 'YOUNGSCVRD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (589, 'AB', 'JEAN D''OR PRAIRIE', 'JENDPRAIRI');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (590, 'QC', 'SAINT-PIE-DE-GUIRE', 'STPIEGUIRE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (591, 'NS', 'UPPER MUSQUODOBOIT', 'UPMUSQUDBT');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (592, 'ON', 'MCDONALD''S CORNERS', 'MCDONASCOR');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (593, 'QC', 'SAINT-RéGIS', 'ST REGIS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (594, 'NB', 'TRACADIE-SHEILA', 'TRACADIE');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (595, 'NB', 'GRAND BAY-WESTFIELD', 'GRDBYWSFLD');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (596, 'QC', 'ST-EUGèNE-DE-GUIGUES', 'STEGNGUGUS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (597, 'NB', 'STE-ANNE-DE-MADAWASKA', 'STANNMDWSK');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (598, 'QC', 'SAINT-FERDINAND-D''HALIFAX', 'STFRNDDHLX');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (599, 'QC', 'SAINT-HONORé (CHICOUTIMI)', 'STHNRCHCTM');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (600, 'QC', 'SAINT-JEAN-DE-L''ÎLE-D''ORLéANS', 'STJNILDORS');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (601, 'QC', 'BéARN', 'BEARNTCMNG');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (602, 'NL', 'FRESHWATER', 'FRWTRPLTBY');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (603, 'QC', 'NOTRE-DAME-DE-LOURDES', 'NRDMLDSMGC');
INSERT INTO exchanges_manual_matches (id, province, localname, lerg6name) VALUES (604, 'QC', 'ST-BRUNO-DE-GUIGUES', 'GUIGUES');


--
-- Name: exchanges_manual_matches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: beaumont_development
--

SELECT pg_catalog.setval('exchanges_manual_matches_id_seq', 604, true);


--
-- Name: exchanges_manual_matches_pkey; Type: CONSTRAINT; Schema: public; Owner: beaumont_development
--

ALTER TABLE ONLY exchanges_manual_matches
    ADD CONSTRAINT exchanges_manual_matches_pkey PRIMARY KEY (id);


--
-- Name: exchanges_manual_matches; Type: ACL; Schema: public; Owner: beaumont_development
--

REVOKE ALL ON TABLE exchanges_manual_matches FROM PUBLIC;
REVOKE ALL ON TABLE exchanges_manual_matches FROM beaumont_development;
GRANT ALL ON TABLE exchanges_manual_matches TO beaumont_development;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE exchanges_manual_matches TO sg1;


--
-- Name: exchanges_manual_matches_id_seq; Type: ACL; Schema: public; Owner: beaumont_development
--

REVOKE ALL ON SEQUENCE exchanges_manual_matches_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE exchanges_manual_matches_id_seq FROM beaumont_development;
GRANT ALL ON SEQUENCE exchanges_manual_matches_id_seq TO beaumont_development;
GRANT ALL ON SEQUENCE exchanges_manual_matches_id_seq TO sg1;


--
-- PostgreSQL database dump complete
--

