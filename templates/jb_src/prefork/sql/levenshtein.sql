-- Lavenstein for non matching strings

drop table lavenshtein_exchanges;
create table lavenshtein_exchanges as 
select distinct l.province, l.name as LocalName, na.name as LergName, levenshtein(l.name, na.name) as lev 
from local_exchanges l, north_american_exchanges na  
where l.province = na.province                      
and levenshtein(l.name, na.name) < 10
and (l.province, l.name) 
      not in (select distinct province, name from north_american_exchanges) 
order by lev, l.name
