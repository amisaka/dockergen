#!/bin/bash

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source "$CURRENT_PATH/settings.sh"

echo "Copying static resources to expected html output folder"

mkdir -p "$OUTPUT_PATH/resources"
cp "$GIT_ROOT_PATH/images/"* "$OUTPUT_PATH/resources"

