#!/bin/bash

# create xml summarizing the services to which a client subscribes

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source "$CURRENT_PATH/settings.sh"

if [ ! -d "$USER_PATH" ]; then
	echo "Folder '"$USER_PATH"' does not exist"
	exit 1
fi

USERS=($(ls --ignore-backups "$USER_PATH"))

echo "*** Cleaning charge summary ***"

for ((i = 0; i < ${#USERS[@]}; i++)) ; do

	CUSTOMER_ID=$(basename "${USERS[$i]}")

	OUTPUT_PATH="$USER_PATH/$CUSTOMER_ID/$XML_FOLDER"

	echo "Customer id '"$CUSTOMER_ID"'"
	echo "Input path '"$OUTPUT_PATH/$SERVICES_FILENAME"'"
	echo "Output path '"$OUTPUT_PATH/$CHARGES_FILENAME"'"

	mkdir -p "$OUTPUT_PATH"

	saxonb-xslt -o:"$OUTPUT_PATH/$CHARGES_FILENAME" -xsl:"$XSLT_PATH/clean-charge-summary.xsl" -s:"$OUTPUT_PATH/$SERVICES_FILENAME" previousChargesUrl="$OUTPUT_PATH/$PREVIOUS_CHARGES_FILENAME"

done

