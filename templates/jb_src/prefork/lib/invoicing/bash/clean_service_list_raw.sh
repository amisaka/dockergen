#!/bin/bash

# take the raw format outputed by the pdf data extraction and convert it in the pdf creator's input format

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source "$CURRENT_PATH/settings.sh"

if [ ! -d "$CUSTOMERS_RAW_PATH" ]; then
	echo "Folder '"$CUSTOMERS_RAW_PATH"' does not exist"
	exit 1
fi

FILES=($(ls --ignore-backups "$CUSTOMERS_RAW_PATH"/*.xml))

echo "*** Cleaning service list raw ***"

for ((i = 0; i < ${#FILES[@]}; i++)) ; do

	FILENAME=$(basename "${FILES[$i]}")
	CUSTOMER_ID="${FILENAME%.*}"

	echo "Customer id '"$CUSTOMER_ID"'"
	echo "Input path '"$SERVICES_RAW_PATH/$CUSTOMER_ID.xml"'"
	echo "Output (temporary) path '"$TEMP_PATH/$SERVICES_FILENAME"'"

	mkdir -p "$TEMP_PATH"

	saxonb-xslt -o:"$TEMP_PATH/$SERVICES_FILENAME" -xsl:"$XSLT_PATH/clean-service-list-raw.xsl" -s:"$SERVICES_RAW_PATH/$CUSTOMER_ID.xml"

done

