#!/bin/bash

# Remove software required to process pdfs
# This script assumes an Ubuntu 14.04 system and will not work on other distributions.

DISTRIBUTION=$(lsb_release -a 2>&1 | grep "Distributor ID:" | awk '{print $3}')
VERSION=$(lsb_release -a 2>&1 | grep "Release:" | awk '{print $2}')

if [[ "$DISTRIBUTION" != "Ubuntu" ]] || [[ "$VERSION" != "14.04" ]]; then
	echo "This uninstaller was created for the Ubuntu 14.04 distribution."
	echo "Please configure the '"$0"' script your environment."
fi


# REMOVE REQUIRED APPLICATIONS


# purge nodejs
sudo unlink /usr/bin/node
sudo apt-get purge --yes nodejs npm libjs-node-uuid
sudo rm -rf /usr/local/lib/node_modules
rm -rf ~/.nodejs/


# purge java and saxon
sudo apt-get purge --yes default-jre libsaxonb-java


# remove any remaining unused packages
sudo apt-get autoremove --yes

