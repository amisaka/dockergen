# Create test data.

# Data from the application data source should not require this procedure.
# Converts xml in an intermediate format that is extracted from pdfs
# and converts it to the format required by the pdf creator.

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

bash "$CURRENT_PATH/clean_invoice.sh"

bash "$CURRENT_PATH/clean_service_list_raw.sh"

bash "$CURRENT_PATH/clean_customer_list.sh"
bash "$CURRENT_PATH/clean_service_list.sh"
bash "$CURRENT_PATH/clean_previous_charge_summary.sh"
bash "$CURRENT_PATH/clean_charge_summary.sh"

bash "$CURRENT_PATH/clean_line_summary.sh"
bash "$CURRENT_PATH/clean_call_summary.sh"
bash "$CURRENT_PATH/clean_area_code_summary.sh"
