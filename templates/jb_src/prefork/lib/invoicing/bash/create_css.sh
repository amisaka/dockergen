#!/bin/bash

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source "../../config/invoicing/settings.sh"

echo "Processing SASS to create CSS"

export PATH=$HOME/.nodejs/bin:$PATH
mkdir -p "resources"

# create css, only one of the 2 following commands are needed
# 1) create readable css output file
node-sass --include-path ~/.nodejs "style/invoice.scss" $(pwd)/"resources/invoice.css"

# 2) create compressed css output file
#node-sass --prefix ~/.nodejs --output-style compressed "$GIT_ROOT_PATH/style/invoice.scss" "$OUTPUT_PATH/resources/invoice.css"

