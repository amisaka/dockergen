# Language specific settings for English.
# These variables specify values that cannot be set through the html content.

# title displayed by the pdf reader
TITLE="IP4B Inc. Invoice"
# invoice footer text
FOOTER_RIGHT_TEXT="Page [page] of [topage]"
FOOTER_CENTER_TEXT="IP4B Inc. 9600, boul. du Golf, Montreal (Quebec) H1J 3A8"
