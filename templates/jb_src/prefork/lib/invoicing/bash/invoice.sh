#!/bin/bash

# set -x
# Creates an invoice for each user that has a home folder under "USER_PATH".
# The folder name is used as the customer id.
# The file paths and names used in creation are defined in "settings.sh".
# Paths are URIs, example valid formats include "file://mydatasource/" or "https://mydatasource/"


if [ "$#" -ne 1 ]; then
    echo "Requires one parameter, either:"
    echo "'en' to create English invoices"
    echo "'fr' to create French invoices"
	exit 1
fi

if ! which saxonb-xslt ; then
    echo "Requires saxonb-xslt to be installed"
    exit 1
fi

if ! which wkhtmltopdf ; then
    echo "Requires wkhtmltopdf to be installed"
    exit 1
fi

LANGUAGE="$1"

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

# import language specific configuration variables
echo "Sourcing $CURRENT_PATH/invoice_settings_$LANGUAGE.sh"
source "$CURRENT_PATH/invoice_settings_$LANGUAGE.sh"

echo "Sourcing $CURRENT_PATH/settings.sh"
source "$CURRENT_PATH/settings.sh"

echo "Creating required folders"

mkdir -p "$OUTPUT_PATH/$HTML_FOLDER"
mkdir -p "$OUTPUT_PATH/$PDF_FOLDER"

echo "Getting file list"

USERS=($(ls --ignore-backups "$USER_PATH"))

echo "Found "${#USERS[@]}" users"

# As per 2018/05/01, seems there needs to be a list of users in sge/prefork/lib/data/user
for ((i = 0; i < ${#USERS[@]}; i++)) ; do

	CUSTOMER_ID=$(basename "${USERS[$i]}")

	URL_ENCODED_CUSTOMER_ID=$(echo "$CUSTOMER_ID" | sed -r 's/\+/%2B/g')

	echo "Processing '"$CUSTOMER_ID"'"

	# get the payee organization path
	# examples of the format is available under "xml/organization"
	# localizations follow the convention of having the language code appended,
	# ex. "default_en.xml" or "alternate_en.xml"
	#PAYMENT_URL=""
	#if [ -f "$DATA_PATH/$PAYMENT_FOLDER/$CUSTOMER_ID.xml" ]; then
	#	PAYMENT_URL="$DATA_PATH/$PAYMENT_FOLDER/$CUSTOMER_ID.xml"
	#fi

        # set -x
	# get the customer base path
	CUSTOMER_BASE_URL=""
	if [ -d "$USER_PATH/$CUSTOMER_ID" ]; then
		CUSTOMER_BASE_URL="$USER_PATH/$CUSTOMER_ID"
	fi

	# get the customer invoice path
	INVOICE_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$INVOICE_FILENAME" ]; then
		INVOICE_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$INVOICE_FILENAME"
	fi

	# get the customer path
	CUSTOMER_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$CUSTOMER_FILENAME" ]; then
		CUSTOMER_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$CUSTOMER_FILENAME"
	fi

	# get the customer services path
	SERVICES_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$SERVICES_FILENAME" ]; then
		SERVICES_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$SERVICES_FILENAME"
	fi

	# get the customer charges path
	CHARGES_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$CHARGES_FILENAME" ]; then
		CHARGES_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$CHARGES_FILENAME"
	fi

	# get the customer previous charges path
	PREVIOUS_CHARGES_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$PREVIOUS_CHARGES_FILENAME" ]; then
		PREVIOUS_CHARGES_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$PREVIOUS_CHARGES_FILENAME"
	fi

	# get the customer lines path
	LINES_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$LINES_FILENAME" ]; then
		LINES_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$LINES_FILENAME"
	fi

	# get the call path
	CALLS_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$CALLS_FILENAME" ]; then
		CALLS_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$CALLS_FILENAME"
	fi

	# get the customer area-code path
	AREA_CODES_URL=""
	if [ ! -z "$CUSTOMER_BASE_URL" ] && [ -f "$CUSTOMER_BASE_URL/$XML_FOLDER/$AREA_CODES_FILENAME" ]; then
		AREA_CODES_URL="$CUSTOMER_BASE_URL/$XML_FOLDER/$AREA_CODES_FILENAME"
	fi

	XHTML_XSLT_FILE="$XSLT_PATH/invoice.xsl"
	XHTML_OUTPUT_FILE="$OUTPUT_PATH/$HTML_FOLDER/$CUSTOMER_ID"_"$LANGUAGE.xhtml"

	echo "Xhtml base url '"$BASE_URL"'"
	echo "Xhtml xslt '"$XHTML_XSLT_FILE"'"
	echo "Xhtml output '"$XHTML_OUTPUT_FILE"'"
	echo "XML invoice url '"$INVOICE_URL"'"
	echo "XML payment url '"$PAYMENT_URL"'"
	echo "XML organization '"$ORGANIZATION"'"
	echo "XML customer base url '"$CUSTOMER_BASE_URL"'"
	echo "XML customer url '"$CUSTOMER_URL"'"
	echo "XML services url '"$SERVICES_URL"'"
	echo "XML invoice url '"$INVOICE_URL"'"
	echo "XML charges url '"$CHARGES_URL"'"
	echo "XML previous charges url '"$PREVIOUS_CHARGES_URL"'"
	echo "XML lines url '"$LINES_URL"'"
	echo "XML calls url '"$CALLS_URL"'"
	echo "XML area-code url '"$AREA_CODES_URL"'"

	# create xhtml output
	saxonb-xslt -o:"$XHTML_OUTPUT_FILE" -xsl:"$XHTML_XSLT_FILE" -s:"$INVOICE_URL" lang="$LANGUAGE" customerId="$CUSTOMER_ID" paymentUrl="$PAYMENT_URL" customerBaseUrl="$CUSTOMER_BASE_URL" customerUrl="$CUSTOMER_URL" servicesUrl="$SERVICES_URL" invoiceUrl="$INVOICE_URL" chargesUrl="$CHARGES_URL" previousChargesUrl="$PREVIOUS_CHARGES_URL" linesUrl="$LINES_URL" callsUrl="$CALLS_URL" areaCodeUrl="$AREA_CODES_URL" organization="$ORGANIZATION" resourceBaseUrl="$RESOURCE_URL"

	PDF_SOURCE_FILE="$BASE_URL/$HTML_FOLDER/$URL_ENCODED_CUSTOMER_ID"_"$LANGUAGE.xhtml"
	PDF_OUTPUT_FILE="$OUTPUT_PATH/$PDF_FOLDER/$CUSTOMER_ID"_"$LANGUAGE.pdf"

	echo "Pdf source '"$PDF_SOURCE_FILE"'"
	echo "Pdf output '"$PDF_OUTPUT_FILE"'"
	echo "Pdf title '"$TITLE"'"
	echo "Pdf footer '"$FOOTER_CENTER_TEXT"'"
	echo "Pdf footer right '"$FOOTER_RIGHT_TEXT"'"

	# create pdf output
	wkhtmltopdf --encoding 'UTF-8' --page-size Letter --title "$TITLE" --footer-center "$FOOTER_CENTER_TEXT" --footer-right "$FOOTER_RIGHT_TEXT" --footer-font-size $FOOTER_FONT_SIZE --print-media-type "$PDF_SOURCE_FILE" "$PDF_OUTPUT_FILE"
	# alternative way to create output. uses example rasterize script from https://github.com/ariya/phantomjs/blob/master/examples/rasterize.js
	#phantomjs "$GIT_ROOT_PATH/js/rasterize.js" "$PDF_SOURCE_FILE" "$PDF_OUTPUT_FILE" 1

done

