#!/bin/bash

# Create all pdf invoices.

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

echo "Creating resources"
bash "$CURRENT_PATH/create_resources.sh"

echo "Creating css"
bash "$CURRENT_PATH/create_css.sh"

echo "Creating english invoices"
bash "$CURRENT_PATH/invoice.sh" en
echo "Creating french invoices"
bash "$CURRENT_PATH/invoice.sh" fr

