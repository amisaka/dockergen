#!/bin/bash

# Install software required to process pdfs
# This script assumes an Ubuntu 14.04 system and will not work on other distributions.

DISTRIBUTION=$(lsb_release -a 2>&1 | grep "Distributor ID:" | awk '{print $3}')
VERSION=$(lsb_release -a 2>&1 | grep "Release:" | awk '{print $2}')

if [[ "$DISTRIBUTION" != "Ubuntu" ]] || [[ "$VERSION" != "14.04" ]]; then
	echo "This installer was created for the Ubuntu 14.04 distribution."
	echo "Please configure the '"$0"' script your environment."
fi


# INSTALL REQUIRED APPLICATIONS
# FROM PARENT FOLDER OF GIT REPOSITORY (FOR NODEJS/BOOTSTRAP)


# install XSLT 2.0 command line processor

# install saxon
sudo apt-get install --yes default-jre libsaxonb-java


# install SASS processor

USER=$(whoami)

# install npm
sudo apt-get install --yes npm

# add link to common name
sudo ln -s /usr/bin/nodejs /usr/bin/node

# update npm
sudo npm install --prefix ~/.nodejs -g npm@latest

# install required nodejs packages
sudo npm install --prefix ~/.nodejs -g node-uuid
sudo npm install --prefix ~/.nodejs -g node-sass
sudo npm install --prefix ~/.nodejs -g bootstrap-sass

sudo chown -R $USER:$USER ~/.nodejs

