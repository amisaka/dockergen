#!/bin/bash

# take the raw format outputed by the pdf data extraction and convert it in the pdf creator's input format

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source "$CURRENT_PATH/settings.sh"

if [ ! -d "$CUSTOMERS_RAW_PATH" ]; then
	echo "Folder '"$CUSTOMERS_RAW_PATH"' does not exist"
	exit 1
fi

FILES=($(ls --ignore-backups "$CUSTOMERS_RAW_PATH"/*.xml))

echo "*** Cleaning customer list ***"

for ((i = 0; i < ${#FILES[@]}; i++)) ; do

	FILENAME=$(basename "${FILES[$i]}")
	CUSTOMER_ID="${FILENAME%.*}"

	OUTPUT_PATH="$USER_PATH/$CUSTOMER_ID/$XML_FOLDER"

	echo "Customer id '"$CUSTOMER_ID"'"
	echo "Input path '"$CUSTOMERS_RAW_PATH/$FILENAME"'"
	echo "Output path '"$OUTPUT_PATH/$CUSTOMER_FILENAME"'"

	mkdir -p "$OUTPUT_PATH"

	saxonb-xslt -o:"$OUTPUT_PATH/$CUSTOMER_FILENAME" -xsl:"$XSLT_PATH/clean-customer-list.xsl" -s:"$CUSTOMERS_RAW_PATH/$FILENAME"

done

