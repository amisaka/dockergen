#!/bin/bash

# take the raw format outputed by the pdf data extraction and convert it in the pdf creator's input format

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source "$CURRENT_PATH/settings.sh"

if [ ! -d "$USER_PATH" ]; then
	echo "Folder '"$USER_PATH"' does not exist"
	exit 1
fi

USERS=($(ls --ignore-backups "$USER_PATH"))

echo "*** Cleaning service list ***"

for ((i = 0; i < ${#USERS[@]}; i++)) ; do

	CUSTOMER_ID=$(basename "${USERS[$i]}")

	OUTPUT_PATH="$USER_PATH/$CUSTOMER_ID/$XML_FOLDER"

	echo "Customer id '"$CUSTOMER_ID"'"
	echo "Input path '"$SERVICES_RAW_PATH/$CUSTOMER_ID.xml"'"
	echo "Output path '"$OUTPUT_PATH/$SERVICES_FILENAME"'"

	mkdir -p "$OUTPUT_PATH"

	saxonb-xslt -o:"$OUTPUT_PATH/$SERVICES_FILENAME" -xsl:"$XSLT_PATH/clean-service-list.xsl" -s:"$TEMP_PATH/$SERVICES_FILENAME"

done

