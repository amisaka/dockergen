#!/bin/bash

# create xml summarizing the calls made to a given area code

CURRENT_PATH=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

source "$CURRENT_PATH/settings.sh"

if [ ! -d "$USER_PATH" ]; then
	echo "Folder '"$USER_PATH"' does not exist"
	exit 1
fi

USERS=($(ls --ignore-backups "$USER_PATH"))

echo "*** Cleaning area code summary ***"

for ((i = 0; i < ${#USERS[@]}; i++)) ; do

	CUSTOMER_ID=$(basename "${USERS[$i]}")

	OUTPUT_PATH="$USER_PATH/$CUSTOMER_ID/$XML_FOLDER"

	echo "Customer id '"$CUSTOMER_ID"'"
	echo "Input path '"$CALLS_PATH/$CUSTOMER_ID.xml"'"
	echo "Output path '"$OUTPUT_PATH/$AREA_CODES_FILENAME"'"

	mkdir -p "$OUTPUT_PATH"

	saxonb-xslt -o:"$OUTPUT_PATH/$AREA_CODES_FILENAME" -xsl:"$XSLT_PATH/clean-area-code-summary.xsl" -s:"$CALLS_PATH/$CUSTOMER_ID.xml"

done

