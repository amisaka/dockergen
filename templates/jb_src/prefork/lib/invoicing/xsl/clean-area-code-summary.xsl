<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:include href="include.xsl"/>
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/calls">
    <areaCodes>
      <summary>
        <areaCodeCount>
          <xsl:variable name="allAreaCodes">
            <xsl:for-each-group select=".//*[local-name()='call']" group-by="substring(./*[local-name()='number'],1,3)">
              <xsl:value-of select="concat(current-grouping-key(),' ')"/>
            </xsl:for-each-group>
          </xsl:variable>
          <xsl:value-of select="count(tokenize(normalize-space($allAreaCodes),' '))"/>
        </areaCodeCount>
        <totalCallCount>
          <xsl:value-of select="count(./*)"/>
        </totalCallCount>
        <totalDuration>
          <xsl:value-of select="sum(./call/*[local-name()='duration']/text())"/>
        </totalDuration>
        <totalCost>
          <xsl:value-of select="format-number(sum(./call/*[local-name()='amount']/text()),'0.00')"/>
        </totalCost>
      </summary>
      <items>
        <xsl:for-each-group select=".//*[local-name()='call']" group-by="substring(./*[local-name()='number'],1,3)">
          <item>
            <areaCode>
              <xsl:value-of select="current-grouping-key()"/>
            </areaCode>
            <callCount>
              <xsl:value-of select="count(current-group())"/>
            </callCount>
            <duration>
              <xsl:value-of select="sum(current-group()/*[local-name()='duration']/text())"/>
            </duration>
            <cost>
              <xsl:value-of select="format-number(sum(current-group()/*[local-name()='amount']/text()),'0.00')"/>
            </cost>
          </item>
        </xsl:for-each-group>
      </items>
    </areaCodes>
  </xsl:template>
</xsl:stylesheet>
