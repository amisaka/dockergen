<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:inv="invoice" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" xpath-default-namespace="invoice" exclude-result-prefixes="xs xsl inv" version="2.0">
  <xsl:template name="call-list">
    <xsl:param name="localeXml" required="yes"/>
    <xsl:param name="callsXml" required="yes"/>
    <xsl:param name="lang" required="yes"/>
    <table class="table table-bordered table-striped">
      <caption>
        <xsl:value-of select="$localeXml//*[@name='captionCallsArray']/text()"/>
      </caption>
      <thead>
        <tr>
          <th class="text-nowrap">
            <xsl:value-of select="$localeXml//*[@name='titleCallsArray']/*[local-name()='item'][position()=1]/text()"/>
          </th>
          <th class="text-nowrap">
            <xsl:value-of select="$localeXml//*[@name='titleCallsArray']/*[local-name()='item'][position()=2]/text()"/>
          </th>
          <th class="text-nowrap">
            <xsl:value-of select="$localeXml//*[@name='titleCallsArray']/*[local-name()='item'][position()=3]/text()"/>
          </th>
          <th class="text-nowrap text-expand">
            <xsl:value-of select="$localeXml//*[@name='titleCallsArray']/*[local-name()='item'][position()=4]/text()"/>
          </th>
          <th class="text-nowrap text-right">
            <xsl:value-of select="$localeXml//*[@name='titleCallsArray']/*[local-name()='item'][position()=5]/text()"/>
          </th>
          <th class="text-nowrap text-right">
            <xsl:value-of select="$localeXml//*[@name='titleCallsArray']/*[local-name()='item'][position()=6]/text()"/>
          </th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="$callsXml//*[local-name()='item']">
          <tr>
            <td class="text-nowrap">
              <xsl:value-of select="./*[local-name()='date']/text()"/>
            </td>
            <td class="text-nowrap">
              <xsl:value-of select="./*[local-name()='time']/text()"/>
            </td>
            <td class="text-nowrap">
              <xsl:value-of select="./*[local-name()='telephoneNumber']/text()"/>
            </td>
            <td class="text-nowrap">
              <xsl:value-of select="./*[local-name()='city']/text()"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="./*[local-name()='region']/text()"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="./*[local-name()='country']/text()"/>
            </td>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-duration">
                <xsl:with-param name="time" select="./*[local-name()='lineDuration']/text()"/>
              </xsl:call-template>
            </td>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-currency-tenth">
                <xsl:with-param name="amount" select="./*[local-name()='lineCost']/text()"/>
                <xsl:with-param name="lang" select="$lang"/>
              </xsl:call-template>
            </td>
          </tr>
        </xsl:for-each>
        <tr>
          <td colspan="3"/>
          <td class="text-nowrap text-right">
            <xsl:call-template name="format-total">
              <xsl:with-param name="total" select="$callsXml//*[local-name()='totalCallCount']/text()"/>
              <xsl:with-param name="lang" select="$lang"/>
            </xsl:call-template>
          </td>
          <td class="text-nowrap text-right">
            <xsl:call-template name="format-duration">
              <xsl:with-param name="time" select="$callsXml//*[local-name()='totalDuration']/text()"/>
            </xsl:call-template>
          </td>
          <td class="text-nowrap text-right">
            <xsl:call-template name="format-currency">
              <xsl:with-param name="amount" select="$callsXml//*[local-name()='totalCost']/text()"/>
              <xsl:with-param name="lang" select="$lang"/>
            </xsl:call-template>
          </td>
        </tr>
      </tbody>
    </table>
  </xsl:template>
</xsl:stylesheet>
