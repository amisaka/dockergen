<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:inv="invoice" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" xpath-default-namespace="invoice" exclude-result-prefixes="xs xsl inv" version="2.0">
  <xsl:template name="line-summary">
    <xsl:param name="localeXml" required="yes"/>
    <xsl:param name="linesXml" required="yes"/>
    <xsl:param name="lang" required="yes"/>
    <xsl:for-each select="$linesXml//*[local-name()='item']">
      <table class="table table-bordered table-striped">
        <caption>
          <xsl:value-of select="./*[local-name()='telephoneNumber']/text()"/>
        </caption>
        <thead>
          <tr>
            <th class="text-nowrap">
              <xsl:value-of select="$localeXml//*[@name='titleCallNumberArray']/*[local-name()='item'][position()=1]/text()"/>
            </th>
            <th class="text-nowrap text-right text-expand">
              <xsl:value-of select="$localeXml//*[@name='titleCallNumberArray']/*[local-name()='item'][position()=2]/text()"/>
            </th>
            <th class="text-nowrap text-right">
              <xsl:value-of select="$localeXml//*[@name='titleCallNumberArray']/*[local-name()='item'][position()=3]/text()"/>
            </th>
            <th class="text-nowrap text-right">
              <xsl:value-of select="$localeXml//*[@name='titleCallNumberArray']/*[local-name()='item'][position()=4]/text()"/>
            </th>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select=".//*[local-name()='call']">
            <tr>
              <td class="text-nowrap">
                <xsl:value-of select="./*[local-name()='number']/text()"/>
              </td>
              <td class="text-nowrap text-right">
                <xsl:value-of select="./*[local-name()='count']/text()"/>
              </td>
              <td class="text-nowrap text-right">
                <xsl:call-template name="format-duration">
                  <xsl:with-param name="time" select="./*[local-name()='duration']/text()"/>
                </xsl:call-template>
              </td>
              <td class="text-nowrap text-right">
                <xsl:call-template name="format-currency-tenth">
                  <xsl:with-param name="amount" select="./*[local-name()='amount']/text()"/>
                  <xsl:with-param name="lang" select="$lang"/>
                </xsl:call-template>
              </td>
            </tr>
          </xsl:for-each>
          <tr>
            <td/>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-summary">
                <xsl:with-param name="distinct" select="./*[local-name()='lineCallCount']/text()"/>
                <xsl:with-param name="total" select="$callsXml//*[local-name()='totalCallCount']/text()"/>
                <xsl:with-param name="lang" select="$lang"/>
              </xsl:call-template>
            </td>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-duration">
                <xsl:with-param name="time" select="./*[local-name()='lineDuration']/text()"/>
              </xsl:call-template>
            </td>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-currency">
                <xsl:with-param name="amount" select="./*[local-name()='lineCost']/text()"/>
                <xsl:with-param name="lang" select="$lang"/>
              </xsl:call-template>
            </td>
          </tr>
        </tbody>
      </table>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
