<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:include href="include.xsl"/>
  <xsl:template match="/summary">
    <summary>
      <chargesPreviousBalance>
        <xsl:call-template name="coalesce">
          <xsl:with-param name="value" select="format-number(chargesPreviousBalance,'#0.00')"/>
          <xsl:with-param name="default" select="'0.00'"/>
        </xsl:call-template>
      </chargesPreviousBalance>
      <chargesLateFees>
        <xsl:call-template name="coalesce">
          <xsl:with-param name="value" select="format-number(chargesLateFees,'#0.00')"/>
          <xsl:with-param name="default" select="'0.00'"/>
        </xsl:call-template>
      </chargesLateFees>
      <chargesPastDueBalance>
        <xsl:call-template name="coalesce">
          <xsl:with-param name="value" select="format-number(chargesPastDueBalance,'#0.00')"/>
          <xsl:with-param name="default" select="'0.00'"/>
        </xsl:call-template>
      </chargesPastDueBalance>
      <chargesUsageFees>
        <xsl:call-template name="coalesce">
          <xsl:with-param name="value" select="format-number(chargesUsageFees,'#0.00')"/>
          <xsl:with-param name="default" select="'0.00'"/>
        </xsl:call-template>
      </chargesUsageFees>
      <paymentAmount>
        <xsl:call-template name="coalesce">
          <xsl:with-param name="value" select="format-number(paymentAmount,'#0.00')"/>
          <xsl:with-param name="default" select="'0.00'"/>
        </xsl:call-template>
      </paymentAmount>
      <paymentDate>
        <xsl:call-template name="coalesce">
          <xsl:with-param name="value" select="'2016-07-29'"/>
          <xsl:with-param name="default" select="'2015-12-30'"/>
        </xsl:call-template>
      </paymentDate>
    </summary>
  </xsl:template>
</xsl:stylesheet>
