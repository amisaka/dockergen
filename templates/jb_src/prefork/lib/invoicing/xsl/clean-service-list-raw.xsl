<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/invoice">
    <services>
      <summary>
        <xsl:copy-of copy-namespaces="no" select="taxes"/>
      </summary>
      <items>
        <xsl:for-each select="item">
          <item>
            <startDate>
              <xsl:apply-templates select="startDate"/>
            </startDate>
            <endDate>
              <xsl:apply-templates select="endDate"/>
            </endDate>
            <service>
              <xsl:value-of select="normalize-space(service)"/>
            </service>
            <description>
              <xsl:apply-templates select="description"/>
            </description>
            <quantity>
              <xsl:apply-templates select="quantity"/>
            </quantity>
            <xsl:apply-templates select="price">
              <xsl:with-param name="quantity">
                <xsl:apply-templates select="quantity"/>
              </xsl:with-param>
            </xsl:apply-templates>
          </item>
        </xsl:for-each>
      </items>
    </services>
  </xsl:template>
  <xsl:template match="startDate">
    <xsl:variable name="day" select="substring(./text(), 1, 2)"/>
    <xsl:variable name="month" select="substring(./text(), 4, 2)"/>
    <xsl:variable name="year" select="concat('20', substring(./text(), 7, 2))"/>
    <xsl:value-of select="normalize-space($year)"/>
    <xsl:text>-</xsl:text>
    <xsl:value-of select="normalize-space($month)"/>
    <xsl:text>-</xsl:text>
    <xsl:value-of select="normalize-space($day)"/>
  </xsl:template>
  <xsl:template match="endDate">
    <xsl:variable name="day" select="substring(./text(), 1, 2)"/>
    <xsl:variable name="month" select="substring(./text(), 4, 2)"/>
    <xsl:variable name="year" select="concat('20', substring(./text(), 7, 2))"/>
    <xsl:value-of select="normalize-space($year)"/>
    <xsl:text>-</xsl:text>
    <xsl:value-of select="normalize-space($month)"/>
    <xsl:text>-</xsl:text>
    <xsl:value-of select="normalize-space($day)"/>
  </xsl:template>
  <xsl:template match="description">
    <xsl:value-of select="normalize-space(./text())"/>
  </xsl:template>
  <xsl:template match="quantity">
    <xsl:value-of select="normalize-space(replace(./text(), '=', ''))"/>
  </xsl:template>
  <xsl:template name="priceNormalized">
    <xsl:param name="price"/>
    <xsl:choose>
      <xsl:when test="contains($price, '.')">
        <!-- is english, just remove commas -->
        <xsl:value-of select="normalize-space(translate($price, ',', ''))"/>
      </xsl:when>
      <xsl:otherwise>
        <!-- is french, remove spaces and replace commas with periods -->
        <xsl:value-of select="normalize-space(translate(translate($price, ',', '.'), ' ', ''))"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="price">
    <xsl:param name="quantity"/>
    <xsl:variable name="priceNormalized">
      <xsl:call-template name="priceNormalized">
        <xsl:with-param name="price">
          <xsl:value-of select="./text()"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <unitPrice>
      <xsl:value-of select="format-number($priceNormalized div $quantity,'0.00')"/>
    </unitPrice>
    <totalPrice>
      <xsl:value-of select="$priceNormalized"/>
    </totalPrice>
  </xsl:template>
</xsl:stylesheet>
