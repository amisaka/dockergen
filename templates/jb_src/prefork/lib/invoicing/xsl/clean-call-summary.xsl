<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/calls">
    <calls>
      <summary>
        <telephoneNumberCount>
          <xsl:variable name="allTelephoneNumbers">
            <xsl:for-each-group select=".//*[local-name()='call']" group-by="./*[local-name()='number']">
              <xsl:value-of select="concat(current-grouping-key(),' ')"/>
            </xsl:for-each-group>
          </xsl:variable>
          <xsl:value-of select="count(tokenize(normalize-space($allTelephoneNumbers),' '))"/>
        </telephoneNumberCount>
        <totalCallCount>
          <xsl:value-of select="count(./*[local-name()='call'])"/>
        </totalCallCount>
        <totalDuration>
          <xsl:value-of select="sum(.//*[local-name()='duration']/text())"/>
        </totalDuration>
        <totalCost>
          <xsl:value-of select="format-number(sum(.//*[local-name()='amount']/text()),'0.00')"/>
        </totalCost>
      </summary>
      <items>
        <xsl:for-each-group select=".//*[local-name()='call']" group-by="./*[local-name()='number']">
          <item>
            <date>
              <xsl:value-of select="./*[local-name()='date']/text()"/>
            </date>
            <time>
              <xsl:value-of select="./*[local-name()='time']/text()"/>
            </time>
            <city>
              <xsl:value-of select="./*[local-name()='city']/text()"/>
            </city>
            <region>
              <xsl:value-of select="./*[local-name()='region']/text()"/>
            </region>
            <country>
              <xsl:value-of select="./*[local-name()='country']/text()"/>
            </country>
            <lineNumber>
              <xsl:value-of select="./*[local-name()='line']/text()"/>
            </lineNumber>
            <telephoneNumber>
              <xsl:value-of select="./*[local-name()='number']/text()"/>
            </telephoneNumber>
            <lineCallCount>
              <xsl:value-of select="count(current-group())"/>
            </lineCallCount>
            <lineDuration>
              <xsl:value-of select="sum(current-group()/*[local-name()='duration']/text())"/>
            </lineDuration>
            <lineCost>
              <xsl:value-of select="format-number(sum(current-group()/*[local-name()='amount']/text()),'0.00')"/>
            </lineCost>
          </item>
        </xsl:for-each-group>
      </items>
    </calls>
  </xsl:template>
</xsl:stylesheet>
