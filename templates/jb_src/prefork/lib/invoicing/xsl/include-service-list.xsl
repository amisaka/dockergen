<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:inv="invoice" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" xpath-default-namespace="invoice" exclude-result-prefixes="xs xsl inv" version="2.0">
  <xsl:template name="service-list">
    <xsl:param name="localeXml" required="yes"/>
    <xsl:param name="servicesXml" required="yes"/>
    <xsl:param name="lang" required="yes"/>
    <table class="table table-bordered table-striped">
      <caption>
        <xsl:value-of select="$localeXml//*[@name='captionChargesArray']/text()"/>
      </caption>
      <thead>
        <tr>
          <th class="text-nowrap">
            <xsl:value-of select="$localeXml//*[@name='titleChargesArray']/*[local-name()='item'][position()=1]/text()"/>
          </th>
          <th class="text-nowrap">
            <xsl:value-of select="$localeXml//*[@name='titleChargesArray']/*[local-name()='item'][position()=2]/text()"/>
          </th>
          <th class="text-nowrap text-expand">
            <xsl:value-of select="$localeXml//*[@name='titleChargesArray']/*[local-name()='item'][position()=3]/text()"/>
          </th>
          <th class="text-nowrap text-right">
            <xsl:value-of select="$localeXml//*[@name='titleChargesArray']/*[local-name()='item'][position()=4]/text()"/>
          </th>
          <th class="text-nowrap text-right">
            <xsl:value-of select="$localeXml//*[@name='titleChargesArray']/*[local-name()='item'][position()=5]/text()"/>
          </th>
          <th class="text-nowrap text-right">
            <xsl:value-of select="$localeXml//*[@name='titleChargesArray']/*[local-name()='item'][position()=6]/text()"/>
          </th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="$servicesXml//*[local-name()='item']">
          <tr>
            <td class="text-nowrap">
              <xsl:value-of select="./*[local-name()='service']/text()"/>
            </td>
            <td class="text-nowrap">
              <xsl:call-template name="format-date-number">
                <xsl:with-param name="date" select="./*[local-name()='startDate']"/>
              </xsl:call-template>
              <xsl:text> - </xsl:text>
              <xsl:call-template name="format-date-number">
                <xsl:with-param name="date" select="./*[local-name()='endDate']"/>
              </xsl:call-template>
            </td>
            <td>
              <xsl:value-of select="./*[local-name()='description']/text()"/>
            </td>
            <td class="text-right">
              <xsl:value-of select="./*[local-name()='quantity']/text()"/>
            </td>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-currency">
                <xsl:with-param name="amount" select="./*[local-name()='unitPrice']/text()"/>
                <xsl:with-param name="lang" select="$lang"/>
              </xsl:call-template>
            </td>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-currency">
                <xsl:with-param name="amount" select="./*[local-name()='totalPrice']/text()"/>
                <xsl:with-param name="lang" select="$lang"/>
              </xsl:call-template>
            </td>
          </tr>
        </xsl:for-each>
        <tr>
          <td colspan="5" class="text-nowrap text-right">
            <xsl:value-of select="$localeXml//*[@name='labelSubTotal']/text()"/>
          </td>
          <td class="text-nowrap text-right">
            <xsl:call-template name="format-currency">
              <xsl:with-param name="amount" select="$servicesXml//*[local-name()='subTotal']"/>
              <xsl:with-param name="lang" select="$lang"/>
            </xsl:call-template>
          </td>
        </tr>
        <xsl:for-each select="$servicesXml//*[local-name()='tax']">
          <tr>
            <td colspan="5" class="text-nowrap text-right">
              <xsl:value-of select="./*[local-name()='description']/text()"/>
            </td>
            <td class="text-nowrap text-right">
              <xsl:call-template name="format-currency">
                <xsl:with-param name="amount" select="./*[local-name()='cost']"/>
                <xsl:with-param name="lang" select="$lang"/>
              </xsl:call-template>
            </td>
          </tr>
        </xsl:for-each>
        <tr>
          <td colspan="5" class="text-nowrap text-right">
            <xsl:value-of select="$localeXml//*[@name='labelTotal']/text()"/>
          </td>
          <td class="text-nowrap text-right">
            <xsl:call-template name="format-currency">
              <xsl:with-param name="amount" select="$servicesXml//*[local-name()='total']"/>
              <xsl:with-param name="lang" select="$lang"/>
            </xsl:call-template>
          </td>
        </tr>
      </tbody>
    </table>
  </xsl:template>
</xsl:stylesheet>
