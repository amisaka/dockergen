<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/calls">
    <lines>
      <summary>
        <totalCallCount>
          <xsl:value-of select="count(./*)"/>
        </totalCallCount>
        <totalDuration>
          <xsl:value-of select="sum(./call/*[local-name()='duration']/text())"/>
        </totalDuration>
        <totalCost>
          <xsl:value-of select="format-number(sum(./call/*[local-name()='amount']/text()),'0.00')"/>
        </totalCost>
      </summary>
      <items>
        <xsl:for-each-group select=".//*[local-name()='call']" group-by="./*[local-name()='line']">
          <item>
            <telephoneNumber>
              <xsl:value-of select="./*[local-name()='number']/text()"/>
            </telephoneNumber>
            <lineCallCount>
              <xsl:value-of select="count(current-group())"/>
            </lineCallCount>
            <lineDuration>
              <xsl:value-of select="sum(current-group()/*[local-name()='duration']/text())"/>
            </lineDuration>
            <lineCost>
              <xsl:value-of select="format-number(sum(current-group()/*[local-name()='amount']/text()),'0.00')"/>
            </lineCost>
            <calls>
              <xsl:for-each-group select="current-group()" group-by="./*[local-name()='number']">
                <call>
                  <number>
                    <xsl:value-of select="./*[local-name()='number']/text()"/>
                  </number>
                  <count>
                    <xsl:value-of select="count(current-group())"/>
                  </count>
                  <duration>
                    <xsl:value-of select="sum(current-group()/*[local-name()='duration']/text())"/>
                  </duration>
                  <amount>
                    <xsl:value-of select="format-number(sum(current-group()/*[local-name()='amount']/text()),'0.000')"/>
                  </amount>
                </call>
              </xsl:for-each-group>
            </calls>
          </item>
        </xsl:for-each-group>
      </items>
    </lines>
  </xsl:template>
</xsl:stylesheet>
