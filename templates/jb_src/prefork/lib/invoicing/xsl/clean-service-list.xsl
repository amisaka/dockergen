<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/services">
    <services>
      <summary>
        <callCount>
          <xsl:value-of select="count(./items/*)"/>
        </callCount>
        <xsl:copy-of copy-namespaces="no" select="./summary/taxes"/>
        <subTotal>
          <xsl:value-of select="format-number(sum(./items/item/*[local-name()='totalPrice']/text()),'0.00')"/>
        </subTotal>
        <taxTotal>
          <xsl:value-of select="format-number(sum(./summary/taxes/tax/*[local-name()='cost']/text()),'0.00')"/>
        </taxTotal>
        <total>
          <xsl:value-of select="format-number(sum(./items/item/*[local-name()='totalPrice']/text()) + sum(./summary/taxes/tax/*[local-name()='cost']/text()),'0.00')"/>
        </total>
      </summary>
      <items>
        <xsl:for-each select=".//*[local-name()='item']">
          <item>
            <startDate>
              <xsl:value-of select="./*[local-name()='startDate']/text()"/>
            </startDate>
            <endDate>
              <xsl:value-of select="./*[local-name()='endDate']/text()"/>
            </endDate>
            <service>
              <xsl:value-of select="./*[local-name()='service']/text()"/>
            </service>
            <description>
              <xsl:value-of select="./*[local-name()='description']/text()"/>
            </description>
            <quantity>
              <xsl:value-of select="./*[local-name()='quantity']/text()"/>
            </quantity>
            <unitPrice>
              <xsl:value-of select="./*[local-name()='unitPrice']/text()"/>
            </unitPrice>
            <totalPrice>
              <xsl:value-of select="./*[local-name()='totalPrice']/text()"/>
            </totalPrice>
          </item>
        </xsl:for-each>
      </items>
    </services>
  </xsl:template>
</xsl:stylesheet>
