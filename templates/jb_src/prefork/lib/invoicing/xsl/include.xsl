<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:inv="invoice" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" xpath-default-namespace="invoice" exclude-result-prefixes="xs xsl inv" version="2.0">
  <xsl:template name="coalesce">
    <xsl:param name="value" as="xs:string"/>
    <xsl:param name="default" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="$value != ''">
        <xsl:value-of select="$value"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$default"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="format-month-name">
    <xsl:param name="months"/>
    <xsl:param name="month"/>
    <xsl:value-of select="$months//*[local-name()=$month]/text()"/>
  </xsl:template>
  <xsl:template name="format-date-name">
    <xsl:param name="months"/>
    <xsl:param name="date"/>
    <xsl:call-template name="format-month-name">
      <xsl:with-param name="months" select="$months"/>
      <xsl:with-param name="month" select="concat('month',format-date($date,'[M1]'))"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:value-of select="format-date($date,'[D1]')"/>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="format-date($date,'[Y0001]')"/>
  </xsl:template>
  <xsl:template name="format-date-number">
    <xsl:param name="date"/>
    <xsl:value-of select="format-date($date,'[D1]')"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="format-date($date,'[M1]')"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="format-date($date,'[Y0001]')"/>
  </xsl:template>
  <xsl:template name="format-currency">
    <xsl:param name="amount" required="yes"/>
    <xsl:param name="lang" required="yes"/>
    <xsl:choose>
      <xsl:when test="$lang = 'fr'">
        <xsl:value-of select="format-number($amount,'# ##0,00 $','fr')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number($amount,'$#,##0.00','en')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="format-currency-tenth">
    <xsl:param name="amount" required="yes"/>
    <xsl:param name="lang" required="yes"/>
    <xsl:choose>
      <xsl:when test="$lang = 'fr'">
        <xsl:value-of select="format-number($amount,'# ##0,000 $','fr')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number($amount,'$#,##0.000','en')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="format-duration">
    <xsl:param name="time" select="."/>
    <xsl:value-of select="format-number($time div 3600, '00')"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="format-number($time div 60 mod 60, '00')"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="format-number($time mod 60, '00')"/>
  </xsl:template>
  <xsl:template name="format-total">
    <xsl:param name="total" required="yes"/>
    <xsl:param name="lang" required="yes"/>
    <xsl:choose>
      <xsl:when test="$lang = 'fr'">
        <xsl:value-of select="$total"/>
        <xsl:text> au total</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$total"/>
        <xsl:text> total</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="format-summary">
    <xsl:param name="distinct" required="yes"/>
    <xsl:param name="total" required="yes"/>
    <xsl:param name="lang" required="yes"/>
    <xsl:choose>
      <xsl:when test="$lang = 'fr'">
        <xsl:value-of select="$distinct"/>
        <xsl:text> distinctes, </xsl:text>
        <xsl:value-of select="$total"/>
        <xsl:text> au total</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$distinct"/>
        <xsl:text> distinct, </xsl:text>
        <xsl:value-of select="$total"/>
        <xsl:text> total</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="logo-image-large">
    <xsl:param name="src"/>
    <xsl:param name="alt"/>
    <img align="left" class="logoLarge">
      <xsl:attribute name="src">
        <xsl:value-of select="$src"/>
      </xsl:attribute>
      <xsl:attribute name="alt">
        <xsl:value-of select="$alt"/>
      </xsl:attribute>
    </img>
  </xsl:template>
  <xsl:template name="logo-image-small">
    <xsl:param name="src"/>
    <xsl:param name="alt"/>
    <img align="left" class="logoSmall">
      <xsl:attribute name="src">
        <xsl:value-of select="$src"/>
      </xsl:attribute>
      <xsl:attribute name="alt">
        <xsl:value-of select="$alt"/>
      </xsl:attribute>
    </img>
  </xsl:template>
</xsl:stylesheet>
