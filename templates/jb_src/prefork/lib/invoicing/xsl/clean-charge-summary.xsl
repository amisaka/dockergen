<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:include href="include.xsl"/>
  <!-- collect parameters used to compute output -->
  <xsl:param name="previousChargesUrl" required="yes"/>
  <!-- hold xml/text fragments used in output -->
  <xsl:variable name="previousChargesXml" select="document($previousChargesUrl)"/>
  <!-- match root element, initiate document processing -->
  <xsl:template match="/services">
    <!-- start html output document -->
    <summary>
      <currentCharges>
        <xsl:value-of select="format-number(./summary/total,'#0.00')"/>
      </currentCharges>
      <subTotal>
        <xsl:value-of select="format-number(./summary/subTotal + $previousChargesXml//*[local-name()='chargesUsageFees']/text() + ./summary/taxTotal,'#0.00')"/>
      </subTotal>
      <pastDue>
        <xsl:value-of select="format-number($previousChargesXml//*[local-name()='chargesPreviousBalance']/text() - $previousChargesXml//*[local-name()='paymentAmount']/text() + $previousChargesXml//*[local-name()='chargesLateFees']/text(),'#0.00')"/>
      </pastDue>
      <currentDue>
        <xsl:value-of select="format-number($previousChargesXml//*[local-name()='chargesPreviousBalance']/text() - $previousChargesXml//*[local-name()='paymentAmount']/text() + $previousChargesXml//*[local-name()='chargesLateFees']/text() + sum(./summary/subTotal) + $previousChargesXml//*[local-name()='chargesUsageFees']/text() + ./summary/taxTotal,'#0.00')"/>
      </currentDue>
      <totalDue>
        <xsl:value-of select="format-number($previousChargesXml//*[local-name()='chargesPreviousBalance']/text() - $previousChargesXml//*[local-name()='paymentAmount']/text() + $previousChargesXml//*[local-name()='chargesLateFees']/text() + sum(./summary/subTotal) + ./summary/taxTotal,'#0.00')"/>
      </totalDue>
      <dueDate>
        <xsl:call-template name="coalesce">
          <xsl:with-param name="value" select="'2016-08-31'"/>
          <xsl:with-param name="default" select="'2015-12-30'"/>
        </xsl:call-template>
      </dueDate>
    </summary>
  </xsl:template>
</xsl:stylesheet>
