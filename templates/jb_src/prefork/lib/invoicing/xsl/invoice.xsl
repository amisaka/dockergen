<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:inv="invoice" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.w3.org/1999/xhtml" xpath-default-namespace="invoice" exclude-result-prefixes="xs xsl inv" version="2.0">
  <!-- reference xsl fragments that are called -->
  <xsl:include href="include.xsl"/>
  <xsl:include href="include-service-list.xsl"/>
  <xsl:include href="include-call-list.xsl"/>
  <xsl:include href="include-line-summary.xsl"/>
  <xsl:include href="include-call-summary.xsl"/>
  <xsl:include href="include-area-code-summary.xsl"/>
  <!-- collect parameters used to compute output -->
  <xsl:param name="customerId" required="yes"/>
  <xsl:param name="lang" select="'en'"/>
  <xsl:param name="organization" select="'default'"/>
  <xsl:param name="fragmentBaseUrl" select="'fragments'"/>
  <xsl:param name="xmlBaseUrl" select="'../xml'"/>
  <xsl:param name="resourceBaseUrl" select="'http://localhost:9000'"/>
  <!-- parameters that can be overridden to direct to a http/https location -->
  <!-- by default the parameters are created by convention to use file paths the reference outside of the repository -->
  <xsl:param name="localeUrl" select="concat($fragmentBaseUrl,'/','resources_',$lang,'.xml')"/>
  <xsl:param name="messageTextUrl" select="concat($fragmentBaseUrl,'/','messageText_',$lang,'.xhtml')"/>
  <xsl:param name="legalTextUrl" select="concat($fragmentBaseUrl,'/','legalText_',$lang,'.xhtml')"/>
  <xsl:param name="addressChangeFormUrl" select="concat($fragmentBaseUrl,'/','addressChangeForm_',$lang,'.xhtml')"/>
  <xsl:param name="organizationUrl" select="concat($xmlBaseUrl,'/organization/',$organization,'_',$lang,'.xml')"/>
  <xsl:param name="monthsUrl" select="concat($xmlBaseUrl,'/months_',$lang,'.xml')"/>
  <!-- urls that define the info specific to creating this invoice using a convention of having
       a base url that specifies the type of document and a filename matching the customer id -->
  <xsl:param name="customerUrl" select="''"/>
  <xsl:param name="servicesUrl" select="''"/>
  <xsl:param name="chargesUrl" select="''"/>
  <xsl:param name="previousChargesUrl" select="''"/>
  <xsl:param name="paymentUrl" select="''"/>
  <xsl:param name="linesUrl" select="''"/>
  <xsl:param name="callsUrl" select="''"/>
  <xsl:param name="areaCodeUrl" select="''"/>
  <!-- hold xml/text fragments used in output -->
  <xsl:variable name="localeXml" select="document($localeUrl)"/>
  <xsl:variable name="messageText" select="document($messageTextUrl)"/>
  <xsl:variable name="legalText" select="document($legalTextUrl)"/>
  <xsl:variable name="addressChangeForm" select="document($addressChangeFormUrl)"/>
  <xsl:variable name="organizationXml" select="document($organizationUrl)"/>
  <xsl:variable name="monthsXml" select="document($monthsUrl)"/>
  <xsl:variable name="customerXml" select="document($customerUrl)"/>
  <xsl:variable name="servicesXml" select="document($servicesUrl)"/>
  <xsl:variable name="chargesXml" select="document($chargesUrl)"/>
  <xsl:variable name="previousChargesXml" select="document($previousChargesUrl)"/>
  <xsl:variable name="paymentXml" select="document($paymentUrl)"/>
  <xsl:variable name="linesXml" select="document($linesUrl)"/>
  <xsl:variable name="callsXml" select="document($callsUrl)"/>
  <xsl:variable name="areaCodeXml" select="document($areaCodeUrl)"/>
  <xsl:output method="xhtml"/>
  <!-- specify localization used for english and french numbers -->
  <xsl:decimal-format name="fr" decimal-separator="," grouping-separator=" "/>
  <xsl:decimal-format name="en" decimal-separator="." grouping-separator=","/>
  <!-- match root element, initiate document processing -->
  <xsl:template match="/">
    <!-- start html output document -->
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>
          <xsl:value-of select="$localeXml//*[@name='title']/text()"/>
          <xsl:text> - </xsl:text>
          <xsl:value-of select="$organizationXml//*[local-name()='name']/text()"/>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <!-- import stylesheet that is created by sass -->
        <link type="text/css" rel="stylesheet" media="all">
          <xsl:attribute name="href">
            <xsl:value-of select="$resourceBaseUrl"/>
            <xsl:text>/resources/invoice.css</xsl:text>
          </xsl:attribute>
        </link>
      </head>
      <body>
        <div class="container-fluid">
          <div class="row text-offset">
            <div class="col-xs-7">
              <!-- add large organization logo to top of first page -->
              <div class="col-xs-12 form-group">
                <xsl:call-template name="logo-image-large">
                  <xsl:with-param name="src">
                    <xsl:value-of select="$resourceBaseUrl"/>
                    <xsl:value-of select="$organizationXml//*[local-name()='logoHeaderImage']/text()"/>
                  </xsl:with-param>
                  <xsl:with-param name="alt">
                    <xsl:value-of select="$resourceBaseUrl"/>
                    <xsl:value-of select="$organizationXml//*[local-name()='logoHeaderAlt']/text()"/>
                  </xsl:with-param>
                </xsl:call-template>
              </div>
              <!-- add organization address -->
              <div class="col-xs-12 form-group">
                <xsl:value-of select="$organizationXml//*[local-name()='street']/text()"/>
                <br/>
                <xsl:value-of select="$organizationXml//*[local-name()='city']/text()"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="$organizationXml//*[local-name()='province']/text()"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="$organizationXml//*[local-name()='country']/text()"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$organizationXml//*[local-name()='postalCode']/text()"/>
              </div>
              <!-- add customer address -->
              <div class="col-xs-12 form-group">
                <strong>
                  <xsl:value-of select="$customerXml//*[local-name()='name']/text()"/>
                </strong>
                <br/>
                <xsl:value-of select="$customerXml//*[local-name()='number']/text()"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='street']/text()"/>
                <br/>
                <xsl:value-of select="$customerXml//*[local-name()='city']/text()"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='province']/text()"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='country']/text()"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='postalCode']/text()"/>
                <xsl:if test="$customerXml//*[local-name()='phone']/text() != ''">
                  <br/>
                  <xsl:value-of select="$localeXml//*[@name='labelPhone']/text()"/>
                  <xsl:value-of select="$customerXml//*[local-name()='phone']/text()"/>
                </xsl:if>
                <xsl:if test="$customerXml//*[local-name()='fax']/text() != ''">
                  <br/>
                  <xsl:value-of select="$localeXml//*[@name='labelFax']/text()"/>
                  <xsl:value-of select="$customerXml//*[local-name()='fax']/text()"/>
                </xsl:if>
                <xsl:if test="$customerXml//*[local-name()='attention']/text() != ''">
                  <br/>
                  <xsl:value-of select="$localeXml//*[@name='labelAttention']/text()"/>
                  <xsl:value-of select="$customerXml//*[local-name()='attention']/text()"/>
                </xsl:if>
              </div>
              <!-- add customer service contact information -->
              <div class="col-xs-12 form-group form-box">
                <div class="form-spacer">
                  <div class="row">
                    <strong>
                      <xsl:value-of select="$localeXml//*[@name='titleCustomerService']/text()"/>
                    </strong>
                  </div>
                  <div class="row">
                    <strong class="pull-left">
                      <xsl:value-of select="$localeXml//*[@name='labelCustomerService']/text()"/>
                    </strong>
                    <span class="pull-right">
                      <xsl:value-of select="$organizationXml//*[local-name()='phoneCustomerService']/text()"/>
                    </span>
                  </div>
                  <div class="row">
                    <strong class="pull-left">
                      <xsl:value-of select="$localeXml//*[@name='labelTechnicalSupport']/text()"/>
                    </strong>
                    <span class="pull-right">
                      <xsl:value-of select="$organizationXml//*[local-name()='phoneTechnicalSupport']/text()"/>
                    </span>
                  </div>
                  <div class="row">
                    <strong class="pull-left">
                      <xsl:value-of select="$localeXml//*[@name='labelBillingEmail']/text()"/>
                    </strong>
                    <span class="pull-right">
                      <xsl:value-of select="$organizationXml//*[local-name()='emailBilling']/text()"/>
                    </span>
                  </div>
                </div>
              </div>
              <!-- add gst/pst information -->
              <div class="col-xs-12 form-group">
                <span>
                  <xsl:value-of select="$localeXml//*[@name='textRegistration']/text()"/>
                </span>
              </div>
              <!-- add message to the customer -->
              <div class="col-xs-12 form-group">
                <xsl:copy-of copy-namespaces="no" select="$messageText"/>
              </div>
            </div>
            <div class="col-xs-5">
              <!-- add account/invoice information -->
              <div class="col-xs-12 form-group">
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='invoiceAccountNumber']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:value-of select="$customerXml//*[local-name()='accountNumber']/text()"/>
                  </span>
                </div>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='invoiceNumber']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:value-of select="//*[local-name()='invoiceNumber']/text()"/>
                  </span>
                </div>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='invoiceCreationDate']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:call-template name="format-date-name">
                      <xsl:with-param name="months" select="$monthsXml"/>
                      <xsl:with-param name="date" select="//*[local-name()='invoiceCreationDate']/text()"/>
                    </xsl:call-template>
                  </span>
                </div>
              </div>
              <!-- add payment summary -->
              <div class="col-xs-12 form-group">
                <div class="row form-title">
                  <strong>
                    <xsl:value-of select="$localeXml//*[@name='titleAccountHistory']/text()"/>
                  </strong>
                </div>
                <div class="row">
                  <span class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelPreviousCharges']/text()"/>
                  </span>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$previousChargesXml//*[local-name()='chargesPreviousBalance']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <div class="row">
                  <span class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelPaymentReceived']/text()"/>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="format-date-name">
                      <xsl:with-param name="months" select="$monthsXml"/>
                      <xsl:with-param name="date" select="$previousChargesXml//*[local-name()='paymentDate']/text()"/>
                    </xsl:call-template>
                  </span>
                  <span class="pull-right">
                    <xsl:text>-</xsl:text>
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$previousChargesXml//*[local-name()='paymentAmount']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <div class="row">
                  <span class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelLateFeeCharges']/text()"/>
                  </span>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$previousChargesXml//*[local-name()='chargesLateFees']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelPastDueBalance']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$chargesXml//*[local-name()='pastDue']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
              </div>
              <!-- add current charge summary -->
              <div class="col-xs-12 form-group">
                <div class="row form-title">
                  <strong>
                    <xsl:value-of select="$localeXml//*[@name='titleCurrentCharges']/text()"/>
                  </strong>
                </div>
                <div class="row">
                  <span class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelMonthlyCharges']/text()"/>
                  </span>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$servicesXml//*[local-name()='subTotal']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <div class="row">
                  <span class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelUsageCharges']/text()"/>
                  </span>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$previousChargesXml//*[local-name()='chargesUsageFees']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <xsl:for-each select="$servicesXml//*[local-name()='tax']">
                  <div class="row">
                    <span class="pull-left">
                      <xsl:value-of select="./*[local-name()='description']/text()"/>
                    </span>
                    <span class="pull-right">
                      <xsl:call-template name="format-currency">
                        <xsl:with-param name="amount" select="./*[local-name()='cost']/text()"/>
                        <xsl:with-param name="lang" select="$lang"/>
                      </xsl:call-template>
                    </span>
                  </div>
                </xsl:for-each>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelTotalCurrentCharges']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$chargesXml//*[local-name()='subTotal']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
              </div>
              <!-- add currently owing summary -->
              <div class="col-xs-12 form-group">
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelTotalDueImmediately']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$chargesXml//*[local-name()='pastDue']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelTotalDueBy']/text()"/>
                    <xsl:text> </xsl:text>
                    <xsl:call-template name="format-date-name">
                      <xsl:with-param name="months" select="$monthsXml"/>
                      <xsl:with-param name="date" select="$chargesXml//*[local-name()='dueDate']/text()"/>
                    </xsl:call-template>
                  </strong>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$chargesXml//*[local-name()='subTotal']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <br/>
                <div class="row">
                  <span class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelTotalDue']/text()"/>
                  </span>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$chargesXml//*[local-name()='currentDue']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <!-- add message above cut line for payment details -->
          <p class="text-center">
            <xsl:value-of select="$localeXml//*[@name='textDetachPayInfo']/text()"/>
          </p>
          <br/>
          <hr/>
          <div class="row">
            <div class="col-xs-7">
              <!-- add small logo to top of payment details on the first page -->
              <div class="col-xs-12 form-group">
                <xsl:choose>
                  <xsl:when test="$paymentXml//*[local-name()='logoPaymentImage']/text() != ''">
                    <xsl:call-template name="logo-image-small">
                      <xsl:with-param name="src">
                        <xsl:value-of select="$resourceBaseUrl"/>
                        <xsl:value-of select="$paymentXml//*[local-name()='logoPaymentImage']/text()"/>
                      </xsl:with-param>
                      <xsl:with-param name="alt">
                        <xsl:value-of select="$resourceBaseUrl"/>
                        <xsl:value-of select="$paymentXml//*[local-name()='logoPaymentAlt']/text()"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:call-template name="logo-image-small">
                      <xsl:with-param name="src">
                        <xsl:value-of select="$resourceBaseUrl"/>
                        <xsl:value-of select="$organizationXml//*[local-name()='logoPaymentImage']/text()"/>
                      </xsl:with-param>
                      <xsl:with-param name="alt">
                        <xsl:value-of select="$resourceBaseUrl"/>
                        <xsl:value-of select="$organizationXml//*[local-name()='logoPaymentAlt']/text()"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:otherwise>
                </xsl:choose>
              </div>
              <!-- add customer contact information to payment details -->
              <div class="col-xs-12 form-group">
                <strong>
                  <xsl:value-of select="$customerXml//*[local-name()='name']/text()"/>
                </strong>
                <br/>
                <xsl:value-of select="$customerXml//*[local-name()='number']/text()"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='street']/text()"/>
                <br/>
                <xsl:value-of select="$customerXml//*[local-name()='city']/text()"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='province']/text()"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='country']/text()"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$customerXml//*[local-name()='postalCode']/text()"/>
                <xsl:if test="$customerXml//*[local-name()='phone']/text() != ''">
                  <br/>
                  <xsl:value-of select="$localeXml//*[@name='labelPhone']/text()"/>
                  <xsl:value-of select="$customerXml//*[local-name()='phone']/text()"/>
                </xsl:if>
                <xsl:if test="$customerXml//*[local-name()='fax']/text() != ''">
                  <br/>
                  <xsl:value-of select="$localeXml//*[@name='labelFax']/text()"/>
                  <xsl:value-of select="$customerXml//*[local-name()='fax']/text()"/>
                </xsl:if>
                <xsl:if test="$customerXml//*[local-name()='attention']/text() != ''">
                  <br/>
                  <xsl:value-of select="$localeXml//*[@name='labelAttention']/text()"/>
                  <xsl:value-of select="$customerXml//*[local-name()='attention']/text()"/>
                </xsl:if>
              </div>
            </div>
            <div class="col-xs-5">
              <!-- add account information to payment details -->
              <div class="col-xs-12">
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='invoiceAccountNumber']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:value-of select="$customerXml//*[local-name()='accountNumber']/text()"/>
                  </span>
                </div>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='invoiceNumber']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:value-of select="//*[local-name()='invoiceNumber']/text()"/>
                  </span>
                </div>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='invoiceCreationDate']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:value-of select="format-date(//*[local-name()='invoiceCreationDate']/text(),'[D01]/[M01]/[Y0001]')"/>
                  </span>
                </div>
                <div class="row">
                  <strong class="pull-left">
                    <xsl:value-of select="$chargesXml//*[@name='dueDate']/text()"/>
                  </strong>
                  <span class="pull-right">
                    <xsl:value-of select="format-date(/invoice/invoiceDueDate,'[D01]/[M01]/[Y0001]')"/>
                  </span>
                </div>
                <br/>
                <div class="row">
                  <span class="pull-left">
                    <xsl:value-of select="$localeXml//*[@name='labelTotalDue']/text()"/>
                  </span>
                  <span class="pull-right">
                    <xsl:call-template name="format-currency">
                      <xsl:with-param name="amount" select="$chargesXml//*[local-name()='currentDue']/text()"/>
                      <xsl:with-param name="lang" select="$lang"/>
                    </xsl:call-template>
                  </span>
                </div>
                <div class="row text-lined">
                  <div class="pull-left text-label">
                    <xsl:value-of select="$localeXml//*[@name='labelAmountEnclosed']/text()"/>
                  </div>
                  <div class="text-nowrap text-block">
                    <xsl:choose>
                      <xsl:when test="$lang = 'fr'">
                        <div class="pull-right">$</div>
                        <div class="text-nowrap text-underline"> </div>
                      </xsl:when>
                      <xsl:otherwise>
                        <div class="pull-left">$</div>
                        <div class="text-nowrap text-underline"> </div>
                      </xsl:otherwise>
                    </xsl:choose>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 form-group">
            <p>
              <xsl:value-of select="$localeXml//*[@name='textChequePayable']/text()"/>
            </p>
            <p><input type="checkbox"/> <xsl:value-of select="$localeXml//*[@name='textCheckChangeAddress']/text()"/></p>
          </div>
          <div class="break"/>
          <div class="text-offset">
            <!-- add legal information -->
            <xsl:copy-of copy-namespaces="no" select="$legalText"/>
          </div>
          <br/>
          <hr/>
          <!-- add address change information -->
          <xsl:copy-of copy-namespaces="no" select="$addressChangeForm"/>
          <div class="break"/>
          <!-- add monthly service charges -->
          <xsl:call-template name="service-list">
            <xsl:with-param name="localeXml" select="$localeXml"/>
            <xsl:with-param name="servicesXml" select="$servicesXml"/>
            <xsl:with-param name="lang" select="$lang"/>
          </xsl:call-template>
          <div class="break"/>
          <!-- add call charges -->
          <xsl:call-template name="call-list">
            <xsl:with-param name="localeXml" select="$localeXml"/>
            <xsl:with-param name="callsXml" select="$callsXml"/>
            <xsl:with-param name="lang" select="$lang"/>
          </xsl:call-template>
          <div class="break"/>
          <!-- add charges by phone number called from -->
          <xsl:call-template name="line-summary">
            <xsl:with-param name="localeXml" select="$localeXml"/>
            <xsl:with-param name="linesXml" select="$linesXml"/>
            <xsl:with-param name="lang" select="$lang"/>
          </xsl:call-template>
          <div class="break"/>
          <!-- add charges by phone number called to -->
          <xsl:call-template name="call-summary">
            <xsl:with-param name="localeXml" select="$localeXml"/>
            <xsl:with-param name="callsXml" select="$callsXml"/>
            <xsl:with-param name="lang" select="$lang"/>
          </xsl:call-template>
          <div class="break"/>
          <!-- add charges by area code -->
          <xsl:call-template name="area-code-summary">
            <xsl:with-param name="localeXml" select="$localeXml"/>
            <xsl:with-param name="areaCodeXml" select="$areaCodeXml"/>
            <xsl:with-param name="lang" select="$lang"/>
          </xsl:call-template>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
