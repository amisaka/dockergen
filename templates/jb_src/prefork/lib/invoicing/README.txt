# NOTES FOR CREATING INVOICE PDF FILES

# Install software requirements.
./bash/install_requirements.sh

# Create test data and clean raw pdf extracts.
./bash/clean_all.sh

# Create pdfs and intermediate html/css files.
./bash/create_all.sh

# Remove software requirements.
./bash/remove_requirements.sh

