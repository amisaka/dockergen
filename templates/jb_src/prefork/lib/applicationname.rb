$application_names = []
def set_application_name(x)
  query = "set application_name = ?"
  query = ActiveRecord::Base.send :sanitize_sql_array, [query, [x]]
  ActiveRecord::Base.connection.execute(query)
end
def push_application_name(x)
  $application_names << x
  set_application_name(x)
end
def pop_application_name
  x = $application_names.pop
  set_application_name(x)
end
