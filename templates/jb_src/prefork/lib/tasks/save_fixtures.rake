# -*- ruby -*-

namespace :db do

  namespace :fixtures do
    desc "Save fixtures for client named $CLIENT to directory $FIXTUREDIR"
    task :save => :environment do
      dir = ENV['FIXTUREDIR'] || 'tmp'

      if ENV['CLIENT']
        clients = Client.find_all_by_name(ENV['CLIENT'])
      else
        clients = Client.all
      end
      fw = FixtureWriter.new(dir)
      clients.each { |c|
        c.savefixturefw(fw)
      }
    end
  end
end

