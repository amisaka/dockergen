# coding: utf-8
# -*- ruby -*-
require 'csv'
require 'dump_stats'

namespace :sg1 do

  desc "Create list of Clients that have duplicated BTNs"
  task :find_dup_btn => :environment do
    stats = Hash.new(0)
    Client.activated.joins(:btns).group("clients.id").having("count(btns.id) > ?",0).each { |cl|
      next unless cl.crm_account
      print "\n\##{cl.id} working on #{cl.name} #{cl.btns.count}..."
      dedup_btn(cl, stats)
    }
    dump_stats(stats)
  end

  desc "Deduplicate the BTNS associated with CLIENT=xx"
  task :dedup_btn, [:client] => [:environment] do |t,args|
    client_id = args[:client].to_i

    client = Client.find(client_id)
    stats = Hash.new(0)
    stats[:clients_processed] += 1
    dedup_btn(client, stats)
    dump_stats(stats)
  end

  def dedup_btn(client, stats = Hash.new(0))
    btnsbag = Hash.new
    # for each btn, keep it if it has the most phones.
    client.btns.each { |btn|
      key = RateCenter.canonify_number(btn.billingtelephonenumber)

      unless btnsbag[key]
        btnsbag[key] = btn
        next
      end
      next unless btn.active?

      cnt = btn.phones.count
      if cnt > btnsbag[key].phones.count
        btnsbag[key] = btn
        stats[:better_btn_found] += 1
      end
    }

    client.btns.each { |btn|
      key = RateCenter.canonify_number(btn.billingtelephonenumber)

      better_btn = btnsbag[key]
      byebug unless better_btn
      if better_btn != btn
        puts "   Moving (#{btn.phones.count}) phones from #{btn.id} to #{better_btn.id}"
        stats[:replaced_btn] += 1
        btn.phones.each { |ph|
          ph.btn = better_btn
          stats[:phones_moved] += 1
          ph.save!
        }
        btn.delete
      end
    }
  end

  desc "Merge CLIENT=xx into client PARENT=xx as a site, merge with SITE=zzname"
  task :adoptclientassite, [:client, :parent, :site] => [:environment] do |t,args|
    client_id = args[:client].to_i
    parent_id = args[:parent].to_i
    site_name = args[:site]

    client = Client.find(client_id)
    parent = Client.find(parent_id)

    mergetosite = parent.sites.where(site_number: site_name).take
    unless mergetosite
      puts "Can not find #{site_name}"
      exit
    end

    client.sites.each { |site|
      puts "#{client.name} has #{site.name} with #{site.phones.count} phones"
    }

    puts "Moving #{client.name} with #{client.sites.count} sites and #{client.btns.count} btns\n    as new sites for #{parent.name} (merging to  #{mergetosite.site_number}/#{mergetosite.name})"
    parent.adopt_client_as_site_and_merge(client, mergetosite)

  end

    desc "Merge CLIENT=xx into client PARENT=xx as a site"
  task :adoptclient, [:client, :parent] => [:environment] do |t,args|
    client_id = args[:client].to_i
    parent_id = args[:parent].to_i

    client = Client.find(client_id)
    parent = Client.find(parent_id)

    client.sites.each { |site|
      puts "#{client.name} has #{site.name} with #{site.phones.count} phones"
    }

    puts "Moving #{client.name} with #{client.sites.count} sites and #{client.btns.count} as new sites for #{parent.name}"
    parent.adopt_client_as_site(client)

  end

  desc "Mergesite[old,new] such that whichever site has fewest phones is an altname for the other"
  task :mergesite, [:site1, :site2] => [:environment] do |t,args|
    site1_id = args[:site1].to_i
    site2_id = args[:site2].to_i

    site1 = Site.find(site1_id)
    site2 = Site.find(site2_id)

    if site1.client != site2.client
      puts "can not merge sites with differing clients: #{site1.client} vs #{site2.client}"
      exit 1
    end

    Site.mergesites(site1, site2) { |mergesite, keepsite|
      puts "Merging site:#{mergesite.name} (\##{mergesite.id}, cnt=#{mergesite.phones.count}) into site:#{keepsite.name} (\##{keepsite.id}, cnt=#{keepsite.phones.count})"
    }

  end

  desc "Mergesite[old,new] always keeping new"
  task :merge2site, [:site1, :site2] => [:environment] do |t,args|
    site1_id = args[:site1].to_i
    site2_id = args[:site2].to_i

    site1 = Site.find(site1_id)
    site2 = Site.find(site2_id)

    if site1.client != site2.client
      puts "can not merge sites with differing clients: #{site1.client} vs #{site2.client}"
      exit 1
    end

    site2.merge_from(site1)
  end



end
