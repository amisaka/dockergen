# -*- ruby -*-

require 'csv'

namespace :sg1 do

  desc "Create missing OTRS Customer Users for all sites"
  task :otrsusers => :environment do
    OTRS::CustomerCompany.all.each {|cc|
      OTRS::CustomerUser.make_from_site(cc.site);
    };
  end
end
