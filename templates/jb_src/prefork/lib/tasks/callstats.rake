# -*- ruby -*-

def calculate_period
  clientnum = ENV['CLIENT']
  todaystr  = ENV['TODAY']
  endstr    = ENV['ENDDAY']

  alarmtime = nil
  if ENV['ALARMTIME']
    alarmtime = TimeOfDay.parse(ENV['ALARMTIME'])  # time at which to stop processing
    Time.zone = "EST5EDT"
    alarmtime = alarmtime.on Time.zone.now
    puts "Will stop processing at #{alarmtime.to_s(:db)} (now: #{Time.zone.now}: past: #{alarmtime.past?})\n"
  end

  if ENV['BEAUMONT_NOTIFY']
    notify = true
    $stderr.puts "Will notify when new stats are created\n"
  end

  days = []
  if todaystr
    if todaystr == "yesterday"
      today = Time.now.yesterday.to_date
    else
      today = todaystr.to_date
    end
  end
  lastday = today
  if endstr
    lastday = endstr.to_date
  end

  unless today
    # otherwise, it uses Callstat.last_period + 1.
    today    = StatsPeriod.last_period.day + 1.day
    lastday  = Call.scoped.order("startdatetime DESC").first.try(:startdatetime).to_date
    lastday  ||= today
  end
  days = today..lastday
  if (lastday < today)
    days = (lastday..today).to_a.reverse
  end

  return days, today, lastday, alarmtime, notify, clientnum
end

namespace :sg1 do

  desc "Generate calls statistics for CLIENT=foo, or all clients marked stats_enabled. Run this for TODAY=, or for the previous complete day.
"
  task :genstats => :environment do

    # set the application name to make it easier to filter pg_log.
    ::ActiveRecord::Base.connection.execute("set application_name = 'genstats';")
    starting = ENV['TODAY'].try(:to_date)
    unless starting
      ending   = Time.now.beginning_of_day
      starting = ending - 1.day
    end
    ending ||= starting

    puts "Processing from #{starting} to #{ending}"

    clients = Client.activated.where(stats_enabled: true)
    if ENV['CLIENT']
      clientid = ENV['CLIENT'].to_i
      clients = Client.where(id: clientid)
    end

    total = 0
    pm1 = PerformanceStat.measure(name: 'genstats') do
      clients.find_each {|client|
        clienttotal = 0
        pm2 = PerformanceStat.measure(name: 'genstats', client: client) do |pm|
          puts "#{pm.start_time.to_s(:db)} \##{client.id} #{client.name}"
          clienttotal = client.calc_daily_stats(starting, nil, nil)
        end
        pm2.set_work_count!(clienttotal)
        puts pm2.say_rate("calls")
        total += clienttotal
      }
    end
    pm1.set_work_count!(total)
    puts pm1.say_rate("clients")
  end

end

namespace :novavision do


  desc "Generate calls statistics for CLIENT=foo, or all clients, on TODAY= to ENDDAY=. Quit at ALARMTIME="
  task :augmentstats => :environment do
    notify    = false
    cs = nil
    client = nil

    days,today,lastday,alarmtime,notify,clientnum = calculate_period

    if clientnum
      client_arel = Client.scoped.where(:id => clientnum.to_i)
    else
      client_arel = Client.scoped
    end

    days.each { |today|
      puts "Processing #{today}"

      client_arel.find_each { |client|
        print sprintf("[%s]  %14s  client %30s: ", Time.now.to_s(:db), today, client.name[0..29])
        incoming,outgoing = client.augment_call_count(today)
        print sprintf("%05u %05u\n", incoming, outgoing)
      }
    }
  end


  desc "Run daily statistics for client CLIENT=foo"
  task :callstat => :environment do
    notify = false

    clientnum = ENV['CLIENT']
    today     = ENV['TODAY'] || Time.now.beginning_of_day
    if ENV['BEAUMONT_NOTIFY']
      notify = true
      $stderr.puts "Will notify when new stats are created\n"
    end

    today = today.to_date

    client = Client.find(clientnum.to_i)

    client.do_daily_stats({today: today, notify: notify})
  end



  desc "Run weekly summary and per extension breakdown for CLIENT=foo, from STARTDAY to LASTDAY (7 days by default), optionally for just HOUR="
  task :weekSummaryAndExtensionBreakdown => :environment do

    options = { notify: false }

    ::ActiveRecord::Base.connection.execute("set application_name = 'weekSummary';")
    Time.zone = ENV['TZ'] || 'America/New_York'

    clientnum = ENV['CLIENT']

    unless clientnum
      puts "Please set CLIENT=number, LASTDAY = lastday, optionally: STARTDAY=lastday, HOUR=xx"
      puts "   it not set, LASTDAY = today"
      puts "   it not set, STARTDAY = today - 6 days (total of 7 days)"
      exit 3
    end

    lastDay = 0; firstDay = 0;
    if ENV['LASTDAY']
        lastDay = Time.parse(ENV['LASTDAY']).to_datetime
    elsif
        lastDay = DateTime.now.beginning_of_day
    end

    if ENV['STARTDAY']
        firstDay = Time.parse(ENV['STARTDAY']).to_datetime
    elsif
        firstDay = lastDay - 6.days
    end
    options[:today]   = firstDay
    options[:enddate] = lastDay

    if ENV['HOUR']
      options[:hour]  = ENV['HOUR']
    end

    if ENV['BEAUMONT_NOTIFY']
      options[:notify] = true
      $stderr.puts "Will notify when new stats are created\n"
    end

    today = lastDay.to_date
    calcstats = Hash.new(0)
    output = $stdout

    client = Client.find(clientnum.to_i)

    # This does the per day stats.
    client.do_extension_stats(lastDay, nil, firstDay,
                              options[:hour], options[:notify],
                              calcstats, output)
    output.puts
    output.puts
	# This does the extensions
    client.do_daily_stats(options)


    # Print the legend summary
    puts
    Client.print_summary_stats_legend
  end



  desc "Run daily statistics for client CLIENT=foo for entire year, write reports to DIR=dir/cfoo_*"
  task :callstat_for_year => :environment do
    notify = false

    clientnum = ENV['CLIENT']
    notify    = !ENV['VERBOSE'].nil?
    year      = ENV['YEAR'] || Time.now.year
    dir       = ENV['OUTDIR'] || File.join(ENV['HOME'], 'reports')

    period = YearlyStatsPeriod.by_year(year).build
    daybegin = period.beginning_of_period.to_date
    dayend   = period.end_of_period.to_date

    client = Client.find(clientnum.to_i)

    (daybegin..dayend).each {|day|
      filename = sprintf("%s/c%03u_%04u_%02u_%02u.csv",
                         dir, client.id, day.year, day.month, day.mday)
      File.open(filename, "w") do |file|
        $stdout = file
        client.do_daily_stats({today: day, notify: notify, output: file})
      end
    }
  end

  desc "Calculate voicemail situation for all clients"
  task :voicemailcalc => :environment do
    count=0
    total = Call.voicemail_not_calc.count
    Call.voicemail_not_calc.find_each do |call|
      print " #{count}/#{total} processed\r" if (count % 1000)==0
      count += 1

      call.calculate_voicemail!
      call.save!
    end
  end

  desc "Run yearly report for client CLIENT=foo YEAR=year"
  task :yearlystat => :environment do
    clientnum = ENV['CLIENT']
    year      = ENV['YEAR'] || Time.now.year
    client = Client.find(clientnum.to_i)

    client.do_year_stats(year)
  end

  desc "Run yearly report for client CLIENT=foo YEAR=year PHONE=id"
  task :weeklystat => :environment do
    clientnum = ENV['CLIENT']
    year      = ENV['YEAR'] || Time.now.year
    phone_did  = ENV['PHONE']
    client = Client.find(clientnum.to_i)
    if phone_did
      phone = client.phones.find_by_phone_number(phone_did)
    else
      phone = nil
    end

    client.do_weekly_stats(year, phone)
  end

  def calc_last_week(todaystr)
    if todaystr == "lastweek"
      today = Time.now - 1.week
    elsif todaystr
      today = todaystr.to_date
    else
      today = Time.now
    end

    return today
  end

  desc "Run hourly report for client CLIENT=foo TODAY={2012-02-29,lastweek} [PHONE=id]"
  task :weeklyhourstat => :environment do
    clientnum = ENV['CLIENT']

    todaystr = ENV['DAY'] || ENV['TODAY'] || "lastweek"
    today = calc_last_week(todaystr)

    phone_did  = ENV['PHONE']
    client = Client.find(clientnum.to_i)
    if phone_did
      phone = client.phones.find_by_phone_number(phone_did)
    else
      phone = nil
    end

    client.do_detailed_extension_stats(today, phone)
  end

  desc "Run week summary report for client CLIENT=foo TODAY={2012-02-29,lastweek} [PHONE=id]"
  task :weeklysummarystat => :environment do
    clientnum = ENV['CLIENT']

    todaystr = ENV['DAY'] || ENV['TODAY'] || "lastweek"
    today = calc_last_week(todaystr)

    phone_did  = ENV['PHONE']
    client = Client.find(clientnum.to_i)
    if phone_did
      phone = client.phones.find_by_phone_number(phone_did)
    else
      phone = nil
    end

    client.do_extension_stats(today, phone)
  end

  desc "Run hourly report for client CLIENT=foo PHONE=id for entire YEAR=year"
  task :yearlyhourstat => :environment do
    clientnum = ENV['CLIENT']
    year      = ENV['YEAR'] || Time.now.year
    phone_did  = ENV['PHONE']
    client = Client.find(clientnum.to_i)
    if phone_did
      phone = client.phones.find_by_phone_number(phone_did)
      unless phone
        abort "Did not find #{phone_did} in client #{client}(\##{clientnum})"
      end
    else
      phone = client
    end

    client.do_year_extension_stats(year, phone)
  end

  desc "Run yearly report for all clients for YEAR=year"
  task :yearlyclientstat => :environment do
    year      = ENV['YEAR'] || Time.now.year

    Client.do_perclient_yearly_stats(year)
  end

  desc "Generate sequence number values for callstats compute server"
  # run this on the primary database system in order to allocate
  # sequence numbers for a new computer server
  #
  # allocate some sequences for a compute system to use.
  #   cdrviewer_cdr and calls
  # are not allocated, as they will get dropped on compute server.
  #
  # will allocate the following:
  #   stats_periods will gain 2 weeks, 14 days
  #                 each day will have 24+1 * 3 = 75
  #   so total will be 1050.  Allocate 4096 for good luck.
  #
  #   callstats will gain 2 weeks, 1050 per client, and 1050 per extension.
  #                 there are 255 clients, call it 1024.
  #                 there are 2716 phones, call it 4096.
  #   so callstats will need 1050 * 1024 + 1050 * 4096 = 5376000 ids.
  #
  task :allocseqnum => :environment do
    callstat_id     = Callstat.allocate_id(1050 * 1024 + 1050 * 4096)
    statsperiods_id = StatsPeriod.allocate_id(4096)

    puts "SELECT pg_catalog.setval('callstats_id_seq',    #{callstat_id}, true);"
    puts "SELECT pg_catalog.setval('stats_periods_id_seq', #{statsperiods_id}, true);"
  end

  desc "Generate a series of scripts for 26 servers"
  task :server26 => :environment do
    year      = ENV['YEAR'] || Time.now.year

    # get period for work
    period = YearlyStatsPeriod.by_year(year).first
    unless period
      period = YearlyStatsPeriod.by_year(year).build
    end

    daybegin = period.beginning_of_period.to_date
    dayend   = period.end_of_period.to_date

    range = (daybegin..dayend).to_a
    totaldays = range.size
    dayspershard = (totaldays / 26).ceil

    #puts "#{daybegin} to #{dayend} (#{totaldays}) has #{dayspershard}"

    first = 0
    last  = dayspershard
    (1..26).each { |shard|
      shard = sprintf("%02u", shard)
      File.open("../data/2012-reload/nv-serv#{shard}.txt", "w") do |f|
	day00 = range[first] - 1.day
	day15 = range[last]  + 1.day
	(day00..day15).each { |day|
	  year  = day.year
	  month = day.month
	  daytos = day.to_s(:db).gsub('-','')
	  f.puts sprintf("%s %s %s %04u %02u %s %s", shard, daytos, day, year, month, range[first], range[last])
	}
      end

      first += dayspershard
      last  += dayspershard
      if last >= totaldays
	last = totaldays-1
      end
    }
  end

end


namespace :sg1 do
  desc "Run weekly summaries, from FIRSTDAY=YYYYMMDD to ENDDAY=, MONDAY=X for CLIENT=ZZ"
  task :weeklySummariesWithExtensionBreakdown => :environment do

    options = { notify: false }

    # this makes it clearer which database daemon is performing work for this task
    ::ActiveRecord::Base.connection.execute("set application_name = 'weekSummary';")
    Time.zone = ENV['TZ'] || 'America/New_York'

    clientnum = ENV['CLIENT']
    lastDay = 0; firstDay = 0;
    if ENV['LASTDAY']
        lastDay = Time.parse(ENV['LASTDAY']).to_datetime
    elsif
        lastDay = DateTime.now.beginning_of_day
    end

    if ENV['STARTDAY']
        firstDay = Time.parse(ENV['STARTDAY']).to_datetime
    elsif
        firstDay = lastDay - 6.days
    end
    options[:today]   = firstDay
    options[:enddate] = lastDay

    if ENV['HOUR']
      options[:hour]  = ENV['HOUR']
    end

    if ENV['BEAUMONT_NOTIFY']
      options[:notify] = true
      $stderr.puts "Will notify when new stats are created\n"
    end

    today = lastDay.to_date
    calcstats = Hash.new(0)
    output = $stdout

    client = Client.find(clientnum.to_i)

    # This does the per day stats.
    client.do_extension_stats(lastDay, nil, firstDay,
                              options[:hour], options[:notify],
                              calcstats, output)
    output.puts
    output.puts

    # This does the extensions
    client.do_daily_stats(options)


    # Print the legend summary
    puts
    Client.print_summary_stats_legend
  end


  desc "Find client number by NAME=regex"
  task :clientsearch => :environment do
    name = ENV['NAME']
    patt = Regexp.new(name)
    Client.all.each { |cl|
      if(patt =~ cl.name)
        activeword = "active"
        activeword = "inactive" unless cl.active?
        puts "  #{cl.name} #{cl.id} #{activeword}.\n"
      end
    }
  end
end

