# -*- ruby -*-

namespace :sg1 do

  desc "Import price list level plans for RATE_PLAN=XX from FILE=XX"
  task :import_rates => :environment do
    rate_plan = nil
    plan_name = ENV['RATE_PLAN']
    if plan_name
      rate_plan = RatePlan.where(name: plan_name).take
    end
    unless rate_plan
      puts "Must set RATE_PLAN=XX to valid plan, (not #{ENV['RATE_PLAN']})"
      exit 1
    end

    file = ENV['FILE']
    unless file
      puts "Must set FILE=path to valid file, (not #{ENV['FILE']})"
      exit 1
    end

    puts "Importing #{ENV['FILE']} to rate plan #{rate_plan.name}"

    stats = Hash.new(0)
    result = Rate.import_file(file, rate_plan, stats)

    stats.each { | key, value |
      puts "  #{key} = #{value}"
    }
    exit 0 if result
    exit 1

  end

  desc "Re-assign rate centers to all CDRs on DATE=foo"
  task :recalc_rate_centers => :environment do
    unless ENV['DATE']
      puts "Must set DATE=YYYY-MM-DD"
      exit
    end
    analysis_day = ENV['DATE'].to_date
    str = analysis_day.strftime("%Y%m%d")
    puts "Processing on date: #{str.to_s}"
    cdrlist = []

    Datasource.where(["datasource_filename LIKE ?", "BW-CDR-#{str}%"]).find_each { |ds|
      cdrs = ds.cdrs.count
      print "DS #{ds.id} has #{cdrs} to process..."
      dsprocessed = ds.recalculate_toll_data
      if dsprocessed
        puts "updated"
        cdrlist << [ ds.id, ds.datasource_filename ]
      else
        puts "unchanged"
      end
    }

    cdrlist.each { |list|
      puts list[1]
    }
  end

  def findplan(var)
    plan_name = ENV[var]
    plan= RatePlan.find_by_name(plan_name)
    unless plan
      puts "Can not find plan: #{ENV[var]}"
      exit 10
    end
    plan
  end

  desc "Set the rate plan for a BTN=XX to PLAN=YY"
  task :set_rate_plan => :environment do
    bname = ENV['BTN']

    if bname
      btns = Btn.active.where(billingtelephonenumber: bname)
      unless btns.count > 0
        puts "  Can not find btn#{bname}, trying it as DID"
        phone = Phone.where(phone_number: bname)
        if phone
          puts "    Phone #{phone.name} has btn: #{phone.btn.name}.."
          btns = [phone.btn]
        else
          puts "Not found as DID"
        end
        exit 5
      end
    else
      btns = Btn.all
    end

    local_plan   = findplan('PLAN_LOCAL')
    national_plan= findplan('PLAN_NATIONAL')
    world_plan   = findplan('PLAN_WORLD')

    btns.each { |btn|
      btn.local_rate_plan = local_plan
      btn.national_rate_plan = national_plan
      btn.world_rate_plan = world_plan
      puts "Updating btn #{btn.id} #{btn.name} for client #{btn.client.name}"
      btn.save!
    }
  end

  def column_name_to_number(x)
    if x =~ /[0-9]+/
      column = x.to_i
    elsif x =~ /[[:alpha:]]/
      column = x[0].upcase.each_byte.first - 65
    end
    return column
  end


  desc "Import a CSV file FILE=XX, extract COLUMN=XX, and determine long distance status for number. Also totals TOTCOL1 and TOTCOL2, and TOTCOL3"
  task :rate_plan_analyze_file => :environment do
    column    = ENV['COLUMN']
    filename  = ENV['FILE']
    totcol1 = nil; total1 = 0
    totcol2 = nil; total2 = 0
    totcol3 = nil; total3 = 0

    puts "Analyzing data from #{filename} using column #{column}"

    column = column_name_to_number(column)
    if ENV['TOTCOL1']
      totcol1 = column_name_to_number(ENV['TOTCOL1'])
    end
    if ENV['TOTCOL2']
      totcol2 = column_name_to_number(ENV['TOTCOL2'])
    end
    if ENV['TOTCOL3']
      totcol3 = column_name_to_number(ENV['TOTCOL3'])
    end
    if totcol1 || totcol2 || totcol3
      puts "Totally data in columns: #{totcol1}, #{totcol2}, #{totcol3}"
    end

    handle = File.open(filename, "r:ISO-8859-1:UTF-8")
    return nil unless handle

    linecount=0
    loadedcount=0

    CSV.parse(handle) do |row|
      linecount += 1
      toll = "toll"

      callednumber = row[column]
      #puts "looking up #{callednumber}"

      # find the appropriate LocalExchange
      na = NorthAmericanExchange.findbyphone(callednumber)

      ratename='unknown'
      if na
        les= na.local_exchanges
        #puts "found north american exchange #{na.id} with local exchange #{les.count}"
        rate_plan = RatePlan.default
        local = les.any? { |le|
          result = rate_plan.acceptable_rate_center(le)
          #print "found north american exchange #{na.id} => #{result}\n"
          result
        }
        if local
          toll = "local"

          if totcol1
            total1 += row[totcol1].to_f
          end
          if totcol2
            total2 += row[totcol2].to_f
          end
          if totcol3
            total3 += row[totcol3].to_f
          end
        end
        ratename=na.name
      end

      print "#{linecount},#{ratename},#{callednumber},#{toll},#{total1},#{total2},#{total3}\n"
    end

  end

end
