# -*- ruby -*-

namespace :sg1 do

  desc "Dumps all phones for CLIENT=xx along with site and site number"
  task :phones_with_sites => :environment do
    clientno = ENV['CLIENT'].to_i
    client = Client.find(clientno)

    CSV.open("tmp/phones_#{clientno}.csv", "wb") do |csv|
      csv << ["phone.id", "phone.notes", "phone_number", "ext", "site.name", "site.site_number", "site.id"]
      client.sites.activated.each { |site|
        site.phones.activated.by_extension.each { |phone|
          csv << [phone.id, phone.notes, phone.phone_number, phone.extension, site.name, site.site_number, site.id]
          if phone.phone_number == phone_sip_username
            same = ""
          else
            same = "X"
          end
          csv << [phone.id, phone.notes, phone.phone_number, phone_sip_username, same, site.name, site.site_number, site.id]
        }
      }
    end
  end

  desc "Cleans up clients which have duplicate billing_btn by merging them"
  task :clientdeaddelete => :environment do
    Client.activated.includes([:btns, :sites]).find_each { |client|
      # does the client even have a valid name?
      if client.name.blank?
        print "Client #{client.id} has no name (#{client.sites.count} sites, #{client.btns.count} btns)"
        phcount = 0
        client.btns.find { |btn|
          phcount += btn.phones.activated.count
        }

        if phcount == 0
          print " ...terminating\n"
          client.terminated!
          client.sites.find { |site|
            site.terminated!
            site.phones.activated.find_each {|ph|
              ph.terminated!
            }
          }
          client.btns.find { |btn|
            btn.phones.activated.find_each {|ph|
              ph.terminated!
            }
          }
          if client.crm_account
            client.crm_account.terminated!
          end
        else
          print "... phones(#{phcount})\n"
        end
      end
    }
  end

  desc "Cleans up crm_accounts which are not referenced"
  task :crmcleanup => :environment do
    CrmAccount.clean_unref
  end

  desc "Cleans up clients which have duplicate billing_btn by merging them"
  task :clientdedup => :environment do
    Client.duplicate_list { |client|
      duplicates = Client.where(:billing_btn => client.billing_btn)
      sites = Hash.new
      btn_list = Hash.new
      primaryclient = nil
      foreign_pid = nil
      crm_account = nil

      puts "Working on client #{client.name}/#{client.id}"

      # look for lowest numbered client which has a
      # crm_account that matches billing_btn.
      # pick that client as the primaryclient, and move all phones,sites,btns
      # to it, also move the foreign_pid
      duplicates.find_each {|nc|
        foreign_pid ||= nc.foreign_pid
        crm_account ||= nc.crm_account
        primaryclient=nc unless primaryclient

        puts "  considering #{nc.name}/#{nc.id}/#{nc.crm_account_id}"
        if primaryclient
          if nc.crm_account and nc.crm_account_id == nc.billing_btn and primaryclient.crm_account.nil?
            primaryclient = nc
          end

          callcount = primaryclient.calls.count
          ncallcount= nc.calls.count

          if ncallcount > callcount
            primaryclient = nc

          elsif primaryclient.sites.count < nc.sites.count
            primaryclient = nc

          # all other things being equal, keep lowest numbered
          elsif primaryclient.id > nc.id
            primaryclient = nc
          end
        end

        nc.btns { |btn|
          if btn_list[btn.billingtelephonenumber]
            obtn = btn_list[btn.billingtelephonenumber]
            if btn.id < obtn.id
              # swap them
              obtn,btn = btn,obtn
            end
            obtn.phones { |ph| ph.btn = btn; ph.save }
            obtn.delete
          end
          btn_list[btn.billingtelephonenumber] = btn
        }
      }

      puts "  client #{client.name}/\##{client.try(:id)} picked \##{primaryclient.try(:id)} as primary (foreign_pid:#{foreign_pid} crm:#{crm_account.try(:id)})"

      byebug unless client
      byebug unless primaryclient

      # now merge all sites and btns into primaryclient, watching for duplicate btns.
      # sites will get cleaned up by sitecleanup afterwards.
      duplicates.find_each {|nc|
        next if nc.id == primaryclient.id
        puts " removed #{nc.name}/\##{nc.id} entry"
        nc.transferto(primaryclient)
        nc.crm_account.delete if nc.crm_account
        nc.delete
      }
      primaryclient.foreign_pid = foreign_pid
      primaryclient.crm_account = crm_account
      primaryclient.save!
    }
  end

  desc "performs garbage collect on OTRS.customer_users"
  task :cucleanup => :environment do
    # set marks to 1 for accounts which are not real.
    total = OTRS::CustomerUser.where(pw: nil).count
    OTRS::CustomerUser.where(pw: nil).update_all(pw: '1')

    # now set marks to nil for entries that have a Site pointing at them.
    OTRS::CustomerUser.where(pw: '1').joins(:sites).update_all(pw: nil)

    # now delete anything with pw: 1
    count = OTRS::CustomerUser.where(pw: '1').count
    puts "Cleaning #{count} OTRS::CustomerUsers of #{total}"
    OTRS::CustomerUser.where(pw: '1').delete_all
  end

  desc "Cleans up sites with nil site_numbers, fixing billing site IDs and removing sites with no phones"
  task :sitecleanup => :environment do

    Site.where(:site_number => '0').find_each { |site|
      if site.site_rep
        site.site_rep.delete
      end
      site.delete
    }

    Client.activated.find_each { |client|

      sitecount = client.sites.count
      next if sitecount == 0
      if sitecount == 1
        site1 = client.sites.first
        next unless site1.site_number.blank?

        puts "Client #{client.name}/\##{client.id} lone site: #{site1.id} has nil site number... fixed"
        site1.site_number = '1'
        site1.save!
        site1.make_otrs_user
        next
      end

      if client.billing_site
        if client.billing_site.site_number.blank?
          client.billing_site.site_number = 'Billing'
          client.billing_site.save!
          puts "Client #{client.name}/#{client.id} has nil billing_site number \##{client.billing_site.id}, fixing"
        end
      end

      # we have more than one site, see if we can nuke any of them.
      # first clean out sites with no phones and no names, make list of names.
      highestname = client.cleanoutdeadsites

      # reload to purge deleted sites.
      client.reload

      client.site_uniquify
    }
  end
end
