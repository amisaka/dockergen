# -*- ruby -*-

namespace :sg1 do

  desc "Report on a CLIENT=XX, the list of sites, and the count of phones in each of those sites"
  task :client_phone_report => :environment do

    if ENV['CLIENT']
      clientnum = ENV['CLIENT'].try(:to_i)
      client = Client.find_by_id(clientnum)

      unless client
        puts "Can not find client with #{ENV['CLIENT']} id, please set CLIENT= properly"
        exit 1
      end

      clients = [client]
    elsif ENV['PATTERN']
      clients = []
      patt = Regexp.new(ENV['PATTERN'])
      Client.all.each { |cl|
        if(patt =~ cl.name)
          clients << cl
        end
      }
    else
      puts "Please set PATTERN= or CLIENT="
      exit 2
    end

    CSV($stdout) do |csv|
      csv << ["client.name", 'client.id', 'client.billing_btn', 'site.name', 'site.id', 'site.site_number', 'site.phones.count']
      clients.each { |client|
        csv << []
        #puts "Processing CLIENT=#{client.name}"

        if !client.active?
          csv << ["Client #{client.name} has been marked inactive"]
        end
        client.sites.activated.find_each { |site|
          csv << [client.name, client.id, client.billing_btn, site.name, site.id, site.site_number, site.phones.count]
        }
      }
    end
  end
end
