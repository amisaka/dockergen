# -*- ruby -*-

namespace :sg1 do
  desc "Export Billing for CLIENT=xx or all clients (FINALINVOICE=false, SKIPPDF=false)"
  task :billclient => :environment do

    nopdf = false
    if ENV['CLIENT']
      clientnum  = ENV['CLIENT']

      unless clientnum
        puts "Please set CLIENT=xxx"
        exit 1
      end

      clients = Client.where(id: clientnum.to_i)
    elsif ENV['CLIENTMIN']
      clientmin = ENV['CLIENTMIN'].to_i
      clients = Client.activated.where(["id >= ?", clientmin])
    else
      clients = Client.activated
    end

    if ENV['MONTH'] and ENV['YEAR']
      month = ENV['MONTH'].to_i
      year  = ENV['YEAR'].to_i

    else
      now = DateTime.now
      year  = now.year
      month = now.month
    end

    if ENV['OUTDIR']
      outdir = ENV['OUTDIR']
    else
      outdir = $InvoiceOutputDir || Rails.root.join('tmp','invoices')
    end

    if ENV['SKIPPDF']=='true'
      nopdf = true
    end

    unless ENV['FINALINVOICE']
      interim_invoice = true
    end

    unless ENV['BACKGROUND']
      puts "Running invoices in the foreground"
      # run the invoice inline
      Rails.application.config.active_job.queue_adapter = :inline
    end

    invoice_debug = false
    invoice_debug = true if ENV['DEBUG']
    invoice_debug = 'stop'    if ENV['DEBUG']=='stop'
    invoice_debug = 'profile' if ENV['DEBUG']=='profile'

    FileUtils.mkdir_p(outdir)

    puts "Invoices (#{clients.count}) being writing to #{outdir}"

    lastbtn = nil
    lastbtnname = nil
    failed = 0
    inv_count = 0
    total = 0
    clients.find_each { |client|
      client.btns.active.each { |btn|
        total += 1
      }
    }

    num = 0

    totbtn = Btn.active.count
    totbtn_invoice = Btn.active.invoice_enabled.count
    puts "There are #{totbtn_invoice} btns with invoicing enabled out of #{totbtn}"

    options = Hash.new(false)
    options.merge!({ skippdf: nopdf,
                     invoice_debug: invoice_debug.to_s,
                     interim_invoice: interim_invoice })

    clients.find_each { |client|
      client.btns.active.invoice_enabled.each { |btn|
        #byebug if lastbtn == btn || lastbtnname == btn.name

        print sprintf("%04d/%04d ", num, total)
        num += 1

        if !btn.invoice_template
          puts "cl=#{client.id} / btn=#{btn.id}    skipped, no template set"
          next
        end

        inv_count += 1

        invoice = btn.invoice_service_for(year, month)
        invoice.invoice_debug = invoice_debug

        print "#{inv_count}\# cl=#{client.id} / btn=#{btn.id} inv\#=#{invoice.id} [#{btn.phones.activated.count},#{invoice.billable_services.count}] "
        cnt = invoice.billable_services.count
        if !invoice.invoice_run
          puts "No invoice run setup for #{year}/#{month}"
          exit
        end
        #next if invoice.billable_services.count == 0

        puts "using #{invoice.invoicenumber} created for #{client.name}/#{invoice.btn.name}"
        invoice.generate_invoice_to(outdir, options)
        lastbtn = btn
        lastbtnname = btn.name
      }
    }
    puts "Bill #{inv_count} clients"
  end

  desc "Remove calls that are within the same rate center"
  task :remove_intra_rate => :environment do
    unless ENV['MONTH'] and ENV['YEAR']
      puts "Must set MONTH=xx and YEAR=xxxx"
      exit 1
    end

    month = ENV['MONTH'].to_i
    year  = ENV['YEAR'].to_i
    beginmonth = Date.new(year, month, 1)
    things = Cdr.where("startdatetime > ?", beginmonth).tollcalls
    total = things.count
    cnt   = 0
    fixed = 0
    puts "Fixing #{total} calls from #{beginmonth.to_s}"

    things.find_each { |cdr|
      cnt += 1
      if cdr.calc_derived(1)
        cdr.save!
        fixed += 1
      end
      if ((cnt % 100) == 0)
        print sprintf("   process cdr:%08d fixed=%06d / cnt=%06d / total = %08d\r",
                      cdr.id, fixed, cnt, total)
      end
    }
    print sprintf("\nTotal                   fixed=%06d / cnt=%06d / total = %08d",
                  fixed, cnt, total)
  end

  def setup_invoice_run(year, month)
    ir = InvoiceRun.invoice_run_for(year, month)

    puts "Found invoice run #{ir.id} with previous run #{ir.previous_run.try(:id)}"
    unless ir.previous_run
      date = Date.new(year, month, 1)
      previous_date = date - 1.month
      puts "  Previous run not found, looking for it at #{previous_date}"
      pr = InvoiceRun.invoice_run_for(previous_date.year,
                                      previous_date.month)
      puts "Attaching invoice run #{ir.id} to #{pr.id} at #{pr.invoice_year} #{pr.invoice_month}"
      ir.previous_run = pr
    end
    ir.save!
  end

  desc "Create new invoice run for MONTH=xx YEAR=yyyy, [CLIENT=dddd]"
  task :setup_invoicerun => :environment do
    unless ENV['MONTH'] and ENV['YEAR']
      puts "Must set MONTH=xx and YEAR=xxxx"
      exit 1
    end

    month = ENV['MONTH'].to_i
    year  = ENV['YEAR'].to_i

    setup_invoice_run(year, month)
  end

  desc "Process new invoice run for MONTH=xx YEAR=yyyy"
  task :invoicerun => :environment do
    unless ENV['MONTH'] and ENV['YEAR']
      puts "Must set MONTH=xx and YEAR=xxxx"
      exit 1
    end

    month = ENV['MONTH'].to_i
    year  = ENV['YEAR'].to_i

    template_id = nil
    if ENV['TEMPLATE']
      template_id  = ENV['TEMPLATE'].to_i
    end

    setup_invoice_run(year, month)
    ir = InvoiceRun.invoice_run_for(year, month)

    puts "Generating invoices for #{ir.id} from #{ir.previous_run.id}"
    cnt = ir.previous_run.renew_invoices(ir, template_id)
    puts "Creating #{cnt} new items"
  end

  desc "Remove foreign_pid from renewed billable services"
  task :fix_bs_foreign_pid => :environment do
    BillableService.where.not(foreign_pid: nil).where(renewal_id: nil).find_each { |bs|
      cnt = 0
      bs1 = bs.renewalof
      while bs1
        cnt += 1
        bs1.foreign_pid = nil
        bs1.save!
        bs1 = bs1.renewalof
      end
      puts "fixing bs=#{bs.id} #{bs.service_start} history #{cnt} times"
    }
  end

  desc "import payments"
  task :import_payments => :environment do
    file    = ENV['FILE']
    effective_date = ENV['DATE'].to_date

    stats = Hash.new(0)
    CreditMemo.import_balance_file({file: file, effective_date: effective_date}, stats)
    stats.keys.each {|thing|
      value = stats[thing]
      puts "  Stat: #{thing} count: #{value}"
    }
  end

end

