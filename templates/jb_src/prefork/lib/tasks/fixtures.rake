# -*- ruby -*-

namespace :db do
  namespace :fixtures do
    desc "Load a single fixture by setting FIXTURE=name of fixture you wish to laod."
    task :load_single => :environment do
      require 'active_record/fixtures'
      ActiveRecord::Base.establish_connection(ActiveRecord::Base.configurations[RAILS_ENV])
      files=ENV['FIXTURE'].split(/,/)
      puts "Loading #{files}.."
      Fixtures.create_fixtures("spec/fixtures", files)
      puts "Fixture #{files} loaded in to #{RAILS_ENV} environment."
    end
  end
end
