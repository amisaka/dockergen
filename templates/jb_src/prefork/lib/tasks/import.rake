# coding: utf-8
# -*- ruby -*-
require 'csv'

namespace :sg1 do
  desc "Import CUSTOMERS= and DEPARTMENTS= from CSV files [IMPORT_DEBUG=true]"
  task :customerimport => :environment do
    customers   = ENV['CUSTOMERS']
    departments = ENV['DEPARTMENTS']

    if customers.blank? or departments.blank?
      puts "Need CUSTOMERS= and DEPARTMENTS="
      exit 2
    end

    clientlist = Hash.new

    puts "\nImporting customers from file #{customers}"
    custcount = Client.import_file({ file: customers, clientlist: clientlist })
    if custcount == 0
      print "Customers not imported correctly\n"
      exit 1
    end

    puts "\nImporting departments from file #{departments}"
    deptcount = Site.import_file({ file: departments, clientlist: clientlist })
    if deptcount == 0
      print "Departments not imported correctly\n"
      exit 1
    end

    puts "Loaded #{custcount} customers with #{deptcount} departments"
  end

  desc "Import ACOMBA= from CSV files [VERBOSE=true] (FORMAT = AcombaReport1)"
  task :acombaimport => :environment do
    customers   = ENV['ACOMBA']
    verbose = false
    verbose     = ENV['VERBOSE'] if !ENV['VERBOSE'].blank?
    filterbad   = ENV['BROKEN'] == 'true'
    btnfilter   = ENV['BTNFILTER'] != 'false'    # default to enabled
    fixbadendings = ENV['FIXBADENDING']=='true'

    if customers.blank?
      puts "Need ACOMBA=file.csv"
      exit 2
    end

    stats = Hash.new(0)

    iofile = File.open(customers, "r:ISO-8859-1:UTF-8")

    if fixbadendings
      STDERR.puts "Fixing line endings"
      IO::popen("-") { |pipe|
        unless pipe
          # pipe is nil in child.
          STDERR.puts "Child process: #{$$}"
          $0 = "fixing line endings for #{customers}"
          lineno = 0
          iofile.each_line { |line|
            #STDERR.puts "processing #{line}"
            line = line.chomp
            unless line =~ /\"$/
              unless line =~ /\,$/
                STDERR.puts "fixing line #{lineno}"
                line += '"'
              end
            end
            puts line
            lineno += 1
          }
        else
          # pipe is non-nil in parent, so capture it and return it for use below.
          iofile = pipe

          # must process this inside the block, because pipe gets closed once the
          # block returns.
          puts "\nImporting customers from file #{customers} VERBOSE=#{verbose}"
          Client.import_acomba_file({ handle: iofile, stats: stats, overwrite: true,
                                      btnfilter: btnfilter,
                                      verbose: verbose, filterbad: filterbad })
        end
      }
    else
      puts "\nImporting customers from file #{customers} VERBOSE=#{verbose}"
      Client.import_acomba_file({ handle: iofile, stats: stats, overwrite: true,
                                  btnfilter: btnfilter,
                                  verbose: verbose, filterbad: filterbad })
    end


    stats.keys.each {|thing|
        value = stats[thing]
        puts "  Stat: #{thing} count: #{value}"
      }

  end

  desc "Set the TEMPLATE=invoice_template_id for a FILE=file of BTNs."
  task :invoice_template => :environment do
    btnlist = ENV['FILE']
    verbose     = ENV['VERBOSE'] if !ENV['VERBOSE'].blank?
    template_id = ENV['TEMPLATE'].to_i

    total = 0
    created=0

    f = File.open(btnlist, "r")
    f.each_line do |line|
      line.strip!
      btn = Btn.find_or_make(line)
      btn.invoice_template_id = template_id
      created += 1 if btn.client.blank?

      btn.save!
      total += 1
    end

    puts "Total lines: #{total}\n"
    puts "Created:     #{created}\n"

  end

  desc "Set the TEMPLATE=invoice_template_id for a CSVFILE=file of account info"
  task :invoice_template_from_csv => :environment do
    btnlist     = ENV['CSVFILE']
    verbose     = ENV['VERBOSE'] if !ENV['VERBOSE'].blank?
    template_id = ENV['TEMPLATE'].to_i

    total = 0
    created=0

    puts "Importing #{btnlist} to #{Rails.env}"
    handle = File.open(btnlist, "r:UTF-8")

    CSV.parse(handle) do |row|
      (account_status, accountnum, name) = row
      next if account_status == "Account Status"

      btn = Btn.find_or_make(accountnum)
      btn.invoice_template_id = template_id
      if btn.client.blank?
        created += 1
        puts "     created btn: #{accountnum} as #{btn.name}"
      end

      btn.save!
      total += 1
    end

    puts "  Total lines: #{total}\n"
    puts "  Created:     #{created}\n"

  end

  desc "Import addresses and totals from PDF extraction of legacy system"
  task :pdf_extraction => :environment do
    addressfile = ENV['ADDRESS']
    servicefile = ENV['SERVICE']
    statsfile   = ENV['STATS']
    date        = ENV['INVOICEDATE'].to_date

    invoices = Hash.new
    File.open(addressfile, 'r:utf-8') do |f|
      f.each_line { |line|
        (invoicenum, address, other) = line.split(/:/)
        (clientname, addr1, addr2, city_prov_code, other1, other2, na, invoice) = other.split(/\|/)

        client = Client.find_or_make(clientname)
        invoices[invoicenum] = { client: client, name: clientname, addr1: addr1, addr2: addr2, cityprovcode: city_prov_code }
        print sprintf("Client: %5u %-20s                          \n", client.id, clientname)
      }
    end

    puts "\n"

    File.open(servicefile, 'r') do |f|
      f.each_line { |line|
        (invoicenum, service, other) = line.split(/:/)

        things = other.split(/\|/)

        if things[0] == '01/03/17 - 3'
          # then it looks like:
          # 01/03/17 ­ 3|/03/17|VLAN 100M bande passante contrat 5 ans|1|0,00
          servicedesc = things[2]
          qty         = things[3]
          price       = things[4].gsub(',','.').to_f
        elsif things[2] == '01/03/17 ­ 31/03/17'
          # then it looks like:
          # 514­274­8356||01/03/17 ­ 31/03/17|Service 911 VoIP ­ frais d'accès mensuel|1|0,75 |1­1
          servicename = things[]
          # XXXX
        end

        invoice = invoices[invoicenum]
        byebug unless invoice

        invoice[:items] ||= []
        item = { servicename: servicename, desc: servicedesc, dates: servicedates, qty: qty, price: price }
        invoice[:items] << item
        print sprintf("Add %-20s to %-20s                          \n", servicedesc, invoice[:client]);
      }
    end

    byebug

    File.open(statsfile, 'r') do |f|

    end

  end

  desc "Import ACOMBA address update from CSV file [VERBOSE=true]"
  task :acomba_address, [:file] => [:environment] do |t,args|
    file = args[:file]

    handle = File.open(file, "r:UTF-8")
    unless handle
      puts "Can not open: #{file}: $!\n"
      exit
    end

    puts "Processing #{file}..."
    cnt = 0
    CSV.parse(handle) do |row|
      site1 = Btn.import_billing_address(cnt, row)
      cnt += 1
    end
    puts "Processed #{cnt} lines"

  end

  desc "Import 33_01 datasource"
  task :import33, [:file] => [:environment] do |t,args|
    file = args[:file]
    handle = File.open(file, "r:UTF-8")
    unless handle
      puts "Can not open: #{file}: $!\n"
      exit
    end

    puts "Processing #{file}..."
    cnt = 0
    CSV.parse(handle) do |row|
      (id,billingtelephonenumber,client_id,miranda_btn,miranda_good,notes) = row
      next if id == 'id'        # header.
      btn = Btn.find(id)
      btn.billingtelephonenumber = RateCenter.canonify_number(billingtelephonenumber)
      byebug unless btn.client.try(:id) == client_id.to_i
      btn.invoicing_enabled = (miranda_good == "TRUE")
      btn.save!
      cnt += 1
    end
    puts "Processed #{cnt} lines"
  end

  desc "Mark Customers as terminated by BTN (see doc/import/ClosedCustomer1)"
  task :customer_status, [:file] => [:environment] do |t,args|
    file = args[:file]
    handle = File.open(file, "r:UTF-8")
    unless handle
      puts "Can not open: #{file}: $!\n"
      exit
    end

    puts "Processing #{file}..."
    cnt = 0
    closed = 0
    failed_template = 0
    not_found = 0
    already_terminated = 0
    CSV.parse(handle) do |row|
      cnt += 1
      (status, btnname, clientname, terminated_on, cycle) = row
      next if status == 'Account Status'        # header.
      btnnameC = RateCenter.canonify_number(btnname)
      btn = Btn.find_by_billingtelephonenumber(btnnameC)

      unless btn
        if btnname != btnnameC
          puts "no btn: #{btnname}/#{btnnameC} found for #{clientname}"
        else
          puts "no btn: #{btnname} found for #{clientname}"
        end
        not_found += 1
        next
      end

      if status == 'Closed'
        if btn.terminated?
          already_terminated += 1
        end
        term_date = terminated_on.to_date
        btn.terminated!(term_date)
        if btn.client
          btn.client.terminated!(term_date)
        end
        closed += 1
      else
        btn.active!
        btn.client.try(:active!)
        template = InvoiceTemplate.where(name: cycle).take
        if template
          btn.invoice_template = template
        else
          puts "Activated client #{clientname}/#{btn.name}, but template=#{cycle} not found"
          failed_template += 1
        end
      end
    end
    puts "Processed #{cnt} lines, \n\tclosed=#{closed}, \n\tfailed_template=#{failed_template}\n\tnot_found=#{not_found}\n\talready_terminated=#{already_terminated}"
  end


end
