# coding: utf-8
# -*- ruby -*-
require 'csv'

namespace :sg1 do

  desc "Phone number survey for CARRIER"
  task :phone_survey, [:carrier,:output] => [:environment] do |t,args|
    carrier_name = args[:carrier]
    output_name  = args[:output]

    output = $stdout
    if output_name
      output = File.open(output_name, "w")
    end

    onnet_cnt = 0
    sip_avail = 0

    puts "Carrier = #{carrier_name}, output to: #{output}"
    Phone.activated.where(virtualdid: false).find_each { |ph|

      rc = RateCenter.lookup_by_partial(ph.phone_number)
      client = ph.site.try(:client) || ph.btn.try(:client)
      unless rc
        output.puts "#{client.name} #{ph.phone_number} has no valid rate_center"
        next
      end

      ln = rc.local_exchanges.where(carrier: carrier_name).take

      if ln
        output.puts sprintf("%30s %14s rc: %16s,%2s  ln: %s/%s",
                   client.name[0..29], ph.phone_number,
                   rc.lerg6name, rc.province, ln.try(:on_net), ln.try(:sip_availability))

        if ln.try(:on_net)
          onnet_cnt += 1
        end
        if ln.try(:sip_availability)
          sip_avail += 1
        end
      end
    }

    output.puts "#{onnet_cnt} numbers are on-net"
    output.puts "#{sip_avail} numbers are SIP available"

  end

end
