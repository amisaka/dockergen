namespace :bad_site_list do
  desc "TODO"
  task generate: :environment do
    SITE_NAME_AS_PHONE_NUMBER_REGEX = '%\d{10,}%'

    SITE_LIST_FILENAME = 'tmp/bad_site_list.txt'

    File.open(SITE_LIST_FILENAME, 'w') do |file|
      Site.where('name SIMILAR TO ? OR name IS NULL', SITE_NAME_AS_PHONE_NUMBER_REGEX).each do |site|
        file.puts "Site Name : #{site.name} (#{site.site_number}) -- site.id : #{site.id} -- Client Name : #{site.client.try(:name)} -- client.id : #{site.client.try(:id)}"
      end
    end
  end

end
