# require 'factory_bot'

namespace :sg1 do

  def cdr_process(toprocess)
    total = toprocess.count

    puts "Working on #{total} "

    progress = 0
    mapped   = 0
    calling_mapped   = 0
    toprocess.find_each { |cdr|
      toll = cdr.calc_derived(cdr.lineno)
      cdr.save!
      progress += 1
      if cdr.callingphone
        calling_mapped += 1
      end
      if cdr.called_rate_center
        mapped += 1
      end
      if ((progress % 100) == 0)
        print sprintf("%05d    %05d / %05d / %05d / %05d\r", cdr.id, calling_mapped, mapped, progress, total)
      end
    }
    puts "\n"
    puts sprintf("TOTAL    %05d / %05d / %05d / %05d", calling_mapped, mapped, progress, total)
  end

  desc "Map existing CDRs to rate centers for billing purposes"
  task :cdr_rate_center_map => :environment do
    starting = Cdr.originating
    if ENV['CLIENT']
      client = Client.find(ENV['CLIENT'].to_i)
      client.btns.active.each { |btn|
        puts "Working on client: #{client.name} btn: #{btn.name}"
        starting = btn.outgoing_cdrs
        toprocess = starting.where(called_rate_center: nil)
        cdr_process(toprocess)
      }
    else
      toprocess = starting.where(called_rate_center: nil)
      cdr_process(toprocess)
    end
  end

  desc "Map existing outgoing CDRs with blank callingphone"
  task :cdr_callingphone_map => :environment do
    starting = Cdr.originating

    startday = ENV['STARTDAY'].try(:to_date)
    endday   = ENV['ENDDAY'].try(:to_date)

    if startday
      starting = starting.where(["startdatetime > ?", startday])
    end
    if endday
      starting = starting.where(["startdatetime < ?", endday])
    end

    if ENV['CLIENT']
      client = Client.find(ENV['CLIENT'].to_i)
      client.btns.active.each { |btn|
        puts "Working on client: #{client.name} btn: #{btn.name}"
        starting = btn.outgoing_cdrs
        toprocess = starting.where(callingphone: nil)
        cdr_process(toprocess)
      }
    else
      toprocess = starting.where(callingphone: nil)
      cdr_process(toprocess)
    end
  end


  desc "Calculate incorrectly tolled amount from STARTDAY=/ENDDAY="
  task :cdr_toll_calc => :environment do
    startday = ENV['STARTDAY'].try(:to_date)
    endday   = ENV['ENDDAY'].try(:to_date)

    total = 0
    Client.activated.find_each { |client|
      client.btns.active.find_each { |btn| total += 1 }}

    refund_minutes = Hash.new(0)
    refund_destinations = Hash.new(0)
    refund_times = Hash.new
    number = 1
    Client.activated.find_each { |client|
      client.btns.active.find_each { |btn|
        $stderr.print sprintf("%04d/%04d working on %40s / %20s            \r", number, total, client.name, btn.name)
        btn.outgoing_cdrs.tollcalls.today(startday, endday).find_each { |cdr|
          oldtoll = cdr.networkcalltype
          newtoll = cdr.calculate_toll_type
          next if newtoll.blank?
          if oldtoll != newtoll
            unless cdr.called_rate_center.virtual_did?
              if newtoll == 'lo'
                refund_destinations[cdr.callednumber] += cdr.duration
              else
                byebug
              end
              refund_times[cdr.callednumber] = cdr.startdatetime
              refund_minutes[btn.id] += cdr.duration
            end
            #cdr.save!
          end
        }
        number += 1
      }
    }

    puts "Name, id, btn, seconds"
    refund_minutes.each { |btnid, seconds|
      btn = Btn.find(btnid)
      client = btn.client

      puts "#{client.name},#{client.id},#{btn.name},#{seconds}"
    }

    puts "Destinations cleared, seconds"
    refund_destinations.each { |dstnum, seconds|
      lasttime = refund_times[dstnum]
      puts "#{dstnum},,#{lasttime.to_s},#{seconds}"
    }
  end

  desc "Map existing CDRs to rate centers for billing purposes"
  task :cdr_rate_center_map => :environment do
    starting = Cdr.originating
    if ENV['CLIENT']
      client = Client.find(ENV['CLIENT'].to_i)
      client.btns.active.each { |btn|
        puts "Working on client: #{client.name} btn: #{btn.name}"
        starting = btn.outgoing_cdrs
        toprocess = starting.where(called_rate_center: nil)
        cdr_process(toprocess)
      }
    else
      toprocess = starting.where(called_rate_center: nil)
      cdr_process(toprocess)
    end
  end

  desc "CDR reformat INDIR= OUTDIR="
  task :cdrreformat => :environment do
    indir       = ENV['INDIR']
    outdir    = ENV['OUTDIR']

    files = []
    Dir.entries(indir).each { |file|
      if file =~ /BW-CDR-.*\.csv$/ ||
         file =~ /BW-CDR-.*\.csv.gz$/
        files << File.join(indir,file)
      end
    }

    files.each {|file|
      outname = sprintf("%s.ocsv", File.basename(file, ".csv"))
      outfile = File.join(outdir, outname)

      puts "Processing #{file} -> #{outfile}"
      outcsv = CSV.open(outfile, "wb")
      CSV.open(file, "r") do |csvin|
        csvin.each {|row|
          outcsv << row
        }
      end
    }
  end

  desc "Import CDR records from legacy providers"
  task :cdr_legacyload => :environment do
    overwrite = !ENV['OVERWRITE'].blank? || false
    dir       = ENV['DIR']  || $LegacyCdrDir
    file      = ENV['FILE']
    type      = ENV['TYPE']

    if ENV['FIXDATE_DEBUG']
      Call.enable_fixdate_debug
    end

    # set UNIQUENAME if the file names are not unique and the datasource name needs
    # to be overridden for testing.
    uniquename= ENV['UNIQUENAME']

    ::ActiveRecord::Base.connection.execute("set application_name = 'cdrload_legacy #{type}';")

    processblock = nil
    cmdoptions = {overwrite: overwrite}

    case type.downcase
    when 'troop'
      datadir      = ENV['TARGETDIR'] || File.join(dir, 'CDR Troop')
      datapattern  = Cdr.troop_pattern
      processblock = lambda { |options|
        number,durations,firstdate,datasource = Cdr.load_cdr_troop(options.merge!(cmdoptions))
      }

    when 'telus'
      datadir      = ENV['TARGETDIR'] || File.join(dir, 'CDR Telus Rebiller')
      datapattern  = Cdr.telus_pattern
      processblock = lambda { |options|
        number,durations,firstdate,datasource = Cdr.load_cdr_telus_rebiller(options.merge!(cmdoptions))
      }

    when 'drapper'
      datadir      = ENV['TARGETDIR'] || File.join(dir, 'CDR DRAPPER')
      datapattern  = Cdr.drapper_pattern
      processblock = lambda { |options|
        options[:origin] = :draper
        number,durations,firstdate,datasource = Cdr.load_cdr_telus_rebiller(options.merge!(cmdoptions))
      }

    when 'obm'
      datadir      = ENV['TARGETDIR'] || File.join(dir, 'CDR OBM')
      datapattern  = Cdr.obm_pattern
      processblock = lambda { |options|
        options[:uniquename] = uniquename if uniquename
        number,durations,firstdate,datasource = Cdr.load_cdr_obm(options.merge!(cmdoptions))
      }

    when 'rogers'
      datadir      = ENV['TARGETDIR'] || File.join(dir, 'CDR ROGERS')
      datapattern  = Cdr.rogers_pattern
      processblock = lambda { |options|
        number,durations,firstdate,datasource = Cdr.load_cdr_rogers(options.merge!(cmdoptions))
      }

    else
      puts "Must set TYPE={troop,telus,drapper,obm,rogers}"
    end

    unless file.blank?
      basename = File.basename(file)

      Cdr.process_legacy_file(file, basename, datadir, &processblock)
      exit
    else
      Cdr.find_and_load_files(datapattern, datadir, &processblock)
    end
    if ENV['FIXDATE_DEBUG']
      Call.dump_fixdate_debug
    end

  end

  desc "Reprocess CDRs from a given datasource, producing a new legacy edit file"
  task :cdr_reprocess => :environment do
    datasourceid = ENV['DATASOURCE']

    unless datasourceid
      puts "Must set DATASOURCE=xxx"
      exit
    end

    ds = Datasource.find(datasourceid.to_i)
    ds.data_at ||= ds.created_at
    print "ds\##{ds.id}: (#{ds.try(:did_supplier).try(:name)})#{ds.data_at.to_s(:db)} cdrs=#{ds.cdrs_count} calc=#{ds.toll_recalculate_count}.."
    proc_cnt = ds.reprocess_cdrs
    puts  "..#{ds.toll_recalculate_count}/#{proc_cnt}"
  end

  desc "Reprocess CDRs for a given date range, producing a new legacy edit file"
  task :cdr_reprocess_range => :environment do
    if ENV['START'] and ENV['FINISH']
      start_day  = ENV['START'].to_datetime
      finish_day = ENV['FINISH'].to_datetime
    elsif ENV['MONTH'] and ENV['YEAR']
      start_day = DateTime.new(ENV['YEAR'].to_i, ENV['MONTH'].to_i, 1).beginning_of_month
      finish_day= start_day.end_of_month
    else
      puts "Please set START=/FINISH=, or YEAR=/MONTH="
      exit
    end
    ::ActiveRecord::Base.connection.execute("set application_name = 'cdr reprocess range';")

    puts "Preloading phone cache"
    $0 = sprintf("beaumont: preloading phones")
    Cdr.preload_tables

    if ENV['RAILS_CACHE_DEBUG']
      Rails.cache.logger = Logger.new("#{Rails.root}/log/#{Rails.env}_cache.log")
      Rails.cache.logger.level = Logger::DEBUG
    end

    puts "Processing from #{start_day.to_s(:db)} to #{finish_day.to_s(:db)}"

    Datasource.where(data_at: start_day..finish_day).find_each {|ds|
      print "ds\##{ds.id}: #{ds.data_at.to_s(:db)} cdrs=#{ds.cdrs_count} calc=#{ds.toll_recalculate_count}.."
      proc_cnt = ds.reprocess_cdrs
      puts  "..#{ds.toll_recalculate_count}/#{proc_cnt}"
    }
  end

  desc "take a datasource worth of CDRs and clone/anonymize them"
  task :clone_cdrdata, [:datasource_id,:client_id,:startdate,:newdate,:outdir] => [:environment] do |t,args|
    ds_id      = args[:datasource_id]
    startdate  = args[:startdate].to_datetime
    newdate    = args[:newdate].to_datetime
    client_id  = args[:client_id]
    outdir     = args[:outdir]
    datasource = Datasource.find(ds_id)
    fw = FixtureWriter.new(outdir)
    fk = FakerMapper.new

    ds2 = datasource.dup
    ds2.datasource_filename = "copy#{datasource.id}"
    ds2.save!

    puts "Writing Datasource: #{ds2.id} to #{outdir} from #{startdate}"

    ds2.savefixturefw(fw)

    records =  datasource.cdrs.where(["startdatetime >= ?", startdate])
    # find date of first record to establish the offset.
    firstdate = records.first.startdatetime

    offset = (newdate.to_i - firstdate.to_i).seconds
    fk.date_offset = offset
    fk.client = Client.find(client_id)

    # go through all the records and duplicate them.
    records.each { |cdr|
      cdr2 = fk.dup_cdr(cdr, ds2)
      cdr2.savefixturefw(fw)
      puts "writing cdr: #{cdr.id} -> #{cdr2.id}"
    }
    fw.closefiles
  end

  desc "Clean up some old CDRs, with DATE=earliest as limit"
  task :cdr_clean => :environment do
    until_date = ENV['DATE'].to_date
    dryrun     = ENV['DRYRUN'].present?
    daystodelete=ENV['DELETEDAYS'].try(:to_i) || 3

    earliest_cdr = Cdr.oldest_first.first

    # exist if nothing to do.
    puts "Earliest record is from #{earliest_cdr.startdatetime}"
    if earliest_cdr.startdatetime > until_date
      puts "... not deleting anything"
      exit 0
    end

    # now clean out three days worth of CDRs
    three_days_later = earliest_cdr.startdatetime + (daystodelete).days
    if three_days_later > until_date
      three_days_later = until_date
    end

    puts "Deleting from #{earliest_cdr.startdatetime} to #{three_days_later}"
    things = Cdr.where(["startdatetime < ?", three_days_later])
    cnt = things.count
    puts " ... has #{cnt}, at: #{Time.now}"
    exit 1 if dryrun

    things.delete_all
    puts " finished at: #{Time.now}"
  end

  desc "Do periodic (daily) report of CDR load statistics"
  task :cdrload_report => :environment do
    # find out when we last ran.
    lasttime_var = SystemVariable.findormake(:lastcdrreport)

    nowtime = lasttime = Time.now
    lasttime = nowtime - 1.day
    # if not set, then set it to now
    unless lasttime_var.number
      lasttime_var.number = lasttime.to_i
    end
    #lasttime_var.save!
    lasttime = Time.at(lasttime_var.number)

    puts "Reporting on CDR loading since #{lasttime} to #{nowtime}"
    totalfiles = 0.0
    totalcdrs  = 0.0
    totaltime  = 0
    totalcalls = 0.0
    oldestdata = nil
    newestdata = nil
    Datasource.where(["created_at > ?", lasttime]).order(:created_at).each { |ds|
      totalcdrs  += (ds.cdrs_count||0)
      totalcalls += (ds.calls_count||0)
      totalfiles += 1
      totaltime  += (ds.load_time||0)
      if ds.data_at
        if oldestdata.blank?  or oldestdata > ds.data_at
          oldestdata = ds.data_at
        end
        if newestdata.blank?  or newestdata < ds.data_at
          newestdata = ds.data_at
        end
      end
    }

    avgcdrs   = 0
    avgcalls  = 0
    avgfiles  = 0
    if totaltime > 0
      avgcdrs   = totalcdrs / totaltime
      avgcalls  = totalcalls / totaltime
      avgfiles  = totalfiles / totaltime
    end
    puts "#{totalfiles} were processed in #{totaltime}s"
    puts sprintf("   at average: %.6f cdrs/s", avgcdrs)
    puts sprintf("   at average: %.6f calls/s",avgcalls)
    puts sprintf("   at average: %.6f files/s",avgfiles)

    lasttime_var.number = lasttime.to_i
  end


  desc "Import CDR record of Broadworks calls, OUTDIR=filtered"
  task :cdrload => :environment do
    overwrite = !ENV['OVERWRITE'].blank? || false
    dir       = ENV['DIR']
    outdir    = ENV['OUTDIR']

    ::ActiveRecord::Base.connection.execute("set application_name = 'cdrload';")
    files = []
    if dir.blank?
      file      = ENV['FILE']
      files << file
    else
      Dir.entries(dir).each { |file|
        if file =~ /BW-CDR-.*\.csv$/ ||
            file =~ /BW-CDR-.*\.csv.gz$/
          files << File.join(dir,file)
        end
      }
    end

    verbose   = false
    if ENV['VERBOSE']
      verbose   = true
    end
    quiet = false
    if ENV['QUIET']
      quiet     = true
    end
    if ENV['ARCHIVE']
      archive   = ENV['ARCHIVE']
    end

    finish=false

    Signal.trap("USR1") do
      puts "Terminating on USR1..."
      finish=true
    end
    Signal.trap("TERM") do
      puts "Terminating on TERM..."
      finish=true
    end

    options = { overwrite: overwrite,
                verbose: verbose,
                archive: archive}
    unless outdir.blank?
      options[:outdir] = outdir
    end

    files.sort.each { |file|
      break if finish            # shutdown nicely if asked to
      puts "Loading CDR data from #{file}\n" unless quiet
      starttime = Time.now

      options[:file] = file
      number,durations,firstdate,datasource =
                                 Cdr.load_bw_file(options);
      if number == :duplicate
        puts "..skipped, is duplicate"       unless quiet
      else
        endtime   = Time.now
        duration = endtime-starttime
        rate = number / duration
        avgduration = duration / number
        puts "Loading CDR data from #{file}\n" unless quiet
        puts " ..loaded #{number} records in #{duration}s, at #{rate} cdrs/s dbtime:#{avgduration}\n" unless quiet
        if(datasource.load_time > 0)
          rate1 = datasource.cdrs_count / datasource.load_time
          rate2 = datasource.calls_count / datasource.load_time
          did = datasource.id.try(:to_s) || "bad-id"
          unless quiet
            puts sprintf(" ..datasource: %05d %05d CDRs  %05ds, at %2.2f cdrs/s\n", did, datasource.cdrs_count, datasource.load_time, rate1)
            puts sprintf(" ..datasource: %05d %05d calls %05ds, at %2.2f calls/s\n", did, datasource.calls_count, datasource.load_time, rate2)
          end
        end
      end
    }
  end

  desc "Load a single FILE= of CDRs, or CALLID=, and then output to dir specified by FIXTURES= ."
  task :makefixtures => :environment do
    output   = ENV['FIXTURES']

    if output.nil?
      puts "FIXTURES is not specified"
      exit
    end

    if ENV['FILE']
      filename = ENV['FILE']

      puts "Loading CDR data from #{file}\n"
      basename = File.basename(filename, ".gz")
      datasource = Datasource.create(:datasource_filename => basename)
      handle = File.open(filename, "r:ISO-8859-1")
      number = Cdr.load_bw_from_handle(handle, datasource, true)

      cdrs = datasource.cdrs
    end
    if ENV['CALLID']
      call = Call.find(ENV['CALLID'].to_i)
      cdrs = call.cdrs
    end

    # now dump all the records associated with this datasource
    fw = FixtureWriter.new('.')
    #debugger
    fw.set_file(Cdr, File.open(output, "w"))

    # doing this by calls, and then cdrs naturally groups the cdrs by call.
    # on the other hand, if the calls do not work, it fails.
    cdrs.find_each { |cdr|
	  cdr.savefixturefw(fw)
    }
  end

  desc "Load a FILE= of CDRs, and then make OOffice happy CSV from them"
  task :makeprettycsv => :environment do
    filename = ENV['FILE']
    output   = ENV['CSV']

    if output.blank?
      puts "File: #{output} is blank"
      exit 2
    end

    puts "Loading CDR data from #{filename}, write to #{output}\n"
    basename = File.basename(filename, ".gz")
    datasource = Datasource.create(:datasource_filename => filename)
    handle = File.open(filename, "r:ISO-8859-1")
    number = Cdr.load_bw_from_handle(handle, datasource, true)

    # now dump all the records associated with this datasource

    # doing this by calls, and then cdrs naturally groups the cdrs by call.
    cdrs = []
    datasource.calls.find_each { |call|
      call.cdrs.find_each { |cdr|
	cdrs << cdr
      }
    }
    one = cdrs.shift

    if one
      File.open(output, "w") do |file|
        one.dump_with_friends(file, cdrs)
      end
    end

    datasource.cdrs.delete_all
    datasource.calls.delete_all
    datasource.delete
  end

  desc "make pretty csv from list of calls"
  task :prettycalls => :environment do
    output   = ENV['CSV']

    if output.blank?
      puts "File: #{output} is blank"
      exit 2
    end

    listofcdrs = Hash.new
    n1.each { |c1|
      c1.cdrs.each { |cdr1|
        listofcdrs[cdr1.id] = cdr1
      }
    }
    n2.each { |c1|
      c1.cdrs.each { |cdr1|
        listofcdrs[cdr1.id] = cdr1
      }
    }

    cdrs = listofcdrs.values
    one = cdrs.shift

    File.open(output,"w") do | file|
      one.dump_with_friends(file, cdrs)
    end
  end



end
