# coding: utf-8
# -*- ruby -*-

namespace :sg1 do

  desc "Import CSV of service export for authoritative list of phone numbers from FILE=csv, IMPORT_DEBUG=true"
  task :import_service_list => :environment do
    file = ENV['FILE'] || 'tblService.csv'
    import_debug = ENV['IMPORT_DEBUG'].present?

    handle = File.open(file, "r:UTF-8")
    unless handle
      puts "Can not open: #{file}: $!\n"
      exit
    end

    puts "Processing #{file}..."
    cnt = 0
    create_num = 0
    CSV.parse(handle) do |row|
      (loadID,serviceStatus,btnum,customerName,serviceID,serviceType,description,departmentID,departmentName,lineOfBusiness,supplierName,supplierActivationDate,couverture) = row

      # customerName unsure what to do with it now.
      # supplier

      next if serviceStatus == 'ServiceStatus'  # header
      next unless serviceStatus == 'Active'

      virtualdid = false

      case serviceType
      when 'ANI'
        true
      when 'Virtual DID'
        virtualdid = true
      when 'Travel'
        true
      when 'Toll Free Switched and Dedicated'
        true
      when ''
        true
      when nil
        true
      else
        byebug
      end

      local_rate_plan = nil  # because the default is to take whatever the BTN has.
      case couverture
      when 'Personalisé'
        true
      when "Personnalisé"
        true
      when 'Couverture Provinciale'
        local_rate_plan = LocalRatePlan.default
      when 'Couverture Nationale'
        local_rate_plan = LocalRatePlan.canada_wide

      when 'Canada seulement'
        # what does this mean?
        local_rate_plan = LocalRatePlan.canada_wide

      when 'Canada et État-Unis'
        # what does this mean?
        local_rate_plan = LocalRatePlan.default

      when ''
        local_rate_plan = LocalRatePlan.default
      when nil
        true
      else
        byebug
      end

      # now look the phone up by serviceID, and for certain serviceTypes, load it.
      btn = Btn.get_by_billingtelephonenumber(btnum)
      next unless btn

      client = btn.client
      next unless client

      p1 = Phone.find_phoneno(client, serviceID, nil)

      unless p1
        p1 = Phone.create(btn: btn,
                          phone_number: serviceID)
        create_num += 1
      end

      unless description.blank?
        p1.firstname = description
      end
      p1.line_of_business = lineOfBusiness
      unless supplierActivationDate.blank?
        p1.activation_date = supplierActivationDate.to_date
      end

      site = p1.site || client.billing_site
      if !departmentID.blank?
        site = client.sites.where(site_number: departmentID).take
      end

      supplier = nil
      case supplierName
      when ''
        true
      when nil
        true
      when 'BroadSoft'
        supplier = DIDSupplier.find_by_symbol(:broadsoft)
      when 'Thinktel'
        supplier = DIDSupplier.find_by_symbol(:thinktel)
      when 'Bell Troop'
        supplier = DIDSupplier.find_by_symbol(:troop)
      when 'Allstream (LD)'
        supplier = DIDSupplier.find_by_symbol(:allstream)
      when 'Drapper'
        supplier = DIDSupplier.find_by_symbol(:drapper)
      when 'Bell OBM (LL)'
        supplier = DIDSupplier.find_by_symbol(:obm)
      when 'Bell OBM (800)'
        supplier = DIDSupplier.find_by_symbol(:obm)
        p1.incoming_toll = true

      when "Sprint/Rogers (800)"
        supplier = DIDSupplier.find_by_symbol(:rogers)
        p1.incoming_toll = true
      when 'Bell OBM (LD)'
        supplier = DIDSupplier.find_by_symbol(:obm)
      when 'Bell acc BWFHNXQX99'
        supplier = DIDSupplier.find_by_symbol(:obm)
      when 'Telus Rebiller (LD)'
        supplier = DIDSupplier.find_by_symbol(:telus)
      when 'Telus Quebec (LL)'
        supplier = DIDSupplier.find_by_symbol(:telus)
      when 'ISP Telecom'
        supplier = DIDSupplier.find_by_symbol(:isptelecom)
      when 'Cogeco'
        supplier = DIDSupplier.find_by_symbol(:cogeo)
      when 'Rogers (LL)'
        supplier = DIDSupplier.find_by_symbol(:rogers)
      when 'Shaw'
        supplier = DIDSupplier.find_by_symbol(:telus)
      when "Allstream (800)"
        supplier = DIDSupplier.find_by_symbol(:allstream)
        p1.incoming_toll = true
      else
        byebug
      end
      if p1.notes.blank?
        p1.notes = supplierName
      end
      p1.site = site
      p1.did_supplier = supplier
      p1.local_rate_plan = local_rate_plan

      p1.save!
      cnt += 1
    end

    puts "loaded #{cnt} lines, created #{create_num} new items"
  end
end

