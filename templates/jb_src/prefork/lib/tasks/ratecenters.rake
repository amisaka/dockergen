# coding: utf-8

# typically, sg1:updateTelecordia and sg1:update_footprints should be run
#   daily from cron.
require 'dump_stats'

namespace :sg1 do
  desc "Import LERG8 data, first argument is full file path."
  task :updateLerg8 => :environment do

    if ARGV.empty?
	    puts "Specify file path to data"
	    exit
    end
    count = Lerg8.import_file(ARGV[1])

    puts "Imported #{count} lines"
  end

  desc "Load LERG8 from Telecordia ZIP file, TELECORDIADATADIR= defaults to ~/ip4b/telecom/.."
  task :updateTelecordia => :environment do

    datadir   = $TelcordiaDataDir
    if ENV['TELCORDIADATADIR']
      datadir = ENV['TELCORDIADATADIR'] || ENV['TELECORDIADATADIR']
    end
    forced = false
    forced_lerg6 = false
    forced_lerg8 = false
    if ENV['FORCELOAD']
      forced = true
      forced_lerg6 = true
      forced_lerg8 = true
    end
    if ENV['FORCELOAD_LERG6']
      forced_lerg6 = true
    end
    if ENV['FORCELOAD_LERG8']
      forced_lerg8 = true
    end
    verbose = false
    if ENV['VERBOSE']
      verbose = true
    end

    stats = Hash.new(0)

    stats[:debug] = !ENV['IMPORTDEBUG'].blank? || verbose

    lerg6file = LergMethods::latestLerg6ZipFile(datadir)
    exit unless lerg6file
    lerg6zipfile = lerg6file

    # see if this file was loaded before, look for last filename loaded.
    lerg6basename = File.basename(lerg6zipfile)
    lerg6sum = Digest::SHA256.file(lerg6zipfile).hexdigest
    tmpdir1 = nil
    tmpdir2 = nil

    newdata = SystemVariable.filechanged(:lerg6filename, lerg6zipfile,
                                          :lerg6sha2sum, lerg6sum)
    if newdata or forced_lerg6

      puts "Loading lerg6 data from #{lerg6zipfile} forced=#{forced}"

      started = Time.now
      total = `wc -l <#{lerg6file}`.chomp
      puts "File size is: #{total}"
      stats[:lerg6_total] = total
      puts "#{started} Starting import of #{lerg6file}"
      lerg6file,tmpdir1 = LergMethods::findlerg6(lerg6file)
      count = NorthAmericanExchange.import_file(lerg6file, stats)
      ended   = Time.now
      puts "#{ended} Ending import of #{lerg6file}"

      SystemVariable.set_string(:lerg6filename,lerg6zipfile)
      SystemVariable.set_string(:lerg6sha2sum, lerg6sum)
    end

    lerg8file = LergMethods::latestLerg8ZipFile(datadir)
    lerg8zipfile = lerg8file
    lerg8basename = File.basename(lerg8zipfile)
    lerg8sum = Digest::SHA256.file(lerg8zipfile).hexdigest

    newdata = SystemVariable.filechanged(:lerg8filename, lerg8zipfile,
                                         :lerg8sha2sum, lerg8sum)
    if newdata or forced_lerg8

      puts "Loading lerg8 data from #{lerg8zipfile} forced=#{forced}"

      started = Time.now
      total = `wc -l <#{lerg8file}`.chomp
      stats[:lerg8_total] = total
      puts "File size is: #{total}"
      puts "#{started} Starting import of #{lerg8file}"
      lerg8file,tmpdir2 = LergMethods::findlerg8(lerg8file)
      count = Lerg8.import_file(lerg8file, stats)
      SystemVariable.set_string(:lerg8filename,lerg8zipfile)
      SystemVariable.set_string(:lerg8sha2sum, lerg8sum)
    end

    dump_stats(stats)

    # clean up temp files
    if tmpdir1
      File.unlink(lerg6file)
      FileUtils.rmdir(tmpdir1)
    end
    if tmpdir2
      File.unlink(lerg8file)
      FileUtils.rmdir(tmpdir2)
    end
  end

  # To trim LERG6.DAT to canadian provinces, use:
  # grep -E '^.{77}(PQ|AB|BC|YK|MB|NB|NF|NL|NS|NT|NU|VU|ON|PE|QC|SK|YT|LB|QB)' LERG6.DAT | sort  -k 1.78 > LERG6.canadaOnly.DAT

  desc "Load LERG6 data to NorthAmericanExchange, first argument is file."
  task :updateLerg6 => :environment do

    if ARGV.empty?
	    puts "Specify file path to data as first argument"
	    exit
    end

    before = NorthAmericanExchange.count

    stats = Hash.new(0)
    NorthAmericanExchange.import_file(ARGV[1], stats)
    dump_stats(stats)

  end

  desc "Import a new FILE=file footprint for CARRIER=thing into LocalExchange table.  The seperator may be set with SEP= (default is tab).  "
  task :updateCarrier => :environment do

    #  Thinktel Footprint:  T:\Technique\ThinkTel\August

    stats = Hash.new(0)

    if ENV['FILE'].blank? || ENV['CARRIER'].blank?
        $stderr.puts "\nBoth path and carrier variables must be specified.  Specify PATH=/the/path/to/the/file and CARRIER=\"ThinkTel\".\n"
        exit
    end

    filename = ENV['FILE']
    carrier  = ENV['CARRIER']
    debug    = ENV['DEBUG'] || "false"
    debug    = (debug == "true")

    if ! File.exists?(filename)
        $stderr.puts "There does not seem to be a file at #{filename}.  Usually this means you need a full path rather than a relative one. "
        exit
    end

    stats[:name_province] = true
    seperator = ","
    if ! ENV['SEP'].blank?
        seperator = ENV['SEP']
    end

    LocalExchange.import_file(filename, carrier, seperator, debug, stats)
    dump_stats(stats)
  end

  desc "Find and import Foot Print file "
  task :update_footprints => :environment do

    stats = Hash.new(0)

    forced = false
    if ENV['FORCELOAD']
      forced = true
    end

    carrier  = ENV['CARRIER'] || 'Thinktel'
    seperator = ","
    debug    = ENV['DEBUG'] || "false"
    debug    = (debug == "true")

    carrierFileName = "footprint_#{carrier}_file"
    carrierFileSum  = "footprint_#{carrier}_sum"

    dirpath = File.join(ENV['HOME'],"ip4b", "telecom", "Technique", carrier)
    puts "Looking for new foot prints in #{dirpath}"
    path,tmpdir,workfile = LocalExchange.latestFootPrint(dirpath)

    footprint_basename = File.basename(path)
    footprint_sum = Digest::SHA256.file(path).hexdigest

    newdata = SystemVariable.filechanged(carrierFileName, footprint_basename,
                                         carrierFileSum,  footprint_sum)
    if newdata or forced

      puts "Loading #{carrier} data from #{workfile} forced=#{forced}"

      n = Time.now

      # mark everything as deleted!
      LocalExchange.where(carrier: carrier, deleted_at: nil).update_all(deleted_at: n)

      LocalExchange.import_file(workfile, carrier, seperator, debug, stats)
      dump_stats(stats)

      # import_file will have the affect of marking active items as no longer deleted.
      # print off the list of items which are still deleted
      LocalExchange.where(carrier: carrier, deleted_at: n).find_each {|le|
        rc = le.rate_center
        les = []

        unless rc
          le.north_american_exchanges.each { |nae|
            les = nae.rate_center.local_exchanges.where(carrier: carrier)
          }
        end

        print "deleted #{le.id} #{le.name}/#{le.province} (rc: #{rc.try(:lerg6name)})"
        les.each {|le|
          puts " replaced by: #{le.id} #{le.name}/#{le.province}"
        }
        puts ""
      }

      SystemVariable.set_string(carrierFileName, footprint_basename)
      SystemVariable.set_string(carrierFileSum,  footprint_sum)
    end

    if workfile != path
      File.unlink(workfile)
    end

    if tmpdir
      FileUtils.rmdir(tmpdir)
    end

    if newdata
      exit 0
    else
      exit 5
    end

  end

  desc "Do matching of new footprint entries for CARRIER=XXX"
  task :extend_footprint => :environment do

    carrier = ENV['CARRIER']
    if carrier.blank?
      puts "Must set CARRIER=name"
      exit 10
    end

    stats = Hash.new(0)
    count = LocalExchange.where(carrier: carrier, rate_center_id: nil).count
    puts "Working on #{count} unmatched items"
    count = LocalExchange.extend_footprint(carrier, stats)
    dump_stats(stats)
    LocalExchangesNorthAmericanExchange.report_match_rate()
  end

  desc "Report match rate for LERG6/Footprint"
  task :reportMatchRate => :environment do
    LocalExchangesNorthAmericanExchange.report_match_rate()
  end

  desc "Create rate center objects from linked local_exchange/north_american_exchange objects."
  task :create_rate_center_from_lenae => :environment do
    LocalExchangesNorthAmericanExchange.create_rate_centers
  end

  desc "Write rate center list to fixture file"
  task :write_rate_centers => :environment do
    puts "Writing to tmp/rate_centers/rate_centers.yml"
    FileUtils::mkdir_p('tmp/rate_centers')
    fw = FixtureWriter.new('tmp/rate_centers')
    RateCenter.find_each {|rc| rc.savefixturefw(fw) }
    fw.closefiles
  end

  desc "Update LocalExchange <-> NorthAmericanExchange N:M from rate centers"
  task :update_lenae => :environment do
    debug = false
    debug = true if ENV['DEBUG']

    LocalExchangesNorthAmericanExchange.report_match_rate()
    LocalExchangesNorthAmericanExchange.update_from_rate_center(debug)
    LocalExchangesNorthAmericanExchange.report_match_rate()
  end

  desc "De-duplicate rate centers"
  task :deduplicate_rate_centers => :environment do
    rcfirst = nil
    RateCenter.active.where(country_code: 1).order("lerg6name, province, id").each { |rc|
      if rcfirst
        if rc.lerg6name == rcfirst.lerg6name and rc.province == rcfirst.province
          puts "merging #{rc.lerg6name}  #{rc.id} #{rcfirst.id}"

          rcfirst.localnames = (rcfirst.localnames + rc.localnames).uniq
          rc.local_exchanges.each { |le| le.rate_center = rcfirst; le.save }
          rc.north_american_exchanges.each { |ne| ne.rate_center = rcfirst; ne.save }
          rc.deleted_at = Time.now
          rc.local_exchanges_north_american_exchanges.delete_all
          rc.save!
          rc = rcfirst
        end
      end

      rcfirst  = rc
    }
  end

end

