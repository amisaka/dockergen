require 'hakka'

namespace :sg1 do
  desc "Imports a single service (product) record from Hakka into services table"
  task :import_service, [:customer_pid, :dryrun] => [:environment] do |t, args|
    Hakka::ProductImporter.import! args[:customer_pid],
      dryrun: ActiveRecord::Type::Boolean.new.type_cast_from_user(args[:dryrun])
  end

  desc "Imports all products (services) records from Hakka into services table"
  task :import_services, [:dryrun] => [:environment] do |t, args|
    Hakka::ProductImporter.import_all! \
      dryrun: ActiveRecord::Type::Boolean.new.type_cast_from_user(args[:dryrun])
  end
end
