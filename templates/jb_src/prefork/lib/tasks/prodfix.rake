# -*- ruby -*-
require 'csv'

namespace :sg1 do

  desc "Init data_at"
  task :init_data_at => :environment do
    Datasource.do_analysis_six
  end

  desc "Go through CDRs and list datasources which have at least one mis-typed CDR, from START=/END="
  task :cdr_reload_for_toll => :environment do
    startdate  = ENV['START'].try(:to_date)
    enddate    = ENV['END'].try(:to_date)

    unless (startdate && enddate)
      $stderr.puts "Must set START= and END="
      exit
    end

    ((startdate)..(enddate)).each {|day|
      puts "\# working on #{day}"
      Datasource.where(["DATE_TRUNC('day', cdrviewer_datasource.data_at) = ?", day]).find_each { |ds|
        calculated = ds.recalculate_toll_data
        puts "#{ds.datasource_filename}" if calculated
      }
    }
  end


end
