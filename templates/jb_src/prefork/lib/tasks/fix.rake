namespace :fix do
  desc "repairs buildings and crm accounts records corrupted addresses"
  task repair_all: :environment do
    BuildingAddress.repair_all
    CrmAccountAddress.repair_all
  end
end
