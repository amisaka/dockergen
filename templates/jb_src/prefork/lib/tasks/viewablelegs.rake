# -*- ruby -*-

namespace :novavision do
  desc "Import CDR record of Broadworks calls"
  task :calc_viewable_legs => :environment do
    count = 0
    Call.find_each(:conditions => [ :viewable_legs => nil ]) { |c|

      # If Call object's duration is not set, then calculate it
      next unless c.viewable_legs.nil?

      c[:viewable_legs] = c.non_redundant_legs.count
      c.save!
      count += 1

      if (count % 1000) == 1
        puts " ..calc_duration #{count} records"
      end
    }
  end
end
