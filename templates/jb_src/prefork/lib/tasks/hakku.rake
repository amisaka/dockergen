require 'hakku'

# The hakku database can be an SQLite database configured as such:
#
#hakku:
#  adapter: sqlite3
#  encoding: unicode
#  database: /var/tmp/db/hakku.sqlite
#
# make the the user running the rake tasks has write access to the file.

$HakkuBackup = "data"

namespace :hakku do

  USERNAME         = 'mrdupont'
  HOSTNAME         = 'sg1db2.isp.ip4b.net'

  SOURCE_PATHNAME  = '/sg1/mrdupont/customerdata/2017-05-24_Backup'
  FIXED_PATHNAME   = '/sg1/mrdupont/customerdata/2017-05-24_fixed'

  SOURCE_DIRNAME   = 'data'
  FIXED_DIRNAME   = 'data_fixed'

  task mount: :environment do
    puts "Creating mount point..."
    system "mkdir -p #{SOURCE_DIRNAME}"
    system "mkdir -p #{FIXED_DIRNAME}"
    puts "Mounting SSHFS..."
    system "sshfs -o nonempty,reconnect #{USERNAME}@#{HOSTNAME}:#{SOURCE_PATHNAME} #{SOURCE_DIRNAME}"
    system "sshfs -o nonempty,reconnect #{USERNAME}@#{HOSTNAME}:#{FIXED_PATHNAME} #{FIXED_DIRNAME}"
  end

  task umount: :environment do
    puts "Dismounting SSHFS..."
    system "fusermount -u #{SOURCE_DIRNAME}"
    system "fusermount -u #{FIXED_DIRNAME}"
    puts "Removing mount point..."
    system "rmdir #{SOURCE_DIRNAME}"
    system "rmdir #{FIXED_DIRNAME}"
  end

  desc "Creates a table into the temporary database"
  task :create, [:table] => [:environment] do |t, args|
    case args[:table]

    # rake hakku:create[service]
    when 'service', 'Service'
      Hakku::Table::Service.create

    # rake hakku:create[service_type]
    when 'service_type', 'ServiceType'
      Hakku::Table::ServiceType.create

    # rake hakku:create[customer]
    when 'customer', 'Customer'
      Hakku::Table::Customer.create

    # rake hakku:create[department]
    when 'department'
      Hakku::Table::Department.create

    # rake hakku:create[product]
    when 'product', 'Product'
      Hakku::Table::Product.create

    # rake hakku:create[line_of_business]
    when 'line_of_business', 'LineOfBusiness'
      Hakku::Table::LineOfBusiness.create

      # rake hakku:create[charge_assignment]
    when 'charge_assignment', 'ChargeAssignment'
      Hakku::Table::ChargeAssignment.create

    else
      raise "not yet implemented"
    end
  end

  desc "Imports data into the temporary database"
  task :import, [:table, :backupdir, :file] => [:environment] do |t, args|

    $HakkuBackup = args[:backupdir] || SOURCE_DIRNAME
    file = args[:file]

    case args[:table].downcase
    # rake hakku:import[service]
    when 'service'
      Hakku::Table::Service.import(file)

    # rake hakku:import[service_type]
    when 'service_type', 'ServiceType'
      Hakku::Table::ServiceType.import

    # rake hakku:import[customer]
    when 'customer'
      Hakku::Table::Customer.import

    # rake hakku:import[product]
    when 'department'
      Hakku::Table::Department.import(file)

    # rake hakku:import[product]
    when 'product', 'Product'
      Hakku::Table::Product.import(file)

    # rake hakku:import[line_of_business]
    when 'line_of_business', 'LineOfBusiness'
      Hakku::Table::LineOfBusiness.import

    # rake hakku:import[charge_assignment]
    when 'charge_assignment', 'ChargeAssignment'
      Hakku::Table::ChargeAssignment.import

    else
      raise "not yet implemented"
    end
  end

  desc "sanitize a source CSV from src_path to dst_path"
  task :sanitize, [:src_path, :dst_path] => [:environment] do |t, args|
    Hakku.sanitize(args[:src_path], args[:dst_path])
  end

  desc "Updates data from temporary database into real database"
  task :update, [:table] => [:environment] do |t, args|
    case args[:table]

    # rake hakku:update[billable_services]
    when 'billable_services',
      Hakku::ChargeAssignment.delta.each do |charge|
        charge.update_associated_asset
      end

    # rake hakku:update[service]
    when 'service', 'Service'
      Hakku::Service.delta.each do |service|
        service.update_associated_asset
      end

    # rake hakku:update[customer]
    when 'customer', 'Customer'
      Hakku::Customer.delta.each do |customer|
        customer.update_associated_client
      end

    # rake hakku:update[service_type]
    when 'service_type', 'ServiceType'
      Hakku::Service.all.each do |service|
        service.update_service_type
      end

    # rake hakku:update[line_of_business]
  when 'line_of_business', 'LineOfBusiness'
      Hakku::Service.all.each do |service|
        service.update_line_of_business
      end

    else
      raise "not yet implemented"
    end
  end

  def import_cust_from_hakku(num, client, cust, year, month)
    invoice = client.most_recent_invoice(year, month)

    cust.create_billable_services(invoice)
    invoice.reload

    recurrence_count = 0

    invoice.billable_services.each { |bs|
      ri = bs.hakku_chargeassignment.try(:recurrence_info)
      if ri
        cycle_date  = ri[:AnnualCycleDate]
        freq_months = ri[:FrequencyMonths]
        month = cycle_date.try(:month)

        #puts "checking recurrence for #{bs.id}"

        case
        when (freq_months == 12 and invoice.service_start.month == month)
          # this is the month/invoice to attach this too.
          bs.active_month = true

        when (freq_months == 12 and month)
          # find a month in the future with the right month, create an invoice,
          # and attach the billable service to that invoice.
          ninvoice = invoice.invoice_next_month_num(month)
          bs.invoice = ninvoice
          puts "  recurrence for #{bs.id} moved from #{invoice.id} to #{ninvoice.id} #{ninvoice.invoice_date}"
          bs.save!
          recurrence_count += 1
        end
      end
    }


    #byebug if invoice.billable_services.count == 0

    printf "%u: cust %35s  code: %s [assets: %u/%u services: %u year-recurring: %u] %s\n", num,
           client.name, invoice.client.account_code,
           $HAKKUSTATS[:service_updated],
           $HAKKUSTATS[:department_updated],
           invoice.billable_services.count, recurrence_count,
           invoice.billable_services.first.try(:service_name)
  end

  def dump_hakku_stats
    $HAKKUSTATS.keys.each {|thing|
      value = $HAKKUSTATS[thing]
      puts "  Stat: #{thing} count: #{value}"
    }
  end

  desc "Import customers, services, products and relationships from SQLITE database (HAKKU)"
  task :hakkuimport => :environment do

    num = 0
    $HAKKUSTATS = Hash.new(0)

    # use the list of charge assignments to discover clients that need
    if ENV['MONTH'] and ENV['YEAR']
      month = ENV['MONTH'].to_i
      year  = ENV['YEAR'].to_i
    else
      puts "Must set YEAR= and MONTH="
    end

    if ENV['CLIENT']
      client = Client.find(ENV['CLIENT'].to_i)
      cust = client.hakku_customer
      import_cust_from_hakku(1, client, cust, year, month)
      exit 0
    end


    clients_already_processed = Hash.new

    Hakku::ChargeAssignment.find_each { |ca|

      cust = ca.customer
      $HAKKUSTATS[:num] += 1

      unless cust
        $HAKKUSTATS[:no_customer] += 1
        next
      end
      client = cust.associated_client

      if clients_already_processed[client.id]
        $HAKKUSTATS[:already_processed] += 1
        next
      end

      import_cust_from_hakku($HAKKUSTATS[:num], client, cust, year, month)
      clients_already_processed[client.id] = true

      if ($HAKKUSTATS[:num] % 100) == 0
        dump_hakku_stats
      end

    }

    dump_hakku_stats

  end

end
