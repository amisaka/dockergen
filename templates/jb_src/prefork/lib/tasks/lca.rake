# coding: utf-8
#=> NorthAmericanExchange(id: integer, province: string, name: string, npa: integer, nxx: integer, created_at: datetime, updated_at: datetime)

#To trim LERG6.DAT to canadian provinces, use:
#grep -E '^.{77}(PQ|AB|BC|YK|MB|NB|NF|NL|NS|NT|NU|ON|PE|QC|SK|YT|LB|QB)' LERG6.DAT | sort  -k 1.78 > LERG6.canadaOnly.DAT

namespace :sg1 do
  desc "Merge duplicate lerg6 entries"
  task :dedup_rate_centers => :environment do
    unique = Hash.new
    removed = 1
    RateCenter.find_each { |rc|
      next unless rc.lerg6name
      key = (rc.province||"") + "/" + rc.lerg6name
      if unique[key]
        rc_other = unique[key]
        puts "Duplicate: #{rc.id}/#{rc.province}/#{rc.lerg6name} #{rc.localnames}  duplicates #{rc_other.id}/#{rc_other.province}/#{rc_other.lerg6name} #{rc_other.localnames}"

        cnt1_nae = rc.north_american_exchanges.count
        cnt2_nae = rc_other.north_american_exchanges.count
        cnt1_le = rc.local_exchanges.count
        cnt2_le = rc_other.local_exchanges.count
        if rc.active? and rc_other.inactive?
          rc_keep = rc
          rc_drop = rc_other
        elsif rc.inactive? and rc_other.active?
          rc_keep = rc_other
          rc_drop = rc
        elsif cnt1_nae > cnt2_nae
          rc_keep = rc
          rc_drop = rc_other
        elsif cnt2_nae > cnt1_nae
          rc_keep = rc_other
          rc_drop = rc
        elsif cnt1_le < cnt2_le
          rc_keep = rc
          rc_drop = rc_other
        elsif cnt1_le > cnt2_le
          rc_keep = rc_other
          rc_drop = rc
        elsif rc.id < rc_other.id
          rc_keep = rc
          rc_drop = rc_other
        else
          rc_keep = rc_other
          rc_drop = rc
        end

        puts "Keeping: #{rc_keep.id}/#{rc_keep.lerg6name} #{rc_keep.localnames} (#{cnt1_nae},#{cnt1_le} <=> #{cnt2_nae},#{cnt2_le}"
        # merge localnames
        localnames = Hash.new
        rc_keep.localnames.each { |n| localnames[n]=true }
        rc_drop.localnames.each { |n| localnames[n]=true }
        rc_keep.localnames = localnames.keys

        # merge LE entries
        rc_drop.local_exchanges.each { |le| le.rate_center = rc_keep; le.save! }

        # merge NAE entries
        rc_drop.north_american_exchanges.each { |nae| nae.rate_center = rc_keep; nae.save! }

        # really drop it.
        rc_drop.delete

        unique[key] = rc_keep
        removed += 1
      else
        unique[key] = rc
      end
    }
    puts "deduplicated: #{removed}"
  end

  desc "Writes the LCA file to LCA=file"
  task :writeLCAfile => :environment do
    lca_list = Hash.new

    lca_list['THINKTEL_QC']="local_exchanges.province = 'QC' and local_exchanges.carrier = 'Thinktel'"
    lca_list['THINKTEL_NATIONAL']="local_exchanges.carrier = 'Thinktel'"
    lca_list['ISPTELECOM_QC']="local_exchanges.carrier = 'ISPTelecom' and local_exchanges.province = 'QC'"

    lcafile_name = ENV['LCA']
    File.open(lcafile_name, "w") do |lcafile|

      dflt="DFLT"
      lca_list.each { |key, filter|
        rc_list = []
        repeated = Hash.new
        LocalExchange.where(filter).includes(:rate_center).distinct("rate_centers.lerg6name").order("rate_centers.lerg6name").each { |le|
          unless le.rate_center
            byebug
          end
          if le.rate_center.inactive?
            puts "LE #{le.id} #{le.name} points to inactive rc#{le.rate_center.id} #{le.rate_center.name}"
            next
          end

          unless repeated[le.rate_center.lerg6name]
            rc_list << le.rate_center
            repeated[le.rate_center.lerg6name] = le.rate_center
          end
        }

        # always add DIR ASST.
        dirasst = RateCenter.where(lerg6name: 'DIR ASST').take
        if dirasst
          rc_list << dirasst
        end

        if dflt
          lcafile.puts "<LCABLOCK>\nDFLT_LCA_ID\n"
          dflt = false
        else
          lcafile.puts "<LCABLOCK>\n#{key}_LCA_ID\n"
        end
        lcafile.puts "<LCAGROUP>\n"
        lcafile.print "#{key}_LCA_GROUP;"
        rc_list.each { |rc|
          lcafile.print rc.lca_item
        }
        lcafile.puts "\n</LCAGROUP>\n"

        cnt = 0
        rc_list.each { |rc|
          lcafile.print rc.lca_block(key)
          cnt += 1
        }
        lcafile.puts "</LCABLOCK>\n"
      }
    end
  end

  desc "Writes the NNACL file to NNACL=file"
  task :write_NNACL => :environment do
    nnaclfile = ENV['NNACL']

    cnt = 0
    File.open(nnaclfile, "w") do |nnacl|
      NorthAmericanExchange.all.order(:npa,:nxx).each { |nae|
        nnacl.puts nae.to_nnacl
        cnt += 1
      }
    end
    puts "Wrote #{cnt} lines"
  end
end


namespace :novavision do

  @defaultFieldSeperator = "\t"

  desc "Do a list of NPANXX for CARRIER=, Optional: PROV=, SEP= (for seperator)"
  task :listNPANXX => :environment do
    if ENV['CARRIER'].nil?
        data = getCarriers();

        puts "Must specify a carrier with CARRIER=, Optional: PROV=, SEP= (for seperator), FILE= (for ouput)"
        format="%-20s\t%s\t%6s\t\t%s\n"
        printf(format, "Carrier", "Prov", "Count", "Last update");
        data.each { | row | printf(format, row['carrier'], row['province'], row['count'], row['max'][0..15]) ; }
        exit 1
    end

    provArg = ''
    provArgPhantom = "('PQ', 'AB', 'BC', 'YK', 'MB', 'NB', 'NF', 'NL', 'NS', 'NT', 'NU', 'VU', 'ON', 'PE', 'QC', 'SK', 'YT', 'LB', 'QB')"
    if ! ENV['PROV'].nil? or ! ENV['PROVINCE'].nil?
        provStr = ENV['PROVINCE'] if ENV['PROV'].nil?
        provArg = "and l.province = '#{provStr}' ";
        provArgPhantom = "('#{provStr}')"
    end

    q  = "select npa, nxx from north_american_exchanges na, local_exchanges l, local_exchanges_north_american_exchanges lna where lna.local_exchanges_id = l.id and lna.north_american_exchanges_id = na.id and l.carrier = '#{ENV['CARRIER']}' #{provArg} "
    q += "\n UNION \n"
    q += " select npa, nxx from north_american_exchanges na where name in ('XXXXXXXXXX', 'DIR ASST') and province in #{provArgPhantom} "
    q += " order by npa, nxx "
    q += " ; "
    $stderr.puts q if ENV['quiet'].nil? or ENV['quiet'] != 'very'

    npanxx = executeSQL(q, 'getNpaNxx')

    q  = "select npa, nxx from north_american_exchanges na, local_exchanges l, local_exchanges_north_american_exchanges lna where lna.local_exchanges_id = l.id and lna.north_american_exchanges_id = na.id and l.carrier = '#{ENV['CARRIER']}' #{provArg} "
    q += "\n UNION \n"
    q += " select npa, nxx from north_american_exchanges na where name in ('XXXXXXXXXX', 'DIR ASST') and province in #{provArgPhantom} "
    q += " order by npa, nxx "
    q += " ; "
    $stderr.puts q if ENV['quiet'].nil? or ENV['quiet'] != 'very'

    #q = "select province, npa, nxx from north_american_exchanges na where name in ('XXXXXXXXXX', 'DIR ASST') and province in #{provSql} order by npa, nxx;"

    #$stderr.puts q

    #npanxxPhantom = executeSQL(q, 'getNpaNxxPhantom')

    #debugger

    seperator = ''
    if ENV['SEP']
        seperator = ENV['SEP']
    end


    out = $stdout
    if ! ENV['FILE'].nil?
        out = File.open(ENV['FILE'], 'w')
    end

    npanxx.each { |npax| out.printf("%s%s%s\n", npax['npa'], seperator, npax['nxx']) }

    if ! ENV['FILE'].nil?
        out.close
    end

  end


  def purgeCarrierData()
    if LocalExchange.where(:carrier => carrier).count > 0
        confirm = confirmClear("There is already #{carrier} data in the table.  Clear it?  Answering no will NOT exit but continue the import.")

	    if confirm
		    puts 'Deleting data....'
		    LocalExchange.where(:carrier => carrier).delete_all
	    end
    end
  end

  def readTSVline(l)
     seperator=@defaultFieldSeperator
     if ! ENV['sep'].blank?
        seperator= ENV['sep']
     end

     CSV.parse_line(l, :col_sep => seperator).collect{|x|
        if ! x.nil?
            x.strip;
        end
     }
  end




  def localExchangesSanityCheck(carrier)
    puts "\nHere is a random sample of what has been imported:\n"
    sleep 1
    format = "%50s\t%-10s\t%s\n"
    printf(format, "Name", "Province", "Carrier")
    LocalExchange.where(:carrier => carrier).order("RANDOM()").first(20).sort_by{|row|
        row['name']
    }.each { |row|
        printf(format, row['name'], row['province'], row['carrier'])
    }
  end

  def deleteNotFoundRateCenters(checkList, type, carrier = nil)
    actions = []
    if checkList.count > 0
        notFoundList = ''

        checkList.each { |le|
            case type
                when 'local'
                    notFoundList += "#{le['name']}\t#{le['province']}\n"
                when 'lerg8'
                    notFoundList += "#{le['shortName']}\t#{le['fullName']}\t#{le['province']}\n"
                when 'lerg6'
                    notFoundList += "#{le['npa']}\t#{le['nxx']}\t#{le['name']}\t#{le['province']}\n"
                else
                    $stderr.puts "Unrecognized type '#{type}' given to deleteNotFoundRateCenters"
                    exit 1
            end
        }


	    confirm = confirmClear("\nThe following entries were not found.\n" + notFoundList + "Clear the above not found entries?\n")
		if confirm
           puts 'Deleting data....' if ENV['quiet'].nil?
        end

        checkList.each { |le|
            actionHash = {:province => le.province.upcase}
            if confirm
                ## See http://stackoverflow.com/questions/27275766/rails-has-many-with-through-option-loses-joins
                ### why we do a direct sql
                #sql = "DELETE FROM local_exchanges WHERE carrier = '#{carrier}' and name = \"#{le.name}\" and province = '#{le.province}'"
                #ActiveRecord::Base.connection.execute(sql)
                case type
                    when 'local'
                        actionHash.merge!({:name => le.name.upcase, :carrier => le.carrier})
                        LocalExchange.destroy(le.id)
                    when 'lerg8'
                        actionHash.merge!({:shortName => le.shortName, :fullName => le.fullName})
                        Lerg8.destroy(le.id)
                    when 'lerg6'
                        actionHash.merge!({:name => le.name, :npa => le.npa, :nxx => le.nxx})
                        NorthAmericanExchange.destroy(le.id)
                end

                action = 'Deleted'
            else
                action = 'Not found, not deleted'
            end

            actionHash.merge!({:action => action})

            actions.push(actionHash)

        }
    end

    return actions
  end



  def confirmClear(question)
    confirm = nil
    if ENV['deleteMissing'].nil?
	    puts
		positives = ['yes', 'y', 'oui', 'o']
		prompt = '> '

	    puts question + "\n"
		puts "To clear data, answer: " + positives.join(", ")
		print prompt
	    $stdin.flush
	    clear = $stdin.gets.chomp

		confirm = positives.include?(clear)

	    if confirm
	        puts "Will clear data... "
	    else
	        puts "Not clearing data... "
	    end

	    puts
    else
        confirm = ENV['deleteMissing'] == 'yes'
    end
    return confirm
  end

    def printTimeDiff(start, finish)
        diff = finish - start
        units = 'seconds'
        if diff >= 60
            diff = diff/60
            units = 'minutes'
        end
        puts "#{ diff.round(1) } #{ units }. "
    end


    def matchByReplace(fromStr, toStr)
        puts "\nRun a query matching all rate centers that are an exact match if replacing #{fromStr} with #{toStr}) ..."
        start = Time.now
        q = "insert into local_exchanges_north_american_exchanges (local_exchanges_id, north_american_exchanges_id) "\
            "select distinct l.id, na.id "\
            "from local_exchanges l, north_american_exchanges na "\
            "where l.province = na.province and regexp_replace(regexp_replace(l.name, '\\W+', '', 'g'), '#{fromStr}', '#{toStr}', 'g') = regexp_replace(na.name, '\\W+', '', 'g') "\
            "and l.id not in (select distinct local_exchanges_id from local_exchanges_north_american_exchanges); "
        executeSQL(q, "from #{fromStr} to #{toStr}")
        finish = Time.now
        printTimeDiff(start, finish)

        LocalExchangesNorthAmericanExchange.report_match_rate()
    end

    # Writes the LCA file
    # Your LCA config file should be formatted as
    # LCA_GROUP:andClause
    # The local_exchanges table is shorthanded to l,
    # For example, an LCA GROUP with all Thinktel not in Quebec plus another LCA group for all of comwave:
    # THINKTEL_NON_QC:l.carrier='Thinktel' and l.province != 'QC'
    # COMWAVE:l.carrier='Comwave'
    # If no config file specified, it will go by rate centers

    desc "Writes the LCA file using by default short names (long names if longNames=yes) and config file specific by config=/full/path/to/config"
    task :writeLCAfile => :environment do

      lcaGroups = {}
      if !ENV['config'].blank?
        # Read the config file to figure out how to query
        #debugger

        configFilePath = ENV['config']
        $stderr.puts "Reading " + configFilePath
        f = File.new(configFilePath)

        f.readlines.map do |line|
          lcaLabel, andCondition  = line.split(':');
          lcaGroups[lcaLabel]    = andCondition;
        end
      else
        # Default to one lca group per carrier, all rate centers of that carrrier,
        q = "select distinct carrier from local_exchanges;"
        carriers = executeSQL(q, 'getAllCarriers')
        carriers.each { |row|
          carrier = row['carrier']
          andCondition = "l.carrier = '#{carrier}'";
          lcaGroups[carrier]    = andCondition;
        }
      end

      if ENV['nameType'].nil? || (ENV['nameType'] != 'short' && ENV['nameType'] != 'long')
        $stderr.puts
        $stderr.puts "To avoid any confusion, nameType= is *mandatory* and must be set to either short or long"
      end


      ### Some for each loop here  calling writeLCABlock #####################
      shortnames = ENV['nameType'] == 'short'
      i = 0
      lcaGroups.each { |lcaName, andCondition|
        i += 1
        rateCenterSet = getRateCenters(andCondition, lcaName, shortnames)

        $stderr.puts "Returning a set for #{lcaName} with #{rateCenterSet.select {|i| ! i.nil?}.count} non nulls and #{rateCenterSet.count} items."

        if !ENV['countOnly']
          writeLCABlock(lcaName, rateCenterSet, shortnames, i==1)
        end
      }
    end


    def getRateCenters(andCondition, lcaName, shortnames=true)
      q = "select distinct \"fullName\" as name "\
          "from north_american_exchanges na, local_exchanges l, lerg8s e, local_exchanges_north_american_exchanges j "\
          "where 1=1 "\
          "and l.id = j.local_exchanges_id and j.north_american_exchanges_id = na.id " \
          "and na.name = e.\"shortName\" " \
          "and na.province = e.province " \
          "and #{andCondition} " \
          "order by \"fullName\" "

      if shortnames then
          q = "select distinct na.name as name "\
           "from north_american_exchanges na, local_exchanges l, local_exchanges_north_american_exchanges j "\
           "where 1=1 "\
           "and l.id = j.local_exchanges_id and j.north_american_exchanges_id = na.id "\
           "and #{andCondition} " \
           "order by name"
      end

      $stderr.puts q


      c = ActiveRecord::Base.connection.raw_connection
      statement_name = 'getAllRateCentersForPeer'+lcaName
      c.prepare(statement_name, q)

      rateCenters = c.exec_prepared(statement_name)

      $stderr.puts "Got #{rateCenters.count} rows"

      returnSet = Set.new

      rateCenters.each {|row|
        returnSet.add(row['name'])
      }


      return returnSet

    end




    def writeLCABlock(lcaGroup, rateCenters, shortnames, dfltLCAID)

      puts '<LCABLOCK>'

      if ! dfltLCAID
        carrierLCAID = lcaGroup.upcase + '_LCA_ID'
      else
        carrierLCAID = 'DFLT_LCA_ID'
      end
      puts carrierLCAID

      carrierLcaGroup = lcaGroup.upcase + '_LCA_GROUP;'

      lcaGroupTag = '<LCAGROUP>' + "\n" + carrierLcaGroup
      allLcaTags  = ''

      $stderr.puts "Working on #{lcaGroup} with #{rateCenters.count} rate centers, #{rateCenters.select {|i| ! i.nil?}.count} non null"

      rateCenters.each { |rateCenter|
          rateCenter, allLcaTags = generateGroupAndTags(rateCenter, lcaGroupTag, allLcaTags, carrierLcaGroup)
      }

      ### Now add DIRECTORY ASSISTANCE and RATE CENTER NOT APPLICABLE.

      fakeRateCenters = ['DIRECTORY ASSISTANCE', 'RATE CENTER NOT APPLICABLE']

      if shortnames
        fakeRateCenters = ['DIR ASST', 'XXXXXXXXXX']
      end


      fakeRateCenters.each { |rateCenter|
          rateCenter, allLcaTags = generateGroupAndTags(rateCenter, lcaGroupTag, allLcaTags, carrierLcaGroup, true)
      }

      lcaGroupTag += "\n</LCAGROUP>\n\n"

      puts lcaGroupTag

      puts allLcaTags

      puts "\n</LCABLOCK>"

    end


    def generateGroupAndTags(rateCenter, lcaGroupTag, allLcaTags, carrierLcaGroup, phantom = false)
        maxLength = 30

        rateCenter = rateCenter.strip

        if rateCenter.length > maxLength
          rateCenter = rateCenter[0, maxLength]
        end

        num = phantom ? 99999 : 888
        rateCenterLabel = "#{num}," + rateCenter + ';'
        lcaGroupTag << rateCenterLabel

        allLcaTags << "<LCA>\n" + rateCenterLabel + carrierLcaGroup + "\n</LCA>\n\n"

        return lcaGroupTag, allLcaTags

    end



    def is_numeric(string)
        return Float(string) != nil rescue false
    end


    def manualMatches(byHowMany)
        c = ActiveRecord::Base.connection.raw_connection
        prompt = '> '

        puts "\nRunning query for best levenshtein matches for manual matching"
        start = Time.now
        q = "select le.* "\
            "from lavenshtein_exchanges le, "\
            "(select province, localname, min(lev) as minlev from lavenshtein_exchanges group by province, localname) mins "\
            "where lev = mins.minlev "\
            "and le.province = mins.province and le.localname = mins.localname "\
            "and (le.province, le.localname, le.LergName) not in (select distinct province, localname, lerg6name from exchanges_manual_matches) "\
            "order by le.lev * 100 / char_length(le.localname) asc;"
        puts q
        results = executeSQL(q, 'select best lavenshtein')
        finish = Time.now
        printTimeDiff(start, finish)



        puts "\nAsking to match by #{byHowMany}..."
        start = Time.now

        #Go through each by #{byHowMany} to manually match
        total = results.count; i = 0
        results.each_slice(byHowMany) {|row|
	        puts "\nEst-ce que les #{byHowMany} suivants concordent?  Do the following #{byHowMany} match? (#{i} / #{total})"
            puts "y,o,Y,O = Yes / Oui   n,N = Non   c = Cancel / Arreter"
            j = 0
            row.each_with_index { |a,j|
                puts "#{j} : #{a['province']}, #{a['localname']} (#{a['localname'].length} chars) => #{a['lergname']}  (lev = #{a['lev']})  ";
            }
    	    print prompt

	        match = $stdin.gets.chomp

            if match == "c" then break; end

            positives = ['yes', 'y', 'oui', 'o']
            if positives.include?(match)
                row.each { |a| c.exec_prepared('insert manual match', [ a['province'], a['localname'], a['lergname'] ] ) }
            end

            # TODO: if no then enter in non-matches

            i += byHowMany
        }
        finish = Time.now
        printTimeDiff(start, finish)


        updateWithManualMatches()
    end


    def updateWithManualMatches
        start = Time.now
        puts "\nUpdating match table with manual matches..."
        q = "insert into local_exchanges_north_american_exchanges (local_exchanges_id, north_american_exchanges_id) "\
            "select distinct l.id, na.id "\
            "from local_exchanges l, north_american_exchanges na, exchanges_manual_matches m "\
            "where l.province = na.province and l.province = m.province and na.province = m.province "\
            "and l.name = m.localname and na.name = m.lerg6name "\
            "and l.id not in (select distinct local_exchanges_id from local_exchanges_north_american_exchanges); "
        executeSQL(q, 'updating table with manual matches')
        finish = Time.now

        LocalExchangesNorthAmericanExchange.report_match_rate()
    end

    def executeSQL(q, queryName)
        c = ActiveRecord::Base.connection.raw_connection

        if ! defined? @statementsReady
            @statementsReady = Array.new
        end


        if ! @statementsReady.include? queryName
            #puts "#{ queryName } not in #{ @@statementsReady.join(', ') }"
            c.prepare(queryName, q)
            @statementsReady << queryName
        end


        result = c.exec_prepared(queryName)
        return result
    end


    desc "Do a count of the lerg6 data to check if data is present"
    task :checkLERG6 => :environment do
        q = "select province, npa, count(id), min(updated_at) from north_american_exchanges group by province, npa order by province, npa;"

        data = executeSQL(q, 'lerg6check');

        if data.num_tuples.zero?
            puts "No data found in north_american_exchanges table"
        end

        format="%s\t%s\t%6s\t\t%s\n"
        printf(format, "Prov", "NPA", "Count", "Min update time")
        data.each { | row | printf(format, row['province'], row['npa'], row['count'], row['min'][0..15]) ; }
    end

    def getCarriers()
        q = "select carrier, province, count(id), max(updated_at) from local_exchanges group by carrier, province order by carrier, province;"
        data = executeSQL(q, 'listCarriers');

        return data
    end

    desc "List carriers in the local exchange table. This is useful to check if your spelling is correct before running importCarrier."
    task :listCarriers => :environment do

        data = getCarriers();

        if data.num_tuples.zero?
            puts "No data found in local_exchanges table"
        end

        format="%-20s\t%s\t%6s\t\t%s\n"
        printf(format, "Carrier", "Prov", "Count", "Last update");
        data.each { | row | printf(format, row['carrier'], row['province'], row['count'], row['max'][0..15]) ; }
    end

    desc "Tell me if a value of in one csv column is in another csv column of a different or same file and carry out specified action"
    task :isXinY => :environment do
        start = Time.now()

        # Check if mandatory arguments are there
        if ENV['X'].nil? || ENV['Y'].nil? || ENV.count == 0
            puts "Mandatory arguments are:"
            puts "\tX       = Path,column number of csv column with needles (defaults to 1st column)"
            puts "\tY       = Path,column number of csv column with haystack"

            puts

            puts "Code assumes linux bash syntax for wc -l and free"

            puts

            puts "Optional argumentss are "
            puts "\taction  = Action to take.  Possibilies are:"
            puts "\t\t    output:\t\tKeep rows that contain the needle in the sepecied column (default)"
            puts "\t\t    append,$column:\tAppend the specified column to the end of the row in output. No default."
            puts "\tsep     = Seperator for csv file (optional). Defaults to comma."
            puts "\tincludeHeaders  = If set to anything, include first row in both files.  Deafault behavior is to skip"
            puts "\tmaxNeedleLength = Max length of a needle. Only a hint to determine process of searching.  Won't affect needle or search results.  Defaults to 100"
            puts
            puts "\t\tSo"
            puts "\t\t  novavision:isXinY X=needles.csv,3 Y=haystack.csv,5 action=append,2  "
            puts "\t\twill seach values listed in the 3rd column of needles.csv in de 5th column of haystack.csv and"
            puts "\t\tappend the rows of the 2nd column in it's output of haystack.csv"
            puts
            puts "\tBehavior is untested if file paths have commas, most likely fail in error"
            puts "\tComparaison of haystack and needles will strip ends of whitespace"
            puts "\tColumn count starts at ONE"
            puts

            exit 1
        end

        # Check if file paths are valid
        ['X', 'Y'].each { |a|
            path = ENV[a].split(',')[0]
            if ! Pathname.new(path).exist?
                $stderr.puts "The file for argument #{a} does not seem to exist - #{path}"
                exit 1
            end
        }

        # Ok, now do the work
        skipHeaders = ENV['includeHeaders'].nil?

        action          = 'output'

        if ! ENV['action'].nil?
            case ENV['action'].split(',')[0]
                when 'output'
                when 'append'
                    action = ENV['action']
                else
                    $stderr.puts "I don't know what kind of action is #{ENV['action']}"
            end
        end


        # Parse arguments for paths and columns
        needlePath, needleColumn = ENV['X'].split(',')
        haystackPath, haystackColumn = ENV['Y'].split(',')
        if needleColumn.nil?
            needleColumn = 1;
        end
        needleColumn = needleColumn.to_i - 1;
        haystackColumn = haystackColumn.to_i - 1


        # Determine if we are loading all needles and scanning the hay stack
        maxNeedleLength = 100
        loadAllNeedles = FALSE

        if ! ENV['maxNeedleLength'].nil?
            maxNeedleLength = ENV['maxNeedleLength']
        end

        numberOfNeedles = %x(wc -l #{needlePath}).strip.split(' ')[0].to_i
        memoryCanUse = %x(free -b).split(" ")[9].to_i/3   # Divide by 3 is just a precaution

        # Assuming UTF8, hence why we multiply by 4
        if numberOfNeedles * maxNeedleLength * 4 < memoryCanUse
            loadAllNeedles = TRUE
        end

        require "csv"
        needleCount = 0
        needles = []
        $stderr.puts "Reading #{needlePath}"
        File.foreach(needlePath) { |l|
            needleCount += 1
            next if needleCount == 1 && skipHeaders

            needle = readTSVline(valid_string)[needleColumn]

            next if  needle.nil?

            needle.strip!

            if ! loadAllNeedles
                searchHaystack(haystackPath, needle, haystackColumn, action, skipHeaders)
            else
                needles.push(needle)
            end

        }


        if loadAllNeedles
            searchHaystack(haystackPath, needles, haystackColumn, action)
        end


        done = Time.now()

        $stderr.puts "#{(done-start).round}  seconds"
    end


    #####
    # search a haystack in the form of a csv file
    # haystackPath = The path of the file to search
    # needle = The string or strings (in an array) to search
    # action = What to do with a match in the haystack
    # skipHeaders = skip searching the first line?
    #####
    def searchHaystack(haystackPath, needle, haystackColumn, action = 'output', skipHeaders = TRUE)
        haystackCount = 0
        $stderr.puts "Reading #{haystackPath}, searching for #{needle}"
        File.foreach(haystackPath) { |h|
            haystackCount += 1
            if haystackCount == 1 && skipHeaders
                puts h
                next
            end


            hayLine = readTSVline(h)
            hay     = hayLine[haystackColumn]

            if hay.class.name == 'String'
                hay.strip!

                match = FALSE

                if needle.class.name == 'String' && hay == needle
                    match = TRUE
                elsif needle.class.name == 'Array' && needle.include?(hay)
                    match = TRUE
                end



                if match
                    case action.split(',')[0]
                        when 'output'
                            puts h
                        when 'append'
                            appendColumn = action.split(',')[1].strip.to_i - 1
                            appendCell   = hayLine[appendColumn]

						    seperator="\t"
						    if ! ENV['sep'].blank?
						       seperator= ENV['sep']
						    end

                            puts h.strip + seperator + appendCell

                        else
                            puts "Undefined action #{action}"
                            exit 1
                    end
                # If the action is append, output non matching lines
                elsif action.split(',')[0] == 'append'
                    puts h
                end
            end


        }
    end



end


