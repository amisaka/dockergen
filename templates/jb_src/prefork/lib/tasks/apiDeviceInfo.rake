require 'nokogiri'
require 'active_support/core_ext/hash'
require 'open-uri'
require 'set'

namespace :novavision do



@errorCount = 0
@lastCall = Time.now
@baseUrl  = $XSP_URL;



desc "Setup connection continously prompt for URL"
task :queryAPI do
    username, password, userIDs = userInputs()
    url = ''
    loops=0

    dataLength = 0
    @lastCall = Time.now
    while(true)
        loops += 1
        @errorCount = 0
        if dataLength > 95 or url.empty? or ! (url.include?('%USERID%') or url.include?('<userid>'))
    	    if (ENV['url'].nil? or ENV['url'].empty?) or loops > 1
    	        $stderr.print @baseUrl
    	        $stdin.flush
    	        url = $stdin.gets.chomp
    	    else
    	        url = ENV['url']
    	    end
        end

        info = nil
        if url.include?('%USERID%') or url.include?('<userid>')
            resultType, comments, info = getInfo(url, username, password, userIDs.sample, 'custom')
        else
            resultType, comments, info = getInfo(url, username, password, 'nilUserID', 'custom')
        end

        dataType = info.class.name
        if dataType == "array" or dataType == "Hash"
            str = JSON.pretty_generate(info)
            puts str
            dataLength = str.length
        else
            $stderr.puts "Cannot generate JSON with #{dataType} data type"
        end
    end



end


desc "get device info for a list of broadworks ID, producing a table of userids and vDID (deviceName). Optional maxError="
task :deviceInfo do
    username, password, userIDs = userInputs()


    @lastCall = Time.now
    puts ["User ID", "Comments", "Phone number", "Extension", "Alternate Numbers", "Device name"].join("\t")
    userIDs.each { |userID|

        commentsAll = Set.new
        resultType  = 'nothing'

        profileUrl     = "user/%USERID%/profile"
        resultType, comments, profileInfo = getInfo(profileUrl, username, password, userID, 'profile')
        commentsAll.merge(comments)

        number = extension = ''
        profileBase = nil
        if ! profileInfo.nil? and profileInfo.class.name == 'Hash'
            profileBase = profileInfo['Profile']['details']
            number      = profileBase['number']['$'] if ! profileBase['number'].nil?
            extension   = profileBase['extension']['$'] if ! profileBase['extension'].nil?
        elsif profileInfo.class.name == 'String'
            errStr      = profileInfo
            number      = errStr
            extension   = errStr
        end

        altNumUrl = "user/%USERID%/services/AlternateNumbers"
        resultType, comments, altNumInfo = getInfo(altNumUrl, username, password, userID, 'alternateNumbers')
        commentsAll.merge(comments)


        altNumbers = Set.new
        if ! altNumInfo.nil? and altNumInfo.class.name == 'Hash' and ! altNumInfo['AlternateNumbers']['numberEntry'].nil?
            altNumEntries = altNumInfo['AlternateNumbers']['numberEntry']

            # If there is only one altNumEntry, it will be a hash of data for that single entry,
            # So we need to wrap it.
            if altNumEntries.class.name == 'Hash'
                altNumEntries = [altNumEntries]
            end

	        altNumEntries.each { |ne|
	            number = ne['phoneNumber']['$']
	            extension = ''
	            extension = ne['extension']['$'] if ! ne['extension'].nil?

	            fullNumber = number
	            fullNumber << extension if extension != ''

	            altNumbers.add(fullNumber)
	        }
        elsif altNumInfo.class.name == 'String'
            errStr  = altNumInfo
            altNumbers.add(errStr)
        end

	    # The deviceName (which we THINK is the vDID), is under AccessDevices->accessDevice->deviceName
        deviceUrl     = "user/%USERID%/profile/device"
        resultType, comments, deviceInfo = getInfo(deviceUrl, username, password, userID, 'device')
        commentsAll.merge(comments)
	    devicesName = Set.new
	    accessDevices = []
        if ! deviceInfo.nil?
		    accessDevices = deviceInfo['AccessDevices']['accessDevice'] if ! deviceInfo['AccessDevices'].nil? and !deviceInfo['AccessDevices']['accessDevice'].nil?
	        accessDevices = [].push(accessDevices) if accessDevices.is_a?({}.class)
		    accessDevices.each { |ad|
		        #if ! ad['devicesName'].nil? and ! ad['devicesName']['$'].nil?
		        if ! ad.nil? and ! ad['deviceName'].nil? and ! ad['deviceName']['$'].nil?
		            deviceName = ad['deviceName']['$'].strip
		            if /\A[-+]?\d+\z/.match(deviceName)
		                devicesName.add(deviceName.to_i)
	                    resultType  = 'good'
	                    comments.add("Numeric device name")
		            elsif /\d+{10}/.match(deviceName)
		                devicesName.add(deviceName)
	                    resultType = 'odd' if resultType != 'good'
	                    commentsAll.add("Not entirely numeric device name")
		            else
	                    resultType = 'ok' if resultType != 'good'
		            end
		        else
	                commentsAll.add("Ad or devicesName nil")
	                resultType = 'adNil'
	                debugger
		        end
		    }
        end

        if accessDevices.count === 0 || accessDevices.first.count === 0
            commentsAll.add("No access device")
            resultType = 'noAccessDevice'
        end

        puts [userID, commentsAll.to_a.join(', '), number, extension, altNumbers.to_a.join(', '), devicesName.to_a.join(', ')].join("\t")

        if ! ENV['debug'].nil? and resultType == 'nothing'
            debugger
        end
    }
end



def userInputs()

    # Check if we have a file at the specified path
    if ENV['userIdsIn'].nil?
        $stderr.puts "Need a file with a list of user IDs which path is specified by userIdsIn="
        exit 1
    end

    userIdsFilePath = ENV['userIdsIn']
    userIDs = readBWuserIDs(userIdsFilePath)

    username, password = login()

    return username, password, userIDs
end

def login()
    username = password = ''

    # Check if we have a user name
    if ENV['user'].nil? or ENV['user'].empty?
        $stdin.flush
        $stderr.print "Username (Tip: next time, use user=)? "
        username = $stdin.gets.chomp
    else
        username = ENV['user']
    end


    # Prompt for password
    $stderr.print "Password for user #{username}? "
    system 'stty -echo'
    password = $stdin.gets.chomp
    system 'stty echo'
    $stderr.puts

    return username, password
end


def readBWuserIDs(userIdsFilePath)
    if ! File.exists?(userIdsFilePath)
        $stderr.puts "There does not seem to be a file at userIdsFilePath."
        exit 1
    end


    # Print headers
    #puts ["Broadworks userID", "Comments" "Numbers"].join("\t")

    ### Open the file to get all the user IDs
    #userIDs = ['5144444742x3214@voip.ip4b.net']
    userIDs = []
    f = File.open(userIdsFilePath, "r")
    $stderr.puts "Reading #{userIdsFilePath}..."
    lineCount = 0
    userLastCharIndex = -1
    f.each_line do |line|
        lineCount += 1

        ### Determine if it's a header line
        if [/Group/, /Hosting/, /NE/, /Enterprise/, /Site/, /User/, /DN Type/].all? { |pattern|
            pattern.match(line)
        }
            ### If so, determine the postion of the user column
            userLastCharIndex = line.index('User') + 4
        end

        next if userLastCharIndex === -1

        ### Determine if it's a data column
        if [/DFLT_SITE/, /hosted/, /as/].all? { |pattern| pattern.match(line) }

            ### Now we have to determine the starting position of the userID, backtracking from last char
            userBeginPos = line.reverse.index(/\s/, line.length-userLastCharIndex)

            ### Get and add that userID
            userID = line[line.length-userBeginPos, userLastCharIndex-(line.length-userBeginPos)]
            userIDs.push(userID)
        end
    end

    f.close
    $stderr.puts "#{lineCount} lines read"
    $stderr.puts "#{userIDs.count} users loaded"
    userIDs.uniq!
    $stderr.puts "#{userIDs.count} unique users loaded"

    return userIDs

    #puts userIDs.join(' ')
end


def formatUrl(url, userID, attempt)
        userID  = userID += '@voip.ip4b.net' if (! userID.include?('@')) and attempt == 1
        url = @baseUrl + url
        url.sub!(/%USERID%/, userID)
        url.sub!(/<userid>/, userID)

        url = url + '?format=json' if ! url.ends_with?('json')

        return url
end


def getInfo(initialUrl, username, password, userID, infoType)
        attempt = 0
        comments = Set.new

        # Determine max error
        maxError   = 3
        maxError   = ENV['maxError'].to_i if ! ENV['maxError'].nil?

        theInfoStr = ''
        begin
            attempt += 1
            url = formatUrl(initialUrl, userID, attempt)
            $stderr.puts "Opening #{url} ..."
            sleep [[(Time.now - @lastCall)*2, 0.1].max, 10].min
            @lastCall   = Time.now
	        theInfoStr  = open(url, http_basic_authentication: [username,password]).read()
            @errorCount -= 1 if @errorCount > 0
        rescue OpenURI::HTTPError => e
            retry if attempt < 2
            resultType = 'httpError'
            @errorCount += 1
            $stderr.puts "OpenURI::HTTPError: #{e.to_s}"
            if @errorCount > maxError
                $stderr.puts "Error count reached max error of #{maxError}"
                exit 1
            else
                #debugger if e.to_s.include?("401")
                $stderr.puts "Continous error count #{@errorCount} of #{maxError}"
                #comments.add(e.to_s)
                return resultType, comments, e.to_s
            end
        end


	    theInfo     = nil
        begin
    	    theInfo     = JSON.parse(theInfoStr)
        rescue JSON::ParserError => e
            resultType = 'parseError'
            $stderr.puts "JSON parser error: #{e.to_s}"
            $stderr.puts "We got the following string from url: #{url}:\n#{theInfoStr}"
            #puts [userID, "JSON parsing error"].join("\t")
            debugger
        end
	    #puts "Counted #{theInfo.values[0].count} top elements and the keys were #{theInfo.values[0].keys.join(", ")}"

        strPath = "/tmp/bwApi.#{infoType}.#{resultType}.#{userID}.json"
        File.open(strPath, 'w') {|f| f.write(JSON.pretty_generate(theInfo)) }


        return resultType, comments, theInfo
end

# Flatten a multi-dimensial hash into keys of strigs.
# @author URi Agassi http://stackoverflow.com/questions/23521230/flattening-nested-hash-to-a-single-hash-with-ruby-rails#answer-23521624
def flatten_hash(hash)
  hash.each_with_object({}) do |(k, v), h|
    if v.is_a? Hash
      flatten_hash(v).map do |h_k, h_v|
        h["#{k}.#{h_k}".to_sym] = h_v
      end
    else
      h[k] = v
    end
  end
end
end



=begin
Ditching this part of the code
=end

