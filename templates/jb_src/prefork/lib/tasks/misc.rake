# -*- ruby -*-

namespace :sg1 do

  desc "Fix BTNS that do not point to the client that they belong to"
  task :fix_disconnected_btn => :environment do
    deleted = 0

    Btn.active.where(client_id: nil).each { |b1|
      canon_b1 = RateCenter.canonify_number(b1.billingtelephonenumber)

      client = b1.phones.first.try(:client)
      unless client
        b1.phones.each {|p1|
          unless client
            client = p1.client
          end
        }
      end
      puts "#{b1.id} #{b1.billingtelephonenumber} #{b1.phones.activated.count} #{client}";

      if client

        client.btns.active.each { |b2|
          puts "    #{b2.id} #{b2.billingtelephonenumber} #{b2.phones.activated.count} "
          canon_b2 = RateCenter.canonify_number(b2.billingtelephonenumber)
          if b2.phones.activated.count == 0 and canon_b2 == canon_b1
            puts "       -> deleted"
            b2.delete
            deleted += 1
            next
          end
          b2.billingtelephonenumber = canon_b2
          b2.save!
        }

        b1.client = client
        b1.billingtelephonenumber = canon_b1
        b1.save!
      end
    }
    puts "Deleted #{deleted} btn"
  end

  desc "List Clients with more than one BTN"
  task :multibtnlist => :environment do
    File.open("/tmp/btnlist.txt", "w") do |f|
      Client.activated.find_each { |client|
        deadcount = client.btns.count
        activecount=client.btns.active.count
        if activecount > 1
          f.puts "Client #{client.id} #{client.name} active=#{activecount} (/#{deadcount})"
          client.btns.each { |btn|
            phonecount = btn.phones.activated.count
            phonedeadcount = btn.phones.count
            f.puts "    has btn #{btn.name} and has #{phonecount} active phones (#{phonedeadcount} total)"
          }
        end
      }
    end
  end
end

