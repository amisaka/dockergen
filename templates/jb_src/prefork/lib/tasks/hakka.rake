# coding: utf-8
# -*- ruby -*-
require 'csv'

namespace :sg1 do

  def import_cust_from_hakka(num, client, cust, year, month)
    invoice = client.most_recent_invoice(year, month)

    cust.create_billable_services(invoice)
    invoice.reload

    recurrence_count = 0

    invoice.billable_services.each { |bs|
      ri = bs.hakka_chargeassignment.recurrence_info
      if ri
        cycle_date  = ri[:AnnualCycleDate]
        freq_months = ri[:FrequencyMonths]
        month = cycle_date.try(:month)

        #puts "checking recurrence for #{bs.id}"

        case
        when (freq_months == 12 and invoice.service_start.month == month)
          # this is the month/invoice to attach this too.
          bs.active_month = true

        when (freq_months == 12 and month)
          # find a month in the future with the right month, create an invoice,
          # and attach the billable service to that invoice.
          ninvoice = invoice.invoice_next_month_num(month)
          bs.invoice = ninvoice
          puts "  recurrence for #{bs.id} moved from #{invoice.id} to #{ninvoice.id} #{ninvoice.invoice_date}"
          bs.save!
          recurrence_count += 1
        end
      end
    }

    #byebug if invoice.billable_services.count == 0

    printf "%u: cust %35s  code: %s [inv: %u/%s services: %u year-recurring: %u] %s\n", num,
           client.name, invoice.client.account_code,
           invoice.id, invoice.type,
           invoice.billable_services.count, recurrence_count,
           invoice.billable_services.first.try(:service_name)
  end

  def dump_hakka_stats
    $HAKKASTATS.keys.each {|thing|
      value = $HAKKASTATS[thing]
      puts "  Stat: #{thing} count: #{value}"
    }
  end

  desc "Import customers, services, products and relationships from MSSQL database (HAKKA)"
  task :hakkaimport => :environment do

    num = 0
    $HAKKASTATS = Hash.new(0)

    # use the list of charge assignments to discover clients that need

    if ENV['MONTH'] and ENV['YEAR']
      month = ENV['MONTH'].to_i
      year  = ENV['YEAR'].to_i
    else
      puts "Must set YEAR= and MONTH="
    end

    if ENV['CLIENT']
      client = Client.find(ENV['CLIENT'].to_i)
      cust = client.hakka_customer
      import_cust_from_hakka(1, client, cust, year, month)
      exit 0
    end


    clients_already_processed = Hash.new

    Hakka::ChargeAssignment.find_each { |ca|

      cust = ca.customer
      client = cust.associated_client

      next if clients_already_processed[client.id]

      import_cust_from_hakka(num, client, cust, year, month)
      clients_already_processed[client.id] = true
      num += 1
    }
    dump_hakku_stats
  end
end

namespace :hakka do

  desc "Import customer->salesperson mappings"
  task :customer_salesperson => :environment do
    Hakka::CustomerGroup.all.each { |cg|
      # find an related SuiteCRM.user by matching the *DESCRIPTION* directly.
      sgu = SuiteCrm::User.where(description: cg.Description).take
      sgucnt = 0
      sgusuccess=0
      unless sgu
        first = cg.Name[0..29]
        last  = cg.Name[30..59]||"" + "(imported)"
        sgu = SuiteCrm::User.create(description: cg.Description,
                                    first_name: first,
                                    last_name: last)
        puts "Creating new SuiteCrm::User #{sgu.first_name} for #{cg.Name}/#{cg.Description}"
      end

      # now walk through the list of related Customers, and for each find the
      # right crm_account via the Customer->Client (ForeignPID) relationship.
      cg.customers.each { |cust|
        client = cust.client
        crm_account = client.try(:crm_account)
        sgucnt += 1
        unless crm_account
          puts "  ..can not find crm_account, Customer:#{cust.name} #{client.try(:name)}"
          next
        end
        crm_account.crm_user = sgu
        crm_account.save!
        sgusuccess += 1
      }
      puts "Mapped #{cg.Name}->#{sgu.id} with #{sgucnt} objects, #{sgusuccess} linked"
    }
  end

end
