-- Name: calls_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY calls DROP CONSTRAINT calls_client_id_fk;


--
-- Name: calls_datasource_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY calls
    DROP CONSTRAINT calls_datasource_id_fk;


--
-- Name: callstats_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats
    DROP CONSTRAINT callstats_client_id_fk;


--
-- Name: callstats_phone_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats
    DROP CONSTRAINT callstats_phone_id_fk;


--
-- Name: callstats_stats_period_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats
    DROP CONSTRAINT callstats_stats_period_id_fk;


--
-- Name: cdrviewer_cdr_calledbtn_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    DROP CONSTRAINT cdrviewer_cdr_calledbtn_id_fk;


--
-- Name: cdrviewer_cdr_calledphone_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    DROP CONSTRAINT cdrviewer_cdr_calledphone_id_fk;


--
-- Name: cdrviewer_cdr_callingbtn_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    DROP CONSTRAINT cdrviewer_cdr_callingbtn_id_fk;


--
-- Name: cdrviewer_cdr_callingphone_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    DROP CONSTRAINT cdrviewer_cdr_callingphone_id_fk;


--
-- Name: cdrviewer_cdr_datasource_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    DROP CONSTRAINT cdrviewer_cdr_datasource_id_fk;


--
-- Name: clients_billing_site_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY clients
    DROP CONSTRAINT clients_billing_site_id_fk;


--
-- Name: phones_btn_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY phones
    DROP CONSTRAINT phones_btn_id_fk;


--
-- Name: phones_site_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY phones
    DROP CONSTRAINT phones_site_id_fk;


--
-- Name: portal_customerservice_service_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_customerservice
    DROP CONSTRAINT portal_customerservice_service_id_fk;


--
-- Name: sites_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sites
    DROP CONSTRAINT sites_client_id_fk;

