-- beaumont has/wants:
--
--  create_table "clients", force: :cascade do |t|
--     t.string   "name"
--     t.integer  "billing_site_id"
--     t.string   "admin_contact"
--     t.string   "technical_contact"
--     t.datetime "created_at"
--     t.datetime "updated_at"
--     t.string   "description"
--     t.string   "logo"
--     t.string   "url"
--   end
--
-- OTRS has/wants:
--   customer_id,
--           name AS name,
--           ''::text AS street,
--           ''::text AS zip,
--           ''::text AS city,
--           ''::text AS country,
--            ''::text AS url,
--            ''::text AS comments,
--            1 AS create_by,
--            1 AS change_by,
--            1 AS valid_id,
--            created_at AS create_time,
--            updated_at AS change_time FROM public.clients);
--
-- The *ACCOUNTS*, which seem to be companies, part of SuiteCRM has:
--
-- id           char(36)	No
-- name 	varchar(150)	Yes 	NULL
-- date_entered 	datetime	Yes 	NULL
-- date_modified 	datetime	Yes 	NULL
-- modified_user_id 	char(36)	Yes 	NULL
-- created_by 	char(36)	Yes 	NULL
-- description 	text	Yes 	NULL
-- deleted 	tinyint(1)	Yes 	0
-- assigned_user_id 	char(36)	Yes 	NULL
-- account_type 	varchar(50)	Yes 	NULL
-- industry 	varchar(50)	Yes 	NULL
-- annual_revenue 	varchar(100)	Yes 	NULL
-- phone_fax 	varchar(100)	Yes 	NULL
-- billing_address_street 	varchar(150)	Yes 	NULL
-- billing_address_city 	varchar(100)	Yes 	NULL
-- billing_address_state 	varchar(100)	Yes 	NULL
-- billing_address_postalcode 	varchar(20)	Yes 	NULL
-- billing_address_country 	varchar(255)	Yes 	NULL
-- rating 	varchar(100)	Yes 	NULL
-- phone_office 	varchar(100)	Yes 	NULL
-- phone_alternate 	varchar(100)	Yes 	NULL
-- website 	varchar(255)	Yes 	NULL
-- ownership 	varchar(100)	Yes 	NULL
-- employees 	varchar(10)	Yes 	NULL
-- ticker_symbol 	varchar(10)	Yes 	NULL
-- shipping_address_street 	varchar(150)	Yes 	NULL
-- shipping_address_city 	varchar(100)	Yes 	NULL
-- shipping_address_state 	varchar(100)	Yes 	NULL
-- shipping_address_postalcode 	varchar(20)	Yes 	NULL
-- shipping_address_country 	varchar(255)	Yes 	NULL
-- parent_id 	char(36)	Yes 	NULL
-- sic_code 	varchar(10)	Yes 	NULL
-- campaign_id
--
--
-- so import all of this as crm_accounts, and then build a view on top of that for the use of OTRS

--
-- Name: mysql_fdw; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS mysql_fdw WITH SCHEMA public;
COMMENT ON EXTENSION mysql_fdw IS 'Foreign data wrapper for connecting OTRS/Beaumont to SuiteCRM';

DROP SERVER IF EXISTS embassy_server CASCADE;
CREATE SERVER embassy_server FOREIGN DATA WRAPPER mysql_fdw OPTIONS (
    host '127.0.0.1',
    port '3306'
);
ALTER SERVER embassy_server OWNER TO sg1;

--
-- Name: USER MAPPING postgres SERVER mysql_server; Type: USER MAPPING; Schema: -; Owner: postgres
--
-- this must be setup with the real password, which is why this is
-- documentation only.
--
-- rails does not believe that the foreign table exists, so we actually
-- create a different table, and then we create a view of that table,
-- which rails is happy about.
--
--
DROP USER MAPPING FOR public SERVER embassy_server;
CREATE USER MAPPING FOR public SERVER embassy_server OPTIONS (
    password 'ur4aeCh5owee',
    username 'suitecrm'
);

DROP FOREIGN TABLE IF EXISTS suitecrm.crm_accounts;

CREATE SCHEMA IF NOT EXISTS suitecrm;
CREATE FOREIGN TABLE suitecrm.crm_accounts (
       id                       text,
       name                     text,
       date_entered 	        timestamp,
       date_modified 	                timestamp,
       modified_user_id 	        text,
       created_by 	                text,
       description 	                text,
       deleted 	                integer,
       assigned_user_id                text,
       account_type 	                text,
       industry 	                text,
       annual_revenue 	                text,
       phone_fax 	                text,
       billing_address_street 	        text,
       billing_address_city 	        text,
       billing_address_state 	        text,
       billing_address_postalcode 	text,
       billing_address_country 	text,
       rating 	                        text,
       phone_office 	                text,
       phone_alternate 	        text,
       website 	                text,
       ownership 	                text,
       employees 	                text,
       ticker_symbol 	                text,
       shipping_address_street 	text,
       shipping_address_city 	        text,
       shipping_address_state 	        text,
       shipping_address_postalcode 	text,
       shipping_address_country 	text,
       parent_id 	text,
       sic_code 	text,
       campaign_id     text
)
SERVER embassy_server
OPTIONS (
    dbname 'suitecrm',
    table_name 'accounts'
);

-- TRUNCATE does not work on FDW.
DELETE FROM suitecrm.crm_accounts;
INSERT INTO suitecrm.crm_accounts (SELECT * FROM public.crm_accounts);

DROP TABLE IF EXISTS public.crm_accounts_old;
-- ALTER TABLE public.crm_accounts RENAME TO crm_accounts_old;
-- ALTER TABLE public.crm_accounts_old RENAME TO crm_accounts;

-- CREATE OR REPLACE VIEW public.crm_accounts AS (SELECT * FROM suitecrm.crm_accounts);

-- GRANT ALL ON SCHEMA suitecrm TO SG1;
-- GRANT ALL ON        suitecrm.crm_accounts  TO sg1;
-- GRANT ALL ON        public.crm_accounts  TO sg1;

ALTER FOREIGN TABLE suitecrm.crm_accounts OWNER TO sg1;


