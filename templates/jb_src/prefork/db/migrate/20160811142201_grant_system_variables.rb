class GrantSystemVariables < ActiveRecord::Migration
  def up
    SystemVariable.grant_access
  end
end
