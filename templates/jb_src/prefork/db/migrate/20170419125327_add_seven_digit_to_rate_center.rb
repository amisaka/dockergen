class AddSevenDigitToRateCenter < ActiveRecord::Migration
  def change
    add_column :rate_centers, :seven_digit, :boolean
  end
end
