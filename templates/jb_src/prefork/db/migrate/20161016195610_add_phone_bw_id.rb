class AddPhoneBwId < ActiveRecord::Migration
  def change
    change_table :phones do |t|
      t.column :foreign_userId, :text
    end
  end
end
