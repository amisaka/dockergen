class CreateBillableServices < ActiveRecord::Migration
  def change
    create_table 'public.billable_services' do |t|
      t.integer :invoice_id
      t.integer :service_id
      t.integer :qty
      t.date :service_begin
      t.date :service_end
      t.decimal :discount, precision: 5, scale: 2
      t.boolean :renewable
      t.text :comments
      t.decimal :totalprice, precision: 5, scale: 2
      t.timestamps null: false
    end
  end
end
