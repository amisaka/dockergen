class AddIndexOnStartdatetimeOnCalls < ActiveRecord::Migration
  def self.up
      add_index     :calls,    :startdatetime
  end

  def self.down
      remove_index  :calls,    :startdatetime
  end
end
