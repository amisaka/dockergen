class PhoneTypeIsId < ActiveRecord::Migration
  def self.up
    rename_column :phones, :phone_type_it, :phone_type_id
  end

  def self.down
    rename_column :phones, :phone_type_id, :phone_type_it
  end
end
