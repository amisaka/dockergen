class CreatePhones < ActiveRecord::Migration
  def self.up
    create_table :phones do |t|
      t.timestamps
      # CSV header says: Client,BTN,Numero,"Poste tel.","Boite vocale",Access,Notes,"Date d'activation","Fin de service","MAC address","Type device","SIP U/N","SIP P/W","Tested 911","VOIP Supplier",Courriel,"IP address","Type de connexion"
      t.integer :client_id
      t.integer :btn_id
      t.string :phone_number
      t.string :extension
      t.string :voicemail_number
      t.string :access_info
      t.text   :notes
      t.date   :activation_date
      t.date   :termination_date
      t.string :phone_mac_addr
      t.integer :phone_type_it
      t.string  :sip_username
      t.string  :sip_password
      t.string  :e911_notes
      t.boolean :e911_tested
      t.integer :e911_tested_by_id
      t.date    :e911_tested_date
      t.integer :voip_supplier_id
      t.string  :email
      t.string  :ipaddress
      t.string  :connectiontype_id
    end
    Phone.grant_access
  end

  def self.down
    drop_table :phones
  end
end
