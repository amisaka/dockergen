class CustommessageChangeName < ActiveRecord::Migration
  def self.up
    execute <<-SQL

      --------------------------------------------------
      -- clean up portal_userprofilecustomer table (remove rows that refer to non-existant userprofiles)
      --------------------------------------------------
      CREATE OR REPLACE FUNCTION clean_up_userprofilecustomer () RETURNS integer AS $$
      DECLARE
          pupc_record RECORD;
          answer INTEGER := 0;
      BEGIN
          SELECT INTO pupc_record * FROM portal_userprofilecustomer WHERE NOT user_profile_id IN (SELECT id FROM portal_userprofile);
          IF FOUND THEN
              DELETE FROM portal_userprofilecustomer WHERE NOT user_profile_id IN (SELECT id FROM portal_userprofile);
              answer = 1;
          END IF;

          RETURN answer;
      END;
      $$ LANGUAGE plpgsql;

      SELECT clean_up_userprofilecustomer () AS success;

      DROP FUNCTION clean_up_userprofilecustomer ();

      --------------------------------------------------
      -- Change the name of the custom message service
      --------------------------------------------------

      UPDATE portal_service SET (name) = ('Affichage Dynamique') WHERE name like '%Digital Display Message%';
      UPDATE portal_service SET (name) = ('Affichage Dynamique') WHERE name like '%Dynamic%';

      --------------------------------------------------
      -- Add a userprofile for a user if it's missing
      --------------------------------------------------

      CREATE OR REPLACE FUNCTION userprofile_add(varchar) RETURNS integer AS $$
      DECLARE
          auth_user_record RECORD;
          userprofile_record RECORD;
          answer INTEGER := 0;
          uname ALIAS FOR $1;
      BEGIN
          SELECT INTO auth_user_record * from auth_user WHERE username = uname;
          IF FOUND THEN
              -- how do I know there is only one record?
              SELECT INTO userprofile_record * from portal_userprofile WHERE user_id = auth_user_record.id;

              IF NOT FOUND THEN
                  INSERT INTO portal_userprofile (user_id, logo) VALUES (auth_user_record.id, 'user_logos/g151.png');
                  answer = 1;
              END IF;
          END IF;

          RETURN answer;
          
      END;
      $$ LANGUAGE plpgsql;

      SELECT userprofile_add ('cpepin') AS success;
      SELECT userprofile_add ('bjb') AS success;
      SELECT userprofile_add ('chris') AS success;
      SELECT userprofile_add ('crm') AS success;
      SELECT userprofile_add ('jutras') AS success;

      DROP FUNCTION userprofile_add (varchar);

      --------------------------------------------------
      -- add new group
      --------------------------------------------------

      CREATE OR REPLACE FUNCTION group_add (varchar) RETURNS integer AS $$
      DECLARE
        auth_group_record RECORD;
        answer INTEGER := 0;
        gname ALIAS FOR $1;
      BEGIN
          SELECT INTO auth_group_record * FROM auth_group WHERE name = gname;
          IF NOT FOUND THEN
              INSERT INTO auth_group (name) VALUES (gname);
              answer = 1;
          END IF;

          RETURN answer;

      END;

      $$ LANGUAGE plpgsql;

      SELECT group_add ('custommessage') AS success;

      DROP FUNCTION group_add (varchar);

      --------------------------------------------------
      -- create permissions related to new group
      --------------------------------------------------

      CREATE OR REPLACE FUNCTION perm_add (varchar, varchar, varchar) RETURNS integer AS $$
      DECLARE
        perm_record RECORD;
        ct_record RECORD;
        answer INTEGER := 0;
        p_ct_name ALIAS FOR $1;
        p_ct_model ALIAS for $2;
        p_codename ALIAS for $3;
      BEGIN
          SELECT INTO ct_record * FROM django_content_type WHERE model = p_ct_model;
          IF FOUND THEN
              SELECT INTO perm_record * FROM auth_permission WHERE name = p_ct_name;
              IF NOT FOUND THEN
                  INSERT INTO auth_permission (name, content_type_id, codename) VALUES (p_ct_name, ct_record.id, p_codename);
                  answer = 1;
              END IF;
          ELSE
              answer = -1;
          END IF;

          RETURN answer;

      END;

      $$ LANGUAGE plpgsql;

      SELECT perm_add ('Can add custom message', 'custommessage', 'add_custommessage') AS success;
      SELECT perm_add ('Can change custom message', 'custommessage', 'change_custommessage') AS success;
      SELECT perm_add ('Can delete custom message', 'custommessage', 'delete_custommessage') AS success;

      DROP FUNCTION perm_add (varchar, varchar, varchar);

      --------------------------------------------------
      -- add permissions to group
      --------------------------------------------------

      CREATE OR REPLACE FUNCTION group_permissions_add (varchar, varchar) RETURNS integer AS $$
      DECLARE
          auth_group_record RECORD;
          auth_permission_record RECORD;
          auth_group_perm_record RECORD;
          gname ALIAS FOR $1;
          perm_codename ALIAS FOR $2;
          answer INTEGER := 0;
      BEGIN
          SELECT INTO auth_group_record * FROM auth_group WHERE name = gname;
          IF FOUND THEN
              SELECT INTO auth_permission_record * FROM auth_permission WHERE codename = perm_codename;
              IF FOUND THEN
                  SELECT INTO auth_group_perm_record * FROM auth_group_permissions WHERE group_id = auth_group_record.id AND permission_id = auth_permission_record.id;
                  IF NOT FOUND THEN
                      INSERT INTO auth_group_permissions (group_id, permission_id) VALUES (auth_group_record.id, auth_permission_record.id);
                      answer = 1;
                  END IF;
              ELSE
                  answer = -1;
              END IF;
          ELSE
              answer = -2;
          END IF;

          RETURN answer;

      END;
      $$ LANGUAGE plpgsql;

      SELECT group_permissions_add ('custommessage', 'add_custommessage') AS success;
      SELECT group_permissions_add ('custommessage', 'change_custommessage') AS success;
      SELECT group_permissions_add ('custommessage', 'delete_custommessage') AS success;
      SELECT group_permissions_add ('session', 'add_session') AS success;
      SELECT group_permissions_add ('session', 'change_session') AS success;
      SELECT group_permissions_add ('session', 'delete_session') AS success;
      SELECT group_permissions_add ('log entry', 'add_logentry') AS success;
      SELECT group_permissions_add ('log entry', 'change_logentry') AS success;
      SELECT group_permissions_add ('log entry', 'delete_logentry') AS success;

      DROP FUNCTION group_permissions_add (varchar, varchar);

      --------------------------------------------------
      -- add certain users (if they exist) to that group
      --------------------------------------------------
      CREATE OR REPLACE FUNCTION add_user_to_group(varchar) RETURNS integer AS $$
      DECLARE
          auth_user_record RECORD;
          auth_group_record RECORD;
          auth_user_groups_record RECORD;
          answer INTEGER := 0;
          uname ALIAS FOR $1;
      BEGIN
          SELECT INTO auth_user_record * from auth_user WHERE username = uname;
          IF FOUND THEN
              SELECT INTO auth_group_record * from auth_group WHERE name = 'custommessage';

              SELECT INTO auth_user_groups_record * from auth_user_groups WHERE user_id = auth_user_record.id AND group_id = auth_group_record.id;

              IF NOT FOUND THEN
                  INSERT INTO auth_user_groups (user_id, group_id) values (auth_user_record.id, auth_group_record.id);
                  answer = 1;
              END IF;
          END IF;

          RETURN answer;
      END;
      $$ LANGUAGE plpgsql;
      
      SELECT add_user_to_group ('cpepin') AS success;
      SELECT add_user_to_group ('bjb') AS success;
      SELECT add_user_to_group ('chris') AS success;
      SELECT add_user_to_group ('crm') AS success;
      SELECT add_user_to_group ('jutras') AS success;

      DROP FUNCTION add_user_to_group (varchar);

    SQL
  end

  def self.down
    execute <<-SQL
      UPDATE portal_service SET (name) = ('Digital Display Message') WHERE name = 'Affichage Dynamique';
    SQL
  end
end
