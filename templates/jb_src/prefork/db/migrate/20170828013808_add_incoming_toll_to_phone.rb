class AddIncomingTollToPhone < ActiveRecord::Migration
  def change
    add_column :phones, :incoming_toll, :boolean, :default => false
  end
end
