class CreateSites < ActiveRecord::Migration
  def self.up
    create_table :sites do |t|
      t.string :name
      t.string :unitnumber
      t.integer :building_id

      t.timestamps
    end
    Site.grant_access
  end

  def self.down
    drop_table :sites
  end
end
