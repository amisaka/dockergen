class AddLateChargesToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :late_charges, :decimal, precision: 8, scale: 2
  end
end
