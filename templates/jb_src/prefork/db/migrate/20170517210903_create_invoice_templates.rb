class CreateInvoiceTemplates < ActiveRecord::Migration
  def up
    create_table :invoice_templates do |t|
      t.text :name
      t.text :invoice_resources_path
      t.text :invoice_cycle

      t.timestamps null: false
    end
    InvoiceTemplate.grant_access
  end
  def down
    drop_table :invoice_templates
  end
end
