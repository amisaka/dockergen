class UpdateSeqPermissions < ActiveRecord::Migration
  def self.up
    execute "GRANT SELECT,UPDATE,USAGE ON local_exchanges_id_seq TO beaumont;";
    execute "GRANT SELECT,UPDATE,USAGE ON exchanges_manual_matches_id_seq TO beaumont;";
    execute "GRANT SELECT,UPDATE,USAGE ON lerg8s_id_seq TO beaumont;";
    execute "GRANT SELECT,UPDATE,USAGE ON local_exchanges_north_american_exchanges_id_seq TO beaumont;";
    execute "GRANT SELECT,UPDATE,USAGE ON north_american_exchanges_id_seq TO beaumont;";
  end

end
