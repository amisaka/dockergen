class AddCrmUser < ActiveRecord::Migration
  def down
    execute "DROP TABLE IF EXISTS suitecrm.users;"
  end

  def up
    # in production, this is a connection to a mysql suitecrm database.
    unless Rails.env.production?
      execute "CREATE SCHEMA IF NOT EXISTS suitecrm;";
      execute "grant all on schema suitecrm to sg1;"
      create_table "suitecrm.users", { id: false } do |t|
        t.column :id,                :text, :primary_key => true
        t.string   "user_name"
        t.string   "user_hash"
        t.boolean  "system_generated_password"
        t.datetime "pwd_last_changed"
        t.string   "authenticate_id"
        t.boolean  "sugar_login"
        t.string   "first_name"
        t.string   "last_name"
        t.boolean  "is_admin"
        t.boolean  "external_auth_only"
        t.boolean  "receive_notifications"
        t.text     "description"
        t.datetime "date_entered"
        t.datetime "date_modified"
        t.string   "modified_user_id"
        t.string   "created_by"
        t.string   "title"
        t.string   "photo"
        t.string   "department"
        t.string   "phone_home"
        t.string   "phone_mobile"
        t.string   "phone_work"
        t.string   "phone_other"
        t.string   "phone_fax"
        t.string   "status"
        t.string   "address_street"
        t.string   "address_city"
        t.string   "address_state"
        t.string   "address_country"
        t.string   "address_postalcode"
        t.boolean  "deleted"
        t.boolean  "portal_only"
        t.boolean  "show_on_employees"
        t.string   "employee_status"
        t.string   "messenger_id"
        t.string   "messenger_type"
        t.string   "reports_to_id"
        t.boolean  "is_group"
      end

      SuiteCrm::User.grant_access
    end
  end
end
