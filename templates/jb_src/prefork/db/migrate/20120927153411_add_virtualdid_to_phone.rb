class AddVirtualdidToPhone < ActiveRecord::Migration
  def self.up
    add_column :phones, :virtualdid, :boolean, :default => false
  end

  def self.down
    remove_column :phones, :virtualdid
  end
end
