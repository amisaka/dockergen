class AddServicesSubtotalChargesSubtotalToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :services_subtotal, :decimal, precision: 8, scale: 2
    add_column :invoices, :charges_subtotal,  :decimal, precision: 8, scale: 2
  end
end
