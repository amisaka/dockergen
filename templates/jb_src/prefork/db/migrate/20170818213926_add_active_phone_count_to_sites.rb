class AddActivePhoneCountToSites < ActiveRecord::Migration
  def up
    add_column :sites, :active_phones_count, :integer, :null => false, :default => 0

    Site.reset_column_information
    Site.all.find_each do |p|
      Site.update_counters p.id, :active_phones_count => p.active_phones.length
    end
  end
  def down
    remove_column :sites, :active_phones_count
  end
end
