class AddPerfStatsToDatasource < ActiveRecord::Migration
  def self.up
    add_column "cdrviewer_datasource", :cdr_count, :integer
    add_column "cdrviewer_datasource", :call_count, :integer
    add_column "cdrviewer_datasource", :load_time, :integer
  end

  def self.down
    remove_column "cdrviewer_datasource", :cdr_count
    remove_column "cdrviewer_datasource", :call_count
    remove_column "cdrViewer_datasource", :load_time
  end
end
