class DatasourceFilenameShouldBeText < ActiveRecord::Migration
  def up
    change_column :cdrviewer_datasource, :datasource_filename, :text, null: false
  end
end
