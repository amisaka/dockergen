class AddHakkaToClient < ActiveRecord::Migration
  def change
    change_table(:clients) do |t|
      t.column :foreign_pid, :integer
      t.column :billing_btn, :text
      t.column :billing_cycle_id, :integer
      t.column :started_at,       :datetime
      t.column :terminated_at,    :datetime
      t.column :language_preference, :text
    end
  end
end
