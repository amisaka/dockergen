class AddServiceNameToBillableServices < ActiveRecord::Migration
  def change
    add_column :billable_services, :service_name, :text
    add_column :billable_services, :asset_id, :integer
  end
end
