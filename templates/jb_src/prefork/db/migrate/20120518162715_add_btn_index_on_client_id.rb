class AddBtnIndexOnClientId < ActiveRecord::Migration
  def self.up
    add_index :btns,    :client_id
  end

  def self.down
    remove_index :btns, :client_id
  end
end
