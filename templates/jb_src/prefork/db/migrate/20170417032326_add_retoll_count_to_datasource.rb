class AddRetollCountToDatasource < ActiveRecord::Migration
  def change
    add_column :cdrviewer_datasource, :toll_recalculate_count, :integer, :default => 0
  end
end
