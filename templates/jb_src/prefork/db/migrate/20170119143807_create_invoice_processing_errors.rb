class CreateInvoiceProcessingErrors < ActiveRecord::Migration
  def change
    create_table :invoice_processing_errors do |t|
      t.integer :invoice_run_id
      t.integer :cdr_id
      t.text :reason
      t.text :missing_thing
      t.datetime :fixed_at

      t.timestamps null: false
    end
    InvoiceProcessingError.grant_access
  end
end
