# coding: utf-8
class UpdateRateCentersForMatches < ActiveRecord::Migration
  def up
    if Rails.env[0..9] == 'production'
      [
        [155, ['FUGEREVILLE','FUGÈREVILLE']],
        [192, ['TRING-JONCTION']],
        [255, ['LAC-MISTASSINI','MISTISSINI']],
        [385, ['BEARN', 'BÉARN']],
        [387, ['LACERLOCHERE','LAVERLOCHÈRE']],
        [390, ['SAINT-BRUNO-DE-GUIGUES','ST-BRUNO-DE-GUIGUES']],
        [580, ['LEBEL-SUR-QUÉVILLON']],
        [582, ['RIVIÈRE-HÉVA']],
        [596, ['ST-EUGÈNE-DE-GUIGUES']],
        [2008, ['SAINTE-FELICITE']],
        [2096, ["ST-FERDINAND-D'HALIFAX"]],
        [2123, ["VAL D'OR", "VAL-D'OR"]],
        [19058,["KUUJJUAQ"]],
        [19076,["LYSTER"]],
        [19088,["EASTMAIN 1"]],
      ].each {|entry|
        id = entry[0]
        lnames = entry[1]
        RateCenter.find(id).update(localnames: lnames)
      }

      RateCenter.find(601).update(deleted_at: '2016-09-01'.to_date)
    end
  end
end
