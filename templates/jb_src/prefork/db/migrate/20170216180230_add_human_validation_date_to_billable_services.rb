class AddHumanValidationDateToBillableServices < ActiveRecord::Migration
  def change
    add_column :billable_services, :human_validation_date, :datetime
  end
end
