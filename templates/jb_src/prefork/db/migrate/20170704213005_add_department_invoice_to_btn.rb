class AddDepartmentInvoiceToBtn < ActiveRecord::Migration
  def up
    add_column :btns, :department_invoice, :boolean
    Btn.reset_column_information
    Btn.find_each { |b1|
      dept_invoice = false
      next unless b1.try(:client).try(:sites)

      if b1.client.sites.count > 1
        bs = b1.billable_services.where.not(site_id: nil)
        sites = Hash.new
        bs.each { |bs1|
          sites[bs1.site_id] = bs1.site
        }
        if sites.keys.count > 1
          dept_invoice = true
        end
      end
      b1.department_invoice = dept_invoice
    }
  end
  def down
    remove_column :btns, :department_invoice
  end
end
