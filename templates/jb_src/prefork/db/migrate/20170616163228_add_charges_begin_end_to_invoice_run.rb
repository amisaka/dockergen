class AddChargesBeginEndToInvoiceRun < ActiveRecord::Migration
  def change
    add_column :invoice_runs, :charges_start,  :date
    add_column :invoice_runs, :charges_finish, :date
  end
end
