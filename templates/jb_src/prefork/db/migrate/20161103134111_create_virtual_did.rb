class CreateVirtualDID < ActiveRecord::Migration
  def change
    vdid = RateCenter.where(country_code: 0, lerg6name: 'virtual').take
    unless vdid
      vdid = RateCenter.create(country_code: 0, lerg6name: 'virtual')
    end
    vdid.virtual_did = true
    vdid.save!

    (201..799).each { |areacode|
      nae = NorthAmericanExchange.find_or_create_by(npa: areacode, nxx: 555)
      nae.name = 'Virtual'
      nae.province = 'DID'
      nae.mapped_at   = Time.now
      nae.rate_center = vdid
      nae.save!
    }
  end
end
