class ExtendSizeOfDatasource < ActiveRecord::Migration
  def change
    change_column :cdrviewer_datasource, :datasource_filename, :string, :limit => 256, :null => false
  end
end
