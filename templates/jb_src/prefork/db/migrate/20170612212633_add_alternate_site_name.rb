class AddAlternateSiteName < ActiveRecord::Migration
  def change
    add_column :sites, :alt_name, :text
  end
end
