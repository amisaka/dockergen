class AddExchangesManualMatches < ActiveRecord::Migration
  def self.up
    create_table "exchanges_manual_matches" do |t|
      t.string "province",  limit: 5
      t.string "localname", limit: 40
      t.string "lerg6name", limit: 10
    end
    execute "GRANT SELECT,UPDATE,INSERT,DELETE ON exchanges_manual_matches TO beaumont;";
  end

  def self.down
    drop_table "exchanges_manual_matches"
  end
end
