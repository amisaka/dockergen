class AddDestinationCityCode < ActiveRecord::Migration
  def change
    change_table :rate_centers do |t|
      t.column :city_code, :text
    end
  end
end
