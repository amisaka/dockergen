class LegacyDjangoTables < ActiveRecord::Migration
  def self.up
    # this is never for production
    # return true if ::Rails.env.include?("production")

    create_table "auth_group", :force => true do |t|
      t.string "name", :limit => 80, :null => false
    end

    add_index "auth_group", ["name"], :name => "auth_group_name_key", :unique => true

    create_table "auth_group_permissions", :force => true do |t|
      t.integer "group_id",      :null => false
      t.integer "permission_id", :null => false
    end

    add_index "auth_group_permissions", ["group_id", "permission_id"], :name => "auth_group_permissions_group_id_permission_id_key", :unique => true

    create_table "auth_message", :force => true do |t|
      t.integer "user_id", :null => false
      t.text    "message", :null => false
    end

    add_index "auth_message", ["user_id"], :name => "auth_message_user_id"

    create_table "auth_permission", :force => true do |t|
      t.string  "name",            :limit => 50,  :null => false
      t.integer "content_type_id",                :null => false
      t.string  "codename",        :limit => 100, :null => false
    end

    add_index "auth_permission", ["content_type_id", "codename"], :name => "auth_permission_content_type_id_codename_key", :unique => true
    add_index "auth_permission", ["content_type_id"], :name => "auth_permission_content_type_id"

    create_table "auth_user", :force => true do |t|
      t.string   "username",     :limit => 30,  :null => false
      t.string   "first_name",   :limit => 30,  :null => false
      t.string   "last_name",    :limit => 30,  :null => false
      t.string   "email",        :limit => 75,  :null => false
      t.string   "password",     :limit => 128, :null => false
      t.boolean  "is_staff",                    :null => false
      t.boolean  "is_active",                   :null => false
      t.boolean  "is_superuser",                :null => false
      t.datetime "last_login",                  :null => false
      t.datetime "date_joined",                 :null => false
    end

    add_index "auth_user", ["username"], :name => "auth_user_username_key", :unique => true

    create_table "auth_user_groups", :force => true do |t|
      t.integer "user_id",  :null => false
      t.integer "group_id", :null => false
    end

    add_index "auth_user_groups", ["user_id", "group_id"], :name => "auth_user_groups_user_id_group_id_key", :unique => true

    create_table "auth_user_user_permissions", :force => true do |t|
      t.integer "user_id",       :null => false
      t.integer "permission_id", :null => false
    end

    add_index "auth_user_user_permissions", ["user_id", "permission_id"], :name => "auth_user_user_permissions_user_id_permission_id_key", :unique => true

    create_table "btns", :force => true do |t|
      t.string   "billingtelephonenumber"
      t.integer  "client_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.date     "start_date",             :default => '1970-01-01'
      t.date     "end_date"
    end

    create_table "cdrviewer_areacode", :force => true do |t|
      t.string "npa",         :limit => 3,  :null => false
      t.string "country",     :limit => 16, :null => false
      t.string "description", :limit => 64, :null => false
    end

    create_table "cdrviewer_cdr", :force => true do |t|
      t.string   "calltype",                             :limit => 13
      t.string   "direction",                            :limit => 11
      t.string   "callednumber",                         :limit => 161
      t.decimal  "starttime",                                            :precision => 19, :scale => 3
      t.datetime "startdatetime"
      t.time     "newstarttime"
      t.string   "recordid",                             :limit => 48
      t.string   "serviceprovider",                      :limit => 30
      t.string   "usernumber",                           :limit => 16
      t.string   "groupnumber",                          :limit => 16
      t.string   "callingnumber",                        :limit => 161
      t.string   "callingpresentationindicator",         :limit => 20
      t.string   "usertimezone",                         :limit => 11
      t.string   "answerindicator",                      :limit => 19
      t.string   "answertime",                           :limit => 18
      t.string   "terminationcause",                     :limit => 3
      t.string   "carrieridentificationcode",            :limit => 4
      t.string   "dialeddigits",                         :limit => 161
      t.string   "callcategory",                         :limit => 8
      t.string   "networkcalltype",                      :limit => 4
      t.string   "networktranslatednumber",              :limit => 161
      t.string   "networktranslatedgroup",               :limit => 32
      t.string   "releasingparty",                       :limit => 6
      t.string   "route",                                :limit => 86
      t.string   "networkcallid",                        :limit => 161
      t.string   "codec",                                :limit => 30
      t.string   "accessdeviceaddress",                  :limit => 80
      t.string   "accesscallid",                         :limit => 161
      t.string   "spare1",                               :limit => 128
      t.string   "failovercorrelationid",                :limit => 161
      t.string   "spare2",                               :limit => 128
      t.string   "group",                                :limit => 30
      t.string   "department"
      t.string   "accountcode",                          :limit => 14
      t.string   "authorizationcode",                    :limit => 14
      t.string   "originalcallednumber",                 :limit => 161
      t.string   "originalcalledpresentationindicator",  :limit => 20
      t.string   "originalcalledreason",                 :limit => 40
      t.string   "redirectingnumber",                    :limit => 161
      t.string   "redirectingreason",                    :limit => 40
      t.string   "chargeindicator",                      :limit => 1
      t.string   "typeofnetwork",                        :limit => 7
      t.string   "voiceportalcalling_invtime",           :limit => 18
      t.string   "localcallid",                          :limit => 40
      t.string   "remotecallid",                         :limit => 40
      t.string   "callingpartycategory",                 :limit => 20
      t.decimal  "releasetime",                                          :precision => 19, :scale => 3
      t.datetime "releasedatetime"
      t.time     "newreleasetime"
      t.string   "networktype",                          :limit => 4
      t.string   "redirectingpresentationindicator",     :limit => 20
      t.string   "instantconference_to",                 :limit => 161
      t.string   "instantconference_from",               :limit => 161
      t.string   "instantconference_conferenceid",       :limit => 128
      t.string   "instantconference_role",               :limit => 12
      t.string   "instantconference_bridge",             :limit => 128
      t.string   "instantconference_owner",              :limit => 161
      t.string   "instantconference_ownerdn",            :limit => 32
      t.string   "instantconference_title",              :limit => 80
      t.string   "instantconference_projectcode",        :limit => 40
      t.string   "key",                                  :limit => 161
      t.string   "creator",                              :limit => 80
      t.string   "originatornetwork",                    :limit => 80
      t.string   "terminatornetwork",                    :limit => 80
      t.string   "userid",                               :limit => 161
      t.string   "otherpartyname",                       :limit => 80
      t.string   "otherpartynamepresentationindicator",  :limit => 20
      t.string   "hoteling_group",                       :limit => 30
      t.string   "hoteling_userid",                      :limit => 161
      t.string   "hoteling_usernumber",                  :limit => 15
      t.string   "hoteling_groupnumber",                 :limit => 15
      t.string   "trunkgroupname"
      t.string   "clidpermitted",                        :limit => 3
      t.string   "accessnetworkinfo",                    :limit => 1024
      t.string   "relatedcallid",                        :limit => 40
      t.string   "relatedcallidreason",                  :limit => 40
      t.string   "transfer_relatedcallid",               :limit => 161
      t.string   "transfer_type",                        :limit => 20
      t.string   "conference_starttime",                 :limit => 18
      t.string   "conference_stoptime",                  :limit => 18
      t.string   "conference_confid",                    :limit => 40
      t.string   "conference_type",                      :limit => 10
      t.string   "faxmessaging",                         :limit => 9
      t.string   "trunkgroupinfo"
      t.string   "recalltype",                           :limit => 20
      t.string   "q850cause",                            :limit => 3
      t.string   "dialeddigitscontext",                  :limit => 161
      t.string   "callednumbercontext",                  :limit => 161
      t.string   "networktranslatednumbercontext",       :limit => 161
      t.string   "callingnumbercontext",                 :limit => 161
      t.string   "originalcallednumbercontext",          :limit => 161
      t.string   "redirectingnumbercontext",             :limit => 161
      t.string   "routingnumber"
      t.string   "originationmethod",                    :limit => 30
      t.string   "broadworksanywhere_relatedcallid",     :limit => 40
      t.string   "calledassertedidentity",               :limit => 161
      t.string   "calledassertedpresentationindicator",  :limit => 20
      t.string   "sdp",                                  :limit => 1024
      t.string   "ascalltype",                           :limit => 11
      t.string   "cbfauthorizationcode",                 :limit => 14
      t.string   "callbridge_callbridgeresult",          :limit => 7
      t.string   "prepaidstatus",                        :limit => 22
      t.string   "configurableclid",                     :limit => 20
      t.string   "instantconference_invtime",            :limit => 18
      t.string   "instantconference_callid",             :limit => 161
      t.string   "instantconference_recording_duration", :limit => 10
      t.datetime "answerdatetime"
      t.time     "newanswertime"
      t.integer  "datasource_id"
      t.integer  "lineno"
      t.string   "usagetype",                            :limit => 3
      t.string   "productid",                            :limit => 161
      t.boolean  "originateportedflag"
      t.string   "originatenumber",                      :limit => 161
      t.boolean  "destinationportedflag"
      t.string   "destinationnumber",                    :limit => 161
      t.string   "forwardedcallingnumber",               :limit => 161
      t.string   "originatelrn",                         :limit => 161
      t.string   "destinationlrn",                       :limit => 161
      t.integer  "duration"
      t.integer  "callingphone_id"
      t.integer  "calledphone_id"
      t.integer  "callingbtn_id"
      t.integer  "calledbtn_id"
      t.integer  "call_id"
    end

    add_index "cdrviewer_cdr", ["call_id"], :name => "index_cdrviewer_cdr_on_call_id"
    add_index "cdrviewer_cdr", ["callednumber"], :name => "index_cdrviewer_cdr_on_callednumber"
    add_index "cdrviewer_cdr", ["calledphone_id"], :name => "cdrviewer_cdr_calledphone_id"
    add_index "cdrviewer_cdr", ["callingnumber"], :name => "index_cdrviewer_cdr_on_callingnumber"
    add_index "cdrviewer_cdr", ["callingphone_id"], :name => "cdrviewer_cdr_callingphone_id"
    add_index "cdrviewer_cdr", ["datasource_id"], :name => "index_cdrviewer_cdr_on_datasource_id"
    add_index "cdrviewer_cdr", ["localcallid"], :name => "index_cdrviewer_cdr_on_localcallid"
    add_index "cdrviewer_cdr", ["remotecallid"], :name => "index_cdrviewer_cdr_remotecallid"
    add_index "cdrviewer_cdr", ["newanswertime"], :name => "cdrviewer_cdr_newanswertime"
    add_index "cdrviewer_cdr", ["newreleasetime"], :name => "cdrviewer_cdr_newreleasetime"
    add_index "cdrviewer_cdr", ["newstarttime"], :name => "cdrviewer_cdr_newstarttime"
    add_index "cdrviewer_cdr", ["startdatetime"], :name => "cdrviewer_cdr_startdatetime"
    add_index "cdrviewer_cdr", ["transfer_relatedcallid"], :name => "index_cdrviewer_cdr_on_transfer_relatedcallid"

    create_table "cdrviewer_dailypeak", :force => true do |t|
      t.date    "date",           :default => '1970-01-01'
      t.integer "midnight_calls", :default => 0,            :null => false
      t.integer "peak_calls",     :default => 0,            :null => false
    end

    create_table "cdrviewer_datasource", :force => true do |t|
      t.string "datasource_filename", :limit => 64, :null => false
    end

    add_index "cdrviewer_datasource", ["datasource_filename"], :name => "index_cdrviewer_datasource_on_datasource_filename"

    create_table "portal_customerservice", :force => true do |t|
      t.integer  "service_id",  :null => false
      t.integer  "customer_id", :null => false
      t.datetime "startdate",   :null => false
      t.datetime "enddate"
    end

    add_index "portal_customerservice", ["customer_id"], :name => "portal_customerservice_customer_id"
    add_index "portal_customerservice", ["service_id"], :name => "portal_customerservice_service_id"

    create_table "portal_service", :force => true do |t|
      t.string "name",        :limit => 32,                      :null => false
      t.text   "description",                                    :null => false
      t.string "url",         :limit => 32, :default => "/foo/", :null => false
      t.string "icon",        :limit => 64, :default => "",      :null => false
    end

    create_table "portal_userprofile", :force => true do |t|
      t.integer "user_id",                :null => false
      t.string  "logo",    :limit => 256
    end

    add_index "portal_userprofile", ["user_id"], :name => "portal_userprofile_user_id_key", :unique => true

    create_table "portal_userprofilecustomer", :force => true do |t|
      t.integer  "user_profile_id", :null => false
      t.integer  "customer_id",     :null => false
      t.datetime "startdate",       :null => false
      t.datetime "enddate"
    end

    add_index "portal_userprofilecustomer", ["customer_id"], :name => "portal_userprofilecustomer_customer_id"
    add_index "portal_userprofilecustomer", ["user_profile_id"], :name => "portal_userprofilecustomer_user_profile_id"

    create_table "privateradio_channel", :force => true do |t|
      t.string "name", :limit => 32, :null => false
    end

    create_table "privateradio_channelschedule", :force => true do |t|
      t.integer "channel_id",                      :null => false
      t.integer "customer_id",                     :null => false
      t.boolean "daysun",       :default => false, :null => false
      t.boolean "daymon",       :default => false, :null => false
      t.boolean "daytue",       :default => false, :null => false
      t.boolean "daywed",       :default => false, :null => false
      t.boolean "daythu",       :default => false, :null => false
      t.boolean "dayfri",       :default => false, :null => false
      t.boolean "daysat",       :default => false, :null => false
      t.time    "start_time",                      :null => false
      t.time    "end_time",                        :null => false
      t.date    "start_date",                      :null => false
      t.date    "end_date"
      t.boolean "override",                        :null => false
      t.integer "pctg_english",                    :null => false
      t.integer "pctg_channel",                    :null => false
    end

    add_index "privateradio_channelschedule", ["channel_id"], :name => "privateradio_channelschedule_channel_id"
    add_index "privateradio_channelschedule", ["customer_id"], :name => "privateradio_channelschedule_customer_id"

    create_table "privateradio_messageorder", :force => true do |t|
      t.integer "user_id",                                             :null => false
      t.integer "customer_id",                                         :null => false
      t.text    "title",                                               :null => false
      t.text    "text"
      t.string  "voice",              :limit => 64
      t.string  "language",           :limit => 32
      t.text    "description"
      t.boolean "need_text_creation",               :default => false, :null => false
      t.boolean "need_text_editing",                :default => false, :null => false
      t.boolean "need_production",                  :default => false, :null => false
      t.date    "date_created",                                        :null => false
      t.date    "date_updated",                                        :null => false
      t.date    "bo_date_approved"
      t.date    "ho_date_approved"
    end

    add_index "privateradio_messageorder", ["customer_id"], :name => "privateradio_messageorder_customer_id"
    add_index "privateradio_messageorder", ["user_id"], :name => "privateradio_messageorder_user_id"

    create_table "privateradio_workorder", :force => true do |t|
      t.integer "messageorder_id",                 :null => false
      t.date    "date_needed"
      t.date    "date_created",                    :null => false
      t.date    "date_approved"
      t.date    "date_scheduled"
      t.date    "date_produced"
      t.date    "date_accepted"
      t.date    "date_updated"
      t.string  "description",      :limit => 128
      t.text    "novavision_notes"
      t.text    "accept_notes"
    end

    add_index "privateradio_workorder", ["messageorder_id"], :name => "privateradio_workorder_messageorder_id"

    # django things
    create_table "django_admin_log", :force => true do |t|
      t.datetime "action_time",                    :null => false
      t.integer  "user_id",                        :null => false
      t.integer  "content_type_id"
      t.text     "object_id"
      t.string   "object_repr",     :limit => 200, :null => false
      t.integer  "action_flag",     :limit => 2,   :null => false
      t.text     "change_message",                 :null => false
    end

    add_index "django_admin_log", ["content_type_id"], :name => "django_admin_log_content_type_id"
    add_index "django_admin_log", ["user_id"], :name => "django_admin_log_user_id"

    create_table "django_content_type", :force => true do |t|
      t.string "name",      :limit => 100, :null => false
      t.string "app_label", :limit => 100, :null => false
      t.string "model",     :limit => 100, :null => false
    end

    add_index "django_content_type", ["app_label", "model"], :name => "django_content_type_app_label_model_key", :unique => true

    create_table "django_session", :force => true do |t|
      t.string   "session_key",  :limit => 40, :null => false
      t.text     "session_data",               :null => false
      t.datetime "expire_date",                :null => false
    end

    create_table "django_site", :force => true do |t|
      t.string "domain", :limit => 100, :null => false
      t.string "name",   :limit => 50,  :null => false
    end
  end

  def self.down
  end
end
