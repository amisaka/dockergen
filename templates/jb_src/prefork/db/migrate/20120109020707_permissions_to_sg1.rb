class PermissionsToSg1 < ActiveRecord::Migration
  def self.up
    ['clients','phones','btns', 'cdrviewer_cdr',
      'sites', 'buildings'].each { |db|
      execute "grant select,insert,update,delete on #{db} to beaumont;"
      execute "grant usage,select,update on #{db}_id_seq to beaumont;"
    }
  end

  def self.down
  end
end
