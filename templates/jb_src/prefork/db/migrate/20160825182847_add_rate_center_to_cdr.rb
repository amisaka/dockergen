class AddRateCenterToCdr < ActiveRecord::Migration
  def change
    change_table :cdrviewer_cdr do |t|
      t.column :called_rate_center_id, :integer
    end
  end
end
