class CreateCallstats < ActiveRecord::Migration
  def self.up
    create_table :callstats do |t|
      t.integer :client_id
      t.date :day
      t.integer :call_count
      t.integer :cdr_count
      t.time :starttime
      t.time :endtime
      t.string :type

      t.timestamps  # later removed
    end
    Callstat.grant_access
  end

  def self.down
    drop_table :callstats
  end
end
