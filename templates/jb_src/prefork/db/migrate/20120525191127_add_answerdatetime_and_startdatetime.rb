class AddAnswerdatetimeAndStartdatetime < ActiveRecord::Migration
  def self.up
    change_table :calls do |b|
      b.column :startdatetime,   :datetime, :default => '1970-01-01'
      b.column :answerdatetime,  :datetime, :default => '1970-01-01'
      b.column :releasedatetime, :datetime
    end

    Call.all.each { |c|
      first = c.cdrs.oldest_first.first
      last  = c.cdrs.newest_first.first

      c[:answerdatetime]  = first.try(:answerdatetime)
      c[:terminated]      = first.try(:terminating?)
      c[:releasedatetime] = last.try(:releasedatetime)
      c[:startdatetime]   = first.try(:startdatetime)
      
      c[:our_number]      = first.try(:callinglabel)
      c[:their_number]    = last.try(:calledlabel)
      c.save!
    }
  end

  def self.down
    change_table :calls do |b|
      b.remove :startdatetime
      b.remove :answerdatetime
      b.remove :releasedatetime
    end
  end
end
