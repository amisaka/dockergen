class DropTimestampsFromCallstats < ActiveRecord::Migration
  def self.up
    remove_column :callstats, "created_at"
    remove_column :callstats, "updated_at"
  end

  def self.down
  end
end
