require 'yaml'

class CreateAddShortNamesTables < ActiveRecord::Migration
  def change
    create_table :short_names do |t|
      t.string :from
      t.string :to

      t.timestamps null: false
    end

    YAML.load(File.open('spec/fixtures/short_names.yml')).each {|key,thing|
      ShortName.create(thing)
    }
  end
end
