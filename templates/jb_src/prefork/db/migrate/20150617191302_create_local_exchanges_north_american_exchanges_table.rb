class CreateLocalExchangesNorthAmericanExchangesTable < ActiveRecord::Migration
    def self.up
        create_table :local_exchanges_north_american_exchanges do |t|
            t.references :local_exchanges
            t.references :north_american_exchanges
        end
        add_index "local_exchanges_north_american_exchanges", ["local_exchanges_id", "north_american_exchanges_id"], :name => "index_l_na_exchanges", :unique => true

        LocalExchangesNorthAmericanExchange.grant_access
    end

  def self.down
        drop_table :local_exchanges_north_american_exchanges
  end
end
