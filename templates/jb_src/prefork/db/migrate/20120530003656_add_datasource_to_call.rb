class AddDatasourceToCall < ActiveRecord::Migration
  def self.up
    change_table :calls do |b|
      b.column :datasource_id, :integer
    end
  end

  def self.down
    change_table :calls do |b|
      b.remote :datasource_id
    end
  end
end
