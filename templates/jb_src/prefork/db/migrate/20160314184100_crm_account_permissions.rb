class CrmAccountPermissions < ActiveRecord::Migration
  def self.up
    ['crm_accounts' ].each { |db|
      execute "grant select,insert,update,delete on #{db} to beaumont;"
    }
  end

  def self.down
  end
end
