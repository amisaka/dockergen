class CreateLerg8s < ActiveRecord::Migration
  def self.up
    create_table :lerg8s do |t|
      t.string :shortName
      t.string :fullName
      t.string :province

      t.timestamps
    end
    Lerg8.grant_access
  end

  def self.down
    drop_table :lerg8s
  end
end
