class FixLenaePrimaryKey < ActiveRecord::Migration
  def change
    execute "ALTER TABLE local_exchanges_north_american_exchanges ALTER COLUMN id SET DEFAULT nextval('local_exchanges_north_american_exchanges_id_seq'::regclass);"
    execute "ALTER TABLE local_exchanges_north_american_exchanges ALTER COLUMN id SET NOT NULL;"
    execute "ALTER TABLE local_exchanges_north_american_exchanges DROP CONSTRAINT IF EXISTS local_exchanges_north_american_exchanges_pkey;"
    add_index :local_exchanges_north_american_exchanges, :id, :name => 'local_exchanges_north_american_exchanges_pkey', :unique => true
    execute "ALTER TABLE local_exchanges_north_american_exchanges ADD PRIMARY KEY USING INDEX local_exchanges_north_american_exchanges_pkey;"
    execute "SELECT setval('local_exchanges_north_american_exchanges_id_seq', (SELECT MAX(id) FROM local_exchanges_north_american_exchanges));"
  end
end
