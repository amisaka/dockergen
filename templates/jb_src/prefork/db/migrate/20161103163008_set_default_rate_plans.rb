class SetDefaultRatePlans < ActiveRecord::Migration
  def up
    localplan = RatePlan.where(province: 'QC', country: 'Canada', name: 'ProvincePlan').take
    nationalplan = RatePlan.where(name: 'VoIP-NorthAmerican-Bronze').take
    worldplan = RatePlan.where(name: 'VoIP-Country-Bronze').take

    Btn.active.find_each { |btn|
      btn.local_rate_plan    ||= localplan
      btn.national_rate_plan ||= nationalplan
      btn.world_rate_plan    ||= worldplan
      btn.save!
    }
  end
end
