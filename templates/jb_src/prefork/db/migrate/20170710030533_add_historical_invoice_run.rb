class AddHistoricalInvoiceRun < ActiveRecord::Migration
  def up
    InvoiceRun.where(invoice_year: 1900, invoice_month: 12).first_or_create
  end
end
