class AddCarrierToLocalExchange < ActiveRecord::Migration
  def self.up
    add_column :local_exchanges, :carrier, :string
  end

  def self.down
    remove_column :local_exchanges, :carrier
  end
end
