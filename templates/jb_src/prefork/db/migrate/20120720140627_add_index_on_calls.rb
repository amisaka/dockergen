class AddIndexOnCalls < ActiveRecord::Migration
  def self.up
    add_index :calls,    :master_callid
  end

  def self.down
    remove_index :calls, :master_callid
  end
end
