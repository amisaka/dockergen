class AddIndexOnClientToCalls < ActiveRecord::Migration
  def up
    unless index_exists?(:calls, :client_id)
      add_index :calls, :client_id
    end
  end
  def down
    drop_index :calls, :client_id
  end
end
