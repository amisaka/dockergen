class CreateCrmAccounts < ActiveRecord::Migration
  def change
    # this table gets replaced in the second half with a foreign table
    # view into a SuiteCRM MySQL table.
    create_table :crm_accounts, { id: false } do |t|
      t.column :id,                :text, :primary_key => true
      t.column :name,              :text
      t.column :date_entered, 	   :datetime
       t.column :date_modified,    :datetime
       t.column :modified_user_id, :text
       t.column :created_by,       :text
       t.column :description,    :text
       t.column :deleted,     	   :integer, default: 0
       t.column :assigned_user_id,    :text
       t.column :account_type,    :text
       t.column :industry,    :text
       t.column :annual_revenue,    :text
       t.column :phone_fax,    :text
       t.column :billing_address_street,    :text
       t.column :billing_address_city,    :text
       t.column :billing_address_state,    :text
       t.column :billing_address_postalcode,    :text
       t.column :billing_address_country,    :text
       t.column :rating,    :text
       t.column :phone_office,    :text
       t.column :phone_alternate,    :text
       t.column :website,    :text
       t.column :ownership,    :text
       t.column :employees,    :text
       t.column :ticker_symbol,    :text
       t.column :shipping_address_street,    :text
       t.column :shipping_address_city,    :text
       t.column :shipping_address_state,    :text
       t.column :shipping_address_postalcode,    :text
       t.column :shipping_address_country,    :text
       t.column :parent_id,    :text
       t.column :sic_code,    :text
       t.column :campaign_id,    :text
    end
    CrmAccount.grant_access
  end
end
