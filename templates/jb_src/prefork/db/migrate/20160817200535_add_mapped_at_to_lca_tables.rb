class AddMappedAtToLcaTables < ActiveRecord::Migration
  def change
    add_column :local_exchanges,          :mapped_at, :datetime
    add_column :north_american_exchanges, :mapped_at, :datetime
    add_column :local_exchanges_north_american_exchanges, :updated_at, :datetime
  end
end
