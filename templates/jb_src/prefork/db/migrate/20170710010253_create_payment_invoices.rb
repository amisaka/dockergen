class CreatePaymentInvoices < ActiveRecord::Migration
  def change
    create_table :payment_invoices do |t|
      t.integer :payment_id
      t.integer :invoice_id
      t.decimal :amount

      t.timestamps null: false
    end
    PaymentInvoice.grant_access
  end
end
