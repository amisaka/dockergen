class AddAutocreateToPhone < ActiveRecord::Migration
  def self.up
    add_column :phones, :autocreate, :boolean, :default => false
    add_column :btns,   :lastresort, :boolean, :default => false

    $BtnOfLastResort = Btn.create(
      billingtelephonenumber: '18004653847',
      client_id: $LastResortClient)

    $BtnOfLastResort.attributes[:lastresort] = true
    $BtnOfLastResort.save!
  end

  def self.down
    remove_column :phones, :autocreate
    remove_column :btns,   :lastresort
  end
end
