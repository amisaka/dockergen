class AddForeignPidToBillableServices < ActiveRecord::Migration
  def change
    add_column :billable_services, :foreign_pid, :integer
    add_index :billable_services, :foreign_pid
  end
end
