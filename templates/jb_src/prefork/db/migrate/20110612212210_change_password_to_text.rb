class ChangePasswordToText < ActiveRecord::Migration
  def self.up
    remove_column :phones, :sip_password
    add_column :phones, :sip_password, :text
  end

  def self.down
  end
end
