class SetCountryToCaWhenCanada < ActiveRecord::Migration
  def change
    RateCenter.where(isocountry: 'US').where(province: ['BC','AB','SK','MB','ON','QC','NB','NS','PE','NL','NU','NT','YK']).update_all(isocountry: 'CA', country: 'Canada')
  end
end
