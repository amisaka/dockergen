class AddCountryCodeToRatePlan < ActiveRecord::Migration
  def change
    add_column :rate_plans, :country_code, :integer
  end
end
