class CreateBtns < ActiveRecord::Migration
  def self.up
    create_table :btns do |t|
      t.string :phone_number
      t.integer :client_id

      t.timestamps
    end
    Btn.grant_access
  end

  def self.down
    drop_table :btns
  end
end
