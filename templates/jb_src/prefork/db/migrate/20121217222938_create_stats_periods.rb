class CreateStatsPeriods < ActiveRecord::Migration
  def self.up
    create_table :stats_periods do |t|
      t.date :day
      t.integer :hour
      t.string :type

      t.timestamps
    end

    StatsPeriod.grant_access
  end

  def self.down
    drop_table :stats_periods
  end
end
