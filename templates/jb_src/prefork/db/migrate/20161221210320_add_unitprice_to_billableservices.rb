class AddUnitpriceToBillableservices < ActiveRecord::Migration
  def change
    add_column :billable_services, :unitprice, :decimal, precision: 8, scale: 2
  end
end
