class AddStatsProcessorMarkToClient < ActiveRecord::Migration
  def change
    change_table :clients do |t|
      t.column :stats_enabled, :boolean, default: false
    end
  end
end
