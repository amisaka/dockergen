class AddSupportOnlyToInvoiceTemplates < ActiveRecord::Migration
  def change
    add_column :invoice_templates, :irregular_invoice, :boolean, :default => false
    InvoiceTemplate.create(name: "Support Only Invoices", invoice_cycle: "support", irregular_invoice: true)
  end
end
