class AddServiceTypeToPhones < ActiveRecord::Migration
  def change
    add_column :phones, :service_type, :string
  end
end
