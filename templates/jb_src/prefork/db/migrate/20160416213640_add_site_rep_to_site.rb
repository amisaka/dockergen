class AddSiteRepToSite < ActiveRecord::Migration
  def change
    add_column :sites, :site_rep_id, :integer

    OTRS::CustomerUser.find_each { |cu|
      cc = cu.otrs_customercompany
      site = cc.try(:site)
      next unless site
      unless site.try(:site_rep)
        site.site_rep = cu
        site.save!
      end
    }
  end
end
