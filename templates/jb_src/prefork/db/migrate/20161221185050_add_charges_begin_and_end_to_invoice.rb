class AddChargesBeginAndEndToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :charges_begin, :date
    add_column :invoices, :charges_end, :date
  end
end
