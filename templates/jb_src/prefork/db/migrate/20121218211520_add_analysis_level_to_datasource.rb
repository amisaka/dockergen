class AddAnalysisLevelToDatasource < ActiveRecord::Migration
  def self.up
    add_column :cdrviewer_datasource, :analysis_level, :integer, :default => 0
    add_column :cdrviewer_datasource, :active_day,     :integer, :default => 0
  end

  def self.down
    remove_column :cdrviewer_datasource, :analysis_level
    remove_column :cdrviewer_datasource, :active_day
  end
end
