class CreateDefaultBtnAndNewcustomer < ActiveRecord::Migration
  def up
    # added because when running all migrations, the site_number attribute is not
    # always found.
    add_column :sites, :support_service_level, :text, :default => '1'
    add_column 'otrs.customer_user', :support_service_level, :text, :default => '1'

    Site.connection.schema_cache.clear!
    Site.reset_column_information
    OTRS::CustomerUser.reset_column_information
    Rails.cache.clear

    Client.make_customer('Client inconnu/Unknown client','0000000000', 'Unknown', 'unknown@example.ip4b.net')

    Client.make_customer('Client nouveau/New client', '0000000001','Location nouveau', 'newinstall@example.ip4b.net')
  end
end
