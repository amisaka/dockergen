class RemovePrivateradio < ActiveRecord::Migration
  def change
    drop_table :privateradio_channel
    drop_table :privateradio_channelschedule
    drop_table :privateradio_messageorder
    drop_table :privateradio_workorder
  end
end
