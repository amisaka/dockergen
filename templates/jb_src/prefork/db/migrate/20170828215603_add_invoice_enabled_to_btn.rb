class AddInvoiceEnabledToBtn < ActiveRecord::Migration
  def change
    add_column :btns, :invoicing_enabled, :boolean
  end
end
