class AddToClient < ActiveRecord::Migration
  def change
    change_table(:clients) do |t|
      t.column :crm_account_id,   :string
    end
  end
end
