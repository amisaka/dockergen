class AddHuntgroupToPhone < ActiveRecord::Migration
  def self.up
    add_column :phones, :huntgroup, :boolean, :default => false
  end

  def self.down
    remove_column :phones, :huntgroup
  end
end
