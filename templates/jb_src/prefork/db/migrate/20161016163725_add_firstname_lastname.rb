class AddFirstnameLastname < ActiveRecord::Migration
  def change
    change_table :phones do |t|
      t.column :firstname, :text
      t.column :lastname,  :text
      t.column :calling_firstname, :text
      t.column :calling_lastname,  :text
    end
    change_table :sites do |t|
      t.column :bw_groupId, :text
    end
  end
end
