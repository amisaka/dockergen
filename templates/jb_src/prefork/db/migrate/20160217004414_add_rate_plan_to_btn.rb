class AddRatePlanToBtn < ActiveRecord::Migration
  def self.up
    add_column :btns, :rate_plan_id, :integer
  end

  def self.down
    remove_column :btns, :rate_plan_id
  end
end
