class CreateStockInvoiceTemplates < ActiveRecord::Migration
  def up
    YAML.load(File.open('db/initialdata/invoice_templates.yml')).each {|key,thing|
      it = InvoiceTemplate.find_by_id(thing['id']) || InvoiceTemplate.create(id: thing['id'])
      it.id = thing['id']
      thing.delete('id')
      it.update(thing)
    }
  end
end
