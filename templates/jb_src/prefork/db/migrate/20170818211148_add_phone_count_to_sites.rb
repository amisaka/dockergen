class AddPhoneCountToSites < ActiveRecord::Migration
  def up
    add_column :sites, :phones_count, :integer, :null => false, :default => 0

    Site.reset_column_information
    Site.all.find_each do |p|
      Site.update_counters p.id, :phones_count => p.phones.length
    end
  end
  def down
    remove_column :sites, :phones_count
  end
end
