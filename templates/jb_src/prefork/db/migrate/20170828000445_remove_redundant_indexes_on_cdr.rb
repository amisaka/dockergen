class RemoveRedundantIndexesOnCdr < ActiveRecord::Migration
  disable_ddl_transaction!
  def up
    execute "DROP INDEX CONCURRENTLY IF EXISTS cdrviewer_cdr_route_idx;"
    execute "DROP INDEX CONCURRENTLY IF EXISTS cdrviewer_cdr_direction_group;"
    add_index :cdrviewer_cdr, :invoice_id,  algorithm: :concurrently
  end
end
