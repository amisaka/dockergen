class AddHumanValidationDateToClients < ActiveRecord::Migration
  def change
    add_column :clients, :human_validation_date, :datetime
  end
end
