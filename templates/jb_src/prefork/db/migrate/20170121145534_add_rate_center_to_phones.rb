class AddRateCenterToPhones < ActiveRecord::Migration
  def change
    add_column :phones, :rate_center_id, :integer
    add_column :btns,   :rate_center_id, :integer
    Phone.reset_column_information
    Btn.reset_column_information
  end
end
