class AddPstManToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :pstman, :decimal, precision: 10, scale: 2
  end
end
