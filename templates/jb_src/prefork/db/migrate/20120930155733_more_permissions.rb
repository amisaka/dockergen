class MorePermissions < ActiveRecord::Migration
  def self.up
    ['portal_service', 'callstats', 'portal_customerservice' ].each { |db|
      execute "grant select,insert,update,delete on #{db} to beaumont;"
      execute "grant usage,select,update on #{db}_id_seq to beaumont;"
    }
  end

  def self.down
  end
end
