class AddTypeToRatePlan < ActiveRecord::Migration
  def change
    add_column :rate_plans, :type, :text
    RatePlan.reset_column_information
  end
end
