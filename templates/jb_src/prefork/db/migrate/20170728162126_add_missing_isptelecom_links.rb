# coding: utf-8
class AddMissingIsptelecomLinks < ActiveRecord::Migration
  def up
    return true unless ::Rails.env.include?("production")

    [
      [443,1301,"HEBERTVILLE","QC"],
      [1837,1335,"LA BAIE","QC"],
      [1959,1446,"STE-GENEVIEVE","QC"],
      [2031,1524,"SAINT-FELIX-DE-KINGSEY","QC"],
      [2071,1525,"SAINT-SEBASTIEN-DE-FRONTENAC","QC"],
      [2044,1567,"ST-JEAN-SUR-RICHELIEU","QC"],
      [2011,1579,"STE-JULIE-DE-VERCHERES","QC"],
      [2005,1585,"STE-ANNE-DES-PLAINES","QC"],
      [2115,1596,"THERFORD MINES","QC"],
      [1720,1610,"ARNTFIELD","QC"],
      [1720,1624,"EVAIN","QC"],
      [1720,1652,"ROLLET","QC"],
    ].each { |mapping|
      (rcid, lid, localname, prov) = mapping
      rc = RateCenter.find(rcid)
      le = LocalExchange.find(lid)
      le.rate_center = rc
      le.save!
      rc.localnames << le.name
      rc.save!
    }
  end
end
