class AddBilledCostToCdr < ActiveRecord::Migration
  def change
    # what we were charged for this call
    add_column :cdrviewer_cdr, :call_cost,  :decimal, precision: 7, scale: 3

    # what we charged for this call.
    add_column :cdrviewer_cdr, :call_price, :decimal, precision: 7, scale: 3
  end
end
