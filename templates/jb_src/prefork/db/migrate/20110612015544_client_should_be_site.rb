class ClientShouldBeSite < ActiveRecord::Migration
  def self.up
    rename_column :phones, :client_id, :site_id
  end

  def self.down
    rename_column :phones, :site_id, :client_id
  end
end
