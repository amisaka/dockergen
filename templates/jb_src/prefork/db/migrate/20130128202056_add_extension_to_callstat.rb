class AddExtensionToCallstat < ActiveRecord::Migration
  def self.up
    add_column :callstats, :phone_id, :integer, :null => true, :default => nil
  end

  def self.down
    remove_column :callstats, :phone_id
  end
end
