class CreateClients < ActiveRecord::Migration
  def self.up
    create_table :clients do |t|
      t.string :name
      t.integer :billing_site_id
      t.string :admin_contact
      t.string :technical_contact

      t.timestamps null: true
    end
    Client.grant_access

    add_column :sites, :client_id, :integer
  end

  def self.down
    drop_table :clients
    remove_column :sites, :client_id
  end
end
