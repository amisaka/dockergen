class CompleteDevise2Info < ActiveRecord::Migration
  def change
    ## see: https://github.com/plataformatec/devise/wiki/How-To:-Upgrade-to-Devise-2.0-migration-schema-style
    change_table(:users) do |t|
      ## Recoverable
      t.datetime :reset_password_sent_at
    end
  end
end
