class AddUniqueIndex < ActiveRecord::Migration
  def self.up
    add_index :btns,    :id, :unique => true
    add_index :clients, :id, :unique => true
    add_index :phones,  :id, :unique => true
    add_index :sites,   :id, :unique => true
  end

  def self.down
    remove_index :btns, :id
    remove_index :clients, :id
    remove_index :phones, :id
    remove_index :sites, :id
  end
end
