class AddInvoiceNumberToCdr < ActiveRecord::Migration
  def up
    fake_invoice = Invoice.fake_invoice

    # done without the default initially, because this permits the effort
    # to run without so many locks
    add_column :cdrviewer_cdr, :invoice_id, :integer

    # only update value for recent CDRs.
    n = connection.quote(fake_invoice.id)
    execute "UPDATE cdrviewer_cdr SET invoice_id=#{n} WHERE invoice_id IS NULL AND startdatetime >= '2017-01-01';"
  end
end
