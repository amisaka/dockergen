class ChangeCrmAccountIdToAccountCode < ActiveRecord::Migration
  def change
    Client.find_each { |client|
      client.client_crm_account_code_fix
    }
  end
end
