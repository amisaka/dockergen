class CalculateCallDuration < ActiveRecord::Migration
  def self.up
    count = 0
    Call.where(:duration => nil).find_each { |c|

      # If Call object's duration is not set, then calculate it
      next unless c.duration.nil?

      c[:duration] = c.calc_duration
      c.save!
      count += 1

      if (count % 1000) == 1
        puts " ..calc_duration #{count} records"
      end
    }
  end

  def self.down
  end
end
