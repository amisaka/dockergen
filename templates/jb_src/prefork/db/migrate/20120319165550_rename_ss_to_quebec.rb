class RenameSsToQuebec < ActiveRecord::Migration
  def self.up
    Site.all.each { |s|
      if s.name == "SS/HQ"
        s.name = "Quebec"
        s.save!
      end
    }
  end

  def self.down
    # nothing to do
  end
end
