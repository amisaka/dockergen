class AddRenewalToBillableService < ActiveRecord::Migration
  def change
    add_column :billable_services, :renewal_id, :integer
  end
end
