class AddTypeIndexOncallstats < ActiveRecord::Migration
  def self.up

    add_index     :callstats, :type
    add_index     :callstats, :stats_period_id
    add_index     :callstats, :phone_id
    add_index     :callstats, [:stats_period_id,:type]
  end

  def self.down
    remove_index     :callstats, :type
    remove_index     :callstats, :stats_period_id
    remove_index     :callstats, :phone_id
    remove_index     :callstats, [:stats_period_id, :type]
  end
end
