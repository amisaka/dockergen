class AddDirectionGroupIndex < ActiveRecord::Migration
  # this speeds up CDR loading significantly on machines with a small amount of ram
  def change
    execute "commit;"
    execute "create index concurrently cdrviewer_cdr_direction_group on cdrviewer_cdr ( ascalltype, direction );"
  end
end
