class RemoveSpareFromCdr < ActiveRecord::Migration
  def change
    remove_column :cdrviewer_cdr, :spare1
    remove_column :cdrviewer_cdr, :spare2
  end
end
