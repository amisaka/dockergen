class AddForeignPidToServices < ActiveRecord::Migration
  def change
    add_column :services, :foreign_pid, :integer
    add_index :services, :foreign_pid, unique: true
  end
end
