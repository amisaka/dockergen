class AddLocalnamesToRateCenters < ActiveRecord::Migration
  def up
    add_column :rate_centers, :localnames, :text, array: true, default: []
    RateCenter.reset_column_information

    RateCenter.find_each { |rc|
      rc.localnames = [rc.localname]
      rc.save!
    }

    remove_column :rate_centers, :localname
    RateCenter.reset_column_information
  end
end
