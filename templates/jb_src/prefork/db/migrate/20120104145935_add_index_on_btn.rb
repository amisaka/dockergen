class AddIndexOnBtn < ActiveRecord::Migration
  def self.up
    add_index :btns,    :billingtelephonenumber, :unique => true
  end

  def self.down
    remove_index :btns, :billingtelephonenumber
  end
end
