class UpdatePermissions < ActiveRecord::Migration
  def self.up
    execute "GRANT SELECT,UPDATE,INSERT,DELETE ON local_exchanges TO beaumont;";
    execute "GRANT SELECT,UPDATE,INSERT,DELETE ON north_american_exchanges TO beaumont;";
    execute "GRANT SELECT,UPDATE,INSERT,DELETE ON exchanges_manual_matches TO beaumont;";
    execute "GRANT SELECT,UPDATE,INSERT,DELETE,TRUNCATE ON local_exchanges_north_american_exchanges TO beaumont;";
    execute "GRANT SELECT,UPDATE,INSERT,DELETE ON carriers TO beaumont;";
  end

end
