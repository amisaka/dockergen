class AddTransfertovoicemailToCall < ActiveRecord::Migration
  def self.up
    add_column :calls, :transfertovoicemail, :boolean, :null => true, :default => nil
  end

  def self.down
    remove_column :calls, :transfertovoicemail
  end
end
