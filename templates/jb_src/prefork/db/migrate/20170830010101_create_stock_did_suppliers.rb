class CreateStockDIDSuppliers < ActiveRecord::Migration
  def up
    YAML.load(File.open('db/initialdata/did_suppliers.yml')).each {|key,thing|
      it = DIDSupplier.find_by_symbol(key) || DIDSupplier.where(id: thing['id']).take || DIDSupplier.create(id: thing['id'])
      it.id = thing['id']
      thing.delete('id')
      it.update(thing)
      SystemVariable.setnumber(key, it.id)
    }
  end
end
