class CreateInvoices < ActiveRecord::Migration
  def change
    create_table 'public.invoices' do |t|
      t.integer :client_id
      t.date :invoice_date
      t.date :service_begin
      t.date :service_end
      t.decimal :last_amount_invoiced, scale: 2, precision: 10
      t.decimal :last_amount_paid,     scale: 2, precision: 10
      t.decimal :amount_invoiced,      scale: 2, precision: 10
      t.decimal :pstbc ,      scale: 2, precision: 10
      t.decimal :pstalb,      scale: 2, precision: 10
      t.decimal :pstsask,     scale: 2, precision: 10
      t.decimal :hst_tvh,     scale: 2, precision: 10
      t.decimal :gst_tps,     scale: 2, precision: 10
      t.decimal :tvq,         scale: 2, precision: 10
      t.text :invoicenumber
      t.text :invoice_path

      t.timestamps null: false
    end
    Invoice.grant_access
  end
end
