class AddTypeToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :type, :text, :default => 'SimpleInvoice'
  end
end
