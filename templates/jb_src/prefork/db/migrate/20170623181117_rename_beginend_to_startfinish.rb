class RenameBeginendToStartfinish < ActiveRecord::Migration
  def change
    rename_column :invoices, :charges_begin, :charges_start
    rename_column :invoices, :charges_end,   :charges_finish
    rename_column :invoices, :service_begin, :service_start
    rename_column :invoices, :service_end,   :service_finish

    rename_column :billable_services, :service_begin, :service_start
    rename_column :billable_services, :service_end,   :service_finish
  end
end
