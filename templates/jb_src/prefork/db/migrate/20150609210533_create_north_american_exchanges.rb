class CreateNorthAmericanExchanges < ActiveRecord::Migration
  def self.up
    create_table :north_american_exchanges do |t|
      t.string :province
      t.string :name
      t.integer :npa
      t.integer :nxx

      t.timestamps
    end
    NorthAmericanExchange.grant_access
  end

  def self.down
    drop_table :north_american_exchanges
  end
end
