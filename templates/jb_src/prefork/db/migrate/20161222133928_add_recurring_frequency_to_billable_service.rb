class AddRecurringFrequencyToBillableService < ActiveRecord::Migration
  def change
    add_column :billable_services, :frequency_months, :integer, default: 1
    add_column :billable_services, :active_month,     :boolean, default: false
  end
end
