class ExtendTotalprice < ActiveRecord::Migration
  def up
    change_column :billable_services, :totalprice, :decimal, precision: 8, scale: 2
  end
end
