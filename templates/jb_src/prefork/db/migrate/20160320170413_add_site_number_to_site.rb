class AddSiteNumberToSite < ActiveRecord::Migration
  def change
    add_column :sites, :site_number,     :text
    add_column :sites, :activation_date, :datetime, :default => Time.now
    add_column :sites, :termination_date, :datetime, :default => nil
    Rails.cache.clear
  end
end
