class AddCountryCodeMobileToRateCenter < ActiveRecord::Migration
  def change
    change_table :rate_centers do |t|
      t.column :isocountry,   :text,    limit: 2, default: 'US'  # "FR"
      t.column :country,      :text,    default: "United States" # France
      t.column :country_code, :integer, default: 1, null: nil    # eg: 33
      t.column :mobile,       :boolean, default: false, null: nil
    end

    # now load the calling_codes list from the list maintained in github
    # as the calling_codes table.
    RateCenter.update_from_calling_codes

  end
end
