class CreateDIDSuppliers < ActiveRecord::Migration
  def up
    create_table :did_suppliers do |t|
      t.text :name
      t.boolean :voip

      t.timestamps null: false
    end
    rename_column :phones, :voip_supplier_id, :did_supplier_id
    DIDSupplier.grant_access
  end
  def down
    drop_table :did_suppliers
  end
end
