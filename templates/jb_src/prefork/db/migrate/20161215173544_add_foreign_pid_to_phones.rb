class AddForeignPidToPhones < ActiveRecord::Migration
  def change
    add_column :phones, :foreign_pid, :integer
    add_index :phones, :foreign_pid
  end
end
