class ExpandBtn < ActiveRecord::Migration
  def self.up
    change_table :btns do |b|
      b.column :start_date, :date, :default => '1970-01-01'
      b.column :end_date,   :date
      b.rename :phone_number, :billingtelephonenumber
    end
  end

  def self.down
    change_table :btns do |b|
      b.remove :start_date
      b.remove :end_date
      b.rename :billingtelephonenumber, :phone_number
    end

  end
end
