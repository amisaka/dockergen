class AddCarrierIdToRatePlan < ActiveRecord::Migration
  def change
    add_column :rate_plans, :carrier_id, :integer
  end
end
