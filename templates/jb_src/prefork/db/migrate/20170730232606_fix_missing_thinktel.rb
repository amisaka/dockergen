# coding: utf-8
class FixMissingThinktel < ActiveRecord::Migration
  def up
    return true unless ::Rails.env.include?("production")

    [
      [10, 2204, 'BEAUHARNOIS'],
      [10, 1324, 'BEAUHARNOIS'],
      [39, 2290, 'LACMGANTIC'],
      [39, 1523, 'LACMGANTIC'],
      [19, 2193, 'LAVAL-EST'],
      [44, 820, "ST HIPPOLYTE"],
      [44, 1569, "ST HIPPOLYTE"],
      [44, 2188, "ST HIPPOLYTE"],
      [46, 812, "ST HYACINTHE"],
      [46, 1553, "ST HYACINTHE"],
      [46, 2252, "ST HYACINTHE"],
      [38, 1378, "GRAND-REMOUS"],
      [41, 1356, "MONT-LAURIER"],
      [42, 1402, "STE-JULIENNE"],
      [19, 1448, "LAVAL EAST"],
      [35,   1477, "BLANC-SABLON"],
      [16,   1617, "CHIBOUGAMAU"],
      [20,   1540, "STE AGATHE"],
      [36,   1599, "DESCHAILLONS"],
      [48,  1677, "ST-POLYCARPE"],
      [48,  1677, "ST_POLYCARPE"],
      [15,   1325, "CHATEAUGUAY"],
      [7, 1407, "BERGERONNES"],
      [1, 1663, "WASKAGANISH"],
      [37, 1367, "FRELIGHSBURG"],
      [43, 2203, "SAINTE-GENEVIèVE"],
      [48,  2178, "SAINT-POLYCARPE"],
      [42,  2243, "SAINTE-JULIENNE"],
      [553, 2970, "DEUX-RIVIERES"],
      [1146, 3134, "LITTLE HEARTS"],
      [1698, 3202, "MORELL-ST PETERS"],
    ].each { |mapping|
      (rcid, lid, name) = mapping
      rc = RateCenter.find(rcid)
      le = LocalExchange.find(lid)
      le.rate_center = rc
      le.save!
      rc.deleted_at = nil
      rc.localnames << le.name
      rc.save!
    }
  end
end
