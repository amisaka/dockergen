class AddTypeToCall < ActiveRecord::Migration
  def self.up
    add_column :calls, :type, :string, :default => 'Call'
    add_index  :calls, :type
    #Call.find_each { |call| call.type = 'Call'; call.save! }
    # above too slow.
    connection.execute("UPDATE calls SET type='Call' where type IS NULL;");
  end

  def self.down
    remove_column :calls, :type, :string
  end
end
