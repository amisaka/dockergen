class FixLanguagePreference < ActiveRecord::Migration
  def up
    # for Anglais
    Client.where("language_preference LIKE 'A%'").update_all(language_preference: 'English')
    Client.where("language_preference LIKE 'E%'").update_all(language_preference: 'English')
    Client.where("language_preference LIKE 'F%'").update_all(language_preference: 'French')
  end
end
