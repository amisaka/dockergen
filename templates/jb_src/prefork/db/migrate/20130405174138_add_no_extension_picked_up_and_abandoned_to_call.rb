class AddNoExtensionPickedUpAndAbandonedToCall < ActiveRecord::Migration
  def self.up
    add_column :calls, :no_extension_picked_up, :boolean
    add_column :calls, :abandoned_call,         :boolean
  end
  def self.down
    remove_column :calls, :no_extension_picked_up, :boolean
    remove_column :calls, :abandoned_call,         :boolean
  end
end
