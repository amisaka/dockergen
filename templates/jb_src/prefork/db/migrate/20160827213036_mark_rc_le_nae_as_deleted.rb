class MarkRcLeNaeAsDeleted < ActiveRecord::Migration
  def change
    change_table :rate_centers do |t|
      t.column :deleted_at, :datetime, default: nil
    end
    change_table :local_exchanges do |t|
      t.column :deleted_at, :datetime, default: nil
    end
    change_table :north_american_exchanges do |t|
      t.column :deleted_at, :datetime, default: nil
    end
  end
end
