class AddProcessedDatetimeToDatasource < ActiveRecord::Migration
  def change
    change_table :cdrviewer_datasource do |t|
      t.column :created_at, :datetime
      t.column :updated_at, :datetime
      t.column :data_at,    :datetime
    end

    Datasource.reset_column_information
    Datasource.do_analysis_six
  end
end
