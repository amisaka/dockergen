class RewordOtrsUserRepresentative < ActiveRecord::Migration
  def change
    OTRS::CustomerUser.find_each { |ou|
      unless ou.customer_id
        ou.delete
        next
      end

      site = ou.sites.first
      unless site.otrs_customercompany
        ou.delete
        next
      end

      if site.try(:client) and !site.site_number.blank?
        puts "ou:#{ou.id} (#{site.client.name}) processing site: #{site.try(:id)} #{site.site_number}"
        site.client.client_crm_account_code_fix
        ou.update_from_site(site)
      end
    }
  end
end
