class AddDecimalToInvoiceNumbers < ActiveRecord::Migration
  def change
    change_table :invoices do |t|
      t.change  "last_amount_invoiced", :decimal, precision: 11, scale: 3
      t.change  "last_amount_paid",     :decimal, precision: 11, scale: 3
      t.change  "amount_invoiced",      :decimal, precision: 11, scale: 3
      t.change  "pstbc",                :decimal, precision: 11, scale: 3
      t.change  "pstalb",               :decimal, precision: 11, scale: 3
      t.change  "pstsask",              :decimal, precision: 11, scale: 3
      t.change  "hst_tvh",              :decimal, precision: 11, scale: 3
      t.change  "gst_tps",              :decimal, precision: 11, scale: 3
      t.change  "tvq",                  :decimal, precision: 11, scale: 3
      t.change  "pstman",               :decimal, precision: 11, scale: 3
      t.change  "call_pstbc",           :decimal, precision: 11, scale: 3
      t.change  "call_pstalb",          :decimal, precision: 11, scale: 3
      t.change  "call_pstsask",         :decimal, precision: 11, scale: 3
      t.change  "call_pstman",          :decimal, precision: 11, scale: 3
      t.change  "call_tvq",             :decimal, precision: 11, scale: 3
      t.change  "call_gst_tps",         :decimal, precision: 11, scale: 3
      t.change  "call_hst_tvh",         :decimal, precision: 11, scale: 3
      t.change  "services_subtotal",    :decimal, precision: 11, scale: 3
      t.change  "charges_subtotal",     :decimal, precision: 11, scale: 3
      t.change  "late_charges",         :decimal, precision: 11, scale: 3
    end
  end
end
