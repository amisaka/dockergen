class CreateRates < ActiveRecord::Migration
  def up
    create_table 'public.rates' do |t|
      t.integer :rate_plan_id
      t.integer :rate_center_id
      t.float   :cost
      t.integer :billing_increment
      t.integer :minimum_call_length
      t.timestamps null: false
    end

    Rate.grant_access
  end

  def down
    drop_table 'public.rates'
  end
end
