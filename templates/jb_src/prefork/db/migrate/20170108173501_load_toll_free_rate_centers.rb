class LoadTollFreeRateCenters < ActiveRecord::Migration
  def up
    YAML.load(File.open('db/initialdata/toll_free_rate_centers.yml')).each {|key,thing|
      rp = RateCenter.find_or_create_by(id: thing['id'])
      thing.delete('id')
      rp.update(thing)
    }
  end
end
