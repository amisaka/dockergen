class RenameExchangeToRateCenter < ActiveRecord::Migration
  def change
    rename_table :exchanges_manual_matches, :rate_centers
    add_column :north_american_exchanges, :rate_center_id, :integer
  end
end
