# coding: utf-8
class UpdateMoreRateCentersForMatches < ActiveRecord::Migration
  def up
    if Rails.env[0..9] == 'production'
      [
        [29,  ["PETIT ROCHER", "PETIT-ROCHER"]],
        [212, ["BLACK'S HARBOUR", "BLACKS HARBOUR"]],
        [266, ["YOUNGS COVE ROAD","YOUNG'S COVE ROAD"]],
        [391, ["FRESHWATER"]],
        [1345, ["DEUX-RIVIÈRES", "DEUX-RIVIERES"]],
        [595, ["GRAND BAY-WESTFIELD"]],
        [1146,["LITTLE HEARTS EASE", "LITTLE HEART'S EASE"]],
        [1698,["MORELL SAINT PETERS","MORELL-ST PETERS"]],
        [2143,[""]],   # was lerg6name FRANCIS, localnames: REGINA
        [4004,["FRANCOIS LAKE"]],
        [5656,["REGINA BEACH"]],
      ].each {|entry|
        id = entry[0]
        lnames = entry[1]
        RateCenter.find(id).update(localnames: lnames, deleted_at: nil)
      }
    end
  end
end
