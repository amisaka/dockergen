class PortalCustomer < ActiveRecord::Base
  self.table_name = "portal_customer"
end

class ClientsBecomesCustomer < ActiveRecord::Migration
  def self.up
    database_user = $DATABASE_USER || ENV['LOGNAME'];

    execute "grant all on sequence public.clients_id_seq to beaumont;"
    execute "grant all on sequence public.clients_id_seq to beaumont;"
    execute "grant all on sequence btns_id_seq to beaumont;"
    execute "grant all on sequence buildings_id_seq  to beaumont;"
    execute "grant all on sequence phones_id_seq to beaumont;"
    execute "grant all on sequence sites_id_seq to beaumont;"
    execute "grant all on sequence users_id_seq to beaumont;"

    execute "grant all on sequence btns_id_seq to beaumont;"
    execute "grant all on sequence buildings_id_seq  to beaumont;"
    execute "grant all on sequence phones_id_seq to beaumont;"
    execute "grant all on sequence sites_id_seq to beaumont;"
    execute "grant all on sequence users_id_seq to beaumont;"

    change_table :clients do |t|
      t.column :description, :string
      t.column :logo,        :string
      t.column :url,         :string
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
