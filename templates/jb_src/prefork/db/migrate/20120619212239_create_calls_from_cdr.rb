class CreateCallsFromCdr < ActiveRecord::Migration

  # this gets run under a transaction, which is really not so great.
  # if possible, always run this from rake novavision:callfix instead.
  def self.up
    Cdr.callidfix_all
  end

  def self.down
  end
end
