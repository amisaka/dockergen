class DefaultDIDSupplier < ActiveRecord::Migration
  def change
    Datasource.where(did_supplier_id: nil).update_all(did_supplier_id: DIDSupplier.find_by_symbol(:broadsoft).try(:id))
  end
end
