class ChangeBtnIndexToIncludeClientId < ActiveRecord::Migration
  def self.up
    #remove_index :btns, :billingtelephonenumber
    # above breaks with no such index for reasons never explained.
    # do it manually
    execute "drop index if exists index_btns_on_billingtelephonenumber;"
    add_index :btns,    :billingtelephonenumber, :unique => false
    add_index :btns,    [:billingtelephonenumber,:client_id,:end_date], :unique => true
  end

  def self.down
    remove_index :btns,[:billingtelephonenumber,:client_id,:end_date]
  end
end
