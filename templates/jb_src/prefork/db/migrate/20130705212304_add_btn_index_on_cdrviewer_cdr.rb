class AddBtnIndexOnCdrviewerCdr < ActiveRecord::Migration
  def self.up
    add_index :cdrviewer_cdr, :calledbtn_id
    add_index :cdrviewer_cdr, :callingbtn_id
  end

  def self.down
    remove_index :cdrviewer_cdr, :calledbtn_id
    remove_index :cdrviewer_cdr, :callingbtn_id
  end
end
