class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.datetime :payment_date
      t.decimal :amount, precision: 11, scale: 3
      t.text :payment_type
      t.text :payment_info
      t.integer :client_id

      t.timestamps null: false
    end
    Payment.grant_access

  end
end
