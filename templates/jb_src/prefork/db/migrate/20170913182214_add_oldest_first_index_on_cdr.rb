class AddOldestFirstIndexOnCdr < ActiveRecord::Migration
  def change
    add_index :cdrviewer_cdr, [:startdatetime, :lineno, :id]
  end
end
