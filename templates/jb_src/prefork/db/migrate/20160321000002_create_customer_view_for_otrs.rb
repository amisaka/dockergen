class CreateCustomerViewForOtrs < ActiveRecord::Migration
  def up
    # requires database owner, not just permissions.
    #execute "CREATE SCHEMA IF NOT EXISTS otrs;"

    execute File.read(File.join(Rails.root, "db/view/customer_company.sql"))
  end
  def down
    execute "DROP VIEW IF EXISTS otrs.customer_company;"
  end
end
