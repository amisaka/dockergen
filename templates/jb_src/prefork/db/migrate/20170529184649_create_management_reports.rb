class CreateManagementReports < ActiveRecord::Migration
  def up
    create_table :management_reports do |t|
      t.string :name
      t.datetime :reported_at
      t.json :value

      t.timestamps null: false
    end
    ManagementReport.grant_access
  end
  def down
    drop_table :management_reports
  end
end
