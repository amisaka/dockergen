class CreateBuildings < ActiveRecord::Migration
  def self.up
    create_table :buildings do |t|
      t.string :address1
      t.string :address2
      t.string :city
      t.string :province
      t.string :country
      t.string :postalcode

      t.timestamps
    end
    Building.grant_access
  end

  def self.down
    drop_table :buildings
  end
end
