class AddCallTaxesToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :call_pstbc, :decimal,   precision: 10, scale: 2
    add_column :invoices, :call_pstalb, :decimal,  precision: 10, scale: 2
    add_column :invoices, :call_pstsask, :decimal, precision: 10, scale: 2
    add_column :invoices, :call_pstman, :decimal,  precision: 10, scale: 2
    add_column :invoices, :call_tvq, :decimal,     precision: 10, scale: 2
    add_column :invoices, :call_gst_tps, :decimal, precision: 10, scale: 2
    add_column :invoices, :call_hst_tvh, :decimal, precision: 10, scale: 2
  end
end
