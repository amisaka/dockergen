class SetDefaultCountOnDatasource < ActiveRecord::Migration
  def self.up
    change_column "cdrviewer_datasource", :cdr_count, :integer, :default => 0
    change_column "cdrviewer_datasource", :call_count, :integer, :default => 0
  end
end
