class AddSiteAndSummaryId < ActiveRecord::Migration
  def change
    add_column :invoices, :site_id, :integer
    add_column :invoices, :summary_invoice_id, :integer
  end
end
