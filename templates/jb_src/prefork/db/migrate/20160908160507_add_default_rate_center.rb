class AddDefaultRateCenter < ActiveRecord::Migration
  def change
    unless RateCenter.find_by_id($DefaultRateCenter)
      RateCenter.create(id: $DefaultRateCenter,
                        province: 'DF',
                        lerg6name: 'DfltRC999',
                        localnames: [],
                        deleted_at: nil,
                        created_at: '1999-01-01'.to_date,
                        isocountry: 'XX',
                        country: "Rate Center",
                        country_code: 999,
                        mobile: 0)
    end
  end
end
