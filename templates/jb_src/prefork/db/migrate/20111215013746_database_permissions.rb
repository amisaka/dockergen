class DatabasePermissions < ActiveRecord::Migration
  def self.up
    execute "grant all on table    clients        to beaumont;"
    execute "grant all on sequence clients_id_seq to beaumont;"
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
