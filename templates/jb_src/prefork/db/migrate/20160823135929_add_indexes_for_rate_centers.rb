class AddIndexesForRateCenters < ActiveRecord::Migration
  def change
    # clear NAE table out of npa/nxx duplicates!
    lastnpa=nil
    lastnxx=nil
    NorthAmericanExchange.order([:npa,:nxx]).each { |nae|
      if nae.npa == lastnpa and nae.nxx == lastnxx
        nae.delete
        puts "Deleting duplicate for #{lastnpa} #{lastnxx}"
      else
        lastnpa = nae.npa
        lastnxx = nae.nxx
      end
    }
    add_index :north_american_exchanges, [:npa,:nxx], :unique => true
    add_index :north_american_exchanges, [:name,:province]
    add_index :local_exchanges, [:name, :province]
    add_index :rate_centers, :province
    add_index :rate_centers, [:province, :lerg6name]
  end
end
