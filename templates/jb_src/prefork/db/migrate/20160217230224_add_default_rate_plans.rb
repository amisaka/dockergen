class AddDefaultRatePlans < ActiveRecord::Migration
  def self.up
    unless RatePlan.find_by_name('ProvincePlan')
      RatePlan.create(:province => 'QC',
                      :country => 'Canada',
                      :description => 'Quebec Long Distance Plan',
                      :name => 'ProvincePlan')
    end

    unless RatePlan.find_by_name('NationalPlan')
      RatePlan.create(:province => nil,
                      :country => 'Canada',
                      :description => 'Canada-Wide Long Distance Plan',
                      :name => 'NationalPlan')
    end
  end

  def self.down
  end
end
