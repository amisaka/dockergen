class GrantDelayedJobs < ActiveRecord::Migration
  def self.up
    ApplicationRecord.grant_access('delayed_jobs')
  end
end
