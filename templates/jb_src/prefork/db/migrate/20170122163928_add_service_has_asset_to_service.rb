class AddServiceHasAssetToService < ActiveRecord::Migration
  def change
    add_column :services, :service_has_asset, :boolean, default: false
  end
end
