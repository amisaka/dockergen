class AddIndexTransferRelatedcallidToCdrviewerCdr < ActiveRecord::Migration
  def self.up
    # actually already present
    #add_index :cdrviewer_cdr,    :transfer_relatedcallid
    add_index :cdrviewer_cdr,    :relatedcallid
  end

  def self.down
    #drop_index :cdrviewer_cdr,   :transfer_relatedcallid
    drop_index :cdrviewer_cdr,   :relatedcallid
  end
end
