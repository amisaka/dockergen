class AddLocalNationalWorldRatePlansToBtn < ActiveRecord::Migration
  def change
    change_table :btns do |t|
      t.column :local_rate_plan_id,    :integer
      t.column :national_rate_plan_id, :integer
      t.column :world_rate_plan_id,    :integer
    end
    change_table :phones do |t|
      t.column :local_rate_plan_id,    :integer
      t.column :national_rate_plan_id, :integer
      t.column :world_rate_plan_id,    :integer
    end
  end
end
