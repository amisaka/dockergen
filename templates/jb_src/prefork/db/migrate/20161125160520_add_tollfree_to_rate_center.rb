class AddTollfreeToRateCenter < ActiveRecord::Migration
  def change
    add_column :rate_centers, :tollfree, :boolean, default: false
    add_column :rate_centers, :north_american_lookup, :boolean, default: false

    RateCenter.reset_column_information  
  end
end
