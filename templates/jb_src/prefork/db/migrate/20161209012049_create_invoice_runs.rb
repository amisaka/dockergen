class CreateInvoiceRuns < ActiveRecord::Migration
  def change
    create_table :invoice_runs do |t|
      t.integer    :invoice_year
      t.integer    :invoice_month
      t.text       :processed_up_to_en
      t.text       :payments_due_at_en
      t.text       :seasonal_message_en
      t.text       :processed_up_to_fr
      t.text       :payments_due_at_fr
      t.text       :seasonal_message_fr
      t.datetime   :invoice_run_date
      t.timestamps null: false
    end
    add_column :invoices, :invoice_run_id, :integer
  end
end
