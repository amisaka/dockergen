class AddSipAvailAndOnNet < ActiveRecord::Migration
  def change
    add_column :local_exchanges, :sip_availability, :boolean, :default => false
    add_column :local_exchanges, :on_net,           :boolean, :default => false
  end
end
