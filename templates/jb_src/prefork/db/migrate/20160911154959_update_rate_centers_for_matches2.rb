# coding: utf-8
class UpdateRateCentersForMatches2 < ActiveRecord::Migration
  def up
    if Rails.env[0..9] == 'production'
      [
        [212, 563],
        [1345, 90],
        [1345, 553],
        [391, 602],
        [391, 1132],
        [293, 595],
        [293, 1077],
        [1146, 282],
        [1146, 581],
        [1698, 587],
        [1698, 295],
        [29,   537],
        [29,   1096],
        [266,  588],
        [266,  1112],
      ].each { |am|
        rcBB = RateCenter.find(am[0])
        rcBB.deleted_at = nil; rcBB.save!
        rc1 = RateCenter.find_by_id(am[1])
        rc1.local_exchanges.each { |le|
          le.rate_center = rcBB; le.save!
        }
        rc1.north_american_exchanges.each { |nae|
          nae.rate_center = rcBB; nae.save!
        }
        rc1.update(deleted_at: '2016-09-01'.to_date)
      }

      # reset Regina,SK to cause rematch
      LocalExchange.where(name: "REGINA").update_all(rate_center_id: nil)
    end
  end
end
