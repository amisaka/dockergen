class AddCallfromCalltoToCall < ActiveRecord::Migration
  def self.up
    change_table :calls do |b|
      b.column :our_number,   :string
      b.column :their_number, :string
      b.column :terminated,   :boolean
      b.column :client_id,    :integer
    end

    count = 0
    Call.all.each { |c|
      c.fixdates!
      count += 1
      if (count % 1000) == 1
        puts " ..fixdate #{count} records"
      end
    }
 end

  def self.down
    change_table :calls do |b|
      b.remove :our_number
      b.remove :their_number
      b.remove :terminated
      b.remove :client_id
    end
  end
end
