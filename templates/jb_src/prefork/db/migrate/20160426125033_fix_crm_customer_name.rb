class FixCrmCustomerName < ActiveRecord::Migration
  def change
    CrmAccount.where(:name => nil).find_each {|crm|
      client = crm.client
      if client and crm.name.blank? and !client.read_attribute(:name).blank?
        crm.name = client.read_attribute(:name)
        crm.save!
      end
    }
  end
end
