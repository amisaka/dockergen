class AddIndexOnCdrviewerId < ActiveRecord::Migration
  def self.up
    add_index :cdrviewer_cdr, :id, :unique => true
  end

  def self.down
  end
end
