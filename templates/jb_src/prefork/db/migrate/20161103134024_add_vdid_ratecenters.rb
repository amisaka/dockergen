class AddVdidRatecenters < ActiveRecord::Migration
  def change
    add_column :rate_centers, :virtual_did, :boolean, :default => false

    RateCenter.reset_column_information
  end
end
