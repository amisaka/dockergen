class AddHumanValidationDateToPhones < ActiveRecord::Migration
  def change
    add_column :phones, :human_validation_date, :datetime
  end
end
