class AddLineOfBusinessToPhones < ActiveRecord::Migration
  def change
    add_column :phones, :line_of_business, :string
  end
end
