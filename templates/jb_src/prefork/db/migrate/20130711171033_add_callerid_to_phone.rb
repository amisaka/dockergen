class AddCalleridToPhone < ActiveRecord::Migration
  def self.up
    add_column :phones, :callerid, :string, :default => nil
  end

  def self.down
    remove_column :phones, :callerid
  end
end
