class AddEntryNumToLerg8 < ActiveRecord::Migration
  def change
    add_column :lerg8s, :status,   :text
    add_column :lerg8s, :entryNum, :text
  end
end
