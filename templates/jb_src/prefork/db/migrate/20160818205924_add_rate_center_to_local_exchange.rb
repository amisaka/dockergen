class AddRateCenterToLocalExchange < ActiveRecord::Migration
  def change
    add_column :local_exchanges, :rate_center_id, :integer
  end
end
