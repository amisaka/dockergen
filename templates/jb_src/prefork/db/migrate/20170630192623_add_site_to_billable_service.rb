class AddSiteToBillableService < ActiveRecord::Migration
  def change
    add_column :billable_services, :site_id, :integer
  end
end
