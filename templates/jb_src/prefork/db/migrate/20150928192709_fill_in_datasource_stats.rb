class FillInDatasourceStats < ActiveRecord::Migration
  def self.up

    c = self.connection

    Datasource.where(:cdrs_count => nil).find_each { |ds|
      q = 'update cdrviewer_datasource '\
            'set cdrs_count = (SELECT COUNT(*) FROM "cdrviewer_cdr" WHERE ("cdrviewer_cdr".datasource_id = %d )), '\
            'calls_count = (SELECT COUNT(*) FROM "calls" WHERE ("calls".datasource_id = %d ))  where id = %d ; ' % [ ds.id, ds.id, ds.id ];
      c.execute(q)


      # The query above does what the code below did.
      # See ticket https://code.credil.org/issues/10205 why the code below was commented out 2015/10/12
      # It can be deleted
      #ds.cdrs_count  = ds.cdrs.count
      #ds.calls_count = ds.calls.count
      #ds.set_default_analysis_level!
      #ds.save!
    }
  end

  def self.down
  end
end
