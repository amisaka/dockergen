class AddDIDSupplierToDatasource < ActiveRecord::Migration
  def change
    add_column :cdrviewer_datasource, :did_supplier_id, :integer
  end
end
