class CanonicalizeBtn < ActiveRecord::Migration
  def up
    Btn.find_each { |b1|
      begin
        b1.billingtelephonenumber = RateCenter.canonify_number(b1.billingtelephonenumber)
        b1.save!
      rescue StandardError
        nil
      end
    }
  end
end
