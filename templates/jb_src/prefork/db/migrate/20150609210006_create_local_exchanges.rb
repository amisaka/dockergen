class CreateLocalExchanges < ActiveRecord::Migration
  def self.up
    create_table :local_exchanges do |t|
      t.string :province
      t.string :name

      t.timestamps
    end
    LocalExchange.grant_access
  end

  def self.down
    drop_table :local_exchanges
  end
end
