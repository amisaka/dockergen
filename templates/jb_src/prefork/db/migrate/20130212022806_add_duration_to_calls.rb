class AddDurationToCalls < ActiveRecord::Migration
  def self.up
    change_table :calls do |b|
      b.column :duration, :integer
    end
  end

  def self.down
    change_table :calls do |b|
      b.remove :duration
    end
  end

end
