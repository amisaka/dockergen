class SplitStatsIntoPeriodsAndData < ActiveRecord::Migration
  def self.up
    remove_column :callstats, :call_count
    remove_column :callstats, :cdr_count
    remove_column :callstats, :day
    remove_column :callstats, :starttime
    remove_column :callstats, :endtime
    add_column :callstats, :value, :integer
    add_column :callstats, :stats_period_id, :integer
    #add_column :callstats, :statperiod_id, :integer
  end

  def self.down
    remove_column :callstats, :value 
    remove_column :callstats, :stats_period_id
    add_column :callstats, :call_count, :integer
    add_column :callstats, :cdr_count, :integer
    add_column :callstats, :day,       :date
    add_column :callstats, :starttime, :time
    add_column :callstats, :startend,  :time
  end
end
