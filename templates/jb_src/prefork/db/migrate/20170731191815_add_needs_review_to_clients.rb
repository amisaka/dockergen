class AddNeedsReviewToClients < ActiveRecord::Migration
  def change
    add_column :clients, :needs_review, :boolean, default: false, null: false
  end
end
