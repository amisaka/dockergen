class AddMedalRatePlans < ActiveRecord::Migration
  def up
    YAML.load(File.open('db/initialdata/rate_plans.yml')).each {|key,thing|
      rp = RatePlan.find_or_create_by(name: thing['name'])
      thing.delete('id')
      rp.update(thing)
    }
  end
end
