class CreateVoicePortalPhone < ActiveRecord::Migration
  def self.up
    add_column :phones, :voicemailportal, :boolean, :default => false
    voiceportal = Phone.new
    voiceportal.attributes['voicemailportal']=true
    voiceportal.extension='900'
    voiceportal.phone_number='2905550110'
    voiceportal.virtualdid=true
    voiceportal.notes='Voice Mail Portal'
    voiceportal.save!
  end

  def self.down
    remove_column :phones, :voiceportal
  end
end
