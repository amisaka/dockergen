class AddStatementTypeToClients < ActiveRecord::Migration
  def change
    add_column :clients, :statement_type, :string
  end
end
