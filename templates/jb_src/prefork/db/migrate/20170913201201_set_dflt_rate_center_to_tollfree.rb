class SetDfltRateCenterToTollfree < ActiveRecord::Migration
  def up
    if RateCenter.exists?(999999)
      rc1 = RateCenter.find(999999)
      rc1.tollfree = true
      rc1.save!
    end
  end
end
