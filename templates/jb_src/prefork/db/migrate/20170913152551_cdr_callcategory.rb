class CdrCallcategory < ActiveRecord::Migration
  def up
    change_column :cdrviewer_cdr, :callcategory, :text
  end
end
