class SetCaribbeanAreaCodes < ActiveRecord::Migration
  def change

    plans = Array.new
    (1..17).each {|id| plans[id] = RatePlan.find_by_id(id) }
    plans[5]  = RatePlan.find_by_id(3)   # North American Bronze -> Country-Bronze
    plans[8]  = RatePlan.find_by_id(6)   # North American Silver -> Country-Silver
    plans[14] = RatePlan.find_by_id(12)  # North American Gold   -> Country-Gold
    plans[11] = RatePlan.find_by_id(9)   # North American Diamond-> Country-Diamond

    num = 0
    [
      [ 'American Samoa',          'AS',684],
      [ 'Anguilla',                'AI',264],
      [ 'Antigua and Barbuda',     'AG',268],
      [ 'Bahamas',                 'BS',242],
      [ 'Barbados',                'BB',246],
      [ 'Bermuda',                 'BM',441],
      [ 'British Virgin Islands',  'VG',284],
      [ 'Cayman Islands',          'GP',345],
      [ 'Dominica',                'DM',767],
      [ 'Dominican Republic',      'DO',809],
      [ 'Dominican Republic',      'DO',829],
      [ 'Dominican Republic',      'DO',849],
      [ 'Grenada',                 'GD',473],
      [ 'Guam',                    'GU',671],
      [ 'Jamaica',                 'JM',876],
      [ 'Montserrat',              'MS',664],
      [ 'Northern Mariana Islands','MP',670],
      [ 'Puerto Rico',             'PR',787],
      [ 'Puerto Rico',             'PR',939],
      [ 'Saint Kitts and Nevis',   'KN',869],
      [ 'Saint Lucia',             'LC',758],
      [ 'Saint Vincent and the Grenadines', 'VC', 784],
      [ 'Saint Maarten', 	   'SX',721],
      [ 'Trinidad and Tobago',     'TT',868],
      [ 'Turks and Caicos Islands','TC', 649],
      [ 'United States Virgin Islands',	'VI',340],
    ].each { |island|
      (country, iso, areacode) = island
      NorthAmericanExchange.where(npa: areacode).each { |nae|
        nae.province = ""
        nae.save!
        rc = nae.rate_center
        rc.rates.each { |r|
          if plans[r.rate_plan_id]
            r.rate_plan = plans[r.rate_plan_id]
          else
            byebug
          end
          r.save!
        }
        puts "#{num} updating #{nae.npa}/#{nae.nxx} to #{iso}, #{country}"
        num += 1

        next if rc.calltype == :caribbean_calls
        rc.country = country
        rc.country_code = 1000 + areacode
        rc.isocountry = iso
        rc.province   = ''
        rc.north_american_lookup = false
        rc.save!

      }
    }

  end
end
