class CreateCarriers < ActiveRecord::Migration
  def self.up
    create_table :carriers do |t|
      t.string :name
      t.string :coverage

      t.timestamps
    end
    add_column :local_exchanges, :carrierplan_id, :integer

    Carrier.grant_access

    # now initialize connection with data from local_exchanges
    LocalExchange.find_each { |le|
      cr = Carrier.find_or_create_by_name(le.attributes['carrier'])
      cr.save!
      le.carrierplan_id = cr.id
      le.save!
    }
  end

  def self.down
    remove_column :local_exchanges, :carrier_id, :integer
    drop_table :carriers
  end
end
