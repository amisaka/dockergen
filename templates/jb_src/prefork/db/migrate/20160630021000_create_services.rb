class CreateServices < ActiveRecord::Migration
  def change
    create_table 'public.services' do |t|
      t.text :description
      t.text :name
      t.decimal :unitprice, precision: 7, scale: 2
      t.boolean :renewable
      t.boolean :available

      t.timestamps null: false
    end
    Service.grant_access
  end
end
