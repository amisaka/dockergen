class AddViewableLegsToCalls < ActiveRecord::Migration
  def self.up
    add_column    :calls, :viewable_legs, :integer
  end

  def self.down
    remove_column :calls, :viewable_legs
  end
end
