class AddCreatedUpdatedToRateCenters < ActiveRecord::Migration
  def change
    change_table :rate_centers do |t|
      t.column :updated_at, :datetime
      t.column :created_at, :datetime
    end
  end
end
