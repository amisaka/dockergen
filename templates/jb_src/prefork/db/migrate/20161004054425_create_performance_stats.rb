class CreatePerformanceStats < ActiveRecord::Migration
  def up
    create_table "public.performance_stats" do |t|
      t.text :login
      t.text :version
      t.text :name
      t.text :type
      t.integer :client_id
      t.datetime :start_time
      t.datetime :end_time
      t.integer :work_count

      t.timestamps null: false
    end
    PerformanceStat.grant_access
  end

  def down
    drop_table :performance_stats
  end
end
