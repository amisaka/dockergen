class AddBaseSuppliersToProduction < ActiveRecord::Migration
  def up
    DIDSupplier.make_supplier(:isptelecom,
                              {name: 'ISP Telecom VoIP',
                               voip: true})

    DIDSupplier.make_supplier(:thinktel,
                              {name: 'Thinktel/Distributel VoIP',
                               voip: true})

    DIDSupplier.make_supplier(:broadworks,
                              { name: 'Broadworks CDR',
                                voip: true })

    DIDSupplier.make_supplier(:troop,
                              { name: 'Bell TROOP legacy',
                                voip: false })

    DIDSupplier.make_supplier(:telus,
                              { name: 'Telus Rebillier',
                                voip: false })

    DIDSupplier.make_supplier(:draper,
                              { name: 'Draper Long Distance cards',
                                voip: true })

    DIDSupplier.make_supplier(:obm,
                              { name: 'OBM',
                                voip: false })

    DIDSupplier.make_supplier(:rogers,
                              { name: 'Rogers',
                                voip: false })
  end
end
