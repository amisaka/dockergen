class CreateCalls < ActiveRecord::Migration
  def self.up
    create_table :calls do |t|
      t.string :master_callid, :limit => 40
      t.string :call_type, :limit => 16

      t.timestamps
    end
    Call.grant_access
    #add_index  :cdrviewer_cdr, :localcallid
    #add_index  :cdrviewer_cdr, :transfer_relatedcallid
    execute "grant select,insert,update,delete on calls to beaumont;"
    execute "grant usage,select,update on calls_id_seq to beaumont;"
  end

  def self.down
    drop_table :calls
  end
end
