class DropFxConstraintOnCdr < ActiveRecord::Migration
  def up
    execute "alter table cdrviewer_cdr DROP CONSTRAINT IF EXISTS callingphone_id_refs_id_6057b4b6;"
    execute "alter table cdrviewer_cdr DROP CONSTRAINT IF EXISTS calledphone_id_refs_id_6057b4b6;"
  end
end
