class AddPreviousRunToInvoiceRun < ActiveRecord::Migration
  def change
    add_column :invoice_runs, :previous_run_id, :integer
  end
end
