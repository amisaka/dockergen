class RenameDsCounts < ActiveRecord::Migration
  def self.up
    rename_column "cdrviewer_datasource", :cdr_count,  :cdrs_count
    rename_column "cdrviewer_datasource", :call_count, :calls_count
  end

  def self.down
    rename_column "cdrviewer_datasource", :cdrs_count,  :cdr_count
    rename_column "cdrviewer_datasource", :calls_count, :call_count
  end
end
