class AddForeignPidToSite < ActiveRecord::Migration
  def change
    add_column :sites, :foreign_pid, :integer
  end
end
