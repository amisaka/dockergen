class AddNnaclItemsForNae < ActiveRecord::Migration
  def change
    change_table :north_american_exchanges do |t|
      t.text :coc_type
      t.text :special_service_code
      t.boolean :portable_indicator
      t.text :switch
      t.text :ocn
      t.text :lata
    end
  end
end
