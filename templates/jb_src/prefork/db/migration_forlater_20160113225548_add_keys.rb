class AddKeys < ActiveRecord::Migration
  def change
    add_foreign_key "btns", "clients", name: "btns_client_id_fk"
    add_foreign_key "calls", "clients", name: "calls_client_id_fk"
    add_foreign_key "calls", "cdrviewer_datasource", column: "datasource_id", name: "calls_datasource_id_fk"
    add_foreign_key "callstats", "clients", name: "callstats_client_id_fk"
    add_foreign_key "callstats", "phones", name: "callstats_phone_id_fk"
    add_foreign_key "callstats", "stats_periods", name: "callstats_stats_period_id_fk"
    add_foreign_key "cdrviewer_cdr", "calls", name: "cdrviewer_cdr_call_id_fk"
    add_foreign_key "cdrviewer_cdr", "btns", column: "calledbtn_id", name: "cdrviewer_cdr_calledbtn_id_fk"
    add_foreign_key "cdrviewer_cdr", "phones", column: "calledphone_id", name: "cdrviewer_cdr_calledphone_id_fk"
    add_foreign_key "cdrviewer_cdr", "btns", column: "callingbtn_id", name: "cdrviewer_cdr_callingbtn_id_fk"
    add_foreign_key "cdrviewer_cdr", "phones", column: "callingphone_id", name: "cdrviewer_cdr_callingphone_id_fk"
    add_foreign_key "cdrviewer_cdr", "cdrviewer_datasource", column: "datasource_id", name: "cdrviewer_cdr_datasource_id_fk"
    add_foreign_key "clients", "sites", column: "billing_site_id", name: "clients_billing_site_id_fk"
    add_foreign_key "phones", "btns", name: "phones_btn_id_fk"
    add_foreign_key "phones", "sites", name: "phones_site_id_fk"
    add_foreign_key "portal_customerservice", "portal_service", column: "service_id", name: "portal_customerservice_service_id_fk"
    add_foreign_key "sites", "buildings", name: "sites_building_id_fk"
    add_foreign_key "sites", "clients", name: "sites_client_id_fk"
  end
end
