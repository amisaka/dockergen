--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5rc1
-- Dumped by pg_dump version 9.5rc1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: otrs; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA otrs;


--
-- Name: v; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA v;


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


SET search_path = otrs, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acl; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE acl (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    description character varying(250),
    valid_id smallint NOT NULL,
    stop_after_match smallint,
    config_match text,
    config_change text,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: acl_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE acl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acl_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE acl_id_seq OWNED BY acl.id;


--
-- Name: acl_sync; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE acl_sync (
    acl_id character varying(200) NOT NULL,
    sync_state character varying(30) NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    change_time timestamp(0) without time zone NOT NULL
);


--
-- Name: article; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE article (
    id bigint NOT NULL,
    ticket_id bigint NOT NULL,
    article_type_id smallint NOT NULL,
    article_sender_type_id smallint NOT NULL,
    a_from character varying(3800),
    a_reply_to character varying(500),
    a_to character varying(3800),
    a_cc character varying(3800),
    a_subject character varying(3800),
    a_message_id character varying(3800),
    a_message_id_md5 character varying(32),
    a_in_reply_to character varying(3800),
    a_references character varying(3800),
    a_content_type character varying(250),
    a_body character varying NOT NULL,
    incoming_time integer NOT NULL,
    content_path character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: article_attachment; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE article_attachment (
    id bigint NOT NULL,
    article_id bigint NOT NULL,
    filename character varying(250),
    content_size character varying(30),
    content_type character varying(450),
    content_id character varying(250),
    content_alternative character varying(50),
    disposition character varying(15),
    content text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: article_attachment_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE article_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: article_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE article_attachment_id_seq OWNED BY article_attachment.id;


--
-- Name: article_flag; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE article_flag (
    article_id bigint NOT NULL,
    article_key character varying(50) NOT NULL,
    article_value character varying(50),
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL
);


--
-- Name: article_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE article_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: article_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE article_id_seq OWNED BY article.id;


--
-- Name: article_plain; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE article_plain (
    id bigint NOT NULL,
    article_id bigint NOT NULL,
    body text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: article_plain_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE article_plain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: article_plain_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE article_plain_id_seq OWNED BY article_plain.id;


--
-- Name: article_search; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE article_search (
    id bigint NOT NULL,
    ticket_id bigint NOT NULL,
    article_type_id smallint NOT NULL,
    article_sender_type_id smallint NOT NULL,
    a_from character varying(3800),
    a_to character varying(3800),
    a_cc character varying(3800),
    a_subject character varying(3800),
    a_body character varying NOT NULL,
    incoming_time integer NOT NULL
);


--
-- Name: article_sender_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE article_sender_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: article_sender_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE article_sender_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: article_sender_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE article_sender_type_id_seq OWNED BY article_sender_type.id;


--
-- Name: article_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE article_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: article_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE article_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: article_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE article_type_id_seq OWNED BY article_type.id;


--
-- Name: auto_response; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE auto_response (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    text0 character varying(6000),
    text1 character varying(6000),
    type_id smallint NOT NULL,
    system_address_id smallint NOT NULL,
    content_type character varying(250),
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: auto_response_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE auto_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auto_response_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE auto_response_id_seq OWNED BY auto_response.id;


--
-- Name: auto_response_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE auto_response_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: auto_response_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE auto_response_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auto_response_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE auto_response_type_id_seq OWNED BY auto_response_type.id;


--
-- Name: cloud_service_config; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE cloud_service_config (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    config text NOT NULL,
    config_md5 character varying(32) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: cloud_service_config_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE cloud_service_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cloud_service_config_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE cloud_service_config_id_seq OWNED BY cloud_service_config.id;


--
-- Name: customer_company; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE customer_company (
    customer_id character varying(150) NOT NULL,
    name character varying(200) NOT NULL,
    street character varying(200),
    zip character varying(200),
    city character varying(200),
    country character varying(200),
    url character varying(200),
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: customer_preferences; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE customer_preferences (
    user_id character varying(250) NOT NULL,
    preferences_key character varying(150) NOT NULL,
    preferences_value character varying(250)
);


--
-- Name: customer_user; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE customer_user (
    id integer NOT NULL,
    login character varying(200) NOT NULL,
    email character varying(150) NOT NULL,
    customer_id character varying(150) NOT NULL,
    pw character varying(64),
    title character varying(50),
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    phone character varying(150),
    fax character varying(150),
    mobile character varying(150),
    street character varying(150),
    zip character varying(200),
    city character varying(200),
    country character varying(200),
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: customer_user_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE customer_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_user_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE customer_user_id_seq OWNED BY customer_user.id;


--
-- Name: dynamic_field; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE dynamic_field (
    id integer NOT NULL,
    internal_field smallint DEFAULT 0 NOT NULL,
    name character varying(200) NOT NULL,
    label character varying(200) NOT NULL,
    field_order integer NOT NULL,
    field_type character varying(200) NOT NULL,
    object_type character varying(200) NOT NULL,
    config text,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: dynamic_field_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE dynamic_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dynamic_field_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE dynamic_field_id_seq OWNED BY dynamic_field.id;


--
-- Name: dynamic_field_value; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE dynamic_field_value (
    id integer NOT NULL,
    field_id integer NOT NULL,
    object_id bigint NOT NULL,
    value_text character varying(3800),
    value_date timestamp(0) without time zone,
    value_int bigint
);


--
-- Name: dynamic_field_value_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE dynamic_field_value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dynamic_field_value_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE dynamic_field_value_id_seq OWNED BY dynamic_field_value.id;


--
-- Name: follow_up_possible; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE follow_up_possible (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: follow_up_possible_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE follow_up_possible_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: follow_up_possible_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE follow_up_possible_id_seq OWNED BY follow_up_possible.id;


--
-- Name: generic_agent_jobs; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE generic_agent_jobs (
    job_name character varying(200) NOT NULL,
    job_key character varying(200) NOT NULL,
    job_value character varying(200)
);


--
-- Name: gi_debugger_entry; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE gi_debugger_entry (
    id bigint NOT NULL,
    communication_id character varying(32) NOT NULL,
    communication_type character varying(50) NOT NULL,
    remote_ip character varying(50),
    webservice_id integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL
);


--
-- Name: gi_debugger_entry_content; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE gi_debugger_entry_content (
    id bigint NOT NULL,
    gi_debugger_entry_id bigint NOT NULL,
    debug_level character varying(50) NOT NULL,
    subject character varying(255) NOT NULL,
    content text,
    create_time timestamp(0) without time zone NOT NULL
);


--
-- Name: gi_debugger_entry_content_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE gi_debugger_entry_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gi_debugger_entry_content_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE gi_debugger_entry_content_id_seq OWNED BY gi_debugger_entry_content.id;


--
-- Name: gi_debugger_entry_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE gi_debugger_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gi_debugger_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE gi_debugger_entry_id_seq OWNED BY gi_debugger_entry.id;


--
-- Name: gi_object_lock_state; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE gi_object_lock_state (
    webservice_id integer NOT NULL,
    object_type character varying(30) NOT NULL,
    object_id bigint NOT NULL,
    lock_state character varying(30) NOT NULL,
    lock_state_counter integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    change_time timestamp(0) without time zone NOT NULL
);


--
-- Name: gi_webservice_config; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE gi_webservice_config (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    config text NOT NULL,
    config_md5 character varying(32) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: gi_webservice_config_history; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE gi_webservice_config_history (
    id bigint NOT NULL,
    config_id integer NOT NULL,
    config text NOT NULL,
    config_md5 character varying(32) NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: gi_webservice_config_history_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE gi_webservice_config_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gi_webservice_config_history_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE gi_webservice_config_history_id_seq OWNED BY gi_webservice_config_history.id;


--
-- Name: gi_webservice_config_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE gi_webservice_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gi_webservice_config_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE gi_webservice_config_id_seq OWNED BY gi_webservice_config.id;


--
-- Name: group_customer_user; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE group_customer_user (
    user_id character varying(100) NOT NULL,
    group_id integer NOT NULL,
    permission_key character varying(20) NOT NULL,
    permission_value smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: group_role; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE group_role (
    role_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_key character varying(20) NOT NULL,
    permission_value smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: group_user; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE group_user (
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_key character varying(20) NOT NULL,
    permission_value smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: groups; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: link_object; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE link_object (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


--
-- Name: link_object_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE link_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: link_object_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE link_object_id_seq OWNED BY link_object.id;


--
-- Name: link_relation; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE link_relation (
    source_object_id smallint NOT NULL,
    source_key character varying(50) NOT NULL,
    target_object_id smallint NOT NULL,
    target_key character varying(50) NOT NULL,
    type_id smallint NOT NULL,
    state_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL
);


--
-- Name: link_state; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE link_state (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: link_state_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE link_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: link_state_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE link_state_id_seq OWNED BY link_state.id;


--
-- Name: link_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE link_type (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: link_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE link_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: link_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE link_type_id_seq OWNED BY link_type.id;


--
-- Name: mail_account; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE mail_account (
    id integer NOT NULL,
    login character varying(200) NOT NULL,
    pw character varying(200) NOT NULL,
    host character varying(200) NOT NULL,
    account_type character varying(20) NOT NULL,
    queue_id integer NOT NULL,
    trusted smallint NOT NULL,
    imap_folder character varying(250),
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: mail_account_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE mail_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mail_account_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE mail_account_id_seq OWNED BY mail_account.id;


--
-- Name: notification_event; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE notification_event (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    valid_id smallint NOT NULL,
    comments character varying(250),
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: notification_event_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE notification_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notification_event_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE notification_event_id_seq OWNED BY notification_event.id;


--
-- Name: notification_event_item; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE notification_event_item (
    notification_id integer NOT NULL,
    event_key character varying(200) NOT NULL,
    event_value character varying(200) NOT NULL
);


--
-- Name: notification_event_message; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE notification_event_message (
    id integer NOT NULL,
    notification_id integer NOT NULL,
    subject character varying(200) NOT NULL,
    text character varying(4000) NOT NULL,
    content_type character varying(250) NOT NULL,
    language character varying(60) NOT NULL
);


--
-- Name: notification_event_message_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE notification_event_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notification_event_message_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE notification_event_message_id_seq OWNED BY notification_event_message.id;


--
-- Name: package_repository; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE package_repository (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    version character varying(250) NOT NULL,
    vendor character varying(250) NOT NULL,
    install_status character varying(250) NOT NULL,
    filename character varying(250),
    content_type character varying(250),
    content text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: package_repository_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE package_repository_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: package_repository_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE package_repository_id_seq OWNED BY package_repository.id;


--
-- Name: personal_queues; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE personal_queues (
    user_id integer NOT NULL,
    queue_id integer NOT NULL
);


--
-- Name: personal_services; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE personal_services (
    user_id integer NOT NULL,
    service_id integer NOT NULL
);


--
-- Name: pm_activity; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE pm_activity (
    id integer NOT NULL,
    entity_id character varying(50) NOT NULL,
    name character varying(200) NOT NULL,
    config text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: pm_activity_dialog; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE pm_activity_dialog (
    id integer NOT NULL,
    entity_id character varying(50) NOT NULL,
    name character varying(200) NOT NULL,
    config text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: pm_activity_dialog_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE pm_activity_dialog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pm_activity_dialog_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE pm_activity_dialog_id_seq OWNED BY pm_activity_dialog.id;


--
-- Name: pm_activity_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE pm_activity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pm_activity_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE pm_activity_id_seq OWNED BY pm_activity.id;


--
-- Name: pm_entity_sync; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE pm_entity_sync (
    entity_type character varying(30) NOT NULL,
    entity_id character varying(50) NOT NULL,
    sync_state character varying(30) NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    change_time timestamp(0) without time zone NOT NULL
);


--
-- Name: pm_process; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE pm_process (
    id integer NOT NULL,
    entity_id character varying(50) NOT NULL,
    name character varying(200) NOT NULL,
    state_entity_id character varying(50) NOT NULL,
    layout text NOT NULL,
    config text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: pm_process_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE pm_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pm_process_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE pm_process_id_seq OWNED BY pm_process.id;


--
-- Name: pm_transition; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE pm_transition (
    id integer NOT NULL,
    entity_id character varying(50) NOT NULL,
    name character varying(200) NOT NULL,
    config text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: pm_transition_action; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE pm_transition_action (
    id integer NOT NULL,
    entity_id character varying(50) NOT NULL,
    name character varying(200) NOT NULL,
    config text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: pm_transition_action_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE pm_transition_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pm_transition_action_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE pm_transition_action_id_seq OWNED BY pm_transition_action.id;


--
-- Name: pm_transition_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE pm_transition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pm_transition_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE pm_transition_id_seq OWNED BY pm_transition.id;


--
-- Name: postmaster_filter; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE postmaster_filter (
    f_name character varying(200) NOT NULL,
    f_stop smallint,
    f_type character varying(20) NOT NULL,
    f_key character varying(200) NOT NULL,
    f_value character varying(200) NOT NULL,
    f_not smallint
);


--
-- Name: process_id; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE process_id (
    process_name character varying(200) NOT NULL,
    process_id character varying(200) NOT NULL,
    process_host character varying(200) NOT NULL,
    process_create integer NOT NULL,
    process_change integer NOT NULL
);


--
-- Name: queue; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE queue (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    group_id integer NOT NULL,
    unlock_timeout integer,
    first_response_time integer,
    first_response_notify smallint,
    update_time integer,
    update_notify smallint,
    solution_time integer,
    solution_notify smallint,
    system_address_id smallint NOT NULL,
    calendar_name character varying(100),
    default_sign_key character varying(100),
    salutation_id smallint NOT NULL,
    signature_id smallint NOT NULL,
    follow_up_id smallint NOT NULL,
    follow_up_lock smallint NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: queue_auto_response; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE queue_auto_response (
    id integer NOT NULL,
    queue_id integer NOT NULL,
    auto_response_id integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: queue_auto_response_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE queue_auto_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: queue_auto_response_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE queue_auto_response_id_seq OWNED BY queue_auto_response.id;


--
-- Name: queue_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: queue_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE queue_id_seq OWNED BY queue.id;


--
-- Name: queue_preferences; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE queue_preferences (
    queue_id integer NOT NULL,
    preferences_key character varying(150) NOT NULL,
    preferences_value character varying(250)
);


--
-- Name: queue_standard_template; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE queue_standard_template (
    queue_id integer NOT NULL,
    standard_template_id integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: role_user; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE role_user (
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: roles; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: salutation; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE salutation (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    text character varying(3000) NOT NULL,
    content_type character varying(250),
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: salutation_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE salutation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: salutation_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE salutation_id_seq OWNED BY salutation.id;


--
-- Name: scheduler_future_task; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE scheduler_future_task (
    id bigint NOT NULL,
    ident bigint NOT NULL,
    execution_time timestamp(0) without time zone NOT NULL,
    name character varying(150),
    task_type character varying(150) NOT NULL,
    task_data text NOT NULL,
    attempts smallint NOT NULL,
    lock_key bigint NOT NULL,
    lock_time timestamp(0) without time zone,
    create_time timestamp(0) without time zone NOT NULL
);


--
-- Name: scheduler_future_task_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE scheduler_future_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduler_future_task_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE scheduler_future_task_id_seq OWNED BY scheduler_future_task.id;


--
-- Name: scheduler_recurrent_task; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE scheduler_recurrent_task (
    id bigint NOT NULL,
    name character varying(150) NOT NULL,
    task_type character varying(150) NOT NULL,
    last_execution_time timestamp(0) without time zone NOT NULL,
    last_worker_task_id bigint,
    last_worker_status smallint,
    last_worker_running_time integer,
    lock_key bigint NOT NULL,
    lock_time timestamp(0) without time zone,
    create_time timestamp(0) without time zone NOT NULL,
    change_time timestamp(0) without time zone NOT NULL
);


--
-- Name: scheduler_recurrent_task_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE scheduler_recurrent_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduler_recurrent_task_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE scheduler_recurrent_task_id_seq OWNED BY scheduler_recurrent_task.id;


--
-- Name: scheduler_task; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE scheduler_task (
    id bigint NOT NULL,
    ident bigint NOT NULL,
    name character varying(150),
    task_type character varying(150) NOT NULL,
    task_data text NOT NULL,
    attempts smallint NOT NULL,
    lock_key bigint NOT NULL,
    lock_time timestamp(0) without time zone,
    lock_update_time timestamp(0) without time zone,
    create_time timestamp(0) without time zone NOT NULL
);


--
-- Name: scheduler_task_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE scheduler_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduler_task_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE scheduler_task_id_seq OWNED BY scheduler_task.id;


--
-- Name: search_profile; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE search_profile (
    login character varying(200) NOT NULL,
    profile_name character varying(200) NOT NULL,
    profile_type character varying(30) NOT NULL,
    profile_key character varying(200) NOT NULL,
    profile_value character varying(200)
);


--
-- Name: service; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE service (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    valid_id smallint NOT NULL,
    comments character varying(250),
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: service_customer_user; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE service_customer_user (
    customer_user_login character varying(200) NOT NULL,
    service_id integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL
);


--
-- Name: service_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE service_id_seq OWNED BY service.id;


--
-- Name: service_preferences; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE service_preferences (
    service_id integer NOT NULL,
    preferences_key character varying(150) NOT NULL,
    preferences_value character varying(250)
);


--
-- Name: service_sla; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE service_sla (
    service_id integer NOT NULL,
    sla_id integer NOT NULL
);


--
-- Name: sessions; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE sessions (
    id bigint NOT NULL,
    session_id character varying(100) NOT NULL,
    data_key character varying(100) NOT NULL,
    data_value character varying,
    serialized smallint NOT NULL
);


--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE sessions_id_seq OWNED BY sessions.id;


--
-- Name: signature; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE signature (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    text character varying(3000) NOT NULL,
    content_type character varying(250),
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: signature_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE signature_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: signature_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE signature_id_seq OWNED BY signature.id;


--
-- Name: sla; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE sla (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    calendar_name character varying(100),
    first_response_time integer NOT NULL,
    first_response_notify smallint,
    update_time integer NOT NULL,
    update_notify smallint,
    solution_time integer NOT NULL,
    solution_notify smallint,
    valid_id smallint NOT NULL,
    comments character varying(250),
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: sla_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE sla_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sla_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE sla_id_seq OWNED BY sla.id;


--
-- Name: sla_preferences; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE sla_preferences (
    sla_id integer NOT NULL,
    preferences_key character varying(150) NOT NULL,
    preferences_value character varying(250)
);


--
-- Name: smime_signer_cert_relations; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE smime_signer_cert_relations (
    id integer NOT NULL,
    cert_hash character varying(8) NOT NULL,
    cert_fingerprint character varying(59) NOT NULL,
    ca_hash character varying(8) NOT NULL,
    ca_fingerprint character varying(59) NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: smime_signer_cert_relations_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE smime_signer_cert_relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: smime_signer_cert_relations_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE smime_signer_cert_relations_id_seq OWNED BY smime_signer_cert_relations.id;


--
-- Name: standard_attachment; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE standard_attachment (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    content_type character varying(250) NOT NULL,
    content text NOT NULL,
    filename character varying(250) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: standard_attachment_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE standard_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: standard_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE standard_attachment_id_seq OWNED BY standard_attachment.id;


--
-- Name: standard_template; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE standard_template (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    text character varying,
    content_type character varying(250),
    template_type character varying(100) DEFAULT 'Answer'::character varying NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: standard_template_attachment; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE standard_template_attachment (
    id integer NOT NULL,
    standard_attachment_id integer NOT NULL,
    standard_template_id integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: standard_template_attachment_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE standard_template_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: standard_template_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE standard_template_attachment_id_seq OWNED BY standard_template_attachment.id;


--
-- Name: standard_template_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE standard_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: standard_template_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE standard_template_id_seq OWNED BY standard_template.id;


--
-- Name: system_address; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE system_address (
    id integer NOT NULL,
    value0 character varying(200) NOT NULL,
    value1 character varying(200) NOT NULL,
    value2 character varying(200),
    value3 character varying(200),
    queue_id integer NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: system_address_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE system_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_address_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE system_address_id_seq OWNED BY system_address.id;


--
-- Name: system_data; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE system_data (
    data_key character varying(160) NOT NULL,
    data_value text,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: system_maintenance; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE system_maintenance (
    id integer NOT NULL,
    start_date integer NOT NULL,
    stop_date integer NOT NULL,
    comments character varying(250) NOT NULL,
    login_message character varying(250),
    show_login_message smallint,
    notify_message character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: system_maintenance_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE system_maintenance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: system_maintenance_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE system_maintenance_id_seq OWNED BY system_maintenance.id;


--
-- Name: ticket; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket (
    id bigint NOT NULL,
    tn character varying(50) NOT NULL,
    title character varying(255),
    queue_id integer NOT NULL,
    ticket_lock_id smallint NOT NULL,
    type_id smallint,
    service_id integer,
    sla_id integer,
    user_id integer NOT NULL,
    responsible_user_id integer NOT NULL,
    ticket_priority_id smallint NOT NULL,
    ticket_state_id smallint NOT NULL,
    customer_id character varying(150),
    customer_user_id character varying(250),
    timeout integer NOT NULL,
    until_time integer NOT NULL,
    escalation_time integer NOT NULL,
    escalation_update_time integer NOT NULL,
    escalation_response_time integer NOT NULL,
    escalation_solution_time integer NOT NULL,
    archive_flag smallint DEFAULT 0 NOT NULL,
    create_time_unix bigint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_flag; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_flag (
    ticket_id bigint NOT NULL,
    ticket_key character varying(50) NOT NULL,
    ticket_value character varying(50),
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL
);


--
-- Name: ticket_history; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_history (
    id bigint NOT NULL,
    name character varying(200) NOT NULL,
    history_type_id smallint NOT NULL,
    ticket_id bigint NOT NULL,
    article_id bigint,
    type_id smallint NOT NULL,
    queue_id integer NOT NULL,
    owner_id integer NOT NULL,
    priority_id smallint NOT NULL,
    state_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_history_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_history_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_history_id_seq OWNED BY ticket_history.id;


--
-- Name: ticket_history_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_history_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_history_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_history_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_history_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_history_type_id_seq OWNED BY ticket_history_type.id;


--
-- Name: ticket_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_id_seq OWNED BY ticket.id;


--
-- Name: ticket_index; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_index (
    ticket_id bigint NOT NULL,
    queue_id integer NOT NULL,
    queue character varying(200) NOT NULL,
    group_id integer NOT NULL,
    s_lock character varying(200) NOT NULL,
    s_state character varying(200) NOT NULL,
    create_time_unix bigint NOT NULL
);


--
-- Name: ticket_lock_index; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_lock_index (
    ticket_id bigint NOT NULL
);


--
-- Name: ticket_lock_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_lock_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_lock_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_lock_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_lock_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_lock_type_id_seq OWNED BY ticket_lock_type.id;


--
-- Name: ticket_loop_protection; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_loop_protection (
    sent_to character varying(250) NOT NULL,
    sent_date character varying(150) NOT NULL
);


--
-- Name: ticket_priority; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_priority (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_priority_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_priority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_priority_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_priority_id_seq OWNED BY ticket_priority.id;


--
-- Name: ticket_state; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_state (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    type_id smallint NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_state_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_state_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_state_id_seq OWNED BY ticket_state.id;


--
-- Name: ticket_state_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_state_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    comments character varying(250),
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_state_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_state_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_state_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_state_type_id_seq OWNED BY ticket_state_type.id;


--
-- Name: ticket_type; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_type (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: ticket_type_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE ticket_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ticket_type_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE ticket_type_id_seq OWNED BY ticket_type.id;


--
-- Name: ticket_watcher; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE ticket_watcher (
    ticket_id bigint NOT NULL,
    user_id integer NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: time_accounting; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE time_accounting (
    id bigint NOT NULL,
    ticket_id bigint NOT NULL,
    article_id bigint,
    time_unit numeric(10,2) NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: time_accounting_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE time_accounting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: time_accounting_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE time_accounting_id_seq OWNED BY time_accounting.id;


--
-- Name: user_preferences; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE user_preferences (
    user_id integer NOT NULL,
    preferences_key character varying(150) NOT NULL,
    preferences_value text
);


--
-- Name: users; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    login character varying(200) NOT NULL,
    pw character varying(64) NOT NULL,
    title character varying(50),
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    valid_id smallint NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: valid; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE valid (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    create_time timestamp(0) without time zone NOT NULL,
    create_by integer NOT NULL,
    change_time timestamp(0) without time zone NOT NULL,
    change_by integer NOT NULL
);


--
-- Name: valid_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE valid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: valid_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE valid_id_seq OWNED BY valid.id;


--
-- Name: virtual_fs; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE virtual_fs (
    id bigint NOT NULL,
    filename character varying(350) NOT NULL,
    backend character varying(60) NOT NULL,
    backend_key character varying(160) NOT NULL,
    create_time timestamp(0) without time zone NOT NULL
);


--
-- Name: virtual_fs_db; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE virtual_fs_db (
    id bigint NOT NULL,
    filename character varying(350) NOT NULL,
    content text NOT NULL,
    create_time timestamp(0) without time zone NOT NULL
);


--
-- Name: virtual_fs_db_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE virtual_fs_db_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: virtual_fs_db_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE virtual_fs_db_id_seq OWNED BY virtual_fs_db.id;


--
-- Name: virtual_fs_id_seq; Type: SEQUENCE; Schema: otrs; Owner: -
--

CREATE SEQUENCE virtual_fs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: virtual_fs_id_seq; Type: SEQUENCE OWNED BY; Schema: otrs; Owner: -
--

ALTER SEQUENCE virtual_fs_id_seq OWNED BY virtual_fs.id;


--
-- Name: virtual_fs_preferences; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE virtual_fs_preferences (
    virtual_fs_id bigint NOT NULL,
    preferences_key character varying(150) NOT NULL,
    preferences_value character varying(350)
);


--
-- Name: web_upload_cache; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE web_upload_cache (
    form_id character varying(250),
    filename character varying(250),
    content_id character varying(250),
    content_size character varying(30),
    content_type character varying(250),
    disposition character varying(15),
    content text NOT NULL,
    create_time_unix bigint NOT NULL
);


--
-- Name: xml_storage; Type: TABLE; Schema: otrs; Owner: -
--

CREATE TABLE xml_storage (
    xml_type character varying(200) NOT NULL,
    xml_key character varying(250) NOT NULL,
    xml_content_key character varying(250) NOT NULL,
    xml_content_value character varying
);


SET search_path = public, pg_catalog;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_message (
    id integer NOT NULL,
    user_id integer NOT NULL,
    message text NOT NULL
);


--
-- Name: auth_message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_message_id_seq OWNED BY auth_message.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    password character varying(128) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    is_superuser boolean NOT NULL,
    last_login timestamp without time zone NOT NULL,
    date_joined timestamp without time zone NOT NULL
);


--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: btns; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE btns (
    id integer NOT NULL,
    billingtelephonenumber character varying,
    client_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    start_date date DEFAULT '1970-01-01'::date,
    end_date date,
    rate_plan_id integer,
    lastresort boolean DEFAULT false
);


--
-- Name: btns_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE btns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: btns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE btns_id_seq OWNED BY btns.id;


--
-- Name: buildings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE buildings (
    id integer NOT NULL,
    address1 character varying,
    address2 character varying,
    city character varying,
    province character varying,
    country character varying,
    postalcode character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: buildings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE buildings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: buildings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE buildings_id_seq OWNED BY buildings.id;


--
-- Name: calls; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE calls (
    id integer NOT NULL,
    master_callid character varying(40),
    call_type character varying(16),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    startdatetime timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone,
    answerdatetime timestamp without time zone DEFAULT '1970-01-01 00:00:00'::timestamp without time zone,
    releasedatetime timestamp without time zone,
    datasource_id integer,
    our_number character varying,
    their_number character varying,
    terminated boolean,
    client_id integer,
    transfertovoicemail boolean,
    duration integer,
    viewable_legs integer,
    no_extension_picked_up boolean,
    abandoned_call boolean,
    type character varying DEFAULT 'Call'::character varying
);


--
-- Name: calls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE calls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: calls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE calls_id_seq OWNED BY calls.id;


--
-- Name: callstats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE callstats (
    id integer NOT NULL,
    client_id integer,
    type character varying,
    value integer,
    stats_period_id integer,
    phone_id integer
);


--
-- Name: callstats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE callstats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: callstats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE callstats_id_seq OWNED BY callstats.id;


--
-- Name: carriers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE carriers (
    id integer NOT NULL,
    name character varying,
    coverage character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: carriers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE carriers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: carriers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE carriers_id_seq OWNED BY carriers.id;


--
-- Name: cdrviewer_areacode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cdrviewer_areacode (
    id integer NOT NULL,
    npa character varying(3) NOT NULL,
    country character varying(16) NOT NULL,
    description character varying(64) NOT NULL
);


--
-- Name: cdrviewer_areacode_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cdrviewer_areacode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cdrviewer_areacode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cdrviewer_areacode_id_seq OWNED BY cdrviewer_areacode.id;


--
-- Name: cdrviewer_cdr; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cdrviewer_cdr (
    id integer NOT NULL,
    calltype character varying(13),
    direction character varying(11),
    callednumber character varying(161),
    starttime numeric(19,3),
    startdatetime timestamp without time zone,
    newstarttime time without time zone,
    recordid character varying(48),
    serviceprovider character varying(30),
    usernumber character varying(16),
    groupnumber character varying(16),
    callingnumber character varying(161),
    callingpresentationindicator character varying(20),
    usertimezone character varying(11),
    answerindicator character varying(19),
    answertime character varying(18),
    terminationcause character varying(3),
    carrieridentificationcode character varying(4),
    dialeddigits character varying(161),
    callcategory character varying(8),
    networkcalltype character varying(4),
    networktranslatednumber character varying(161),
    networktranslatedgroup character varying(32),
    releasingparty character varying(6),
    route character varying(86),
    networkcallid character varying(161),
    codec character varying(30),
    accessdeviceaddress character varying(80),
    accesscallid character varying(161),
    spare1 character varying(128),
    failovercorrelationid character varying(161),
    spare2 character varying(128),
    "group" character varying(30),
    department character varying,
    accountcode character varying(14),
    authorizationcode character varying(14),
    originalcallednumber character varying(161),
    originalcalledpresentationindicator character varying(20),
    originalcalledreason character varying(40),
    redirectingnumber character varying(161),
    redirectingreason character varying(40),
    chargeindicator character varying(1),
    typeofnetwork character varying(7),
    voiceportalcalling_invtime character varying(18),
    localcallid character varying(40),
    remotecallid character varying(40),
    callingpartycategory character varying(20),
    releasetime numeric(19,3),
    releasedatetime timestamp without time zone,
    newreleasetime time without time zone,
    networktype character varying(4),
    redirectingpresentationindicator character varying(20),
    instantconference_to character varying(161),
    instantconference_from character varying(161),
    instantconference_conferenceid character varying(128),
    instantconference_role character varying(12),
    instantconference_bridge character varying(128),
    instantconference_owner character varying(161),
    instantconference_ownerdn character varying(32),
    instantconference_title character varying(80),
    instantconference_projectcode character varying(40),
    key character varying(161),
    creator character varying(80),
    originatornetwork character varying(80),
    terminatornetwork character varying(80),
    userid character varying(161),
    otherpartyname character varying(80),
    otherpartynamepresentationindicator character varying(20),
    hoteling_group character varying(30),
    hoteling_userid character varying(161),
    hoteling_usernumber character varying(15),
    hoteling_groupnumber character varying(15),
    trunkgroupname character varying,
    clidpermitted character varying(3),
    accessnetworkinfo character varying(1024),
    relatedcallid character varying(40),
    relatedcallidreason character varying(40),
    transfer_relatedcallid character varying(161),
    transfer_type character varying(20),
    conference_starttime character varying(18),
    conference_stoptime character varying(18),
    conference_confid character varying(40),
    conference_type character varying(10),
    faxmessaging character varying(9),
    trunkgroupinfo character varying,
    recalltype character varying(20),
    q850cause character varying(3),
    dialeddigitscontext character varying(161),
    callednumbercontext character varying(161),
    networktranslatednumbercontext character varying(161),
    callingnumbercontext character varying(161),
    originalcallednumbercontext character varying(161),
    redirectingnumbercontext character varying(161),
    routingnumber character varying,
    originationmethod character varying(30),
    broadworksanywhere_relatedcallid character varying(40),
    calledassertedidentity character varying(161),
    calledassertedpresentationindicator character varying(20),
    sdp character varying(1024),
    ascalltype character varying(11),
    cbfauthorizationcode character varying(14),
    callbridge_callbridgeresult character varying(7),
    prepaidstatus character varying(22),
    configurableclid character varying(20),
    instantconference_invtime character varying(18),
    instantconference_callid character varying(161),
    instantconference_recording_duration character varying(10),
    answerdatetime timestamp without time zone,
    newanswertime time without time zone,
    datasource_id integer,
    lineno integer,
    usagetype character varying(3),
    productid character varying(161),
    originateportedflag boolean,
    originatenumber character varying(161),
    destinationportedflag boolean,
    destinationnumber character varying(161),
    forwardedcallingnumber character varying(161),
    originatelrn character varying(161),
    destinationlrn character varying(161),
    duration integer,
    callingphone_id integer,
    calledphone_id integer,
    callingbtn_id integer,
    calledbtn_id integer,
    call_id integer
);


--
-- Name: cdrviewer_cdr_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cdrviewer_cdr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cdrviewer_cdr_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cdrviewer_cdr_id_seq OWNED BY cdrviewer_cdr.id;


--
-- Name: cdrviewer_dailypeak; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cdrviewer_dailypeak (
    id integer NOT NULL,
    date date DEFAULT '1970-01-01'::date,
    midnight_calls integer DEFAULT 0 NOT NULL,
    peak_calls integer DEFAULT 0 NOT NULL
);


--
-- Name: cdrviewer_dailypeak_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cdrviewer_dailypeak_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cdrviewer_dailypeak_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cdrviewer_dailypeak_id_seq OWNED BY cdrviewer_dailypeak.id;


--
-- Name: cdrviewer_datasource; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cdrviewer_datasource (
    id integer NOT NULL,
    datasource_filename character varying(256) NOT NULL,
    analysis_level integer DEFAULT 0,
    active_day integer DEFAULT 0,
    cdrs_count integer DEFAULT 0,
    calls_count integer DEFAULT 0,
    load_time integer
);


--
-- Name: cdrviewer_datasource_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cdrviewer_datasource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cdrviewer_datasource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cdrviewer_datasource_id_seq OWNED BY cdrviewer_datasource.id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE clients (
    id integer NOT NULL,
    billing_site_id integer,
    admin_contact character varying,
    technical_contact character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    description character varying,
    logo character varying,
    url character varying,
    foreign_pid integer,
    billing_btn text,
    billing_cycle_id integer,
    started_at timestamp without time zone,
    terminated_at timestamp without time zone,
    language_preference text,
    crm_account_id character varying
);


--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- Name: crm_accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE crm_accounts (
    id integer NOT NULL,
    name text,
    date_entered timestamp without time zone,
    date_modified timestamp without time zone,
    modified_user_id text,
    created_by text,
    description text,
    deleted integer DEFAULT 0,
    assigned_user_id text,
    account_type text,
    industry text,
    annual_revenue text,
    phone_fax text,
    billing_address_street text,
    billing_address_city text,
    billing_address_state text,
    billing_address_postalcode text,
    billing_address_country text,
    rating text,
    phone_office text,
    phone_alternate text,
    website text,
    ownership text,
    employees text,
    ticker_symbol text,
    shipping_address_street text,
    shipping_address_city text,
    shipping_address_state text,
    shipping_address_postalcode text,
    shipping_address_country text,
    parent_id text,
    sic_code text,
    campaign_id text
);


--
-- Name: crm_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE crm_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: crm_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE crm_accounts_id_seq OWNED BY crm_accounts.id;


--
-- Name: digitalsignage_custommessage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE digitalsignage_custommessage (
    id integer NOT NULL,
    client_id integer,
    custom_message character varying(256),
    date_created date,
    datetime_start timestamp without time zone,
    datetime_end timestamp without time zone,
    server character varying,
    macaddr character varying
);


--
-- Name: digitalsignage_custommessage_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE digitalsignage_custommessage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: digitalsignage_custommessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE digitalsignage_custommessage_id_seq OWNED BY digitalsignage_custommessage.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp without time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL
);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_session (
    id integer NOT NULL,
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp without time zone NOT NULL
);


--
-- Name: django_session_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_session_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_session_id_seq OWNED BY django_session.id;


--
-- Name: django_site; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: exchanges_manual_matches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE exchanges_manual_matches (
    id integer NOT NULL,
    province character varying(5),
    localname character varying(40),
    lerg6name character varying(10)
);


--
-- Name: exchanges_manual_matches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE exchanges_manual_matches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: exchanges_manual_matches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE exchanges_manual_matches_id_seq OWNED BY exchanges_manual_matches.id;


--
-- Name: lavenshtein_exchanges; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE lavenshtein_exchanges (
    id integer NOT NULL,
    province character varying(255),
    localname character varying(255),
    lergname character varying(255),
    lev integer
);


--
-- Name: lavenshtein_exchanges_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lavenshtein_exchanges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lavenshtein_exchanges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lavenshtein_exchanges_id_seq OWNED BY lavenshtein_exchanges.id;


--
-- Name: lerg8s; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE lerg8s (
    id integer NOT NULL,
    "shortName" character varying,
    "fullName" character varying,
    province character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: lerg8s_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lerg8s_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lerg8s_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lerg8s_id_seq OWNED BY lerg8s.id;


--
-- Name: local_exchanges; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE local_exchanges (
    id integer NOT NULL,
    province character varying,
    name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    carrier character varying,
    carrierplan_id integer
);


--
-- Name: local_exchanges_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE local_exchanges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: local_exchanges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE local_exchanges_id_seq OWNED BY local_exchanges.id;


--
-- Name: local_exchanges_north_american_exchanges; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE local_exchanges_north_american_exchanges (
    id integer NOT NULL,
    local_exchanges_id integer,
    north_american_exchanges_id integer
);


--
-- Name: local_exchanges_north_american_exchanges_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE local_exchanges_north_american_exchanges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: local_exchanges_north_american_exchanges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE local_exchanges_north_american_exchanges_id_seq OWNED BY local_exchanges_north_american_exchanges.id;


--
-- Name: north_american_exchanges; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE north_american_exchanges (
    id integer NOT NULL,
    province character varying,
    name character varying,
    npa integer,
    nxx integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: north_american_exchanges_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE north_american_exchanges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: north_american_exchanges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE north_american_exchanges_id_seq OWNED BY north_american_exchanges.id;


--
-- Name: phones; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE phones (
    id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    site_id integer,
    btn_id integer,
    phone_number character varying,
    extension character varying,
    voicemail_number character varying,
    access_info character varying,
    notes text,
    activation_date date,
    termination_date date,
    phone_mac_addr character varying,
    phone_type_id integer,
    sip_username character varying,
    e911_notes character varying,
    e911_tested boolean,
    e911_tested_by_id integer,
    e911_tested_date date,
    voip_supplier_id integer,
    email character varying,
    ipaddress character varying,
    connectiontype_id character varying,
    sip_password text,
    virtualdid boolean DEFAULT false,
    voicemailportal boolean DEFAULT false,
    callerid character varying,
    autocreate boolean DEFAULT false,
    huntgroup boolean DEFAULT false
);


--
-- Name: phones_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE phones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: phones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE phones_id_seq OWNED BY phones.id;


--
-- Name: portal_customerservice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE portal_customerservice (
    id integer NOT NULL,
    service_id integer NOT NULL,
    customer_id integer NOT NULL,
    startdate timestamp without time zone NOT NULL,
    enddate timestamp without time zone
);


--
-- Name: portal_customerservice_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE portal_customerservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: portal_customerservice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE portal_customerservice_id_seq OWNED BY portal_customerservice.id;


--
-- Name: portal_service; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE portal_service (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    description text NOT NULL,
    url character varying(32) DEFAULT '/foo/'::character varying NOT NULL,
    icon character varying(64) DEFAULT ''::character varying NOT NULL
);


--
-- Name: portal_service_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE portal_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: portal_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE portal_service_id_seq OWNED BY portal_service.id;


--
-- Name: portal_userprofile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE portal_userprofile (
    id integer NOT NULL,
    user_id integer NOT NULL,
    logo character varying(256)
);


--
-- Name: portal_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE portal_userprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: portal_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE portal_userprofile_id_seq OWNED BY portal_userprofile.id;


--
-- Name: portal_userprofilecustomer; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE portal_userprofilecustomer (
    id integer NOT NULL,
    user_profile_id integer NOT NULL,
    customer_id integer NOT NULL,
    startdate timestamp without time zone NOT NULL,
    enddate timestamp without time zone
);


--
-- Name: portal_userprofilecustomer_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE portal_userprofilecustomer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: portal_userprofilecustomer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE portal_userprofilecustomer_id_seq OWNED BY portal_userprofilecustomer.id;


--
-- Name: privateradio_channel; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE privateradio_channel (
    id integer NOT NULL,
    name character varying(32) NOT NULL
);


--
-- Name: privateradio_channel_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE privateradio_channel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: privateradio_channel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE privateradio_channel_id_seq OWNED BY privateradio_channel.id;


--
-- Name: privateradio_channelschedule; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE privateradio_channelschedule (
    id integer NOT NULL,
    channel_id integer NOT NULL,
    customer_id integer NOT NULL,
    daysun boolean DEFAULT false NOT NULL,
    daymon boolean DEFAULT false NOT NULL,
    daytue boolean DEFAULT false NOT NULL,
    daywed boolean DEFAULT false NOT NULL,
    daythu boolean DEFAULT false NOT NULL,
    dayfri boolean DEFAULT false NOT NULL,
    daysat boolean DEFAULT false NOT NULL,
    start_time time without time zone NOT NULL,
    end_time time without time zone NOT NULL,
    start_date date NOT NULL,
    end_date date,
    override boolean NOT NULL,
    pctg_english integer NOT NULL,
    pctg_channel integer NOT NULL
);


--
-- Name: privateradio_channelschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE privateradio_channelschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: privateradio_channelschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE privateradio_channelschedule_id_seq OWNED BY privateradio_channelschedule.id;


--
-- Name: privateradio_messageorder; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE privateradio_messageorder (
    id integer NOT NULL,
    user_id integer NOT NULL,
    customer_id integer NOT NULL,
    title text NOT NULL,
    text text,
    voice character varying(64),
    language character varying(32),
    description text,
    need_text_creation boolean DEFAULT false NOT NULL,
    need_text_editing boolean DEFAULT false NOT NULL,
    need_production boolean DEFAULT false NOT NULL,
    date_created date NOT NULL,
    date_updated date NOT NULL,
    bo_date_approved date,
    ho_date_approved date
);


--
-- Name: privateradio_messageorder_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE privateradio_messageorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: privateradio_messageorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE privateradio_messageorder_id_seq OWNED BY privateradio_messageorder.id;


--
-- Name: privateradio_workorder; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE privateradio_workorder (
    id integer NOT NULL,
    messageorder_id integer NOT NULL,
    date_needed date,
    date_created date NOT NULL,
    date_approved date,
    date_scheduled date,
    date_produced date,
    date_accepted date,
    date_updated date,
    description character varying(128),
    novavision_notes text,
    accept_notes text
);


--
-- Name: privateradio_workorder_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE privateradio_workorder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: privateradio_workorder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE privateradio_workorder_id_seq OWNED BY privateradio_workorder.id;


--
-- Name: rate_plans; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE rate_plans (
    id integer NOT NULL,
    province character varying,
    country character varying,
    name character varying,
    description character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: rate_plans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE rate_plans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rate_plans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE rate_plans_id_seq OWNED BY rate_plans.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: sensors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sensors (
    id integer NOT NULL,
    name character varying,
    inner_ipv4 character varying,
    inner_ipv6 character varying,
    outer_ipv4 character varying,
    outer_ipv6 character varying,
    macaddress character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: sensors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sensors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sensors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sensors_id_seq OWNED BY sensors.id;


--
-- Name: sites; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sites (
    id integer NOT NULL,
    name character varying,
    unitnumber character varying,
    building_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    client_id integer
);


--
-- Name: sites_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sites_id_seq OWNED BY sites.id;


--
-- Name: stats_periods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE stats_periods (
    id integer NOT NULL,
    day date,
    hour integer,
    type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: stats_periods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stats_periods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stats_periods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stats_periods_id_seq OWNED BY stats_periods.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    class_id integer,
    phone character varying,
    fullname character varying,
    username character varying,
    agent boolean,
    admin boolean,
    reset_password_sent_at timestamp without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


SET search_path = otrs, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY acl ALTER COLUMN id SET DEFAULT nextval('acl_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article ALTER COLUMN id SET DEFAULT nextval('article_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_attachment ALTER COLUMN id SET DEFAULT nextval('article_attachment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_plain ALTER COLUMN id SET DEFAULT nextval('article_plain_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_sender_type ALTER COLUMN id SET DEFAULT nextval('article_sender_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_type ALTER COLUMN id SET DEFAULT nextval('article_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response ALTER COLUMN id SET DEFAULT nextval('auto_response_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response_type ALTER COLUMN id SET DEFAULT nextval('auto_response_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY cloud_service_config ALTER COLUMN id SET DEFAULT nextval('cloud_service_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY customer_user ALTER COLUMN id SET DEFAULT nextval('customer_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field ALTER COLUMN id SET DEFAULT nextval('dynamic_field_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field_value ALTER COLUMN id SET DEFAULT nextval('dynamic_field_value_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY follow_up_possible ALTER COLUMN id SET DEFAULT nextval('follow_up_possible_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_debugger_entry ALTER COLUMN id SET DEFAULT nextval('gi_debugger_entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_debugger_entry_content ALTER COLUMN id SET DEFAULT nextval('gi_debugger_entry_content_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config ALTER COLUMN id SET DEFAULT nextval('gi_webservice_config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config_history ALTER COLUMN id SET DEFAULT nextval('gi_webservice_config_history_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_object ALTER COLUMN id SET DEFAULT nextval('link_object_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_state ALTER COLUMN id SET DEFAULT nextval('link_state_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_type ALTER COLUMN id SET DEFAULT nextval('link_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY mail_account ALTER COLUMN id SET DEFAULT nextval('mail_account_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event ALTER COLUMN id SET DEFAULT nextval('notification_event_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event_message ALTER COLUMN id SET DEFAULT nextval('notification_event_message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY package_repository ALTER COLUMN id SET DEFAULT nextval('package_repository_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity ALTER COLUMN id SET DEFAULT nextval('pm_activity_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity_dialog ALTER COLUMN id SET DEFAULT nextval('pm_activity_dialog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_process ALTER COLUMN id SET DEFAULT nextval('pm_process_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition ALTER COLUMN id SET DEFAULT nextval('pm_transition_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition_action ALTER COLUMN id SET DEFAULT nextval('pm_transition_action_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue ALTER COLUMN id SET DEFAULT nextval('queue_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_auto_response ALTER COLUMN id SET DEFAULT nextval('queue_auto_response_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY salutation ALTER COLUMN id SET DEFAULT nextval('salutation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_future_task ALTER COLUMN id SET DEFAULT nextval('scheduler_future_task_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_recurrent_task ALTER COLUMN id SET DEFAULT nextval('scheduler_recurrent_task_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_task ALTER COLUMN id SET DEFAULT nextval('scheduler_task_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service ALTER COLUMN id SET DEFAULT nextval('service_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sessions ALTER COLUMN id SET DEFAULT nextval('sessions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY signature ALTER COLUMN id SET DEFAULT nextval('signature_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sla ALTER COLUMN id SET DEFAULT nextval('sla_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY smime_signer_cert_relations ALTER COLUMN id SET DEFAULT nextval('smime_signer_cert_relations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_attachment ALTER COLUMN id SET DEFAULT nextval('standard_attachment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template ALTER COLUMN id SET DEFAULT nextval('standard_template_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template_attachment ALTER COLUMN id SET DEFAULT nextval('standard_template_attachment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_address ALTER COLUMN id SET DEFAULT nextval('system_address_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_maintenance ALTER COLUMN id SET DEFAULT nextval('system_maintenance_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket ALTER COLUMN id SET DEFAULT nextval('ticket_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history ALTER COLUMN id SET DEFAULT nextval('ticket_history_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history_type ALTER COLUMN id SET DEFAULT nextval('ticket_history_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_lock_type ALTER COLUMN id SET DEFAULT nextval('ticket_lock_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_priority ALTER COLUMN id SET DEFAULT nextval('ticket_priority_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state ALTER COLUMN id SET DEFAULT nextval('ticket_state_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state_type ALTER COLUMN id SET DEFAULT nextval('ticket_state_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_type ALTER COLUMN id SET DEFAULT nextval('ticket_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY time_accounting ALTER COLUMN id SET DEFAULT nextval('time_accounting_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY valid ALTER COLUMN id SET DEFAULT nextval('valid_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY virtual_fs ALTER COLUMN id SET DEFAULT nextval('virtual_fs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY virtual_fs_db ALTER COLUMN id SET DEFAULT nextval('virtual_fs_db_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_message ALTER COLUMN id SET DEFAULT nextval('auth_message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY btns ALTER COLUMN id SET DEFAULT nextval('btns_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY buildings ALTER COLUMN id SET DEFAULT nextval('buildings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY calls ALTER COLUMN id SET DEFAULT nextval('calls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats ALTER COLUMN id SET DEFAULT nextval('callstats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY carriers ALTER COLUMN id SET DEFAULT nextval('carriers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_areacode ALTER COLUMN id SET DEFAULT nextval('cdrviewer_areacode_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr ALTER COLUMN id SET DEFAULT nextval('cdrviewer_cdr_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_dailypeak ALTER COLUMN id SET DEFAULT nextval('cdrviewer_dailypeak_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_datasource ALTER COLUMN id SET DEFAULT nextval('cdrviewer_datasource_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY crm_accounts ALTER COLUMN id SET DEFAULT nextval('crm_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY digitalsignage_custommessage ALTER COLUMN id SET DEFAULT nextval('digitalsignage_custommessage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_session ALTER COLUMN id SET DEFAULT nextval('django_session_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY exchanges_manual_matches ALTER COLUMN id SET DEFAULT nextval('exchanges_manual_matches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lavenshtein_exchanges ALTER COLUMN id SET DEFAULT nextval('lavenshtein_exchanges_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lerg8s ALTER COLUMN id SET DEFAULT nextval('lerg8s_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_exchanges ALTER COLUMN id SET DEFAULT nextval('local_exchanges_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_exchanges_north_american_exchanges ALTER COLUMN id SET DEFAULT nextval('local_exchanges_north_american_exchanges_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY north_american_exchanges ALTER COLUMN id SET DEFAULT nextval('north_american_exchanges_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY phones ALTER COLUMN id SET DEFAULT nextval('phones_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_customerservice ALTER COLUMN id SET DEFAULT nextval('portal_customerservice_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_service ALTER COLUMN id SET DEFAULT nextval('portal_service_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_userprofile ALTER COLUMN id SET DEFAULT nextval('portal_userprofile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_userprofilecustomer ALTER COLUMN id SET DEFAULT nextval('portal_userprofilecustomer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_channel ALTER COLUMN id SET DEFAULT nextval('privateradio_channel_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_channelschedule ALTER COLUMN id SET DEFAULT nextval('privateradio_channelschedule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_messageorder ALTER COLUMN id SET DEFAULT nextval('privateradio_messageorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_workorder ALTER COLUMN id SET DEFAULT nextval('privateradio_workorder_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rate_plans ALTER COLUMN id SET DEFAULT nextval('rate_plans_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sensors ALTER COLUMN id SET DEFAULT nextval('sensors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sites ALTER COLUMN id SET DEFAULT nextval('sites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stats_periods ALTER COLUMN id SET DEFAULT nextval('stats_periods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


SET search_path = otrs, pg_catalog;

--
-- Name: acl_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY acl
    ADD CONSTRAINT acl_name UNIQUE (name);


--
-- Name: acl_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY acl
    ADD CONSTRAINT acl_pkey PRIMARY KEY (id);


--
-- Name: article_attachment_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_attachment
    ADD CONSTRAINT article_attachment_pkey PRIMARY KEY (id);


--
-- Name: article_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article
    ADD CONSTRAINT article_pkey PRIMARY KEY (id);


--
-- Name: article_plain_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_plain
    ADD CONSTRAINT article_plain_pkey PRIMARY KEY (id);


--
-- Name: article_search_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_search
    ADD CONSTRAINT article_search_pkey PRIMARY KEY (id);


--
-- Name: article_sender_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_sender_type
    ADD CONSTRAINT article_sender_type_name UNIQUE (name);


--
-- Name: article_sender_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_sender_type
    ADD CONSTRAINT article_sender_type_pkey PRIMARY KEY (id);


--
-- Name: article_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_type
    ADD CONSTRAINT article_type_name UNIQUE (name);


--
-- Name: article_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_type
    ADD CONSTRAINT article_type_pkey PRIMARY KEY (id);


--
-- Name: auto_response_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response
    ADD CONSTRAINT auto_response_name UNIQUE (name);


--
-- Name: auto_response_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response
    ADD CONSTRAINT auto_response_pkey PRIMARY KEY (id);


--
-- Name: auto_response_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response_type
    ADD CONSTRAINT auto_response_type_name UNIQUE (name);


--
-- Name: auto_response_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response_type
    ADD CONSTRAINT auto_response_type_pkey PRIMARY KEY (id);


--
-- Name: cloud_service_config_config_md5; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY cloud_service_config
    ADD CONSTRAINT cloud_service_config_config_md5 UNIQUE (config_md5);


--
-- Name: cloud_service_config_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY cloud_service_config
    ADD CONSTRAINT cloud_service_config_name UNIQUE (name);


--
-- Name: cloud_service_config_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY cloud_service_config
    ADD CONSTRAINT cloud_service_config_pkey PRIMARY KEY (id);


--
-- Name: customer_user_login; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY customer_user
    ADD CONSTRAINT customer_user_login UNIQUE (login);


--
-- Name: customer_user_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY customer_user
    ADD CONSTRAINT customer_user_pkey PRIMARY KEY (id);


--
-- Name: dynamic_field_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field
    ADD CONSTRAINT dynamic_field_name UNIQUE (name);


--
-- Name: dynamic_field_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field
    ADD CONSTRAINT dynamic_field_pkey PRIMARY KEY (id);


--
-- Name: dynamic_field_value_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field_value
    ADD CONSTRAINT dynamic_field_value_pkey PRIMARY KEY (id);


--
-- Name: follow_up_possible_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY follow_up_possible
    ADD CONSTRAINT follow_up_possible_name UNIQUE (name);


--
-- Name: follow_up_possible_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY follow_up_possible
    ADD CONSTRAINT follow_up_possible_pkey PRIMARY KEY (id);


--
-- Name: gi_debugger_entry_communication_id; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_debugger_entry
    ADD CONSTRAINT gi_debugger_entry_communication_id UNIQUE (communication_id);


--
-- Name: gi_debugger_entry_content_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_debugger_entry_content
    ADD CONSTRAINT gi_debugger_entry_content_pkey PRIMARY KEY (id);


--
-- Name: gi_debugger_entry_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_debugger_entry
    ADD CONSTRAINT gi_debugger_entry_pkey PRIMARY KEY (id);


--
-- Name: gi_object_lock_state_list; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_object_lock_state
    ADD CONSTRAINT gi_object_lock_state_list UNIQUE (webservice_id, object_type, object_id);


--
-- Name: gi_webservice_config_config_md5; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config
    ADD CONSTRAINT gi_webservice_config_config_md5 UNIQUE (config_md5);


--
-- Name: gi_webservice_config_history_config_md5; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config_history
    ADD CONSTRAINT gi_webservice_config_history_config_md5 UNIQUE (config_md5);


--
-- Name: gi_webservice_config_history_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config_history
    ADD CONSTRAINT gi_webservice_config_history_pkey PRIMARY KEY (id);


--
-- Name: gi_webservice_config_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config
    ADD CONSTRAINT gi_webservice_config_name UNIQUE (name);


--
-- Name: gi_webservice_config_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config
    ADD CONSTRAINT gi_webservice_config_pkey PRIMARY KEY (id);


--
-- Name: groups_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_name UNIQUE (name);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: link_object_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_object
    ADD CONSTRAINT link_object_name UNIQUE (name);


--
-- Name: link_object_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_object
    ADD CONSTRAINT link_object_pkey PRIMARY KEY (id);


--
-- Name: link_relation_view; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_relation
    ADD CONSTRAINT link_relation_view UNIQUE (source_object_id, source_key, target_object_id, target_key, type_id);


--
-- Name: link_state_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_state
    ADD CONSTRAINT link_state_name UNIQUE (name);


--
-- Name: link_state_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_state
    ADD CONSTRAINT link_state_pkey PRIMARY KEY (id);


--
-- Name: link_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_type
    ADD CONSTRAINT link_type_name UNIQUE (name);


--
-- Name: link_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_type
    ADD CONSTRAINT link_type_pkey PRIMARY KEY (id);


--
-- Name: mail_account_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY mail_account
    ADD CONSTRAINT mail_account_pkey PRIMARY KEY (id);


--
-- Name: notification_event_message_notification_id_language; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event_message
    ADD CONSTRAINT notification_event_message_notification_id_language UNIQUE (notification_id, language);


--
-- Name: notification_event_message_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event_message
    ADD CONSTRAINT notification_event_message_pkey PRIMARY KEY (id);


--
-- Name: notification_event_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event
    ADD CONSTRAINT notification_event_name UNIQUE (name);


--
-- Name: notification_event_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event
    ADD CONSTRAINT notification_event_pkey PRIMARY KEY (id);


--
-- Name: package_repository_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY package_repository
    ADD CONSTRAINT package_repository_pkey PRIMARY KEY (id);


--
-- Name: pm_activity_dialog_entity_id; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity_dialog
    ADD CONSTRAINT pm_activity_dialog_entity_id UNIQUE (entity_id);


--
-- Name: pm_activity_dialog_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity_dialog
    ADD CONSTRAINT pm_activity_dialog_pkey PRIMARY KEY (id);


--
-- Name: pm_activity_entity_id; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity
    ADD CONSTRAINT pm_activity_entity_id UNIQUE (entity_id);


--
-- Name: pm_activity_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity
    ADD CONSTRAINT pm_activity_pkey PRIMARY KEY (id);


--
-- Name: pm_entity_sync_list; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_entity_sync
    ADD CONSTRAINT pm_entity_sync_list UNIQUE (entity_type, entity_id);


--
-- Name: pm_process_entity_id; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_process
    ADD CONSTRAINT pm_process_entity_id UNIQUE (entity_id);


--
-- Name: pm_process_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_process
    ADD CONSTRAINT pm_process_pkey PRIMARY KEY (id);


--
-- Name: pm_transition_action_entity_id; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition_action
    ADD CONSTRAINT pm_transition_action_entity_id UNIQUE (entity_id);


--
-- Name: pm_transition_action_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition_action
    ADD CONSTRAINT pm_transition_action_pkey PRIMARY KEY (id);


--
-- Name: pm_transition_entity_id; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition
    ADD CONSTRAINT pm_transition_entity_id UNIQUE (entity_id);


--
-- Name: pm_transition_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition
    ADD CONSTRAINT pm_transition_pkey PRIMARY KEY (id);


--
-- Name: queue_auto_response_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_auto_response
    ADD CONSTRAINT queue_auto_response_pkey PRIMARY KEY (id);


--
-- Name: queue_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_name UNIQUE (name);


--
-- Name: queue_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: roles_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_name UNIQUE (name);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: salutation_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY salutation
    ADD CONSTRAINT salutation_name UNIQUE (name);


--
-- Name: salutation_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY salutation
    ADD CONSTRAINT salutation_pkey PRIMARY KEY (id);


--
-- Name: scheduler_future_task_ident; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_future_task
    ADD CONSTRAINT scheduler_future_task_ident UNIQUE (ident);


--
-- Name: scheduler_future_task_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_future_task
    ADD CONSTRAINT scheduler_future_task_pkey PRIMARY KEY (id);


--
-- Name: scheduler_recurrent_task_name_task_type; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_recurrent_task
    ADD CONSTRAINT scheduler_recurrent_task_name_task_type UNIQUE (name, task_type);


--
-- Name: scheduler_recurrent_task_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_recurrent_task
    ADD CONSTRAINT scheduler_recurrent_task_pkey PRIMARY KEY (id);


--
-- Name: scheduler_task_ident; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_task
    ADD CONSTRAINT scheduler_task_ident UNIQUE (ident);


--
-- Name: scheduler_task_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY scheduler_task
    ADD CONSTRAINT scheduler_task_pkey PRIMARY KEY (id);


--
-- Name: service_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_name UNIQUE (name);


--
-- Name: service_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (id);


--
-- Name: service_sla_service_sla; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service_sla
    ADD CONSTRAINT service_sla_service_sla UNIQUE (service_id, sla_id);


--
-- Name: sessions_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: signature_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT signature_name UNIQUE (name);


--
-- Name: signature_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT signature_pkey PRIMARY KEY (id);


--
-- Name: sla_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sla
    ADD CONSTRAINT sla_name UNIQUE (name);


--
-- Name: sla_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sla
    ADD CONSTRAINT sla_pkey PRIMARY KEY (id);


--
-- Name: smime_signer_cert_relations_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY smime_signer_cert_relations
    ADD CONSTRAINT smime_signer_cert_relations_pkey PRIMARY KEY (id);


--
-- Name: standard_attachment_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_attachment
    ADD CONSTRAINT standard_attachment_name UNIQUE (name);


--
-- Name: standard_attachment_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_attachment
    ADD CONSTRAINT standard_attachment_pkey PRIMARY KEY (id);


--
-- Name: standard_template_attachment_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template_attachment
    ADD CONSTRAINT standard_template_attachment_pkey PRIMARY KEY (id);


--
-- Name: standard_template_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template
    ADD CONSTRAINT standard_template_name UNIQUE (name);


--
-- Name: standard_template_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template
    ADD CONSTRAINT standard_template_pkey PRIMARY KEY (id);


--
-- Name: system_address_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_address
    ADD CONSTRAINT system_address_pkey PRIMARY KEY (id);


--
-- Name: system_data_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_data
    ADD CONSTRAINT system_data_pkey PRIMARY KEY (data_key);


--
-- Name: system_maintenance_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_maintenance
    ADD CONSTRAINT system_maintenance_pkey PRIMARY KEY (id);


--
-- Name: ticket_flag_per_user; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_flag
    ADD CONSTRAINT ticket_flag_per_user UNIQUE (ticket_id, ticket_key, create_by);


--
-- Name: ticket_history_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT ticket_history_pkey PRIMARY KEY (id);


--
-- Name: ticket_history_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history_type
    ADD CONSTRAINT ticket_history_type_name UNIQUE (name);


--
-- Name: ticket_history_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history_type
    ADD CONSTRAINT ticket_history_type_pkey PRIMARY KEY (id);


--
-- Name: ticket_lock_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_lock_type
    ADD CONSTRAINT ticket_lock_type_name UNIQUE (name);


--
-- Name: ticket_lock_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_lock_type
    ADD CONSTRAINT ticket_lock_type_pkey PRIMARY KEY (id);


--
-- Name: ticket_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (id);


--
-- Name: ticket_priority_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_priority
    ADD CONSTRAINT ticket_priority_name UNIQUE (name);


--
-- Name: ticket_priority_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_priority
    ADD CONSTRAINT ticket_priority_pkey PRIMARY KEY (id);


--
-- Name: ticket_state_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state
    ADD CONSTRAINT ticket_state_name UNIQUE (name);


--
-- Name: ticket_state_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state
    ADD CONSTRAINT ticket_state_pkey PRIMARY KEY (id);


--
-- Name: ticket_state_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state_type
    ADD CONSTRAINT ticket_state_type_name UNIQUE (name);


--
-- Name: ticket_state_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state_type
    ADD CONSTRAINT ticket_state_type_pkey PRIMARY KEY (id);


--
-- Name: ticket_tn; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_tn UNIQUE (tn);


--
-- Name: ticket_type_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_type
    ADD CONSTRAINT ticket_type_name UNIQUE (name);


--
-- Name: ticket_type_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_type
    ADD CONSTRAINT ticket_type_pkey PRIMARY KEY (id);


--
-- Name: time_accounting_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY time_accounting
    ADD CONSTRAINT time_accounting_pkey PRIMARY KEY (id);


--
-- Name: users_login; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_login UNIQUE (login);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: valid_name; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY valid
    ADD CONSTRAINT valid_name UNIQUE (name);


--
-- Name: valid_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY valid
    ADD CONSTRAINT valid_pkey PRIMARY KEY (id);


--
-- Name: virtual_fs_db_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY virtual_fs_db
    ADD CONSTRAINT virtual_fs_db_pkey PRIMARY KEY (id);


--
-- Name: virtual_fs_pkey; Type: CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY virtual_fs
    ADD CONSTRAINT virtual_fs_pkey PRIMARY KEY (id);


SET search_path = public, pg_catalog;

--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_message
    ADD CONSTRAINT auth_message_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: btns_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY btns
    ADD CONSTRAINT btns_pkey PRIMARY KEY (id);


--
-- Name: buildings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY buildings
    ADD CONSTRAINT buildings_pkey PRIMARY KEY (id);


--
-- Name: calls_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT calls_pkey PRIMARY KEY (id);


--
-- Name: callstats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats
    ADD CONSTRAINT callstats_pkey PRIMARY KEY (id);


--
-- Name: carriers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY carriers
    ADD CONSTRAINT carriers_pkey PRIMARY KEY (id);


--
-- Name: cdrviewer_areacode_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_areacode
    ADD CONSTRAINT cdrviewer_areacode_pkey PRIMARY KEY (id);


--
-- Name: cdrviewer_cdr_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    ADD CONSTRAINT cdrviewer_cdr_pkey PRIMARY KEY (id);


--
-- Name: cdrviewer_dailypeak_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_dailypeak
    ADD CONSTRAINT cdrviewer_dailypeak_pkey PRIMARY KEY (id);


--
-- Name: cdrviewer_datasource_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_datasource
    ADD CONSTRAINT cdrviewer_datasource_pkey PRIMARY KEY (id);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: crm_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY crm_accounts
    ADD CONSTRAINT crm_accounts_pkey PRIMARY KEY (id);


--
-- Name: digitalsignage_custommessage_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY digitalsignage_custommessage
    ADD CONSTRAINT digitalsignage_custommessage_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (id);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: exchanges_manual_matches_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY exchanges_manual_matches
    ADD CONSTRAINT exchanges_manual_matches_pkey PRIMARY KEY (id);


--
-- Name: lavenshtein_exchanges_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY lavenshtein_exchanges
    ADD CONSTRAINT lavenshtein_exchanges_pkey PRIMARY KEY (id);


--
-- Name: lerg8s_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY lerg8s
    ADD CONSTRAINT lerg8s_pkey PRIMARY KEY (id);


--
-- Name: local_exchanges_north_american_exchanges_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_exchanges_north_american_exchanges
    ADD CONSTRAINT local_exchanges_north_american_exchanges_pkey PRIMARY KEY (id);


--
-- Name: local_exchanges_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY local_exchanges
    ADD CONSTRAINT local_exchanges_pkey PRIMARY KEY (id);


--
-- Name: north_american_exchanges_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY north_american_exchanges
    ADD CONSTRAINT north_american_exchanges_pkey PRIMARY KEY (id);


--
-- Name: phones_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY phones
    ADD CONSTRAINT phones_pkey PRIMARY KEY (id);


--
-- Name: portal_customerservice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_customerservice
    ADD CONSTRAINT portal_customerservice_pkey PRIMARY KEY (id);


--
-- Name: portal_service_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_service
    ADD CONSTRAINT portal_service_pkey PRIMARY KEY (id);


--
-- Name: portal_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_userprofile
    ADD CONSTRAINT portal_userprofile_pkey PRIMARY KEY (id);


--
-- Name: portal_userprofilecustomer_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_userprofilecustomer
    ADD CONSTRAINT portal_userprofilecustomer_pkey PRIMARY KEY (id);


--
-- Name: privateradio_channel_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_channel
    ADD CONSTRAINT privateradio_channel_pkey PRIMARY KEY (id);


--
-- Name: privateradio_channelschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_channelschedule
    ADD CONSTRAINT privateradio_channelschedule_pkey PRIMARY KEY (id);


--
-- Name: privateradio_messageorder_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_messageorder
    ADD CONSTRAINT privateradio_messageorder_pkey PRIMARY KEY (id);


--
-- Name: privateradio_workorder_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY privateradio_workorder
    ADD CONSTRAINT privateradio_workorder_pkey PRIMARY KEY (id);


--
-- Name: rate_plans_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rate_plans
    ADD CONSTRAINT rate_plans_pkey PRIMARY KEY (id);


--
-- Name: sensors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sensors
    ADD CONSTRAINT sensors_pkey PRIMARY KEY (id);


--
-- Name: sites_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (id);


--
-- Name: stats_periods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY stats_periods
    ADD CONSTRAINT stats_periods_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


SET search_path = otrs, pg_catalog;

--
-- Name: article_article_sender_type_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_article_sender_type_id ON article USING btree (article_sender_type_id);


--
-- Name: article_article_type_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_article_type_id ON article USING btree (article_type_id);


--
-- Name: article_attachment_article_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_attachment_article_id ON article_attachment USING btree (article_id);


--
-- Name: article_flag_article_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_flag_article_id ON article_flag USING btree (article_id);


--
-- Name: article_flag_article_id_create_by; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_flag_article_id_create_by ON article_flag USING btree (article_id, create_by);


--
-- Name: article_message_id_md5; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_message_id_md5 ON article USING btree (a_message_id_md5);


--
-- Name: article_plain_article_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_plain_article_id ON article_plain USING btree (article_id);


--
-- Name: article_search_article_sender_type_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_search_article_sender_type_id ON article_search USING btree (article_sender_type_id);


--
-- Name: article_search_article_type_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_search_article_type_id ON article_search USING btree (article_type_id);


--
-- Name: article_search_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_search_ticket_id ON article_search USING btree (ticket_id);


--
-- Name: article_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX article_ticket_id ON article USING btree (ticket_id);


--
-- Name: customer_preferences_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX customer_preferences_user_id ON customer_preferences USING btree (user_id);


--
-- Name: dynamic_field_value_field_values; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX dynamic_field_value_field_values ON dynamic_field_value USING btree (object_id, field_id);


--
-- Name: dynamic_field_value_search_date; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX dynamic_field_value_search_date ON dynamic_field_value USING btree (field_id, value_date);


--
-- Name: dynamic_field_value_search_int; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX dynamic_field_value_search_int ON dynamic_field_value USING btree (field_id, value_int);


--
-- Name: generic_agent_jobs_job_name; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX generic_agent_jobs_job_name ON generic_agent_jobs USING btree (job_name);


--
-- Name: gi_debugger_entry_content_create_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX gi_debugger_entry_content_create_time ON gi_debugger_entry_content USING btree (create_time);


--
-- Name: gi_debugger_entry_content_debug_level; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX gi_debugger_entry_content_debug_level ON gi_debugger_entry_content USING btree (debug_level);


--
-- Name: gi_debugger_entry_create_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX gi_debugger_entry_create_time ON gi_debugger_entry USING btree (create_time);


--
-- Name: group_customer_user_group_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX group_customer_user_group_id ON group_customer_user USING btree (group_id);


--
-- Name: group_customer_user_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX group_customer_user_user_id ON group_customer_user USING btree (user_id);


--
-- Name: group_role_group_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX group_role_group_id ON group_role USING btree (group_id);


--
-- Name: group_role_role_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX group_role_role_id ON group_role USING btree (role_id);


--
-- Name: group_user_group_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX group_user_group_id ON group_user USING btree (group_id);


--
-- Name: group_user_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX group_user_user_id ON group_user USING btree (user_id);


--
-- Name: link_relation_list_source; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX link_relation_list_source ON link_relation USING btree (source_object_id, source_key, state_id);


--
-- Name: link_relation_list_target; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX link_relation_list_target ON link_relation USING btree (target_object_id, target_key, state_id);


--
-- Name: notification_event_item_event_key; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX notification_event_item_event_key ON notification_event_item USING btree (event_key);


--
-- Name: notification_event_item_event_value; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX notification_event_item_event_value ON notification_event_item USING btree (event_value);


--
-- Name: notification_event_item_notification_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX notification_event_item_notification_id ON notification_event_item USING btree (notification_id);


--
-- Name: notification_event_message_language; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX notification_event_message_language ON notification_event_message USING btree (language);


--
-- Name: notification_event_message_notification_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX notification_event_message_notification_id ON notification_event_message USING btree (notification_id);


--
-- Name: object_lock_state_list_state; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX object_lock_state_list_state ON gi_object_lock_state USING btree (webservice_id, object_type, object_id, lock_state);


--
-- Name: personal_queues_queue_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX personal_queues_queue_id ON personal_queues USING btree (queue_id);


--
-- Name: personal_queues_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX personal_queues_user_id ON personal_queues USING btree (user_id);


--
-- Name: personal_services_service_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX personal_services_service_id ON personal_services USING btree (service_id);


--
-- Name: personal_services_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX personal_services_user_id ON personal_services USING btree (user_id);


--
-- Name: postmaster_filter_f_name; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX postmaster_filter_f_name ON postmaster_filter USING btree (f_name);


--
-- Name: queue_group_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX queue_group_id ON queue USING btree (group_id);


--
-- Name: queue_preferences_queue_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX queue_preferences_queue_id ON queue_preferences USING btree (queue_id);


--
-- Name: role_user_role_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX role_user_role_id ON role_user USING btree (role_id);


--
-- Name: role_user_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX role_user_user_id ON role_user USING btree (user_id);


--
-- Name: scheduler_future_task_ident_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX scheduler_future_task_ident_id ON scheduler_future_task USING btree (ident, id);


--
-- Name: scheduler_future_task_lock_key_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX scheduler_future_task_lock_key_id ON scheduler_future_task USING btree (lock_key, id);


--
-- Name: scheduler_recurrent_task_lock_key_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX scheduler_recurrent_task_lock_key_id ON scheduler_recurrent_task USING btree (lock_key, id);


--
-- Name: scheduler_recurrent_task_task_type_name; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX scheduler_recurrent_task_task_type_name ON scheduler_recurrent_task USING btree (task_type, name);


--
-- Name: scheduler_task_ident_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX scheduler_task_ident_id ON scheduler_task USING btree (ident, id);


--
-- Name: scheduler_task_lock_key_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX scheduler_task_lock_key_id ON scheduler_task USING btree (lock_key, id);


--
-- Name: search_profile_login; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX search_profile_login ON search_profile USING btree (login);


--
-- Name: search_profile_profile_name; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX search_profile_profile_name ON search_profile USING btree (profile_name);


--
-- Name: service_customer_user_customer_user_login; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX service_customer_user_customer_user_login ON service_customer_user USING btree (customer_user_login);


--
-- Name: service_customer_user_service_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX service_customer_user_service_id ON service_customer_user USING btree (service_id);


--
-- Name: service_preferences_service_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX service_preferences_service_id ON service_preferences USING btree (service_id);


--
-- Name: sessions_data_key; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX sessions_data_key ON sessions USING btree (data_key);


--
-- Name: sessions_session_id_data_key; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX sessions_session_id_data_key ON sessions USING btree (session_id, data_key);


--
-- Name: sla_preferences_sla_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX sla_preferences_sla_id ON sla_preferences USING btree (sla_id);


--
-- Name: ticket_archive_flag; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_archive_flag ON ticket USING btree (archive_flag);


--
-- Name: ticket_create_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_create_time ON ticket USING btree (create_time);


--
-- Name: ticket_create_time_unix; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_create_time_unix ON ticket USING btree (create_time_unix);


--
-- Name: ticket_customer_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_customer_id ON ticket USING btree (customer_id);


--
-- Name: ticket_customer_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_customer_user_id ON ticket USING btree (customer_user_id);


--
-- Name: ticket_escalation_response_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_escalation_response_time ON ticket USING btree (escalation_response_time);


--
-- Name: ticket_escalation_solution_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_escalation_solution_time ON ticket USING btree (escalation_solution_time);


--
-- Name: ticket_escalation_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_escalation_time ON ticket USING btree (escalation_time);


--
-- Name: ticket_escalation_update_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_escalation_update_time ON ticket USING btree (escalation_update_time);


--
-- Name: ticket_flag_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_flag_ticket_id ON ticket_flag USING btree (ticket_id);


--
-- Name: ticket_flag_ticket_id_create_by; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_flag_ticket_id_create_by ON ticket_flag USING btree (ticket_id, create_by);


--
-- Name: ticket_flag_ticket_id_ticket_key; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_flag_ticket_id_ticket_key ON ticket_flag USING btree (ticket_id, ticket_key);


--
-- Name: ticket_history_create_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_create_time ON ticket_history USING btree (create_time);


--
-- Name: ticket_history_history_type_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_history_type_id ON ticket_history USING btree (history_type_id);


--
-- Name: ticket_history_owner_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_owner_id ON ticket_history USING btree (owner_id);


--
-- Name: ticket_history_priority_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_priority_id ON ticket_history USING btree (priority_id);


--
-- Name: ticket_history_queue_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_queue_id ON ticket_history USING btree (queue_id);


--
-- Name: ticket_history_state_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_state_id ON ticket_history USING btree (state_id);


--
-- Name: ticket_history_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_ticket_id ON ticket_history USING btree (ticket_id);


--
-- Name: ticket_history_type_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_history_type_id ON ticket_history USING btree (type_id);


--
-- Name: ticket_index_group_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_index_group_id ON ticket_index USING btree (group_id);


--
-- Name: ticket_index_queue_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_index_queue_id ON ticket_index USING btree (queue_id);


--
-- Name: ticket_index_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_index_ticket_id ON ticket_index USING btree (ticket_id);


--
-- Name: ticket_lock_index_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_lock_index_ticket_id ON ticket_lock_index USING btree (ticket_id);


--
-- Name: ticket_loop_protection_sent_date; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_loop_protection_sent_date ON ticket_loop_protection USING btree (sent_date);


--
-- Name: ticket_loop_protection_sent_to; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_loop_protection_sent_to ON ticket_loop_protection USING btree (sent_to);


--
-- Name: ticket_queue_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_queue_id ON ticket USING btree (queue_id);


--
-- Name: ticket_queue_view; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_queue_view ON ticket USING btree (ticket_state_id, ticket_lock_id);


--
-- Name: ticket_responsible_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_responsible_user_id ON ticket USING btree (responsible_user_id);


--
-- Name: ticket_ticket_lock_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_ticket_lock_id ON ticket USING btree (ticket_lock_id);


--
-- Name: ticket_ticket_priority_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_ticket_priority_id ON ticket USING btree (ticket_priority_id);


--
-- Name: ticket_ticket_state_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_ticket_state_id ON ticket USING btree (ticket_state_id);


--
-- Name: ticket_timeout; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_timeout ON ticket USING btree (timeout);


--
-- Name: ticket_title; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_title ON ticket USING btree (title);


--
-- Name: ticket_type_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_type_id ON ticket USING btree (type_id);


--
-- Name: ticket_until_time; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_until_time ON ticket USING btree (until_time);


--
-- Name: ticket_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_user_id ON ticket USING btree (user_id);


--
-- Name: ticket_watcher_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_watcher_ticket_id ON ticket_watcher USING btree (ticket_id);


--
-- Name: ticket_watcher_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX ticket_watcher_user_id ON ticket_watcher USING btree (user_id);


--
-- Name: time_accounting_ticket_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX time_accounting_ticket_id ON time_accounting USING btree (ticket_id);


--
-- Name: user_preferences_user_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX user_preferences_user_id ON user_preferences USING btree (user_id);


--
-- Name: virtual_fs_backend; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX virtual_fs_backend ON virtual_fs USING btree (backend);


--
-- Name: virtual_fs_db_filename; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX virtual_fs_db_filename ON virtual_fs_db USING btree (filename);


--
-- Name: virtual_fs_filename; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX virtual_fs_filename ON virtual_fs USING btree (filename);


--
-- Name: virtual_fs_preferences_key_value; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX virtual_fs_preferences_key_value ON virtual_fs_preferences USING btree (preferences_key, preferences_value);


--
-- Name: virtual_fs_preferences_virtual_fs_id; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX virtual_fs_preferences_virtual_fs_id ON virtual_fs_preferences USING btree (virtual_fs_id);


--
-- Name: xml_storage_key_type; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX xml_storage_key_type ON xml_storage USING btree (xml_key, xml_type);


--
-- Name: xml_storage_xml_content_key; Type: INDEX; Schema: otrs; Owner: -
--

CREATE INDEX xml_storage_xml_content_key ON xml_storage USING btree (xml_content_key);


SET search_path = public, pg_catalog;

--
-- Name: auth_group_name_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX auth_group_name_key ON auth_group USING btree (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX auth_group_permissions_group_id_permission_id_key ON auth_group_permissions USING btree (group_id, permission_id);


--
-- Name: auth_message_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_message_user_id ON auth_message USING btree (user_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX auth_permission_content_type_id_codename_key ON auth_permission USING btree (content_type_id, codename);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX auth_user_groups_user_id_group_id_key ON auth_user_groups USING btree (user_id, group_id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX auth_user_user_permissions_user_id_permission_id_key ON auth_user_user_permissions USING btree (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX auth_user_username_key ON auth_user USING btree (username);


--
-- Name: cdrviewer_cdr_calledphone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdrviewer_cdr_calledphone_id ON cdrviewer_cdr USING btree (calledphone_id);


--
-- Name: cdrviewer_cdr_callingphone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdrviewer_cdr_callingphone_id ON cdrviewer_cdr USING btree (callingphone_id);


--
-- Name: cdrviewer_cdr_newanswertime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdrviewer_cdr_newanswertime ON cdrviewer_cdr USING btree (newanswertime);


--
-- Name: cdrviewer_cdr_newreleasetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdrviewer_cdr_newreleasetime ON cdrviewer_cdr USING btree (newreleasetime);


--
-- Name: cdrviewer_cdr_newstarttime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdrviewer_cdr_newstarttime ON cdrviewer_cdr USING btree (newstarttime);


--
-- Name: cdrviewer_cdr_startdatetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdrviewer_cdr_startdatetime ON cdrviewer_cdr USING btree (startdatetime);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_content_type_app_label_model_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX django_content_type_app_label_model_key ON django_content_type USING btree (app_label, model);


--
-- Name: index_btns_on_billingtelephonenumber; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_btns_on_billingtelephonenumber ON btns USING btree (billingtelephonenumber);


--
-- Name: index_btns_on_billingtelephonenumber_and_client_id_and_end_date; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_btns_on_billingtelephonenumber_and_client_id_and_end_date ON btns USING btree (billingtelephonenumber, client_id, end_date);


--
-- Name: index_btns_on_client_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_btns_on_client_id ON btns USING btree (client_id);


--
-- Name: index_calls_on_master_callid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_calls_on_master_callid ON calls USING btree (master_callid);


--
-- Name: index_calls_on_startdatetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_calls_on_startdatetime ON calls USING btree (startdatetime);


--
-- Name: index_calls_on_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_calls_on_type ON calls USING btree (type);


--
-- Name: index_callstats_on_phone_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_callstats_on_phone_id ON callstats USING btree (phone_id);


--
-- Name: index_callstats_on_stats_period_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_callstats_on_stats_period_id ON callstats USING btree (stats_period_id);


--
-- Name: index_callstats_on_stats_period_id_and_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_callstats_on_stats_period_id_and_type ON callstats USING btree (stats_period_id, type);


--
-- Name: index_callstats_on_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_callstats_on_type ON callstats USING btree (type);


--
-- Name: index_cdrviewer_cdr_on_call_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_call_id ON cdrviewer_cdr USING btree (call_id);


--
-- Name: index_cdrviewer_cdr_on_calledbtn_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_calledbtn_id ON cdrviewer_cdr USING btree (calledbtn_id);


--
-- Name: index_cdrviewer_cdr_on_callednumber; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_callednumber ON cdrviewer_cdr USING btree (callednumber);


--
-- Name: index_cdrviewer_cdr_on_callingbtn_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_callingbtn_id ON cdrviewer_cdr USING btree (callingbtn_id);


--
-- Name: index_cdrviewer_cdr_on_callingnumber; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_callingnumber ON cdrviewer_cdr USING btree (callingnumber);


--
-- Name: index_cdrviewer_cdr_on_datasource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_datasource_id ON cdrviewer_cdr USING btree (datasource_id);


--
-- Name: index_cdrviewer_cdr_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_cdrviewer_cdr_on_id ON cdrviewer_cdr USING btree (id);


--
-- Name: index_cdrviewer_cdr_on_localcallid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_localcallid ON cdrviewer_cdr USING btree (localcallid);


--
-- Name: index_cdrviewer_cdr_on_relatedcallid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_relatedcallid ON cdrviewer_cdr USING btree (relatedcallid);


--
-- Name: index_cdrviewer_cdr_on_transfer_relatedcallid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_on_transfer_relatedcallid ON cdrviewer_cdr USING btree (transfer_relatedcallid);


--
-- Name: index_cdrviewer_cdr_remotecallid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_cdr_remotecallid ON cdrviewer_cdr USING btree (remotecallid);


--
-- Name: index_cdrviewer_datasource_on_datasource_filename; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_cdrviewer_datasource_on_datasource_filename ON cdrviewer_datasource USING btree (datasource_filename);


--
-- Name: index_clients_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_clients_on_id ON clients USING btree (id);


--
-- Name: index_l_na_exchanges; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_l_na_exchanges ON local_exchanges_north_american_exchanges USING btree (local_exchanges_id, north_american_exchanges_id);


--
-- Name: index_phones_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_phones_on_id ON phones USING btree (id);


--
-- Name: index_sites_on_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sites_on_id ON sites USING btree (id);


--
-- Name: portal_customerservice_customer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX portal_customerservice_customer_id ON portal_customerservice USING btree (customer_id);


--
-- Name: portal_customerservice_service_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX portal_customerservice_service_id ON portal_customerservice USING btree (service_id);


--
-- Name: portal_userprofile_user_id_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX portal_userprofile_user_id_key ON portal_userprofile USING btree (user_id);


--
-- Name: portal_userprofilecustomer_customer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX portal_userprofilecustomer_customer_id ON portal_userprofilecustomer USING btree (customer_id);


--
-- Name: portal_userprofilecustomer_user_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX portal_userprofilecustomer_user_profile_id ON portal_userprofilecustomer USING btree (user_profile_id);


--
-- Name: privateradio_channelschedule_channel_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX privateradio_channelschedule_channel_id ON privateradio_channelschedule USING btree (channel_id);


--
-- Name: privateradio_channelschedule_customer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX privateradio_channelschedule_customer_id ON privateradio_channelschedule USING btree (customer_id);


--
-- Name: privateradio_messageorder_customer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX privateradio_messageorder_customer_id ON privateradio_messageorder USING btree (customer_id);


--
-- Name: privateradio_messageorder_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX privateradio_messageorder_user_id ON privateradio_messageorder USING btree (user_id);


--
-- Name: privateradio_workorder_messageorder_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX privateradio_workorder_messageorder_id ON privateradio_workorder USING btree (messageorder_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


SET search_path = otrs, pg_catalog;

--
-- Name: fk_acl_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY acl
    ADD CONSTRAINT fk_acl_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_acl_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY acl
    ADD CONSTRAINT fk_acl_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_acl_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY acl
    ADD CONSTRAINT fk_acl_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_article_article_sender_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article
    ADD CONSTRAINT fk_article_article_sender_type_id_id FOREIGN KEY (article_sender_type_id) REFERENCES article_sender_type(id);


--
-- Name: fk_article_article_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article
    ADD CONSTRAINT fk_article_article_type_id_id FOREIGN KEY (article_type_id) REFERENCES article_type(id);


--
-- Name: fk_article_attachment_article_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_attachment
    ADD CONSTRAINT fk_article_attachment_article_id_id FOREIGN KEY (article_id) REFERENCES article(id);


--
-- Name: fk_article_attachment_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_attachment
    ADD CONSTRAINT fk_article_attachment_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_article_attachment_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_attachment
    ADD CONSTRAINT fk_article_attachment_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_article_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article
    ADD CONSTRAINT fk_article_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_article_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article
    ADD CONSTRAINT fk_article_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_article_flag_article_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_flag
    ADD CONSTRAINT fk_article_flag_article_id_id FOREIGN KEY (article_id) REFERENCES article(id);


--
-- Name: fk_article_flag_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_flag
    ADD CONSTRAINT fk_article_flag_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_article_plain_article_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_plain
    ADD CONSTRAINT fk_article_plain_article_id_id FOREIGN KEY (article_id) REFERENCES article(id);


--
-- Name: fk_article_plain_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_plain
    ADD CONSTRAINT fk_article_plain_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_article_plain_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_plain
    ADD CONSTRAINT fk_article_plain_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_article_search_article_sender_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_search
    ADD CONSTRAINT fk_article_search_article_sender_type_id_id FOREIGN KEY (article_sender_type_id) REFERENCES article_sender_type(id);


--
-- Name: fk_article_search_article_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_search
    ADD CONSTRAINT fk_article_search_article_type_id_id FOREIGN KEY (article_type_id) REFERENCES article_type(id);


--
-- Name: fk_article_search_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_search
    ADD CONSTRAINT fk_article_search_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_article_sender_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_sender_type
    ADD CONSTRAINT fk_article_sender_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_article_sender_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_sender_type
    ADD CONSTRAINT fk_article_sender_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_article_sender_type_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_sender_type
    ADD CONSTRAINT fk_article_sender_type_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_article_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article
    ADD CONSTRAINT fk_article_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_article_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_type
    ADD CONSTRAINT fk_article_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_article_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_type
    ADD CONSTRAINT fk_article_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_article_type_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article_type
    ADD CONSTRAINT fk_article_type_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_article_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY article
    ADD CONSTRAINT fk_article_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_auto_response_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response
    ADD CONSTRAINT fk_auto_response_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_auto_response_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response
    ADD CONSTRAINT fk_auto_response_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_auto_response_system_address_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response
    ADD CONSTRAINT fk_auto_response_system_address_id_id FOREIGN KEY (system_address_id) REFERENCES system_address(id);


--
-- Name: fk_auto_response_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response_type
    ADD CONSTRAINT fk_auto_response_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_auto_response_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response_type
    ADD CONSTRAINT fk_auto_response_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_auto_response_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response
    ADD CONSTRAINT fk_auto_response_type_id_id FOREIGN KEY (type_id) REFERENCES auto_response_type(id);


--
-- Name: fk_auto_response_type_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response_type
    ADD CONSTRAINT fk_auto_response_type_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_auto_response_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY auto_response
    ADD CONSTRAINT fk_auto_response_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_cloud_service_config_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY cloud_service_config
    ADD CONSTRAINT fk_cloud_service_config_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_cloud_service_config_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY cloud_service_config
    ADD CONSTRAINT fk_cloud_service_config_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_cloud_service_config_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY cloud_service_config
    ADD CONSTRAINT fk_cloud_service_config_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_customer_user_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY customer_user
    ADD CONSTRAINT fk_customer_user_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_customer_user_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY customer_user
    ADD CONSTRAINT fk_customer_user_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_customer_user_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY customer_user
    ADD CONSTRAINT fk_customer_user_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_dynamic_field_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field
    ADD CONSTRAINT fk_dynamic_field_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_dynamic_field_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field
    ADD CONSTRAINT fk_dynamic_field_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_dynamic_field_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field
    ADD CONSTRAINT fk_dynamic_field_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_dynamic_field_value_field_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY dynamic_field_value
    ADD CONSTRAINT fk_dynamic_field_value_field_id_id FOREIGN KEY (field_id) REFERENCES dynamic_field(id);


--
-- Name: fk_follow_up_possible_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY follow_up_possible
    ADD CONSTRAINT fk_follow_up_possible_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_follow_up_possible_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY follow_up_possible
    ADD CONSTRAINT fk_follow_up_possible_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_follow_up_possible_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY follow_up_possible
    ADD CONSTRAINT fk_follow_up_possible_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_gi_debugger_entry_content_gi_debugger_entry_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_debugger_entry_content
    ADD CONSTRAINT fk_gi_debugger_entry_content_gi_debugger_entry_id_id FOREIGN KEY (gi_debugger_entry_id) REFERENCES gi_debugger_entry(id);


--
-- Name: fk_gi_debugger_entry_webservice_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_debugger_entry
    ADD CONSTRAINT fk_gi_debugger_entry_webservice_id_id FOREIGN KEY (webservice_id) REFERENCES gi_webservice_config(id);


--
-- Name: fk_gi_object_lock_state_webservice_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_object_lock_state
    ADD CONSTRAINT fk_gi_object_lock_state_webservice_id_id FOREIGN KEY (webservice_id) REFERENCES gi_webservice_config(id);


--
-- Name: fk_gi_webservice_config_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config
    ADD CONSTRAINT fk_gi_webservice_config_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_gi_webservice_config_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config
    ADD CONSTRAINT fk_gi_webservice_config_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_gi_webservice_config_history_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config_history
    ADD CONSTRAINT fk_gi_webservice_config_history_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_gi_webservice_config_history_config_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config_history
    ADD CONSTRAINT fk_gi_webservice_config_history_config_id_id FOREIGN KEY (config_id) REFERENCES gi_webservice_config(id);


--
-- Name: fk_gi_webservice_config_history_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config_history
    ADD CONSTRAINT fk_gi_webservice_config_history_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_gi_webservice_config_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY gi_webservice_config
    ADD CONSTRAINT fk_gi_webservice_config_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_group_customer_user_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_customer_user
    ADD CONSTRAINT fk_group_customer_user_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_group_customer_user_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_customer_user
    ADD CONSTRAINT fk_group_customer_user_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_group_customer_user_group_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_customer_user
    ADD CONSTRAINT fk_group_customer_user_group_id_id FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: fk_group_role_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT fk_group_role_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_group_role_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT fk_group_role_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_group_role_group_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT fk_group_role_group_id_id FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: fk_group_role_role_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT fk_group_role_role_id_id FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: fk_group_user_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_user
    ADD CONSTRAINT fk_group_user_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_group_user_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_user
    ADD CONSTRAINT fk_group_user_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_group_user_group_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_user
    ADD CONSTRAINT fk_group_user_group_id_id FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: fk_group_user_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY group_user
    ADD CONSTRAINT fk_group_user_user_id_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_groups_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT fk_groups_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_groups_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT fk_groups_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_groups_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT fk_groups_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_link_relation_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_relation
    ADD CONSTRAINT fk_link_relation_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_link_relation_source_object_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_relation
    ADD CONSTRAINT fk_link_relation_source_object_id_id FOREIGN KEY (source_object_id) REFERENCES link_object(id);


--
-- Name: fk_link_relation_state_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_relation
    ADD CONSTRAINT fk_link_relation_state_id_id FOREIGN KEY (state_id) REFERENCES link_state(id);


--
-- Name: fk_link_relation_target_object_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_relation
    ADD CONSTRAINT fk_link_relation_target_object_id_id FOREIGN KEY (target_object_id) REFERENCES link_object(id);


--
-- Name: fk_link_relation_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_relation
    ADD CONSTRAINT fk_link_relation_type_id_id FOREIGN KEY (type_id) REFERENCES link_type(id);


--
-- Name: fk_link_state_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_state
    ADD CONSTRAINT fk_link_state_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_link_state_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_state
    ADD CONSTRAINT fk_link_state_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_link_state_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_state
    ADD CONSTRAINT fk_link_state_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_link_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_type
    ADD CONSTRAINT fk_link_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_link_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_type
    ADD CONSTRAINT fk_link_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_link_type_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY link_type
    ADD CONSTRAINT fk_link_type_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_mail_account_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY mail_account
    ADD CONSTRAINT fk_mail_account_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_mail_account_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY mail_account
    ADD CONSTRAINT fk_mail_account_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_mail_account_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY mail_account
    ADD CONSTRAINT fk_mail_account_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_notification_event_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event
    ADD CONSTRAINT fk_notification_event_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_notification_event_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event
    ADD CONSTRAINT fk_notification_event_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_notification_event_item_notification_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event_item
    ADD CONSTRAINT fk_notification_event_item_notification_id_id FOREIGN KEY (notification_id) REFERENCES notification_event(id);


--
-- Name: fk_notification_event_message_notification_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event_message
    ADD CONSTRAINT fk_notification_event_message_notification_id_id FOREIGN KEY (notification_id) REFERENCES notification_event(id);


--
-- Name: fk_notification_event_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY notification_event
    ADD CONSTRAINT fk_notification_event_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_package_repository_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY package_repository
    ADD CONSTRAINT fk_package_repository_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_package_repository_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY package_repository
    ADD CONSTRAINT fk_package_repository_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_personal_queues_queue_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY personal_queues
    ADD CONSTRAINT fk_personal_queues_queue_id_id FOREIGN KEY (queue_id) REFERENCES queue(id);


--
-- Name: fk_personal_queues_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY personal_queues
    ADD CONSTRAINT fk_personal_queues_user_id_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_personal_services_service_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY personal_services
    ADD CONSTRAINT fk_personal_services_service_id_id FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: fk_personal_services_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY personal_services
    ADD CONSTRAINT fk_personal_services_user_id_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_pm_activity_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity
    ADD CONSTRAINT fk_pm_activity_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_pm_activity_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity
    ADD CONSTRAINT fk_pm_activity_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_pm_activity_dialog_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity_dialog
    ADD CONSTRAINT fk_pm_activity_dialog_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_pm_activity_dialog_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_activity_dialog
    ADD CONSTRAINT fk_pm_activity_dialog_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_pm_process_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_process
    ADD CONSTRAINT fk_pm_process_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_pm_process_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_process
    ADD CONSTRAINT fk_pm_process_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_pm_transition_action_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition_action
    ADD CONSTRAINT fk_pm_transition_action_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_pm_transition_action_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition_action
    ADD CONSTRAINT fk_pm_transition_action_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_pm_transition_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition
    ADD CONSTRAINT fk_pm_transition_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_pm_transition_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY pm_transition
    ADD CONSTRAINT fk_pm_transition_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_queue_auto_response_auto_response_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_auto_response
    ADD CONSTRAINT fk_queue_auto_response_auto_response_id_id FOREIGN KEY (auto_response_id) REFERENCES auto_response(id);


--
-- Name: fk_queue_auto_response_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_auto_response
    ADD CONSTRAINT fk_queue_auto_response_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_queue_auto_response_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_auto_response
    ADD CONSTRAINT fk_queue_auto_response_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_queue_auto_response_queue_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_auto_response
    ADD CONSTRAINT fk_queue_auto_response_queue_id_id FOREIGN KEY (queue_id) REFERENCES queue(id);


--
-- Name: fk_queue_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_queue_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_queue_follow_up_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_follow_up_id_id FOREIGN KEY (follow_up_id) REFERENCES follow_up_possible(id);


--
-- Name: fk_queue_group_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_group_id_id FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: fk_queue_preferences_queue_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_preferences
    ADD CONSTRAINT fk_queue_preferences_queue_id_id FOREIGN KEY (queue_id) REFERENCES queue(id);


--
-- Name: fk_queue_salutation_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_salutation_id_id FOREIGN KEY (salutation_id) REFERENCES salutation(id);


--
-- Name: fk_queue_signature_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_signature_id_id FOREIGN KEY (signature_id) REFERENCES signature(id);


--
-- Name: fk_queue_standard_template_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_standard_template
    ADD CONSTRAINT fk_queue_standard_template_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_queue_standard_template_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_standard_template
    ADD CONSTRAINT fk_queue_standard_template_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_queue_standard_template_queue_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_standard_template
    ADD CONSTRAINT fk_queue_standard_template_queue_id_id FOREIGN KEY (queue_id) REFERENCES queue(id);


--
-- Name: fk_queue_standard_template_standard_template_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue_standard_template
    ADD CONSTRAINT fk_queue_standard_template_standard_template_id_id FOREIGN KEY (standard_template_id) REFERENCES standard_template(id);


--
-- Name: fk_queue_system_address_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_system_address_id_id FOREIGN KEY (system_address_id) REFERENCES system_address(id);


--
-- Name: fk_queue_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY queue
    ADD CONSTRAINT fk_queue_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_role_user_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT fk_role_user_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_role_user_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT fk_role_user_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_role_user_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT fk_role_user_user_id_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_roles_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT fk_roles_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_roles_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT fk_roles_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_roles_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT fk_roles_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_salutation_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY salutation
    ADD CONSTRAINT fk_salutation_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_salutation_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY salutation
    ADD CONSTRAINT fk_salutation_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_salutation_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY salutation
    ADD CONSTRAINT fk_salutation_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_service_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service
    ADD CONSTRAINT fk_service_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_service_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service
    ADD CONSTRAINT fk_service_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_service_customer_user_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service_customer_user
    ADD CONSTRAINT fk_service_customer_user_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_service_customer_user_service_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service_customer_user
    ADD CONSTRAINT fk_service_customer_user_service_id_id FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: fk_service_preferences_service_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service_preferences
    ADD CONSTRAINT fk_service_preferences_service_id_id FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: fk_service_sla_service_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service_sla
    ADD CONSTRAINT fk_service_sla_service_id_id FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: fk_service_sla_sla_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY service_sla
    ADD CONSTRAINT fk_service_sla_sla_id_id FOREIGN KEY (sla_id) REFERENCES sla(id);


--
-- Name: fk_signature_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT fk_signature_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_signature_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT fk_signature_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_signature_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT fk_signature_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_sla_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sla
    ADD CONSTRAINT fk_sla_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_sla_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sla
    ADD CONSTRAINT fk_sla_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_sla_preferences_sla_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY sla_preferences
    ADD CONSTRAINT fk_sla_preferences_sla_id_id FOREIGN KEY (sla_id) REFERENCES sla(id);


--
-- Name: fk_smime_signer_cert_relations_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY smime_signer_cert_relations
    ADD CONSTRAINT fk_smime_signer_cert_relations_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_smime_signer_cert_relations_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY smime_signer_cert_relations
    ADD CONSTRAINT fk_smime_signer_cert_relations_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_standard_attachment_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_attachment
    ADD CONSTRAINT fk_standard_attachment_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_standard_attachment_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_attachment
    ADD CONSTRAINT fk_standard_attachment_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_standard_attachment_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_attachment
    ADD CONSTRAINT fk_standard_attachment_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_standard_template_attachment_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template_attachment
    ADD CONSTRAINT fk_standard_template_attachment_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_standard_template_attachment_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template_attachment
    ADD CONSTRAINT fk_standard_template_attachment_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_standard_template_attachment_standard_attachment_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template_attachment
    ADD CONSTRAINT fk_standard_template_attachment_standard_attachment_id_id FOREIGN KEY (standard_attachment_id) REFERENCES standard_attachment(id);


--
-- Name: fk_standard_template_attachment_standard_template_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template_attachment
    ADD CONSTRAINT fk_standard_template_attachment_standard_template_id_id FOREIGN KEY (standard_template_id) REFERENCES standard_template(id);


--
-- Name: fk_standard_template_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template
    ADD CONSTRAINT fk_standard_template_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_standard_template_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template
    ADD CONSTRAINT fk_standard_template_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_standard_template_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY standard_template
    ADD CONSTRAINT fk_standard_template_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_system_address_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_address
    ADD CONSTRAINT fk_system_address_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_system_address_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_address
    ADD CONSTRAINT fk_system_address_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_system_address_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_address
    ADD CONSTRAINT fk_system_address_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_system_data_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_data
    ADD CONSTRAINT fk_system_data_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_system_data_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_data
    ADD CONSTRAINT fk_system_data_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_system_maintenance_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_maintenance
    ADD CONSTRAINT fk_system_maintenance_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_system_maintenance_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_maintenance
    ADD CONSTRAINT fk_system_maintenance_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_system_maintenance_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY system_maintenance
    ADD CONSTRAINT fk_system_maintenance_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_ticket_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_flag_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_flag
    ADD CONSTRAINT fk_ticket_flag_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_flag_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_flag
    ADD CONSTRAINT fk_ticket_flag_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_ticket_history_article_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_article_id_id FOREIGN KEY (article_id) REFERENCES article(id);


--
-- Name: fk_ticket_history_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_history_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_history_history_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_history_type_id_id FOREIGN KEY (history_type_id) REFERENCES ticket_history_type(id);


--
-- Name: fk_ticket_history_owner_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_owner_id_id FOREIGN KEY (owner_id) REFERENCES users(id);


--
-- Name: fk_ticket_history_priority_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_priority_id_id FOREIGN KEY (priority_id) REFERENCES ticket_priority(id);


--
-- Name: fk_ticket_history_queue_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_queue_id_id FOREIGN KEY (queue_id) REFERENCES queue(id);


--
-- Name: fk_ticket_history_state_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_state_id_id FOREIGN KEY (state_id) REFERENCES ticket_state(id);


--
-- Name: fk_ticket_history_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_ticket_history_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history_type
    ADD CONSTRAINT fk_ticket_history_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_history_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history_type
    ADD CONSTRAINT fk_ticket_history_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_history_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history
    ADD CONSTRAINT fk_ticket_history_type_id_id FOREIGN KEY (type_id) REFERENCES ticket_type(id);


--
-- Name: fk_ticket_history_type_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_history_type
    ADD CONSTRAINT fk_ticket_history_type_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_ticket_index_group_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_index
    ADD CONSTRAINT fk_ticket_index_group_id_id FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: fk_ticket_index_queue_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_index
    ADD CONSTRAINT fk_ticket_index_queue_id_id FOREIGN KEY (queue_id) REFERENCES queue(id);


--
-- Name: fk_ticket_index_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_index
    ADD CONSTRAINT fk_ticket_index_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_ticket_lock_index_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_lock_index
    ADD CONSTRAINT fk_ticket_lock_index_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_ticket_lock_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_lock_type
    ADD CONSTRAINT fk_ticket_lock_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_lock_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_lock_type
    ADD CONSTRAINT fk_ticket_lock_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_lock_type_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_lock_type
    ADD CONSTRAINT fk_ticket_lock_type_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_ticket_priority_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_priority
    ADD CONSTRAINT fk_ticket_priority_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_priority_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_priority
    ADD CONSTRAINT fk_ticket_priority_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_queue_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_queue_id_id FOREIGN KEY (queue_id) REFERENCES queue(id);


--
-- Name: fk_ticket_responsible_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_responsible_user_id_id FOREIGN KEY (responsible_user_id) REFERENCES users(id);


--
-- Name: fk_ticket_service_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_service_id_id FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: fk_ticket_sla_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_sla_id_id FOREIGN KEY (sla_id) REFERENCES sla(id);


--
-- Name: fk_ticket_state_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state
    ADD CONSTRAINT fk_ticket_state_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_state_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state
    ADD CONSTRAINT fk_ticket_state_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_state_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state_type
    ADD CONSTRAINT fk_ticket_state_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_state_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state_type
    ADD CONSTRAINT fk_ticket_state_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_state_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state
    ADD CONSTRAINT fk_ticket_state_type_id_id FOREIGN KEY (type_id) REFERENCES ticket_state_type(id);


--
-- Name: fk_ticket_state_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_state
    ADD CONSTRAINT fk_ticket_state_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_ticket_ticket_lock_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_ticket_lock_id_id FOREIGN KEY (ticket_lock_id) REFERENCES ticket_lock_type(id);


--
-- Name: fk_ticket_ticket_priority_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_ticket_priority_id_id FOREIGN KEY (ticket_priority_id) REFERENCES ticket_priority(id);


--
-- Name: fk_ticket_ticket_state_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_ticket_state_id_id FOREIGN KEY (ticket_state_id) REFERENCES ticket_state(id);


--
-- Name: fk_ticket_type_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_type
    ADD CONSTRAINT fk_ticket_type_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_type_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_type
    ADD CONSTRAINT fk_ticket_type_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_type_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_type_id_id FOREIGN KEY (type_id) REFERENCES ticket_type(id);


--
-- Name: fk_ticket_type_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_type
    ADD CONSTRAINT fk_ticket_type_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_ticket_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT fk_ticket_user_id_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_ticket_watcher_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_watcher
    ADD CONSTRAINT fk_ticket_watcher_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_ticket_watcher_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_watcher
    ADD CONSTRAINT fk_ticket_watcher_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_ticket_watcher_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_watcher
    ADD CONSTRAINT fk_ticket_watcher_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_ticket_watcher_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY ticket_watcher
    ADD CONSTRAINT fk_ticket_watcher_user_id_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_time_accounting_article_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY time_accounting
    ADD CONSTRAINT fk_time_accounting_article_id_id FOREIGN KEY (article_id) REFERENCES article(id);


--
-- Name: fk_time_accounting_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY time_accounting
    ADD CONSTRAINT fk_time_accounting_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_time_accounting_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY time_accounting
    ADD CONSTRAINT fk_time_accounting_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_time_accounting_ticket_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY time_accounting
    ADD CONSTRAINT fk_time_accounting_ticket_id_id FOREIGN KEY (ticket_id) REFERENCES ticket(id);


--
-- Name: fk_user_preferences_user_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY user_preferences
    ADD CONSTRAINT fk_user_preferences_user_id_id FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_users_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_users_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_users_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_users_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_users_valid_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_users_valid_id_id FOREIGN KEY (valid_id) REFERENCES valid(id);


--
-- Name: fk_valid_change_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY valid
    ADD CONSTRAINT fk_valid_change_by_id FOREIGN KEY (change_by) REFERENCES users(id);


--
-- Name: fk_valid_create_by_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY valid
    ADD CONSTRAINT fk_valid_create_by_id FOREIGN KEY (create_by) REFERENCES users(id);


--
-- Name: fk_virtual_fs_preferences_virtual_fs_id_id; Type: FK CONSTRAINT; Schema: otrs; Owner: -
--

ALTER TABLE ONLY virtual_fs_preferences
    ADD CONSTRAINT fk_virtual_fs_preferences_virtual_fs_id_id FOREIGN KEY (virtual_fs_id) REFERENCES virtual_fs(id);


SET search_path = public, pg_catalog;

--
-- Name: calls_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT calls_client_id_fk FOREIGN KEY (client_id) REFERENCES clients(id);


--
-- Name: calls_datasource_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY calls
    ADD CONSTRAINT calls_datasource_id_fk FOREIGN KEY (datasource_id) REFERENCES cdrviewer_datasource(id);


--
-- Name: callstats_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats
    ADD CONSTRAINT callstats_client_id_fk FOREIGN KEY (client_id) REFERENCES clients(id);


--
-- Name: callstats_phone_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats
    ADD CONSTRAINT callstats_phone_id_fk FOREIGN KEY (phone_id) REFERENCES phones(id);


--
-- Name: callstats_stats_period_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY callstats
    ADD CONSTRAINT callstats_stats_period_id_fk FOREIGN KEY (stats_period_id) REFERENCES stats_periods(id);


--
-- Name: cdrviewer_cdr_calledbtn_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    ADD CONSTRAINT cdrviewer_cdr_calledbtn_id_fk FOREIGN KEY (calledbtn_id) REFERENCES btns(id);


--
-- Name: cdrviewer_cdr_calledphone_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    ADD CONSTRAINT cdrviewer_cdr_calledphone_id_fk FOREIGN KEY (calledphone_id) REFERENCES phones(id);


--
-- Name: cdrviewer_cdr_callingbtn_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    ADD CONSTRAINT cdrviewer_cdr_callingbtn_id_fk FOREIGN KEY (callingbtn_id) REFERENCES btns(id);


--
-- Name: cdrviewer_cdr_callingphone_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    ADD CONSTRAINT cdrviewer_cdr_callingphone_id_fk FOREIGN KEY (callingphone_id) REFERENCES phones(id);


--
-- Name: cdrviewer_cdr_datasource_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdrviewer_cdr
    ADD CONSTRAINT cdrviewer_cdr_datasource_id_fk FOREIGN KEY (datasource_id) REFERENCES cdrviewer_datasource(id);


--
-- Name: clients_billing_site_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_billing_site_id_fk FOREIGN KEY (billing_site_id) REFERENCES sites(id);


--
-- Name: phones_btn_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY phones
    ADD CONSTRAINT phones_btn_id_fk FOREIGN KEY (btn_id) REFERENCES btns(id);


--
-- Name: phones_site_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY phones
    ADD CONSTRAINT phones_site_id_fk FOREIGN KEY (site_id) REFERENCES sites(id);


--
-- Name: portal_customerservice_service_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY portal_customerservice
    ADD CONSTRAINT portal_customerservice_service_id_fk FOREIGN KEY (service_id) REFERENCES portal_service(id);


--
-- Name: sites_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sites
    ADD CONSTRAINT sites_client_id_fk FOREIGN KEY (client_id) REFERENCES clients(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO public;

INSERT INTO schema_migrations (version) VALUES ('20110118210000');

INSERT INTO schema_migrations (version) VALUES ('20110611001644');

INSERT INTO schema_migrations (version) VALUES ('20110611152615');

INSERT INTO schema_migrations (version) VALUES ('20110611152616');

INSERT INTO schema_migrations (version) VALUES ('20110611152617');

INSERT INTO schema_migrations (version) VALUES ('20110611213711');

INSERT INTO schema_migrations (version) VALUES ('20110612015544');

INSERT INTO schema_migrations (version) VALUES ('20110612022609');

INSERT INTO schema_migrations (version) VALUES ('20110612212210');

INSERT INTO schema_migrations (version) VALUES ('20110615020536');

INSERT INTO schema_migrations (version) VALUES ('20111215013745');

INSERT INTO schema_migrations (version) VALUES ('20111215013746');

INSERT INTO schema_migrations (version) VALUES ('20111220162119');

INSERT INTO schema_migrations (version) VALUES ('20111223212055');

INSERT INTO schema_migrations (version) VALUES ('20120104145935');

INSERT INTO schema_migrations (version) VALUES ('20120108010101');

INSERT INTO schema_migrations (version) VALUES ('20120109020707');

INSERT INTO schema_migrations (version) VALUES ('20120319165550');

INSERT INTO schema_migrations (version) VALUES ('20120511173107');

INSERT INTO schema_migrations (version) VALUES ('20120518162715');

INSERT INTO schema_migrations (version) VALUES ('20120525191127');

INSERT INTO schema_migrations (version) VALUES ('20120530003656');

INSERT INTO schema_migrations (version) VALUES ('20120530133839');

INSERT INTO schema_migrations (version) VALUES ('20120531180705');

INSERT INTO schema_migrations (version) VALUES ('20120605145255');

INSERT INTO schema_migrations (version) VALUES ('20120619212239');

INSERT INTO schema_migrations (version) VALUES ('20120720140627');

INSERT INTO schema_migrations (version) VALUES ('20120910150100');

INSERT INTO schema_migrations (version) VALUES ('20120927153411');

INSERT INTO schema_migrations (version) VALUES ('20120930155733');

INSERT INTO schema_migrations (version) VALUES ('20121119230712');

INSERT INTO schema_migrations (version) VALUES ('20121210223747');

INSERT INTO schema_migrations (version) VALUES ('20121214150142');

INSERT INTO schema_migrations (version) VALUES ('20121217221222');

INSERT INTO schema_migrations (version) VALUES ('20121217222938');

INSERT INTO schema_migrations (version) VALUES ('20121218211520');

INSERT INTO schema_migrations (version) VALUES ('20121224175652');

INSERT INTO schema_migrations (version) VALUES ('20130123175828');

INSERT INTO schema_migrations (version) VALUES ('20130128202056');

INSERT INTO schema_migrations (version) VALUES ('20130128221640');

INSERT INTO schema_migrations (version) VALUES ('20130130205832');

INSERT INTO schema_migrations (version) VALUES ('20130207165956');

INSERT INTO schema_migrations (version) VALUES ('20130212022806');

INSERT INTO schema_migrations (version) VALUES ('20130215160026');

INSERT INTO schema_migrations (version) VALUES ('20130307151504');

INSERT INTO schema_migrations (version) VALUES ('20130401233638');

INSERT INTO schema_migrations (version) VALUES ('20130405174138');

INSERT INTO schema_migrations (version) VALUES ('20130501175748');

INSERT INTO schema_migrations (version) VALUES ('20130705212304');

INSERT INTO schema_migrations (version) VALUES ('20130711171033');

INSERT INTO schema_migrations (version) VALUES ('20150609210006');

INSERT INTO schema_migrations (version) VALUES ('20150609210533');

INSERT INTO schema_migrations (version) VALUES ('20150609210935');

INSERT INTO schema_migrations (version) VALUES ('20150617191302');

INSERT INTO schema_migrations (version) VALUES ('20150713220846');

INSERT INTO schema_migrations (version) VALUES ('20150918173306');

INSERT INTO schema_migrations (version) VALUES ('20150921181754');

INSERT INTO schema_migrations (version) VALUES ('20150925005818');

INSERT INTO schema_migrations (version) VALUES ('20150928020202');

INSERT INTO schema_migrations (version) VALUES ('20150928192709');

INSERT INTO schema_migrations (version) VALUES ('20151210165514');

INSERT INTO schema_migrations (version) VALUES ('20160121025849');

INSERT INTO schema_migrations (version) VALUES ('20160121025850');

INSERT INTO schema_migrations (version) VALUES ('20160122142946');

INSERT INTO schema_migrations (version) VALUES ('20160122150000');

INSERT INTO schema_migrations (version) VALUES ('20160208162955');

INSERT INTO schema_migrations (version) VALUES ('20160212200500');

INSERT INTO schema_migrations (version) VALUES ('20160216213344');

INSERT INTO schema_migrations (version) VALUES ('20160217004414');

INSERT INTO schema_migrations (version) VALUES ('20160217004520');

INSERT INTO schema_migrations (version) VALUES ('20160217230224');

INSERT INTO schema_migrations (version) VALUES ('20160218205905');

INSERT INTO schema_migrations (version) VALUES ('20160218210000');

INSERT INTO schema_migrations (version) VALUES ('20160218210001');

INSERT INTO schema_migrations (version) VALUES ('20160218210002');

INSERT INTO schema_migrations (version) VALUES ('20160221032705');

INSERT INTO schema_migrations (version) VALUES ('20160314184100');

INSERT INTO schema_migrations (version) VALUES ('20160314193028');

INSERT INTO schema_migrations (version) VALUES ('20160321000000');

INSERT INTO schema_migrations (version) VALUES ('20160321180625');

INSERT INTO schema_migrations (version) VALUES ('20160322');

INSERT INTO schema_migrations (version) VALUES ('20160322173100');

