#!/bin/bash

# Extracts all the *.gz files to tmp

mostRecentDir=`find -maxdepth 1 -type d -name '20*' | sort | tail -n 1`

dirToExtract=${1:-$mostRecentDir}

if [ ! -d "$dirToExtract" ] ; then
    echo $dirToExtract is not a dir
    exit 1
fi

read -p "Wipe all LERG files from /tmp? (Y/*) " yn
if [[ "$yn" =~ ^[Yy] ]]; then
    echo Wiping LERG files
    rm /tmp/LERG*
fi

for file in $dirToExtract/*.gz ; do
    bn=`basename $file`
    target=/tmp/`echo $bn | rev | cut -d '.' -f 2- | rev` ;

    if [ -f $target ]; then

        ls -lh $target;
        read -p "Do you wish to overwrite this file? (Y/N) " yn

        if [[ ! "$yn" =~ ^[Yy] ]]; then
            echo Answer did not contain Y or y.  Not overwritting.
            continue
        fi

    fi

    gzip -dcv $file > $target;
    ls -lh $target;
done
