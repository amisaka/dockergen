These files were created on mercredi 11 mai 2016, 16:33:12 (UTC-0400)

The related ticket number is 107954

This set was created because SAINT PAUL was missing from the NNACL file.  It was missing because "SAINT PAUL, ALBERTA" became the effective full name entry starting mid - April.  My script got confused because there was a Saint Paul rate center in the US.  This is the corrected version with "SAINT PAUL, ALBERTA" but without "SAINT PAUL"  
-- jlam@credil.org 
