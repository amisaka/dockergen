== README
 
This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

ruby >= 2.3.1
rails ~> 5.0

* System dependencies

- postgresql

* Configuration

Copy dotenv sample file and edit configuration values to suit your needs

```
cp .env.sample .env
```

* Database creation

```
sudo apt-get install postgresql postgresql-contrib
```

* Database initialization

```
bundle exec ruby rake db:setup
```

* How to run the test suite

```
bundle exec ruby bin/rspec
```

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

```
bundle exec cap production deploy
```


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
