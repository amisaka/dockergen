
# Add the following to the ~/.vimrc file for user jason on app:
# :colorscheme delek
# :syntax on
# :set nu

# On host machine:
# Installed docker and docker-compose through digital ocean tutorial, have to
# run the following if permissions are an issue when running docker:
sudo gpasswd -a jason docker
sudo usermod -aG docker jason
sudo setfacl -m user:jason:rw /var/run/docker.sock
# From https://askubuntu.com/questions/477551/how-can-i-use-docker-without-sudo
# The mechanism by which adding a user to group docker grants permission to run 
# docker is to get access to the socket of docker at /var/run/docker.sock. If 
# the filesystem that contains /var/run has been mounted with ACLs enabled, 
# this can also be achieved via ACLs.
sudo setfacl -m user:username:rw /var/run/docker.sock
# I'm only including this for completeness.
# In general, I recommend to avoid ACLs whenever a good alternative based on 
# groups is available: It is better if the privileges in a system can be 
# understood by looking at group memberships only. Having to scan the file system 
# for ACL entries in order to understand system privileges is an additional burden for security audits.
# Jason: I could not get the groups to work, the ACL did.

# For the following, make sure I am doing it to the right db env found in
# config/database.yml. That is, the host needs to be db, which is the postgres
# container.
host: db

# I just ran the following and I could access the site:
docker-compose build
docker-compose up
# ... this is because when I ran build, it did not wipe out what I did below. 
# That is, I do not need to go through the following when I make a change to 
# docker config files for a set of containers.

################################################################################
# NGINX
################################################################################

# I had a page that was taking too long to reload, added the following to group
# http in file /etc/nginx/nginx.conf:
#   proxy_read_timeout 600s;
# Then, I ran the following in the terminal:
nginx -s reload

# I need to add that line to /etc/nginx/conf.d/default.conf so when the system
# reboots it does keeps that line. I am not sure how to set that up.
# I have added it.

################################################################################
# DATABASE
################################################################################

# Make sure that the file code/config/database.yml has the right configuration 
# for 'RAILS_ENV test'.

#Need to connect to database and create users (I can automate this at some point
in the docker file changing to user postgres and running the commands):
docker exec -t -i work_db_1 /bin/bash
su - postgres
exec bash
createuser user_name
createdb db_name
psql
alter user user_name with encrypted password 'dbsecretpw';
grant all privileges on database db_name to user_name;
alter user user_name CREATEDB;
ALTER USER user_name WITH SUPERUSER; # See comments below.
\conninfo # Gets connection info, get the host (i.e. '/var/run/postgresql').
\q
exit
psql -d db_name -U user_name -h '/var/run/postgresql'
# The following also worked:
psql -d db_name -U user_name -h localhost

# These may need to be run before the next section or after.
docker-compose run app rake db:create RAILS_ENV=test # Already created db
						     # already, do I need to do
						     # this?
docker-compose run app rake db:migrate db:seed RAILS_ENV=test # This did not
							      # work for my work
							      # repo.

# In postgres, to connect to db:
\connect db_name;

# For the following to work, I need to assign superuser to user_name.
docker-compose run app bin/rails db:environment:set RAILS_ENV=test
docker-compose run app rake db:setup RAILS_ENV=test
# Run this instead of 'docker-compose run app rake db:migrate db:seed RAILS_ENV=test'
# Super user:
ALTER USER user_name WITH SUPERUSER;

# Then I will need to set up ssh connect to the container and put my private key
# on it so I can push to sged.credil.org.

# For sensitive or env specific variables, use env_file in the
# docker-compose.yml file:
# https://docs.docker.com/compose/compose-file/#env_file

# For new environments, or instances of the setup, make sure to change the
# /var/www/dir_name for web and app Dockerfiles.
# Make sure the root dir for the docker-compose.yml file is the same as
# dir_name, it may not matter, but I have had issues and they may have been
# related, did not have to to clarify.

# Login as root, see dockerNotes.txt on how to create users, develop with them.
docker exec -t -i wrk_app_1 /bin/bash # To login as root.
docker exec --user user_name -t -i wrk_app_1 /bin/bash # Login in as another user.

################################################################################
# SAMPLE DATA
################################################################################

# Make sure the config/database.yml is configured correctly for test.

# TODO: Need to copy over sample data to work with.
PGPASSWORD=dbsecretpw psql -h localhost -d db_name -U user_name < sandbox.data.sql

################################################################################
# SSH
################################################################################

# Note: On home laptop or computer, if it is on a separate IP version such as
# IPv4 and the machine that you are supposed to deploy to is on IPv6, then push
# changes to remote repo and then login in to work computer that has both IPv4
# and IPv6, and deploy from the work computer.

# TODO: Copy over .ssh/id_rsa key so you can push to specified repo or do cap
# deploy..
docker cp ../../../../.ssh/id_rsa wrk_app_1:/home/jjrh/
docker exec --user user_name -t -i wrk_app_1 /bin/bash #
sudo chown jjrh:jjrh ~/.ssh/id_rsa

# Make sure on docker machine that the following is done.
eval $(ssh-agent)
ssh-add
# or specific key:
ssh-add  ~/.ssh/keyname # I think that this is private key.
ssh-add -l # see what keys are added.
# The .ssh/config file has local config settings.

ssh user_name_on_remote_machine@url
w
id

################################################################################
# RAILS USER
################################################################################

# May be able to do this through Dockerfile somehow.

# cd to rails root dir and run the following:
rails console
User.connection # This shows all the attributes of the User class.
User.create!(
  username: "test_user2", 
  fullname: "Test User2", 
  email: "test_user2@example.com", 
  password: "test1test1",
  password_confirmation: "test1test1", 
  confirmed_at: Time.now,
  admin: true,
  agent: true)

################################################################################
# RAILS DEVELOPMENT
################################################################################

# Check env:
rails console
Rails.env

# Changed the following so code changes will appear in the web browser faster.
# The file is config/environments/test.rb:
#   config.cache_classes = false
#   config.consider_all_requests_local       = false
#   config.action_controller.perform_caching = true
# Change the above to their opposites.

# For changes that in css and javascript, I may need to disable caching in the
# web browser.

###############################################################################
# TERMINAL
################################################################################

# If I run the followng, then I see what terminal type I am using (I can change
# this at some point.
echo $TERM
  xterm

# To resize the window (currenlty, it is small for me), run the following:
echo "xterm*allowWindowOps: true" > ~/.Xdefaults
# Logout and login
echo -ne "\e[8;30;30t"
# I do not know what that does, but it worked, even if I logged out and in
# again.

