#!/bin/bash

TYPE=$1
WDIR=$2
RVERSION=$3
FULLNAME=$(awk -v user="$USER" -F":" 'user==$1{print $5}' /etc/passwd)
# This variable above assumes that your full name is into /etc/passwd

cat << EOF
FROM $TYPE:$RVERSION
LABEL maintainer="$FULLNAME $USER@credil.org"
LABEL description="This is a default template for Ruby On Rails"

RUN apt-get update -qq && \
    apt-get install -y apt-utils build-essential libpq-dev nodejs vim curl
RUN mkdir /$WDIR
WORKDIR /$WDIR
COPY Gemfile /$WDIR/Gemfile
COPY Gemfile.lock /$WDIR/Gemfile.lock
RUN bundle install
COPY . /$WDIR

EOF
