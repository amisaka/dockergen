#!/bin/bash

DBNAME=$1
PORT=$2
PROJNAME=$3
WDIR=$4


cat << EOF
version: '3'

services:
  db:
    image: $DBNAME
    volumes:
      - ./tmp/db:/var/lib/postgresql/data
  web:
    build: .
    command: bundle exec rails s -p $PORT -b '0.0.0.0'
    volumes:
    - .:/$WDIR
    ports:
    - "$PORT:$PORT"
    depends_on:
    - db

EOF
