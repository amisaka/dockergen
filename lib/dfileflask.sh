#!/bin/bash

TYPE=$1
WDIR=$2
VERSION=$3
FULLNAME=$(awk -v user="$USER" -F":" 'user==$1{print $5}' /etc/passwd)
# This variable above assumes that your full name is into /etc/passwd

cat << EOF
FROM $TYPE:$VERSION
LABEL maintainer="$FULLNAME $USER@credil.org"
LABEL description="This is a default template for Python-Flask"


RUN apt-get update -y && \
    apt-get install -y apt-utils python-pip python-dev vim curl

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /$WDIR/requirements.txt
COPY ./app.py /$WDIR/app.py

WORKDIR /$WDIR

RUN pip install -r requirements.txt

COPY . /$WDIR
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
EOF
