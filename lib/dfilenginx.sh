#!/bin/bash

TYPE=$1
NVER=$2
FULLNAME=$(awk -v user="$USER" -F":" 'user==$1{print $5}' /etc/passwd)
# This variable above assumes that your full name is into /etc/passwd

cat << EOF
FROM $TYPE:$NVER

LABEL maintainer="$FULLNAME $USER@credil.org"
LABEL description="This is a default template for Django"

RUN apk add --update nginx && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /tmp/nginx/

COPY files/nginx.conf /etc/nginx/nginx.conf
COPY files/default.conf /etc/nginx/conf.d/default.conf

ADD files/html.tar.gz /usr/share/nginx/

EXPOSE 80/tcp

ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]
EOF
