#!/bin/bash

PORT=$1



cat << EOF
version: '3'

services:
  web:
    build: .
    ports:
        - "$PORT:80"
EOF
