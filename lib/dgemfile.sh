#!/bin/bash
# Ruby on Rails Gemfile generator

VERSION=$1

cat << EOF
source 'https://rubygems.org'
gem 'rails', '$VERSION'

EOF
