#!/bin/bash

VERSION=$1

cat << EOF
Django>=$VERSION
psycopg2-binary
gunicorn
EOF

