#!/bin/bash
# Jason's sandbox Ruby on Rails Gemfile generator

VERSION=$1

cat << EOF
source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', git: 'https://github.com/rails/rails.git'
gem 'rails', '~> $VERSION'

# Documentation
gem 'yard'

# App Server
gem 'puma'

# Use SCSS for stylesheets
gem 'sass-rails', '>= 3.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'

# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Postgresl
gem 'pg'

# needed for suitecrm connetion.
# gem 'mysql'

# pull in ActiveResource to reach out of Broadworks
gem 'rest-client'

# hakka database is mssql
group :hakka do
  gem 'tiny_tds', "~> 1.3.0"
  gem 'activerecord-sqlserver-adapter', '~> 5.0.0'
end

gem 'business_time'

gem "dynamic_form"

gem 'iconv'

# Bootstrap
gem 'bootstrap-sass', '~> 3.3.6'

# use delayed job with active_record
gem 'delayed_job_active_record'

# daemonize for delayed job.
gem 'daemons'

# Font Awesome (icons)
gem 'font-awesome-sass', '~> 4.7.0'

# Use Unicorn as the app server
# gem 'unicorn'

gem 'ipaddress'
gem 'uuid'

# time of day parser
gem 'tod'
gem 'timezone', '~> 1.0'

# Authentication
gem 'devise', "~> 4.3.0"

gem 'best_in_place'

# used in production.
gem 'SyslogLogger'

gem 'cancancan'
gem 'nokogiri', '>= 0.5.1'

gem 'canonicalized-cdn-provinces', git: 'https://github.com/credil/canonicalized-cdn-provinces-ruby.git'
#gem 'canonicalized-cdn-provinces', :path => '../canonicalized-cdn-provinces-ruby'

# GPG is used to encrypt (SIP) passwords without requiring a symmetric
# master key available everywhere.
gem 'acts_as_encrypted_with_gpgme', git: 'https://github.com/credil/acts_as_encrypted_with_gpgme.git'
gem 'gpgme', git: 'https://github.com/ueno/ruby-gpgme.git'

#gem 'gpgme', :path => '/home/mcr/C/nv/ruby-gpgme'
#gem 'acts_as_encrypted_with_gpgme', :path => '/home/mcr/C/nv/acts_as_encrypted_with_gpgme'

# Bundle gems for the local environment. Make sure to
# put test-only gems in this group so their generators
# and rake tasks are available in development mode:
group :development do
  gem 'web-console', '~> 2.3.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rspec'
end

# Call 'byebug' anywhere in the code to stop execution and get a debugger console
gem 'byebug'

# Use Pry console for all modes
gem 'pry-rails'

# Fake data for testing
gem 'ffaker'

group :development, :test do
  # Deploy with Capistrano
  gem 'capistrano',  '~> 3.9'
  gem 'capistrano-rbenv', '~> 2.1'
  gem 'capistrano-bundler', '~> 1.3'
  gem 'capistrano-rails', '~> 1.3'

  # Capistrano puma integration
  gem 'capistrano3-puma'

  gem 'immigrant'

  # Rspec for testing
  gem 'rspec-rails', '~> 3.0'

  # tests for common Rails functionality
  gem 'shoulda-matchers'

  # Factories
  gem 'factory_bot_rails'

  # test automatically
  gem 'listen', '3.0' # required for quard with mri 4.1.5
  gem 'ruby_dep', '1.3' # required for quard with mri 4.1.5
  gem 'guard-rspec', require: false
end

# used for asset-precompile!
gem 'sqlite3'

# Capybara DSL and acceptance test framework
gem 'capybara', group: [:test]

# Use responders controller
gem 'responders'

# Pagination
gem 'kaminari'

# tinymce-rails WYSIWYG html editor for editing formatteble invoice message
gem 'tinymce-rails'

# I18n locales for multiple languages
gem 'rails-i18n', '~> $VERSION'

# Decorators
gem 'draper'

# Error logging
gem "sentry-raven"

# Searches
gem 'searchlight'

# Bootstrap Datepicker
gem 'bootstrap-datepicker-rails'

# Foreman
gem 'foreman'
gem 'dotenv-rails'

# Paperclip file attachments
gem "paperclip", "~> 5.0.0"

# Phone normalization
gem 'phony'

EOF