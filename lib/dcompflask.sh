#!/bin/bash

DBNAME=$1
PORT=$2
PROJNAME=$3
WDIR=$4


cat << EOF
version: '3'

services:
  web:
    build: .
    ports:
        - "$PORT:$PORT"
    volumes:
    - .:/$WDIR
    depends_on:
    - $DBNAME
  $DBNAME:
    image: redis
EOF

