#!/bin/bash

TYPE=$1
RVERSION=$2
WDIR=$3
FULLNAME=$(awk -v user="$USER" -F":" 'user==$1{print $5}' /etc/passwd)
# This variable above assumes that your full name is into /etc/passwd

cat << EOF
FROM $TYPE:$RVERSION
LABEL maintainer="$FULLNAME $USER@credil.org"
LABEL description="This is a CREDIL template for Ruby On Rails"

# Install dependencies
RUN apt-get update -qq && \
    apt-get install -y build-essential libpq-dev nodejs procps net-tools iputils-ping vim curl
RUN apt-get install -y --no-install-recommends apt-utils

# User sudo access
RUN apt-get update && apt-get install -y sudo

# SSH
RUN apt-get update && apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:test1' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Set an environment variable where the Rails app is installed to inside of
# Docker image:
ENV RAILS_ROOT $WDIR
RUN mkdir -p \$RAILS_ROOT

# Set working directory, where the commands will be ran:
WORKDIR \$RAILS_ROOT

# ALL as New User
RUN useradd -d /home/$USER -ms /bin/$USER -g root -G sudo -p $USER $USER
RUN echo '$USER:test1' | chpasswd
RUN groupadd $USER
RUN usermod -a -G $USER $USER
COPY var/.vimrc /home/$USER/

# Setting env up
# When this is set, and there are not controllers to views, then the result is a
# message on a page that is "The page you were looking for doesn't exist." When
# the env variable is not production, then the default rails page shows up.
#ENV RAILS_ENV='production'
#ENV RAKE_ENV='production'
ENV RAILS_ENV='test'
ENV RAKE_ENV='test'

# TODO: Put the code in a subdir that is in .gitignore,
# then, add all the new files to the public.devops repo.
# In the Makefile, create a dir and that is where I will run git clone ...


# Adding gems
COPY src/Gemfile Gemfile
COPY src/Gemfile.lock Gemfile.lock
#RUN bundle install --jobs 20 --retry 5 --without development test
RUN bundle install --jobs 20 --retry 5 --without development production

# Adding project files
COPY src/ .

RUN chown -R $USER:$USER /var/www/rails

USER $USER
#WORKDIR /home/$USER

RUN bundle exec rake assets:precompile

USER root

EXPOSE 3000 22

# The CMD is the command that should be run on 'docker-compose up', the above
# stuff only gets setup on 'docker-compose build'. That is, CMD is to start the
# service(s) when the docker container is started. Ideally, there should be one
# service per container.
#CMD ["bundle", "exec", "puma", "-C", "config/puma.rb", "&&", "/usr/sbin/sshd", "-D"]
USER $USER
CMD ["sh","-c", "bundle exec puma -C config/puma.rb && /usr/sbin/sshd -D"]
#CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]

EOF
