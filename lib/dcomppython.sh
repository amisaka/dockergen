#!/bin/bash

DBNAME=$1
PORT=$2
PROJNAME=$3
WDIR=$4
APPNAME=$5


cat << EOF
version: '3'

services:
  db:
    image: $DBNAME
  web:
    build: .
    command: bash -c "python3 manage.py makemigrations && \
                     python3 manage.py migrate && \
                     python3 manage.py startapp $APPNAME && \
                     python3 manage.py runserver 0.0.0.0:$PORT"
    container_name: $WDIR
    volumes:
    - .:/$WDIR
    ports:
    - "$PORT:$PORT"
    depends_on:
    - db

EOF
