#!/bin/bash

TYPE=$1
WDIR=$2
APPNAME=$3
FULLNAME=$(awk -v user="$USER" -F":" 'user==$1{print $5}' /etc/passwd)
# This variable above assumes that your full name is into /etc/passwd

cat << EOF
FROM $TYPE:3

LABEL maintainer="$FULLNAME $USER@credil.org"
LABEL description="This is a default template for Django"

ENV PYTHONUNBUFFERED 1

RUN apt-get update -y && \
    apt-get install -y apt-utils python-pip python-dev vim curl

RUN mkdir /$WDIR
WORKDIR /$WDIR
ADD requirements.txt /$WDIR/
RUN pip install -r requirements.txt
ADD . /$WDIR/

EOF
