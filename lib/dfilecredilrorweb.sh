#!/bin/bash

TYPE=$1
WORKDIR=$2
PROJECT=$3
FULLNAME=$(awk -v user="$USER" -F":" 'user==$1{print $5}' /etc/passwd)
# This variable above assumes that your full name is into /etc/passwd

cat << EOF
FROM $TYPE

LABEL maintainer="$FULLNAME $USER@credil.org"
LABEL description="This is a CREDIL's template for RAILS+NGINX"

# Install dependencies
RUN apt-get update -qq && apt-get -y install apache2-utils apt-utils
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs procps net-tools iputils-ping vim curl
RUN apt-get install -y --no-install-recommends apt-utils

# establish where Nginx should look for files
ENV RAILS_ROOT $WORKDIR

# Set our working directory inside the image
WORKDIR \$RAILS_ROOT

# create log directory
RUN mkdir log

# copy over static assets
COPY src/public public/

# Copy Nginx config template
COPY $PROJECT/web/nginx.conf /tmp/docker.nginx

# substitute variable references in the Nginx config template for real values
# from the environment
# put the final config in its place
RUN envsubst '\$RAILS_ROOT' < /tmp/docker.nginx > /etc/nginx/conf.d/default.conf

EXPOSE 8002

# Use the "exec" form of CMD so Nginx shuts down gracefully on SIGTERM (i.e.
# 'docker stop')
CMD [ "nginx", "-g", "daemon off;" ]
EOF
