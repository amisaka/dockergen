#!/bin/bash

DBNAME=$1
PORT=$2
PROJNAME=$3
APPNAME=$4

cat << EOF
version: '3'

volumes:
  postgres_data: {}
  rails_code: {}
  app_user: {}

services:
  $APPNAME:
    build:
      context: .
      dockerfile: ./$PROJNAME/$APPNAME/Dockerfile
    volumes:
      - rails_code:/var/www/rails_dev/
      - app_user:/home/$USER/
    depends_on:
      - db
    ports:
      - 127.0.2.1:2222:22

  db:
    image: $DBNAME
    volumes:
      - postgres_data:/var/lib/postgresql/data

  web:
    build:
      context: .
      dockerfile: ./$PROJNAME/web/Dockerfile
    depends_on:
      - $APPNAME
    ports:
    - 127.0.2.1:$PORT:80
    # The above sets the IP address for web container. I think it can be
    # either IPv6 or IPv4. URL or DNS names can be assigned to the different
    # IP addresses in the local host's (Jason's desktop's) /etc/hosts file.
    #- 8002:80
    # This needs to be 8002:80, because the Nginx web server on docker
    # container needs to be 80 and then the 8002 is the port that points to it
    # on the host machine (Jason's desktop).
    #      - "8003:8003"
    #      - "8004:8004"
    #networks:
    #  vpcbr:
    #  ipv4_address: 10.5.0.6
    #
#networks:
#  vpcbr:
#    driver: bridge
#    ipam:
#     config:
#       - subnet: 10.5.0.0/16
EOF
